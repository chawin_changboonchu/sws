﻿using System;
using System.Linq;
using System.Windows.Forms;
using SWS.Model;

namespace SWS
{
    public partial class Inventory
    {
        private void AL1SelectBtn_Click(object sender, EventArgs e)
        {
            log.Info("Start Method: AL1SelectBtn_Click() No parameter");

            if (aL1OptionsLsb.SelectedItem != null)
            {
                aL1LotFormatTxb.Text += aL1OptionsLsb.SelectedItem.ToString();
                aL1ExampleTxb.Text = ReplaceFormatTextExample(aL1LotFormatTxb.Text);
            }

            log.Info("End Method: AL1SelectBtn_Click()");
        }

        private void AL1ClearBtn_Click(object sender, EventArgs e)
        {
            log.Info("Start Method: AL1ClearBtn_Click() No parameter");
            
            aL1LotFormatTxb.Text = "";
            aL1ExampleTxb.Text = "";

            WriteLog(0, "Lot", "Setup", $"เคลียร์การตั้งค่า Lot");
            log.Info("End Method: AL1ClearBtn_Click()");
        }

        private void LotSetupTabPage_Enter(object sender, EventArgs e)
        {
        }

        private void AL1LotTypeCbb_SelectedIndexChanged(object sender, EventArgs e)
        {
            log.Info("Start Method: AL1LotTypeCbb_SelectedIndexChanged() No parameter");
            
            var itemType = aL1LotTypeCbb.SelectedItem as ItemType;
            aL1LotFormatTxb.Text = itemType.LotFormat;
            aL1ExampleTxb.Text = ReplaceFormatTextExample(aL1LotFormatTxb.Text);

            log.Info("End Method: AL1LotTypeCbb_SelectedIndexChanged()");
        }

        private void AL1SubmitBtn_Click(object sender, EventArgs e)
        {
            log.Info("Start Method: AL1SubmitBtn_Click() No parameter");
            try
            {
                var itemType = aL1LotTypeCbb.SelectedItem as ItemType;
                _entities.ItemType.FirstOrDefault(it => it.ID == itemType.ID).LotFormat = aL1LotFormatTxb.Text;
                _entities.SaveChanges();

                WriteLog(0,"Lot","Setup",$"เปลี่ยนการตั้งค่า Lot ของ {itemType?.Name} เป็น {aL1LotFormatTxb.Text}");
                MessageBox.Show("บันทึกเสร็จสิ้น");
            }
            catch (Exception ex)
            {
                log.Error("Method: AL1SubmitBtn_Click() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: AL1SubmitBtn_Click()");
            }
        }
    }
}