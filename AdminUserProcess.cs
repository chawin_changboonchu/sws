﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using SWS.Model;

namespace SWS
{
    public partial class Inventory
    {
        private void AU1SubmitNewUserBtn_Click(object sender, EventArgs e)
        {
            log.Info("Start Method: AU1SubmitNewUserBtn_Click() No parameter");

            try
            {
                var newUsername = aU1NewUserNameTxb.Text;
                var newPassword = aU1NewPasswordTxb.Text;

                if (string.IsNullOrEmpty(newUsername) || string.IsNullOrEmpty(newPassword))
                {
                    MessageBox.Show("กรุณากรอกชื่อและรหัสผ่านให้ถูกต้อง");
                    return;
                }

                var newHashedPswd = ComputeSha256Hash(newPassword);

                if (_entities.User.Any(u => u.UserName == newUsername))
                {
                    MessageBox.Show("มีผู้ใช้งานชื่อนี้แล้วในระบบ");
                    return;
                }
                var newUser = _entities.User.Add(new User()
                {
                    UserName = newUsername,
                    Password = newHashedPswd,
                    CreationDate = DateTime.Now,
                    FirstLogin = true,
                    RoleID = (long)aU1NewRoleCbb.SelectedValue
                });

                //_userList.Add(new AdminUserUserTable()
                //{
                //    CreationDate = newUser.CreationDate ?? DateTime.Now,
                //    UserName =  newUser.UserName,
                //    LastLoginDate = DateTime.MinValue,
                //    RoleID = newUser.RoleID.Value
                //});


                _entities.SaveChanges();

                InitializeUserTable();
            }
            catch (Exception ex)
            {
                log.Error("Method: AU1SubmitNewUserBtn_Click() Error: " + ex.Message); MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                WriteLog(0,"User","Add new",
                    $"เพิ่มผู้ใช้ใหม่:{aU1NewUserNameTxb.Text} แผนก:{aU1NewRoleCbb.SelectedItem?.ToString()}");
                log.Info("End Method: AU1SubmitNewUserBtn_Click()");
            }
        }

        private void AdminUserTabPage_Enter(object sender, EventArgs e)
        {
            
        }

        private void InitializeUserTable()
        {
            log.Info("Start Method: InitializeUserTable() No parameter");

            try
            {
                _userList = new BindingList<AdminUserUserTable>();
                foreach (var user in _entities.User.ToList())
                {
                    _userList.Add(new AdminUserUserTable()
                    {
                        RoleID = user.Role.ID,
                        CreationDate = user.CreationDate ?? DateTime.MinValue,
                        UserName = user.UserName,
                        LastLoginDate = user.LastLoginDate ?? DateTime.MinValue
                    });
                }

                aU1UserListDgv.Columns.Clear();
                aU1UserListDgv.DataSource = _userList;
                aU1UserListDgv.Columns["UserName"].ReadOnly = true;
                aU1UserListDgv.Columns["CreationDate"].ReadOnly = true;
                aU1UserListDgv.Columns["LastLoginDate"].ReadOnly = true;
                aU1UserListDgv.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

                aU1UserListDgv.Columns.Add(new DataGridViewComboBoxColumn()
                {
                    Name = "Role",
                    HeaderText = "แผนก",
                    DataSource = _entities.Role.Where(r => r.Name != "Admin").ToList(),
                    DisplayMember = "Name",
                    ValueMember = "ID"
                });

                aU1UserListDgv.Columns.Add(new DataGridViewButtonColumn()
                {
                    Text = "ลบ",
                    Name = "delete button",
                    HeaderText = "ลบผู้ใช้",
                    UseColumnTextForButtonValue = true
                });

                aU1UserListDgv.Refresh();
            }
            catch (Exception ex)
            {
                log.Error("Method: InitializeUserTable() Error: " + ex.Message); MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: InitializeUserTable()");
            }
        }

        private void AU1UserListDgv_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            log.Info("Start Method: AU1UserListDgv_CellEndEdit() No parameter");

            try
            {
                var adminUserTableItem = aU1UserListDgv.Rows[e.RowIndex].DataBoundItem as AdminUserUserTable;
                if (adminUserTableItem.RoleID == 1)
                {
                    return;
                }

                if (!aU1UserListDgv.Columns.Contains("Role"))
                {
                    return;
                }

                var roleCbb = (DataGridViewComboBoxCell)(aU1UserListDgv.Rows[e.RowIndex].Cells["Role"]);

                var selectedRoleID = roleCbb.Value;

                if (adminUserTableItem.RoleID != Convert.ToInt64(selectedRoleID))
                {
                    var user = _entities.User.FirstOrDefault(u => u.UserName == adminUserTableItem.UserName);
                    user.RoleID = Convert.ToInt64(selectedRoleID);
                    _entities.SaveChanges();

                    WriteLog(0, "User", "Change Role", $"เปลี่ยนแผนกของผู้ใช้:{user} เป็น Role ID:{user.RoleID}");
                }
                adminUserTableItem.RoleID = Convert.ToInt64(selectedRoleID);
            }
            catch (Exception ex)
            {
                log.Error("Method: AU1UserListDgv_CellEndEdit() Error: " + ex.Message); MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: AU1UserListDgv_CellEndEdit()");
            }
        }

        private void AU1UserListDgv_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            log.Info("Start Method: AU1UserListDgv_CellClick() No parameter");

            try
            {
                var button = aU1UserListDgv.Columns[e.ColumnIndex] as DataGridViewButtonColumn;
                if (button != null && button.Name == "delete button")
                {
                    var adminUserTableItem = aU1UserListDgv.Rows[e.RowIndex].DataBoundItem as AdminUserUserTable;
                    var confirmResult =  MessageBox.Show("คุณต้องการลบผู้ใช้งาน " + adminUserTableItem.UserName + " ?",
                        "Confirm Delete!!",
                        MessageBoxButtons.YesNo);
                    
                    if (confirmResult == DialogResult.Yes)
                    {
                        var deleteUser = _entities.User.FirstOrDefault(u => u.UserName == adminUserTableItem.UserName);

                        WriteLog(0,"User","Delete",$"ลบผู้ใช้ {deleteUser?.UserName}");

                        _userList.Remove(adminUserTableItem);
                        _entities.User.Remove(deleteUser);
                        _entities.SaveChanges();
                        InitializeUserTable();
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Method: AU1UserListDgv_CellClick() Error: " + ex.Message); MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: AU1UserListDgv_CellClick()");
            }
        }

        private void aU1UserListDgv_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            var adminUserTableItem = aU1UserListDgv.Rows[e.RowIndex].DataBoundItem as AdminUserUserTable;
            if (aU1UserListDgv.Columns.Contains("Role"))
            {
                var roleCbb = (DataGridViewComboBoxCell)(aU1UserListDgv.Rows[e.RowIndex].Cells["Role"]);
                if (adminUserTableItem.RoleID == 1)
                {
                    roleCbb.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
                    roleCbb.ReadOnly = true;
                }
                else
                {
                    roleCbb.Value = adminUserTableItem.RoleID;
                }
            }
        }
    }
}