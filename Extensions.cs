﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Controls;

namespace SWS
{
    public static class Extensions
    {
        public static IEnumerable<TControl> GetChildControls<TControl>(this Control control) where TControl : Control
        {
            var children = (control.Controls != null) ? control.Controls.OfType<TControl>() : Enumerable.Empty<TControl>();
            return children.SelectMany(c => GetChildControls<TControl>(c)).Concat(children);
        }
        public static void SetReadOnly(this MetroTextBox txtBox)
        {
            txtBox.UseCustomForeColor = true;
            txtBox.UseCustomBackColor = true;
            txtBox.ReadOnly = true;
            txtBox.ForeColor = Color.SlateGray;
            txtBox.BackColor = Color.WhiteSmoke;
        }

        public static void SetNormal(this MetroTextBox txtBox)
        {
            txtBox.UseCustomForeColor = true;
            txtBox.UseCustomBackColor = true;
            txtBox.ReadOnly = false;
            txtBox.ForeColor = Color.DarkSlateGray;
            txtBox.BackColor = Color.Azure;
        }

        public static void SetDisable(this MetroComboBox cbBox)
        {
            cbBox.UseCustomForeColor = true;
            cbBox.UseCustomBackColor = true;
            cbBox.Enabled = false;
            cbBox.ForeColor = Color.SlateGray;
            cbBox.BackColor = Color.WhiteSmoke;
        }

        public static void SetEnable(this MetroComboBox cbBox)
        {
            cbBox.UseCustomForeColor = true;
            cbBox.UseCustomBackColor = true;
            cbBox.Enabled = true;
            cbBox.ForeColor = Color.DarkSlateGray;
            cbBox.BackColor = Color.Azure;
        }
    }
}
