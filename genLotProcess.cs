﻿using MetroFramework.Controls;
using Spire.Xls;
using SWS.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;

namespace SWS
{
    public partial class Inventory
    {
        public List<B1_Genlot> GetGRDetail(string grNo, string itemcode)
        {
            log.Info(string.Format(@"Start Method: GetGRDetail() Parameter:grNo Value:'{0}' Parameter:itemcode Value:'{1}'", grNo, itemcode));

            try
            {
                string whereStr = "";
                List<B1_Genlot> grs = new List<B1_Genlot>();

                if (grNo != "" && itemcode != "")
                {
                    whereStr = string.Format(@" and T0.[DocNum] = '{0}' and T1.[ItemCode] = '{1}'", grNo, itemcode);
                }

                var result = _b1Entities.Database.SqlQuery<B1_Genlot>(
                    string.Format(@"SELECT T0.[DocNum] AS 'GR_NO', T0.[DocEntry] AS 'GR_DocEntry', T0.[DocStatus] AS 'GR_DocStatus'
                , T0.[CreateDate] AS 'GR_CreateDate', T0.[CardCode] AS 'Customer_Code', T0.[CardName] AS 'Customer_Name'
                , T1.[LineNum] AS 'GR_Item_RowNo', T1.[LineStatus] AS 'GR_Item_RowStatus', T1.[ItemCode] AS 'GR_Item_Code'
                , T1.[Dscription] AS 'GR_Item_Name', T1.[Quantity] AS 'GR_Item_Qty', T1.unitMsr AS 'Uom', T1.[WhsCode] AS 'GR_Item_WH'
                , T4.[DistNumber] AS 'Batch_No', T4.[MnfSerial] AS 'Batch_Att_1', T4.[InDate] AS 'Batch_Adm_Date'
                , T5.[ItmsGrpCod] AS 'Item_Group_Code'
                --, T6.[ItmsGrpNam] AS Item_Group_Name
                --, T7.[BatchNum] AS 'Item_Batch_Name'
                --, T7.[SuppSerial] AS 'Item_Batch_Att_1', T7.[InDate] AS 'Item_Batch_Adm_Date', T7.[WhsCode] AS 'Item_Batch_WH'
                --, T7.[Quantity] AS 'Item_Batch_Qty'
                , T8.[DocNum] AS 'PO_No', T8.[DocEntry] AS 'PO_DocEntry'
                FROM OPDN T0
                    INNER JOIN PDN1 T1 ON T0.[DocEntry] = T1.[DocEntry]
                left join OITL T2 on t1.docentry = T2.[ApplyEntry] and T1.[LineNum] = T2.[ApplyLine] and T2.[ApplyType] = 20
                left JOIN ITL1 T3 ON T2.LogEntry = T3.LogEntry
                left join OBTN T4 on T4.[ItemCode] = T3.[ItemCode] and T3.[MdAbsEntry] = t4.[absentry]
                INNER JOIN OITM T5 ON T1.[ItemCode] = T5.[ItemCode]
                --INNER JOIN OITB T6 ON T5.[ItmsGrpCod] = T6.[ItmsGrpCod]
                --INNER JOIN OIBT T7 ON T5.[ItemCode] = T7.[ItemCode] and T4.[DistNumber] = T7.[BatchNum]
                left join OPOR T8 ON T8.DocEntry = T1.[BaseEntry]
                --left JOIN POR1 T9 ON T9.[TrgetEntry] = T0.[DocEntry]
                WHERE T0.[DocStatus] = 'O' and T1.[LineStatus] = 'O'
                and T1.WhsCode = 'SQC' and T5.[ItmsGrpCod] = 101
                --and T7.[WhsCode] = 'SQC'
                --and isnull(T7.Quantity, 0) > 0
                {0}
                ", whereStr)).ToList();

                grs = result.ToList();
                return grs;
            }
            catch (Exception ex)
            {
                log.Error("Method: GetGRDetail() Error: " + ex.Message); MessageBox.Show("Error: " + ex.Message);
                return null;
            }
            finally
            {
                log.Info("End Method: GetGRDetail()");
            }
        }

        public void GL1Pnl_KeyPress(Object sender, KeyPressEventArgs e)
        {
            log.Info("Start Method: GL1Pnl_KeyPress() No parameter");

            try
            {
                if (e.KeyChar != (char)Keys.Return)
                {
                    _gl1BarcodeBuffer += e.KeyChar;
                    return;
                }

                HandleGL1SubmitAction();

                _gl1BarcodeBuffer = "";
            }
            catch (Exception ex)
            {
                log.Error("Method: GL1Pnl_KeyPress() Error: " + ex.Message); MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: GL1Pnl_KeyPress()");
            }
        }

        public void InitializeGenLotPanel(B1_Genlot grItemDetail)
        {
            log.Info(string.Format(@"Start Method: InitializeGenLotPanel() Parameter:grItemDetail Value:object..{0}", grItemDetail));
            //log.Warn(Newtonsoft.Json.JsonConvert.SerializeObject(grItemDetail));

            try
            {
                //Init itemCategory
                var itemCategory = _entities.ItemCategory.FirstOrDefault(ic => ic.ItemCode == grItemDetail.GR_Item_Code);
                var itemType = itemCategory.ItemType;

                var docFormat = _entities.DocumentSetup.FirstOrDefault(ds => ds.DocType == "Generate Lot").DocFormat;
                var lotFormat = itemType.LotFormat;

                var todayDocCount = _entities.GenLot.Count(gl => gl.CreationDate.Value.Year == DateTime.Now.Year && gl.CreationDate.Value.Month == DateTime.Now.Month && gl.CreationDate.Value.Day == DateTime.Now.Day);

                //Init instance

                _genLotInstance = _entities.GenLot.FirstOrDefault(gl => gl.RefNo == grItemDetail.GR_NO.ToString() && gl.ItemCategoryID == itemCategory.ID);
                gL2GenerateLotBtn.Enabled = false;

                if (_genLotInstance == null)
                {
                    _genLotInstance = new GenLot()
                    {
                        DocNo = "TRL-" + ReplaceFormatText(docFormat, "", "", "", "", "", "") + $"-{todayDocCount + 1:D3}",
                        AssignDate = grItemDetail.GR_CreateDate,
                        RefNo = grItemDetail.GR_NO.ToString(),
                        Barcode = grItemDetail.Batch_No,
                        CreationDate = DateTime.Now,
                        ItemCategoryID = _entities.ItemCategory.FirstOrDefault(ic => ic.ItemCode == grItemDetail.GR_Item_Code).ID, //sync for check matching between B1 items and this DB when form load
                        LotNo = grItemDetail.Batch_No, //get from B1 in Gr detail
                        TotalGrossWeight = _isKgs ? Math.Round(grItemDetail.GR_Item_Qty, 2) : Math.Round(ConvertLbsToKgs(grItemDetail.GR_Item_Qty), 2), //gross kgs
                        FromWhse = grItemDetail.GR_Item_WH,
                        ToWhse = "SRM"
                    };

                    gL2GenerateLotBtn.Enabled = true;
                }

                gL2DocNoTxb.Text = _genLotInstance.DocNo;
                gL2AssignDateTxb.Text =
                    _genLotInstance.AssignDate.Value.ToString("d MMMM yyyy", CultureInfo.CreateSpecificCulture("th-TH"));
                gL2RefNoTxb.Text = _genLotInstance.RefNo;
                gL2ItemCodeTxb.Text = itemCategory.ItemCode;
                gL2ItemLotNoTxb.Text = _genLotInstance.LotNo;
                gL2LotAttrTxb.Text = grItemDetail.Batch_Att_1;
                gL2QtyCountTxb.Text = _genLotInstance.TotalQty == null ? "" : _genLotInstance.TotalQty.ToString();
                gL2QtyWeightTxb.Text = $"{_genLotInstance.TotalGrossWeight:N2}";
                gL2TareTxb.Text = _genLotInstance.TotalTare == null ? "" : _genLotInstance.TotalTare.ToString();
                gL2FromWhseTxb.Text = _genLotInstance.FromWhse;
                gL2ToWhseTxb.Text = _genLotInstance.ToWhse;
                gL2SubmitBtn.Enabled = false;
                gL2PrintAllChk.Checked = true;
                gL2PrintStartTxb.Text = "";
                gL2PrintToTxb.Text = "";
                gL2PrintStartTxb.Enabled = false;
                gL2PrintToTxb.Enabled = false;

                if (_genLotInstance.GenLotItem.Count > 0)
                {
                    gL2SubmitBtn.Enabled = true;
                    _genLotList = new List<GenLotItemTable>();
                    var count = 1;

                    foreach (var genLotItem in _genLotInstance.GenLotItem)
                        _genLotList.Add(new GenLotItemTable
                        {
                            Row = count++,
                            Attributes = genLotItem.Item.Attributes,
                            LotNumber = genLotItem.Item.LotNo,
                            GrossWeight = (double)genLotItem.Item.Measure,
                            NetWeight = (double)(genLotItem.Item.Measure - genLotItem.Item.OuterPackageWeight),
                            VendorRef = genLotItem.Item.VendorRef,
                            IsPrinted = genLotItem.IsPrinted ?? false,
                            IsSentToB1 = genLotItem.IsSentToB1 ?? false,
                            ID = genLotItem.ItemID
                        });

                    //var qtyCount = _genLotInstance.TotalQty;
                    //var weightTare = _genLotInstance.TotalTare;
                    //var perPiece = weightTare / qtyCount;
                    GLRecalculateTarePerBale();
                    GLRecalculateTotalWeight();

                    //gL2QtyCountTxb.Text = qtyCount.ToString();
                    //gL2TareTxb.Text = weightTare.ToString();
                    //gL2DifferenceTxb.Text = (Math.Round(_genLotList.Sum(gl => gl.GrossWeight), 2) - (double)_genLotInstance.TotalGrossWeight).ToString();
                    //gL2WeightPerTareTxb.Text = $"{perPiece:N2}";

                    gL2QtyCountTxb.SetReadOnly();
                    gL2TareTxb.SetReadOnly();
                    gL2GenLotTableDGV.Columns.Clear();
                    gL2GenLotTableDGV.DataSource = _genLotList;
                    gL2GenLotTableDGV.Columns.Insert(gL2GenLotTableDGV.Columns.Count, new DataGridViewButtonColumn()
                    {
                        Text = "พิมพ์บาร์โค้ด",
                        Name = "print barcode button",
                        HeaderText = "",
                        UseColumnTextForButtonValue = true
                    });
                    gL2GenLotTableDGV.Columns["GrossWeight"].DefaultCellStyle.Format = "N2";
                    gL2GenLotTableDGV.Columns["NetWeight"].DefaultCellStyle.Format = "N2";
                    gL2GenLotTableDGV.Columns["print barcode button"].Width = 100;
                    //gL2GenLotTableDGV.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                    //gL2GenLotTableDGV.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                    //gL2GenLotTableDGV.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                    //gL2GenLotTableDGV.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                    //gL2GenLotTableDGV.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                    gL2GenLotTableDGV.Columns[gL2GenLotTableDGV.Columns.Count - 1].Visible = true;
                    //gL2GenLotTableDGV.ReadOnly = true;

                    //gL2GenLotTableDTG.DataSource = _genLotList;
                    //gL2GenLotTableDTG.AutoSizeColumnsMode = AutoSizeColumnsMode.AllCellsWithLastColumnFill;
                    //gL2GenLotTableDTG.AllowEditing = false;

                    //if (Convert.ToDecimal(gL2DifferenceTxb.Text) > 0)
                    //{
                    //    gL2SubmitBtn.Enabled = true;
                    //}
                    //else
                    //{
                    //    gL2SubmitBtn.Enabled = false;
                    //}

                    gL2PrintFormBtn.Enabled = true;
                    gL2PrintBarcodeBtn.Enabled = true;
                    gL2ToWhseTxb.SetDisable();
                }
                else
                {
                    gL2GenLotTableDGV.Columns.Clear();
                    gL2GenLotTableDGV.DataSource = null;
                    gL2QtyCountTxb.SetNormal();
                    gL2TareTxb.SetNormal();
                    gL2GenLotTableDGV.ReadOnly = false;
                    gL2PrintFormBtn.Enabled = false;
                    gL2PrintBarcodeBtn.Enabled = false;
                    gL2ToWhseTxb.SetEnable();
                    //gL2SubmitBtn.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                log.Error("Method: InitializeGenLotPanel() Error: " + ex.Message); MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                _entities.SaveChanges();
                log.Info("End Method: InitializeGenLotPanel()");
            }
        }

        private void GenLotTabPage_Enter(object sender, EventArgs e)
        {

        }

        private void GL1BarcodeInputBtn_Click(object sender, EventArgs e)
        {
            gL1Pnl.Visible = false;
            genLotTabPage.Controls.Add(gL2Pnl);
            gL2Pnl.Dock = DockStyle.Fill;
            gL2Pnl.Visible = true;
        }

        private void GL1GrNoTxb_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if (e.KeyChar == (char)Keys.Return) HandleGL1SubmitAction();
        }

        private void GL1GrNoTxb_KeyUp(object sender, KeyEventArgs e)
        {
            log.Info("Start Method: GL1GrNoTxb_KeyUp() No parameter");

            //AOM
            if (gL1GrNoTxb.Text.Trim().Length > 7)
            {
                //get item list og this GR to dropdowm box
                var grItemList = grList.Where(i => i.GR_NO.ToString() == gL1GrNoTxb.Text.Trim()).Select(gr => gr.GR_Item_Code).ToList();
                if (grItemList.Count > 0)
                {
                    gL1ItemCodeCbb.DataSource = grItemList;
                    gL1ItemCodeCbb.SelectedIndex = 0;
                    gL1ItemCodeCbb.SetEnable();
                    gL1SubmitBtn.Enabled = true;
                }
                else //retry
                {
                    Cursor.Current = Cursors.WaitCursor;
                    grList = GetGRDetail("", "");
                    grItemList = grList.Where(i => i.GR_NO.ToString() == gL1GrNoTxb.Text.Trim())
                        .Select(gr => gr.GR_Item_Code).ToList();
                    if (grItemList.Count > 0)
                    {
                        gL1ItemCodeCbb.DataSource = grItemList;
                        gL1ItemCodeCbb.SelectedIndex = 0;
                        gL1ItemCodeCbb.SetEnable();
                        gL1SubmitBtn.Enabled = true;
                    }
                    Cursor.Current = Cursors.Default;
                }
            }
            else
            {
                gL1ItemCodeCbb.DataSource = null;
                gL1ItemCodeCbb.SetDisable();
                gL1SubmitBtn.Enabled = false;
            }

            log.Info("End Method: GL1GrNoTxb_KeyUp()");
        }

        private void GL1Pnl_MouseHover(object sender, EventArgs e)
        {
            //            if (gL1GrNoTxb.Focused || gL1ItemCodeCbb.Focused)
            //            {
            //                return;
            //            }

            //gL1Pnl.Focus();
        }

        private void GL1SubmitBtn_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            _gl1BarcodeBuffer = "";
            HandleGL1SubmitAction();
            Cursor.Current = Cursors.Default;
        }

        private void GL2ExitBtn_Click(object sender, EventArgs e)
        {
            if (_genLotInstance != null)
                WriteLog(_genLotInstance?.ID, "GL", "Exit",
                    $"ออกจากหน้า Gen lot ของ Doc no:{_genLotInstance?.DocNo} Gr no:{_genLotInstance?.RefNo} Item code:{GetItemCategoryByItemCategoryID(_genLotInstance.ItemCategoryID.GetValueOrDefault()).ItemCode}");

            gL2Pnl.Visible = false;
            genLotTabPage.Controls.Add(gL1Pnl);
            gL1Pnl.Dock = DockStyle.Fill;
            gL1Pnl.Visible = true;
        }

        private void GL2GenerateLotBtn_Click(object sender, EventArgs e)
        {
            log.Info("Start Method: gL2GenerateLotBtn_Click() No parameter");

            try
            {
                if (string.IsNullOrEmpty(gL2QtyCountTxb.Text) || string.IsNullOrEmpty(gL2QtyWeightTxb.Text))
                {
                    MessageBox.Show("กรุณาใส่จำนวนเบลและน.น. Tare");
                    return;
                }

                _genLotInstance.TotalQty = decimal.Parse(gL2QtyCountTxb.Text);
                _genLotInstance.TotalTare = decimal.Parse(gL2TareTxb.Text);
                _genLotInstance.FromWhse = gL2FromWhseTxb.Text;
                _genLotInstance.ToWhse = gL2ToWhseTxb.Text;

                if (_genLotInstance.ID == 0)
                {
                    _genLotInstance = _entities.GenLot.Add(_genLotInstance);
                }

                if (_genLotInstance.GenLotItem.Count > 0)
                {
                    foreach (var glitem in _genLotInstance.GenLotItem)
                    {
                        _entities.Item.Remove(glitem.Item);
                    }
                    _entities.GenLotItem.RemoveRange(_genLotInstance.GenLotItem);
                }

                _entities.SaveChanges();

                var itemCategory = _entities.ItemCategory.FirstOrDefault(ic => ic.ItemCode == grItemDetail.GR_Item_Code);
                var itemType = itemCategory.ItemType;
                var itemsToAdd = new List<Item>();
                var genLotItemsToAdd = new List<GenLotItem>();
                _genLotList = new List<GenLotItemTable>();

                for (var i = 1; i <= (int)_genLotInstance.TotalQty; i++)
                {
                    _genLotList.Add(new GenLotItemTable
                    {
                        ID = 0,
                        Row = i,
                        Attributes = grItemDetail.Batch_Att_1,
                        LotNumber = _genLotInstance.LotNo + $"-{i:D4}",
                        GrossWeight = (double)(_genLotInstance.TotalGrossWeight / (int)_genLotInstance.TotalQty),
                        NetWeight = (double)((_genLotInstance.TotalGrossWeight - _genLotInstance.TotalTare) /
                                              (int)_genLotInstance.TotalQty),
                        VendorRef = ""
                    });
                }

                foreach (var newItem in _genLotList.Select(genLotItem => new Item
                {
                    Attributes = genLotItem.Attributes ?? "",
                    Barcode = itemCategory.ItemCode + "_" + genLotItem.LotNumber + "_" + (genLotItem.Attributes ?? "").Replace("\r", "").Replace("\n", "") + "_" + $"Gross={genLotItem.GrossWeight:N2}" + $"_Net={genLotItem.NetWeight:N2}" + "_" + DateTime.Today.ToShortDateString(),
                    Count = 1,
                    ItemCategoryID = _genLotInstance.ItemCategoryID,
                    ItemTypeID = itemType.ID,
                    LotNo = genLotItem.LotNumber,
                    Measure = (decimal)genLotItem.GrossWeight,
                    OuterPackageWeight = (decimal)(genLotItem.GrossWeight - genLotItem.NetWeight),
                    VendorRef = genLotItem.VendorRef,
                    Whse = _genLotInstance.ToWhse + InTranWhse,
                    //ReceiveDate = DateTime.Now
                }))
                {
                    itemsToAdd.Add(newItem);
                    genLotItemsToAdd.Add(new GenLotItem
                    {
                        GenLot = _genLotInstance,
                        Item = newItem
                    });
                }

                var addedItemList = _entities.Item.AddRange(itemsToAdd).ToList();
                _entities.GenLotItem.AddRange(genLotItemsToAdd);

                _entities.SaveChanges();

                for (int i = 0; i < addedItemList.Count; i++)
                {
                    _genLotList[i].ID = addedItemList[i].ID;
                }

                _entities.SaveChanges();

                gL2GenLotTableDGV.Columns.Clear();
                gL2GenLotTableDGV.DataSource = _genLotList;
                gL2GenLotTableDGV.Columns.Insert(gL2GenLotTableDGV.Columns.Count, new DataGridViewButtonColumn()
                {
                    Text = "พิมพ์บาร์โค้ด",
                    Name = "print barcode button",
                    HeaderText = "",
                    UseColumnTextForButtonValue = true
                });
                gL2GenLotTableDGV.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                gL2GenLotTableDGV.Columns[gL2GenLotTableDGV.Columns.Count - 1].Visible = true;
                gL2GenLotTableDGV.Columns["Row"].ReadOnly = true;
                gL2GenLotTableDGV.Columns["LotNumber"].ReadOnly = true;
                gL2GenLotTableDGV.Columns["NetWeight"].ReadOnly = true;
                gL2GenLotTableDGV.Columns[3].DefaultCellStyle.Format = "N2";
                gL2GenLotTableDGV.Columns[4].DefaultCellStyle.Format = "N2";
                gL2GenLotTableDGV.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                gL2GenLotTableDGV.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                gL2GenLotTableDGV.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                gL2GenLotTableDGV.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;

                GLRecalculateTotalWeight();
                gL2SubmitBtn.Enabled = true;
                gL2PrintFormBtn.Enabled = true;
                gL2PrintBarcodeBtn.Enabled = true;
            }
            catch (Exception ex)
            {
                log.Error("Method: gL2GenerateLotBtn_Click() Error: " + ex.Message); MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                WriteLog(_genLotInstance?.ID, "GL", "Create Sub Lot",
                    $"แตก Lot ด้วย Item code:{GetItemCategoryByItemCategoryID(_genLotInstance.ItemCategoryID.GetValueOrDefault()).ItemCode} หมายเลข Lot:{_genLotInstance.LotNo} จำนวนเบล:{_genLotInstance.TotalQty} น.น. Tare:{_genLotInstance.TotalTare} น.น. Gross (Kgs):{_genLotInstance.TotalGrossWeight} จาก WH:{_genLotInstance.FromWhse} ไปยัง WH:{_genLotInstance.ToWhse}");
                log.Info("End Method: gL2GenerateLotBtn_Click()");
            }
        }

        private void GL2GenLotTableDGV_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            log.Info("Start Method: gL2GenLotTableDGV_CellValueChanged() No parameter");

            var editItem = gL2GenLotTableDGV.CurrentRow.DataBoundItem as GenLotItemTable;

            var perTareWeight = double.Parse(gL2WeightPerTareTxb.Text);

            editItem.NetWeight = editItem.GrossWeight - perTareWeight;

            var genLotItem = _entities.GenLotItem.FirstOrDefault(gl => gl.ItemID == editItem.ID && gl.GenLotID == _genLotInstance.ID);
            genLotItem.Item.Attributes = editItem.Attributes;
            genLotItem.Item.Measure = Convert.ToDecimal(editItem.GrossWeight.ToString());
            genLotItem.Item.OuterPackageWeight = Convert.ToDecimal(perTareWeight.ToString());
            genLotItem.Item.VendorRef = editItem.VendorRef;
            _entities.SaveChanges();

            //gL2GenLotTableDGV.Sort(gL2GenLotTableDGV.Columns[0], ListSortDirection.Ascending);

            GLRecalculateTotalWeight();

            gL2GenLotTableDGV.Refresh();

            log.Info("End Method: gL2GenLotTableDGV_CellValueChanged()");
        }

        private void GL2PrintBarcodeBtn_Click(object sender, EventArgs e)
        {
            log.Info("Start Method: GL2PrintBarcodeBtn_Click() No parameter");

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                GLHandlePrintBarcode();
                gL2GenLotTableDGV.Refresh();

            }
            catch (Exception ex)
            {
                log.Error("Method: GL2PrintBarcodeBtn_Click() Error: " + ex.Message); MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: GL2PrintBarcodeBtn_Click()");
                Cursor.Current = Cursors.Default;
            }
        }

        private void GLHandlePrintBarcode(int start = 0, int end = 0)
        {
            log.Info("Start Method: GLHandlePrintBarcode() No parameter");
            try
            {
                var printStart = 0;
                var printEnd = 0;

                if (start > 0 && end > 0)
                {
                    printStart = start;
                    printEnd = end;
                }
                else if (gL2PrintAllChk.Checked)
                {
                    printStart = 1;
                    printEnd = _genLotList.Count;
                }
                else
                {
                    if (string.IsNullOrEmpty(gL2PrintStartTxb.Text) || string.IsNullOrEmpty(gL2PrintToTxb.Text))
                    {
                        MessageBox.Show("กรุณาใส่ตัวเลขให้ถูกค้อง");
                        return;
                    }

                    printStart = Convert.ToInt32(gL2PrintStartTxb.Text);
                    printEnd = Convert.ToInt32(gL2PrintToTxb.Text);
                }

                var genLotPrintList = _genLotList.Skip(printStart - 1).Take(printEnd - printStart + 1).ToList();

                var totalRowCount = genLotPrintList.Count;

                var pageCount = totalRowCount;//Math.Ceiling(totalRowCount / 3f);
                                              //if (pageCount == 0)
                                              //{
                                              //    pageCount = 1;
                                              //}

                WriteLog(_genLotInstance?.ID, "GL", "Print Barcode",
                                    $"Print barcode ย่อยของหมายเลข Lot:{_genLotInstance?.LotNo} จาก Row:{printStart} ถึง Row:{printEnd} เป็นจำนวน(ชิ้น):{totalRowCount}");

                //var lastPageRowCount = (int)Math.Round((double)(totalRowCount % 100) / 2, MidpointRounding.AwayFromZero);
                var tempWorkBook = new Workbook();
                tempWorkBook.Version = ExcelVersion.Version2010;
                tempWorkBook.Worksheets.Clear();
                var worksheet = _barcodeWorkbook.Worksheets[0];

                for (int i = 0; i < pageCount; i++)
                {
                    //worksheet.Columns[0].ColumnWidth = 5.14;
                    //worksheet.Columns[1].ColumnWidth = 14.29;
                    //worksheet.Columns[2].ColumnWidth = 5.14;
                    //worksheet.Columns[3].ColumnWidth = 5.14;
                    //worksheet.Columns[4].ColumnWidth = 14.29;
                    //worksheet.Columns[5].ColumnWidth = 5.14;
                    //worksheet.Rows[0].RowHeight = 78.75;
                    //worksheet.Rows[3].RowHeight = 78.75;
                    //worksheet.Rows[6].RowHeight = 78.75;
                    //worksheet.Rows[1].RowHeight = 60;
                    //worksheet.Rows[4].RowHeight = 60;
                    //worksheet.Rows[7].RowHeight = 60;
                    //worksheet.Rows[2].RowHeight = 7.5;
                    //worksheet.Rows[5].RowHeight = 7.5;
                    tempWorkBook.Worksheets.AddCopy(worksheet);
                }

                var itemList = _entities.Item.ToList();

                var sheetCount = 0;

                var mediumFont = tempWorkBook.CreateFont();
                mediumFont.KnownColor = ExcelColors.Black;
                mediumFont.IsBold = false;
                mediumFont.IsItalic = false;
                mediumFont.FontName = "Calibri";
                mediumFont.Size = 8;

                var smallFont = tempWorkBook.CreateFont();
                smallFont.KnownColor = ExcelColors.Black;
                smallFont.IsBold = false;
                smallFont.IsItalic = false;
                smallFont.FontName = "Calibri";
                smallFont.Size = 7;

                var logo = new Bitmap(ConfigurationManager.AppSettings["Logo90"]);

                foreach (var genlotitem in genLotPrintList)
                {
                    var item = itemList.FirstOrDefault(i => i.LotNo == genlotitem.LotNumber);
                    //tempWorkBook.Worksheets[0].SetRowHeight(rowCount, 170);
                    var leftPicture = tempWorkBook.Worksheets[sheetCount].Pictures.Add(3, 1, GetQRcodeImage(item.Barcode));
                    var rightPicture = tempWorkBook.Worksheets[sheetCount].Pictures.Add(1, 1, GetQRcodeImage(item.Barcode));
                    var leftLogo = tempWorkBook.Worksheets[sheetCount].Pictures.Add(2, 1, logo);
                    var rightLogo = tempWorkBook.Worksheets[sheetCount].Pictures.Add(4, 1, logo);

                    var attributeText = item.Attributes?.Replace("\r", " ").Replace("\n", " ");
                    if (!string.IsNullOrEmpty(attributeText) && attributeText.Length > 31)
                    {
                        attributeText = attributeText.Substring(0, 31) + "\n" +
                                        attributeText.Substring(31, attributeText.Length - 31);
                    }
                    else
                    {
                        attributeText = "";
                    }
                    var weightText = "_" + $"{genlotitem.NetWeight:N2}";

                    tempWorkBook.Worksheets[sheetCount].Range[1, 2].RichText.Text = item.ItemCategory.LotNo + "\n" + item.LotNo + weightText + "\n" + attributeText;
                    tempWorkBook.Worksheets[sheetCount].Range[3, 2].RichText.Text = item.ItemCategory.LotNo + "\n" + item.LotNo + weightText + "\n" + attributeText;
                    tempWorkBook.Worksheets[sheetCount].Range[1, 2].IsWrapText = true;
                    tempWorkBook.Worksheets[sheetCount].Range[3, 2].IsWrapText = true;

                    tempWorkBook.Worksheets[sheetCount].Range[1, 2].RichText.SetFont(0, item.ItemCategory.LotNo.Length + item.LotNo.Length + weightText.Length + 1, mediumFont);
                    tempWorkBook.Worksheets[sheetCount].Range[1, 2].RichText.SetFont(item.ItemCategory.LotNo.Length + item.LotNo.Length + weightText.Length + 1, item.ItemCategory.LotNo.Length + item.LotNo.Length + weightText.Length + attributeText.Length + 1, smallFont);
                    tempWorkBook.Worksheets[sheetCount].Range[3, 2].RichText.SetFont(0, item.ItemCategory.LotNo.Length + item.LotNo.Length + weightText.Length + 1, mediumFont);
                    tempWorkBook.Worksheets[sheetCount].Range[3, 2].RichText.SetFont(item.ItemCategory.LotNo.Length + item.LotNo.Length + weightText.Length + 1, item.ItemCategory.LotNo.Length + item.LotNo.Length + weightText.Length + attributeText.Length + 1, smallFont);
                    //leftPicture.LeftColumnOffset = ;
                    leftPicture.TopRowOffset = 65;
                    rightPicture.TopRowOffset = 65;

                    leftLogo.Width = 25;
                    leftLogo.Height = 29;
                    rightLogo.Width = 25;
                    rightLogo.Height = 29;
                    leftLogo.TopRowOffset = 2;
                    rightLogo.TopRowOffset = 2;
                    leftLogo.LeftColumnOffset = 50;
                    rightLogo.LeftColumnOffset = 50;
                    //tempWorkBook.Worksheets[0].SetRowHeight(rowCount, 100);
                    //tempWorkBook.Worksheets[0].Pictures.Add(rowCount++, 1, GetBarcodeImage(item.Barcode));

                    sheetCount++;

                    genlotitem.IsPrinted = true;
                    var genlotItem = _entities.GenLotItem.FirstOrDefault(gl => gl.ItemID == item.ID && gl.GenLotID == _genLotInstance.ID);
                    if (genlotItem != null)
                    {
                        genlotItem.IsPrinted = true;
                    }
                }

                _entities.SaveChanges();

                //workbook.Save();
                //var saveDialog = new SaveFileDialog
                //{
                //    Filter = @"Excel Worksheets (*.xlsx)|*.xlsx",
                //    FileName = "Export File " + DateTime.Now.ToString("dd-MM-yy-hhmmss"),
                //    DefaultExt = "xlsx"
                //};

                //var saveDialog = new SaveFileDialog
                //{
                //    Filter = @"XPS file (*.xps)|*.xps",
                //    FileName = "XPS File " + DateTime.Now.ToString("dd-MM-yy-hhmmss"),
                //    DefaultExt = "xps"
                //};

                //saveDialog.OpenFile();
                //if (saveDialog.ShowDialog() == DialogResult.OK)
                //{
                //tempWorkBook.SaveToFile(@"C:\Src\tmp.xlsx", ExcelVersion.Version2016);
                //}

                if (_printEnable)
                {
                    PrintExcelByInterop(tempWorkBook, _barcodePrinterName);
                }
                else
                {
                    SaveFileExcel(tempWorkBook, "Generate Lot Barcode");
                }

                //var dialog = new PrintDialog();
                //dialog.AllowPrintToFile = true;
                //dialog.AllowCurrentPage = true;
                //dialog.AllowSomePages = true;
                //dialog.AllowSelection = true;
                //dialog.UseEXDialog = true;
                //dialog.PrinterSettings.Duplex = Duplex.Simplex;
                //dialog.PrinterSettings.FromPage = 0;
                //dialog.PrinterSettings.ToPage = 99999;
                //dialog.PrinterSettings.PrintRange = PrintRange.AllPages;
                //tempWorkBook.PrintDialog = dialog;

                //var pd = tempWorkBook.PrintDocument;
                //if (dialog.ShowDialog() == DialogResult.OK)
                //{
                //    pd.Print();
                //}

            }
            catch (Exception ex)
            {
                log.Error("Method: GLHandlePrintBarcode() Error: " + ex.Message); MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: GLHandlePrintBarcode()");
            }
        }

        private void GL2PrintFormBtn_Click(object sender, EventArgs e)
        {
            log.Info("Start Method: gL2PrintFormBtn_Click() No parameter");

            try
            {
                var totalRowCount = _genLotList.Count;
                var pageCount = (int)((totalRowCount) / 100);
                var lastPageRowCount = (int)Math.Ceiling((double)(totalRowCount % 100) / 2);
                var worksheet = _formWorkbook.Worksheets[0];

                if (lastPageRowCount == 0)
                {
                    pageCount--;
                    lastPageRowCount = 50;
                }

                //worksheet.Range["J2"].Value = _genLotInstance.DocNo;
                worksheet.Range["J3"].Value = _genLotInstance.AssignDate.HasValue ? _genLotInstance.AssignDate.Value.ToString("d MMMM yyyy", CultureInfo.CreateSpecificCulture("th-TH")) : "ไม่มีข้อมูล";
                worksheet.Range["J4"].Value = _genLotInstance.RefNo;
                worksheet.Range["D8"].Value = $"{Convert.ToDecimal(gL2TotalGrossWeightTxb.Text):N2}";
                worksheet.Range["D9"].Value = $"{Convert.ToDecimal(gL2TotalNetWeightTxb.Text):N2}";
                worksheet.Range["E8"].Value = $"{ConvertKgsToLbs(Convert.ToDecimal(gL2TotalGrossWeightTxb.Text)):N2}";
                worksheet.Range["E9"].Value = $"{ConvertKgsToLbs(Convert.ToDecimal(gL2TotalNetWeightTxb.Text)):N2}";
                worksheet.Range["D10"].Value = $"{Convert.ToDecimal(gL2DifferenceTxb.Text):N2}";
                worksheet.Range["I7"].Value = _genLotInstance.FromWhse;
                worksheet.Range["I8"].Value = _genLotInstance.ToWhse;
                worksheet.Range["I9"].Value = gL2LotAttrTxb.Text;
                worksheet.Range["I10"].Value = $"{_genLotInstance.TotalTare:N2}";
                worksheet.Range["K10"].Value = gL2WeightPerTareTxb.Text;
                worksheet.Range["K27"].Value = _genLotInstance.PrintCount == null ? "พิมพ์ครั้งที่ 1" : "พิมพ์ครั้งที่ " + (_genLotInstance.PrintCount.Value);

                worksheet.InsertRow(14, 47, InsertOptionsType.FormatAsBefore);

                var tempWorkBook = new Workbook();
                tempWorkBook.Version = ExcelVersion.Version2010;
                tempWorkBook.Worksheets.Clear();

                for (var k = worksheet.PrstGeomShapes.Count - 1; k >= 0; k--)
                {
                    worksheet.PrstGeomShapes[k].Visible = false;
                }

                for (var i = 0; i < pageCount; i++)
                {
                    var currentWorksheet = tempWorkBook.Worksheets.AddCopy(worksheet);
                    var DocNoBarcode = currentWorksheet.Pictures.Add(1, 10, GetBarcodeImage(_genLotInstance.DocNo));
                    var ItemCodeBarcode = currentWorksheet.Pictures.Add(6, 3, GetBarcodeImage(gL2ItemCodeTxb.Text));
                    var LotNoBarcode = currentWorksheet.Pictures.Add(10, 1, GetBarcodeImage(gL2ItemLotNoTxb.Text));
                }

                worksheet.DeleteRow(14, 47);

                for (var k = worksheet.PrstGeomShapes.Count - 1; k >= 0; k--)
                {
                    worksheet.PrstGeomShapes[k].Visible = true;
                }

                worksheet.InsertRow(14, lastPageRowCount > 2 ? lastPageRowCount - 2 : 0, InsertOptionsType.FormatAsBefore);
                var lastSheet = tempWorkBook.Worksheets.AddCopy(worksheet);
                lastSheet.Pictures.Add(6, 3,
                    GetBarcodeImage(_entities.ItemCategory
                        .FirstOrDefault(ic => ic.ID == _genLotInstance.ItemCategoryID).ItemCode));
                lastSheet.Pictures.Add(10, 1, GetBarcodeImage(_genLotInstance.LotNo));
                lastSheet.Pictures.Add(1, 10, GetBarcodeImage(_genLotInstance.DocNo));
                lastSheet.Range[14 + lastPageRowCount, 10].Text = gL2TotalGrossWeightTxb.Text;
                lastSheet.Range[14 + lastPageRowCount, 11].Text = gL2TotalNetWeightTxb.Text;
                worksheet.DeleteRow(14, lastPageRowCount > 2 ? lastPageRowCount - 2 : 0);

                //worksheet.InsertRow(14, 49 - lastPageRowCount, InsertOptionsType.FormatAsBefore);

                var itemList = _entities.Item.ToList();

                var colOffset = 1;
                var rowOffset = 13;
                var itemCount = 1;
                var sheetCount = 0;
                var maxRowNumber = 62;

                foreach (var item in _genLotList)
                {
                    if (sheetCount == pageCount) // last page
                    {
                        maxRowNumber = lastPageRowCount + 13;
                    }

                    if (rowOffset > maxRowNumber)
                    {
                        if (colOffset == 1)
                        {
                            colOffset = 7;
                            rowOffset = 13;
                        }
                        else
                        {
                            sheetCount++;
                            colOffset = 1;
                            rowOffset = 13;
                        }
                    }
                    tempWorkBook.Worksheets[sheetCount].Range[rowOffset, colOffset].Value = itemCount.ToString();
                    tempWorkBook.Worksheets[sheetCount].Range[rowOffset, colOffset + 1].Value = item.LotNumber;
                    tempWorkBook.Worksheets[sheetCount].Range[rowOffset, colOffset + 2].Value = item.Attributes;
                    tempWorkBook.Worksheets[sheetCount].Range[rowOffset, colOffset + 3].Value = $"{item.GrossWeight:N2}";
                    tempWorkBook.Worksheets[sheetCount].Range[rowOffset, colOffset + 4].Value = $"{item.NetWeight:N2}";
                    tempWorkBook.Worksheets[sheetCount].Range[rowOffset, colOffset + 5].Value =
                        itemList.FirstOrDefault(i => i.LotNo == item.LotNumber).ItemType.UOM_Measure;

                    itemCount++;
                    rowOffset++;
                }

                foreach (var sheet in tempWorkBook.Worksheets)
                {
                    sheet.Columns[11].AutoFitColumns();
                }

                if (_printEnable)
                {
                    PrintExcelByInterop(tempWorkBook, _formPrinterName);
                }
                else
                {
                    SaveFileExcel(tempWorkBook, "Generate Lot Form");
                }

                if (_genLotInstance.PrintCount == null)
                {
                    _genLotInstance.PrintCount = 2;
                }
                else
                {
                    _genLotInstance.PrintCount++;
                }
            }
            catch (Exception ex)
            {
                log.Error("Method: gL2PrintFormBtn_Click() Error: " + ex.Message); MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                WriteLog(_genLotInstance?.ID, "GL", "Print form"
                            , $"พิมพ์แบบฟอร์มของ Gr No:{_genLotInstance?.RefNo} หมายเลข Lot:{_genLotInstance?.LotNo} Doc no:{_genLotInstance?.DocNo}");
                log.Info("End Method: gL2PrintFormBtn_Click()");
            }
        }

        private void GL2QtyCountTxb_KeyPress(object sender, KeyPressEventArgs e)
        {
            EnforceDecimalKeyPress(e);
        }

        private void GL2QtyCountTxb_KeyUp(object sender, KeyEventArgs e)
        {
            GLRecalculateTarePerBale();
        }

        private void GL2RefreshBtn_Click(object sender, EventArgs e)
        {
            log.Info("Start Method: gL2RefreshBtn_Click() No parameter");

            try
            {
                decimal grossQtyLbs;
                decimal grossQtyKgs;
                var grDetail = GetGRDetail(_genLotInstance.RefNo, gL2ItemCodeTxb.Text);
                if (grDetail != null)
                {
                    string oldWeight = gL2QtyWeightTxb.Text;
                    grossQtyLbs = grDetail.FirstOrDefault().GR_Item_Qty;
                    grossQtyKgs = _isKgs ? grDetail.FirstOrDefault().GR_Item_Qty : ConvertLbsToKgs(grDetail.FirstOrDefault().GR_Item_Qty);
                    _genLotInstance.TotalGrossWeight = grossQtyKgs; //update _genLotInstance
                    grItemDetail.GR_Item_Qty = grossQtyKgs; //update grItemDetail

                    //update grList of this gr
                    var obj = grList.FirstOrDefault(x => x.GR_NO.ToString() == _genLotInstance.RefNo && x.GR_Item_Code == gL2ItemCodeTxb.Text);
                    if (obj != null) obj.GR_Item_Qty = grossQtyLbs;

                    gL2QtyWeightTxb.Text = $"{_genLotInstance.TotalGrossWeight:N2}";

                    GLRecalculateTotalWeight();

                    WriteLog(_genLotInstance?.ID, "GL", "Refresh B1 Gwg"
                            , $"โหลดน.น.สินค้าจาก B1 ของ Gr No:{_genLotInstance?.RefNo} หมายเลข Lot:{_genLotInstance?.LotNo} จากเดิม={oldWeight} Kgs. เปลี่ยนเป็น={_genLotInstance?.TotalGrossWeight} Kgs.");

                    MessageBox.Show("อัพเดตน.น.สินค้าจากระบบ B1 เรียบร้อยแล้ว");
                }
                else
                {
                    WriteLog(_genLotInstance?.ID, "GL", "Refresh B1 Gwg"
                            , $"โหลดน.น.สินค้าจาก B1 ของ Gr No:{_genLotInstance?.RefNo} หมายเลข Lot:{_genLotInstance?.LotNo} ไม่สำเร็จ");

                    MessageBox.Show("ไม่สามารถดึงข้อมูลน.น.สินค้าจากระบบ B1 ได้ กรุณาลองใหม่อีกครั้ง");
                }
            }
            catch (Exception ex)
            {
                log.Error("Method: gL2RefreshBtn_Click() Error: " + ex.Message); MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: gL2RefreshBtn_Click()");
            }
        }

        private void GL2SubmitBtn_Click(object sender, EventArgs e)
        {
            log.Info("Start Method: gL2SubmitBtn_Click() No parameter");
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                //Check data before interface to B1
                if (Convert.ToDecimal(gL2TotalGrossWeightTxb.Text) <= Convert.ToDecimal(gL2QtyWeightTxb.Text))
                {
                    //Case table qty <= B1 qty -- interface to B1
                    var genLotItemsToSend = _genLotList.Where(gl => gl.IsPrinted && !gl.IsSentToB1).ToList();

                    if (genLotItemsToSend.Count == 0)
                    {
                        MessageBox.Show("ไม่มีรายการที่เปลี่ยนแปลง ยกเลิกการบันทึก");
                        gL2SubmitBtn.Enabled = true;
                        return;
                    }

                    WriteLog(_genLotInstance?.ID, "GL", "Save"
                            , $"บันทึกข้อมูลการแตก Lot ลงระบบด้วยกรณีของ Green Diffs={gL2DifferenceTxb.Text} หมายเลข Lot:{_genLotInstance?.LotNo} จำนวน(Row):{genLotItemsToSend.Count} น.น. Net(Kgs):{genLotItemsToSend.Sum(gl => gl.NetWeight)}");

                    //gL2SubmitBtn.Enabled = false;

                    var itemCategory = GetItemCategoryByItemCategoryID(_genLotInstance.ItemCategoryID.Value);

                    var availableQty = _b1Entities.OIBT
                        .Where(oibt =>
                            oibt.ItemCode == itemCategory.ItemCode
                            && oibt.BatchNum == _genLotInstance.LotNo
                            && oibt.WhsCode == _genLotInstance.FromWhse).Select(oibt => oibt.Quantity).FirstOrDefault();
                    var totalnet = ConvertKgsToLbs(Convert.ToDecimal(genLotItemsToSend.Sum(gl => gl.NetWeight)));

                    if (availableQty < totalnet)
                    {
                        MessageBox.Show("สินค้ามีจำนวนในสต็อคต้นทางไม่เพียงพอ");
                        gL2SubmitBtn.Enabled = true;
                        return;
                    }

                    if (totalnet > 0)
                    {
                        var result = Interface_GenLotExcelTemplate(totalnet);
                        if (result)
                        {
                            //Update IsReceived
                            var genLot = _entities.GenLot.FirstOrDefault(gl => gl.DocNo == _genLotInstance.DocNo);
                            if (genLot == null)
                            {
                                _genLotInstance.TotalQty = decimal.Parse(gL2QtyCountTxb.Text);
                                _genLotInstance.TotalTare = decimal.Parse(gL2TareTxb.Text);
                                _genLotInstance.FromWhse = gL2FromWhseTxb.Text;
                                _genLotInstance.ToWhse = gL2ToWhseTxb.Text;
                                _genLotInstance.IsApproved = Convert.ToDecimal(gL2DifferenceTxb.Text) <= 0;
                                if (_genLotInstance.ID == 0)
                                {
                                    _genLotInstance = _entities.GenLot.Add(_genLotInstance);
                                }
                            }
                            else
                            {
                                _genLotInstance.FromWhse = gL2FromWhseTxb.Text;
                                _genLotInstance.ToWhse = gL2ToWhseTxb.Text;
                                _genLotInstance.IsApproved = Convert.ToDecimal(gL2DifferenceTxb.Text) <= 0;
                            }

                            foreach (var item in genLotItemsToSend)
                            {
                                item.IsSentToB1 = true;
                                var genlotItem = _entities.GenLotItem.FirstOrDefault(gl => gl.ItemID == item.ID && gl.GenLotID == _genLotInstance.ID);
                                if (genlotItem != null)
                                {
                                    genlotItem.IsSentToB1 = true;
                                    genlotItem.Item.Whse = genlotItem.Item.Whse.Replace(InTranWhse, "");
                                    genlotItem.Item.Attributes = item.Attributes;
                                    genlotItem.Item.VendorRef = item.VendorRef;
                                    genlotItem.Item.ReceiveDate = DateTime.Now;
                                }
                            }

                            _entities.SaveChanges();
                            gL2ToWhseTxb.SetDisable();
                            gL2QtyCountTxb.SetReadOnly();
                            gL2TareTxb.SetReadOnly();
                            MessageBox.Show("บันทึกเรียบร้อย");
                        }
                        else
                        {
                            //Enable Save btn
                            gL2SubmitBtn.Enabled = true;
                            foreach (var item in genLotItemsToSend)
                            {
                                item.IsSentToB1 = false;
                                var genlotItem = _entities.GenLotItem.FirstOrDefault(gl => gl.ItemID == item.ID && gl.GenLotID == _genLotInstance.ID);
                                if (genlotItem != null)
                                {
                                    genlotItem.IsSentToB1 = false;
                                }
                            }
                            //RevertAllEntitiesChanges();
                            MessageBox.Show("บันทึกไม่สำเร็จ กรุณาลองใหม่อีกครั้ง");
                        }
                    }
                }
                else
                {
                    //Case table qty > B1 qty -- Show error
                    WriteLog(_genLotInstance?.ID, "GL", "Save"
                           , $"บันทึกข้อมูลการแตก Lot ลงระบบด้วยกรณีของ Red Diffs={gL2DifferenceTxb.Text} หมายเลข Lot:{_genLotInstance?.LotNo} ของ Gr No:{_genLotInstance?.RefNo}");

                    MessageBox.Show("กรุณาติดต่อแผนกบัญชีเพื่ออัพเดตน.น.สินค้าเพิ่มเติมในระบบ B1 และกลับมาบันทึกข้อมูลเข้าระบบนี้ใหม่อีกครั้งในภายหลัง");
                    gL2SubmitBtn.Enabled = true;
                }

                gL2GenLotTableDGV.Columns[gL2GenLotTableDGV.Columns.Count - 1].Visible = true;
                gL2QtyCountTxb.SetReadOnly();
                gL2TareTxb.SetReadOnly();
                //gL2GenLotTableDGV.ReadOnly = true;
            }
            catch (Exception ex)
            {
                log.Error("Method: gL2SubmitBtn_Click() Error: " + ex.Message); MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: gL2SubmitBtn_Click()");
                Cursor.Current = Cursors.Default;
            }
        }

        private void GL2TareTxb_KeyPress(object sender, KeyPressEventArgs e)
        {
            EnforceDecimalKeyPress(e);
        }

        private void GL2TareTxb_KeyUp(object sender, KeyEventArgs e)
        {
            GLRecalculateTarePerBale();
        }

        private void GLShowPanel(ref MetroPanel panel)
        {
            gL1Pnl.Hide();
            gL2Pnl.Hide();

            panel.Dock = DockStyle.Fill;
            panel.Show();
        }

        private void HandleGL1SubmitAction()
        {
            log.Info("Start Method: HandleGL1SubmitAction() No parameter");

            try
            {
                var GlNo = gL1GrNoTxb.Text.Trim();

                if (GlNo == "" && string.IsNullOrEmpty(_gl1BarcodeBuffer))
                {
                    MessageBox.Show("กรุณากรอกหมายเลข GR");
                    return;
                }

                if (!string.IsNullOrEmpty(_gl1BarcodeBuffer))
                {
                    var docNo = _gl1BarcodeBuffer.Trim();
                    var genlotList = _entities.GenLot.Where(gl => gl.DocNo == docNo || gl.RefNo == docNo).ToList();
                    if (genlotList.Count > 1)
                    {
                        gL1GrNoTxb.Text = docNo;
                        gL1ItemCodeCbb.DataSource = grList.Where(gr => gr.GR_NO.ToString() == docNo)
                            .Select(gr => gr.GR_Item_Code).ToList();
                        gL1ItemCodeCbb.SetEnable();
                        gL1SubmitBtn.Enabled = true;
                        MessageBox.Show("พบ item code มากกว่า 1, กรุณาเลือก item code แล้วกดยืนยัน");
                        return;
                    }
                    _genLotInstance = _entities.GenLot.FirstOrDefault(gl => gl.DocNo == docNo || gl.RefNo == docNo);
                    if (_genLotInstance == null)
                    {
                        if (int.TryParse(docNo, out var docNoInt))
                        {
                            var matchedGrList = grList.Where(gr => gr.GR_NO == docNoInt).ToList();
                            var grItemList = matchedGrList.Select(gr => gr.GR_Item_Code).ToList();

                            if (matchedGrList.Count >= 1)
                            {
                                gL1GrNoTxb.Text = "" + docNoInt;
                                gL1ItemCodeCbb.DataSource = grItemList;
                                gL1ItemCodeCbb.SelectedIndex = 0;
                                if (grItemList.Count > 1)
                                {
                                    gL1ItemCodeCbb.SetEnable();
                                    gL1SubmitBtn.Enabled = true;
                                    MessageBox.Show("พบ item code มากกว่า 1, กรุณาเลือก item code แล้วกดยืนยัน");
                                    return;
                                }
                                
                                //gL1SubmitBtn.PerformClick();
                                //GLShowPanel(ref gL2Pnl);
                            }
                            else //retry
                            {
                                grList = GetGRDetail("", "");
                                matchedGrList = grList.Where(i => i.GR_NO == docNoInt).ToList();
                                if (matchedGrList.Count >= 1)
                                {
                                    gL1GrNoTxb.Text = "" + docNoInt;
                                    gL1ItemCodeCbb.DataSource = grItemList;
                                    gL1ItemCodeCbb.SelectedIndex = 0;
                                    if (grItemList.Count > 1)
                                    {
                                        gL1SubmitBtn.Enabled = true;
                                        gL1ItemCodeCbb.SetEnable();
                                        MessageBox.Show("พบ item code มากกว่า 1, กรุณาเลือก item code แล้วกดยืนยัน");
                                        return;
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("ไม่พบหมายเลขเอกสารที่แสกน");
                                    return;
                                }
                            }
                        }
                    }
                    _gl1BarcodeBuffer = "";
                }
                else
                {
                    _genLotInstance = _entities.GenLot.FirstOrDefault(gl => gl.DocNo == GlNo);
                }

                var grNo = string.Empty;
                var grItemCode = string.Empty;

                if (_genLotInstance != null)
                {
                    grNo = _genLotInstance.RefNo;
                    grItemCode = _entities.ItemCategory.FirstOrDefault(ic => ic.ID == _genLotInstance.ItemCategoryID.Value)
                        .ItemCode;
                }
                else
                {
                    grNo = gL1GrNoTxb.Text.Trim();
                    grItemCode = gL1ItemCodeCbb.SelectedItem?.ToString();
                    gL2TotalGrossWeightTxb.Text = "";
                    gL2TotalNetWeightTxb.Text = "";
                    gL2DifferenceTxb.Text = "";
                    gL2WeightPerTareTxb.Text = "";
                }

                grItemDetail = grList.Where(i => i.GR_NO.ToString() == grNo && i.GR_Item_Code == grItemCode).FirstOrDefault();

                if (grItemDetail.Uom.ToUpper() == "KG")
                {
                    _isKgs = true;
                }
                else
                {
                    _isKgs = false;
                }

                if (grItemDetail == null)
                {
                    MessageBox.Show("ไม่พบหมายเลข GR ที่ระบุ");
                    return;
                }

                InitializeGenLotPanel(grItemDetail);

                if (_genLotInstance != null)
                    WriteLog(_genLotInstance.ID, "GL", "Load",
                        $"โหลดหน้า Genlot ด้วยหมายเลข GR {_genLotInstance?.RefNo} รหัสสินค้า {GetItemCategoryByItemCategoryID(_genLotInstance.ItemCategoryID.GetValueOrDefault()).ItemCode}");

                GLShowPanel(ref gL2Pnl);
            }
            catch (Exception ex)
            {
                log.Error("Method: HandleGL1SubmitAction() Error: " + ex.Message); MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: HandleGL1SubmitAction()");
            }
        }

        private bool Interface_GenLotExcelTemplate(decimal totalNetWeight)
        {
            log.Info("Start Method: Interface_GenLotExcelTemplate() No parameter");

            try
            {
                // gen B1 header template
                string docEntry = "1";
                string docDate = DateTime.Today.ToString("yyyyMMdd", CultureInfo.CreateSpecificCulture("en-US"));
                string comments = _genLotInstance.DocNo; //BCDoc 
                string jrnlMemo = "GR No. " + _genLotInstance.RefNo; //GRNo
                string fromWh = _genLotInstance.FromWhse; //grItemDetail.GR_Item_WH;
                string toWh = _genLotInstance.ToWhse; //"SRM";

                var _interfaceWorkbook_Header = new Workbook();
                _interfaceWorkbook_Header.LoadFromFile(ConfigurationManager.AppSettings["GenLot_HeaderCsvTemplate"], ",", 1, 1);
                var worksheetHd = _interfaceWorkbook_Header.Worksheets[0];

                worksheetHd.Range["A3"].Value = docEntry;
                worksheetHd.Range["D3"].Value = docDate;
                worksheetHd.Range["K3"].Value = comments;
                worksheetHd.Range["L3"].Value = jrnlMemo;
                worksheetHd.Range["O3"].Value = fromWh;
                worksheetHd.Range["P3"].Value = toWh;

                var tempWorkBookHd = new Workbook();
                tempWorkBookHd.Version = ExcelVersion.Version2010;
                tempWorkBookHd.Worksheets.Clear();
                tempWorkBookHd.Worksheets.AddCopy(worksheetHd);

                string headerPath = SaveFileCSV(tempWorkBookHd, "Header-OWTR");

                //B1 line
                string parentKey = "1";
                string lineNum = "0";
                string lineItemCode = _entities.ItemCategory.FirstOrDefault(ic => ic.ID == _genLotInstance.ItemCategoryID.Value).ItemCode; //grItemDetail.GR_Item_Code;
                string lineQuantity = totalNetWeight.ToString().Replace(",", ""); //(ConvertKgsToLbs(Convert.ToDecimal(gL2TotalNetWeightTxb.Text))).ToString();
                string unitMsr = "Lbs";

                var _interfaceWorkbook_Line = new Workbook();
                _interfaceWorkbook_Line.LoadFromFile(ConfigurationManager.AppSettings["GenLot_LineCsvTemplate"], ",", 1, 1);
                var worksheetLn = _interfaceWorkbook_Line.Worksheets[0];

                worksheetLn.Range["A3"].Value = parentKey;
                worksheetLn.Range["B3"].Value = lineNum;
                worksheetLn.Range["C3"].Value = lineItemCode;
                worksheetLn.Range["E3"].Value = lineQuantity;
                //worksheetLn.Range["W3"].Text = unitMsr;

                var tempWorkBookLn = new Workbook();
                tempWorkBookLn.Version = ExcelVersion.Version2010;
                tempWorkBookLn.Worksheets.Clear();
                tempWorkBookLn.Worksheets.AddCopy(worksheetLn);

                string linePath = SaveFileCSV(tempWorkBookLn, "Line-WTR1");

                //B1 Batch
                string batchDocNum = "1";
                string batchLineNum = "0";
                string distNumber = _genLotInstance.LotNo;
                string batchQuantity = lineQuantity;
                string baseLineNumber = "0";

                var _interfaceWorkbook_Batch = new Workbook();
                _interfaceWorkbook_Batch.LoadFromFile(ConfigurationManager.AppSettings["GenLot_BatchCsvTemplate"], ",", 1, 1);
                var worksheetBt = _interfaceWorkbook_Batch.Worksheets[0];

                worksheetBt.Range["A3"].Value = batchDocNum;
                worksheetBt.Range["B3"].Value = batchLineNum;
                worksheetBt.Range["C3"].Value = distNumber;
                worksheetBt.Range["K3"].Value = batchQuantity;
                worksheetBt.Range["L3"].Value = baseLineNumber;

                var tempWorkBookBt = new Workbook();
                tempWorkBookBt.Version = ExcelVersion.Version2010;
                tempWorkBookBt.Worksheets.Clear();
                tempWorkBookBt.Worksheets.AddCopy(worksheetBt);

                string batchPath = SaveFileCSV(tempWorkBookBt, "Batch-OBTN");

                ////Copy XML to new one and also change template paths in XML
                //bool isTransfer = true;
                //string templatePath = ConfigurationManager.AppSettings["GenLot_XmlTemplate"];
                //string xmlOutputPath = ConfigurationManager.AppSettings["InterfaceOutputPath"] + "GenLotXml_" + DateTime.Now.ToString("dd-MM-yy-hhmmss") + ".xml"; ;
                //CreateXMLInterface(isTransfer, templatePath, headerPath, linePath, batchPath, xmlOutputPath);

                ////Call .bat
                //string batName = "GenLotBat_" + DateTime.Now.ToString("dd-MM-yy-hhmmss");
                //CreateBatFile(batName, xmlOutputPath);

                //Copy XML to new one and also change template paths in XML
                bool isTransfer = true;
                string templatePath = ConfigurationManager.AppSettings["GenLot_XmlTemplate"];
                string xmlFileName = "GenLotXml_" + DateTime.Now.ToString("dd-MM-yy-hhmmss") + ".xml";
                string xmlOutputPath = ConfigurationManager.AppSettings["InterfaceOutputPath"] + xmlFileName;
                string localXmlPath = ConfigurationManager.AppSettings["LocalTemplateOutputPath"] + xmlFileName;
                CreateXMLInterface(isTransfer, templatePath, headerPath, linePath, batchPath, xmlOutputPath);

                //Call .bat
                string batName = "GenLotBat_" + DateTime.Now.ToString("dd-MM-yy-hhmmss");
                CreateBatFile(batName, localXmlPath);

                //Detect error log from DTW
                bool result = GetInterfaceResult(batName);

                return result;
            }
            catch (Exception ex)
            {
                log.Error("Method: Interface_GenLotExcelTemplate() Error: " + ex.Message); MessageBox.Show("Error: " + ex.Message);
                return false;
            }
            finally
            {
                log.Info("End Method: Interface_GenLotExcelTemplate()");
            }
        }

        private void GLRecalculateTarePerBale()
        {
            log.Info("Start Method: GLRecalculateTarePerBale() No parameter");

            try
            {
                if (!string.IsNullOrEmpty(gL2QtyCountTxb.Text) && !string.IsNullOrEmpty(gL2TareTxb.Text))
                {
                    var count = double.Parse(gL2QtyCountTxb.Text);
                    var tare = double.Parse(gL2TareTxb.Text);
                    var perPiece = tare / count;

                    gL2WeightPerTareTxb.Text = $"{perPiece:N2}";
                }
            }
            catch (Exception ex)
            {
                log.Error("Method: GLRecalculateTarePerBale() Error: " + ex.Message); MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: GLRecalculateTarePerBale()");
            }
        }

        private void GLRecalculateTotalWeight()
        {
            log.Info("Start Method: GLRecalculateTotalWeight() No parameter");

            try
            {
                gL2TotalGrossWeightTxb.Text = $"{Math.Round(_genLotList.Sum(gl => gl.GrossWeight), 2).ToString():N2}";
                gL2TotalNetWeightTxb.Text = $"{Math.Round(_genLotList.Sum(gl => gl.NetWeight), 2).ToString():N2}";

                var weightDiff = Math.Round(_genLotList.Sum(gl => gl.GrossWeight) - (double)_genLotInstance.TotalGrossWeight, 2);
                gL2DifferenceTxb.Text = $"{weightDiff:N2}";

                if (weightDiff <= 0)
                {
                    SetTextBoxPassed(ref gL2DifferenceTxb);
                }
                else
                {
                    SetTextBoxFailed(ref gL2DifferenceTxb);
                }
            }
            catch (Exception ex)
            {
                log.Error("Method: GLRecalculateTotalWeight() Error: " + ex.Message); MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: GLRecalculateTotalWeight()");
            }
        }

        private void GL2PrintAllChk_CheckedChanged(object sender, EventArgs e)
        {
            if (gL2PrintAllChk.Checked)
            {
                gL2PrintStartTxb.Text = "";
                gL2PrintToTxb.Text = "";
                gL2PrintStartTxb.Enabled = false;
                gL2PrintToTxb.Enabled = false;
            }
            else
            {
                gL2PrintStartTxb.Enabled = true;
                gL2PrintToTxb.Enabled = true;
            }
        }

        private void GL2PrintStartTxb_KeyPress(object sender, KeyPressEventArgs e)
        {
            EnforceNumericKeyPress(e);
        }

        private void GL2PrintToTxb_KeyPress(object sender, KeyPressEventArgs e)
        {
            EnforceNumericKeyPress(e);
        }

        private void GL2GenLotTableDGV_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            log.Info("Start Method: GL2GenLotTableDGV_CellClick() No parameter");
            try
            {
                var button = gL2GenLotTableDGV.Columns[e.ColumnIndex] as DataGridViewButtonColumn;
                if (button != null && button.Name == "print barcode button")
                {
                    var genlotTableItem = gL2GenLotTableDGV.Rows[e.RowIndex].DataBoundItem as GenLotItemTable;

                    genlotTableItem.IsPrinted = true;

                    if (_genLotInstance.ID > 0)
                    {
                        GLHandlePrintBarcode(genlotTableItem.Row, genlotTableItem.Row);
                        genlotTableItem.IsPrinted = true;
                        var itemID = _genLotList[genlotTableItem.Row - 1].ID;
                        var genlotID = _genLotInstance.ID;

                        var genLotItem = _entities.GenLotItem.FirstOrDefault(gli => gli.GenLotID == genlotID && gli.ItemID == itemID);
                        if (genLotItem != null)
                        {
                            genLotItem.IsPrinted = true;
                            _entities.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Method: GL2GenLotTableDGV_CellClick() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: GL2GenLotTableDGV_CellClick()");
            }
        }

        private void GL2GenLotTableDGV_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            var genLotTableItem = gL2GenLotTableDGV.Rows[e.RowIndex].DataBoundItem as GenLotItemTable;
            if (genLotTableItem == null)
            {
                return;
            }

            if (genLotTableItem.IsPrinted)
            {
                gL2GenLotTableDGV.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightBlue;
                gL2GenLotTableDGV.Columns["Row"].ReadOnly = true;
                gL2GenLotTableDGV.Columns["LotNumber"].ReadOnly = true;
                gL2GenLotTableDGV.Columns["NetWeight"].ReadOnly = true;
            }

            if (genLotTableItem.IsSentToB1)
            {
                gL2GenLotTableDGV.Rows[e.RowIndex].ReadOnly = true;
                gL2GenLotTableDGV.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.MediumBlue;
            }
        }
    }
}