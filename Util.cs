﻿using MetroFramework.Controls;
using Spire.Xls;
using SWS.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using MetroFramework;
using MetroFramework.Forms;
using ZXing;
using Font = System.Drawing.Font;
using Workbook = Spire.Xls.Workbook;

namespace SWS
{
    public partial class Inventory
    {
        public void WriteLog(long? processID, string processName, string action, string message)
        {
            try
            {
                var newTransactionRecord = new TransactionLog()
                {
                    Action = action,
                    DateTime = DateTime.Now,
                    Message = message,
                    ProcessID = processID,
                    ProcessName = processName,
                    UserName = _currentUser?.UserName
                };

                _entities.TransactionLog.Add(newTransactionRecord);
                _entities.SaveChanges();
            }
            catch (Exception)
            {
            }
        }

        private static decimal ConvertKgsToLbs(decimal kgs)
        {
            return kgs * (decimal)2.20462;
        }

        private static decimal ConvertLbsToKgs(decimal lbs)
        {
            return lbs / (decimal)2.20462;
        }

        private static Bitmap GetBarcodeImage(string barcode)
        {
            BarcodeWriter writer = new BarcodeWriter();
            writer.Format = BarcodeFormat.CODE_128;
            writer.Options.Margin = 3;
            writer.Options.PureBarcode = true;
            writer.Options.Height = 42;
            writer.Options.Width = 170;

            var bmp = writer.Write(barcode);

            var rectf = new RectangleF(0, bmp.Height - 15, bmp.Width, 20);

            var g = Graphics.FromImage(bmp);

            g.SmoothingMode = SmoothingMode.AntiAlias;
            //g.Clear(Color.White);
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;
            g.PixelOffsetMode = PixelOffsetMode.HighQuality;
            g.FillRectangle(Brushes.White, rectf);
            var sf = new StringFormat();
            sf.LineAlignment = StringAlignment.Center;
            sf.Alignment = StringAlignment.Center;
            //g.DrawImage(bmp, 20, 20);
            g.DrawString(barcode, new Font("Verdana", 8), Brushes.Black, rectf, sf);

            g.Flush();

            return bmp;
        }

        private static Bitmap GetBarcodeImage120W(string barcode)
        {
            BarcodeWriter writer = new BarcodeWriter();
            writer.Format = BarcodeFormat.CODE_128;
            writer.Options.Margin = 3;
            writer.Options.PureBarcode = true;
            writer.Options.Height = 42;
            writer.Options.Width = 120;

            var bmp = writer.Write(barcode);

            var rectf = new RectangleF(0, bmp.Height - 15, bmp.Width, 20);

            var g = Graphics.FromImage(bmp);

            g.SmoothingMode = SmoothingMode.AntiAlias;
            //g.Clear(Color.White);
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;
            g.PixelOffsetMode = PixelOffsetMode.HighQuality;
            g.FillRectangle(Brushes.White, rectf);
            var sf = new StringFormat();
            sf.LineAlignment = StringAlignment.Center;
            sf.Alignment = StringAlignment.Center;
            //g.DrawImage(bmp, 20, 20);
            g.DrawString(barcode, new Font("Verdana", 8), Brushes.Black, rectf, sf);

            g.Flush();

            return bmp;
        }

        private static Bitmap GetQRcodeImage(string barcode)
        {
            BarcodeWriter writer = new BarcodeWriter();
            writer.Format = BarcodeFormat.QR_CODE;
            writer.Options.Margin = 2;
            writer.Options.PureBarcode = true;
            writer.Options.Height = 105;
            writer.Options.Width = 105;

            var bmp = writer.Write(barcode);

            var rectf = new RectangleF(0, 0, bmp.Width, 15);

            using (var entities = new SWSEntities())
            {
                var itemID = entities.Item.FirstOrDefault(i => i.Barcode == barcode)?.ID;

                var g = Graphics.FromImage(bmp);

                g.SmoothingMode = SmoothingMode.AntiAlias;
                //g.Clear(Color.White);
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g.PixelOffsetMode = PixelOffsetMode.HighQuality;
                g.FillRectangle(Brushes.White, rectf);
                var sf = new StringFormat();
                sf.LineAlignment = StringAlignment.Center;
                sf.Alignment = StringAlignment.Center;
                //g.DrawImage(bmp, 20, 20);
                g.DrawString((itemID.HasValue ? itemID.Value.ToString() : ""), new Font("Verdana", 8), Brushes.Black, rectf, sf);

                g.Flush();
            }


            return bmp;
        }

        private static Bitmap GetQRcodeImageForFG(string barcode)
        {
            BarcodeWriter writer = new BarcodeWriter();
            writer.Format = BarcodeFormat.QR_CODE;
            writer.Options.Margin = 4;
            writer.Options.PureBarcode = true;
            writer.Options.Height = 162; //was 178
            writer.Options.Width = 162; //was 178

            var bmp = writer.Write(barcode);

            var rectf = new RectangleF(0, bmp.Height - 20, bmp.Width, 20);

            using (var entities = new SWSEntities())
            {
                var itemID = entities.Item.FirstOrDefault(i => i.Barcode == barcode)?.ID;

                var g = Graphics.FromImage(bmp);

                g.SmoothingMode = SmoothingMode.AntiAlias;
                //g.Clear(Color.White);
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g.PixelOffsetMode = PixelOffsetMode.HighQuality;
                g.FillRectangle(Brushes.White, rectf);
                var sf = new StringFormat();
                sf.LineAlignment = StringAlignment.Center;
                sf.Alignment = StringAlignment.Center;
                //g.DrawImage(bmp, 20, 20);
                g.DrawString((itemID.HasValue ? itemID.Value.ToString() : ""), new Font("Verdana", 8), Brushes.Black, rectf, sf);

                g.Flush();
            }

            return bmp;
        }

        private static void PrintExcelByInterop(Workbook workbook, string printerName)
        {
            var timestamp = DateTime.Now.Ticks.ToString();
            workbook.SaveToFile(Application.StartupPath + @"\Temp\tmp" + timestamp + ".xlsx", ExcelVersion.Version2016);
            var excelApp = new Microsoft.Office.Interop.Excel.Application();

            // Open the Workbook:
            var wb = excelApp.Workbooks.Open(
                Application.StartupPath + @"\Temp\tmp" + timestamp + ".xlsx",
                Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                Type.Missing, Type.Missing, Type.Missing, Type.Missing);

            var printerPortList = new List<string>
            {
                "Ne00",
                "Ne01",
                "Ne02",
                "Ne03",
                "Ne04",
                "Ne05",
                "Ne06",
                "Ne07",
                "Ne08",
                "Ne09",
                "Ne10",
                "Ne11",
                "Ne12",
                "Ne13",
                "Ne14",
                "Ne15",
                "Ne16"
            };

            // Get the current printer
            string defaultPrinter = null;
            defaultPrinter = excelApp.ActivePrinter;

            foreach (var port in printerPortList)
            {
                try
                {
                    excelApp.ActivePrinter = printerName + " on " + port + ":";
                    break;
                }
                catch (Exception)
                {
                }
            }

            if (printerName == ConfigurationManager.AppSettings["FormPrinter"])
            {
                foreach (Microsoft.Office.Interop.Excel.Worksheet ws in wb.Worksheets)
                {
                    var pageSetup = ws.PageSetup;
                    // A4 papersize
                    //pageSetup.PaperSize = XlPaperSize.xlPaperLetter;
                    pageSetup.Zoom = false;
                    pageSetup.FitToPagesWide = 1;
                }
            }

            // Get the first worksheet.
            // (Excel uses base 1 indexing, not base 0.)
            //Microsoft.Office.Interop.Excel.Worksheet ws = (Microsoft.Office.Interop.Excel.Worksheet)wb.Worksheets[1];
            // Setup our sheet

            wb.PrintOutEx(
                Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            // Print out 1 copy to the default printer:
            //ws.PrintOut(
            //    Type.Missing, Type.Missing, Type.Missing, Type.Missing,
            //    Type.Missing, Type.Missing, Type.Missing, Type.Missing);

            //bool dialogResult =
            //    excelApp.Dialogs[Microsoft.Office.Interop.Excel.XlBuiltInDialog.xlDialogPrintPreview].Show(
            //        Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing,
            //        Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing,
            //        Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing,
            //        Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing,
            //        Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing,
            //        Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);

            // Set printer back to what it was
            excelApp.ActivePrinter = defaultPrinter;

            // Cleanup:
            GC.Collect();
            GC.WaitForPendingFinalizers();

            wb.Close(false, Type.Missing, Type.Missing);
            Marshal.FinalReleaseComObject(wb);

            excelApp.Quit();
            Marshal.FinalReleaseComObject(excelApp);
        }

        private static void RunBatchFile(string xmlPath)
        {
            log.Info("Start Method: RunBatchFile()");
            try
            {
                //var processInfo = new ProcessStartInfo("cmd.exe", "/c " + command);
                //cd "C:\Program Files (x86)\SAP\Data Transfer Workbench\" DTW.exe - s C:\TESTE\DTW_Teste.xml

                var processInfo = new ProcessStartInfo();
                processInfo.FileName = ConfigurationManager.AppSettings["DtwExePath"]; //DTW Path file
                //processInfo.Arguments = "-s " + xmlPath; //xmlPath
                processInfo.CreateNoWindow = true;
                processInfo.UseShellExecute = false;
                processInfo.RedirectStandardError = true;
                processInfo.RedirectStandardOutput = true;

                var process = Process.Start(processInfo); //System.Diagnostics.Process.Start("c:\\batchfilename.bat");

                process.OutputDataReceived += (object sender, DataReceivedEventArgs e) =>
                    log.Info("output>>" + e.Data);
                process.BeginOutputReadLine();

                process.ErrorDataReceived += (object sender, DataReceivedEventArgs e) =>
                    log.Error("error>>" + e.Data);
                process.BeginErrorReadLine();

                process.WaitForExit();

                log.Info(string.Format("ExitCode: {0}", process.ExitCode));
                process.Close();
            }
            catch (Exception ex)
            {
                log.Error("Method: RunBatchFile() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: RunBatchFile()");
            }
        }

        private void AddBatNameToDB(string fileName)
        {
            log.Info($"Start Method: AddBatNameToDB() Parameter: fileName = {fileName}");
            try
            {
                _entities.InterfaceResult.Add(new InterfaceResult()
                {
                    FileName = fileName,
                    CreationDate = DateTime.Now
                });

                _entities.SaveChanges();
            }
            catch (Exception ex)
            {
                log.Error("Method: AddBatNameToDB() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: AddBatNameToDB()");
            }
        }

        private string ComputeSha256Hash(string rawData)
        {
            using (SHA256 sha256Hash = SHA256.Create())
            {
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));

                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }

        private void CreateBatFile(string fileName, string xmlPath)
        {
            log.Info($"Start Method: CreateBatFile() Parameter: fileName = {fileName}, xmlPath = {xmlPath}");
            try
            {
                AddBatNameToDB(fileName);

                var batFilePath = Path.Combine(ConfigurationManager.AppSettings["InterfaceOutputPath"], fileName + ".bat");
                var dtwPath = ConfigurationManager.AppSettings["DtwExePath"];

                StreamWriter w = new StreamWriter(batFilePath);
                w.WriteLine("\"" + dtwPath + "\" -s " + xmlPath);
                w.Close();
            }
            catch (Exception ex)
            {
                log.Error("Method: CreateBatFile() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: CreateBatFile()");
            }
        }

        private void CreateXMLInterface(bool isTransfer, string templatePath, string headerPath, string linePath, string batchPath, string outputPath)
        {
            //XDocument doc = XDocument.Load(templatePath);
            //foreach (XElement cell in doc.Descendants("Files"))
            //{
            //    if (isTransfer)
            //    {
            //        cell.Element("StockTransfer").Value = headerPath;
            //        cell.Element("StockTransfer_Lines").Value = linePath;
            //        cell.Element("BatchNumbers").Value = batchPath;
            //    }
            //    else
            //    {
            //        cell.Element("Documents").Value = headerPath;
            //        cell.Element("Document_Lines").Value = linePath;
            //        cell.Element("BatchNumbers").Value = batchPath;
            //    }
            //}

            //doc.Save(outputPath);

            XDocument doc = XDocument.Load(templatePath);
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.OmitXmlDeclaration = true;
            StringWriter sw = new StringWriter();
            using (XmlWriter xw = XmlWriter.Create(outputPath, settings))
            {
                foreach (XElement cell in doc.Descendants("Files"))
                {
                    if (isTransfer)
                    {
                        cell.Element("StockTransfer").Value = headerPath;
                        cell.Element("StockTransfer_Lines").Value = linePath;
                        cell.Element("BatchNumbers").Value = batchPath;
                    }
                    else
                    {
                        cell.Element("Documents").Value = headerPath;
                        cell.Element("Document_Lines").Value = linePath;
                        cell.Element("BatchNumbers").Value = batchPath;
                    }
                }
                doc.Save(xw);
            }
        }

        private void EnforceDecimalKeyPress(KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
            {
                e.Handled = true;
            }

            base.OnKeyPress(e);
        }

        private void EnforceNumericKeyPress(KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }

            base.OnKeyPress(e);
        }

        private ItemCategory FindCategoryByItemCode(string itemCode)
        {
            return _entities.ItemCategory.FirstOrDefault(ic => ic.ItemCode == itemCode);
        }

        private Bitmap GetBarcodeImage(string barcode, string line1, string line2, string line3)
        {
            BarcodeWriter writer = new BarcodeWriter();
            writer.Format = BarcodeFormat.QR_CODE;
            writer.Options.PureBarcode = true;
            writer.Options.Height = 220;
            writer.Options.Width = 180;

            var bmp = writer.Write(barcode);

            RectangleF rectf = new RectangleF(0, bmp.Height - 47, bmp.Width, 20);
            RectangleF rectf2 = new RectangleF(0, bmp.Height - 32, bmp.Width, 20);
            RectangleF rectf3 = new RectangleF(0, bmp.Height - 17, bmp.Width, 20);

            var canvas = new Bitmap(300, 300);

            Graphics g = Graphics.FromImage(canvas);

            g.SmoothingMode = SmoothingMode.AntiAlias;
            //g.Clear(Color.White);
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;
            g.PixelOffsetMode = PixelOffsetMode.HighQuality;
            g.FillRectangle(Brushes.White, rectf);
            g.FillRectangle(Brushes.White, rectf2);
            g.FillRectangle(Brushes.White, rectf3);
            StringFormat sf = new StringFormat();
            sf.LineAlignment = StringAlignment.Center;
            sf.Alignment = StringAlignment.Center;
            g.DrawImage(bmp, 20, 20);
            g.DrawString(line1, new Font("Verdana", 8), Brushes.Black, rectf, sf);
            g.DrawString(line2, new Font("Verdana", 7), Brushes.Black, rectf2, sf);
            g.DrawString(line3, new Font("Verdana", 6), Brushes.Black, rectf3, sf);

            g.Flush();
            return bmp;
        }

        private string GetEngUOMCountFromItemCode(string itemCategoryItemCode)
        {
            var itemCategory = _entities.ItemCategory.FirstOrDefault(ic => ic.ItemCode == itemCategoryItemCode);
            var itemType = itemCategory.ItemType;

            return GetEngUOMCountFromItemType(itemType);
        }

        private string GetEngUOMCountFromItemType(ItemType itemType)
        {
            return itemType.UOM_Count.Trim();
        }

        private string GetEngUOMMeasureFromItemCode(string itemCategoryItemCode)
        {
            var itemCategory = _entities.ItemCategory.FirstOrDefault(ic => ic.ItemCode == itemCategoryItemCode);
            var itemType = itemCategory.ItemType;

            return GetEngUOMMeasureFromItemType(itemType);
        }

        private string GetEngUOMMeasureFromItemType(ItemType itemType)
        {
            return itemType.UOM_Measure.Trim();
        }

        private bool GetInterfaceResult(string fileName)
        {
            log.Info($"Start Method: GetInterfaceResult() Parameter: fileName = {fileName}");

            try
            {
                string transid = "";
                bool res = false;

                var stopwatch = Stopwatch.StartNew();
                bool loops = true;
                var result = _entities.InterfaceResult.Where(i => i.FileName == fileName)?.OrderBy(x => x.CreationDate).FirstOrDefault();

                if (result != null)
                {
                    while (loops)
                    {
                        _entities.Entry(result).State = EntityState.Detached;
                        result = _entities.InterfaceResult.Where(i => i.FileName == fileName)?.OrderBy(x => x.CreationDate).FirstOrDefault();
                        //_entities.Entry(result).ReloadAsync();

                        if (result?.Result != null)
                        {
                            transid = result.TranID ?? "";
                            res = Convert.ToBoolean(result.Result);
                            stopwatch.Stop();
                            loops = false;
                        }
                        else if (stopwatch.ElapsedMilliseconds > 10000) //1Min
                        {
                            stopwatch.Stop();
                            loops = false;
                        }
                        else
                        {
                            Thread.Sleep(100);
                            loops = true;
                        }
                    }
                }

                if (!res) // && transid == ""
                {
                    MessageBox.Show("อัพเดตข้อมูลเข้า B1 ไม่สำเร็จ กรุณาตรวจสอบข้อมูลและทำรายการใหม่อีกครั้ง");
                }

                WriteLog(0, "Interface", "InterfaceResult", $"Track result จาก Interface ของ Batch:{fileName} ได้ผลลัพธ์เป็น {res.ToString()} และ TransID={transid}");
                return res;
            }
            catch (Exception ex)
            {
                log.Error("Method: GetInterfaceResult() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
                return false;
            }
            finally
            {
                log.Info("End Method: GetInterfaceResult()");
            }
        }

        private Item GetItemByID(long itemId)
        {
            return _entities.Item.FirstOrDefault(i => i.ID == itemId);
        }

        private ItemCategory GetItemCategoryByItemCategoryID(long itemCategoryID)
        {
            return _entities.ItemCategory.FirstOrDefault(ic => ic.ID == itemCategoryID);
        }

        private ItemType GetItemTypeByItemCode(string itemItemCode)
        {
            var itemCategory = _entities.ItemCategory.FirstOrDefault(ic => ic.ItemCode == itemItemCode);

            return itemCategory?.ItemType;
        }

        private Bitmap GetLogoImage()
        {
            log.Info("Start Method: GetLogoImage() No parameter");

            try
            {
                float width = 40;
                float height = 34;
                var brush = new SolidBrush(Color.White);

                var image = new Bitmap(ConfigurationManager.AppSettings["LogoBW"]);

                float scale = Math.Min(width / image.Width, height / image.Height);

                var bmp = new Bitmap((int)width, (int)height);
                var graph = Graphics.FromImage(bmp);

                // uncomment for higher quality output
                graph.InterpolationMode = InterpolationMode.High;
                graph.CompositingQuality = CompositingQuality.HighQuality;
                //graph.SmoothingMode = SmoothingMode.AntiAlias;

                var scaleWidth = (int)(image.Width * scale);
                var scaleHeight = (int)(image.Height * scale);

                graph.FillRectangle(brush, new RectangleF(0, 0, width, height));
                graph.DrawImage(image, ((int)width - scaleWidth) / 2, ((int)height - scaleHeight) / 2, scaleWidth, scaleHeight);

                return bmp;
            }
            catch (Exception ex)
            {
                log.Error("Method: GetLogoImage() Error: " + ex.Message); MessageBox.Show("Error: " + ex.Message);
                return null;
            }
            finally
            {
                log.Info("End Method: GetLogoImage()");
            }
        }

        private string GetNameByPackageType(string name)
        {
            switch (name)
            {
                case "":
                    return "ไม่ระบุ";

                case "Bag":
                    return "กระสอบแดง";

                case "Box":
                    return "กล่อง";

                case "Palette":
                    return "พาเลท";

                default:
                    return "";
            }
        }

        private string GetPackageTypeByName(string packageName)
        {
            switch (packageName)
            {
                case "ไม่ระบุ":
                    return "";

                case "กระสอบแดง":
                case "กระสอบเหลือง":
                    return "Bag";

                case "กล่อง":
                    return "Box";

                case "พาเลท":
                    return "Palette";

                default:
                    return "";
            }
        }

        private string GetProductionWarehouseFromJobOrder(string jobOrderNo)
        {
            log.Info("Start Method: GetProductionWarehouseFromJobOrder()");

            try
            {
                string whs = _b1Entities.Database.SqlQuery<string>(
                    string.Format(@"
                        SELECT TOP (1) SUBSTRING(RM_wareHouse,4,1) as PD_Whs
                        FROM (
                       SELECT T0.[DocNum] AS 'JO_DocNum'
                       , T0.[DocEntry] AS 'JO_DocEntry'
                       , T1.[ItemCode] AS 'RM_ItemCode'
                       , T1.[IssueType] AS 'RM_IssueType'
                       , T1.[wareHouse] AS 'RM_wareHouse'
                       , T1.LineNum
                       FROM OWOR T0
                       INNER JOIN WOR1 T1 ON T0.[DocEntry] = T1.[DocEntry]
                       WHERE T0.[DocNum] = '{0}'
                       AND T1.[IssueType] = 'B'
                       AND T1.[ItemType] = 290
                        ) a
                        ORDER BY LineNum ASC
                    ", jobOrderNo)).FirstOrDefault();

                return whs;
            }
            catch (Exception ex)
            {
                log.Error("Method: GetProductionWarehouseFromJobOrder() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
                return "";
            }
            finally
            {
                log.Info("End Method: GetProductionWarehouseFromJobOrder()");
            }
        }

        private decimal GetStandardItemWeightPerUnitByItemCode(string itemCode)
        {
            log.Info("Start Method: GetStandardItemWeightPerUnitByItemCode() No parameter");

            try
            {
                var result = _b1Entities.Database.SqlQuery<B1_ItemStandardWeight>(
                    string.Format(@"
                                SELECT T0.[ItemCode], T0.[IWeight1], T0.[InvntryUom]
                                FROM OITM T0
								WHERE ItemCode = '{0}'", itemCode)).FirstOrDefault();

                return result.IWeight1;
            }
            catch (Exception ex)
            {
                log.Error("Method: GetStandardItemWeightPerUnitByItemCode() Error: " + ex.Message); MessageBox.Show("Error: " + ex.Message);
                return 0;
            }
            finally
            {
                log.Info("End Method: GetStandardItemWeightPerUnitByItemCode()");
            }
        }

        private string GetThaiUOMCountFromItemCode(string itemCategoryItemCode)
        {
            var itemCategory = _entities.ItemCategory.FirstOrDefault(ic => ic.ItemCode == itemCategoryItemCode);
            var itemType = itemCategory.ItemType;

            return GetThaiUOMCountFromItemType(itemType);
        }

        private string GetThaiUOMCountFromItemType(ItemType itemType)
        {
            switch (itemType.UOM_Count.Trim())
            {
                case "Bale":
                    return "เบล";

                case "Bag":
                    return "กระสอบ";

                case "Tank":
                    return "ถัง";

                case "Cone":
                    return "ลูก";

                case "Box":
                    return "กล่อง";

                case "Palette":
                    return "พาเลท";

                case "Fold":
                    return "พับ";

                default:
                    return "หน่วย";
            }
        }

        private string GetThaiUOMMeasureFromItemCode(string itemCategoryItemCode)
        {
            var itemCategory = _entities.ItemCategory.FirstOrDefault(ic => ic.ItemCode == itemCategoryItemCode);
            var itemType = itemCategory.ItemType;

            return GetThaiUOMMeasureFromItemType(itemType);
        }

        private string GetThaiUOMMeasureFromItemType(ItemType itemType)
        {
            switch (itemType.UOM_Measure.Trim())
            {
                case "Kgs":
                    return "กก.";

                case "Lbs":
                    return "ปอนด์";

                case "Yds":
                    return "หลา";

                default:
                    return "หน่วย";
            }
        }

        private bool IsItemFGS(string itemCode)
        {
            var itemCategory = _entities.ItemCategory.FirstOrDefault(ic => ic.ItemCode == itemCode);

            if (itemCategory != null)
            {
                var itemType = itemCategory?.ItemType;

                return itemType?.Name.Trim() == "Finish Goods - SP";
            }

            return false;
        }

        private bool IsItemFGW(string itemCode)
        {
            var itemCategory = _entities.ItemCategory.FirstOrDefault(ic => ic.ItemCode == itemCode);

            if (itemCategory != null)
            {
                var itemType = itemCategory?.ItemType;

                return itemType?.Name.Trim() == "Finish Goods - WE";
            }

            return false;
        }

        private void RemoveItemByID(long itemId)
        {
            var genlotItems = _entities.GenLotItem.Where(gli => gli.ItemID == itemId).ToList();
            _entities.GenLotItem.RemoveRange(genlotItems);
            var transferItems = _entities.TransferItem.Where(tri => tri.ItemID == itemId).ToList();
            _entities.TransferItem.RemoveRange(transferItems);
            var jobReceiveItems = _entities.JobReceiveItem.Where(jri => jri.ItemID == itemId).ToList();
            _entities.JobReceiveItem.RemoveRange(jobReceiveItems);
            var issueProductionItems = _entities.IssueProductionItem.Where(ipi => ipi.ItemID == itemId).ToList();
            _entities.IssueProductionItem.RemoveRange(issueProductionItems);
            var deliveryOrderItems = _entities.DeliveryOrderItem.Where(doi => doi.ItemID == itemId).ToList();
            _entities.DeliveryOrderItem.RemoveRange(deliveryOrderItems);
            _entities.SaveChanges();
            var item = GetItemByID(itemId);
            _entities.Item.Remove(item);
            _entities.SaveChanges();
        }

        private string ReplaceFormatText(string formatString, string factNo, string machineNo, string shift, string measure, string runningNo, string formula, string itemCode)
        {
            log.Info(string.Format(@"Start Method: ReplaceFormatText() Parameter:formatString Value:'{0}'
                                        Parameter:factNo Value:'{1}' Parameter:machineNo Value:'{2}'
                                        Parameter:shift Value:'{3}' Parameter:measure Value:'{4}'
                                        Parameter:runningNo Value:'{5}'"
                , formatString, factNo, machineNo, shift, measure, runningNo));
            try
            {
                itemCode = itemCode.Substring(itemCode.Length - 5, 5);
                return formatString.Replace("[year]", DateTime.Now.ToString("yy", CultureInfo.CreateSpecificCulture("th-TH")))
                    .Replace("[month]", DateTime.Now.ToString("MM", CultureInfo.CreateSpecificCulture("th-TH")))
                    .Replace("[day]", DateTime.Now.ToString("dd", CultureInfo.CreateSpecificCulture("th-TH")))
                    .Replace("[factno]", factNo)
                    .Replace("[machno]", $"{Convert.ToInt32(machineNo):D3}")
                    .Replace("[shift]", shift)
                    .Replace("[weight]", measure)
                    .Replace("[length]", measure)
                    .Replace("[formula]", formula)
                    .Replace("[itemcode]", itemCode);
            }
            catch (Exception ex)
            {
                log.Error("Method: ReplaceFormatText() Error: " + ex.Message); MessageBox.Show("Error: " + ex.Message);
                return "";
            }
            finally
            {
                log.Info("End Method: ReplaceFormatText()");
            }
        }

        private string ReplaceFormatText(string formatString, string factNo, string machineNo, string shift, string measure, string runningNo, string formula)
        {
            log.Info(string.Format(@"Start Method: ReplaceFormatText() Parameter:formatString Value:'{0}'
                                        Parameter:factNo Value:'{1}' Parameter:machineNo Value:'{2}'
                                        Parameter:shift Value:'{3}' Parameter:measure Value:'{4}'
                                        Parameter:runningNo Value:'{5}'"
                , formatString, factNo, machineNo, shift, measure, runningNo));
            try
            {
                return formatString.Replace("[year]", DateTime.Now.ToString("yy", CultureInfo.CreateSpecificCulture("th-TH")))
                    .Replace("[month]", DateTime.Now.ToString("MM", CultureInfo.CreateSpecificCulture("th-TH")))
                    .Replace("[day]", DateTime.Now.ToString("dd", CultureInfo.CreateSpecificCulture("th-TH")))
                    .Replace("[factno]", factNo)
                    .Replace("[machno]", machineNo)
                    .Replace("[shift]", shift)
                    .Replace("[weight]", measure)
                    .Replace("[length]", measure)
                    .Replace("[formula]", formula);
            }
            catch (Exception ex)
            {
                log.Error("Method: ReplaceFormatText() Error: " + ex.Message); MessageBox.Show("Error: " + ex.Message);
                return "";
            }
            finally
            {
                log.Info("End Method: ReplaceFormatText()");
            }
        }

        private string ReplaceFormatTextExample(string formatString)
        {
            log.Info(string.Format(@"Start Method: ReplaceFormatTextExample() Parameter:formatString Value:'{0}'", formatString));

            try
            {
                return formatString.Replace("[year]", DateTime.Now.ToString("yy", CultureInfo.CreateSpecificCulture("th-TH")))
                    .Replace("[month]", DateTime.Now.ToString("MM", CultureInfo.CreateSpecificCulture("th-TH")))
                    .Replace("[day]", DateTime.Now.ToString("dd", CultureInfo.CreateSpecificCulture("th-TH")))
                    .Replace("[factno]", "02")
                    .Replace("[machno]", "04")
                    .Replace("[shift]", "A")
                    .Replace("[weight]", "356.68")
                    .Replace("[length]", "42.78")
                    .Replace("[formula]", "07")
                    .Replace("[itemcode]", "TC137");
            }
            catch (Exception ex)
            {
                log.Error("Method: ReplaceFormatTextExample() Error: " + ex.Message); MessageBox.Show("Error: " + ex.Message);
                return "";
            }
            finally
            {
                log.Info("End Method: ReplaceFormatTextExample()");
            }
        }

        private void RevertAllEntitiesChanges()
        {
            foreach (var dbEntityEntry in _entities.ChangeTracker.Entries().Where(t => t.Entity != null))
            {
                dbEntityEntry.State = EntityState.Detached;
                _currentUser = _entities.User.FirstOrDefault(u => u.UserName == _currentUser.UserName);
            }
        }

        private string SaveFileCSV(Workbook workbook, string fileName)
        {
            string finalName = fileName + " " + DateTime.Now.ToString("dd-MM-yy-hhmmss") + ".csv";
            string pathfile = ConfigurationManager.AppSettings["InterfaceOutputPath"] + finalName;

            workbook.Worksheets[0].SaveToFile(pathfile, ",", Encoding.UTF8);

            string body = File.ReadAllText(pathfile);
            body = body.Replace("\"", "");
            File.WriteAllText(pathfile, body);

            string localPath = ConfigurationManager.AppSettings["LocalTemplateOutputPath"] + finalName;

            return localPath;
        }
        private void SaveFileExcel(Workbook workbook, string processName)
        {
            var saveDialog = new SaveFileDialog
            {
                Filter = @"Excel Worksheets (*.xlsx)|*.xlsx",
                FileName = processName + " " + DateTime.Now.ToString("dd-MM-yy-hhmmss"),
                DefaultExt = "xlsx"
            };
            saveDialog.OpenFile();
            if (saveDialog.ShowDialog() == DialogResult.OK)
            {
                workbook.SaveToFile(saveDialog.FileName, ExcelVersion.Version2016);
            }
        }

        private void SetTextBoxFailed(ref MetroTextBox txtBox)
        {
            txtBox.ForeColor = Color.DarkRed;
            txtBox.BackColor = Color.LightSalmon;
        }

        private void SetTextBoxPassed(ref MetroTextBox txtBox)
        {
            txtBox.ForeColor = Color.Green;
            txtBox.BackColor = Color.LightGreen;
        }

        public static string ShowConfirmDialog()
        {
            var prompt = new MetroForm()
            {
                Width = 600,
                Height = 200,
                //FormBorderStyle = FormBorderStyle.FixedDialog,
                Text = "",
                StartPosition = FormStartPosition.CenterScreen,
                Resizable = false,
                Style = MetroColorStyle.Orange

            };
            var textLabel = new MetroLabel() { Left = 100, Top = 20, Width = 500, Text = "กรุณาใส่หมายเลขอ้างอิง CN/SO", FontSize = MetroLabelSize.Tall};
            var textBox = new MetroTextBox() { Left = 100, Top = 70, Width = 400, Height = 40, FontSize = MetroTextBoxSize.Tall};
            var confirmation = new MetroButton() { Text = "OK", Left = 490, Width = 100, Top = 160, DialogResult = DialogResult.OK, FontSize = MetroButtonSize.Tall};
            confirmation.Click += (sender, e) =>
            {
                if (!string.IsNullOrEmpty(textBox.Text))
                {
                    prompt.Close();
                }
            };
            // prompt.Controls.Add(textBox);
            prompt.Controls.Add(confirmation);
            prompt.Controls.Add(textLabel);
            prompt.Controls.Add(textBox);
            prompt.AcceptButton = confirmation;

            return prompt.ShowDialog() == DialogResult.OK ? textBox.Text : "";

        }
    }
}