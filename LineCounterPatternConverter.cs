﻿using log4net.Core;
using log4net.Layout.Pattern;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWS
{

        public sealed class LineCounterPatternConverter : PatternLayoutConverter
        {
            private static int _counter = 0;

            protected override void Convert(TextWriter writer, LoggingEvent loggingEvent)
            {
                var counter = System.Threading.Interlocked.Increment(ref _counter);
                writer.Write(counter.ToString());
            }
        }

}
