﻿using System.Drawing;
using System.Windows.Forms;
using MetroFramework.Controls;
using MetroFramework.Forms;

namespace SWS
{
    partial class Inventory : MetroForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Inventory));
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.mainTabControl = new MetroFramework.Controls.MetroTabControl();
            this.genLotTabPage = new MetroFramework.Controls.MetroTabPage();
            this.gL1Pnl = new MetroFramework.Controls.MetroPanel();
            this.gL1InputPnl = new MetroFramework.Controls.MetroPanel();
            this.gL1ItemCodeCbb = new MetroFramework.Controls.MetroComboBox();
            this.gL1GrNoLbl = new MetroFramework.Controls.MetroLabel();
            this.gL1AndLbl = new MetroFramework.Controls.MetroLabel();
            this.gL1GrNoTxb = new MetroFramework.Controls.MetroTextBox();
            this.gL1ItemCodeLbl = new MetroFramework.Controls.MetroLabel();
            this.gL1SubmitBtn = new MetroFramework.Controls.MetroButton();
            this.gL1TopLbl = new MetroFramework.Controls.MetroLabel();
            this.gL1BarcodeInputBtn = new MetroFramework.Controls.MetroButton();
            this.gL1OrLbl = new MetroFramework.Controls.MetroLabel();
            this.gL1BarcodePnl = new MetroFramework.Controls.MetroPanel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.gL2Pnl = new MetroFramework.Controls.MetroPanel();
            this.gL2TopLbl = new System.Windows.Forms.Label();
            this.gL2TotalWeightPnl = new MetroFramework.Controls.MetroPanel();
            this.metroPanel8 = new MetroFramework.Controls.MetroPanel();
            this.metroLabel11 = new MetroFramework.Controls.MetroLabel();
            this.gL2Kgs4Lbl = new MetroFramework.Controls.MetroLabel();
            this.gL2TotalNetWeightTxb = new MetroFramework.Controls.MetroTextBox();
            this.metroPanel9 = new MetroFramework.Controls.MetroPanel();
            this.metroLabel12 = new MetroFramework.Controls.MetroLabel();
            this.gL2TotalGrossWeightTxb = new MetroFramework.Controls.MetroTextBox();
            this.gL2Kgs3Lbl = new MetroFramework.Controls.MetroLabel();
            this.gL2TotalWeightLbl = new MetroFramework.Controls.MetroLabel();
            this.gL2DifferenceTxb = new MetroFramework.Controls.MetroTextBox();
            this.gL2QtyWeightTxb = new MetroFramework.Controls.MetroTextBox();
            this.gL2WeightPerTareTxb = new MetroFramework.Controls.MetroTextBox();
            this.gL2ItemCodeTxb = new MetroFramework.Controls.MetroTextBox();
            this.gL2AssignDateTxb = new MetroFramework.Controls.MetroTextBox();
            this.metroPanel4 = new MetroFramework.Controls.MetroPanel();
            this.gL2PrintAllChk = new MetroFramework.Controls.MetroCheckBox();
            this.gL2ToLbl = new MetroFramework.Controls.MetroLabel();
            this.gL2PrintStartTxb = new MetroFramework.Controls.MetroTextBox();
            this.gL2PrintToTxb = new MetroFramework.Controls.MetroTextBox();
            this.gL2FromLbl = new MetroFramework.Controls.MetroLabel();
            this.gL2RefreshBtn = new MetroFramework.Controls.MetroButton();
            this.gL2ExitBtn = new MetroFramework.Controls.MetroButton();
            this.gL2PrintBarcodeBtn = new MetroFramework.Controls.MetroButton();
            this.gL2PrintFormBtn = new MetroFramework.Controls.MetroButton();
            this.gL2SubmitBtn = new MetroFramework.Controls.MetroButton();
            this.gL2GenerateLotBtn = new MetroFramework.Controls.MetroButton();
            this.gL2FromWhseTxb = new MetroFramework.Controls.MetroComboBox();
            this.gL2ToWhseTxb = new MetroFramework.Controls.MetroComboBox();
            this.gL2DocNoLbl = new MetroFramework.Controls.MetroLabel();
            this.gL2WeightPerTareLbl = new MetroFramework.Controls.MetroLabel();
            this.gL2AssignDateLbl = new MetroFramework.Controls.MetroLabel();
            this.gL2RefNoLbl = new MetroFramework.Controls.MetroLabel();
            this.gL2Kgs2Lbl = new MetroFramework.Controls.MetroLabel();
            this.gL2TareTxb = new MetroFramework.Controls.MetroTextBox();
            this.gL2RefNoTxb = new MetroFramework.Controls.MetroTextBox();
            this.gL2TareLbl = new MetroFramework.Controls.MetroLabel();
            this.gL2DocNoTxb = new MetroFramework.Controls.MetroTextBox();
            this.gL2DifferenceLbl = new MetroFramework.Controls.MetroLabel();
            this.gL2ItemCodeLbl = new MetroFramework.Controls.MetroLabel();
            this.gL2QtyCountLbl = new MetroFramework.Controls.MetroLabel();
            this.gL2KgsLbl = new MetroFramework.Controls.MetroLabel();
            this.gL2ItemLotNoLbl = new MetroFramework.Controls.MetroLabel();
            this.gL2ItemLotNoTxb = new MetroFramework.Controls.MetroTextBox();
            this.gL2QtyWeightLbl = new MetroFramework.Controls.MetroLabel();
            this.gL2QtyCountTxb = new MetroFramework.Controls.MetroTextBox();
            this.gL2FromWhseLbl = new MetroFramework.Controls.MetroLabel();
            this.gL2LotAttrTxb = new MetroFramework.Controls.MetroTextBox();
            this.gL2ToWhseLbl = new MetroFramework.Controls.MetroLabel();
            this.gL2LotAttrLbl = new MetroFramework.Controls.MetroLabel();
            this.gL2GenLotTableDGV = new System.Windows.Forms.DataGridView();
            this.issueTabPage = new MetroFramework.Controls.MetroTabPage();
            this.iP3Pnl = new MetroFramework.Controls.MetroPanel();
            this.iP3SendB1Btn = new MetroFramework.Controls.MetroButton();
            this.iP3TopLbl = new System.Windows.Forms.Label();
            this.iP3BarcodePnl = new MetroFramework.Controls.MetroPanel();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.iPMagicBtn = new MetroFramework.Controls.MetroButton();
            this.iP3ScannedItemDgv = new System.Windows.Forms.DataGridView();
            this.iP3DateCbb = new MetroFramework.Controls.MetroDateTime();
            this.iP3DateLbl = new MetroFramework.Controls.MetroLabel();
            this.iP3TotalPnl = new MetroFramework.Controls.MetroPanel();
            this.metroPanel6 = new MetroFramework.Controls.MetroPanel();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.iP3TotalNetWeightLbl = new MetroFramework.Controls.MetroLabel();
            this.iP3TotalNetWeightTxb = new MetroFramework.Controls.MetroTextBox();
            this.iP3TotalGrossWeightPnl = new MetroFramework.Controls.MetroPanel();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.iP3TotalGrossWeightTxb = new MetroFramework.Controls.MetroTextBox();
            this.iP3TotalGrossWeightLbl = new MetroFramework.Controls.MetroLabel();
            this.iP3TotalWeightLbl = new MetroFramework.Controls.MetroLabel();
            this.iP3CancelBtn = new MetroFramework.Controls.MetroButton();
            this.iP3SubmitBtn = new MetroFramework.Controls.MetroButton();
            this.iP2Pnl = new MetroFramework.Controls.MetroPanel();
            this.iP2BcColorLbl = new MetroFramework.Controls.MetroLabel();
            this.iP2BcColorTxb = new MetroFramework.Controls.MetroTextBox();
            this.iP2IssueProductionItemDgv = new System.Windows.Forms.DataGridView();
            this.iP2SORefTxb = new MetroFramework.Controls.MetroTextBox();
            this.iP2SORefLbl = new MetroFramework.Controls.MetroLabel();
            this.iP2JobReceiveBtn = new MetroFramework.Controls.MetroButton();
            this.iP2ScanBarcodeIssueBtn = new MetroFramework.Controls.MetroButton();
            this.iP2OpenQtyUnitLbl = new MetroFramework.Controls.MetroLabel();
            this.iP2RejectedQtyUnitLbl = new MetroFramework.Controls.MetroLabel();
            this.iP2RejectedQtyCountTxb = new MetroFramework.Controls.MetroTextBox();
            this.iP2RejectedQtyCountUnitLbl = new MetroFramework.Controls.MetroLabel();
            this.iP2CompletedQty2Txb = new MetroFramework.Controls.MetroTextBox();
            this.iP2CompletedQtyCountUnitLbl = new MetroFramework.Controls.MetroLabel();
            this.iP2DueDateTxb = new MetroFramework.Controls.MetroTextBox();
            this.iP2CustomerTxb = new MetroFramework.Controls.MetroTextBox();
            this.iP2RejectedQtyTxb = new MetroFramework.Controls.MetroTextBox();
            this.iP2CompletedQtyCountTxb = new MetroFramework.Controls.MetroTextBox();
            this.iP2OpenQtyTxb = new MetroFramework.Controls.MetroTextBox();
            this.iP2ItemCodeTxb = new MetroFramework.Controls.MetroTextBox();
            this.iP2AssignDateTxb = new MetroFramework.Controls.MetroTextBox();
            this.iP2WhseCbb = new MetroFramework.Controls.MetroComboBox();
            this.iP2OpenQtyLbl = new MetroFramework.Controls.MetroLabel();
            this.iP2RefNoLbl = new MetroFramework.Controls.MetroLabel();
            this.iP2CompletedQtyUnitLbl = new MetroFramework.Controls.MetroLabel();
            this.iP2CompletedQtyTxb = new MetroFramework.Controls.MetroTextBox();
            this.iP2RefNoTxb = new MetroFramework.Controls.MetroTextBox();
            this.iP2CompletedQtyLbl = new MetroFramework.Controls.MetroLabel();
            this.iP2JobOrderNoTxb = new MetroFramework.Controls.MetroTextBox();
            this.iP2RejectedQtyLbl = new MetroFramework.Controls.MetroLabel();
            this.iP2ItemCodeLbl = new MetroFramework.Controls.MetroLabel();
            this.iP2PlannedQtyLbl = new MetroFramework.Controls.MetroLabel();
            this.iP2CompletedQty2UnitLbl = new MetroFramework.Controls.MetroLabel();
            this.iP2ItemDescLbl = new MetroFramework.Controls.MetroLabel();
            this.iP2ItemDescTxb = new MetroFramework.Controls.MetroTextBox();
            this.iP2PlannedQtyUnitLbl = new MetroFramework.Controls.MetroLabel();
            this.iP2PlannedQtyTxb = new MetroFramework.Controls.MetroTextBox();
            this.iP2WhseLbl = new MetroFramework.Controls.MetroLabel();
            this.iP2DueDateLbl = new MetroFramework.Controls.MetroLabel();
            this.iP2CustomerLbl = new MetroFramework.Controls.MetroLabel();
            this.iP2BackBtn = new MetroFramework.Controls.MetroButton();
            this.iP2PrintFormBtn = new MetroFramework.Controls.MetroButton();
            this.iP2JobOrderNoLbl = new MetroFramework.Controls.MetroLabel();
            this.iP2AssignDateLbl = new MetroFramework.Controls.MetroLabel();
            this.iP2TopLbl = new System.Windows.Forms.Label();
            this.iP1Pnl = new MetroFramework.Controls.MetroPanel();
            this.iP1BarcodePnl = new MetroFramework.Controls.MetroPanel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroPanel5 = new MetroFramework.Controls.MetroPanel();
            this.iP1JobNoTxb = new MetroFramework.Controls.MetroTextBox();
            this.iP1JobNoLbl = new MetroFramework.Controls.MetroLabel();
            this.iP1SubmitBtn = new MetroFramework.Controls.MetroButton();
            this.iP1TopLbl = new MetroFramework.Controls.MetroLabel();
            this.iP1BarcodeBtn = new MetroFramework.Controls.MetroButton();
            this.iP1OrLbl = new MetroFramework.Controls.MetroLabel();
            this.iP4Pnl = new MetroFramework.Controls.MetroPanel();
            this.iP4TareInfoPnl = new MetroFramework.Controls.MetroPanel();
            this.iP4PlasticChk = new MetroFramework.Controls.MetroCheckBox();
            this.iP4ConeCbb = new MetroFramework.Controls.MetroComboBox();
            this.iP4ConeLbl = new MetroFramework.Controls.MetroLabel();
            this.iP4AmountPerPackageLbl = new MetroFramework.Controls.MetroLabel();
            this.iP4AmountPerPackageTxb = new MetroFramework.Controls.MetroTextBox();
            this.iP4AmountPerPackageUnitLbl = new MetroFramework.Controls.MetroLabel();
            this.iP4InnerPackageWeightPerPieceUnitLbl = new MetroFramework.Controls.MetroLabel();
            this.iP4InnerPackageWeightPerPieceLbl = new MetroFramework.Controls.MetroLabel();
            this.iP4InnerPackageWeightPerPieceTxb = new MetroFramework.Controls.MetroTextBox();
            this.iP4TareWeightPerPieceUnitLbl = new MetroFramework.Controls.MetroLabel();
            this.iP4TareWeightPerPieceLbl = new MetroFramework.Controls.MetroLabel();
            this.iP4TareWeightPerPieceTxb = new MetroFramework.Controls.MetroTextBox();
            this.iP4OuterPackagingLbl = new MetroFramework.Controls.MetroLabel();
            this.iP4PackagingCbb = new MetroFramework.Controls.MetroComboBox();
            this.iP4OuterPackagingWeightTxb = new MetroFramework.Controls.MetroTextBox();
            this.iP4OuterPackagingWeightUnitLbl = new MetroFramework.Controls.MetroLabel();
            this.iP4InnerPackagingLbl = new MetroFramework.Controls.MetroLabel();
            this.iP4InnerPackagingWeightTxb = new MetroFramework.Controls.MetroTextBox();
            this.iP4PackagingLbl = new MetroFramework.Controls.MetroLabel();
            this.iP4InnerPackagingWeightUnitLbl = new MetroFramework.Controls.MetroLabel();
            this.iP4MiscPackagingLbl = new MetroFramework.Controls.MetroLabel();
            this.iP4MiscPackagingWeightTxb = new MetroFramework.Controls.MetroTextBox();
            this.iP4MiscPackagingWeightUnitLbl = new MetroFramework.Controls.MetroLabel();
            this.iP4OuterPackagingCountLbl = new MetroFramework.Controls.MetroLabel();
            this.iP4OuterPackagingCountTxb = new MetroFramework.Controls.MetroTextBox();
            this.iP4OuterPackagingCountUnitLbl = new MetroFramework.Controls.MetroLabel();
            this.iP4MiscPackagingCountUnitLbl = new MetroFramework.Controls.MetroLabel();
            this.iP4InnerPackagingCountLbl = new MetroFramework.Controls.MetroLabel();
            this.iP4MiscPackagingCountTxb = new MetroFramework.Controls.MetroTextBox();
            this.iP4InnerPackagingCountTxb = new MetroFramework.Controls.MetroTextBox();
            this.iP4MiscPackagingCountLbl = new MetroFramework.Controls.MetroLabel();
            this.iP4InnerPackagingCountUnitLbl = new MetroFramework.Controls.MetroLabel();
            this.iP4SpiralChk = new MetroFramework.Controls.MetroCheckBox();
            this.iP4TopLbl = new System.Windows.Forms.Label();
            this.iP4JobNoLbl = new MetroFramework.Controls.MetroLabel();
            this.iP4ItemCodeLbl = new MetroFramework.Controls.MetroLabel();
            this.iP4GenerateLotBtn = new MetroFramework.Controls.MetroButton();
            this.iP4MachineLbl = new MetroFramework.Controls.MetroLabel();
            this.iP4FormulaCbb = new MetroFramework.Controls.MetroComboBox();
            this.iP4ShiftCbb = new MetroFramework.Controls.MetroComboBox();
            this.iP4FormulaLbl = new MetroFramework.Controls.MetroLabel();
            this.iP4ToWhseLbl = new MetroFramework.Controls.MetroLabel();
            this.iP4BinLocationCbb = new MetroFramework.Controls.MetroComboBox();
            this.iP4CompletedMeasureTxb = new MetroFramework.Controls.MetroTextBox();
            this.iP4BinLocationLbl = new MetroFramework.Controls.MetroLabel();
            this.iP4CompletedMeasureUnitLbl = new MetroFramework.Controls.MetroLabel();
            this.iP4ItemCodeTxb = new MetroFramework.Controls.MetroTextBox();
            this.iP4CompletedCountUnitLbl = new MetroFramework.Controls.MetroLabel();
            this.iP4DateLbl = new MetroFramework.Controls.MetroLabel();
            this.iP4LotNoTxb = new MetroFramework.Controls.MetroTextBox();
            this.iP4CompletedQtyLbl = new MetroFramework.Controls.MetroLabel();
            this.iP4CompletedCountQtyTxb = new MetroFramework.Controls.MetroTextBox();
            this.iP4ToWhseCbb = new MetroFramework.Controls.MetroComboBox();
            this.iP4CompletedMeasureLbsTxb = new MetroFramework.Controls.MetroTextBox();
            this.iP4LotNoLbl = new MetroFramework.Controls.MetroLabel();
            this.iP4ShiftLbl = new MetroFramework.Controls.MetroLabel();
            this.iP4JobNoTxb = new MetroFramework.Controls.MetroTextBox();
            this.iP4CompletedMeasureLbsUnitTxb = new MetroFramework.Controls.MetroLabel();
            this.iP4MachineTxb = new MetroFramework.Controls.MetroTextBox();
            this.iP4FactoryLbl = new MetroFramework.Controls.MetroLabel();
            this.iP4CompletedMeasure2Lbl = new MetroFramework.Controls.MetroLabel();
            this.iP4FactoryCbb = new MetroFramework.Controls.MetroComboBox();
            this.iP4DateCbb = new MetroFramework.Controls.MetroDateTime();
            this.iP4CompletedMeasure1Lbl = new MetroFramework.Controls.MetroLabel();
            this.iP4SubmitBtn = new MetroFramework.Controls.MetroButton();
            this.iP4PrintBarcodeBtn = new MetroFramework.Controls.MetroButton();
            this.metroPanel21 = new MetroFramework.Controls.MetroPanel();
            this.iP4PrintAllChk = new MetroFramework.Controls.MetroCheckBox();
            this.iP4PrintToLbl = new MetroFramework.Controls.MetroLabel();
            this.iP4PrintFromTxb = new MetroFramework.Controls.MetroTextBox();
            this.iP4PrintToTxb = new MetroFramework.Controls.MetroTextBox();
            this.iP4PrintFromLbl = new MetroFramework.Controls.MetroLabel();
            this.iP4CancelBtn = new MetroFramework.Controls.MetroButton();
            this.iP4TotalWeightPnl = new MetroFramework.Controls.MetroPanel();
            this.metroPanel7 = new MetroFramework.Controls.MetroPanel();
            this.iP4TotalCountLbl = new MetroFramework.Controls.MetroLabel();
            this.iP4TotalCountTxb = new MetroFramework.Controls.MetroTextBox();
            this.iP4TotalCountUnitLbl = new MetroFramework.Controls.MetroLabel();
            this.metroPanel15 = new MetroFramework.Controls.MetroPanel();
            this.metroLabel14 = new MetroFramework.Controls.MetroLabel();
            this.iP4TotalNetWeightUnitLbl = new MetroFramework.Controls.MetroLabel();
            this.iP4TotalNetWeightTxb = new MetroFramework.Controls.MetroTextBox();
            this.metroPanel19 = new MetroFramework.Controls.MetroPanel();
            this.metroLabel13 = new MetroFramework.Controls.MetroLabel();
            this.iP4TotalGrossWeightTxb = new MetroFramework.Controls.MetroTextBox();
            this.iP4TotalGrossWeightUnitLbl = new MetroFramework.Controls.MetroLabel();
            this.iP4TotalWeightLbl = new MetroFramework.Controls.MetroLabel();
            this.iP4JobReceiveItemDgv = new System.Windows.Forms.DataGridView();
            this.iP4StartOptionsPanel = new MetroFramework.Controls.MetroPanel();
            this.iP4StartOptionsLbl = new MetroFramework.Controls.MetroLabel();
            this.iP4StartOptionsCbb = new MetroFramework.Controls.MetroComboBox();
            this.iP5Pnl = new MetroFramework.Controls.MetroPanel();
            this.iP5issueListCbb = new MetroFramework.Controls.MetroComboBox();
            this.iP5TopLbl = new MetroFramework.Controls.MetroLabel();
            this.delArrTabPage = new MetroFramework.Controls.MetroTabPage();
            this.dO1Pnl = new MetroFramework.Controls.MetroPanel();
            this.dO1MessageLbl = new MetroFramework.Controls.MetroLabel();
            this.dO1TerminateDoBtn = new MetroFramework.Controls.MetroButton();
            this.dO1SendToB1Btn = new MetroFramework.Controls.MetroButton();
            this.dO1TopLbl = new System.Windows.Forms.Label();
            this.dO1BarcodePnl = new MetroFramework.Controls.MetroPanel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.dO1MagicRandomItemBtn = new MetroFramework.Controls.MetroButton();
            this.dO1ItemDgv = new System.Windows.Forms.DataGridView();
            this.dO1TelephoneTxb = new MetroFramework.Controls.MetroTextBox();
            this.dO1TelephoneLbl = new MetroFramework.Controls.MetroLabel();
            this.dO1WeightLbsUnitLbl = new MetroFramework.Controls.MetroLabel();
            this.dO1WeightLbsTxb = new MetroFramework.Controls.MetroTextBox();
            this.dO1EqualLbl = new MetroFramework.Controls.MetroLabel();
            this.dO1NetWeightUnitLbl = new MetroFramework.Controls.MetroLabel();
            this.dO1NetWeightTxb = new MetroFramework.Controls.MetroTextBox();
            this.dO1NetWeightLbl = new MetroFramework.Controls.MetroLabel();
            this.dO1TareLbl = new MetroFramework.Controls.MetroLabel();
            this.dO1TareUnitLbl = new MetroFramework.Controls.MetroLabel();
            this.dO1TareTxb = new MetroFramework.Controls.MetroTextBox();
            this.dO1QtyWeightUnitLbl = new MetroFramework.Controls.MetroLabel();
            this.dO1QtyWeightTxb = new MetroFramework.Controls.MetroTextBox();
            this.dO1QtyWeightLbl = new MetroFramework.Controls.MetroLabel();
            this.dO1QtyCountUnitLbl = new MetroFramework.Controls.MetroLabel();
            this.dO1QtyCountTxb = new MetroFramework.Controls.MetroTextBox();
            this.dO1QtyCountLbl = new MetroFramework.Controls.MetroLabel();
            this.dO1ItemCodeTxb = new MetroFramework.Controls.MetroTextBox();
            this.dO1ItemCodeLbl = new MetroFramework.Controls.MetroLabel();
            this.dO1ItemDescLbl = new MetroFramework.Controls.MetroLabel();
            this.dO1ItemDescTxb = new MetroFramework.Controls.MetroTextBox();
            this.dO1CustomerTxb = new MetroFramework.Controls.MetroTextBox();
            this.dO1AssignDateTxb = new MetroFramework.Controls.MetroTextBox();
            this.dO1CancelBtn = new MetroFramework.Controls.MetroButton();
            this.dO1PrintFormBtn = new MetroFramework.Controls.MetroButton();
            this.dO1SubmitBtn = new MetroFramework.Controls.MetroButton();
            this.dO1DocNoLbl = new MetroFramework.Controls.MetroLabel();
            this.dO1AssignDateLbl = new MetroFramework.Controls.MetroLabel();
            this.dO1RefNoLbl = new MetroFramework.Controls.MetroLabel();
            this.dO1RefNoTxb = new MetroFramework.Controls.MetroTextBox();
            this.dO1DocNoTxb = new MetroFramework.Controls.MetroTextBox();
            this.dO1CustomerLbl = new MetroFramework.Controls.MetroLabel();
            this.dO1AddressLbl = new MetroFramework.Controls.MetroLabel();
            this.dO1AddressTxb = new MetroFramework.Controls.MetroTextBox();
            this.dO1RemarkTxb = new MetroFramework.Controls.MetroTextBox();
            this.dO1RemarkLbl = new MetroFramework.Controls.MetroLabel();
            this.dL1Pnl = new MetroFramework.Controls.MetroPanel();
            this.dL1BarcodePnl = new MetroFramework.Controls.MetroPanel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroPanel2 = new MetroFramework.Controls.MetroPanel();
            this.dL1CreateDelBtn = new MetroFramework.Controls.MetroButton();
            this.dL1DelNoTxb = new MetroFramework.Controls.MetroTextBox();
            this.dL1DelNoLbl = new MetroFramework.Controls.MetroLabel();
            this.dL1Or1Lbl = new MetroFramework.Controls.MetroLabel();
            this.dL1SubmitBtn = new MetroFramework.Controls.MetroButton();
            this.dL1ScanBarcodeBtn = new MetroFramework.Controls.MetroButton();
            this.dL1Or2Lbl = new MetroFramework.Controls.MetroLabel();
            this.dL1TopLbl = new MetroFramework.Controls.MetroLabel();
            this.dL3Pnl = new MetroFramework.Controls.MetroPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.dL3DeliveryDgv = new System.Windows.Forms.DataGridView();
            this.dL3AssignDateTxb = new MetroFramework.Controls.MetroTextBox();
            this.dL3CancelBtn = new MetroFramework.Controls.MetroButton();
            this.dL3SubmitBtn = new MetroFramework.Controls.MetroButton();
            this.dL3DocNoLbl = new MetroFramework.Controls.MetroLabel();
            this.dL3AssignDateLbl = new MetroFramework.Controls.MetroLabel();
            this.dL3RefNoLbl = new MetroFramework.Controls.MetroLabel();
            this.dL3RefNoTxb = new MetroFramework.Controls.MetroTextBox();
            this.dL3DocNoTxb = new MetroFramework.Controls.MetroTextBox();
            this.dL2Pnl = new MetroFramework.Controls.MetroPanel();
            this.dL2TopLbl = new System.Windows.Forms.Label();
            this.dL2ShipDateDpk = new MetroFramework.Controls.MetroDateTime();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.dL2DueDateTxb = new MetroFramework.Controls.MetroTextBox();
            this.dL2DeliveryDgv = new System.Windows.Forms.DataGridView();
            this.dL2SubmitBtn = new MetroFramework.Controls.MetroButton();
            this.dL2ShipDateLbl = new MetroFramework.Controls.MetroLabel();
            this.dL3PrintFormBtn = new MetroFramework.Controls.MetroButton();
            this.metroLabel188 = new MetroFramework.Controls.MetroLabel();
            this.dL2ItemDeliveryMeasureUnitLbl = new MetroFramework.Controls.MetroLabel();
            this.dL2ItemDeliveryCountUnitLbl = new MetroFramework.Controls.MetroLabel();
            this.dL2ItemStockMeasureUnitLbsLbl = new MetroFramework.Controls.MetroLabel();
            this.dL2ItemStockMeasureUnitLbl = new MetroFramework.Controls.MetroLabel();
            this.dL2ItemStockCountUnitLbl = new MetroFramework.Controls.MetroLabel();
            this.dL2ItemCodeCbb = new MetroFramework.Controls.MetroComboBox();
            this.dL2SaleOrderCbb = new MetroFramework.Controls.MetroComboBox();
            this.dL2CustomerCbb = new MetroFramework.Controls.MetroComboBox();
            this.dL2PackageTypeCbb = new MetroFramework.Controls.MetroComboBox();
            this.dL2PackageTypeLbl = new MetroFramework.Controls.MetroLabel();
            this.dL2CancelBtn = new MetroFramework.Controls.MetroButton();
            this.dL2AddItemBtn = new MetroFramework.Controls.MetroButton();
            this.dL2ItemDeliveryMeasureLbsLbl = new MetroFramework.Controls.MetroLabel();
            this.dL2ItemDeliveryMeasureLbl = new MetroFramework.Controls.MetroLabel();
            this.dL2ItemDeliveryCountLbl = new MetroFramework.Controls.MetroLabel();
            this.dL2ItemStockMeasureLbsLbl = new MetroFramework.Controls.MetroLabel();
            this.dL2ItemStockMeasureLbl = new MetroFramework.Controls.MetroLabel();
            this.dL2ItemStockMeasureLbsTxb = new MetroFramework.Controls.MetroTextBox();
            this.dL2ItemDeliveryCountTxb = new MetroFramework.Controls.MetroTextBox();
            this.dL2ItemDeliveryMeasureTxb = new MetroFramework.Controls.MetroTextBox();
            this.dL2ItemDeliveryMeasureLbsTxb = new MetroFramework.Controls.MetroTextBox();
            this.dL2ItemStockMeasureTxb = new MetroFramework.Controls.MetroTextBox();
            this.dL2ItemStockCountTxb = new MetroFramework.Controls.MetroTextBox();
            this.dL2ItemStockCountLbl = new MetroFramework.Controls.MetroLabel();
            this.dL2ItemCodeLbl = new MetroFramework.Controls.MetroLabel();
            this.dL2CustomerLbl = new MetroFramework.Controls.MetroLabel();
            this.dL2SaleOrderLbl = new MetroFramework.Controls.MetroLabel();
            this.dL2CreateDelBtn = new MetroFramework.Controls.MetroButton();
            this.transferTabPage = new MetroFramework.Controls.MetroTabPage();
            this.tR2Pnl = new MetroFramework.Controls.MetroPanel();
            this.tR2TopLbl = new System.Windows.Forms.Label();
            this.tR2BarcodePnl = new MetroFramework.Controls.MetroPanel();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.tR2RefNoCbb = new MetroFramework.Controls.MetroComboBox();
            this.tR2MagicBtn = new MetroFramework.Controls.MetroButton();
            this.tR2ItemDgv = new System.Windows.Forms.DataGridView();
            this.tR2BinLocationCbb = new MetroFramework.Controls.MetroComboBox();
            this.tR2InRdb = new MetroFramework.Controls.MetroRadioButton();
            this.tR2OutRdb = new MetroFramework.Controls.MetroRadioButton();
            this.tR2BinLocationLbl = new MetroFramework.Controls.MetroLabel();
            this.tR2ReceiveBtn = new MetroFramework.Controls.MetroButton();
            this.tR2TotalWeightPnl = new MetroFramework.Controls.MetroPanel();
            this.metroPanel17 = new MetroFramework.Controls.MetroPanel();
            this.metroLabel15 = new MetroFramework.Controls.MetroLabel();
            this.tR2TotalCountTxb = new MetroFramework.Controls.MetroTextBox();
            this.tR2TotalCountUnitLbl = new MetroFramework.Controls.MetroLabel();
            this.metroPanel18 = new MetroFramework.Controls.MetroPanel();
            this.metroLabel17 = new MetroFramework.Controls.MetroLabel();
            this.tR2TotalNetWeightUnitLbl = new MetroFramework.Controls.MetroLabel();
            this.tR2TotalNetWeightTxb = new MetroFramework.Controls.MetroTextBox();
            this.metroPanel20 = new MetroFramework.Controls.MetroPanel();
            this.metroLabel16 = new MetroFramework.Controls.MetroLabel();
            this.tR2TotalGrossWeightTxb = new MetroFramework.Controls.MetroTextBox();
            this.tR2TotalGrossWeightUnitLbl = new MetroFramework.Controls.MetroLabel();
            this.tR2TotalWeightLbl = new MetroFramework.Controls.MetroLabel();
            this.tR2AssignDateTxb = new MetroFramework.Controls.MetroTextBox();
            this.tR2CancelBtn = new MetroFramework.Controls.MetroButton();
            this.tR2PrintFormBtn = new MetroFramework.Controls.MetroButton();
            this.tR2SubmitBtn = new MetroFramework.Controls.MetroButton();
            this.tR2FromWhseCbb = new MetroFramework.Controls.MetroComboBox();
            this.tR2ToWhseCbb = new MetroFramework.Controls.MetroComboBox();
            this.tR2DocNoLbl = new MetroFramework.Controls.MetroLabel();
            this.tR2AssignDateLbl = new MetroFramework.Controls.MetroLabel();
            this.tR2RefNoLbl = new MetroFramework.Controls.MetroLabel();
            this.tR2DocNoTxb = new MetroFramework.Controls.MetroTextBox();
            this.tR2FromWhseLbl = new MetroFramework.Controls.MetroLabel();
            this.tR2RemarkTxb = new MetroFramework.Controls.MetroTextBox();
            this.tR2ToWhseLbl = new MetroFramework.Controls.MetroLabel();
            this.tR2RemarkLbl = new MetroFramework.Controls.MetroLabel();
            this.tR1Pnl = new MetroFramework.Controls.MetroPanel();
            this.tR1BarcodePnl = new MetroFramework.Controls.MetroPanel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroPanel3 = new MetroFramework.Controls.MetroPanel();
            this.tR1CreateTransferBtn = new MetroFramework.Controls.MetroButton();
            this.tR1TransferNoTxb = new MetroFramework.Controls.MetroTextBox();
            this.tR1TransferNoLbl = new MetroFramework.Controls.MetroLabel();
            this.tR1Or1Lbl = new MetroFramework.Controls.MetroLabel();
            this.tR1SubmitBtn = new MetroFramework.Controls.MetroButton();
            this.tR1BarcdeBtn = new MetroFramework.Controls.MetroButton();
            this.tR1Or2Lbl = new MetroFramework.Controls.MetroLabel();
            this.tR1TopLbl = new MetroFramework.Controls.MetroLabel();
            this.reportTabPage = new MetroFramework.Controls.MetroTabPage();
            this.metroPanel14 = new MetroFramework.Controls.MetroPanel();
            this.whReportPnl = new System.Windows.Forms.Panel();
            this.toWhCbb = new MetroFramework.Controls.MetroComboBox();
            this.fromWhCbb = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel19 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel18 = new MetroFramework.Controls.MetroLabel();
            this.label2 = new System.Windows.Forms.Label();
            this.genReportBtn = new MetroFramework.Controls.MetroButton();
            this.endDatePicker = new MetroFramework.Controls.MetroDateTime();
            this.metroLabel92 = new MetroFramework.Controls.MetroLabel();
            this.startDatePicker = new MetroFramework.Controls.MetroDateTime();
            this.metroLabel91 = new MetroFramework.Controls.MetroLabel();
            this.reportNameCbb = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel86 = new MetroFramework.Controls.MetroLabel();
            this.adminUserTabPage = new MetroFramework.Controls.MetroTabPage();
            this.aU1Pnl = new MetroFramework.Controls.MetroPanel();
            this.aU1UserListDgv = new System.Windows.Forms.DataGridView();
            this.aU1NewUserNameTxb = new MetroFramework.Controls.MetroTextBox();
            this.aU1NewUserNameLbl = new MetroFramework.Controls.MetroLabel();
            this.aU1NewPasswordLbl = new MetroFramework.Controls.MetroLabel();
            this.aU1TopLbl = new MetroFramework.Controls.MetroLabel();
            this.aU1NewPasswordTxb = new MetroFramework.Controls.MetroTextBox();
            this.aU1SubmitNewUserBtn = new MetroFramework.Controls.MetroButton();
            this.aU1NewRoleCbb = new MetroFramework.Controls.MetroComboBox();
            this.aU1NewRoleLbl = new MetroFramework.Controls.MetroLabel();
            this.aU1UserListLbl = new MetroFramework.Controls.MetroLabel();
            this.lotSetupTabPage = new MetroFramework.Controls.MetroTabPage();
            this.aLPnl = new MetroFramework.Controls.MetroPanel();
            this.aL1ClearBtn = new MetroFramework.Controls.MetroButton();
            this.aL1TopLbl = new MetroFramework.Controls.MetroLabel();
            this.aL1SubmitBtn = new MetroFramework.Controls.MetroButton();
            this.aL1LotFormatTxb = new MetroFramework.Controls.MetroTextBox();
            this.aL1LotTypeLbl = new MetroFramework.Controls.MetroLabel();
            this.aL1OptionsLsb = new System.Windows.Forms.ListBox();
            this.aL1LotTypeCbb = new MetroFramework.Controls.MetroComboBox();
            this.aL1SelectBtn = new MetroFramework.Controls.MetroButton();
            this.aL1ExampleTxb = new MetroFramework.Controls.MetroTextBox();
            this.aL1OptionsLbl = new MetroFramework.Controls.MetroLabel();
            this.aL1ExampleLbl = new MetroFramework.Controls.MetroLabel();
            this.aL1LotFormatLbl = new MetroFramework.Controls.MetroLabel();
            this.secretTabPage = new MetroFramework.Controls.MetroTabPage();
            this.metroButton13 = new MetroFramework.Controls.MetroButton();
            this.metroButton14 = new MetroFramework.Controls.MetroButton();
            this.metroButton15 = new MetroFramework.Controls.MetroButton();
            this.metroButton16 = new MetroFramework.Controls.MetroButton();
            this.metroButton17 = new MetroFramework.Controls.MetroButton();
            this.metroButton18 = new MetroFramework.Controls.MetroButton();
            this.metroButton19 = new MetroFramework.Controls.MetroButton();
            this.metroButton20 = new MetroFramework.Controls.MetroButton();
            this.metroButton22 = new MetroFramework.Controls.MetroButton();
            this.metroButton24 = new MetroFramework.Controls.MetroButton();
            this.metroButton12 = new MetroFramework.Controls.MetroButton();
            this.metroButton11 = new MetroFramework.Controls.MetroButton();
            this.metroButton10 = new MetroFramework.Controls.MetroButton();
            this.metroButton9 = new MetroFramework.Controls.MetroButton();
            this.metroButton8 = new MetroFramework.Controls.MetroButton();
            this.metroButton7 = new MetroFramework.Controls.MetroButton();
            this.metroButton6 = new MetroFramework.Controls.MetroButton();
            this.metroButton5 = new MetroFramework.Controls.MetroButton();
            this.metroButton4 = new MetroFramework.Controls.MetroButton();
            this.metroButton3 = new MetroFramework.Controls.MetroButton();
            this.metroButton2 = new MetroFramework.Controls.MetroButton();
            this.metroButton1 = new MetroFramework.Controls.MetroButton();
            this.itemSetupTabPage = new MetroFramework.Controls.MetroTabPage();
            this.aIPnl = new MetroFramework.Controls.MetroPanel();
            this.aI1SyncBtn = new MetroFramework.Controls.MetroButton();
            this.aI1SubmitBtn = new MetroFramework.Controls.MetroButton();
            this.aI1TypeLbl = new MetroFramework.Controls.MetroLabel();
            this.aI1CategoryLbl = new MetroFramework.Controls.MetroLabel();
            this.aI1TopLbl = new MetroFramework.Controls.MetroLabel();
            this.aI1TypeDgv = new System.Windows.Forms.DataGridView();
            this.aI1CategoryDgv = new System.Windows.Forms.DataGridView();
            this.docSetupTabPage = new MetroFramework.Controls.MetroTabPage();
            this.aDPnl = new MetroFramework.Controls.MetroPanel();
            this.aD1ClearBtn = new MetroFramework.Controls.MetroButton();
            this.aD1TopLbl = new MetroFramework.Controls.MetroLabel();
            this.aD1SubmitBtn = new MetroFramework.Controls.MetroButton();
            this.aD1DocFormatTxb = new MetroFramework.Controls.MetroTextBox();
            this.aD1DocTypeLbl = new MetroFramework.Controls.MetroLabel();
            this.aD1OptionsLsb = new System.Windows.Forms.ListBox();
            this.aD1DocTypeCbb = new MetroFramework.Controls.MetroComboBox();
            this.aD1SelectBtn = new MetroFramework.Controls.MetroButton();
            this.aD1ExampleTxb = new MetroFramework.Controls.MetroTextBox();
            this.aD1OptionsLbl = new MetroFramework.Controls.MetroLabel();
            this.aD1ExampleLbl = new MetroFramework.Controls.MetroLabel();
            this.aD1DocFormatLbl = new MetroFramework.Controls.MetroLabel();
            this.reprintTabPage = new MetroFramework.Controls.MetroTabPage();
            this.reprintPanel = new MetroFramework.Controls.MetroPanel();
            this.reprintInfoPnl = new MetroFramework.Controls.MetroPanel();
            this.rePrintBcBtn = new MetroFramework.Controls.MetroButton();
            this.metroLabel20 = new MetroFramework.Controls.MetroLabel();
            this.grossWgTxb = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel21 = new MetroFramework.Controls.MetroLabel();
            this.createDateTxb = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel22 = new MetroFramework.Controls.MetroLabel();
            this.lotNoTxb = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel23 = new MetroFramework.Controls.MetroLabel();
            this.itemCodetxb = new MetroFramework.Controls.MetroTextBox();
            this.checkIdBtn = new MetroFramework.Controls.MetroButton();
            this.metroLabel24 = new MetroFramework.Controls.MetroLabel();
            this.itemIdTxb = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel25 = new MetroFramework.Controls.MetroLabel();
            this.lGFormPnl = new MetroFramework.Controls.MetroPanel();
            this.versionLbl = new MetroFramework.Controls.MetroLabel();
            this.lGMagicBtn = new MetroFramework.Controls.MetroButton();
            this.lGSubmitBtn = new MetroFramework.Controls.MetroButton();
            this.lGPasswordTxb = new MetroFramework.Controls.MetroTextBox();
            this.lGTopLbl = new MetroFramework.Controls.MetroLabel();
            this.lGUserNameTxb = new MetroFramework.Controls.MetroTextBox();
            this.lGPasswordLbl = new MetroFramework.Controls.MetroLabel();
            this.lGUserNameLbl = new MetroFramework.Controls.MetroLabel();
            this.lGFirstLogPnl = new MetroFramework.Controls.MetroPanel();
            this.lGFirstLogCurrentUserLbl = new MetroFramework.Controls.MetroLabel();
            this.lGFirstLogPassword2Lbl = new MetroFramework.Controls.MetroLabel();
            this.lGFirstLogSubmitBtn = new MetroFramework.Controls.MetroButton();
            this.lGFirstLogPassword2Txb = new MetroFramework.Controls.MetroTextBox();
            this.lGFirstLogLbl = new MetroFramework.Controls.MetroLabel();
            this.lGFirstLogPassword1Txb = new MetroFramework.Controls.MetroTextBox();
            this.lGFirstLogPassword1Lbl = new MetroFramework.Controls.MetroLabel();
            this.lGFirstLogUsernameLbl = new MetroFramework.Controls.MetroLabel();
            this.logoutLnk = new MetroFramework.Controls.MetroLink();
            this.mainTabControl.SuspendLayout();
            this.genLotTabPage.SuspendLayout();
            this.gL1Pnl.SuspendLayout();
            this.gL1InputPnl.SuspendLayout();
            this.gL1BarcodePnl.SuspendLayout();
            this.gL2Pnl.SuspendLayout();
            this.gL2TotalWeightPnl.SuspendLayout();
            this.metroPanel8.SuspendLayout();
            this.metroPanel9.SuspendLayout();
            this.metroPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gL2GenLotTableDGV)).BeginInit();
            this.issueTabPage.SuspendLayout();
            this.iP3Pnl.SuspendLayout();
            this.iP3BarcodePnl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iP3ScannedItemDgv)).BeginInit();
            this.iP3TotalPnl.SuspendLayout();
            this.metroPanel6.SuspendLayout();
            this.iP3TotalGrossWeightPnl.SuspendLayout();
            this.iP2Pnl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iP2IssueProductionItemDgv)).BeginInit();
            this.iP1Pnl.SuspendLayout();
            this.iP1BarcodePnl.SuspendLayout();
            this.metroPanel5.SuspendLayout();
            this.iP4Pnl.SuspendLayout();
            this.iP4TareInfoPnl.SuspendLayout();
            this.metroPanel21.SuspendLayout();
            this.iP4TotalWeightPnl.SuspendLayout();
            this.metroPanel7.SuspendLayout();
            this.metroPanel15.SuspendLayout();
            this.metroPanel19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iP4JobReceiveItemDgv)).BeginInit();
            this.iP4StartOptionsPanel.SuspendLayout();
            this.iP5Pnl.SuspendLayout();
            this.delArrTabPage.SuspendLayout();
            this.dO1Pnl.SuspendLayout();
            this.dO1BarcodePnl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dO1ItemDgv)).BeginInit();
            this.dL1Pnl.SuspendLayout();
            this.dL1BarcodePnl.SuspendLayout();
            this.metroPanel2.SuspendLayout();
            this.dL3Pnl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dL3DeliveryDgv)).BeginInit();
            this.dL2Pnl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dL2DeliveryDgv)).BeginInit();
            this.transferTabPage.SuspendLayout();
            this.tR2Pnl.SuspendLayout();
            this.tR2BarcodePnl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tR2ItemDgv)).BeginInit();
            this.tR2TotalWeightPnl.SuspendLayout();
            this.metroPanel17.SuspendLayout();
            this.metroPanel18.SuspendLayout();
            this.metroPanel20.SuspendLayout();
            this.tR1Pnl.SuspendLayout();
            this.tR1BarcodePnl.SuspendLayout();
            this.metroPanel3.SuspendLayout();
            this.reportTabPage.SuspendLayout();
            this.metroPanel14.SuspendLayout();
            this.whReportPnl.SuspendLayout();
            this.adminUserTabPage.SuspendLayout();
            this.aU1Pnl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.aU1UserListDgv)).BeginInit();
            this.lotSetupTabPage.SuspendLayout();
            this.aLPnl.SuspendLayout();
            this.secretTabPage.SuspendLayout();
            this.itemSetupTabPage.SuspendLayout();
            this.aIPnl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.aI1TypeDgv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aI1CategoryDgv)).BeginInit();
            this.docSetupTabPage.SuspendLayout();
            this.aDPnl.SuspendLayout();
            this.reprintTabPage.SuspendLayout();
            this.reprintPanel.SuspendLayout();
            this.reprintInfoPnl.SuspendLayout();
            this.lGFormPnl.SuspendLayout();
            this.lGFirstLogPnl.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainTabControl
            // 
            this.mainTabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mainTabControl.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.mainTabControl.Controls.Add(this.genLotTabPage);
            this.mainTabControl.Controls.Add(this.issueTabPage);
            this.mainTabControl.Controls.Add(this.delArrTabPage);
            this.mainTabControl.Controls.Add(this.transferTabPage);
            this.mainTabControl.Controls.Add(this.reportTabPage);
            this.mainTabControl.Controls.Add(this.adminUserTabPage);
            this.mainTabControl.Controls.Add(this.lotSetupTabPage);
            this.mainTabControl.Controls.Add(this.secretTabPage);
            this.mainTabControl.Controls.Add(this.itemSetupTabPage);
            this.mainTabControl.Controls.Add(this.docSetupTabPage);
            this.mainTabControl.Controls.Add(this.reprintTabPage);
            this.mainTabControl.FontSize = MetroFramework.MetroTabControlSize.Tall;
            this.mainTabControl.Location = new System.Drawing.Point(20, 53);
            this.mainTabControl.Name = "mainTabControl";
            this.mainTabControl.SelectedIndex = 2;
            this.mainTabControl.Size = new System.Drawing.Size(1246, 714);
            this.mainTabControl.TabIndex = 19;
            this.mainTabControl.UseSelectable = true;
            this.mainTabControl.SelectedIndexChanged += new System.EventHandler(this.mainTabControl_SelectedIndexChanged);
            this.mainTabControl.Deselecting += new System.Windows.Forms.TabControlCancelEventHandler(this.MainTabControl_Deselecting);
            // 
            // genLotTabPage
            // 
            this.genLotTabPage.Controls.Add(this.gL1Pnl);
            this.genLotTabPage.Controls.Add(this.gL2Pnl);
            this.genLotTabPage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.genLotTabPage.HorizontalScrollbarBarColor = false;
            this.genLotTabPage.HorizontalScrollbarHighlightOnWheel = false;
            this.genLotTabPage.HorizontalScrollbarSize = 0;
            this.genLotTabPage.Location = new System.Drawing.Point(4, 47);
            this.genLotTabPage.Name = "genLotTabPage";
            this.genLotTabPage.Size = new System.Drawing.Size(1238, 663);
            this.genLotTabPage.TabIndex = 0;
            this.genLotTabPage.Text = "สร้าง Lot";
            this.genLotTabPage.VerticalScrollbarBarColor = false;
            this.genLotTabPage.VerticalScrollbarHighlightOnWheel = false;
            this.genLotTabPage.VerticalScrollbarSize = 0;
            this.genLotTabPage.Enter += new System.EventHandler(this.GenLotTabPage_Enter);
            // 
            // gL1Pnl
            // 
            this.gL1Pnl.Controls.Add(this.gL1InputPnl);
            this.gL1Pnl.Controls.Add(this.gL1TopLbl);
            this.gL1Pnl.Controls.Add(this.gL1BarcodeInputBtn);
            this.gL1Pnl.Controls.Add(this.gL1OrLbl);
            this.gL1Pnl.Controls.Add(this.gL1BarcodePnl);
            this.gL1Pnl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gL1Pnl.HorizontalScrollbarBarColor = false;
            this.gL1Pnl.HorizontalScrollbarHighlightOnWheel = false;
            this.gL1Pnl.HorizontalScrollbarSize = 0;
            this.gL1Pnl.Location = new System.Drawing.Point(0, 0);
            this.gL1Pnl.Name = "gL1Pnl";
            this.gL1Pnl.Size = new System.Drawing.Size(1238, 663);
            this.gL1Pnl.TabIndex = 20;
            this.gL1Pnl.VerticalScrollbarBarColor = false;
            this.gL1Pnl.VerticalScrollbarHighlightOnWheel = false;
            this.gL1Pnl.VerticalScrollbarSize = 0;
            this.gL1Pnl.MouseHover += new System.EventHandler(this.GL1Pnl_MouseHover);
            // 
            // gL1InputPnl
            // 
            this.gL1InputPnl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gL1InputPnl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.gL1InputPnl.Controls.Add(this.gL1ItemCodeCbb);
            this.gL1InputPnl.Controls.Add(this.gL1GrNoLbl);
            this.gL1InputPnl.Controls.Add(this.gL1AndLbl);
            this.gL1InputPnl.Controls.Add(this.gL1GrNoTxb);
            this.gL1InputPnl.Controls.Add(this.gL1ItemCodeLbl);
            this.gL1InputPnl.Controls.Add(this.gL1SubmitBtn);
            this.gL1InputPnl.HorizontalScrollbarBarColor = true;
            this.gL1InputPnl.HorizontalScrollbarHighlightOnWheel = false;
            this.gL1InputPnl.HorizontalScrollbarSize = 10;
            this.gL1InputPnl.Location = new System.Drawing.Point(341, 114);
            this.gL1InputPnl.Name = "gL1InputPnl";
            this.gL1InputPnl.Size = new System.Drawing.Size(557, 166);
            this.gL1InputPnl.TabIndex = 42;
            this.gL1InputPnl.VerticalScrollbarBarColor = true;
            this.gL1InputPnl.VerticalScrollbarHighlightOnWheel = false;
            this.gL1InputPnl.VerticalScrollbarSize = 10;
            // 
            // gL1ItemCodeCbb
            // 
            this.gL1ItemCodeCbb.FormattingEnabled = true;
            this.gL1ItemCodeCbb.ItemHeight = 23;
            this.gL1ItemCodeCbb.Location = new System.Drawing.Point(170, 114);
            this.gL1ItemCodeCbb.Name = "gL1ItemCodeCbb";
            this.gL1ItemCodeCbb.Size = new System.Drawing.Size(214, 29);
            this.gL1ItemCodeCbb.TabIndex = 41;
            this.gL1ItemCodeCbb.UseSelectable = true;
            // 
            // gL1GrNoLbl
            // 
            this.gL1GrNoLbl.AutoSize = true;
            this.gL1GrNoLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.gL1GrNoLbl.Location = new System.Drawing.Point(35, 23);
            this.gL1GrNoLbl.Name = "gL1GrNoLbl";
            this.gL1GrNoLbl.Size = new System.Drawing.Size(103, 25);
            this.gL1GrNoLbl.TabIndex = 6;
            this.gL1GrNoLbl.Text = "หมายเลข GR";
            // 
            // gL1AndLbl
            // 
            this.gL1AndLbl.AutoSize = true;
            this.gL1AndLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.gL1AndLbl.Location = new System.Drawing.Point(259, 71);
            this.gL1AndLbl.Name = "gL1AndLbl";
            this.gL1AndLbl.Size = new System.Drawing.Size(37, 25);
            this.gL1AndLbl.TabIndex = 40;
            this.gL1AndLbl.Text = "และ";
            // 
            // gL1GrNoTxb
            // 
            // 
            // 
            // 
            this.gL1GrNoTxb.CustomButton.Image = null;
            this.gL1GrNoTxb.CustomButton.Location = new System.Drawing.Point(186, 2);
            this.gL1GrNoTxb.CustomButton.Name = "";
            this.gL1GrNoTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.gL1GrNoTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.gL1GrNoTxb.CustomButton.TabIndex = 1;
            this.gL1GrNoTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.gL1GrNoTxb.CustomButton.UseSelectable = true;
            this.gL1GrNoTxb.CustomButton.Visible = false;
            this.gL1GrNoTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.gL1GrNoTxb.Lines = new string[0];
            this.gL1GrNoTxb.Location = new System.Drawing.Point(170, 23);
            this.gL1GrNoTxb.MaxLength = 32767;
            this.gL1GrNoTxb.Name = "gL1GrNoTxb";
            this.gL1GrNoTxb.PasswordChar = '\0';
            this.gL1GrNoTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.gL1GrNoTxb.SelectedText = "";
            this.gL1GrNoTxb.SelectionLength = 0;
            this.gL1GrNoTxb.SelectionStart = 0;
            this.gL1GrNoTxb.ShortcutsEnabled = true;
            this.gL1GrNoTxb.Size = new System.Drawing.Size(214, 30);
            this.gL1GrNoTxb.TabIndex = 7;
            this.gL1GrNoTxb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.gL1GrNoTxb.UseSelectable = true;
            this.gL1GrNoTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.gL1GrNoTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.gL1GrNoTxb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.GL1GrNoTxb_KeyPress);
            this.gL1GrNoTxb.KeyUp += new System.Windows.Forms.KeyEventHandler(this.GL1GrNoTxb_KeyUp);
            // 
            // gL1ItemCodeLbl
            // 
            this.gL1ItemCodeLbl.AutoSize = true;
            this.gL1ItemCodeLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.gL1ItemCodeLbl.Location = new System.Drawing.Point(48, 118);
            this.gL1ItemCodeLbl.Name = "gL1ItemCodeLbl";
            this.gL1ItemCodeLbl.Size = new System.Drawing.Size(80, 25);
            this.gL1ItemCodeLbl.TabIndex = 37;
            this.gL1ItemCodeLbl.Text = "รหัสสินค้า";
            // 
            // gL1SubmitBtn
            // 
            this.gL1SubmitBtn.BackColor = System.Drawing.Color.SteelBlue;
            this.gL1SubmitBtn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.gL1SubmitBtn.ForeColor = System.Drawing.Color.LightCyan;
            this.gL1SubmitBtn.Location = new System.Drawing.Point(424, 109);
            this.gL1SubmitBtn.Name = "gL1SubmitBtn";
            this.gL1SubmitBtn.Size = new System.Drawing.Size(96, 40);
            this.gL1SubmitBtn.TabIndex = 35;
            this.gL1SubmitBtn.Text = "ยืนยัน";
            this.gL1SubmitBtn.UseCustomBackColor = true;
            this.gL1SubmitBtn.UseCustomForeColor = true;
            this.gL1SubmitBtn.UseSelectable = true;
            this.gL1SubmitBtn.Click += new System.EventHandler(this.GL1SubmitBtn_Click);
            // 
            // gL1TopLbl
            // 
            this.gL1TopLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gL1TopLbl.AutoSize = true;
            this.gL1TopLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.gL1TopLbl.Location = new System.Drawing.Point(235, 76);
            this.gL1TopLbl.Name = "gL1TopLbl";
            this.gL1TopLbl.Size = new System.Drawing.Size(768, 25);
            this.gL1TopLbl.TabIndex = 36;
            this.gL1TopLbl.Text = "กรุณาระบุหมายเลข GR และรหัสสินค้า หรือยิงบาร์โค้ดของหมายเลขใบโอนถ่ายLot เพื่อเข้า" +
    "สู่หน้าบันทึกการโอน Lot";
            // 
            // gL1BarcodeInputBtn
            // 
            this.gL1BarcodeInputBtn.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gL1BarcodeInputBtn.Location = new System.Drawing.Point(512, 515);
            this.gL1BarcodeInputBtn.Name = "gL1BarcodeInputBtn";
            this.gL1BarcodeInputBtn.Size = new System.Drawing.Size(214, 30);
            this.gL1BarcodeInputBtn.TabIndex = 34;
            this.gL1BarcodeInputBtn.Text = "ยิงบาร์โค้ดของหมายเลขใบโอนถ่าย Lot";
            this.gL1BarcodeInputBtn.UseSelectable = true;
            this.gL1BarcodeInputBtn.Visible = false;
            this.gL1BarcodeInputBtn.Click += new System.EventHandler(this.GL1BarcodeInputBtn_Click);
            // 
            // gL1OrLbl
            // 
            this.gL1OrLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gL1OrLbl.AutoSize = true;
            this.gL1OrLbl.Location = new System.Drawing.Point(604, 468);
            this.gL1OrLbl.Name = "gL1OrLbl";
            this.gL1OrLbl.Size = new System.Drawing.Size(30, 19);
            this.gL1OrLbl.TabIndex = 8;
            this.gL1OrLbl.Text = "หรือ";
            this.gL1OrLbl.Visible = false;
            // 
            // gL1BarcodePnl
            // 
            this.gL1BarcodePnl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gL1BarcodePnl.BackColor = System.Drawing.Color.Transparent;
            this.gL1BarcodePnl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.gL1BarcodePnl.Controls.Add(this.metroLabel1);
            this.gL1BarcodePnl.HorizontalScrollbarBarColor = true;
            this.gL1BarcodePnl.HorizontalScrollbarHighlightOnWheel = false;
            this.gL1BarcodePnl.HorizontalScrollbarSize = 10;
            this.gL1BarcodePnl.Location = new System.Drawing.Point(494, 299);
            this.gL1BarcodePnl.Name = "gL1BarcodePnl";
            this.gL1BarcodePnl.Size = new System.Drawing.Size(250, 85);
            this.gL1BarcodePnl.TabIndex = 43;
            this.gL1BarcodePnl.UseCustomBackColor = true;
            this.gL1BarcodePnl.VerticalScrollbarBarColor = true;
            this.gL1BarcodePnl.VerticalScrollbarHighlightOnWheel = false;
            this.gL1BarcodePnl.VerticalScrollbarSize = 10;
            this.gL1BarcodePnl.MouseLeave += new System.EventHandler(this.gLBarcodePnl_MouseLeave);
            this.gL1BarcodePnl.MouseHover += new System.EventHandler(this.gLBarcodePnl_MouseHover);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(73, 5);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(90, 19);
            this.metroLabel1.TabIndex = 2;
            this.metroLabel1.Text = "Barcode Area";
            this.metroLabel1.UseCustomBackColor = true;
            this.metroLabel1.MouseLeave += new System.EventHandler(this.gLBarcodePnl_MouseLeave);
            this.metroLabel1.MouseHover += new System.EventHandler(this.gLBarcodePnl_MouseHover);
            // 
            // gL2Pnl
            // 
            this.gL2Pnl.Controls.Add(this.gL2TopLbl);
            this.gL2Pnl.Controls.Add(this.gL2TotalWeightPnl);
            this.gL2Pnl.Controls.Add(this.gL2DifferenceTxb);
            this.gL2Pnl.Controls.Add(this.gL2QtyWeightTxb);
            this.gL2Pnl.Controls.Add(this.gL2WeightPerTareTxb);
            this.gL2Pnl.Controls.Add(this.gL2ItemCodeTxb);
            this.gL2Pnl.Controls.Add(this.gL2AssignDateTxb);
            this.gL2Pnl.Controls.Add(this.metroPanel4);
            this.gL2Pnl.Controls.Add(this.gL2RefreshBtn);
            this.gL2Pnl.Controls.Add(this.gL2ExitBtn);
            this.gL2Pnl.Controls.Add(this.gL2PrintBarcodeBtn);
            this.gL2Pnl.Controls.Add(this.gL2PrintFormBtn);
            this.gL2Pnl.Controls.Add(this.gL2SubmitBtn);
            this.gL2Pnl.Controls.Add(this.gL2GenerateLotBtn);
            this.gL2Pnl.Controls.Add(this.gL2FromWhseTxb);
            this.gL2Pnl.Controls.Add(this.gL2ToWhseTxb);
            this.gL2Pnl.Controls.Add(this.gL2DocNoLbl);
            this.gL2Pnl.Controls.Add(this.gL2WeightPerTareLbl);
            this.gL2Pnl.Controls.Add(this.gL2AssignDateLbl);
            this.gL2Pnl.Controls.Add(this.gL2RefNoLbl);
            this.gL2Pnl.Controls.Add(this.gL2Kgs2Lbl);
            this.gL2Pnl.Controls.Add(this.gL2TareTxb);
            this.gL2Pnl.Controls.Add(this.gL2RefNoTxb);
            this.gL2Pnl.Controls.Add(this.gL2TareLbl);
            this.gL2Pnl.Controls.Add(this.gL2DocNoTxb);
            this.gL2Pnl.Controls.Add(this.gL2DifferenceLbl);
            this.gL2Pnl.Controls.Add(this.gL2ItemCodeLbl);
            this.gL2Pnl.Controls.Add(this.gL2QtyCountLbl);
            this.gL2Pnl.Controls.Add(this.gL2KgsLbl);
            this.gL2Pnl.Controls.Add(this.gL2ItemLotNoLbl);
            this.gL2Pnl.Controls.Add(this.gL2ItemLotNoTxb);
            this.gL2Pnl.Controls.Add(this.gL2QtyWeightLbl);
            this.gL2Pnl.Controls.Add(this.gL2QtyCountTxb);
            this.gL2Pnl.Controls.Add(this.gL2FromWhseLbl);
            this.gL2Pnl.Controls.Add(this.gL2LotAttrTxb);
            this.gL2Pnl.Controls.Add(this.gL2ToWhseLbl);
            this.gL2Pnl.Controls.Add(this.gL2LotAttrLbl);
            this.gL2Pnl.Controls.Add(this.gL2GenLotTableDGV);
            this.gL2Pnl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gL2Pnl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gL2Pnl.HorizontalScrollbarBarColor = true;
            this.gL2Pnl.HorizontalScrollbarHighlightOnWheel = false;
            this.gL2Pnl.HorizontalScrollbarSize = 10;
            this.gL2Pnl.Location = new System.Drawing.Point(0, 0);
            this.gL2Pnl.Name = "gL2Pnl";
            this.gL2Pnl.Size = new System.Drawing.Size(1238, 663);
            this.gL2Pnl.TabIndex = 33;
            this.gL2Pnl.VerticalScrollbarBarColor = true;
            this.gL2Pnl.VerticalScrollbarHighlightOnWheel = false;
            this.gL2Pnl.VerticalScrollbarSize = 10;
            this.gL2Pnl.Visible = false;
            // 
            // gL2TopLbl
            // 
            this.gL2TopLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gL2TopLbl.AutoSize = true;
            this.gL2TopLbl.BackColor = System.Drawing.Color.White;
            this.gL2TopLbl.Font = new System.Drawing.Font("Microsoft Tai Le", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gL2TopLbl.Location = new System.Drawing.Point(520, 26);
            this.gL2TopLbl.Name = "gL2TopLbl";
            this.gL2TopLbl.Size = new System.Drawing.Size(198, 34);
            this.gL2TopLbl.TabIndex = 105;
            this.gL2TopLbl.Text = "บันทึกการโอน Lot";
            // 
            // gL2TotalWeightPnl
            // 
            this.gL2TotalWeightPnl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gL2TotalWeightPnl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.gL2TotalWeightPnl.Controls.Add(this.metroPanel8);
            this.gL2TotalWeightPnl.Controls.Add(this.metroPanel9);
            this.gL2TotalWeightPnl.Controls.Add(this.gL2TotalWeightLbl);
            this.gL2TotalWeightPnl.HorizontalScrollbarBarColor = true;
            this.gL2TotalWeightPnl.HorizontalScrollbarHighlightOnWheel = false;
            this.gL2TotalWeightPnl.HorizontalScrollbarSize = 10;
            this.gL2TotalWeightPnl.Location = new System.Drawing.Point(894, 322);
            this.gL2TotalWeightPnl.Name = "gL2TotalWeightPnl";
            this.gL2TotalWeightPnl.Size = new System.Drawing.Size(344, 57);
            this.gL2TotalWeightPnl.TabIndex = 102;
            this.gL2TotalWeightPnl.VerticalScrollbarBarColor = true;
            this.gL2TotalWeightPnl.VerticalScrollbarHighlightOnWheel = false;
            this.gL2TotalWeightPnl.VerticalScrollbarSize = 10;
            // 
            // metroPanel8
            // 
            this.metroPanel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.metroPanel8.Controls.Add(this.metroLabel11);
            this.metroPanel8.Controls.Add(this.gL2Kgs4Lbl);
            this.metroPanel8.Controls.Add(this.gL2TotalNetWeightTxb);
            this.metroPanel8.HorizontalScrollbarBarColor = true;
            this.metroPanel8.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel8.HorizontalScrollbarSize = 10;
            this.metroPanel8.Location = new System.Drawing.Point(169, 26);
            this.metroPanel8.Name = "metroPanel8";
            this.metroPanel8.Size = new System.Drawing.Size(174, 35);
            this.metroPanel8.TabIndex = 97;
            this.metroPanel8.VerticalScrollbarBarColor = true;
            this.metroPanel8.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel8.VerticalScrollbarSize = 10;
            // 
            // metroLabel11
            // 
            this.metroLabel11.AutoSize = true;
            this.metroLabel11.Location = new System.Drawing.Point(10, 4);
            this.metroLabel11.Name = "metroLabel11";
            this.metroLabel11.Size = new System.Drawing.Size(30, 19);
            this.metroLabel11.TabIndex = 107;
            this.metroLabel11.Text = "Net";
            // 
            // gL2Kgs4Lbl
            // 
            this.gL2Kgs4Lbl.AutoSize = true;
            this.gL2Kgs4Lbl.Location = new System.Drawing.Point(135, 5);
            this.gL2Kgs4Lbl.Name = "gL2Kgs4Lbl";
            this.gL2Kgs4Lbl.Size = new System.Drawing.Size(29, 19);
            this.gL2Kgs4Lbl.TabIndex = 100;
            this.gL2Kgs4Lbl.Text = "Kgs";
            // 
            // gL2TotalNetWeightTxb
            // 
            // 
            // 
            // 
            this.gL2TotalNetWeightTxb.CustomButton.Image = null;
            this.gL2TotalNetWeightTxb.CustomButton.Location = new System.Drawing.Point(53, 2);
            this.gL2TotalNetWeightTxb.CustomButton.Name = "";
            this.gL2TotalNetWeightTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.gL2TotalNetWeightTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.gL2TotalNetWeightTxb.CustomButton.TabIndex = 1;
            this.gL2TotalNetWeightTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.gL2TotalNetWeightTxb.CustomButton.UseSelectable = true;
            this.gL2TotalNetWeightTxb.CustomButton.Visible = false;
            this.gL2TotalNetWeightTxb.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.gL2TotalNetWeightTxb.ForeColor = System.Drawing.Color.Gray;
            this.gL2TotalNetWeightTxb.Lines = new string[0];
            this.gL2TotalNetWeightTxb.Location = new System.Drawing.Point(50, -1);
            this.gL2TotalNetWeightTxb.MaxLength = 32767;
            this.gL2TotalNetWeightTxb.Name = "gL2TotalNetWeightTxb";
            this.gL2TotalNetWeightTxb.PasswordChar = '\0';
            this.gL2TotalNetWeightTxb.ReadOnly = true;
            this.gL2TotalNetWeightTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.gL2TotalNetWeightTxb.SelectedText = "";
            this.gL2TotalNetWeightTxb.SelectionLength = 0;
            this.gL2TotalNetWeightTxb.SelectionStart = 0;
            this.gL2TotalNetWeightTxb.ShortcutsEnabled = true;
            this.gL2TotalNetWeightTxb.Size = new System.Drawing.Size(81, 30);
            this.gL2TotalNetWeightTxb.TabIndex = 99;
            this.gL2TotalNetWeightTxb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.gL2TotalNetWeightTxb.UseCustomForeColor = true;
            this.gL2TotalNetWeightTxb.UseSelectable = true;
            this.gL2TotalNetWeightTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.gL2TotalNetWeightTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroPanel9
            // 
            this.metroPanel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.metroPanel9.Controls.Add(this.metroLabel12);
            this.metroPanel9.Controls.Add(this.gL2TotalGrossWeightTxb);
            this.metroPanel9.Controls.Add(this.gL2Kgs3Lbl);
            this.metroPanel9.HorizontalScrollbarBarColor = true;
            this.metroPanel9.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel9.HorizontalScrollbarSize = 10;
            this.metroPanel9.Location = new System.Drawing.Point(-1, 26);
            this.metroPanel9.Name = "metroPanel9";
            this.metroPanel9.Size = new System.Drawing.Size(177, 35);
            this.metroPanel9.TabIndex = 96;
            this.metroPanel9.VerticalScrollbarBarColor = true;
            this.metroPanel9.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel9.VerticalScrollbarSize = 10;
            // 
            // metroLabel12
            // 
            this.metroLabel12.AutoSize = true;
            this.metroLabel12.Location = new System.Drawing.Point(5, 4);
            this.metroLabel12.Name = "metroLabel12";
            this.metroLabel12.Size = new System.Drawing.Size(41, 19);
            this.metroLabel12.TabIndex = 106;
            this.metroLabel12.Text = "Gross";
            // 
            // gL2TotalGrossWeightTxb
            // 
            // 
            // 
            // 
            this.gL2TotalGrossWeightTxb.CustomButton.Image = null;
            this.gL2TotalGrossWeightTxb.CustomButton.Location = new System.Drawing.Point(49, 2);
            this.gL2TotalGrossWeightTxb.CustomButton.Name = "";
            this.gL2TotalGrossWeightTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.gL2TotalGrossWeightTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.gL2TotalGrossWeightTxb.CustomButton.TabIndex = 1;
            this.gL2TotalGrossWeightTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.gL2TotalGrossWeightTxb.CustomButton.UseSelectable = true;
            this.gL2TotalGrossWeightTxb.CustomButton.Visible = false;
            this.gL2TotalGrossWeightTxb.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.gL2TotalGrossWeightTxb.ForeColor = System.Drawing.Color.Gray;
            this.gL2TotalGrossWeightTxb.Lines = new string[0];
            this.gL2TotalGrossWeightTxb.Location = new System.Drawing.Point(52, -1);
            this.gL2TotalGrossWeightTxb.MaxLength = 32767;
            this.gL2TotalGrossWeightTxb.Name = "gL2TotalGrossWeightTxb";
            this.gL2TotalGrossWeightTxb.PasswordChar = '\0';
            this.gL2TotalGrossWeightTxb.ReadOnly = true;
            this.gL2TotalGrossWeightTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.gL2TotalGrossWeightTxb.SelectedText = "";
            this.gL2TotalGrossWeightTxb.SelectionLength = 0;
            this.gL2TotalGrossWeightTxb.SelectionStart = 0;
            this.gL2TotalGrossWeightTxb.ShortcutsEnabled = true;
            this.gL2TotalGrossWeightTxb.Size = new System.Drawing.Size(77, 30);
            this.gL2TotalGrossWeightTxb.TabIndex = 96;
            this.gL2TotalGrossWeightTxb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.gL2TotalGrossWeightTxb.UseCustomForeColor = true;
            this.gL2TotalGrossWeightTxb.UseSelectable = true;
            this.gL2TotalGrossWeightTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.gL2TotalGrossWeightTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // gL2Kgs3Lbl
            // 
            this.gL2Kgs3Lbl.AutoSize = true;
            this.gL2Kgs3Lbl.Location = new System.Drawing.Point(134, 5);
            this.gL2Kgs3Lbl.Name = "gL2Kgs3Lbl";
            this.gL2Kgs3Lbl.Size = new System.Drawing.Size(29, 19);
            this.gL2Kgs3Lbl.TabIndex = 97;
            this.gL2Kgs3Lbl.Text = "Kgs";
            // 
            // gL2TotalWeightLbl
            // 
            this.gL2TotalWeightLbl.AutoSize = true;
            this.gL2TotalWeightLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.gL2TotalWeightLbl.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.gL2TotalWeightLbl.Location = new System.Drawing.Point(128, -1);
            this.gL2TotalWeightLbl.Name = "gL2TotalWeightLbl";
            this.gL2TotalWeightLbl.Size = new System.Drawing.Size(68, 25);
            this.gL2TotalWeightLbl.TabIndex = 95;
            this.gL2TotalWeightLbl.Text = "น.น.รวม";
            // 
            // gL2DifferenceTxb
            // 
            this.gL2DifferenceTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gL2DifferenceTxb.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.gL2DifferenceTxb.CustomButton.Image = null;
            this.gL2DifferenceTxb.CustomButton.Location = new System.Drawing.Point(71, 2);
            this.gL2DifferenceTxb.CustomButton.Name = "";
            this.gL2DifferenceTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.gL2DifferenceTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.gL2DifferenceTxb.CustomButton.TabIndex = 1;
            this.gL2DifferenceTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.gL2DifferenceTxb.CustomButton.UseSelectable = true;
            this.gL2DifferenceTxb.CustomButton.Visible = false;
            this.gL2DifferenceTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.gL2DifferenceTxb.ForeColor = System.Drawing.Color.Gray;
            this.gL2DifferenceTxb.Lines = new string[0];
            this.gL2DifferenceTxb.Location = new System.Drawing.Point(913, 177);
            this.gL2DifferenceTxb.MaxLength = 32767;
            this.gL2DifferenceTxb.Name = "gL2DifferenceTxb";
            this.gL2DifferenceTxb.PasswordChar = '\0';
            this.gL2DifferenceTxb.ReadOnly = true;
            this.gL2DifferenceTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.gL2DifferenceTxb.SelectedText = "";
            this.gL2DifferenceTxb.SelectionLength = 0;
            this.gL2DifferenceTxb.SelectionStart = 0;
            this.gL2DifferenceTxb.ShortcutsEnabled = true;
            this.gL2DifferenceTxb.Size = new System.Drawing.Size(99, 30);
            this.gL2DifferenceTxb.TabIndex = 51;
            this.gL2DifferenceTxb.UseCustomBackColor = true;
            this.gL2DifferenceTxb.UseCustomForeColor = true;
            this.gL2DifferenceTxb.UseSelectable = true;
            this.gL2DifferenceTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.gL2DifferenceTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // gL2QtyWeightTxb
            // 
            this.gL2QtyWeightTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.gL2QtyWeightTxb.CustomButton.Image = null;
            this.gL2QtyWeightTxb.CustomButton.Location = new System.Drawing.Point(71, 2);
            this.gL2QtyWeightTxb.CustomButton.Name = "";
            this.gL2QtyWeightTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.gL2QtyWeightTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.gL2QtyWeightTxb.CustomButton.TabIndex = 1;
            this.gL2QtyWeightTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.gL2QtyWeightTxb.CustomButton.UseSelectable = true;
            this.gL2QtyWeightTxb.CustomButton.Visible = false;
            this.gL2QtyWeightTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.gL2QtyWeightTxb.ForeColor = System.Drawing.Color.Gray;
            this.gL2QtyWeightTxb.Lines = new string[0];
            this.gL2QtyWeightTxb.Location = new System.Drawing.Point(913, 140);
            this.gL2QtyWeightTxb.MaxLength = 32767;
            this.gL2QtyWeightTxb.Name = "gL2QtyWeightTxb";
            this.gL2QtyWeightTxb.PasswordChar = '\0';
            this.gL2QtyWeightTxb.ReadOnly = true;
            this.gL2QtyWeightTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.gL2QtyWeightTxb.SelectedText = "";
            this.gL2QtyWeightTxb.SelectionLength = 0;
            this.gL2QtyWeightTxb.SelectionStart = 0;
            this.gL2QtyWeightTxb.ShortcutsEnabled = true;
            this.gL2QtyWeightTxb.Size = new System.Drawing.Size(99, 30);
            this.gL2QtyWeightTxb.TabIndex = 50;
            this.gL2QtyWeightTxb.UseCustomForeColor = true;
            this.gL2QtyWeightTxb.UseSelectable = true;
            this.gL2QtyWeightTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.gL2QtyWeightTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // gL2WeightPerTareTxb
            // 
            this.gL2WeightPerTareTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.gL2WeightPerTareTxb.CustomButton.Image = null;
            this.gL2WeightPerTareTxb.CustomButton.Location = new System.Drawing.Point(71, 2);
            this.gL2WeightPerTareTxb.CustomButton.Name = "";
            this.gL2WeightPerTareTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.gL2WeightPerTareTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.gL2WeightPerTareTxb.CustomButton.TabIndex = 1;
            this.gL2WeightPerTareTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.gL2WeightPerTareTxb.CustomButton.UseSelectable = true;
            this.gL2WeightPerTareTxb.CustomButton.Visible = false;
            this.gL2WeightPerTareTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.gL2WeightPerTareTxb.ForeColor = System.Drawing.Color.Gray;
            this.gL2WeightPerTareTxb.Lines = new string[0];
            this.gL2WeightPerTareTxb.Location = new System.Drawing.Point(732, 250);
            this.gL2WeightPerTareTxb.MaxLength = 32767;
            this.gL2WeightPerTareTxb.Name = "gL2WeightPerTareTxb";
            this.gL2WeightPerTareTxb.PasswordChar = '\0';
            this.gL2WeightPerTareTxb.ReadOnly = true;
            this.gL2WeightPerTareTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.gL2WeightPerTareTxb.SelectedText = "";
            this.gL2WeightPerTareTxb.SelectionLength = 0;
            this.gL2WeightPerTareTxb.SelectionStart = 0;
            this.gL2WeightPerTareTxb.ShortcutsEnabled = true;
            this.gL2WeightPerTareTxb.Size = new System.Drawing.Size(99, 30);
            this.gL2WeightPerTareTxb.TabIndex = 48;
            this.gL2WeightPerTareTxb.UseCustomForeColor = true;
            this.gL2WeightPerTareTxb.UseSelectable = true;
            this.gL2WeightPerTareTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.gL2WeightPerTareTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // gL2ItemCodeTxb
            // 
            this.gL2ItemCodeTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.gL2ItemCodeTxb.CustomButton.Image = null;
            this.gL2ItemCodeTxb.CustomButton.Location = new System.Drawing.Point(267, 2);
            this.gL2ItemCodeTxb.CustomButton.Name = "";
            this.gL2ItemCodeTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.gL2ItemCodeTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.gL2ItemCodeTxb.CustomButton.TabIndex = 1;
            this.gL2ItemCodeTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.gL2ItemCodeTxb.CustomButton.UseSelectable = true;
            this.gL2ItemCodeTxb.CustomButton.Visible = false;
            this.gL2ItemCodeTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.gL2ItemCodeTxb.ForeColor = System.Drawing.Color.Gray;
            this.gL2ItemCodeTxb.Lines = new string[0];
            this.gL2ItemCodeTxb.Location = new System.Drawing.Point(229, 139);
            this.gL2ItemCodeTxb.MaxLength = 32767;
            this.gL2ItemCodeTxb.Name = "gL2ItemCodeTxb";
            this.gL2ItemCodeTxb.PasswordChar = '\0';
            this.gL2ItemCodeTxb.ReadOnly = true;
            this.gL2ItemCodeTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.gL2ItemCodeTxb.SelectedText = "";
            this.gL2ItemCodeTxb.SelectionLength = 0;
            this.gL2ItemCodeTxb.SelectionStart = 0;
            this.gL2ItemCodeTxb.ShortcutsEnabled = true;
            this.gL2ItemCodeTxb.Size = new System.Drawing.Size(295, 30);
            this.gL2ItemCodeTxb.TabIndex = 47;
            this.gL2ItemCodeTxb.UseCustomForeColor = true;
            this.gL2ItemCodeTxb.UseSelectable = true;
            this.gL2ItemCodeTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.gL2ItemCodeTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // gL2AssignDateTxb
            // 
            this.gL2AssignDateTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.gL2AssignDateTxb.CustomButton.Image = null;
            this.gL2AssignDateTxb.CustomButton.Location = new System.Drawing.Point(146, 2);
            this.gL2AssignDateTxb.CustomButton.Name = "";
            this.gL2AssignDateTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.gL2AssignDateTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.gL2AssignDateTxb.CustomButton.TabIndex = 1;
            this.gL2AssignDateTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.gL2AssignDateTxb.CustomButton.UseSelectable = true;
            this.gL2AssignDateTxb.CustomButton.Visible = false;
            this.gL2AssignDateTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.gL2AssignDateTxb.ForeColor = System.Drawing.Color.Gray;
            this.gL2AssignDateTxb.Lines = new string[0];
            this.gL2AssignDateTxb.Location = new System.Drawing.Point(1058, 57);
            this.gL2AssignDateTxb.MaxLength = 32767;
            this.gL2AssignDateTxb.Name = "gL2AssignDateTxb";
            this.gL2AssignDateTxb.PasswordChar = '\0';
            this.gL2AssignDateTxb.ReadOnly = true;
            this.gL2AssignDateTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.gL2AssignDateTxb.SelectedText = "";
            this.gL2AssignDateTxb.SelectionLength = 0;
            this.gL2AssignDateTxb.SelectionStart = 0;
            this.gL2AssignDateTxb.ShortcutsEnabled = true;
            this.gL2AssignDateTxb.Size = new System.Drawing.Size(174, 30);
            this.gL2AssignDateTxb.TabIndex = 46;
            this.gL2AssignDateTxb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.gL2AssignDateTxb.UseCustomForeColor = true;
            this.gL2AssignDateTxb.UseSelectable = true;
            this.gL2AssignDateTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.gL2AssignDateTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroPanel4
            // 
            this.metroPanel4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.metroPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.metroPanel4.Controls.Add(this.gL2PrintAllChk);
            this.metroPanel4.Controls.Add(this.gL2ToLbl);
            this.metroPanel4.Controls.Add(this.gL2PrintStartTxb);
            this.metroPanel4.Controls.Add(this.gL2PrintToTxb);
            this.metroPanel4.Controls.Add(this.gL2FromLbl);
            this.metroPanel4.HorizontalScrollbarBarColor = true;
            this.metroPanel4.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel4.HorizontalScrollbarSize = 10;
            this.metroPanel4.Location = new System.Drawing.Point(728, 615);
            this.metroPanel4.Name = "metroPanel4";
            this.metroPanel4.Size = new System.Drawing.Size(334, 40);
            this.metroPanel4.TabIndex = 42;
            this.metroPanel4.VerticalScrollbarBarColor = true;
            this.metroPanel4.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel4.VerticalScrollbarSize = 10;
            // 
            // gL2PrintAllChk
            // 
            this.gL2PrintAllChk.AutoSize = true;
            this.gL2PrintAllChk.Checked = true;
            this.gL2PrintAllChk.CheckState = System.Windows.Forms.CheckState.Checked;
            this.gL2PrintAllChk.FontSize = MetroFramework.MetroCheckBoxSize.Tall;
            this.gL2PrintAllChk.Location = new System.Drawing.Point(6, 7);
            this.gL2PrintAllChk.Name = "gL2PrintAllChk";
            this.gL2PrintAllChk.Size = new System.Drawing.Size(111, 25);
            this.gL2PrintAllChk.TabIndex = 18;
            this.gL2PrintAllChk.Text = "พิมพ์ทั้งหมด";
            this.gL2PrintAllChk.UseSelectable = true;
            this.gL2PrintAllChk.CheckedChanged += new System.EventHandler(this.GL2PrintAllChk_CheckedChanged);
            // 
            // gL2ToLbl
            // 
            this.gL2ToLbl.AutoSize = true;
            this.gL2ToLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.gL2ToLbl.Location = new System.Drawing.Point(228, 7);
            this.gL2ToLbl.Name = "gL2ToLbl";
            this.gL2ToLbl.Size = new System.Drawing.Size(29, 25);
            this.gL2ToLbl.TabIndex = 22;
            this.gL2ToLbl.Text = "ถึง";
            // 
            // gL2PrintStartTxb
            // 
            // 
            // 
            // 
            this.gL2PrintStartTxb.CustomButton.Image = null;
            this.gL2PrintStartTxb.CustomButton.Location = new System.Drawing.Point(34, 2);
            this.gL2PrintStartTxb.CustomButton.Name = "";
            this.gL2PrintStartTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.gL2PrintStartTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.gL2PrintStartTxb.CustomButton.TabIndex = 1;
            this.gL2PrintStartTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.gL2PrintStartTxb.CustomButton.UseSelectable = true;
            this.gL2PrintStartTxb.CustomButton.Visible = false;
            this.gL2PrintStartTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.gL2PrintStartTxb.Lines = new string[0];
            this.gL2PrintStartTxb.Location = new System.Drawing.Point(163, 4);
            this.gL2PrintStartTxb.MaxLength = 32767;
            this.gL2PrintStartTxb.Name = "gL2PrintStartTxb";
            this.gL2PrintStartTxb.PasswordChar = '\0';
            this.gL2PrintStartTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.gL2PrintStartTxb.SelectedText = "";
            this.gL2PrintStartTxb.SelectionLength = 0;
            this.gL2PrintStartTxb.SelectionStart = 0;
            this.gL2PrintStartTxb.ShortcutsEnabled = true;
            this.gL2PrintStartTxb.Size = new System.Drawing.Size(62, 30);
            this.gL2PrintStartTxb.TabIndex = 19;
            this.gL2PrintStartTxb.UseSelectable = true;
            this.gL2PrintStartTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.gL2PrintStartTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.gL2PrintStartTxb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.GL2PrintStartTxb_KeyPress);
            // 
            // gL2PrintToTxb
            // 
            // 
            // 
            // 
            this.gL2PrintToTxb.CustomButton.Image = null;
            this.gL2PrintToTxb.CustomButton.Location = new System.Drawing.Point(34, 2);
            this.gL2PrintToTxb.CustomButton.Name = "";
            this.gL2PrintToTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.gL2PrintToTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.gL2PrintToTxb.CustomButton.TabIndex = 1;
            this.gL2PrintToTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.gL2PrintToTxb.CustomButton.UseSelectable = true;
            this.gL2PrintToTxb.CustomButton.Visible = false;
            this.gL2PrintToTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.gL2PrintToTxb.Lines = new string[0];
            this.gL2PrintToTxb.Location = new System.Drawing.Point(258, 4);
            this.gL2PrintToTxb.MaxLength = 32767;
            this.gL2PrintToTxb.Name = "gL2PrintToTxb";
            this.gL2PrintToTxb.PasswordChar = '\0';
            this.gL2PrintToTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.gL2PrintToTxb.SelectedText = "";
            this.gL2PrintToTxb.SelectionLength = 0;
            this.gL2PrintToTxb.SelectionStart = 0;
            this.gL2PrintToTxb.ShortcutsEnabled = true;
            this.gL2PrintToTxb.Size = new System.Drawing.Size(62, 30);
            this.gL2PrintToTxb.TabIndex = 20;
            this.gL2PrintToTxb.UseSelectable = true;
            this.gL2PrintToTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.gL2PrintToTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.gL2PrintToTxb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.GL2PrintToTxb_KeyPress);
            // 
            // gL2FromLbl
            // 
            this.gL2FromLbl.AutoSize = true;
            this.gL2FromLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.gL2FromLbl.Location = new System.Drawing.Point(123, 7);
            this.gL2FromLbl.Name = "gL2FromLbl";
            this.gL2FromLbl.Size = new System.Drawing.Size(39, 25);
            this.gL2FromLbl.TabIndex = 21;
            this.gL2FromLbl.Text = "จาก";
            // 
            // gL2RefreshBtn
            // 
            this.gL2RefreshBtn.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gL2RefreshBtn.BackColor = System.Drawing.Color.Lavender;
            this.gL2RefreshBtn.BackgroundImage = global::SWS.Properties.Resources.refresh_page_option;
            this.gL2RefreshBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.gL2RefreshBtn.Location = new System.Drawing.Point(1107, 136);
            this.gL2RefreshBtn.Margin = new System.Windows.Forms.Padding(10);
            this.gL2RefreshBtn.Name = "gL2RefreshBtn";
            this.gL2RefreshBtn.Size = new System.Drawing.Size(56, 36);
            this.gL2RefreshBtn.TabIndex = 41;
            this.gL2RefreshBtn.UseSelectable = true;
            this.gL2RefreshBtn.Click += new System.EventHandler(this.GL2RefreshBtn_Click);
            // 
            // gL2ExitBtn
            // 
            this.gL2ExitBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.gL2ExitBtn.BackColor = System.Drawing.Color.IndianRed;
            this.gL2ExitBtn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.gL2ExitBtn.ForeColor = System.Drawing.Color.MistyRose;
            this.gL2ExitBtn.Location = new System.Drawing.Point(1092, 615);
            this.gL2ExitBtn.Name = "gL2ExitBtn";
            this.gL2ExitBtn.Size = new System.Drawing.Size(150, 40);
            this.gL2ExitBtn.TabIndex = 40;
            this.gL2ExitBtn.Text = "ออกจากหน้านี้";
            this.gL2ExitBtn.UseCustomBackColor = true;
            this.gL2ExitBtn.UseCustomForeColor = true;
            this.gL2ExitBtn.UseSelectable = true;
            this.gL2ExitBtn.Click += new System.EventHandler(this.GL2ExitBtn_Click);
            // 
            // gL2PrintBarcodeBtn
            // 
            this.gL2PrintBarcodeBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.gL2PrintBarcodeBtn.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.gL2PrintBarcodeBtn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.gL2PrintBarcodeBtn.ForeColor = System.Drawing.Color.FloralWhite;
            this.gL2PrintBarcodeBtn.Location = new System.Drawing.Point(579, 615);
            this.gL2PrintBarcodeBtn.Name = "gL2PrintBarcodeBtn";
            this.gL2PrintBarcodeBtn.Size = new System.Drawing.Size(150, 40);
            this.gL2PrintBarcodeBtn.TabIndex = 38;
            this.gL2PrintBarcodeBtn.Text = "พิมพ์บาร์โค้ด";
            this.gL2PrintBarcodeBtn.UseCustomBackColor = true;
            this.gL2PrintBarcodeBtn.UseCustomForeColor = true;
            this.gL2PrintBarcodeBtn.UseSelectable = true;
            this.gL2PrintBarcodeBtn.Click += new System.EventHandler(this.GL2PrintBarcodeBtn_Click);
            // 
            // gL2PrintFormBtn
            // 
            this.gL2PrintFormBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.gL2PrintFormBtn.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.gL2PrintFormBtn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.gL2PrintFormBtn.ForeColor = System.Drawing.Color.FloralWhite;
            this.gL2PrintFormBtn.Location = new System.Drawing.Point(389, 615);
            this.gL2PrintFormBtn.Name = "gL2PrintFormBtn";
            this.gL2PrintFormBtn.Size = new System.Drawing.Size(150, 40);
            this.gL2PrintFormBtn.TabIndex = 37;
            this.gL2PrintFormBtn.Text = "พิมพ์แบบฟอร์ม";
            this.gL2PrintFormBtn.UseCustomBackColor = true;
            this.gL2PrintFormBtn.UseCustomForeColor = true;
            this.gL2PrintFormBtn.UseSelectable = true;
            this.gL2PrintFormBtn.Click += new System.EventHandler(this.GL2PrintFormBtn_Click);
            // 
            // gL2SubmitBtn
            // 
            this.gL2SubmitBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.gL2SubmitBtn.BackColor = System.Drawing.Color.SteelBlue;
            this.gL2SubmitBtn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.gL2SubmitBtn.ForeColor = System.Drawing.Color.LightCyan;
            this.gL2SubmitBtn.Location = new System.Drawing.Point(199, 615);
            this.gL2SubmitBtn.Name = "gL2SubmitBtn";
            this.gL2SubmitBtn.Size = new System.Drawing.Size(150, 40);
            this.gL2SubmitBtn.TabIndex = 36;
            this.gL2SubmitBtn.Text = "บันทึกข้อมูล";
            this.gL2SubmitBtn.UseCustomBackColor = true;
            this.gL2SubmitBtn.UseCustomForeColor = true;
            this.gL2SubmitBtn.UseSelectable = true;
            this.gL2SubmitBtn.Click += new System.EventHandler(this.GL2SubmitBtn_Click);
            // 
            // gL2GenerateLotBtn
            // 
            this.gL2GenerateLotBtn.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gL2GenerateLotBtn.BackColor = System.Drawing.Color.MediumPurple;
            this.gL2GenerateLotBtn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.gL2GenerateLotBtn.ForeColor = System.Drawing.Color.Lavender;
            this.gL2GenerateLotBtn.Location = new System.Drawing.Point(913, 247);
            this.gL2GenerateLotBtn.Name = "gL2GenerateLotBtn";
            this.gL2GenerateLotBtn.Size = new System.Drawing.Size(150, 40);
            this.gL2GenerateLotBtn.TabIndex = 33;
            this.gL2GenerateLotBtn.Text = "สร้าง Lot";
            this.gL2GenerateLotBtn.UseCustomBackColor = true;
            this.gL2GenerateLotBtn.UseCustomForeColor = true;
            this.gL2GenerateLotBtn.UseSelectable = true;
            this.gL2GenerateLotBtn.Click += new System.EventHandler(this.GL2GenerateLotBtn_Click);
            // 
            // gL2FromWhseTxb
            // 
            this.gL2FromWhseTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gL2FromWhseTxb.Enabled = false;
            this.gL2FromWhseTxb.FormattingEnabled = true;
            this.gL2FromWhseTxb.ItemHeight = 23;
            this.gL2FromWhseTxb.Items.AddRange(new object[] {
            "SQC",
            "SRM",
            "SPD2",
            "SPD4",
            "SFG"});
            this.gL2FromWhseTxb.Location = new System.Drawing.Point(229, 250);
            this.gL2FromWhseTxb.Name = "gL2FromWhseTxb";
            this.gL2FromWhseTxb.Size = new System.Drawing.Size(110, 29);
            this.gL2FromWhseTxb.TabIndex = 30;
            this.gL2FromWhseTxb.UseSelectable = true;
            // 
            // gL2ToWhseTxb
            // 
            this.gL2ToWhseTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gL2ToWhseTxb.FormattingEnabled = true;
            this.gL2ToWhseTxb.ItemHeight = 23;
            this.gL2ToWhseTxb.Items.AddRange(new object[] {
            "SQC",
            "SRM",
            "SPD2",
            "SPD4",
            "SFG"});
            this.gL2ToWhseTxb.Location = new System.Drawing.Point(229, 288);
            this.gL2ToWhseTxb.Name = "gL2ToWhseTxb";
            this.gL2ToWhseTxb.Size = new System.Drawing.Size(110, 29);
            this.gL2ToWhseTxb.TabIndex = 31;
            this.gL2ToWhseTxb.UseSelectable = true;
            // 
            // gL2DocNoLbl
            // 
            this.gL2DocNoLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gL2DocNoLbl.AutoSize = true;
            this.gL2DocNoLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.gL2DocNoLbl.Location = new System.Drawing.Point(863, 26);
            this.gL2DocNoLbl.Name = "gL2DocNoLbl";
            this.gL2DocNoLbl.Size = new System.Drawing.Size(170, 25);
            this.gL2DocNoLbl.TabIndex = 0;
            this.gL2DocNoLbl.Text = "หมายเลขใบโอนถ่าย Lot";
            // 
            // gL2WeightPerTareLbl
            // 
            this.gL2WeightPerTareLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gL2WeightPerTareLbl.AutoSize = true;
            this.gL2WeightPerTareLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.gL2WeightPerTareLbl.Location = new System.Drawing.Point(571, 252);
            this.gL2WeightPerTareLbl.Name = "gL2WeightPerTareLbl";
            this.gL2WeightPerTareLbl.Size = new System.Drawing.Size(130, 25);
            this.gL2WeightPerTareLbl.TabIndex = 29;
            this.gL2WeightPerTareLbl.Text = "น.น.ต่อ 1 Tare = ";
            // 
            // gL2AssignDateLbl
            // 
            this.gL2AssignDateLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gL2AssignDateLbl.AutoSize = true;
            this.gL2AssignDateLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.gL2AssignDateLbl.Location = new System.Drawing.Point(863, 57);
            this.gL2AssignDateLbl.Name = "gL2AssignDateLbl";
            this.gL2AssignDateLbl.Size = new System.Drawing.Size(42, 25);
            this.gL2AssignDateLbl.TabIndex = 1;
            this.gL2AssignDateLbl.Text = "วันที่";
            // 
            // gL2RefNoLbl
            // 
            this.gL2RefNoLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gL2RefNoLbl.AutoSize = true;
            this.gL2RefNoLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.gL2RefNoLbl.Location = new System.Drawing.Point(863, 88);
            this.gL2RefNoLbl.Name = "gL2RefNoLbl";
            this.gL2RefNoLbl.Size = new System.Drawing.Size(118, 25);
            this.gL2RefNoLbl.TabIndex = 2;
            this.gL2RefNoLbl.Text = "หมายเลขอ้างอิง";
            // 
            // gL2Kgs2Lbl
            // 
            this.gL2Kgs2Lbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gL2Kgs2Lbl.AutoSize = true;
            this.gL2Kgs2Lbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.gL2Kgs2Lbl.Location = new System.Drawing.Point(842, 216);
            this.gL2Kgs2Lbl.Name = "gL2Kgs2Lbl";
            this.gL2Kgs2Lbl.Size = new System.Drawing.Size(36, 25);
            this.gL2Kgs2Lbl.TabIndex = 27;
            this.gL2Kgs2Lbl.Text = "กก.";
            // 
            // gL2TareTxb
            // 
            this.gL2TareTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.gL2TareTxb.CustomButton.Image = null;
            this.gL2TareTxb.CustomButton.Location = new System.Drawing.Point(71, 2);
            this.gL2TareTxb.CustomButton.Name = "";
            this.gL2TareTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.gL2TareTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.gL2TareTxb.CustomButton.TabIndex = 1;
            this.gL2TareTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.gL2TareTxb.CustomButton.UseSelectable = true;
            this.gL2TareTxb.CustomButton.Visible = false;
            this.gL2TareTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.gL2TareTxb.Lines = new string[0];
            this.gL2TareTxb.Location = new System.Drawing.Point(732, 213);
            this.gL2TareTxb.MaxLength = 32767;
            this.gL2TareTxb.Name = "gL2TareTxb";
            this.gL2TareTxb.PasswordChar = '\0';
            this.gL2TareTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.gL2TareTxb.SelectedText = "";
            this.gL2TareTxb.SelectionLength = 0;
            this.gL2TareTxb.SelectionStart = 0;
            this.gL2TareTxb.ShortcutsEnabled = true;
            this.gL2TareTxb.Size = new System.Drawing.Size(99, 30);
            this.gL2TareTxb.TabIndex = 26;
            this.gL2TareTxb.UseSelectable = true;
            this.gL2TareTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.gL2TareTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.gL2TareTxb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.GL2TareTxb_KeyPress);
            this.gL2TareTxb.KeyUp += new System.Windows.Forms.KeyEventHandler(this.GL2TareTxb_KeyUp);
            // 
            // gL2RefNoTxb
            // 
            this.gL2RefNoTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.gL2RefNoTxb.CustomButton.Image = null;
            this.gL2RefNoTxb.CustomButton.Location = new System.Drawing.Point(146, 2);
            this.gL2RefNoTxb.CustomButton.Name = "";
            this.gL2RefNoTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.gL2RefNoTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.gL2RefNoTxb.CustomButton.TabIndex = 1;
            this.gL2RefNoTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.gL2RefNoTxb.CustomButton.UseSelectable = true;
            this.gL2RefNoTxb.CustomButton.Visible = false;
            this.gL2RefNoTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.gL2RefNoTxb.Lines = new string[0];
            this.gL2RefNoTxb.Location = new System.Drawing.Point(1058, 88);
            this.gL2RefNoTxb.MaxLength = 32767;
            this.gL2RefNoTxb.Name = "gL2RefNoTxb";
            this.gL2RefNoTxb.PasswordChar = '\0';
            this.gL2RefNoTxb.ReadOnly = true;
            this.gL2RefNoTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.gL2RefNoTxb.SelectedText = "";
            this.gL2RefNoTxb.SelectionLength = 0;
            this.gL2RefNoTxb.SelectionStart = 0;
            this.gL2RefNoTxb.ShortcutsEnabled = true;
            this.gL2RefNoTxb.Size = new System.Drawing.Size(174, 30);
            this.gL2RefNoTxb.TabIndex = 4;
            this.gL2RefNoTxb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.gL2RefNoTxb.UseSelectable = true;
            this.gL2RefNoTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.gL2RefNoTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // gL2TareLbl
            // 
            this.gL2TareLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gL2TareLbl.AutoSize = true;
            this.gL2TareLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.gL2TareLbl.Location = new System.Drawing.Point(571, 214);
            this.gL2TareLbl.Name = "gL2TareLbl";
            this.gL2TareLbl.Size = new System.Drawing.Size(108, 25);
            this.gL2TareLbl.TabIndex = 25;
            this.gL2TareLbl.Text = "น.น. Tare รวม";
            // 
            // gL2DocNoTxb
            // 
            this.gL2DocNoTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.gL2DocNoTxb.CustomButton.Image = null;
            this.gL2DocNoTxb.CustomButton.Location = new System.Drawing.Point(146, 2);
            this.gL2DocNoTxb.CustomButton.Name = "";
            this.gL2DocNoTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.gL2DocNoTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.gL2DocNoTxb.CustomButton.TabIndex = 1;
            this.gL2DocNoTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.gL2DocNoTxb.CustomButton.UseSelectable = true;
            this.gL2DocNoTxb.CustomButton.Visible = false;
            this.gL2DocNoTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.gL2DocNoTxb.ForeColor = System.Drawing.Color.Gray;
            this.gL2DocNoTxb.Lines = new string[0];
            this.gL2DocNoTxb.Location = new System.Drawing.Point(1058, 26);
            this.gL2DocNoTxb.MaxLength = 32767;
            this.gL2DocNoTxb.Name = "gL2DocNoTxb";
            this.gL2DocNoTxb.PasswordChar = '\0';
            this.gL2DocNoTxb.ReadOnly = true;
            this.gL2DocNoTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.gL2DocNoTxb.SelectedText = "";
            this.gL2DocNoTxb.SelectionLength = 0;
            this.gL2DocNoTxb.SelectionStart = 0;
            this.gL2DocNoTxb.ShortcutsEnabled = true;
            this.gL2DocNoTxb.Size = new System.Drawing.Size(174, 30);
            this.gL2DocNoTxb.TabIndex = 5;
            this.gL2DocNoTxb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.gL2DocNoTxb.UseCustomForeColor = true;
            this.gL2DocNoTxb.UseSelectable = true;
            this.gL2DocNoTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.gL2DocNoTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // gL2DifferenceLbl
            // 
            this.gL2DifferenceLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gL2DifferenceLbl.AutoSize = true;
            this.gL2DifferenceLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.gL2DifferenceLbl.Location = new System.Drawing.Point(842, 180);
            this.gL2DifferenceLbl.Name = "gL2DifferenceLbl";
            this.gL2DifferenceLbl.Size = new System.Drawing.Size(45, 25);
            this.gL2DifferenceLbl.TabIndex = 24;
            this.gL2DifferenceLbl.Text = "Diffs";
            // 
            // gL2ItemCodeLbl
            // 
            this.gL2ItemCodeLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gL2ItemCodeLbl.AutoSize = true;
            this.gL2ItemCodeLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.gL2ItemCodeLbl.Location = new System.Drawing.Point(21, 137);
            this.gL2ItemCodeLbl.Name = "gL2ItemCodeLbl";
            this.gL2ItemCodeLbl.Size = new System.Drawing.Size(80, 25);
            this.gL2ItemCodeLbl.TabIndex = 6;
            this.gL2ItemCodeLbl.Text = "รหัสสินค้า";
            // 
            // gL2QtyCountLbl
            // 
            this.gL2QtyCountLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gL2QtyCountLbl.AutoSize = true;
            this.gL2QtyCountLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.gL2QtyCountLbl.Location = new System.Drawing.Point(571, 144);
            this.gL2QtyCountLbl.Name = "gL2QtyCountLbl";
            this.gL2QtyCountLbl.Size = new System.Drawing.Size(59, 25);
            this.gL2QtyCountLbl.TabIndex = 7;
            this.gL2QtyCountLbl.Text = "จำนวน";
            // 
            // gL2KgsLbl
            // 
            this.gL2KgsLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gL2KgsLbl.AutoSize = true;
            this.gL2KgsLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.gL2KgsLbl.Location = new System.Drawing.Point(1023, 142);
            this.gL2KgsLbl.Name = "gL2KgsLbl";
            this.gL2KgsLbl.Size = new System.Drawing.Size(36, 25);
            this.gL2KgsLbl.TabIndex = 22;
            this.gL2KgsLbl.Text = "กก.";
            // 
            // gL2ItemLotNoLbl
            // 
            this.gL2ItemLotNoLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gL2ItemLotNoLbl.AutoSize = true;
            this.gL2ItemLotNoLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.gL2ItemLotNoLbl.Location = new System.Drawing.Point(21, 174);
            this.gL2ItemLotNoLbl.Name = "gL2ItemLotNoLbl";
            this.gL2ItemLotNoLbl.Size = new System.Drawing.Size(104, 25);
            this.gL2ItemLotNoLbl.TabIndex = 8;
            this.gL2ItemLotNoLbl.Text = "หมายเลข Lot";
            // 
            // gL2ItemLotNoTxb
            // 
            this.gL2ItemLotNoTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.gL2ItemLotNoTxb.CustomButton.Image = null;
            this.gL2ItemLotNoTxb.CustomButton.Location = new System.Drawing.Point(267, 2);
            this.gL2ItemLotNoTxb.CustomButton.Name = "";
            this.gL2ItemLotNoTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.gL2ItemLotNoTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.gL2ItemLotNoTxb.CustomButton.TabIndex = 1;
            this.gL2ItemLotNoTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.gL2ItemLotNoTxb.CustomButton.UseSelectable = true;
            this.gL2ItemLotNoTxb.CustomButton.Visible = false;
            this.gL2ItemLotNoTxb.Enabled = false;
            this.gL2ItemLotNoTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.gL2ItemLotNoTxb.ForeColor = System.Drawing.Color.Gray;
            this.gL2ItemLotNoTxb.Lines = new string[0];
            this.gL2ItemLotNoTxb.Location = new System.Drawing.Point(229, 176);
            this.gL2ItemLotNoTxb.MaxLength = 32767;
            this.gL2ItemLotNoTxb.Name = "gL2ItemLotNoTxb";
            this.gL2ItemLotNoTxb.PasswordChar = '\0';
            this.gL2ItemLotNoTxb.ReadOnly = true;
            this.gL2ItemLotNoTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.gL2ItemLotNoTxb.SelectedText = "";
            this.gL2ItemLotNoTxb.SelectionLength = 0;
            this.gL2ItemLotNoTxb.SelectionStart = 0;
            this.gL2ItemLotNoTxb.ShortcutsEnabled = true;
            this.gL2ItemLotNoTxb.Size = new System.Drawing.Size(295, 30);
            this.gL2ItemLotNoTxb.TabIndex = 10;
            this.gL2ItemLotNoTxb.Tag = "";
            this.gL2ItemLotNoTxb.UseCustomForeColor = true;
            this.gL2ItemLotNoTxb.UseSelectable = true;
            this.gL2ItemLotNoTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.gL2ItemLotNoTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // gL2QtyWeightLbl
            // 
            this.gL2QtyWeightLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gL2QtyWeightLbl.AutoSize = true;
            this.gL2QtyWeightLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.gL2QtyWeightLbl.Location = new System.Drawing.Point(842, 142);
            this.gL2QtyWeightLbl.Name = "gL2QtyWeightLbl";
            this.gL2QtyWeightLbl.Size = new System.Drawing.Size(37, 25);
            this.gL2QtyWeightLbl.TabIndex = 20;
            this.gL2QtyWeightLbl.Text = "เบล";
            // 
            // gL2QtyCountTxb
            // 
            this.gL2QtyCountTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.gL2QtyCountTxb.CustomButton.Image = null;
            this.gL2QtyCountTxb.CustomButton.Location = new System.Drawing.Point(71, 2);
            this.gL2QtyCountTxb.CustomButton.Name = "";
            this.gL2QtyCountTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.gL2QtyCountTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.gL2QtyCountTxb.CustomButton.TabIndex = 1;
            this.gL2QtyCountTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.gL2QtyCountTxb.CustomButton.UseSelectable = true;
            this.gL2QtyCountTxb.CustomButton.Visible = false;
            this.gL2QtyCountTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.gL2QtyCountTxb.Lines = new string[0];
            this.gL2QtyCountTxb.Location = new System.Drawing.Point(732, 140);
            this.gL2QtyCountTxb.MaxLength = 32767;
            this.gL2QtyCountTxb.Name = "gL2QtyCountTxb";
            this.gL2QtyCountTxb.PasswordChar = '\0';
            this.gL2QtyCountTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.gL2QtyCountTxb.SelectedText = "";
            this.gL2QtyCountTxb.SelectionLength = 0;
            this.gL2QtyCountTxb.SelectionStart = 0;
            this.gL2QtyCountTxb.ShortcutsEnabled = true;
            this.gL2QtyCountTxb.Size = new System.Drawing.Size(99, 30);
            this.gL2QtyCountTxb.TabIndex = 18;
            this.gL2QtyCountTxb.UseSelectable = true;
            this.gL2QtyCountTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.gL2QtyCountTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.gL2QtyCountTxb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.GL2QtyCountTxb_KeyPress);
            this.gL2QtyCountTxb.KeyUp += new System.Windows.Forms.KeyEventHandler(this.GL2QtyCountTxb_KeyUp);
            // 
            // gL2FromWhseLbl
            // 
            this.gL2FromWhseLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gL2FromWhseLbl.AutoSize = true;
            this.gL2FromWhseLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.gL2FromWhseLbl.Location = new System.Drawing.Point(21, 251);
            this.gL2FromWhseLbl.Name = "gL2FromWhseLbl";
            this.gL2FromWhseLbl.Size = new System.Drawing.Size(70, 25);
            this.gL2FromWhseLbl.TabIndex = 12;
            this.gL2FromWhseLbl.Text = "จากโกดัง";
            // 
            // gL2LotAttrTxb
            // 
            this.gL2LotAttrTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.gL2LotAttrTxb.CustomButton.Image = null;
            this.gL2LotAttrTxb.CustomButton.Location = new System.Drawing.Point(267, 2);
            this.gL2LotAttrTxb.CustomButton.Name = "";
            this.gL2LotAttrTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.gL2LotAttrTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.gL2LotAttrTxb.CustomButton.TabIndex = 1;
            this.gL2LotAttrTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.gL2LotAttrTxb.CustomButton.UseSelectable = true;
            this.gL2LotAttrTxb.CustomButton.Visible = false;
            this.gL2LotAttrTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.gL2LotAttrTxb.ForeColor = System.Drawing.Color.Gray;
            this.gL2LotAttrTxb.Lines = new string[0];
            this.gL2LotAttrTxb.Location = new System.Drawing.Point(229, 213);
            this.gL2LotAttrTxb.MaxLength = 32767;
            this.gL2LotAttrTxb.Name = "gL2LotAttrTxb";
            this.gL2LotAttrTxb.PasswordChar = '\0';
            this.gL2LotAttrTxb.ReadOnly = true;
            this.gL2LotAttrTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.gL2LotAttrTxb.SelectedText = "";
            this.gL2LotAttrTxb.SelectionLength = 0;
            this.gL2LotAttrTxb.SelectionStart = 0;
            this.gL2LotAttrTxb.ShortcutsEnabled = true;
            this.gL2LotAttrTxb.Size = new System.Drawing.Size(295, 30);
            this.gL2LotAttrTxb.TabIndex = 16;
            this.gL2LotAttrTxb.Theme = MetroFramework.MetroThemeStyle.Light;
            this.gL2LotAttrTxb.UseCustomForeColor = true;
            this.gL2LotAttrTxb.UseSelectable = true;
            this.gL2LotAttrTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.gL2LotAttrTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // gL2ToWhseLbl
            // 
            this.gL2ToWhseLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gL2ToWhseLbl.AutoSize = true;
            this.gL2ToWhseLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.gL2ToWhseLbl.Location = new System.Drawing.Point(21, 290);
            this.gL2ToWhseLbl.Name = "gL2ToWhseLbl";
            this.gL2ToWhseLbl.Size = new System.Drawing.Size(74, 25);
            this.gL2ToWhseLbl.TabIndex = 13;
            this.gL2ToWhseLbl.Text = "ไปที่โกดัง";
            // 
            // gL2LotAttrLbl
            // 
            this.gL2LotAttrLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gL2LotAttrLbl.AutoSize = true;
            this.gL2LotAttrLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.gL2LotAttrLbl.Location = new System.Drawing.Point(21, 211);
            this.gL2LotAttrLbl.Name = "gL2LotAttrLbl";
            this.gL2LotAttrLbl.Size = new System.Drawing.Size(161, 25);
            this.gL2LotAttrLbl.TabIndex = 14;
            this.gL2LotAttrLbl.Text = "คุณสมบัติสินค้า Lot นี้";
            // 
            // gL2GenLotTableDGV
            // 
            this.gL2GenLotTableDGV.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gL2GenLotTableDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.gL2GenLotTableDGV.BackgroundColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gL2GenLotTableDGV.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.gL2GenLotTableDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gL2GenLotTableDGV.DefaultCellStyle = dataGridViewCellStyle2;
            this.gL2GenLotTableDGV.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.gL2GenLotTableDGV.Location = new System.Drawing.Point(3, 378);
            this.gL2GenLotTableDGV.Name = "gL2GenLotTableDGV";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gL2GenLotTableDGV.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.gL2GenLotTableDGV.RowHeadersVisible = false;
            this.gL2GenLotTableDGV.RowHeadersWidth = 62;
            this.gL2GenLotTableDGV.Size = new System.Drawing.Size(1235, 225);
            this.gL2GenLotTableDGV.TabIndex = 104;
            this.gL2GenLotTableDGV.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GL2GenLotTableDGV_CellClick);
            this.gL2GenLotTableDGV.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.GL2GenLotTableDGV_CellValueChanged);
            this.gL2GenLotTableDGV.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.gL2GenLotTableDGV_EditingControlShowing);
            this.gL2GenLotTableDGV.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.GL2GenLotTableDGV_RowPrePaint);
            // 
            // issueTabPage
            // 
            this.issueTabPage.Controls.Add(this.iP3Pnl);
            this.issueTabPage.Controls.Add(this.iP2Pnl);
            this.issueTabPage.Controls.Add(this.iP1Pnl);
            this.issueTabPage.Controls.Add(this.iP4Pnl);
            this.issueTabPage.Controls.Add(this.iP5Pnl);
            this.issueTabPage.HorizontalScrollbarBarColor = true;
            this.issueTabPage.HorizontalScrollbarHighlightOnWheel = false;
            this.issueTabPage.HorizontalScrollbarSize = 0;
            this.issueTabPage.Location = new System.Drawing.Point(4, 47);
            this.issueTabPage.Name = "issueTabPage";
            this.issueTabPage.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.issueTabPage.Size = new System.Drawing.Size(1238, 663);
            this.issueTabPage.TabIndex = 3;
            this.issueTabPage.Text = "ผลิต/เบิก";
            this.issueTabPage.VerticalScrollbarBarColor = true;
            this.issueTabPage.VerticalScrollbarHighlightOnWheel = false;
            this.issueTabPage.VerticalScrollbarSize = 0;
            this.issueTabPage.Enter += new System.EventHandler(this.IssueTabPage_Enter);
            // 
            // iP3Pnl
            // 
            this.iP3Pnl.Controls.Add(this.iP3SendB1Btn);
            this.iP3Pnl.Controls.Add(this.iP3TopLbl);
            this.iP3Pnl.Controls.Add(this.iP3BarcodePnl);
            this.iP3Pnl.Controls.Add(this.iPMagicBtn);
            this.iP3Pnl.Controls.Add(this.iP3ScannedItemDgv);
            this.iP3Pnl.Controls.Add(this.iP3DateCbb);
            this.iP3Pnl.Controls.Add(this.iP3DateLbl);
            this.iP3Pnl.Controls.Add(this.iP3TotalPnl);
            this.iP3Pnl.Controls.Add(this.iP3CancelBtn);
            this.iP3Pnl.Controls.Add(this.iP3SubmitBtn);
            this.iP3Pnl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.iP3Pnl.HorizontalScrollbarBarColor = true;
            this.iP3Pnl.HorizontalScrollbarHighlightOnWheel = false;
            this.iP3Pnl.HorizontalScrollbarSize = 10;
            this.iP3Pnl.Location = new System.Drawing.Point(0, 0);
            this.iP3Pnl.Name = "iP3Pnl";
            this.iP3Pnl.Size = new System.Drawing.Size(1238, 663);
            this.iP3Pnl.TabIndex = 36;
            this.iP3Pnl.VerticalScrollbarBarColor = true;
            this.iP3Pnl.VerticalScrollbarHighlightOnWheel = false;
            this.iP3Pnl.VerticalScrollbarSize = 10;
            this.iP3Pnl.Enter += new System.EventHandler(this.IP3Pnl_Enter);
            this.iP3Pnl.MouseHover += new System.EventHandler(this.IP3Pnl_MouseHover);
            this.iP3Pnl.Validated += new System.EventHandler(this.iP3Pnl_Validated);
            // 
            // iP3SendB1Btn
            // 
            this.iP3SendB1Btn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.iP3SendB1Btn.BackColor = System.Drawing.Color.SteelBlue;
            this.iP3SendB1Btn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.iP3SendB1Btn.ForeColor = System.Drawing.Color.LightCyan;
            this.iP3SendB1Btn.Location = new System.Drawing.Point(699, 615);
            this.iP3SendB1Btn.Name = "iP3SendB1Btn";
            this.iP3SendB1Btn.Size = new System.Drawing.Size(150, 40);
            this.iP3SendB1Btn.TabIndex = 197;
            this.iP3SendB1Btn.Text = "ส่งขึ้น B1";
            this.iP3SendB1Btn.UseCustomBackColor = true;
            this.iP3SendB1Btn.UseCustomForeColor = true;
            this.iP3SendB1Btn.UseSelectable = true;
            this.iP3SendB1Btn.Click += new System.EventHandler(this.iP3SendB1Btn_Click);
            // 
            // iP3TopLbl
            // 
            this.iP3TopLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP3TopLbl.AutoSize = true;
            this.iP3TopLbl.BackColor = System.Drawing.Color.White;
            this.iP3TopLbl.Font = new System.Drawing.Font("Microsoft Tai Le", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.iP3TopLbl.Location = new System.Drawing.Point(463, 30);
            this.iP3TopLbl.Name = "iP3TopLbl";
            this.iP3TopLbl.Size = new System.Drawing.Size(313, 34);
            this.iP3TopLbl.TabIndex = 196;
            this.iP3TopLbl.Text = "สรุปรายการเบิก/รับ/คืนวัตถุดิบ";
            // 
            // iP3BarcodePnl
            // 
            this.iP3BarcodePnl.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.iP3BarcodePnl.BackColor = System.Drawing.Color.Transparent;
            this.iP3BarcodePnl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.iP3BarcodePnl.Controls.Add(this.metroLabel7);
            this.iP3BarcodePnl.HorizontalScrollbarBarColor = true;
            this.iP3BarcodePnl.HorizontalScrollbarHighlightOnWheel = false;
            this.iP3BarcodePnl.HorizontalScrollbarSize = 10;
            this.iP3BarcodePnl.Location = new System.Drawing.Point(12, 13);
            this.iP3BarcodePnl.Name = "iP3BarcodePnl";
            this.iP3BarcodePnl.Size = new System.Drawing.Size(281, 83);
            this.iP3BarcodePnl.TabIndex = 195;
            this.iP3BarcodePnl.UseCustomBackColor = true;
            this.iP3BarcodePnl.VerticalScrollbarBarColor = true;
            this.iP3BarcodePnl.VerticalScrollbarHighlightOnWheel = false;
            this.iP3BarcodePnl.VerticalScrollbarSize = 10;
            this.iP3BarcodePnl.MouseLeave += new System.EventHandler(this.iP3BarcodePnl_MouseLeave);
            this.iP3BarcodePnl.MouseHover += new System.EventHandler(this.iP3BarcodePnl_MouseHover);
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(84, 4);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(90, 19);
            this.metroLabel7.TabIndex = 2;
            this.metroLabel7.Text = "Barcode Area";
            this.metroLabel7.UseCustomBackColor = true;
            // 
            // iPMagicBtn
            // 
            this.iPMagicBtn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.iPMagicBtn.DisplayFocus = true;
            this.iPMagicBtn.Highlight = true;
            this.iPMagicBtn.Location = new System.Drawing.Point(94, 613);
            this.iPMagicBtn.Name = "iPMagicBtn";
            this.iPMagicBtn.Size = new System.Drawing.Size(351, 47);
            this.iPMagicBtn.TabIndex = 171;
            this.iPMagicBtn.Text = "Scan some random item!";
            this.iPMagicBtn.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.iPMagicBtn.UseSelectable = true;
            this.iPMagicBtn.Visible = false;
            this.iPMagicBtn.Click += new System.EventHandler(this.IPMagicBtn_Click);
            // 
            // iP3ScannedItemDgv
            // 
            this.iP3ScannedItemDgv.AllowUserToAddRows = false;
            this.iP3ScannedItemDgv.AllowUserToDeleteRows = false;
            this.iP3ScannedItemDgv.AllowUserToResizeColumns = false;
            this.iP3ScannedItemDgv.AllowUserToResizeRows = false;
            this.iP3ScannedItemDgv.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.iP3ScannedItemDgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.iP3ScannedItemDgv.BackgroundColor = System.Drawing.SystemColors.Window;
            this.iP3ScannedItemDgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.iP3ScannedItemDgv.Location = new System.Drawing.Point(6, 207);
            this.iP3ScannedItemDgv.Name = "iP3ScannedItemDgv";
            this.iP3ScannedItemDgv.RowHeadersVisible = false;
            this.iP3ScannedItemDgv.RowHeadersWidth = 62;
            this.iP3ScannedItemDgv.Size = new System.Drawing.Size(1230, 393);
            this.iP3ScannedItemDgv.TabIndex = 170;
            this.iP3ScannedItemDgv.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.IP3ScannedItemDgv_CellClick);
            this.iP3ScannedItemDgv.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.iP3ScannedItemDgv_CellEndEdit);
            this.iP3ScannedItemDgv.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.iP3ScannedItemDgv_DataBindingComplete);
            this.iP3ScannedItemDgv.MouseHover += new System.EventHandler(this.IP3ScannedItemDgv_MouseHover);
            // 
            // iP3DateCbb
            // 
            this.iP3DateCbb.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.iP3DateCbb.CustomFormat = "d MMMM yyyy";
            this.iP3DateCbb.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.iP3DateCbb.Location = new System.Drawing.Point(973, 86);
            this.iP3DateCbb.MinimumSize = new System.Drawing.Size(0, 29);
            this.iP3DateCbb.Name = "iP3DateCbb";
            this.iP3DateCbb.Size = new System.Drawing.Size(169, 29);
            this.iP3DateCbb.TabIndex = 169;
            this.iP3DateCbb.Value = new System.DateTime(2019, 9, 11, 20, 33, 11, 0);
            // 
            // iP3DateLbl
            // 
            this.iP3DateLbl.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.iP3DateLbl.AutoSize = true;
            this.iP3DateLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP3DateLbl.Location = new System.Drawing.Point(914, 88);
            this.iP3DateLbl.Name = "iP3DateLbl";
            this.iP3DateLbl.Size = new System.Drawing.Size(42, 25);
            this.iP3DateLbl.TabIndex = 168;
            this.iP3DateLbl.Text = "วันที่";
            // 
            // iP3TotalPnl
            // 
            this.iP3TotalPnl.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.iP3TotalPnl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.iP3TotalPnl.Controls.Add(this.metroPanel6);
            this.iP3TotalPnl.Controls.Add(this.iP3TotalGrossWeightPnl);
            this.iP3TotalPnl.Controls.Add(this.iP3TotalWeightLbl);
            this.iP3TotalPnl.HorizontalScrollbarBarColor = true;
            this.iP3TotalPnl.HorizontalScrollbarHighlightOnWheel = false;
            this.iP3TotalPnl.HorizontalScrollbarSize = 10;
            this.iP3TotalPnl.Location = new System.Drawing.Point(914, 147);
            this.iP3TotalPnl.Name = "iP3TotalPnl";
            this.iP3TotalPnl.Size = new System.Drawing.Size(322, 61);
            this.iP3TotalPnl.TabIndex = 101;
            this.iP3TotalPnl.VerticalScrollbarBarColor = true;
            this.iP3TotalPnl.VerticalScrollbarHighlightOnWheel = false;
            this.iP3TotalPnl.VerticalScrollbarSize = 10;
            // 
            // metroPanel6
            // 
            this.metroPanel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.metroPanel6.Controls.Add(this.metroLabel10);
            this.metroPanel6.Controls.Add(this.iP3TotalNetWeightLbl);
            this.metroPanel6.Controls.Add(this.iP3TotalNetWeightTxb);
            this.metroPanel6.HorizontalScrollbarBarColor = true;
            this.metroPanel6.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel6.HorizontalScrollbarSize = 10;
            this.metroPanel6.Location = new System.Drawing.Point(165, 28);
            this.metroPanel6.Name = "metroPanel6";
            this.metroPanel6.Size = new System.Drawing.Size(156, 35);
            this.metroPanel6.TabIndex = 97;
            this.metroPanel6.VerticalScrollbarBarColor = true;
            this.metroPanel6.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel6.VerticalScrollbarSize = 10;
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.Location = new System.Drawing.Point(3, 5);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(30, 19);
            this.metroLabel10.TabIndex = 101;
            this.metroLabel10.Text = "Net";
            // 
            // iP3TotalNetWeightLbl
            // 
            this.iP3TotalNetWeightLbl.AutoSize = true;
            this.iP3TotalNetWeightLbl.Location = new System.Drawing.Point(125, 5);
            this.iP3TotalNetWeightLbl.Name = "iP3TotalNetWeightLbl";
            this.iP3TotalNetWeightLbl.Size = new System.Drawing.Size(27, 19);
            this.iP3TotalNetWeightLbl.TabIndex = 100;
            this.iP3TotalNetWeightLbl.Text = "Kg.";
            // 
            // iP3TotalNetWeightTxb
            // 
            // 
            // 
            // 
            this.iP3TotalNetWeightTxb.CustomButton.Image = null;
            this.iP3TotalNetWeightTxb.CustomButton.Location = new System.Drawing.Point(55, 2);
            this.iP3TotalNetWeightTxb.CustomButton.Margin = new System.Windows.Forms.Padding(2);
            this.iP3TotalNetWeightTxb.CustomButton.Name = "";
            this.iP3TotalNetWeightTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.iP3TotalNetWeightTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.iP3TotalNetWeightTxb.CustomButton.TabIndex = 1;
            this.iP3TotalNetWeightTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.iP3TotalNetWeightTxb.CustomButton.UseSelectable = true;
            this.iP3TotalNetWeightTxb.CustomButton.Visible = false;
            this.iP3TotalNetWeightTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.iP3TotalNetWeightTxb.ForeColor = System.Drawing.Color.Gray;
            this.iP3TotalNetWeightTxb.Lines = new string[] {
        "1050.55"};
            this.iP3TotalNetWeightTxb.Location = new System.Drawing.Point(39, 0);
            this.iP3TotalNetWeightTxb.MaxLength = 32767;
            this.iP3TotalNetWeightTxb.Name = "iP3TotalNetWeightTxb";
            this.iP3TotalNetWeightTxb.PasswordChar = '\0';
            this.iP3TotalNetWeightTxb.ReadOnly = true;
            this.iP3TotalNetWeightTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.iP3TotalNetWeightTxb.SelectedText = "";
            this.iP3TotalNetWeightTxb.SelectionLength = 0;
            this.iP3TotalNetWeightTxb.SelectionStart = 0;
            this.iP3TotalNetWeightTxb.ShortcutsEnabled = true;
            this.iP3TotalNetWeightTxb.Size = new System.Drawing.Size(83, 30);
            this.iP3TotalNetWeightTxb.TabIndex = 99;
            this.iP3TotalNetWeightTxb.Text = "1050.55";
            this.iP3TotalNetWeightTxb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.iP3TotalNetWeightTxb.UseCustomForeColor = true;
            this.iP3TotalNetWeightTxb.UseSelectable = true;
            this.iP3TotalNetWeightTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.iP3TotalNetWeightTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // iP3TotalGrossWeightPnl
            // 
            this.iP3TotalGrossWeightPnl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.iP3TotalGrossWeightPnl.Controls.Add(this.metroLabel9);
            this.iP3TotalGrossWeightPnl.Controls.Add(this.iP3TotalGrossWeightTxb);
            this.iP3TotalGrossWeightPnl.Controls.Add(this.iP3TotalGrossWeightLbl);
            this.iP3TotalGrossWeightPnl.HorizontalScrollbarBarColor = true;
            this.iP3TotalGrossWeightPnl.HorizontalScrollbarHighlightOnWheel = false;
            this.iP3TotalGrossWeightPnl.HorizontalScrollbarSize = 10;
            this.iP3TotalGrossWeightPnl.Location = new System.Drawing.Point(-1, 28);
            this.iP3TotalGrossWeightPnl.Name = "iP3TotalGrossWeightPnl";
            this.iP3TotalGrossWeightPnl.Size = new System.Drawing.Size(168, 35);
            this.iP3TotalGrossWeightPnl.TabIndex = 96;
            this.iP3TotalGrossWeightPnl.VerticalScrollbarBarColor = true;
            this.iP3TotalGrossWeightPnl.VerticalScrollbarHighlightOnWheel = false;
            this.iP3TotalGrossWeightPnl.VerticalScrollbarSize = 10;
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.Location = new System.Drawing.Point(7, 5);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(41, 19);
            this.metroLabel9.TabIndex = 98;
            this.metroLabel9.Text = "Gross";
            // 
            // iP3TotalGrossWeightTxb
            // 
            // 
            // 
            // 
            this.iP3TotalGrossWeightTxb.CustomButton.Image = null;
            this.iP3TotalGrossWeightTxb.CustomButton.Location = new System.Drawing.Point(53, 2);
            this.iP3TotalGrossWeightTxb.CustomButton.Margin = new System.Windows.Forms.Padding(2);
            this.iP3TotalGrossWeightTxb.CustomButton.Name = "";
            this.iP3TotalGrossWeightTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.iP3TotalGrossWeightTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.iP3TotalGrossWeightTxb.CustomButton.TabIndex = 1;
            this.iP3TotalGrossWeightTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.iP3TotalGrossWeightTxb.CustomButton.UseSelectable = true;
            this.iP3TotalGrossWeightTxb.CustomButton.Visible = false;
            this.iP3TotalGrossWeightTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.iP3TotalGrossWeightTxb.ForeColor = System.Drawing.Color.Gray;
            this.iP3TotalGrossWeightTxb.Lines = new string[0];
            this.iP3TotalGrossWeightTxb.Location = new System.Drawing.Point(54, 0);
            this.iP3TotalGrossWeightTxb.MaxLength = 32767;
            this.iP3TotalGrossWeightTxb.Name = "iP3TotalGrossWeightTxb";
            this.iP3TotalGrossWeightTxb.PasswordChar = '\0';
            this.iP3TotalGrossWeightTxb.ReadOnly = true;
            this.iP3TotalGrossWeightTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.iP3TotalGrossWeightTxb.SelectedText = "";
            this.iP3TotalGrossWeightTxb.SelectionLength = 0;
            this.iP3TotalGrossWeightTxb.SelectionStart = 0;
            this.iP3TotalGrossWeightTxb.ShortcutsEnabled = true;
            this.iP3TotalGrossWeightTxb.Size = new System.Drawing.Size(81, 30);
            this.iP3TotalGrossWeightTxb.TabIndex = 96;
            this.iP3TotalGrossWeightTxb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.iP3TotalGrossWeightTxb.UseCustomForeColor = true;
            this.iP3TotalGrossWeightTxb.UseSelectable = true;
            this.iP3TotalGrossWeightTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.iP3TotalGrossWeightTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // iP3TotalGrossWeightLbl
            // 
            this.iP3TotalGrossWeightLbl.AutoSize = true;
            this.iP3TotalGrossWeightLbl.Location = new System.Drawing.Point(136, 5);
            this.iP3TotalGrossWeightLbl.Name = "iP3TotalGrossWeightLbl";
            this.iP3TotalGrossWeightLbl.Size = new System.Drawing.Size(27, 19);
            this.iP3TotalGrossWeightLbl.TabIndex = 97;
            this.iP3TotalGrossWeightLbl.Text = "Kg.";
            // 
            // iP3TotalWeightLbl
            // 
            this.iP3TotalWeightLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP3TotalWeightLbl.AutoSize = true;
            this.iP3TotalWeightLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP3TotalWeightLbl.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.iP3TotalWeightLbl.Location = new System.Drawing.Point(124, 2);
            this.iP3TotalWeightLbl.Name = "iP3TotalWeightLbl";
            this.iP3TotalWeightLbl.Size = new System.Drawing.Size(68, 25);
            this.iP3TotalWeightLbl.TabIndex = 95;
            this.iP3TotalWeightLbl.Text = "น.น.รวม";
            // 
            // iP3CancelBtn
            // 
            this.iP3CancelBtn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.iP3CancelBtn.BackColor = System.Drawing.Color.IndianRed;
            this.iP3CancelBtn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.iP3CancelBtn.ForeColor = System.Drawing.Color.MistyRose;
            this.iP3CancelBtn.Location = new System.Drawing.Point(1091, 615);
            this.iP3CancelBtn.Name = "iP3CancelBtn";
            this.iP3CancelBtn.Size = new System.Drawing.Size(150, 40);
            this.iP3CancelBtn.TabIndex = 37;
            this.iP3CancelBtn.Text = "ออกจากหน้านี้";
            this.iP3CancelBtn.UseCustomBackColor = true;
            this.iP3CancelBtn.UseCustomForeColor = true;
            this.iP3CancelBtn.UseSelectable = true;
            this.iP3CancelBtn.Click += new System.EventHandler(this.IP3CancelBtn_Click);
            // 
            // iP3SubmitBtn
            // 
            this.iP3SubmitBtn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.iP3SubmitBtn.BackColor = System.Drawing.Color.SteelBlue;
            this.iP3SubmitBtn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.iP3SubmitBtn.ForeColor = System.Drawing.Color.LightCyan;
            this.iP3SubmitBtn.Location = new System.Drawing.Point(895, 615);
            this.iP3SubmitBtn.Name = "iP3SubmitBtn";
            this.iP3SubmitBtn.Size = new System.Drawing.Size(150, 40);
            this.iP3SubmitBtn.TabIndex = 36;
            this.iP3SubmitBtn.Text = "บันทึกเข้าระบบ";
            this.iP3SubmitBtn.UseCustomBackColor = true;
            this.iP3SubmitBtn.UseCustomForeColor = true;
            this.iP3SubmitBtn.UseSelectable = true;
            this.iP3SubmitBtn.Click += new System.EventHandler(this.IP3SubmitBtn_Click);
            // 
            // iP2Pnl
            // 
            this.iP2Pnl.Controls.Add(this.iP2BcColorLbl);
            this.iP2Pnl.Controls.Add(this.iP2BcColorTxb);
            this.iP2Pnl.Controls.Add(this.iP2IssueProductionItemDgv);
            this.iP2Pnl.Controls.Add(this.iP2SORefTxb);
            this.iP2Pnl.Controls.Add(this.iP2SORefLbl);
            this.iP2Pnl.Controls.Add(this.iP2JobReceiveBtn);
            this.iP2Pnl.Controls.Add(this.iP2ScanBarcodeIssueBtn);
            this.iP2Pnl.Controls.Add(this.iP2OpenQtyUnitLbl);
            this.iP2Pnl.Controls.Add(this.iP2RejectedQtyUnitLbl);
            this.iP2Pnl.Controls.Add(this.iP2RejectedQtyCountTxb);
            this.iP2Pnl.Controls.Add(this.iP2RejectedQtyCountUnitLbl);
            this.iP2Pnl.Controls.Add(this.iP2CompletedQty2Txb);
            this.iP2Pnl.Controls.Add(this.iP2CompletedQtyCountUnitLbl);
            this.iP2Pnl.Controls.Add(this.iP2DueDateTxb);
            this.iP2Pnl.Controls.Add(this.iP2CustomerTxb);
            this.iP2Pnl.Controls.Add(this.iP2RejectedQtyTxb);
            this.iP2Pnl.Controls.Add(this.iP2CompletedQtyCountTxb);
            this.iP2Pnl.Controls.Add(this.iP2OpenQtyTxb);
            this.iP2Pnl.Controls.Add(this.iP2ItemCodeTxb);
            this.iP2Pnl.Controls.Add(this.iP2AssignDateTxb);
            this.iP2Pnl.Controls.Add(this.iP2WhseCbb);
            this.iP2Pnl.Controls.Add(this.iP2OpenQtyLbl);
            this.iP2Pnl.Controls.Add(this.iP2RefNoLbl);
            this.iP2Pnl.Controls.Add(this.iP2CompletedQtyUnitLbl);
            this.iP2Pnl.Controls.Add(this.iP2CompletedQtyTxb);
            this.iP2Pnl.Controls.Add(this.iP2RefNoTxb);
            this.iP2Pnl.Controls.Add(this.iP2CompletedQtyLbl);
            this.iP2Pnl.Controls.Add(this.iP2JobOrderNoTxb);
            this.iP2Pnl.Controls.Add(this.iP2RejectedQtyLbl);
            this.iP2Pnl.Controls.Add(this.iP2ItemCodeLbl);
            this.iP2Pnl.Controls.Add(this.iP2PlannedQtyLbl);
            this.iP2Pnl.Controls.Add(this.iP2CompletedQty2UnitLbl);
            this.iP2Pnl.Controls.Add(this.iP2ItemDescLbl);
            this.iP2Pnl.Controls.Add(this.iP2ItemDescTxb);
            this.iP2Pnl.Controls.Add(this.iP2PlannedQtyUnitLbl);
            this.iP2Pnl.Controls.Add(this.iP2PlannedQtyTxb);
            this.iP2Pnl.Controls.Add(this.iP2WhseLbl);
            this.iP2Pnl.Controls.Add(this.iP2DueDateLbl);
            this.iP2Pnl.Controls.Add(this.iP2CustomerLbl);
            this.iP2Pnl.Controls.Add(this.iP2BackBtn);
            this.iP2Pnl.Controls.Add(this.iP2PrintFormBtn);
            this.iP2Pnl.Controls.Add(this.iP2JobOrderNoLbl);
            this.iP2Pnl.Controls.Add(this.iP2AssignDateLbl);
            this.iP2Pnl.Controls.Add(this.iP2TopLbl);
            this.iP2Pnl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.iP2Pnl.HorizontalScrollbarBarColor = true;
            this.iP2Pnl.HorizontalScrollbarHighlightOnWheel = false;
            this.iP2Pnl.HorizontalScrollbarSize = 10;
            this.iP2Pnl.Location = new System.Drawing.Point(0, 0);
            this.iP2Pnl.Name = "iP2Pnl";
            this.iP2Pnl.Size = new System.Drawing.Size(1238, 663);
            this.iP2Pnl.TabIndex = 34;
            this.iP2Pnl.Tag = " ";
            this.iP2Pnl.VerticalScrollbarBarColor = true;
            this.iP2Pnl.VerticalScrollbarHighlightOnWheel = false;
            this.iP2Pnl.VerticalScrollbarSize = 10;
            // 
            // iP2BcColorLbl
            // 
            this.iP2BcColorLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP2BcColorLbl.AutoSize = true;
            this.iP2BcColorLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP2BcColorLbl.Location = new System.Drawing.Point(1, 314);
            this.iP2BcColorLbl.Name = "iP2BcColorLbl";
            this.iP2BcColorLbl.Size = new System.Drawing.Size(52, 25);
            this.iP2BcColorLbl.TabIndex = 101;
            this.iP2BcColorLbl.Text = "สี Tag";
            // 
            // iP2BcColorTxb
            // 
            this.iP2BcColorTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.iP2BcColorTxb.CustomButton.Image = null;
            this.iP2BcColorTxb.CustomButton.Location = new System.Drawing.Point(246, 2);
            this.iP2BcColorTxb.CustomButton.Name = "";
            this.iP2BcColorTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.iP2BcColorTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.iP2BcColorTxb.CustomButton.TabIndex = 1;
            this.iP2BcColorTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.iP2BcColorTxb.CustomButton.UseSelectable = true;
            this.iP2BcColorTxb.CustomButton.Visible = false;
            this.iP2BcColorTxb.Enabled = false;
            this.iP2BcColorTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.iP2BcColorTxb.ForeColor = System.Drawing.Color.Gray;
            this.iP2BcColorTxb.Lines = new string[0];
            this.iP2BcColorTxb.Location = new System.Drawing.Point(168, 313);
            this.iP2BcColorTxb.MaxLength = 32767;
            this.iP2BcColorTxb.Name = "iP2BcColorTxb";
            this.iP2BcColorTxb.PasswordChar = '\0';
            this.iP2BcColorTxb.ReadOnly = true;
            this.iP2BcColorTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.iP2BcColorTxb.SelectedText = "";
            this.iP2BcColorTxb.SelectionLength = 0;
            this.iP2BcColorTxb.SelectionStart = 0;
            this.iP2BcColorTxb.ShortcutsEnabled = true;
            this.iP2BcColorTxb.Size = new System.Drawing.Size(274, 30);
            this.iP2BcColorTxb.TabIndex = 100;
            this.iP2BcColorTxb.Tag = "";
            this.iP2BcColorTxb.UseCustomForeColor = true;
            this.iP2BcColorTxb.UseSelectable = true;
            this.iP2BcColorTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.iP2BcColorTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // iP2IssueProductionItemDgv
            // 
            this.iP2IssueProductionItemDgv.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.iP2IssueProductionItemDgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.iP2IssueProductionItemDgv.BackgroundColor = System.Drawing.SystemColors.Window;
            this.iP2IssueProductionItemDgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.iP2IssueProductionItemDgv.Location = new System.Drawing.Point(1, 359);
            this.iP2IssueProductionItemDgv.Name = "iP2IssueProductionItemDgv";
            this.iP2IssueProductionItemDgv.RowHeadersWidth = 62;
            this.iP2IssueProductionItemDgv.Size = new System.Drawing.Size(1235, 239);
            this.iP2IssueProductionItemDgv.TabIndex = 99;
            // 
            // iP2SORefTxb
            // 
            this.iP2SORefTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.iP2SORefTxb.CustomButton.Image = null;
            this.iP2SORefTxb.CustomButton.Location = new System.Drawing.Point(178, 2);
            this.iP2SORefTxb.CustomButton.Name = "";
            this.iP2SORefTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.iP2SORefTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.iP2SORefTxb.CustomButton.TabIndex = 1;
            this.iP2SORefTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.iP2SORefTxb.CustomButton.UseSelectable = true;
            this.iP2SORefTxb.CustomButton.Visible = false;
            this.iP2SORefTxb.Enabled = false;
            this.iP2SORefTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.iP2SORefTxb.ForeColor = System.Drawing.Color.Gray;
            this.iP2SORefTxb.Lines = new string[0];
            this.iP2SORefTxb.Location = new System.Drawing.Point(720, 313);
            this.iP2SORefTxb.MaxLength = 32767;
            this.iP2SORefTxb.Name = "iP2SORefTxb";
            this.iP2SORefTxb.PasswordChar = '\0';
            this.iP2SORefTxb.ReadOnly = true;
            this.iP2SORefTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.iP2SORefTxb.SelectedText = "";
            this.iP2SORefTxb.SelectionLength = 0;
            this.iP2SORefTxb.SelectionStart = 0;
            this.iP2SORefTxb.ShortcutsEnabled = true;
            this.iP2SORefTxb.Size = new System.Drawing.Size(206, 30);
            this.iP2SORefTxb.TabIndex = 98;
            this.iP2SORefTxb.Tag = "";
            this.iP2SORefTxb.UseCustomForeColor = true;
            this.iP2SORefTxb.UseSelectable = true;
            this.iP2SORefTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.iP2SORefTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // iP2SORefLbl
            // 
            this.iP2SORefLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP2SORefLbl.AutoSize = true;
            this.iP2SORefLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP2SORefLbl.Location = new System.Drawing.Point(547, 315);
            this.iP2SORefLbl.Name = "iP2SORefLbl";
            this.iP2SORefLbl.Size = new System.Drawing.Size(136, 25);
            this.iP2SORefLbl.TabIndex = 97;
            this.iP2SORefLbl.Text = "Sale order อ้างอิง";
            // 
            // iP2JobReceiveBtn
            // 
            this.iP2JobReceiveBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.iP2JobReceiveBtn.BackColor = System.Drawing.Color.MediumPurple;
            this.iP2JobReceiveBtn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.iP2JobReceiveBtn.ForeColor = System.Drawing.Color.Lavender;
            this.iP2JobReceiveBtn.Location = new System.Drawing.Point(890, 615);
            this.iP2JobReceiveBtn.Name = "iP2JobReceiveBtn";
            this.iP2JobReceiveBtn.Size = new System.Drawing.Size(150, 40);
            this.iP2JobReceiveBtn.TabIndex = 96;
            this.iP2JobReceiveBtn.Text = "รับสินค้าผลิตเสร็จ";
            this.iP2JobReceiveBtn.UseCustomBackColor = true;
            this.iP2JobReceiveBtn.UseCustomForeColor = true;
            this.iP2JobReceiveBtn.UseSelectable = true;
            this.iP2JobReceiveBtn.Click += new System.EventHandler(this.IP2JobReceiveBtn_Click);
            // 
            // iP2ScanBarcodeIssueBtn
            // 
            this.iP2ScanBarcodeIssueBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.iP2ScanBarcodeIssueBtn.BackColor = System.Drawing.Color.MediumPurple;
            this.iP2ScanBarcodeIssueBtn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.iP2ScanBarcodeIssueBtn.ForeColor = System.Drawing.Color.Lavender;
            this.iP2ScanBarcodeIssueBtn.Location = new System.Drawing.Point(689, 615);
            this.iP2ScanBarcodeIssueBtn.Name = "iP2ScanBarcodeIssueBtn";
            this.iP2ScanBarcodeIssueBtn.Size = new System.Drawing.Size(150, 40);
            this.iP2ScanBarcodeIssueBtn.TabIndex = 95;
            this.iP2ScanBarcodeIssueBtn.Text = "เบิกวัตถุดิบ";
            this.iP2ScanBarcodeIssueBtn.UseCustomBackColor = true;
            this.iP2ScanBarcodeIssueBtn.UseCustomForeColor = true;
            this.iP2ScanBarcodeIssueBtn.UseSelectable = true;
            this.iP2ScanBarcodeIssueBtn.Click += new System.EventHandler(this.IP2ScanBarcodeIssueBtn_Click);
            // 
            // iP2OpenQtyUnitLbl
            // 
            this.iP2OpenQtyUnitLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP2OpenQtyUnitLbl.AutoSize = true;
            this.iP2OpenQtyUnitLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP2OpenQtyUnitLbl.Location = new System.Drawing.Point(841, 276);
            this.iP2OpenQtyUnitLbl.Name = "iP2OpenQtyUnitLbl";
            this.iP2OpenQtyUnitLbl.Size = new System.Drawing.Size(36, 25);
            this.iP2OpenQtyUnitLbl.TabIndex = 91;
            this.iP2OpenQtyUnitLbl.Text = "กก.";
            // 
            // iP2RejectedQtyUnitLbl
            // 
            this.iP2RejectedQtyUnitLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP2RejectedQtyUnitLbl.AutoSize = true;
            this.iP2RejectedQtyUnitLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP2RejectedQtyUnitLbl.Location = new System.Drawing.Point(841, 239);
            this.iP2RejectedQtyUnitLbl.Name = "iP2RejectedQtyUnitLbl";
            this.iP2RejectedQtyUnitLbl.Size = new System.Drawing.Size(36, 25);
            this.iP2RejectedQtyUnitLbl.TabIndex = 90;
            this.iP2RejectedQtyUnitLbl.Text = "กก.";
            // 
            // iP2RejectedQtyCountTxb
            // 
            this.iP2RejectedQtyCountTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.iP2RejectedQtyCountTxb.CustomButton.Image = null;
            this.iP2RejectedQtyCountTxb.CustomButton.Location = new System.Drawing.Point(43, 2);
            this.iP2RejectedQtyCountTxb.CustomButton.Name = "";
            this.iP2RejectedQtyCountTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.iP2RejectedQtyCountTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.iP2RejectedQtyCountTxb.CustomButton.TabIndex = 1;
            this.iP2RejectedQtyCountTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.iP2RejectedQtyCountTxb.CustomButton.UseSelectable = true;
            this.iP2RejectedQtyCountTxb.CustomButton.Visible = false;
            this.iP2RejectedQtyCountTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.iP2RejectedQtyCountTxb.ForeColor = System.Drawing.Color.Gray;
            this.iP2RejectedQtyCountTxb.Lines = new string[0];
            this.iP2RejectedQtyCountTxb.Location = new System.Drawing.Point(927, 237);
            this.iP2RejectedQtyCountTxb.MaxLength = 32767;
            this.iP2RejectedQtyCountTxb.Name = "iP2RejectedQtyCountTxb";
            this.iP2RejectedQtyCountTxb.PasswordChar = '\0';
            this.iP2RejectedQtyCountTxb.ReadOnly = true;
            this.iP2RejectedQtyCountTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.iP2RejectedQtyCountTxb.SelectedText = "";
            this.iP2RejectedQtyCountTxb.SelectionLength = 0;
            this.iP2RejectedQtyCountTxb.SelectionStart = 0;
            this.iP2RejectedQtyCountTxb.ShortcutsEnabled = true;
            this.iP2RejectedQtyCountTxb.Size = new System.Drawing.Size(71, 30);
            this.iP2RejectedQtyCountTxb.TabIndex = 89;
            this.iP2RejectedQtyCountTxb.UseCustomForeColor = true;
            this.iP2RejectedQtyCountTxb.UseSelectable = true;
            this.iP2RejectedQtyCountTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.iP2RejectedQtyCountTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // iP2RejectedQtyCountUnitLbl
            // 
            this.iP2RejectedQtyCountUnitLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP2RejectedQtyCountUnitLbl.AutoSize = true;
            this.iP2RejectedQtyCountUnitLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP2RejectedQtyCountUnitLbl.Location = new System.Drawing.Point(1007, 238);
            this.iP2RejectedQtyCountUnitLbl.Name = "iP2RejectedQtyCountUnitLbl";
            this.iP2RejectedQtyCountUnitLbl.Size = new System.Drawing.Size(66, 25);
            this.iP2RejectedQtyCountUnitLbl.TabIndex = 88;
            this.iP2RejectedQtyCountUnitLbl.Text = "กระสอบ";
            // 
            // iP2CompletedQty2Txb
            // 
            this.iP2CompletedQty2Txb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.iP2CompletedQty2Txb.CustomButton.Image = null;
            this.iP2CompletedQty2Txb.CustomButton.Location = new System.Drawing.Point(85, 2);
            this.iP2CompletedQty2Txb.CustomButton.Name = "";
            this.iP2CompletedQty2Txb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.iP2CompletedQty2Txb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.iP2CompletedQty2Txb.CustomButton.TabIndex = 1;
            this.iP2CompletedQty2Txb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.iP2CompletedQty2Txb.CustomButton.UseSelectable = true;
            this.iP2CompletedQty2Txb.CustomButton.Visible = false;
            this.iP2CompletedQty2Txb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.iP2CompletedQty2Txb.ForeColor = System.Drawing.Color.Gray;
            this.iP2CompletedQty2Txb.Lines = new string[0];
            this.iP2CompletedQty2Txb.Location = new System.Drawing.Point(720, 201);
            this.iP2CompletedQty2Txb.MaxLength = 32767;
            this.iP2CompletedQty2Txb.Name = "iP2CompletedQty2Txb";
            this.iP2CompletedQty2Txb.PasswordChar = '\0';
            this.iP2CompletedQty2Txb.ReadOnly = true;
            this.iP2CompletedQty2Txb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.iP2CompletedQty2Txb.SelectedText = "";
            this.iP2CompletedQty2Txb.SelectionLength = 0;
            this.iP2CompletedQty2Txb.SelectionStart = 0;
            this.iP2CompletedQty2Txb.ShortcutsEnabled = true;
            this.iP2CompletedQty2Txb.Size = new System.Drawing.Size(113, 30);
            this.iP2CompletedQty2Txb.TabIndex = 87;
            this.iP2CompletedQty2Txb.UseCustomForeColor = true;
            this.iP2CompletedQty2Txb.UseSelectable = true;
            this.iP2CompletedQty2Txb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.iP2CompletedQty2Txb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // iP2CompletedQtyCountUnitLbl
            // 
            this.iP2CompletedQtyCountUnitLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP2CompletedQtyCountUnitLbl.AutoSize = true;
            this.iP2CompletedQtyCountUnitLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP2CompletedQtyCountUnitLbl.Location = new System.Drawing.Point(1007, 165);
            this.iP2CompletedQtyCountUnitLbl.Name = "iP2CompletedQtyCountUnitLbl";
            this.iP2CompletedQtyCountUnitLbl.Size = new System.Drawing.Size(66, 25);
            this.iP2CompletedQtyCountUnitLbl.TabIndex = 86;
            this.iP2CompletedQtyCountUnitLbl.Text = "กระสอบ";
            // 
            // iP2DueDateTxb
            // 
            this.iP2DueDateTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.iP2DueDateTxb.CustomButton.Image = null;
            this.iP2DueDateTxb.CustomButton.Location = new System.Drawing.Point(246, 2);
            this.iP2DueDateTxb.CustomButton.Name = "";
            this.iP2DueDateTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.iP2DueDateTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.iP2DueDateTxb.CustomButton.TabIndex = 1;
            this.iP2DueDateTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.iP2DueDateTxb.CustomButton.UseSelectable = true;
            this.iP2DueDateTxb.CustomButton.Visible = false;
            this.iP2DueDateTxb.Enabled = false;
            this.iP2DueDateTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.iP2DueDateTxb.ForeColor = System.Drawing.Color.Gray;
            this.iP2DueDateTxb.Lines = new string[0];
            this.iP2DueDateTxb.Location = new System.Drawing.Point(168, 275);
            this.iP2DueDateTxb.MaxLength = 32767;
            this.iP2DueDateTxb.Name = "iP2DueDateTxb";
            this.iP2DueDateTxb.PasswordChar = '\0';
            this.iP2DueDateTxb.ReadOnly = true;
            this.iP2DueDateTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.iP2DueDateTxb.SelectedText = "";
            this.iP2DueDateTxb.SelectionLength = 0;
            this.iP2DueDateTxb.SelectionStart = 0;
            this.iP2DueDateTxb.ShortcutsEnabled = true;
            this.iP2DueDateTxb.Size = new System.Drawing.Size(274, 30);
            this.iP2DueDateTxb.TabIndex = 85;
            this.iP2DueDateTxb.Tag = "";
            this.iP2DueDateTxb.UseCustomForeColor = true;
            this.iP2DueDateTxb.UseSelectable = true;
            this.iP2DueDateTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.iP2DueDateTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // iP2CustomerTxb
            // 
            this.iP2CustomerTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.iP2CustomerTxb.CustomButton.Image = null;
            this.iP2CustomerTxb.CustomButton.Location = new System.Drawing.Point(247, 2);
            this.iP2CustomerTxb.CustomButton.Name = "";
            this.iP2CustomerTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.iP2CustomerTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.iP2CustomerTxb.CustomButton.TabIndex = 1;
            this.iP2CustomerTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.iP2CustomerTxb.CustomButton.UseSelectable = true;
            this.iP2CustomerTxb.CustomButton.Visible = false;
            this.iP2CustomerTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.iP2CustomerTxb.ForeColor = System.Drawing.Color.Gray;
            this.iP2CustomerTxb.Lines = new string[0];
            this.iP2CustomerTxb.Location = new System.Drawing.Point(169, 125);
            this.iP2CustomerTxb.MaxLength = 32767;
            this.iP2CustomerTxb.Name = "iP2CustomerTxb";
            this.iP2CustomerTxb.PasswordChar = '\0';
            this.iP2CustomerTxb.ReadOnly = true;
            this.iP2CustomerTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.iP2CustomerTxb.SelectedText = "";
            this.iP2CustomerTxb.SelectionLength = 0;
            this.iP2CustomerTxb.SelectionStart = 0;
            this.iP2CustomerTxb.ShortcutsEnabled = true;
            this.iP2CustomerTxb.Size = new System.Drawing.Size(275, 30);
            this.iP2CustomerTxb.TabIndex = 84;
            this.iP2CustomerTxb.UseCustomForeColor = true;
            this.iP2CustomerTxb.UseSelectable = true;
            this.iP2CustomerTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.iP2CustomerTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // iP2RejectedQtyTxb
            // 
            this.iP2RejectedQtyTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.iP2RejectedQtyTxb.CustomButton.Image = null;
            this.iP2RejectedQtyTxb.CustomButton.Location = new System.Drawing.Point(85, 2);
            this.iP2RejectedQtyTxb.CustomButton.Name = "";
            this.iP2RejectedQtyTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.iP2RejectedQtyTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.iP2RejectedQtyTxb.CustomButton.TabIndex = 1;
            this.iP2RejectedQtyTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.iP2RejectedQtyTxb.CustomButton.UseSelectable = true;
            this.iP2RejectedQtyTxb.CustomButton.Visible = false;
            this.iP2RejectedQtyTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.iP2RejectedQtyTxb.ForeColor = System.Drawing.Color.Gray;
            this.iP2RejectedQtyTxb.Lines = new string[0];
            this.iP2RejectedQtyTxb.Location = new System.Drawing.Point(720, 238);
            this.iP2RejectedQtyTxb.MaxLength = 32767;
            this.iP2RejectedQtyTxb.Name = "iP2RejectedQtyTxb";
            this.iP2RejectedQtyTxb.PasswordChar = '\0';
            this.iP2RejectedQtyTxb.ReadOnly = true;
            this.iP2RejectedQtyTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.iP2RejectedQtyTxb.SelectedText = "";
            this.iP2RejectedQtyTxb.SelectionLength = 0;
            this.iP2RejectedQtyTxb.SelectionStart = 0;
            this.iP2RejectedQtyTxb.ShortcutsEnabled = true;
            this.iP2RejectedQtyTxb.Size = new System.Drawing.Size(113, 30);
            this.iP2RejectedQtyTxb.TabIndex = 83;
            this.iP2RejectedQtyTxb.UseCustomForeColor = true;
            this.iP2RejectedQtyTxb.UseSelectable = true;
            this.iP2RejectedQtyTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.iP2RejectedQtyTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // iP2CompletedQtyCountTxb
            // 
            this.iP2CompletedQtyCountTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.iP2CompletedQtyCountTxb.CustomButton.Image = null;
            this.iP2CompletedQtyCountTxb.CustomButton.Location = new System.Drawing.Point(43, 2);
            this.iP2CompletedQtyCountTxb.CustomButton.Name = "";
            this.iP2CompletedQtyCountTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.iP2CompletedQtyCountTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.iP2CompletedQtyCountTxb.CustomButton.TabIndex = 1;
            this.iP2CompletedQtyCountTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.iP2CompletedQtyCountTxb.CustomButton.UseSelectable = true;
            this.iP2CompletedQtyCountTxb.CustomButton.Visible = false;
            this.iP2CompletedQtyCountTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.iP2CompletedQtyCountTxb.ForeColor = System.Drawing.Color.Gray;
            this.iP2CompletedQtyCountTxb.Lines = new string[0];
            this.iP2CompletedQtyCountTxb.Location = new System.Drawing.Point(927, 163);
            this.iP2CompletedQtyCountTxb.MaxLength = 32767;
            this.iP2CompletedQtyCountTxb.Name = "iP2CompletedQtyCountTxb";
            this.iP2CompletedQtyCountTxb.PasswordChar = '\0';
            this.iP2CompletedQtyCountTxb.ReadOnly = true;
            this.iP2CompletedQtyCountTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.iP2CompletedQtyCountTxb.SelectedText = "";
            this.iP2CompletedQtyCountTxb.SelectionLength = 0;
            this.iP2CompletedQtyCountTxb.SelectionStart = 0;
            this.iP2CompletedQtyCountTxb.ShortcutsEnabled = true;
            this.iP2CompletedQtyCountTxb.Size = new System.Drawing.Size(71, 30);
            this.iP2CompletedQtyCountTxb.TabIndex = 82;
            this.iP2CompletedQtyCountTxb.UseCustomForeColor = true;
            this.iP2CompletedQtyCountTxb.UseSelectable = true;
            this.iP2CompletedQtyCountTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.iP2CompletedQtyCountTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // iP2OpenQtyTxb
            // 
            this.iP2OpenQtyTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.iP2OpenQtyTxb.CustomButton.Image = null;
            this.iP2OpenQtyTxb.CustomButton.Location = new System.Drawing.Point(85, 2);
            this.iP2OpenQtyTxb.CustomButton.Name = "";
            this.iP2OpenQtyTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.iP2OpenQtyTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.iP2OpenQtyTxb.CustomButton.TabIndex = 1;
            this.iP2OpenQtyTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.iP2OpenQtyTxb.CustomButton.UseSelectable = true;
            this.iP2OpenQtyTxb.CustomButton.Visible = false;
            this.iP2OpenQtyTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.iP2OpenQtyTxb.ForeColor = System.Drawing.Color.Gray;
            this.iP2OpenQtyTxb.Lines = new string[0];
            this.iP2OpenQtyTxb.Location = new System.Drawing.Point(720, 275);
            this.iP2OpenQtyTxb.MaxLength = 32767;
            this.iP2OpenQtyTxb.Name = "iP2OpenQtyTxb";
            this.iP2OpenQtyTxb.PasswordChar = '\0';
            this.iP2OpenQtyTxb.ReadOnly = true;
            this.iP2OpenQtyTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.iP2OpenQtyTxb.SelectedText = "";
            this.iP2OpenQtyTxb.SelectionLength = 0;
            this.iP2OpenQtyTxb.SelectionStart = 0;
            this.iP2OpenQtyTxb.ShortcutsEnabled = true;
            this.iP2OpenQtyTxb.Size = new System.Drawing.Size(113, 30);
            this.iP2OpenQtyTxb.TabIndex = 80;
            this.iP2OpenQtyTxb.UseCustomForeColor = true;
            this.iP2OpenQtyTxb.UseSelectable = true;
            this.iP2OpenQtyTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.iP2OpenQtyTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // iP2ItemCodeTxb
            // 
            this.iP2ItemCodeTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.iP2ItemCodeTxb.CustomButton.Image = null;
            this.iP2ItemCodeTxb.CustomButton.Location = new System.Drawing.Point(247, 2);
            this.iP2ItemCodeTxb.CustomButton.Name = "";
            this.iP2ItemCodeTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.iP2ItemCodeTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.iP2ItemCodeTxb.CustomButton.TabIndex = 1;
            this.iP2ItemCodeTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.iP2ItemCodeTxb.CustomButton.UseSelectable = true;
            this.iP2ItemCodeTxb.CustomButton.Visible = false;
            this.iP2ItemCodeTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.iP2ItemCodeTxb.ForeColor = System.Drawing.Color.Gray;
            this.iP2ItemCodeTxb.Lines = new string[0];
            this.iP2ItemCodeTxb.Location = new System.Drawing.Point(169, 163);
            this.iP2ItemCodeTxb.MaxLength = 32767;
            this.iP2ItemCodeTxb.Name = "iP2ItemCodeTxb";
            this.iP2ItemCodeTxb.PasswordChar = '\0';
            this.iP2ItemCodeTxb.ReadOnly = true;
            this.iP2ItemCodeTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.iP2ItemCodeTxb.SelectedText = "";
            this.iP2ItemCodeTxb.SelectionLength = 0;
            this.iP2ItemCodeTxb.SelectionStart = 0;
            this.iP2ItemCodeTxb.ShortcutsEnabled = true;
            this.iP2ItemCodeTxb.Size = new System.Drawing.Size(275, 30);
            this.iP2ItemCodeTxb.TabIndex = 79;
            this.iP2ItemCodeTxb.UseCustomForeColor = true;
            this.iP2ItemCodeTxb.UseSelectable = true;
            this.iP2ItemCodeTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.iP2ItemCodeTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // iP2AssignDateTxb
            // 
            this.iP2AssignDateTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.iP2AssignDateTxb.CustomButton.Image = null;
            this.iP2AssignDateTxb.CustomButton.Location = new System.Drawing.Point(157, 2);
            this.iP2AssignDateTxb.CustomButton.Name = "";
            this.iP2AssignDateTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.iP2AssignDateTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.iP2AssignDateTxb.CustomButton.TabIndex = 1;
            this.iP2AssignDateTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.iP2AssignDateTxb.CustomButton.UseSelectable = true;
            this.iP2AssignDateTxb.CustomButton.Visible = false;
            this.iP2AssignDateTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.iP2AssignDateTxb.ForeColor = System.Drawing.Color.Gray;
            this.iP2AssignDateTxb.Lines = new string[0];
            this.iP2AssignDateTxb.Location = new System.Drawing.Point(1051, 56);
            this.iP2AssignDateTxb.MaxLength = 32767;
            this.iP2AssignDateTxb.Name = "iP2AssignDateTxb";
            this.iP2AssignDateTxb.PasswordChar = '\0';
            this.iP2AssignDateTxb.ReadOnly = true;
            this.iP2AssignDateTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.iP2AssignDateTxb.SelectedText = "";
            this.iP2AssignDateTxb.SelectionLength = 0;
            this.iP2AssignDateTxb.SelectionStart = 0;
            this.iP2AssignDateTxb.ShortcutsEnabled = true;
            this.iP2AssignDateTxb.Size = new System.Drawing.Size(185, 30);
            this.iP2AssignDateTxb.TabIndex = 78;
            this.iP2AssignDateTxb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.iP2AssignDateTxb.UseCustomForeColor = true;
            this.iP2AssignDateTxb.UseSelectable = true;
            this.iP2AssignDateTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.iP2AssignDateTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // iP2WhseCbb
            // 
            this.iP2WhseCbb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP2WhseCbb.Enabled = false;
            this.iP2WhseCbb.FormattingEnabled = true;
            this.iP2WhseCbb.ItemHeight = 23;
            this.iP2WhseCbb.Location = new System.Drawing.Point(169, 239);
            this.iP2WhseCbb.Name = "iP2WhseCbb";
            this.iP2WhseCbb.Size = new System.Drawing.Size(130, 29);
            this.iP2WhseCbb.TabIndex = 73;
            this.iP2WhseCbb.UseSelectable = true;
            // 
            // iP2OpenQtyLbl
            // 
            this.iP2OpenQtyLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP2OpenQtyLbl.AutoSize = true;
            this.iP2OpenQtyLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP2OpenQtyLbl.Location = new System.Drawing.Point(547, 277);
            this.iP2OpenQtyLbl.Name = "iP2OpenQtyLbl";
            this.iP2OpenQtyLbl.Size = new System.Drawing.Size(152, 25);
            this.iP2OpenQtyLbl.TabIndex = 72;
            this.iP2OpenQtyLbl.Text = "จำนวนที่เปิดการผลิต";
            // 
            // iP2RefNoLbl
            // 
            this.iP2RefNoLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP2RefNoLbl.AutoSize = true;
            this.iP2RefNoLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP2RefNoLbl.Location = new System.Drawing.Point(908, 89);
            this.iP2RefNoLbl.Name = "iP2RefNoLbl";
            this.iP2RefNoLbl.Size = new System.Drawing.Size(118, 25);
            this.iP2RefNoLbl.TabIndex = 54;
            this.iP2RefNoLbl.Text = "หมายเลขอ้างอิง";
            // 
            // iP2CompletedQtyUnitLbl
            // 
            this.iP2CompletedQtyUnitLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP2CompletedQtyUnitLbl.AutoSize = true;
            this.iP2CompletedQtyUnitLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP2CompletedQtyUnitLbl.Location = new System.Drawing.Point(841, 163);
            this.iP2CompletedQtyUnitLbl.Name = "iP2CompletedQtyUnitLbl";
            this.iP2CompletedQtyUnitLbl.Size = new System.Drawing.Size(36, 25);
            this.iP2CompletedQtyUnitLbl.TabIndex = 71;
            this.iP2CompletedQtyUnitLbl.Text = "กก.";
            // 
            // iP2CompletedQtyTxb
            // 
            this.iP2CompletedQtyTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.iP2CompletedQtyTxb.CustomButton.Image = null;
            this.iP2CompletedQtyTxb.CustomButton.Location = new System.Drawing.Point(85, 2);
            this.iP2CompletedQtyTxb.CustomButton.Name = "";
            this.iP2CompletedQtyTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.iP2CompletedQtyTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.iP2CompletedQtyTxb.CustomButton.TabIndex = 1;
            this.iP2CompletedQtyTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.iP2CompletedQtyTxb.CustomButton.UseSelectable = true;
            this.iP2CompletedQtyTxb.CustomButton.Visible = false;
            this.iP2CompletedQtyTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.iP2CompletedQtyTxb.ForeColor = System.Drawing.Color.Gray;
            this.iP2CompletedQtyTxb.Lines = new string[0];
            this.iP2CompletedQtyTxb.Location = new System.Drawing.Point(720, 163);
            this.iP2CompletedQtyTxb.MaxLength = 32767;
            this.iP2CompletedQtyTxb.Name = "iP2CompletedQtyTxb";
            this.iP2CompletedQtyTxb.PasswordChar = '\0';
            this.iP2CompletedQtyTxb.ReadOnly = true;
            this.iP2CompletedQtyTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.iP2CompletedQtyTxb.SelectedText = "";
            this.iP2CompletedQtyTxb.SelectionLength = 0;
            this.iP2CompletedQtyTxb.SelectionStart = 0;
            this.iP2CompletedQtyTxb.ShortcutsEnabled = true;
            this.iP2CompletedQtyTxb.Size = new System.Drawing.Size(113, 30);
            this.iP2CompletedQtyTxb.TabIndex = 70;
            this.iP2CompletedQtyTxb.UseCustomForeColor = true;
            this.iP2CompletedQtyTxb.UseSelectable = true;
            this.iP2CompletedQtyTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.iP2CompletedQtyTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // iP2RefNoTxb
            // 
            this.iP2RefNoTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.iP2RefNoTxb.CustomButton.Image = null;
            this.iP2RefNoTxb.CustomButton.Location = new System.Drawing.Point(157, 2);
            this.iP2RefNoTxb.CustomButton.Name = "";
            this.iP2RefNoTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.iP2RefNoTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.iP2RefNoTxb.CustomButton.TabIndex = 1;
            this.iP2RefNoTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.iP2RefNoTxb.CustomButton.UseSelectable = true;
            this.iP2RefNoTxb.CustomButton.Visible = false;
            this.iP2RefNoTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.iP2RefNoTxb.ForeColor = System.Drawing.Color.Gray;
            this.iP2RefNoTxb.Lines = new string[0];
            this.iP2RefNoTxb.Location = new System.Drawing.Point(1051, 89);
            this.iP2RefNoTxb.MaxLength = 32767;
            this.iP2RefNoTxb.Name = "iP2RefNoTxb";
            this.iP2RefNoTxb.PasswordChar = '\0';
            this.iP2RefNoTxb.ReadOnly = true;
            this.iP2RefNoTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.iP2RefNoTxb.SelectedText = "";
            this.iP2RefNoTxb.SelectionLength = 0;
            this.iP2RefNoTxb.SelectionStart = 0;
            this.iP2RefNoTxb.ShortcutsEnabled = true;
            this.iP2RefNoTxb.Size = new System.Drawing.Size(185, 30);
            this.iP2RefNoTxb.TabIndex = 55;
            this.iP2RefNoTxb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.iP2RefNoTxb.UseCustomForeColor = true;
            this.iP2RefNoTxb.UseSelectable = true;
            this.iP2RefNoTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.iP2RefNoTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // iP2CompletedQtyLbl
            // 
            this.iP2CompletedQtyLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP2CompletedQtyLbl.AutoSize = true;
            this.iP2CompletedQtyLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP2CompletedQtyLbl.Location = new System.Drawing.Point(547, 165);
            this.iP2CompletedQtyLbl.Name = "iP2CompletedQtyLbl";
            this.iP2CompletedQtyLbl.Size = new System.Drawing.Size(133, 25);
            this.iP2CompletedQtyLbl.TabIndex = 69;
            this.iP2CompletedQtyLbl.Text = "จำนวนที่ผลิตเสร็จ";
            // 
            // iP2JobOrderNoTxb
            // 
            this.iP2JobOrderNoTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.iP2JobOrderNoTxb.CustomButton.Image = null;
            this.iP2JobOrderNoTxb.CustomButton.Location = new System.Drawing.Point(157, 2);
            this.iP2JobOrderNoTxb.CustomButton.Name = "";
            this.iP2JobOrderNoTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.iP2JobOrderNoTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.iP2JobOrderNoTxb.CustomButton.TabIndex = 1;
            this.iP2JobOrderNoTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.iP2JobOrderNoTxb.CustomButton.UseSelectable = true;
            this.iP2JobOrderNoTxb.CustomButton.Visible = false;
            this.iP2JobOrderNoTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.iP2JobOrderNoTxb.ForeColor = System.Drawing.Color.Gray;
            this.iP2JobOrderNoTxb.Lines = new string[0];
            this.iP2JobOrderNoTxb.Location = new System.Drawing.Point(1051, 23);
            this.iP2JobOrderNoTxb.MaxLength = 32767;
            this.iP2JobOrderNoTxb.Name = "iP2JobOrderNoTxb";
            this.iP2JobOrderNoTxb.PasswordChar = '\0';
            this.iP2JobOrderNoTxb.ReadOnly = true;
            this.iP2JobOrderNoTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.iP2JobOrderNoTxb.SelectedText = "";
            this.iP2JobOrderNoTxb.SelectionLength = 0;
            this.iP2JobOrderNoTxb.SelectionStart = 0;
            this.iP2JobOrderNoTxb.ShortcutsEnabled = true;
            this.iP2JobOrderNoTxb.Size = new System.Drawing.Size(185, 30);
            this.iP2JobOrderNoTxb.TabIndex = 56;
            this.iP2JobOrderNoTxb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.iP2JobOrderNoTxb.UseCustomForeColor = true;
            this.iP2JobOrderNoTxb.UseSelectable = true;
            this.iP2JobOrderNoTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.iP2JobOrderNoTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // iP2RejectedQtyLbl
            // 
            this.iP2RejectedQtyLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP2RejectedQtyLbl.AutoSize = true;
            this.iP2RejectedQtyLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP2RejectedQtyLbl.Location = new System.Drawing.Point(547, 239);
            this.iP2RejectedQtyLbl.Name = "iP2RejectedQtyLbl";
            this.iP2RejectedQtyLbl.Size = new System.Drawing.Size(122, 25);
            this.iP2RejectedQtyLbl.TabIndex = 68;
            this.iP2RejectedQtyLbl.Text = "จำนวนที่ใช้ไม่ได้";
            // 
            // iP2ItemCodeLbl
            // 
            this.iP2ItemCodeLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP2ItemCodeLbl.AutoSize = true;
            this.iP2ItemCodeLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP2ItemCodeLbl.Location = new System.Drawing.Point(2, 160);
            this.iP2ItemCodeLbl.Name = "iP2ItemCodeLbl";
            this.iP2ItemCodeLbl.Size = new System.Drawing.Size(80, 25);
            this.iP2ItemCodeLbl.TabIndex = 57;
            this.iP2ItemCodeLbl.Text = "รหัสสินค้า";
            // 
            // iP2PlannedQtyLbl
            // 
            this.iP2PlannedQtyLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP2PlannedQtyLbl.AutoSize = true;
            this.iP2PlannedQtyLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP2PlannedQtyLbl.Location = new System.Drawing.Point(547, 127);
            this.iP2PlannedQtyLbl.Name = "iP2PlannedQtyLbl";
            this.iP2PlannedQtyLbl.Size = new System.Drawing.Size(155, 25);
            this.iP2PlannedQtyLbl.TabIndex = 58;
            this.iP2PlannedQtyLbl.Text = "จำนวนที่วางแผนผลิต";
            // 
            // iP2CompletedQty2UnitLbl
            // 
            this.iP2CompletedQty2UnitLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP2CompletedQty2UnitLbl.AutoSize = true;
            this.iP2CompletedQty2UnitLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP2CompletedQty2UnitLbl.Location = new System.Drawing.Point(841, 203);
            this.iP2CompletedQty2UnitLbl.Name = "iP2CompletedQty2UnitLbl";
            this.iP2CompletedQty2UnitLbl.Size = new System.Drawing.Size(53, 25);
            this.iP2CompletedQty2UnitLbl.TabIndex = 67;
            this.iP2CompletedQty2UnitLbl.Text = "ปอนด์";
            // 
            // iP2ItemDescLbl
            // 
            this.iP2ItemDescLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP2ItemDescLbl.AutoSize = true;
            this.iP2ItemDescLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP2ItemDescLbl.Location = new System.Drawing.Point(2, 198);
            this.iP2ItemDescLbl.Name = "iP2ItemDescLbl";
            this.iP2ItemDescLbl.Size = new System.Drawing.Size(126, 25);
            this.iP2ItemDescLbl.TabIndex = 59;
            this.iP2ItemDescLbl.Text = "รายละเอียดสินค้า";
            // 
            // iP2ItemDescTxb
            // 
            this.iP2ItemDescTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.iP2ItemDescTxb.CustomButton.Image = null;
            this.iP2ItemDescTxb.CustomButton.Location = new System.Drawing.Point(247, 2);
            this.iP2ItemDescTxb.CustomButton.Name = "";
            this.iP2ItemDescTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.iP2ItemDescTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.iP2ItemDescTxb.CustomButton.TabIndex = 1;
            this.iP2ItemDescTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.iP2ItemDescTxb.CustomButton.UseSelectable = true;
            this.iP2ItemDescTxb.CustomButton.Visible = false;
            this.iP2ItemDescTxb.Enabled = false;
            this.iP2ItemDescTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.iP2ItemDescTxb.ForeColor = System.Drawing.Color.Gray;
            this.iP2ItemDescTxb.Lines = new string[0];
            this.iP2ItemDescTxb.Location = new System.Drawing.Point(169, 201);
            this.iP2ItemDescTxb.MaxLength = 32767;
            this.iP2ItemDescTxb.Name = "iP2ItemDescTxb";
            this.iP2ItemDescTxb.PasswordChar = '\0';
            this.iP2ItemDescTxb.ReadOnly = true;
            this.iP2ItemDescTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.iP2ItemDescTxb.SelectedText = "";
            this.iP2ItemDescTxb.SelectionLength = 0;
            this.iP2ItemDescTxb.SelectionStart = 0;
            this.iP2ItemDescTxb.ShortcutsEnabled = true;
            this.iP2ItemDescTxb.Size = new System.Drawing.Size(275, 30);
            this.iP2ItemDescTxb.TabIndex = 60;
            this.iP2ItemDescTxb.Tag = "";
            this.iP2ItemDescTxb.UseCustomForeColor = true;
            this.iP2ItemDescTxb.UseSelectable = true;
            this.iP2ItemDescTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.iP2ItemDescTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // iP2PlannedQtyUnitLbl
            // 
            this.iP2PlannedQtyUnitLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP2PlannedQtyUnitLbl.AutoSize = true;
            this.iP2PlannedQtyUnitLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP2PlannedQtyUnitLbl.Location = new System.Drawing.Point(841, 125);
            this.iP2PlannedQtyUnitLbl.Name = "iP2PlannedQtyUnitLbl";
            this.iP2PlannedQtyUnitLbl.Size = new System.Drawing.Size(36, 25);
            this.iP2PlannedQtyUnitLbl.TabIndex = 66;
            this.iP2PlannedQtyUnitLbl.Text = "กก.";
            // 
            // iP2PlannedQtyTxb
            // 
            this.iP2PlannedQtyTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.iP2PlannedQtyTxb.CustomButton.Image = null;
            this.iP2PlannedQtyTxb.CustomButton.Location = new System.Drawing.Point(85, 2);
            this.iP2PlannedQtyTxb.CustomButton.Name = "";
            this.iP2PlannedQtyTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.iP2PlannedQtyTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.iP2PlannedQtyTxb.CustomButton.TabIndex = 1;
            this.iP2PlannedQtyTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.iP2PlannedQtyTxb.CustomButton.UseSelectable = true;
            this.iP2PlannedQtyTxb.CustomButton.Visible = false;
            this.iP2PlannedQtyTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.iP2PlannedQtyTxb.ForeColor = System.Drawing.Color.Gray;
            this.iP2PlannedQtyTxb.Lines = new string[0];
            this.iP2PlannedQtyTxb.Location = new System.Drawing.Point(720, 125);
            this.iP2PlannedQtyTxb.MaxLength = 32767;
            this.iP2PlannedQtyTxb.Name = "iP2PlannedQtyTxb";
            this.iP2PlannedQtyTxb.PasswordChar = '\0';
            this.iP2PlannedQtyTxb.ReadOnly = true;
            this.iP2PlannedQtyTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.iP2PlannedQtyTxb.SelectedText = "";
            this.iP2PlannedQtyTxb.SelectionLength = 0;
            this.iP2PlannedQtyTxb.SelectionStart = 0;
            this.iP2PlannedQtyTxb.ShortcutsEnabled = true;
            this.iP2PlannedQtyTxb.Size = new System.Drawing.Size(113, 30);
            this.iP2PlannedQtyTxb.TabIndex = 65;
            this.iP2PlannedQtyTxb.UseCustomForeColor = true;
            this.iP2PlannedQtyTxb.UseSelectable = true;
            this.iP2PlannedQtyTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.iP2PlannedQtyTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // iP2WhseLbl
            // 
            this.iP2WhseLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP2WhseLbl.AutoSize = true;
            this.iP2WhseLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP2WhseLbl.Location = new System.Drawing.Point(3, 236);
            this.iP2WhseLbl.Name = "iP2WhseLbl";
            this.iP2WhseLbl.Size = new System.Drawing.Size(101, 25);
            this.iP2WhseLbl.TabIndex = 61;
            this.iP2WhseLbl.Text = "โกดังที่จัดเก็บ";
            // 
            // iP2DueDateLbl
            // 
            this.iP2DueDateLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP2DueDateLbl.AutoSize = true;
            this.iP2DueDateLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP2DueDateLbl.Location = new System.Drawing.Point(1, 272);
            this.iP2DueDateLbl.Name = "iP2DueDateLbl";
            this.iP2DueDateLbl.Size = new System.Drawing.Size(104, 25);
            this.iP2DueDateLbl.TabIndex = 46;
            this.iP2DueDateLbl.Text = "วันส่งมอบงาน";
            // 
            // iP2CustomerLbl
            // 
            this.iP2CustomerLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP2CustomerLbl.AutoSize = true;
            this.iP2CustomerLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP2CustomerLbl.Location = new System.Drawing.Point(3, 122);
            this.iP2CustomerLbl.Name = "iP2CustomerLbl";
            this.iP2CustomerLbl.Size = new System.Drawing.Size(50, 25);
            this.iP2CustomerLbl.TabIndex = 42;
            this.iP2CustomerLbl.Text = "ลูกค้า";
            // 
            // iP2BackBtn
            // 
            this.iP2BackBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.iP2BackBtn.BackColor = System.Drawing.Color.IndianRed;
            this.iP2BackBtn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.iP2BackBtn.ForeColor = System.Drawing.Color.MistyRose;
            this.iP2BackBtn.Location = new System.Drawing.Point(1091, 615);
            this.iP2BackBtn.Name = "iP2BackBtn";
            this.iP2BackBtn.Size = new System.Drawing.Size(150, 40);
            this.iP2BackBtn.TabIndex = 40;
            this.iP2BackBtn.Text = "ออกจากหน้านี้";
            this.iP2BackBtn.UseCustomBackColor = true;
            this.iP2BackBtn.UseCustomForeColor = true;
            this.iP2BackBtn.UseSelectable = true;
            this.iP2BackBtn.Click += new System.EventHandler(this.MetroButton11_Click);
            // 
            // iP2PrintFormBtn
            // 
            this.iP2PrintFormBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.iP2PrintFormBtn.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.iP2PrintFormBtn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.iP2PrintFormBtn.ForeColor = System.Drawing.Color.FloralWhite;
            this.iP2PrintFormBtn.Location = new System.Drawing.Point(488, 615);
            this.iP2PrintFormBtn.Name = "iP2PrintFormBtn";
            this.iP2PrintFormBtn.Size = new System.Drawing.Size(150, 40);
            this.iP2PrintFormBtn.TabIndex = 37;
            this.iP2PrintFormBtn.Text = "พิมพ์แบบฟอร์มนี้";
            this.iP2PrintFormBtn.UseCustomBackColor = true;
            this.iP2PrintFormBtn.UseCustomForeColor = true;
            this.iP2PrintFormBtn.UseSelectable = true;
            this.iP2PrintFormBtn.Click += new System.EventHandler(this.IP2PrintFormBtn_Click);
            // 
            // iP2JobOrderNoLbl
            // 
            this.iP2JobOrderNoLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP2JobOrderNoLbl.AutoSize = true;
            this.iP2JobOrderNoLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP2JobOrderNoLbl.Location = new System.Drawing.Point(908, 23);
            this.iP2JobOrderNoLbl.Name = "iP2JobOrderNoLbl";
            this.iP2JobOrderNoLbl.Size = new System.Drawing.Size(138, 25);
            this.iP2JobOrderNoLbl.TabIndex = 0;
            this.iP2JobOrderNoLbl.Text = "หมายเลขใบสั่งผลิต";
            // 
            // iP2AssignDateLbl
            // 
            this.iP2AssignDateLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP2AssignDateLbl.AutoSize = true;
            this.iP2AssignDateLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP2AssignDateLbl.Location = new System.Drawing.Point(908, 56);
            this.iP2AssignDateLbl.Name = "iP2AssignDateLbl";
            this.iP2AssignDateLbl.Size = new System.Drawing.Size(137, 25);
            this.iP2AssignDateLbl.TabIndex = 1;
            this.iP2AssignDateLbl.Text = "วันที่สร้างใบสั่งผลิต";
            // 
            // iP2TopLbl
            // 
            this.iP2TopLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP2TopLbl.AutoSize = true;
            this.iP2TopLbl.BackColor = System.Drawing.Color.White;
            this.iP2TopLbl.Font = new System.Drawing.Font("Microsoft Tai Le", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.iP2TopLbl.Location = new System.Drawing.Point(460, 24);
            this.iP2TopLbl.Name = "iP2TopLbl";
            this.iP2TopLbl.Size = new System.Drawing.Size(318, 34);
            this.iP2TopLbl.TabIndex = 102;
            this.iP2TopLbl.Text = "ใบสั่งผลิต/เบิกวัตถุดิบ/รับสินค้า";
            // 
            // iP1Pnl
            // 
            this.iP1Pnl.Controls.Add(this.iP1BarcodePnl);
            this.iP1Pnl.Controls.Add(this.metroPanel5);
            this.iP1Pnl.Controls.Add(this.iP1TopLbl);
            this.iP1Pnl.Controls.Add(this.iP1BarcodeBtn);
            this.iP1Pnl.Controls.Add(this.iP1OrLbl);
            this.iP1Pnl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.iP1Pnl.HorizontalScrollbarBarColor = true;
            this.iP1Pnl.HorizontalScrollbarHighlightOnWheel = false;
            this.iP1Pnl.HorizontalScrollbarSize = 10;
            this.iP1Pnl.Location = new System.Drawing.Point(0, 0);
            this.iP1Pnl.Name = "iP1Pnl";
            this.iP1Pnl.Size = new System.Drawing.Size(1238, 663);
            this.iP1Pnl.TabIndex = 35;
            this.iP1Pnl.VerticalScrollbarBarColor = true;
            this.iP1Pnl.VerticalScrollbarHighlightOnWheel = false;
            this.iP1Pnl.VerticalScrollbarSize = 10;
            this.iP1Pnl.MouseHover += new System.EventHandler(this.IP1Pnl_MouseHover);
            // 
            // iP1BarcodePnl
            // 
            this.iP1BarcodePnl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP1BarcodePnl.BackColor = System.Drawing.Color.Transparent;
            this.iP1BarcodePnl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.iP1BarcodePnl.Controls.Add(this.metroLabel4);
            this.iP1BarcodePnl.HorizontalScrollbarBarColor = true;
            this.iP1BarcodePnl.HorizontalScrollbarHighlightOnWheel = false;
            this.iP1BarcodePnl.HorizontalScrollbarSize = 10;
            this.iP1BarcodePnl.Location = new System.Drawing.Point(494, 302);
            this.iP1BarcodePnl.Name = "iP1BarcodePnl";
            this.iP1BarcodePnl.Size = new System.Drawing.Size(250, 85);
            this.iP1BarcodePnl.TabIndex = 45;
            this.iP1BarcodePnl.UseCustomBackColor = true;
            this.iP1BarcodePnl.VerticalScrollbarBarColor = true;
            this.iP1BarcodePnl.VerticalScrollbarHighlightOnWheel = false;
            this.iP1BarcodePnl.VerticalScrollbarSize = 10;
            this.iP1BarcodePnl.MouseLeave += new System.EventHandler(this.iP1BarcodePnl_MouseLeave);
            this.iP1BarcodePnl.MouseHover += new System.EventHandler(this.iP1BarcodePnl_MouseHover);
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(79, 4);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(90, 19);
            this.metroLabel4.TabIndex = 2;
            this.metroLabel4.Text = "Barcode Area";
            this.metroLabel4.UseCustomBackColor = true;
            // 
            // metroPanel5
            // 
            this.metroPanel5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.metroPanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.metroPanel5.Controls.Add(this.iP1JobNoTxb);
            this.metroPanel5.Controls.Add(this.iP1JobNoLbl);
            this.metroPanel5.Controls.Add(this.iP1SubmitBtn);
            this.metroPanel5.HorizontalScrollbarBarColor = true;
            this.metroPanel5.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel5.HorizontalScrollbarSize = 10;
            this.metroPanel5.Location = new System.Drawing.Point(312, 114);
            this.metroPanel5.Name = "metroPanel5";
            this.metroPanel5.Size = new System.Drawing.Size(615, 166);
            this.metroPanel5.TabIndex = 43;
            this.metroPanel5.VerticalScrollbarBarColor = true;
            this.metroPanel5.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel5.VerticalScrollbarSize = 10;
            // 
            // iP1JobNoTxb
            // 
            this.iP1JobNoTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.iP1JobNoTxb.CustomButton.Image = null;
            this.iP1JobNoTxb.CustomButton.Location = new System.Drawing.Point(205, 2);
            this.iP1JobNoTxb.CustomButton.Name = "";
            this.iP1JobNoTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.iP1JobNoTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.iP1JobNoTxb.CustomButton.TabIndex = 1;
            this.iP1JobNoTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.iP1JobNoTxb.CustomButton.UseSelectable = true;
            this.iP1JobNoTxb.CustomButton.Visible = false;
            this.iP1JobNoTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.iP1JobNoTxb.Lines = new string[0];
            this.iP1JobNoTxb.Location = new System.Drawing.Point(189, 67);
            this.iP1JobNoTxb.MaxLength = 32767;
            this.iP1JobNoTxb.Name = "iP1JobNoTxb";
            this.iP1JobNoTxb.PasswordChar = '\0';
            this.iP1JobNoTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.iP1JobNoTxb.SelectedText = "";
            this.iP1JobNoTxb.SelectionLength = 0;
            this.iP1JobNoTxb.SelectionStart = 0;
            this.iP1JobNoTxb.ShortcutsEnabled = true;
            this.iP1JobNoTxb.Size = new System.Drawing.Size(233, 30);
            this.iP1JobNoTxb.TabIndex = 7;
            this.iP1JobNoTxb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.iP1JobNoTxb.UseSelectable = true;
            this.iP1JobNoTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.iP1JobNoTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.iP1JobNoTxb.KeyDown += new System.Windows.Forms.KeyEventHandler(this.iP1JobNoTxb_KeyDown);
            this.iP1JobNoTxb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.iP1JobNoTxb_KeyPress);
            this.iP1JobNoTxb.KeyUp += new System.Windows.Forms.KeyEventHandler(this.IP1JobNoTxb_KeyUp);
            // 
            // iP1JobNoLbl
            // 
            this.iP1JobNoLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP1JobNoLbl.AutoSize = true;
            this.iP1JobNoLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP1JobNoLbl.Location = new System.Drawing.Point(32, 65);
            this.iP1JobNoLbl.Name = "iP1JobNoLbl";
            this.iP1JobNoLbl.Size = new System.Drawing.Size(138, 25);
            this.iP1JobNoLbl.TabIndex = 6;
            this.iP1JobNoLbl.Text = "หมายเลขใบสั่งผลิต";
            // 
            // iP1SubmitBtn
            // 
            this.iP1SubmitBtn.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP1SubmitBtn.BackColor = System.Drawing.Color.SteelBlue;
            this.iP1SubmitBtn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.iP1SubmitBtn.ForeColor = System.Drawing.Color.LightCyan;
            this.iP1SubmitBtn.Location = new System.Drawing.Point(468, 58);
            this.iP1SubmitBtn.Name = "iP1SubmitBtn";
            this.iP1SubmitBtn.Size = new System.Drawing.Size(96, 40);
            this.iP1SubmitBtn.TabIndex = 35;
            this.iP1SubmitBtn.Text = "ยืนยัน";
            this.iP1SubmitBtn.UseCustomBackColor = true;
            this.iP1SubmitBtn.UseCustomForeColor = true;
            this.iP1SubmitBtn.UseSelectable = true;
            this.iP1SubmitBtn.Click += new System.EventHandler(this.IP1SubmitBtn_Click);
            // 
            // iP1TopLbl
            // 
            this.iP1TopLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP1TopLbl.AutoSize = true;
            this.iP1TopLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP1TopLbl.Location = new System.Drawing.Point(221, 72);
            this.iP1TopLbl.Name = "iP1TopLbl";
            this.iP1TopLbl.Size = new System.Drawing.Size(796, 25);
            this.iP1TopLbl.TabIndex = 36;
            this.iP1TopLbl.Text = "กรุณาระบุหมายเลขใบสั่งผลิตหรือเลือกยิงบาร์โค้ดของหมายเลขใบสั่งผลิต เพื่อเข้าสู่หน" +
    "้าใบสั่งผลิต/เบิกวัตถุดิบ/รับสินค้า";
            // 
            // iP1BarcodeBtn
            // 
            this.iP1BarcodeBtn.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP1BarcodeBtn.Location = new System.Drawing.Point(512, 577);
            this.iP1BarcodeBtn.Name = "iP1BarcodeBtn";
            this.iP1BarcodeBtn.Size = new System.Drawing.Size(214, 30);
            this.iP1BarcodeBtn.TabIndex = 34;
            this.iP1BarcodeBtn.Text = "ยิงบาร์โค้ดของหมายเลขใบสั่งผลิต";
            this.iP1BarcodeBtn.UseSelectable = true;
            this.iP1BarcodeBtn.Visible = false;
            this.iP1BarcodeBtn.Click += new System.EventHandler(this.MetroButton17_Click);
            // 
            // iP1OrLbl
            // 
            this.iP1OrLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP1OrLbl.AutoSize = true;
            this.iP1OrLbl.Location = new System.Drawing.Point(604, 530);
            this.iP1OrLbl.Name = "iP1OrLbl";
            this.iP1OrLbl.Size = new System.Drawing.Size(30, 19);
            this.iP1OrLbl.TabIndex = 8;
            this.iP1OrLbl.Text = "หรือ";
            this.iP1OrLbl.Visible = false;
            // 
            // iP4Pnl
            // 
            this.iP4Pnl.Controls.Add(this.iP4TareInfoPnl);
            this.iP4Pnl.Controls.Add(this.iP4SpiralChk);
            this.iP4Pnl.Controls.Add(this.iP4TopLbl);
            this.iP4Pnl.Controls.Add(this.iP4JobNoLbl);
            this.iP4Pnl.Controls.Add(this.iP4ItemCodeLbl);
            this.iP4Pnl.Controls.Add(this.iP4GenerateLotBtn);
            this.iP4Pnl.Controls.Add(this.iP4MachineLbl);
            this.iP4Pnl.Controls.Add(this.iP4FormulaCbb);
            this.iP4Pnl.Controls.Add(this.iP4ShiftCbb);
            this.iP4Pnl.Controls.Add(this.iP4FormulaLbl);
            this.iP4Pnl.Controls.Add(this.iP4ToWhseLbl);
            this.iP4Pnl.Controls.Add(this.iP4BinLocationCbb);
            this.iP4Pnl.Controls.Add(this.iP4CompletedMeasureTxb);
            this.iP4Pnl.Controls.Add(this.iP4BinLocationLbl);
            this.iP4Pnl.Controls.Add(this.iP4CompletedMeasureUnitLbl);
            this.iP4Pnl.Controls.Add(this.iP4ItemCodeTxb);
            this.iP4Pnl.Controls.Add(this.iP4CompletedCountUnitLbl);
            this.iP4Pnl.Controls.Add(this.iP4DateLbl);
            this.iP4Pnl.Controls.Add(this.iP4LotNoTxb);
            this.iP4Pnl.Controls.Add(this.iP4CompletedQtyLbl);
            this.iP4Pnl.Controls.Add(this.iP4CompletedCountQtyTxb);
            this.iP4Pnl.Controls.Add(this.iP4ToWhseCbb);
            this.iP4Pnl.Controls.Add(this.iP4CompletedMeasureLbsTxb);
            this.iP4Pnl.Controls.Add(this.iP4LotNoLbl);
            this.iP4Pnl.Controls.Add(this.iP4ShiftLbl);
            this.iP4Pnl.Controls.Add(this.iP4JobNoTxb);
            this.iP4Pnl.Controls.Add(this.iP4CompletedMeasureLbsUnitTxb);
            this.iP4Pnl.Controls.Add(this.iP4MachineTxb);
            this.iP4Pnl.Controls.Add(this.iP4FactoryLbl);
            this.iP4Pnl.Controls.Add(this.iP4CompletedMeasure2Lbl);
            this.iP4Pnl.Controls.Add(this.iP4FactoryCbb);
            this.iP4Pnl.Controls.Add(this.iP4DateCbb);
            this.iP4Pnl.Controls.Add(this.iP4CompletedMeasure1Lbl);
            this.iP4Pnl.Controls.Add(this.iP4SubmitBtn);
            this.iP4Pnl.Controls.Add(this.iP4PrintBarcodeBtn);
            this.iP4Pnl.Controls.Add(this.metroPanel21);
            this.iP4Pnl.Controls.Add(this.iP4CancelBtn);
            this.iP4Pnl.Controls.Add(this.iP4TotalWeightPnl);
            this.iP4Pnl.Controls.Add(this.iP4JobReceiveItemDgv);
            this.iP4Pnl.Controls.Add(this.iP4StartOptionsPanel);
            this.iP4Pnl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.iP4Pnl.HorizontalScrollbarBarColor = true;
            this.iP4Pnl.HorizontalScrollbarHighlightOnWheel = false;
            this.iP4Pnl.HorizontalScrollbarSize = 10;
            this.iP4Pnl.Location = new System.Drawing.Point(0, 0);
            this.iP4Pnl.Name = "iP4Pnl";
            this.iP4Pnl.Size = new System.Drawing.Size(1238, 663);
            this.iP4Pnl.TabIndex = 38;
            this.iP4Pnl.VerticalScrollbarBarColor = true;
            this.iP4Pnl.VerticalScrollbarHighlightOnWheel = false;
            this.iP4Pnl.VerticalScrollbarSize = 10;
            this.iP4Pnl.Visible = false;
            this.iP4Pnl.Leave += new System.EventHandler(this.IP4Pnl_Leave);
            this.iP4Pnl.Validating += new System.ComponentModel.CancelEventHandler(this.IP4Pnl_Validating);
            this.iP4Pnl.Validated += new System.EventHandler(this.IP4Pnl_Validated);
            // 
            // iP4TareInfoPnl
            // 
            this.iP4TareInfoPnl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP4TareInfoPnl.Controls.Add(this.iP4PlasticChk);
            this.iP4TareInfoPnl.Controls.Add(this.iP4ConeCbb);
            this.iP4TareInfoPnl.Controls.Add(this.iP4ConeLbl);
            this.iP4TareInfoPnl.Controls.Add(this.iP4AmountPerPackageLbl);
            this.iP4TareInfoPnl.Controls.Add(this.iP4AmountPerPackageTxb);
            this.iP4TareInfoPnl.Controls.Add(this.iP4AmountPerPackageUnitLbl);
            this.iP4TareInfoPnl.Controls.Add(this.iP4InnerPackageWeightPerPieceUnitLbl);
            this.iP4TareInfoPnl.Controls.Add(this.iP4InnerPackageWeightPerPieceLbl);
            this.iP4TareInfoPnl.Controls.Add(this.iP4InnerPackageWeightPerPieceTxb);
            this.iP4TareInfoPnl.Controls.Add(this.iP4TareWeightPerPieceUnitLbl);
            this.iP4TareInfoPnl.Controls.Add(this.iP4TareWeightPerPieceLbl);
            this.iP4TareInfoPnl.Controls.Add(this.iP4TareWeightPerPieceTxb);
            this.iP4TareInfoPnl.Controls.Add(this.iP4OuterPackagingLbl);
            this.iP4TareInfoPnl.Controls.Add(this.iP4PackagingCbb);
            this.iP4TareInfoPnl.Controls.Add(this.iP4OuterPackagingWeightTxb);
            this.iP4TareInfoPnl.Controls.Add(this.iP4OuterPackagingWeightUnitLbl);
            this.iP4TareInfoPnl.Controls.Add(this.iP4InnerPackagingLbl);
            this.iP4TareInfoPnl.Controls.Add(this.iP4InnerPackagingWeightTxb);
            this.iP4TareInfoPnl.Controls.Add(this.iP4PackagingLbl);
            this.iP4TareInfoPnl.Controls.Add(this.iP4InnerPackagingWeightUnitLbl);
            this.iP4TareInfoPnl.Controls.Add(this.iP4MiscPackagingLbl);
            this.iP4TareInfoPnl.Controls.Add(this.iP4MiscPackagingWeightTxb);
            this.iP4TareInfoPnl.Controls.Add(this.iP4MiscPackagingWeightUnitLbl);
            this.iP4TareInfoPnl.Controls.Add(this.iP4OuterPackagingCountLbl);
            this.iP4TareInfoPnl.Controls.Add(this.iP4OuterPackagingCountTxb);
            this.iP4TareInfoPnl.Controls.Add(this.iP4OuterPackagingCountUnitLbl);
            this.iP4TareInfoPnl.Controls.Add(this.iP4MiscPackagingCountUnitLbl);
            this.iP4TareInfoPnl.Controls.Add(this.iP4InnerPackagingCountLbl);
            this.iP4TareInfoPnl.Controls.Add(this.iP4MiscPackagingCountTxb);
            this.iP4TareInfoPnl.Controls.Add(this.iP4InnerPackagingCountTxb);
            this.iP4TareInfoPnl.Controls.Add(this.iP4MiscPackagingCountLbl);
            this.iP4TareInfoPnl.Controls.Add(this.iP4InnerPackagingCountUnitLbl);
            this.iP4TareInfoPnl.HorizontalScrollbarBarColor = true;
            this.iP4TareInfoPnl.HorizontalScrollbarHighlightOnWheel = false;
            this.iP4TareInfoPnl.HorizontalScrollbarSize = 10;
            this.iP4TareInfoPnl.Location = new System.Drawing.Point(8, 169);
            this.iP4TareInfoPnl.Name = "iP4TareInfoPnl";
            this.iP4TareInfoPnl.Size = new System.Drawing.Size(750, 230);
            this.iP4TareInfoPnl.TabIndex = 4;
            this.iP4TareInfoPnl.VerticalScrollbarBarColor = true;
            this.iP4TareInfoPnl.VerticalScrollbarHighlightOnWheel = false;
            this.iP4TareInfoPnl.VerticalScrollbarSize = 10;
            // 
            // iP4PlasticChk
            // 
            this.iP4PlasticChk.AutoSize = true;
            this.iP4PlasticChk.FontSize = MetroFramework.MetroCheckBoxSize.Tall;
            this.iP4PlasticChk.Location = new System.Drawing.Point(128, 84);
            this.iP4PlasticChk.Name = "iP4PlasticChk";
            this.iP4PlasticChk.Size = new System.Drawing.Size(115, 25);
            this.iP4PlasticChk.TabIndex = 202;
            this.iP4PlasticChk.Text = "มีถุงพลาสติก";
            this.iP4PlasticChk.UseSelectable = true;
            this.iP4PlasticChk.CheckedChanged += new System.EventHandler(this.iP4PlasticChk_CheckedChanged);
            // 
            // iP4ConeCbb
            // 
            this.iP4ConeCbb.FormattingEnabled = true;
            this.iP4ConeCbb.ItemHeight = 23;
            this.iP4ConeCbb.Items.AddRange(new object[] {
            "กระสอบ",
            "กล่องเล็ก",
            "กล่องใหญ่",
            "พาเลซ"});
            this.iP4ConeCbb.Location = new System.Drawing.Point(128, 49);
            this.iP4ConeCbb.Name = "iP4ConeCbb";
            this.iP4ConeCbb.Size = new System.Drawing.Size(203, 29);
            this.iP4ConeCbb.TabIndex = 182;
            this.iP4ConeCbb.UseSelectable = true;
            this.iP4ConeCbb.SelectedIndexChanged += new System.EventHandler(this.iP4ConeCbb_SelectedIndexChanged);
            // 
            // iP4ConeLbl
            // 
            this.iP4ConeLbl.AutoSize = true;
            this.iP4ConeLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP4ConeLbl.Location = new System.Drawing.Point(4, 51);
            this.iP4ConeLbl.Name = "iP4ConeLbl";
            this.iP4ConeLbl.Size = new System.Drawing.Size(112, 25);
            this.iP4ConeLbl.TabIndex = 181;
            this.iP4ConeLbl.Text = "ชนิดหลอดด้าย";
            // 
            // iP4AmountPerPackageLbl
            // 
            this.iP4AmountPerPackageLbl.AutoSize = true;
            this.iP4AmountPerPackageLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP4AmountPerPackageLbl.Location = new System.Drawing.Point(345, 86);
            this.iP4AmountPerPackageLbl.Name = "iP4AmountPerPackageLbl";
            this.iP4AmountPerPackageLbl.Size = new System.Drawing.Size(205, 25);
            this.iP4AmountPerPackageLbl.TabIndex = 178;
            this.iP4AmountPerPackageLbl.Text = "จำนวนหลอดใน 1 บรรจุภัณฑ์";
            // 
            // iP4AmountPerPackageTxb
            // 
            // 
            // 
            // 
            this.iP4AmountPerPackageTxb.CustomButton.Image = null;
            this.iP4AmountPerPackageTxb.CustomButton.Location = new System.Drawing.Point(73, 2);
            this.iP4AmountPerPackageTxb.CustomButton.Name = "";
            this.iP4AmountPerPackageTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.iP4AmountPerPackageTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.iP4AmountPerPackageTxb.CustomButton.TabIndex = 1;
            this.iP4AmountPerPackageTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.iP4AmountPerPackageTxb.CustomButton.UseSelectable = true;
            this.iP4AmountPerPackageTxb.CustomButton.Visible = false;
            this.iP4AmountPerPackageTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.iP4AmountPerPackageTxb.Lines = new string[0];
            this.iP4AmountPerPackageTxb.Location = new System.Drawing.Point(558, 83);
            this.iP4AmountPerPackageTxb.MaxLength = 32767;
            this.iP4AmountPerPackageTxb.Name = "iP4AmountPerPackageTxb";
            this.iP4AmountPerPackageTxb.PasswordChar = '\0';
            this.iP4AmountPerPackageTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.iP4AmountPerPackageTxb.SelectedText = "";
            this.iP4AmountPerPackageTxb.SelectionLength = 0;
            this.iP4AmountPerPackageTxb.SelectionStart = 0;
            this.iP4AmountPerPackageTxb.ShortcutsEnabled = true;
            this.iP4AmountPerPackageTxb.Size = new System.Drawing.Size(101, 30);
            this.iP4AmountPerPackageTxb.TabIndex = 179;
            this.iP4AmountPerPackageTxb.UseSelectable = true;
            this.iP4AmountPerPackageTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.iP4AmountPerPackageTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.iP4AmountPerPackageTxb.TextChanged += new System.EventHandler(this.IP4AmountPerPackageTxb_TextChanged);
            this.iP4AmountPerPackageTxb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IP4AmountPerPackageTxb_KeyPress);
            // 
            // iP4AmountPerPackageUnitLbl
            // 
            this.iP4AmountPerPackageUnitLbl.AutoSize = true;
            this.iP4AmountPerPackageUnitLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP4AmountPerPackageUnitLbl.Location = new System.Drawing.Point(666, 86);
            this.iP4AmountPerPackageUnitLbl.Name = "iP4AmountPerPackageUnitLbl";
            this.iP4AmountPerPackageUnitLbl.Size = new System.Drawing.Size(34, 25);
            this.iP4AmountPerPackageUnitLbl.TabIndex = 180;
            this.iP4AmountPerPackageUnitLbl.Text = "ชิ้น";
            // 
            // iP4InnerPackageWeightPerPieceUnitLbl
            // 
            this.iP4InnerPackageWeightPerPieceUnitLbl.AutoSize = true;
            this.iP4InnerPackageWeightPerPieceUnitLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP4InnerPackageWeightPerPieceUnitLbl.Location = new System.Drawing.Point(666, 51);
            this.iP4InnerPackageWeightPerPieceUnitLbl.Name = "iP4InnerPackageWeightPerPieceUnitLbl";
            this.iP4InnerPackageWeightPerPieceUnitLbl.Size = new System.Drawing.Size(36, 25);
            this.iP4InnerPackageWeightPerPieceUnitLbl.TabIndex = 169;
            this.iP4InnerPackageWeightPerPieceUnitLbl.Text = "กก.";
            // 
            // iP4InnerPackageWeightPerPieceLbl
            // 
            this.iP4InnerPackageWeightPerPieceLbl.AutoSize = true;
            this.iP4InnerPackageWeightPerPieceLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP4InnerPackageWeightPerPieceLbl.Location = new System.Drawing.Point(345, 51);
            this.iP4InnerPackageWeightPerPieceLbl.Name = "iP4InnerPackageWeightPerPieceLbl";
            this.iP4InnerPackageWeightPerPieceLbl.Size = new System.Drawing.Size(206, 25);
            this.iP4InnerPackageWeightPerPieceLbl.TabIndex = 168;
            this.iP4InnerPackageWeightPerPieceLbl.Text = "น.น.บรรจุภัณฑ์ภายใน ต่อชิ้น";
            // 
            // iP4InnerPackageWeightPerPieceTxb
            // 
            // 
            // 
            // 
            this.iP4InnerPackageWeightPerPieceTxb.CustomButton.Image = null;
            this.iP4InnerPackageWeightPerPieceTxb.CustomButton.Location = new System.Drawing.Point(74, 2);
            this.iP4InnerPackageWeightPerPieceTxb.CustomButton.Name = "";
            this.iP4InnerPackageWeightPerPieceTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.iP4InnerPackageWeightPerPieceTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.iP4InnerPackageWeightPerPieceTxb.CustomButton.TabIndex = 1;
            this.iP4InnerPackageWeightPerPieceTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.iP4InnerPackageWeightPerPieceTxb.CustomButton.UseSelectable = true;
            this.iP4InnerPackageWeightPerPieceTxb.CustomButton.Visible = false;
            this.iP4InnerPackageWeightPerPieceTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.iP4InnerPackageWeightPerPieceTxb.Lines = new string[0];
            this.iP4InnerPackageWeightPerPieceTxb.Location = new System.Drawing.Point(558, 48);
            this.iP4InnerPackageWeightPerPieceTxb.MaxLength = 32767;
            this.iP4InnerPackageWeightPerPieceTxb.Name = "iP4InnerPackageWeightPerPieceTxb";
            this.iP4InnerPackageWeightPerPieceTxb.PasswordChar = '\0';
            this.iP4InnerPackageWeightPerPieceTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.iP4InnerPackageWeightPerPieceTxb.SelectedText = "";
            this.iP4InnerPackageWeightPerPieceTxb.SelectionLength = 0;
            this.iP4InnerPackageWeightPerPieceTxb.SelectionStart = 0;
            this.iP4InnerPackageWeightPerPieceTxb.ShortcutsEnabled = true;
            this.iP4InnerPackageWeightPerPieceTxb.Size = new System.Drawing.Size(102, 30);
            this.iP4InnerPackageWeightPerPieceTxb.TabIndex = 167;
            this.iP4InnerPackageWeightPerPieceTxb.UseSelectable = true;
            this.iP4InnerPackageWeightPerPieceTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.iP4InnerPackageWeightPerPieceTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.iP4InnerPackageWeightPerPieceTxb.TextChanged += new System.EventHandler(this.IP4InnerPackageWeightPerPieceTxb_TextChanged);
            this.iP4InnerPackageWeightPerPieceTxb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IP4InnerPackageWeightPerPieceTxb_KeyPress);
            // 
            // iP4TareWeightPerPieceUnitLbl
            // 
            this.iP4TareWeightPerPieceUnitLbl.AutoSize = true;
            this.iP4TareWeightPerPieceUnitLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP4TareWeightPerPieceUnitLbl.Location = new System.Drawing.Point(666, 15);
            this.iP4TareWeightPerPieceUnitLbl.Name = "iP4TareWeightPerPieceUnitLbl";
            this.iP4TareWeightPerPieceUnitLbl.Size = new System.Drawing.Size(36, 25);
            this.iP4TareWeightPerPieceUnitLbl.TabIndex = 166;
            this.iP4TareWeightPerPieceUnitLbl.Text = "กก.";
            // 
            // iP4TareWeightPerPieceLbl
            // 
            this.iP4TareWeightPerPieceLbl.AutoSize = true;
            this.iP4TareWeightPerPieceLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP4TareWeightPerPieceLbl.Location = new System.Drawing.Point(345, 15);
            this.iP4TareWeightPerPieceLbl.Name = "iP4TareWeightPerPieceLbl";
            this.iP4TareWeightPerPieceLbl.Size = new System.Drawing.Size(175, 25);
            this.iP4TareWeightPerPieceLbl.TabIndex = 165;
            this.iP4TareWeightPerPieceLbl.Text = "น.น.บรรจุภัณฑ์ภายนอก";
            // 
            // iP4TareWeightPerPieceTxb
            // 
            // 
            // 
            // 
            this.iP4TareWeightPerPieceTxb.CustomButton.Image = null;
            this.iP4TareWeightPerPieceTxb.CustomButton.Location = new System.Drawing.Point(73, 2);
            this.iP4TareWeightPerPieceTxb.CustomButton.Name = "";
            this.iP4TareWeightPerPieceTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.iP4TareWeightPerPieceTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.iP4TareWeightPerPieceTxb.CustomButton.TabIndex = 1;
            this.iP4TareWeightPerPieceTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.iP4TareWeightPerPieceTxb.CustomButton.UseSelectable = true;
            this.iP4TareWeightPerPieceTxb.CustomButton.Visible = false;
            this.iP4TareWeightPerPieceTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.iP4TareWeightPerPieceTxb.Lines = new string[0];
            this.iP4TareWeightPerPieceTxb.Location = new System.Drawing.Point(558, 12);
            this.iP4TareWeightPerPieceTxb.MaxLength = 32767;
            this.iP4TareWeightPerPieceTxb.Name = "iP4TareWeightPerPieceTxb";
            this.iP4TareWeightPerPieceTxb.PasswordChar = '\0';
            this.iP4TareWeightPerPieceTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.iP4TareWeightPerPieceTxb.SelectedText = "";
            this.iP4TareWeightPerPieceTxb.SelectionLength = 0;
            this.iP4TareWeightPerPieceTxb.SelectionStart = 0;
            this.iP4TareWeightPerPieceTxb.ShortcutsEnabled = true;
            this.iP4TareWeightPerPieceTxb.Size = new System.Drawing.Size(101, 30);
            this.iP4TareWeightPerPieceTxb.TabIndex = 164;
            this.iP4TareWeightPerPieceTxb.UseSelectable = true;
            this.iP4TareWeightPerPieceTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.iP4TareWeightPerPieceTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.iP4TareWeightPerPieceTxb.TextChanged += new System.EventHandler(this.IP4TareWeightPerPieceTxb_TextChanged);
            this.iP4TareWeightPerPieceTxb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IP4TareWeightPerPieceTxb_KeyPress);
            // 
            // iP4OuterPackagingLbl
            // 
            this.iP4OuterPackagingLbl.AutoSize = true;
            this.iP4OuterPackagingLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP4OuterPackagingLbl.Location = new System.Drawing.Point(3, 206);
            this.iP4OuterPackagingLbl.Name = "iP4OuterPackagingLbl";
            this.iP4OuterPackagingLbl.Size = new System.Drawing.Size(206, 25);
            this.iP4OuterPackagingLbl.TabIndex = 143;
            this.iP4OuterPackagingLbl.Text = "น.น.บรรจุภัณฑ์ภายนอก รวม";
            this.iP4OuterPackagingLbl.Visible = false;
            // 
            // iP4PackagingCbb
            // 
            this.iP4PackagingCbb.FormattingEnabled = true;
            this.iP4PackagingCbb.ItemHeight = 23;
            this.iP4PackagingCbb.Items.AddRange(new object[] {
            "กระสอบ",
            "กล่องเล็ก",
            "กล่องใหญ่",
            "พาเลซ"});
            this.iP4PackagingCbb.Location = new System.Drawing.Point(128, 13);
            this.iP4PackagingCbb.Name = "iP4PackagingCbb";
            this.iP4PackagingCbb.Size = new System.Drawing.Size(203, 29);
            this.iP4PackagingCbb.TabIndex = 177;
            this.iP4PackagingCbb.UseSelectable = true;
            this.iP4PackagingCbb.SelectedIndexChanged += new System.EventHandler(this.IP4PackagingCbb_SelectedIndexChanged);
            // 
            // iP4OuterPackagingWeightTxb
            // 
            // 
            // 
            // 
            this.iP4OuterPackagingWeightTxb.CustomButton.Image = null;
            this.iP4OuterPackagingWeightTxb.CustomButton.Location = new System.Drawing.Point(52, 2);
            this.iP4OuterPackagingWeightTxb.CustomButton.Name = "";
            this.iP4OuterPackagingWeightTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.iP4OuterPackagingWeightTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.iP4OuterPackagingWeightTxb.CustomButton.TabIndex = 1;
            this.iP4OuterPackagingWeightTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.iP4OuterPackagingWeightTxb.CustomButton.UseSelectable = true;
            this.iP4OuterPackagingWeightTxb.CustomButton.Visible = false;
            this.iP4OuterPackagingWeightTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.iP4OuterPackagingWeightTxb.Lines = new string[0];
            this.iP4OuterPackagingWeightTxb.Location = new System.Drawing.Point(202, 203);
            this.iP4OuterPackagingWeightTxb.MaxLength = 32767;
            this.iP4OuterPackagingWeightTxb.Name = "iP4OuterPackagingWeightTxb";
            this.iP4OuterPackagingWeightTxb.PasswordChar = '\0';
            this.iP4OuterPackagingWeightTxb.ReadOnly = true;
            this.iP4OuterPackagingWeightTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.iP4OuterPackagingWeightTxb.SelectedText = "";
            this.iP4OuterPackagingWeightTxb.SelectionLength = 0;
            this.iP4OuterPackagingWeightTxb.SelectionStart = 0;
            this.iP4OuterPackagingWeightTxb.ShortcutsEnabled = true;
            this.iP4OuterPackagingWeightTxb.Size = new System.Drawing.Size(80, 30);
            this.iP4OuterPackagingWeightTxb.TabIndex = 144;
            this.iP4OuterPackagingWeightTxb.UseSelectable = true;
            this.iP4OuterPackagingWeightTxb.Visible = false;
            this.iP4OuterPackagingWeightTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.iP4OuterPackagingWeightTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.iP4OuterPackagingWeightTxb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IP4OuterPackagingWeightTxb_KeyPress);
            // 
            // iP4OuterPackagingWeightUnitLbl
            // 
            this.iP4OuterPackagingWeightUnitLbl.AutoSize = true;
            this.iP4OuterPackagingWeightUnitLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP4OuterPackagingWeightUnitLbl.Location = new System.Drawing.Point(288, 206);
            this.iP4OuterPackagingWeightUnitLbl.Name = "iP4OuterPackagingWeightUnitLbl";
            this.iP4OuterPackagingWeightUnitLbl.Size = new System.Drawing.Size(36, 25);
            this.iP4OuterPackagingWeightUnitLbl.TabIndex = 145;
            this.iP4OuterPackagingWeightUnitLbl.Text = "กก.";
            this.iP4OuterPackagingWeightUnitLbl.Visible = false;
            // 
            // iP4InnerPackagingLbl
            // 
            this.iP4InnerPackagingLbl.AutoSize = true;
            this.iP4InnerPackagingLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP4InnerPackagingLbl.Location = new System.Drawing.Point(3, 137);
            this.iP4InnerPackagingLbl.Name = "iP4InnerPackagingLbl";
            this.iP4InnerPackagingLbl.Size = new System.Drawing.Size(190, 25);
            this.iP4InnerPackagingLbl.TabIndex = 149;
            this.iP4InnerPackagingLbl.Text = "น.น.บรรจุภัณฑ์ภายใน รวม";
            // 
            // iP4InnerPackagingWeightTxb
            // 
            // 
            // 
            // 
            this.iP4InnerPackagingWeightTxb.CustomButton.Image = null;
            this.iP4InnerPackagingWeightTxb.CustomButton.Location = new System.Drawing.Point(52, 2);
            this.iP4InnerPackagingWeightTxb.CustomButton.Name = "";
            this.iP4InnerPackagingWeightTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.iP4InnerPackagingWeightTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.iP4InnerPackagingWeightTxb.CustomButton.TabIndex = 1;
            this.iP4InnerPackagingWeightTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.iP4InnerPackagingWeightTxb.CustomButton.UseSelectable = true;
            this.iP4InnerPackagingWeightTxb.CustomButton.Visible = false;
            this.iP4InnerPackagingWeightTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.iP4InnerPackagingWeightTxb.Lines = new string[0];
            this.iP4InnerPackagingWeightTxb.Location = new System.Drawing.Point(202, 134);
            this.iP4InnerPackagingWeightTxb.MaxLength = 32767;
            this.iP4InnerPackagingWeightTxb.Name = "iP4InnerPackagingWeightTxb";
            this.iP4InnerPackagingWeightTxb.PasswordChar = '\0';
            this.iP4InnerPackagingWeightTxb.ReadOnly = true;
            this.iP4InnerPackagingWeightTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.iP4InnerPackagingWeightTxb.SelectedText = "";
            this.iP4InnerPackagingWeightTxb.SelectionLength = 0;
            this.iP4InnerPackagingWeightTxb.SelectionStart = 0;
            this.iP4InnerPackagingWeightTxb.ShortcutsEnabled = true;
            this.iP4InnerPackagingWeightTxb.Size = new System.Drawing.Size(80, 30);
            this.iP4InnerPackagingWeightTxb.TabIndex = 150;
            this.iP4InnerPackagingWeightTxb.UseSelectable = true;
            this.iP4InnerPackagingWeightTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.iP4InnerPackagingWeightTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.iP4InnerPackagingWeightTxb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IP4InnerPackagingWeightTxb_KeyPress);
            // 
            // iP4PackagingLbl
            // 
            this.iP4PackagingLbl.AutoSize = true;
            this.iP4PackagingLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP4PackagingLbl.Location = new System.Drawing.Point(4, 15);
            this.iP4PackagingLbl.Name = "iP4PackagingLbl";
            this.iP4PackagingLbl.Size = new System.Drawing.Size(118, 25);
            this.iP4PackagingLbl.TabIndex = 176;
            this.iP4PackagingLbl.Text = "ชนิดบรรจุภัณฑ์";
            // 
            // iP4InnerPackagingWeightUnitLbl
            // 
            this.iP4InnerPackagingWeightUnitLbl.AutoSize = true;
            this.iP4InnerPackagingWeightUnitLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP4InnerPackagingWeightUnitLbl.Location = new System.Drawing.Point(288, 137);
            this.iP4InnerPackagingWeightUnitLbl.Name = "iP4InnerPackagingWeightUnitLbl";
            this.iP4InnerPackagingWeightUnitLbl.Size = new System.Drawing.Size(36, 25);
            this.iP4InnerPackagingWeightUnitLbl.TabIndex = 151;
            this.iP4InnerPackagingWeightUnitLbl.Text = "กก.";
            // 
            // iP4MiscPackagingLbl
            // 
            this.iP4MiscPackagingLbl.AutoSize = true;
            this.iP4MiscPackagingLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP4MiscPackagingLbl.Location = new System.Drawing.Point(3, 170);
            this.iP4MiscPackagingLbl.Name = "iP4MiscPackagingLbl";
            this.iP4MiscPackagingLbl.Size = new System.Drawing.Size(176, 25);
            this.iP4MiscPackagingLbl.TabIndex = 152;
            this.iP4MiscPackagingLbl.Text = "น.น.บรรจุภัณฑ์อื่นๆ รวม";
            // 
            // iP4MiscPackagingWeightTxb
            // 
            // 
            // 
            // 
            this.iP4MiscPackagingWeightTxb.CustomButton.Image = null;
            this.iP4MiscPackagingWeightTxb.CustomButton.Location = new System.Drawing.Point(52, 2);
            this.iP4MiscPackagingWeightTxb.CustomButton.Name = "";
            this.iP4MiscPackagingWeightTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.iP4MiscPackagingWeightTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.iP4MiscPackagingWeightTxb.CustomButton.TabIndex = 1;
            this.iP4MiscPackagingWeightTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.iP4MiscPackagingWeightTxb.CustomButton.UseSelectable = true;
            this.iP4MiscPackagingWeightTxb.CustomButton.Visible = false;
            this.iP4MiscPackagingWeightTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.iP4MiscPackagingWeightTxb.Lines = new string[0];
            this.iP4MiscPackagingWeightTxb.Location = new System.Drawing.Point(202, 167);
            this.iP4MiscPackagingWeightTxb.MaxLength = 32767;
            this.iP4MiscPackagingWeightTxb.Name = "iP4MiscPackagingWeightTxb";
            this.iP4MiscPackagingWeightTxb.PasswordChar = '\0';
            this.iP4MiscPackagingWeightTxb.ReadOnly = true;
            this.iP4MiscPackagingWeightTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.iP4MiscPackagingWeightTxb.SelectedText = "";
            this.iP4MiscPackagingWeightTxb.SelectionLength = 0;
            this.iP4MiscPackagingWeightTxb.SelectionStart = 0;
            this.iP4MiscPackagingWeightTxb.ShortcutsEnabled = true;
            this.iP4MiscPackagingWeightTxb.Size = new System.Drawing.Size(80, 30);
            this.iP4MiscPackagingWeightTxb.TabIndex = 153;
            this.iP4MiscPackagingWeightTxb.UseSelectable = true;
            this.iP4MiscPackagingWeightTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.iP4MiscPackagingWeightTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.iP4MiscPackagingWeightTxb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IP4MiscPackagingWeightTxb_KeyPress);
            // 
            // iP4MiscPackagingWeightUnitLbl
            // 
            this.iP4MiscPackagingWeightUnitLbl.AutoSize = true;
            this.iP4MiscPackagingWeightUnitLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP4MiscPackagingWeightUnitLbl.Location = new System.Drawing.Point(288, 170);
            this.iP4MiscPackagingWeightUnitLbl.Name = "iP4MiscPackagingWeightUnitLbl";
            this.iP4MiscPackagingWeightUnitLbl.Size = new System.Drawing.Size(36, 25);
            this.iP4MiscPackagingWeightUnitLbl.TabIndex = 154;
            this.iP4MiscPackagingWeightUnitLbl.Text = "กก.";
            // 
            // iP4OuterPackagingCountLbl
            // 
            this.iP4OuterPackagingCountLbl.AutoSize = true;
            this.iP4OuterPackagingCountLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP4OuterPackagingCountLbl.Location = new System.Drawing.Point(336, 206);
            this.iP4OuterPackagingCountLbl.Name = "iP4OuterPackagingCountLbl";
            this.iP4OuterPackagingCountLbl.Size = new System.Drawing.Size(59, 25);
            this.iP4OuterPackagingCountLbl.TabIndex = 155;
            this.iP4OuterPackagingCountLbl.Text = "จำนวน";
            this.iP4OuterPackagingCountLbl.Visible = false;
            // 
            // iP4OuterPackagingCountTxb
            // 
            // 
            // 
            // 
            this.iP4OuterPackagingCountTxb.CustomButton.Image = null;
            this.iP4OuterPackagingCountTxb.CustomButton.Location = new System.Drawing.Point(35, 2);
            this.iP4OuterPackagingCountTxb.CustomButton.Name = "";
            this.iP4OuterPackagingCountTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.iP4OuterPackagingCountTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.iP4OuterPackagingCountTxb.CustomButton.TabIndex = 1;
            this.iP4OuterPackagingCountTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.iP4OuterPackagingCountTxb.CustomButton.UseSelectable = true;
            this.iP4OuterPackagingCountTxb.CustomButton.Visible = false;
            this.iP4OuterPackagingCountTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.iP4OuterPackagingCountTxb.Lines = new string[0];
            this.iP4OuterPackagingCountTxb.Location = new System.Drawing.Point(406, 203);
            this.iP4OuterPackagingCountTxb.MaxLength = 32767;
            this.iP4OuterPackagingCountTxb.Name = "iP4OuterPackagingCountTxb";
            this.iP4OuterPackagingCountTxb.PasswordChar = '\0';
            this.iP4OuterPackagingCountTxb.ReadOnly = true;
            this.iP4OuterPackagingCountTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.iP4OuterPackagingCountTxb.SelectedText = "";
            this.iP4OuterPackagingCountTxb.SelectionLength = 0;
            this.iP4OuterPackagingCountTxb.SelectionStart = 0;
            this.iP4OuterPackagingCountTxb.ShortcutsEnabled = true;
            this.iP4OuterPackagingCountTxb.Size = new System.Drawing.Size(63, 30);
            this.iP4OuterPackagingCountTxb.TabIndex = 156;
            this.iP4OuterPackagingCountTxb.UseSelectable = true;
            this.iP4OuterPackagingCountTxb.Visible = false;
            this.iP4OuterPackagingCountTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.iP4OuterPackagingCountTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.iP4OuterPackagingCountTxb.TextChanged += new System.EventHandler(this.IP4OuterPackagingCountTxb_TextChanged);
            this.iP4OuterPackagingCountTxb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IP4OuterPackagingCountTxb_KeyPress);
            // 
            // iP4OuterPackagingCountUnitLbl
            // 
            this.iP4OuterPackagingCountUnitLbl.AutoSize = true;
            this.iP4OuterPackagingCountUnitLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP4OuterPackagingCountUnitLbl.Location = new System.Drawing.Point(480, 206);
            this.iP4OuterPackagingCountUnitLbl.Name = "iP4OuterPackagingCountUnitLbl";
            this.iP4OuterPackagingCountUnitLbl.Size = new System.Drawing.Size(34, 25);
            this.iP4OuterPackagingCountUnitLbl.TabIndex = 157;
            this.iP4OuterPackagingCountUnitLbl.Text = "ชิ้น";
            this.iP4OuterPackagingCountUnitLbl.Visible = false;
            // 
            // iP4MiscPackagingCountUnitLbl
            // 
            this.iP4MiscPackagingCountUnitLbl.AutoSize = true;
            this.iP4MiscPackagingCountUnitLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP4MiscPackagingCountUnitLbl.Location = new System.Drawing.Point(480, 170);
            this.iP4MiscPackagingCountUnitLbl.Name = "iP4MiscPackagingCountUnitLbl";
            this.iP4MiscPackagingCountUnitLbl.Size = new System.Drawing.Size(34, 25);
            this.iP4MiscPackagingCountUnitLbl.TabIndex = 163;
            this.iP4MiscPackagingCountUnitLbl.Text = "ชิ้น";
            // 
            // iP4InnerPackagingCountLbl
            // 
            this.iP4InnerPackagingCountLbl.AutoSize = true;
            this.iP4InnerPackagingCountLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP4InnerPackagingCountLbl.Location = new System.Drawing.Point(336, 137);
            this.iP4InnerPackagingCountLbl.Name = "iP4InnerPackagingCountLbl";
            this.iP4InnerPackagingCountLbl.Size = new System.Drawing.Size(59, 25);
            this.iP4InnerPackagingCountLbl.TabIndex = 158;
            this.iP4InnerPackagingCountLbl.Text = "จำนวน";
            // 
            // iP4MiscPackagingCountTxb
            // 
            // 
            // 
            // 
            this.iP4MiscPackagingCountTxb.CustomButton.Image = null;
            this.iP4MiscPackagingCountTxb.CustomButton.Location = new System.Drawing.Point(35, 2);
            this.iP4MiscPackagingCountTxb.CustomButton.Name = "";
            this.iP4MiscPackagingCountTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.iP4MiscPackagingCountTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.iP4MiscPackagingCountTxb.CustomButton.TabIndex = 1;
            this.iP4MiscPackagingCountTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.iP4MiscPackagingCountTxb.CustomButton.UseSelectable = true;
            this.iP4MiscPackagingCountTxb.CustomButton.Visible = false;
            this.iP4MiscPackagingCountTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.iP4MiscPackagingCountTxb.Lines = new string[0];
            this.iP4MiscPackagingCountTxb.Location = new System.Drawing.Point(406, 167);
            this.iP4MiscPackagingCountTxb.MaxLength = 32767;
            this.iP4MiscPackagingCountTxb.Name = "iP4MiscPackagingCountTxb";
            this.iP4MiscPackagingCountTxb.PasswordChar = '\0';
            this.iP4MiscPackagingCountTxb.ReadOnly = true;
            this.iP4MiscPackagingCountTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.iP4MiscPackagingCountTxb.SelectedText = "";
            this.iP4MiscPackagingCountTxb.SelectionLength = 0;
            this.iP4MiscPackagingCountTxb.SelectionStart = 0;
            this.iP4MiscPackagingCountTxb.ShortcutsEnabled = true;
            this.iP4MiscPackagingCountTxb.Size = new System.Drawing.Size(63, 30);
            this.iP4MiscPackagingCountTxb.TabIndex = 162;
            this.iP4MiscPackagingCountTxb.UseSelectable = true;
            this.iP4MiscPackagingCountTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.iP4MiscPackagingCountTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.iP4MiscPackagingCountTxb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IP4MiscPackagingCountTxb_KeyPress);
            // 
            // iP4InnerPackagingCountTxb
            // 
            // 
            // 
            // 
            this.iP4InnerPackagingCountTxb.CustomButton.Image = null;
            this.iP4InnerPackagingCountTxb.CustomButton.Location = new System.Drawing.Point(35, 2);
            this.iP4InnerPackagingCountTxb.CustomButton.Name = "";
            this.iP4InnerPackagingCountTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.iP4InnerPackagingCountTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.iP4InnerPackagingCountTxb.CustomButton.TabIndex = 1;
            this.iP4InnerPackagingCountTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.iP4InnerPackagingCountTxb.CustomButton.UseSelectable = true;
            this.iP4InnerPackagingCountTxb.CustomButton.Visible = false;
            this.iP4InnerPackagingCountTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.iP4InnerPackagingCountTxb.Lines = new string[0];
            this.iP4InnerPackagingCountTxb.Location = new System.Drawing.Point(406, 134);
            this.iP4InnerPackagingCountTxb.MaxLength = 32767;
            this.iP4InnerPackagingCountTxb.Name = "iP4InnerPackagingCountTxb";
            this.iP4InnerPackagingCountTxb.PasswordChar = '\0';
            this.iP4InnerPackagingCountTxb.ReadOnly = true;
            this.iP4InnerPackagingCountTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.iP4InnerPackagingCountTxb.SelectedText = "";
            this.iP4InnerPackagingCountTxb.SelectionLength = 0;
            this.iP4InnerPackagingCountTxb.SelectionStart = 0;
            this.iP4InnerPackagingCountTxb.ShortcutsEnabled = true;
            this.iP4InnerPackagingCountTxb.Size = new System.Drawing.Size(63, 30);
            this.iP4InnerPackagingCountTxb.TabIndex = 159;
            this.iP4InnerPackagingCountTxb.UseSelectable = true;
            this.iP4InnerPackagingCountTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.iP4InnerPackagingCountTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.iP4InnerPackagingCountTxb.TextChanged += new System.EventHandler(this.IP4InnerPackagingCountTxb_TextChanged);
            this.iP4InnerPackagingCountTxb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IP4InnerPackagingCountTxb_KeyPress);
            // 
            // iP4MiscPackagingCountLbl
            // 
            this.iP4MiscPackagingCountLbl.AutoSize = true;
            this.iP4MiscPackagingCountLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP4MiscPackagingCountLbl.Location = new System.Drawing.Point(336, 170);
            this.iP4MiscPackagingCountLbl.Name = "iP4MiscPackagingCountLbl";
            this.iP4MiscPackagingCountLbl.Size = new System.Drawing.Size(59, 25);
            this.iP4MiscPackagingCountLbl.TabIndex = 161;
            this.iP4MiscPackagingCountLbl.Text = "จำนวน";
            // 
            // iP4InnerPackagingCountUnitLbl
            // 
            this.iP4InnerPackagingCountUnitLbl.AutoSize = true;
            this.iP4InnerPackagingCountUnitLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP4InnerPackagingCountUnitLbl.Location = new System.Drawing.Point(480, 137);
            this.iP4InnerPackagingCountUnitLbl.Name = "iP4InnerPackagingCountUnitLbl";
            this.iP4InnerPackagingCountUnitLbl.Size = new System.Drawing.Size(34, 25);
            this.iP4InnerPackagingCountUnitLbl.TabIndex = 160;
            this.iP4InnerPackagingCountUnitLbl.Text = "ชิ้น";
            // 
            // iP4SpiralChk
            // 
            this.iP4SpiralChk.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP4SpiralChk.AutoSize = true;
            this.iP4SpiralChk.FontSize = MetroFramework.MetroCheckBoxSize.Tall;
            this.iP4SpiralChk.Location = new System.Drawing.Point(370, 91);
            this.iP4SpiralChk.Name = "iP4SpiralChk";
            this.iP4SpiralChk.Size = new System.Drawing.Size(80, 25);
            this.iP4SpiralChk.TabIndex = 201;
            this.iP4SpiralChk.Text = "ตีเกลียว";
            this.iP4SpiralChk.UseSelectable = true;
            this.iP4SpiralChk.CheckedChanged += new System.EventHandler(this.iP4SpiralChk_CheckedChanged);
            // 
            // iP4TopLbl
            // 
            this.iP4TopLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP4TopLbl.AutoSize = true;
            this.iP4TopLbl.BackColor = System.Drawing.Color.White;
            this.iP4TopLbl.Font = new System.Drawing.Font("Microsoft Tai Le", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.iP4TopLbl.Location = new System.Drawing.Point(523, 14);
            this.iP4TopLbl.Name = "iP4TopLbl";
            this.iP4TopLbl.Size = new System.Drawing.Size(192, 34);
            this.iP4TopLbl.TabIndex = 200;
            this.iP4TopLbl.Text = "บันทึกรับผลิตเสร็จ";
            // 
            // iP4JobNoLbl
            // 
            this.iP4JobNoLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP4JobNoLbl.AutoSize = true;
            this.iP4JobNoLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP4JobNoLbl.Location = new System.Drawing.Point(10, 50);
            this.iP4JobNoLbl.Name = "iP4JobNoLbl";
            this.iP4JobNoLbl.Size = new System.Drawing.Size(100, 25);
            this.iP4JobNoLbl.TabIndex = 118;
            this.iP4JobNoLbl.Text = "เลขใบสั่งผลิต";
            // 
            // iP4ItemCodeLbl
            // 
            this.iP4ItemCodeLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP4ItemCodeLbl.AutoSize = true;
            this.iP4ItemCodeLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP4ItemCodeLbl.Location = new System.Drawing.Point(10, 88);
            this.iP4ItemCodeLbl.Name = "iP4ItemCodeLbl";
            this.iP4ItemCodeLbl.Size = new System.Drawing.Size(80, 25);
            this.iP4ItemCodeLbl.TabIndex = 103;
            this.iP4ItemCodeLbl.Text = "รหัสสินค้า";
            // 
            // iP4GenerateLotBtn
            // 
            this.iP4GenerateLotBtn.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP4GenerateLotBtn.BackColor = System.Drawing.Color.MediumPurple;
            this.iP4GenerateLotBtn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.iP4GenerateLotBtn.ForeColor = System.Drawing.Color.Lavender;
            this.iP4GenerateLotBtn.Location = new System.Drawing.Point(1001, 273);
            this.iP4GenerateLotBtn.Name = "iP4GenerateLotBtn";
            this.iP4GenerateLotBtn.Size = new System.Drawing.Size(150, 40);
            this.iP4GenerateLotBtn.TabIndex = 198;
            this.iP4GenerateLotBtn.Text = "สร้าง Lot";
            this.iP4GenerateLotBtn.UseCustomBackColor = true;
            this.iP4GenerateLotBtn.UseCustomForeColor = true;
            this.iP4GenerateLotBtn.UseSelectable = true;
            this.iP4GenerateLotBtn.Click += new System.EventHandler(this.IP4GenerateLotBtn_Click);
            // 
            // iP4MachineLbl
            // 
            this.iP4MachineLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP4MachineLbl.AutoSize = true;
            this.iP4MachineLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP4MachineLbl.Location = new System.Drawing.Point(537, 129);
            this.iP4MachineLbl.Name = "iP4MachineLbl";
            this.iP4MachineLbl.Size = new System.Drawing.Size(94, 25);
            this.iP4MachineLbl.TabIndex = 107;
            this.iP4MachineLbl.Text = "เครื่องผลิตที่";
            // 
            // iP4FormulaCbb
            // 
            this.iP4FormulaCbb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP4FormulaCbb.FormattingEnabled = true;
            this.iP4FormulaCbb.ItemHeight = 23;
            this.iP4FormulaCbb.Items.AddRange(new object[] {
            "A",
            "B",
            "C"});
            this.iP4FormulaCbb.Location = new System.Drawing.Point(770, 127);
            this.iP4FormulaCbb.Name = "iP4FormulaCbb";
            this.iP4FormulaCbb.Size = new System.Drawing.Size(72, 29);
            this.iP4FormulaCbb.TabIndex = 192;
            this.iP4FormulaCbb.UseSelectable = true;
            this.iP4FormulaCbb.SelectedIndexChanged += new System.EventHandler(this.iP4FormulaCbb_SelectedIndexChanged);
            // 
            // iP4ShiftCbb
            // 
            this.iP4ShiftCbb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP4ShiftCbb.FormattingEnabled = true;
            this.iP4ShiftCbb.ItemHeight = 23;
            this.iP4ShiftCbb.Items.AddRange(new object[] {
            "A",
            "B",
            "C"});
            this.iP4ShiftCbb.Location = new System.Drawing.Point(770, 89);
            this.iP4ShiftCbb.Name = "iP4ShiftCbb";
            this.iP4ShiftCbb.Size = new System.Drawing.Size(72, 29);
            this.iP4ShiftCbb.TabIndex = 122;
            this.iP4ShiftCbb.UseSelectable = true;
            this.iP4ShiftCbb.SelectedIndexChanged += new System.EventHandler(this.IP4ShiftCbb_SelectedIndexChanged);
            // 
            // iP4FormulaLbl
            // 
            this.iP4FormulaLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP4FormulaLbl.AutoSize = true;
            this.iP4FormulaLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP4FormulaLbl.Location = new System.Drawing.Point(728, 129);
            this.iP4FormulaLbl.Name = "iP4FormulaLbl";
            this.iP4FormulaLbl.Size = new System.Drawing.Size(40, 25);
            this.iP4FormulaLbl.TabIndex = 191;
            this.iP4FormulaLbl.Text = "สูตร";
            // 
            // iP4ToWhseLbl
            // 
            this.iP4ToWhseLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP4ToWhseLbl.AutoSize = true;
            this.iP4ToWhseLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP4ToWhseLbl.Location = new System.Drawing.Point(862, 90);
            this.iP4ToWhseLbl.Name = "iP4ToWhseLbl";
            this.iP4ToWhseLbl.Size = new System.Drawing.Size(116, 25);
            this.iP4ToWhseLbl.TabIndex = 172;
            this.iP4ToWhseLbl.Text = "โกดังที่จะจัดเก็บ";
            // 
            // iP4BinLocationCbb
            // 
            this.iP4BinLocationCbb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP4BinLocationCbb.FormattingEnabled = true;
            this.iP4BinLocationCbb.ItemHeight = 23;
            this.iP4BinLocationCbb.Location = new System.Drawing.Point(1001, 124);
            this.iP4BinLocationCbb.Name = "iP4BinLocationCbb";
            this.iP4BinLocationCbb.Size = new System.Drawing.Size(121, 29);
            this.iP4BinLocationCbb.TabIndex = 188;
            this.iP4BinLocationCbb.UseSelectable = true;
            // 
            // iP4CompletedMeasureTxb
            // 
            this.iP4CompletedMeasureTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.iP4CompletedMeasureTxb.CustomButton.Image = null;
            this.iP4CompletedMeasureTxb.CustomButton.Location = new System.Drawing.Point(93, 2);
            this.iP4CompletedMeasureTxb.CustomButton.Margin = new System.Windows.Forms.Padding(2);
            this.iP4CompletedMeasureTxb.CustomButton.Name = "";
            this.iP4CompletedMeasureTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.iP4CompletedMeasureTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.iP4CompletedMeasureTxb.CustomButton.TabIndex = 1;
            this.iP4CompletedMeasureTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.iP4CompletedMeasureTxb.CustomButton.UseSelectable = true;
            this.iP4CompletedMeasureTxb.CustomButton.Visible = false;
            this.iP4CompletedMeasureTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.iP4CompletedMeasureTxb.Lines = new string[0];
            this.iP4CompletedMeasureTxb.Location = new System.Drawing.Point(1001, 195);
            this.iP4CompletedMeasureTxb.MaxLength = 32767;
            this.iP4CompletedMeasureTxb.Name = "iP4CompletedMeasureTxb";
            this.iP4CompletedMeasureTxb.PasswordChar = '\0';
            this.iP4CompletedMeasureTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.iP4CompletedMeasureTxb.SelectedText = "";
            this.iP4CompletedMeasureTxb.SelectionLength = 0;
            this.iP4CompletedMeasureTxb.SelectionStart = 0;
            this.iP4CompletedMeasureTxb.ShortcutsEnabled = true;
            this.iP4CompletedMeasureTxb.Size = new System.Drawing.Size(121, 30);
            this.iP4CompletedMeasureTxb.TabIndex = 111;
            this.iP4CompletedMeasureTxb.UseSelectable = true;
            this.iP4CompletedMeasureTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.iP4CompletedMeasureTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.iP4CompletedMeasureTxb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IP4CompletedMeasureTxb_KeyPress);
            this.iP4CompletedMeasureTxb.KeyUp += new System.Windows.Forms.KeyEventHandler(this.iP4CompletedMeasureTxb_KeyUp);
            // 
            // iP4BinLocationLbl
            // 
            this.iP4BinLocationLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP4BinLocationLbl.AutoSize = true;
            this.iP4BinLocationLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP4BinLocationLbl.Location = new System.Drawing.Point(862, 125);
            this.iP4BinLocationLbl.Name = "iP4BinLocationLbl";
            this.iP4BinLocationLbl.Size = new System.Drawing.Size(112, 25);
            this.iP4BinLocationLbl.TabIndex = 174;
            this.iP4BinLocationLbl.Text = "ลอคที่จะจัดเก็บ";
            // 
            // iP4CompletedMeasureUnitLbl
            // 
            this.iP4CompletedMeasureUnitLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP4CompletedMeasureUnitLbl.AutoSize = true;
            this.iP4CompletedMeasureUnitLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP4CompletedMeasureUnitLbl.Location = new System.Drawing.Point(1127, 197);
            this.iP4CompletedMeasureUnitLbl.Name = "iP4CompletedMeasureUnitLbl";
            this.iP4CompletedMeasureUnitLbl.Size = new System.Drawing.Size(72, 25);
            this.iP4CompletedMeasureUnitLbl.TabIndex = 112;
            this.iP4CompletedMeasureUnitLbl.Text = "กก./หลา";
            // 
            // iP4ItemCodeTxb
            // 
            this.iP4ItemCodeTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.iP4ItemCodeTxb.CustomButton.Image = null;
            this.iP4ItemCodeTxb.CustomButton.Location = new System.Drawing.Point(199, 2);
            this.iP4ItemCodeTxb.CustomButton.Margin = new System.Windows.Forms.Padding(2);
            this.iP4ItemCodeTxb.CustomButton.Name = "";
            this.iP4ItemCodeTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.iP4ItemCodeTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.iP4ItemCodeTxb.CustomButton.TabIndex = 1;
            this.iP4ItemCodeTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.iP4ItemCodeTxb.CustomButton.UseSelectable = true;
            this.iP4ItemCodeTxb.CustomButton.Visible = false;
            this.iP4ItemCodeTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.iP4ItemCodeTxb.ForeColor = System.Drawing.Color.Gray;
            this.iP4ItemCodeTxb.Lines = new string[0];
            this.iP4ItemCodeTxb.Location = new System.Drawing.Point(127, 88);
            this.iP4ItemCodeTxb.MaxLength = 32767;
            this.iP4ItemCodeTxb.Name = "iP4ItemCodeTxb";
            this.iP4ItemCodeTxb.PasswordChar = '\0';
            this.iP4ItemCodeTxb.ReadOnly = true;
            this.iP4ItemCodeTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.iP4ItemCodeTxb.SelectedText = "";
            this.iP4ItemCodeTxb.SelectionLength = 0;
            this.iP4ItemCodeTxb.SelectionStart = 0;
            this.iP4ItemCodeTxb.ShortcutsEnabled = true;
            this.iP4ItemCodeTxb.Size = new System.Drawing.Size(227, 30);
            this.iP4ItemCodeTxb.TabIndex = 109;
            this.iP4ItemCodeTxb.UseCustomForeColor = true;
            this.iP4ItemCodeTxb.UseSelectable = true;
            this.iP4ItemCodeTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.iP4ItemCodeTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // iP4CompletedCountUnitLbl
            // 
            this.iP4CompletedCountUnitLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP4CompletedCountUnitLbl.AutoSize = true;
            this.iP4CompletedCountUnitLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP4CompletedCountUnitLbl.Location = new System.Drawing.Point(1127, 161);
            this.iP4CompletedCountUnitLbl.Name = "iP4CompletedCountUnitLbl";
            this.iP4CompletedCountUnitLbl.Size = new System.Drawing.Size(95, 25);
            this.iP4CompletedCountUnitLbl.TabIndex = 105;
            this.iP4CompletedCountUnitLbl.Text = "กระสอบ/พับ";
            // 
            // iP4DateLbl
            // 
            this.iP4DateLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP4DateLbl.AutoSize = true;
            this.iP4DateLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP4DateLbl.Location = new System.Drawing.Point(862, 50);
            this.iP4DateLbl.Name = "iP4DateLbl";
            this.iP4DateLbl.Size = new System.Drawing.Size(42, 25);
            this.iP4DateLbl.TabIndex = 164;
            this.iP4DateLbl.Text = "วันที่";
            // 
            // iP4LotNoTxb
            // 
            this.iP4LotNoTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.iP4LotNoTxb.CustomButton.Image = null;
            this.iP4LotNoTxb.CustomButton.Location = new System.Drawing.Point(199, 2);
            this.iP4LotNoTxb.CustomButton.Margin = new System.Windows.Forms.Padding(2);
            this.iP4LotNoTxb.CustomButton.Name = "";
            this.iP4LotNoTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.iP4LotNoTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.iP4LotNoTxb.CustomButton.TabIndex = 1;
            this.iP4LotNoTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.iP4LotNoTxb.CustomButton.UseSelectable = true;
            this.iP4LotNoTxb.CustomButton.Visible = false;
            this.iP4LotNoTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.iP4LotNoTxb.ForeColor = System.Drawing.Color.Gray;
            this.iP4LotNoTxb.Lines = new string[0];
            this.iP4LotNoTxb.Location = new System.Drawing.Point(127, 126);
            this.iP4LotNoTxb.MaxLength = 32767;
            this.iP4LotNoTxb.Name = "iP4LotNoTxb";
            this.iP4LotNoTxb.PasswordChar = '\0';
            this.iP4LotNoTxb.ReadOnly = true;
            this.iP4LotNoTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.iP4LotNoTxb.SelectedText = "";
            this.iP4LotNoTxb.SelectionLength = 0;
            this.iP4LotNoTxb.SelectionStart = 0;
            this.iP4LotNoTxb.ShortcutsEnabled = true;
            this.iP4LotNoTxb.Size = new System.Drawing.Size(227, 30);
            this.iP4LotNoTxb.TabIndex = 113;
            this.iP4LotNoTxb.UseCustomForeColor = true;
            this.iP4LotNoTxb.UseSelectable = true;
            this.iP4LotNoTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.iP4LotNoTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // iP4CompletedQtyLbl
            // 
            this.iP4CompletedQtyLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP4CompletedQtyLbl.AutoSize = true;
            this.iP4CompletedQtyLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP4CompletedQtyLbl.Location = new System.Drawing.Point(863, 162);
            this.iP4CompletedQtyLbl.Name = "iP4CompletedQtyLbl";
            this.iP4CompletedQtyLbl.Size = new System.Drawing.Size(122, 25);
            this.iP4CompletedQtyLbl.TabIndex = 108;
            this.iP4CompletedQtyLbl.Text = "จำนวนผลิตเสร็จ";
            // 
            // iP4CompletedCountQtyTxb
            // 
            this.iP4CompletedCountQtyTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.iP4CompletedCountQtyTxb.CustomButton.Image = null;
            this.iP4CompletedCountQtyTxb.CustomButton.Location = new System.Drawing.Point(93, 2);
            this.iP4CompletedCountQtyTxb.CustomButton.Margin = new System.Windows.Forms.Padding(2);
            this.iP4CompletedCountQtyTxb.CustomButton.Name = "";
            this.iP4CompletedCountQtyTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.iP4CompletedCountQtyTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.iP4CompletedCountQtyTxb.CustomButton.TabIndex = 1;
            this.iP4CompletedCountQtyTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.iP4CompletedCountQtyTxb.CustomButton.UseSelectable = true;
            this.iP4CompletedCountQtyTxb.CustomButton.Visible = false;
            this.iP4CompletedCountQtyTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.iP4CompletedCountQtyTxb.Lines = new string[0];
            this.iP4CompletedCountQtyTxb.Location = new System.Drawing.Point(1001, 160);
            this.iP4CompletedCountQtyTxb.MaxLength = 32767;
            this.iP4CompletedCountQtyTxb.Name = "iP4CompletedCountQtyTxb";
            this.iP4CompletedCountQtyTxb.PasswordChar = '\0';
            this.iP4CompletedCountQtyTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.iP4CompletedCountQtyTxb.SelectedText = "";
            this.iP4CompletedCountQtyTxb.SelectionLength = 0;
            this.iP4CompletedCountQtyTxb.SelectionStart = 0;
            this.iP4CompletedCountQtyTxb.ShortcutsEnabled = true;
            this.iP4CompletedCountQtyTxb.Size = new System.Drawing.Size(121, 30);
            this.iP4CompletedCountQtyTxb.TabIndex = 104;
            this.iP4CompletedCountQtyTxb.UseSelectable = true;
            this.iP4CompletedCountQtyTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.iP4CompletedCountQtyTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.iP4CompletedCountQtyTxb.TextChanged += new System.EventHandler(this.iP4CompletedCountQtyTxb_TextChanged);
            this.iP4CompletedCountQtyTxb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IP4CompletedCountQtyTxb_KeyPress);
            this.iP4CompletedCountQtyTxb.KeyUp += new System.Windows.Forms.KeyEventHandler(this.iP4CompletedCountQtyTxb_KeyUp);
            // 
            // iP4ToWhseCbb
            // 
            this.iP4ToWhseCbb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP4ToWhseCbb.FormattingEnabled = true;
            this.iP4ToWhseCbb.ItemHeight = 23;
            this.iP4ToWhseCbb.Location = new System.Drawing.Point(1001, 89);
            this.iP4ToWhseCbb.Name = "iP4ToWhseCbb";
            this.iP4ToWhseCbb.Size = new System.Drawing.Size(121, 29);
            this.iP4ToWhseCbb.TabIndex = 187;
            this.iP4ToWhseCbb.UseSelectable = true;
            // 
            // iP4CompletedMeasureLbsTxb
            // 
            this.iP4CompletedMeasureLbsTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.iP4CompletedMeasureLbsTxb.CustomButton.Image = null;
            this.iP4CompletedMeasureLbsTxb.CustomButton.Location = new System.Drawing.Point(93, 2);
            this.iP4CompletedMeasureLbsTxb.CustomButton.Margin = new System.Windows.Forms.Padding(2);
            this.iP4CompletedMeasureLbsTxb.CustomButton.Name = "";
            this.iP4CompletedMeasureLbsTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.iP4CompletedMeasureLbsTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.iP4CompletedMeasureLbsTxb.CustomButton.TabIndex = 1;
            this.iP4CompletedMeasureLbsTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.iP4CompletedMeasureLbsTxb.CustomButton.UseSelectable = true;
            this.iP4CompletedMeasureLbsTxb.CustomButton.Visible = false;
            this.iP4CompletedMeasureLbsTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.iP4CompletedMeasureLbsTxb.Lines = new string[0];
            this.iP4CompletedMeasureLbsTxb.Location = new System.Drawing.Point(1001, 230);
            this.iP4CompletedMeasureLbsTxb.MaxLength = 32767;
            this.iP4CompletedMeasureLbsTxb.Name = "iP4CompletedMeasureLbsTxb";
            this.iP4CompletedMeasureLbsTxb.PasswordChar = '\0';
            this.iP4CompletedMeasureLbsTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.iP4CompletedMeasureLbsTxb.SelectedText = "";
            this.iP4CompletedMeasureLbsTxb.SelectionLength = 0;
            this.iP4CompletedMeasureLbsTxb.SelectionStart = 0;
            this.iP4CompletedMeasureLbsTxb.ShortcutsEnabled = true;
            this.iP4CompletedMeasureLbsTxb.Size = new System.Drawing.Size(121, 30);
            this.iP4CompletedMeasureLbsTxb.TabIndex = 123;
            this.iP4CompletedMeasureLbsTxb.UseSelectable = true;
            this.iP4CompletedMeasureLbsTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.iP4CompletedMeasureLbsTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.iP4CompletedMeasureLbsTxb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IP4CompletedMeasureLbsTxb_KeyPress);
            this.iP4CompletedMeasureLbsTxb.KeyUp += new System.Windows.Forms.KeyEventHandler(this.iP4CompletedMeasureLbsTxb_KeyUp);
            // 
            // iP4LotNoLbl
            // 
            this.iP4LotNoLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP4LotNoLbl.AutoSize = true;
            this.iP4LotNoLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP4LotNoLbl.Location = new System.Drawing.Point(10, 127);
            this.iP4LotNoLbl.Name = "iP4LotNoLbl";
            this.iP4LotNoLbl.Size = new System.Drawing.Size(104, 25);
            this.iP4LotNoLbl.TabIndex = 106;
            this.iP4LotNoLbl.Text = "หมายเลข Lot";
            // 
            // iP4ShiftLbl
            // 
            this.iP4ShiftLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP4ShiftLbl.AutoSize = true;
            this.iP4ShiftLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP4ShiftLbl.Location = new System.Drawing.Point(728, 91);
            this.iP4ShiftLbl.Name = "iP4ShiftLbl";
            this.iP4ShiftLbl.Size = new System.Drawing.Size(28, 25);
            this.iP4ShiftLbl.TabIndex = 116;
            this.iP4ShiftLbl.Text = "กะ";
            // 
            // iP4JobNoTxb
            // 
            this.iP4JobNoTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.iP4JobNoTxb.CustomButton.Image = null;
            this.iP4JobNoTxb.CustomButton.Location = new System.Drawing.Point(199, 2);
            this.iP4JobNoTxb.CustomButton.Margin = new System.Windows.Forms.Padding(2);
            this.iP4JobNoTxb.CustomButton.Name = "";
            this.iP4JobNoTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.iP4JobNoTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.iP4JobNoTxb.CustomButton.TabIndex = 1;
            this.iP4JobNoTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.iP4JobNoTxb.CustomButton.UseSelectable = true;
            this.iP4JobNoTxb.CustomButton.Visible = false;
            this.iP4JobNoTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.iP4JobNoTxb.ForeColor = System.Drawing.Color.Gray;
            this.iP4JobNoTxb.Lines = new string[0];
            this.iP4JobNoTxb.Location = new System.Drawing.Point(127, 50);
            this.iP4JobNoTxb.MaxLength = 32767;
            this.iP4JobNoTxb.Name = "iP4JobNoTxb";
            this.iP4JobNoTxb.PasswordChar = '\0';
            this.iP4JobNoTxb.ReadOnly = true;
            this.iP4JobNoTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.iP4JobNoTxb.SelectedText = "";
            this.iP4JobNoTxb.SelectionLength = 0;
            this.iP4JobNoTxb.SelectionStart = 0;
            this.iP4JobNoTxb.ShortcutsEnabled = true;
            this.iP4JobNoTxb.Size = new System.Drawing.Size(227, 30);
            this.iP4JobNoTxb.TabIndex = 119;
            this.iP4JobNoTxb.UseCustomForeColor = true;
            this.iP4JobNoTxb.UseSelectable = true;
            this.iP4JobNoTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.iP4JobNoTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // iP4CompletedMeasureLbsUnitTxb
            // 
            this.iP4CompletedMeasureLbsUnitTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP4CompletedMeasureLbsUnitTxb.AutoSize = true;
            this.iP4CompletedMeasureLbsUnitTxb.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP4CompletedMeasureLbsUnitTxb.Location = new System.Drawing.Point(1127, 231);
            this.iP4CompletedMeasureLbsUnitTxb.Name = "iP4CompletedMeasureLbsUnitTxb";
            this.iP4CompletedMeasureLbsUnitTxb.Size = new System.Drawing.Size(53, 25);
            this.iP4CompletedMeasureLbsUnitTxb.TabIndex = 124;
            this.iP4CompletedMeasureLbsUnitTxb.Text = "ปอนด์";
            // 
            // iP4MachineTxb
            // 
            this.iP4MachineTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.iP4MachineTxb.CustomButton.Image = null;
            this.iP4MachineTxb.CustomButton.Location = new System.Drawing.Point(47, 2);
            this.iP4MachineTxb.CustomButton.Name = "";
            this.iP4MachineTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.iP4MachineTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.iP4MachineTxb.CustomButton.TabIndex = 1;
            this.iP4MachineTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.iP4MachineTxb.CustomButton.UseSelectable = true;
            this.iP4MachineTxb.CustomButton.Visible = false;
            this.iP4MachineTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.iP4MachineTxb.Lines = new string[0];
            this.iP4MachineTxb.Location = new System.Drawing.Point(642, 126);
            this.iP4MachineTxb.MaxLength = 32767;
            this.iP4MachineTxb.Name = "iP4MachineTxb";
            this.iP4MachineTxb.PasswordChar = '\0';
            this.iP4MachineTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.iP4MachineTxb.SelectedText = "";
            this.iP4MachineTxb.SelectionLength = 0;
            this.iP4MachineTxb.SelectionStart = 0;
            this.iP4MachineTxb.ShortcutsEnabled = true;
            this.iP4MachineTxb.Size = new System.Drawing.Size(75, 30);
            this.iP4MachineTxb.TabIndex = 190;
            this.iP4MachineTxb.UseSelectable = true;
            this.iP4MachineTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.iP4MachineTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.iP4MachineTxb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IP4MachineTxb_KeyPress);
            this.iP4MachineTxb.KeyUp += new System.Windows.Forms.KeyEventHandler(this.IP4MachineTxb_KeyUp);
            // 
            // iP4FactoryLbl
            // 
            this.iP4FactoryLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP4FactoryLbl.AutoSize = true;
            this.iP4FactoryLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP4FactoryLbl.Location = new System.Drawing.Point(537, 91);
            this.iP4FactoryLbl.Name = "iP4FactoryLbl";
            this.iP4FactoryLbl.Size = new System.Drawing.Size(102, 25);
            this.iP4FactoryLbl.TabIndex = 120;
            this.iP4FactoryLbl.Text = "โรงงานที่ผลิต";
            // 
            // iP4CompletedMeasure2Lbl
            // 
            this.iP4CompletedMeasure2Lbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP4CompletedMeasure2Lbl.AutoSize = true;
            this.iP4CompletedMeasure2Lbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP4CompletedMeasure2Lbl.Location = new System.Drawing.Point(863, 233);
            this.iP4CompletedMeasure2Lbl.Name = "iP4CompletedMeasure2Lbl";
            this.iP4CompletedMeasure2Lbl.Size = new System.Drawing.Size(72, 25);
            this.iP4CompletedMeasure2Lbl.TabIndex = 194;
            this.iP4CompletedMeasure2Lbl.Text = "น.น.สุทธิ";
            // 
            // iP4FactoryCbb
            // 
            this.iP4FactoryCbb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP4FactoryCbb.FormattingEnabled = true;
            this.iP4FactoryCbb.ItemHeight = 23;
            this.iP4FactoryCbb.Items.AddRange(new object[] {
            "SQC",
            "SRM",
            "SPD2",
            "SPD4",
            "SFG"});
            this.iP4FactoryCbb.Location = new System.Drawing.Point(642, 89);
            this.iP4FactoryCbb.Name = "iP4FactoryCbb";
            this.iP4FactoryCbb.Size = new System.Drawing.Size(72, 29);
            this.iP4FactoryCbb.TabIndex = 121;
            this.iP4FactoryCbb.UseSelectable = true;
            this.iP4FactoryCbb.SelectedIndexChanged += new System.EventHandler(this.IP4FactoryCbb_SelectedIndexChanged);
            // 
            // iP4DateCbb
            // 
            this.iP4DateCbb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP4DateCbb.CustomFormat = "d MMMM yyyy";
            this.iP4DateCbb.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.iP4DateCbb.Location = new System.Drawing.Point(1001, 50);
            this.iP4DateCbb.MinimumSize = new System.Drawing.Size(4, 29);
            this.iP4DateCbb.Name = "iP4DateCbb";
            this.iP4DateCbb.Size = new System.Drawing.Size(169, 29);
            this.iP4DateCbb.TabIndex = 165;
            // 
            // iP4CompletedMeasure1Lbl
            // 
            this.iP4CompletedMeasure1Lbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP4CompletedMeasure1Lbl.AutoSize = true;
            this.iP4CompletedMeasure1Lbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP4CompletedMeasure1Lbl.Location = new System.Drawing.Point(863, 197);
            this.iP4CompletedMeasure1Lbl.Name = "iP4CompletedMeasure1Lbl";
            this.iP4CompletedMeasure1Lbl.Size = new System.Drawing.Size(72, 25);
            this.iP4CompletedMeasure1Lbl.TabIndex = 193;
            this.iP4CompletedMeasure1Lbl.Text = "น.น.สุทธิ";
            // 
            // iP4SubmitBtn
            // 
            this.iP4SubmitBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.iP4SubmitBtn.BackColor = System.Drawing.Color.SteelBlue;
            this.iP4SubmitBtn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.iP4SubmitBtn.ForeColor = System.Drawing.Color.LightCyan;
            this.iP4SubmitBtn.Location = new System.Drawing.Point(389, 615);
            this.iP4SubmitBtn.Name = "iP4SubmitBtn";
            this.iP4SubmitBtn.Size = new System.Drawing.Size(150, 40);
            this.iP4SubmitBtn.TabIndex = 196;
            this.iP4SubmitBtn.Text = "บันทึกข้อมูล";
            this.iP4SubmitBtn.UseCustomBackColor = true;
            this.iP4SubmitBtn.UseCustomForeColor = true;
            this.iP4SubmitBtn.UseSelectable = true;
            this.iP4SubmitBtn.Click += new System.EventHandler(this.IP4SubmitBtn_Click);
            // 
            // iP4PrintBarcodeBtn
            // 
            this.iP4PrintBarcodeBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.iP4PrintBarcodeBtn.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.iP4PrintBarcodeBtn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.iP4PrintBarcodeBtn.ForeColor = System.Drawing.Color.FloralWhite;
            this.iP4PrintBarcodeBtn.Location = new System.Drawing.Point(579, 615);
            this.iP4PrintBarcodeBtn.Name = "iP4PrintBarcodeBtn";
            this.iP4PrintBarcodeBtn.Size = new System.Drawing.Size(150, 40);
            this.iP4PrintBarcodeBtn.TabIndex = 197;
            this.iP4PrintBarcodeBtn.Text = "พิมพ์บาร์โค้ด";
            this.iP4PrintBarcodeBtn.UseCustomBackColor = true;
            this.iP4PrintBarcodeBtn.UseCustomForeColor = true;
            this.iP4PrintBarcodeBtn.UseSelectable = true;
            this.iP4PrintBarcodeBtn.Click += new System.EventHandler(this.IP4PrintBarcodeBtn_Click);
            // 
            // metroPanel21
            // 
            this.metroPanel21.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.metroPanel21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.metroPanel21.Controls.Add(this.iP4PrintAllChk);
            this.metroPanel21.Controls.Add(this.iP4PrintToLbl);
            this.metroPanel21.Controls.Add(this.iP4PrintFromTxb);
            this.metroPanel21.Controls.Add(this.iP4PrintToTxb);
            this.metroPanel21.Controls.Add(this.iP4PrintFromLbl);
            this.metroPanel21.HorizontalScrollbarBarColor = true;
            this.metroPanel21.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel21.HorizontalScrollbarSize = 10;
            this.metroPanel21.Location = new System.Drawing.Point(728, 615);
            this.metroPanel21.Name = "metroPanel21";
            this.metroPanel21.Size = new System.Drawing.Size(346, 40);
            this.metroPanel21.TabIndex = 199;
            this.metroPanel21.VerticalScrollbarBarColor = true;
            this.metroPanel21.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel21.VerticalScrollbarSize = 10;
            // 
            // iP4PrintAllChk
            // 
            this.iP4PrintAllChk.AutoSize = true;
            this.iP4PrintAllChk.Checked = true;
            this.iP4PrintAllChk.CheckState = System.Windows.Forms.CheckState.Checked;
            this.iP4PrintAllChk.FontSize = MetroFramework.MetroCheckBoxSize.Tall;
            this.iP4PrintAllChk.Location = new System.Drawing.Point(8, 7);
            this.iP4PrintAllChk.Name = "iP4PrintAllChk";
            this.iP4PrintAllChk.Size = new System.Drawing.Size(111, 25);
            this.iP4PrintAllChk.TabIndex = 18;
            this.iP4PrintAllChk.Text = "พิมพ์ทั้งหมด";
            this.iP4PrintAllChk.UseSelectable = true;
            this.iP4PrintAllChk.CheckedChanged += new System.EventHandler(this.IP4PrintAllChk_CheckedChanged);
            // 
            // iP4PrintToLbl
            // 
            this.iP4PrintToLbl.AutoSize = true;
            this.iP4PrintToLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP4PrintToLbl.Location = new System.Drawing.Point(230, 7);
            this.iP4PrintToLbl.Name = "iP4PrintToLbl";
            this.iP4PrintToLbl.Size = new System.Drawing.Size(29, 25);
            this.iP4PrintToLbl.TabIndex = 22;
            this.iP4PrintToLbl.Text = "ถึง";
            // 
            // iP4PrintFromTxb
            // 
            // 
            // 
            // 
            this.iP4PrintFromTxb.CustomButton.Image = null;
            this.iP4PrintFromTxb.CustomButton.Location = new System.Drawing.Point(32, 2);
            this.iP4PrintFromTxb.CustomButton.Name = "";
            this.iP4PrintFromTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.iP4PrintFromTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.iP4PrintFromTxb.CustomButton.TabIndex = 1;
            this.iP4PrintFromTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.iP4PrintFromTxb.CustomButton.UseSelectable = true;
            this.iP4PrintFromTxb.CustomButton.Visible = false;
            this.iP4PrintFromTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.iP4PrintFromTxb.Lines = new string[0];
            this.iP4PrintFromTxb.Location = new System.Drawing.Point(164, 4);
            this.iP4PrintFromTxb.MaxLength = 32767;
            this.iP4PrintFromTxb.Name = "iP4PrintFromTxb";
            this.iP4PrintFromTxb.PasswordChar = '\0';
            this.iP4PrintFromTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.iP4PrintFromTxb.SelectedText = "";
            this.iP4PrintFromTxb.SelectionLength = 0;
            this.iP4PrintFromTxb.SelectionStart = 0;
            this.iP4PrintFromTxb.ShortcutsEnabled = true;
            this.iP4PrintFromTxb.Size = new System.Drawing.Size(60, 30);
            this.iP4PrintFromTxb.TabIndex = 19;
            this.iP4PrintFromTxb.UseSelectable = true;
            this.iP4PrintFromTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.iP4PrintFromTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.iP4PrintFromTxb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IP4PrintFromTxb_KeyPress);
            // 
            // iP4PrintToTxb
            // 
            // 
            // 
            // 
            this.iP4PrintToTxb.CustomButton.Image = null;
            this.iP4PrintToTxb.CustomButton.Location = new System.Drawing.Point(40, 2);
            this.iP4PrintToTxb.CustomButton.Name = "";
            this.iP4PrintToTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.iP4PrintToTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.iP4PrintToTxb.CustomButton.TabIndex = 1;
            this.iP4PrintToTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.iP4PrintToTxb.CustomButton.UseSelectable = true;
            this.iP4PrintToTxb.CustomButton.Visible = false;
            this.iP4PrintToTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.iP4PrintToTxb.Lines = new string[0];
            this.iP4PrintToTxb.Location = new System.Drawing.Point(265, 4);
            this.iP4PrintToTxb.MaxLength = 32767;
            this.iP4PrintToTxb.Name = "iP4PrintToTxb";
            this.iP4PrintToTxb.PasswordChar = '\0';
            this.iP4PrintToTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.iP4PrintToTxb.SelectedText = "";
            this.iP4PrintToTxb.SelectionLength = 0;
            this.iP4PrintToTxb.SelectionStart = 0;
            this.iP4PrintToTxb.ShortcutsEnabled = true;
            this.iP4PrintToTxb.Size = new System.Drawing.Size(68, 30);
            this.iP4PrintToTxb.TabIndex = 20;
            this.iP4PrintToTxb.UseSelectable = true;
            this.iP4PrintToTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.iP4PrintToTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.iP4PrintToTxb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IP4PrintToTxb_KeyPress);
            // 
            // iP4PrintFromLbl
            // 
            this.iP4PrintFromLbl.AutoSize = true;
            this.iP4PrintFromLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP4PrintFromLbl.Location = new System.Drawing.Point(126, 7);
            this.iP4PrintFromLbl.Name = "iP4PrintFromLbl";
            this.iP4PrintFromLbl.Size = new System.Drawing.Size(39, 25);
            this.iP4PrintFromLbl.TabIndex = 21;
            this.iP4PrintFromLbl.Text = "จาก";
            // 
            // iP4CancelBtn
            // 
            this.iP4CancelBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.iP4CancelBtn.BackColor = System.Drawing.Color.IndianRed;
            this.iP4CancelBtn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.iP4CancelBtn.ForeColor = System.Drawing.Color.MistyRose;
            this.iP4CancelBtn.Location = new System.Drawing.Point(1092, 615);
            this.iP4CancelBtn.Name = "iP4CancelBtn";
            this.iP4CancelBtn.Size = new System.Drawing.Size(150, 40);
            this.iP4CancelBtn.TabIndex = 195;
            this.iP4CancelBtn.Text = "ออกจากหน้านี้";
            this.iP4CancelBtn.UseCustomBackColor = true;
            this.iP4CancelBtn.UseCustomForeColor = true;
            this.iP4CancelBtn.UseSelectable = true;
            this.iP4CancelBtn.Click += new System.EventHandler(this.IP4CancelBtn_Click);
            // 
            // iP4TotalWeightPnl
            // 
            this.iP4TotalWeightPnl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.iP4TotalWeightPnl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.iP4TotalWeightPnl.Controls.Add(this.metroPanel7);
            this.iP4TotalWeightPnl.Controls.Add(this.metroPanel15);
            this.iP4TotalWeightPnl.Controls.Add(this.metroPanel19);
            this.iP4TotalWeightPnl.Controls.Add(this.iP4TotalWeightLbl);
            this.iP4TotalWeightPnl.HorizontalScrollbarBarColor = true;
            this.iP4TotalWeightPnl.HorizontalScrollbarHighlightOnWheel = false;
            this.iP4TotalWeightPnl.HorizontalScrollbarSize = 10;
            this.iP4TotalWeightPnl.Location = new System.Drawing.Point(764, 355);
            this.iP4TotalWeightPnl.Name = "iP4TotalWeightPnl";
            this.iP4TotalWeightPnl.Size = new System.Drawing.Size(474, 51);
            this.iP4TotalWeightPnl.TabIndex = 186;
            this.iP4TotalWeightPnl.VerticalScrollbarBarColor = true;
            this.iP4TotalWeightPnl.VerticalScrollbarHighlightOnWheel = false;
            this.iP4TotalWeightPnl.VerticalScrollbarSize = 10;
            // 
            // metroPanel7
            // 
            this.metroPanel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.metroPanel7.Controls.Add(this.iP4TotalCountLbl);
            this.metroPanel7.Controls.Add(this.iP4TotalCountTxb);
            this.metroPanel7.Controls.Add(this.iP4TotalCountUnitLbl);
            this.metroPanel7.HorizontalScrollbarBarColor = true;
            this.metroPanel7.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel7.HorizontalScrollbarSize = 10;
            this.metroPanel7.Location = new System.Drawing.Point(-5, 25);
            this.metroPanel7.Name = "metroPanel7";
            this.metroPanel7.Size = new System.Drawing.Size(129, 38);
            this.metroPanel7.TabIndex = 98;
            this.metroPanel7.VerticalScrollbarBarColor = true;
            this.metroPanel7.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel7.VerticalScrollbarSize = 10;
            // 
            // iP4TotalCountLbl
            // 
            this.iP4TotalCountLbl.AutoSize = true;
            this.iP4TotalCountLbl.Location = new System.Drawing.Point(6, 3);
            this.iP4TotalCountLbl.Name = "iP4TotalCountLbl";
            this.iP4TotalCountLbl.Size = new System.Drawing.Size(46, 19);
            this.iP4TotalCountLbl.TabIndex = 164;
            this.iP4TotalCountLbl.Text = "ทั้งหมด";
            // 
            // iP4TotalCountTxb
            // 
            // 
            // 
            // 
            this.iP4TotalCountTxb.CustomButton.Image = null;
            this.iP4TotalCountTxb.CustomButton.Location = new System.Drawing.Point(23, 1);
            this.iP4TotalCountTxb.CustomButton.Name = "";
            this.iP4TotalCountTxb.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.iP4TotalCountTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.iP4TotalCountTxb.CustomButton.TabIndex = 1;
            this.iP4TotalCountTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.iP4TotalCountTxb.CustomButton.UseSelectable = true;
            this.iP4TotalCountTxb.CustomButton.Visible = false;
            this.iP4TotalCountTxb.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.iP4TotalCountTxb.ForeColor = System.Drawing.Color.Gray;
            this.iP4TotalCountTxb.Lines = new string[0];
            this.iP4TotalCountTxb.Location = new System.Drawing.Point(54, 0);
            this.iP4TotalCountTxb.MaxLength = 32767;
            this.iP4TotalCountTxb.Name = "iP4TotalCountTxb";
            this.iP4TotalCountTxb.PasswordChar = '\0';
            this.iP4TotalCountTxb.ReadOnly = true;
            this.iP4TotalCountTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.iP4TotalCountTxb.SelectedText = "";
            this.iP4TotalCountTxb.SelectionLength = 0;
            this.iP4TotalCountTxb.SelectionStart = 0;
            this.iP4TotalCountTxb.ShortcutsEnabled = true;
            this.iP4TotalCountTxb.Size = new System.Drawing.Size(45, 23);
            this.iP4TotalCountTxb.TabIndex = 96;
            this.iP4TotalCountTxb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.iP4TotalCountTxb.UseCustomForeColor = true;
            this.iP4TotalCountTxb.UseSelectable = true;
            this.iP4TotalCountTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.iP4TotalCountTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // iP4TotalCountUnitLbl
            // 
            this.iP4TotalCountUnitLbl.AutoSize = true;
            this.iP4TotalCountUnitLbl.Location = new System.Drawing.Point(100, 3);
            this.iP4TotalCountUnitLbl.Name = "iP4TotalCountUnitLbl";
            this.iP4TotalCountUnitLbl.Size = new System.Drawing.Size(25, 19);
            this.iP4TotalCountUnitLbl.TabIndex = 97;
            this.iP4TotalCountUnitLbl.Text = "ชิ้น";
            // 
            // metroPanel15
            // 
            this.metroPanel15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.metroPanel15.Controls.Add(this.metroLabel14);
            this.metroPanel15.Controls.Add(this.iP4TotalNetWeightUnitLbl);
            this.metroPanel15.Controls.Add(this.iP4TotalNetWeightTxb);
            this.metroPanel15.HorizontalScrollbarBarColor = true;
            this.metroPanel15.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel15.HorizontalScrollbarSize = 10;
            this.metroPanel15.Location = new System.Drawing.Point(297, 25);
            this.metroPanel15.Name = "metroPanel15";
            this.metroPanel15.Size = new System.Drawing.Size(178, 38);
            this.metroPanel15.TabIndex = 97;
            this.metroPanel15.VerticalScrollbarBarColor = true;
            this.metroPanel15.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel15.VerticalScrollbarSize = 10;
            // 
            // metroLabel14
            // 
            this.metroLabel14.AutoSize = true;
            this.metroLabel14.Location = new System.Drawing.Point(3, 2);
            this.metroLabel14.Name = "metroLabel14";
            this.metroLabel14.Size = new System.Drawing.Size(30, 19);
            this.metroLabel14.TabIndex = 199;
            this.metroLabel14.Text = "Net";
            // 
            // iP4TotalNetWeightUnitLbl
            // 
            this.iP4TotalNetWeightUnitLbl.AutoSize = true;
            this.iP4TotalNetWeightUnitLbl.Location = new System.Drawing.Point(119, 2);
            this.iP4TotalNetWeightUnitLbl.Name = "iP4TotalNetWeightUnitLbl";
            this.iP4TotalNetWeightUnitLbl.Size = new System.Drawing.Size(27, 19);
            this.iP4TotalNetWeightUnitLbl.TabIndex = 100;
            this.iP4TotalNetWeightUnitLbl.Text = "Kg.";
            // 
            // iP4TotalNetWeightTxb
            // 
            // 
            // 
            // 
            this.iP4TotalNetWeightTxb.CustomButton.Image = null;
            this.iP4TotalNetWeightTxb.CustomButton.Location = new System.Drawing.Point(47, 1);
            this.iP4TotalNetWeightTxb.CustomButton.Name = "";
            this.iP4TotalNetWeightTxb.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.iP4TotalNetWeightTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.iP4TotalNetWeightTxb.CustomButton.TabIndex = 1;
            this.iP4TotalNetWeightTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.iP4TotalNetWeightTxb.CustomButton.UseSelectable = true;
            this.iP4TotalNetWeightTxb.CustomButton.Visible = false;
            this.iP4TotalNetWeightTxb.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.iP4TotalNetWeightTxb.ForeColor = System.Drawing.Color.Gray;
            this.iP4TotalNetWeightTxb.Lines = new string[0];
            this.iP4TotalNetWeightTxb.Location = new System.Drawing.Point(44, 0);
            this.iP4TotalNetWeightTxb.MaxLength = 32767;
            this.iP4TotalNetWeightTxb.Name = "iP4TotalNetWeightTxb";
            this.iP4TotalNetWeightTxb.PasswordChar = '\0';
            this.iP4TotalNetWeightTxb.ReadOnly = true;
            this.iP4TotalNetWeightTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.iP4TotalNetWeightTxb.SelectedText = "";
            this.iP4TotalNetWeightTxb.SelectionLength = 0;
            this.iP4TotalNetWeightTxb.SelectionStart = 0;
            this.iP4TotalNetWeightTxb.ShortcutsEnabled = true;
            this.iP4TotalNetWeightTxb.Size = new System.Drawing.Size(69, 23);
            this.iP4TotalNetWeightTxb.TabIndex = 99;
            this.iP4TotalNetWeightTxb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.iP4TotalNetWeightTxb.UseCustomForeColor = true;
            this.iP4TotalNetWeightTxb.UseSelectable = true;
            this.iP4TotalNetWeightTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.iP4TotalNetWeightTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroPanel19
            // 
            this.metroPanel19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.metroPanel19.Controls.Add(this.metroLabel13);
            this.metroPanel19.Controls.Add(this.iP4TotalGrossWeightTxb);
            this.metroPanel19.Controls.Add(this.iP4TotalGrossWeightUnitLbl);
            this.metroPanel19.HorizontalScrollbarBarColor = true;
            this.metroPanel19.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel19.HorizontalScrollbarSize = 10;
            this.metroPanel19.Location = new System.Drawing.Point(118, 25);
            this.metroPanel19.Name = "metroPanel19";
            this.metroPanel19.Size = new System.Drawing.Size(211, 38);
            this.metroPanel19.TabIndex = 96;
            this.metroPanel19.VerticalScrollbarBarColor = true;
            this.metroPanel19.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel19.VerticalScrollbarSize = 10;
            // 
            // metroLabel13
            // 
            this.metroLabel13.AutoSize = true;
            this.metroLabel13.Location = new System.Drawing.Point(7, 2);
            this.metroLabel13.Name = "metroLabel13";
            this.metroLabel13.Size = new System.Drawing.Size(41, 19);
            this.metroLabel13.TabIndex = 199;
            this.metroLabel13.Text = "Gross";
            // 
            // iP4TotalGrossWeightTxb
            // 
            // 
            // 
            // 
            this.iP4TotalGrossWeightTxb.CustomButton.Image = null;
            this.iP4TotalGrossWeightTxb.CustomButton.Location = new System.Drawing.Point(47, 1);
            this.iP4TotalGrossWeightTxb.CustomButton.Name = "";
            this.iP4TotalGrossWeightTxb.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.iP4TotalGrossWeightTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.iP4TotalGrossWeightTxb.CustomButton.TabIndex = 1;
            this.iP4TotalGrossWeightTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.iP4TotalGrossWeightTxb.CustomButton.UseSelectable = true;
            this.iP4TotalGrossWeightTxb.CustomButton.Visible = false;
            this.iP4TotalGrossWeightTxb.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.iP4TotalGrossWeightTxb.ForeColor = System.Drawing.Color.Gray;
            this.iP4TotalGrossWeightTxb.Lines = new string[0];
            this.iP4TotalGrossWeightTxb.Location = new System.Drawing.Point(50, 0);
            this.iP4TotalGrossWeightTxb.MaxLength = 32767;
            this.iP4TotalGrossWeightTxb.Name = "iP4TotalGrossWeightTxb";
            this.iP4TotalGrossWeightTxb.PasswordChar = '\0';
            this.iP4TotalGrossWeightTxb.ReadOnly = true;
            this.iP4TotalGrossWeightTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.iP4TotalGrossWeightTxb.SelectedText = "";
            this.iP4TotalGrossWeightTxb.SelectionLength = 0;
            this.iP4TotalGrossWeightTxb.SelectionStart = 0;
            this.iP4TotalGrossWeightTxb.ShortcutsEnabled = true;
            this.iP4TotalGrossWeightTxb.Size = new System.Drawing.Size(69, 23);
            this.iP4TotalGrossWeightTxb.TabIndex = 96;
            this.iP4TotalGrossWeightTxb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.iP4TotalGrossWeightTxb.UseCustomForeColor = true;
            this.iP4TotalGrossWeightTxb.UseSelectable = true;
            this.iP4TotalGrossWeightTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.iP4TotalGrossWeightTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // iP4TotalGrossWeightUnitLbl
            // 
            this.iP4TotalGrossWeightUnitLbl.AutoSize = true;
            this.iP4TotalGrossWeightUnitLbl.Location = new System.Drawing.Point(123, 2);
            this.iP4TotalGrossWeightUnitLbl.Name = "iP4TotalGrossWeightUnitLbl";
            this.iP4TotalGrossWeightUnitLbl.Size = new System.Drawing.Size(27, 19);
            this.iP4TotalGrossWeightUnitLbl.TabIndex = 97;
            this.iP4TotalGrossWeightUnitLbl.Text = "Kg.";
            // 
            // iP4TotalWeightLbl
            // 
            this.iP4TotalWeightLbl.AutoSize = true;
            this.iP4TotalWeightLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP4TotalWeightLbl.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.iP4TotalWeightLbl.Location = new System.Drawing.Point(185, -1);
            this.iP4TotalWeightLbl.Name = "iP4TotalWeightLbl";
            this.iP4TotalWeightLbl.Size = new System.Drawing.Size(91, 25);
            this.iP4TotalWeightLbl.TabIndex = 95;
            this.iP4TotalWeightLbl.Text = "ปริมาณรวม";
            // 
            // iP4JobReceiveItemDgv
            // 
            this.iP4JobReceiveItemDgv.AllowUserToAddRows = false;
            this.iP4JobReceiveItemDgv.AllowUserToDeleteRows = false;
            this.iP4JobReceiveItemDgv.AllowUserToOrderColumns = true;
            this.iP4JobReceiveItemDgv.AllowUserToResizeColumns = false;
            this.iP4JobReceiveItemDgv.AllowUserToResizeRows = false;
            this.iP4JobReceiveItemDgv.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.iP4JobReceiveItemDgv.BackgroundColor = System.Drawing.SystemColors.Window;
            this.iP4JobReceiveItemDgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.iP4JobReceiveItemDgv.Location = new System.Drawing.Point(1, 405);
            this.iP4JobReceiveItemDgv.Name = "iP4JobReceiveItemDgv";
            this.iP4JobReceiveItemDgv.RowHeadersVisible = false;
            this.iP4JobReceiveItemDgv.RowHeadersWidth = 62;
            this.iP4JobReceiveItemDgv.Size = new System.Drawing.Size(1237, 206);
            this.iP4JobReceiveItemDgv.TabIndex = 189;
            this.iP4JobReceiveItemDgv.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.IP4JobReceiveItemDgv_CellClick);
            this.iP4JobReceiveItemDgv.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.IP4JobReceiveItemDgv_CellValueChanged);
            this.iP4JobReceiveItemDgv.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.IP4JobReceiveItemDgv_RowPrePaint);
            // 
            // iP4StartOptionsPanel
            // 
            this.iP4StartOptionsPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.iP4StartOptionsPanel.Controls.Add(this.iP4StartOptionsLbl);
            this.iP4StartOptionsPanel.Controls.Add(this.iP4StartOptionsCbb);
            this.iP4StartOptionsPanel.HorizontalScrollbarBarColor = true;
            this.iP4StartOptionsPanel.HorizontalScrollbarHighlightOnWheel = false;
            this.iP4StartOptionsPanel.HorizontalScrollbarSize = 10;
            this.iP4StartOptionsPanel.Location = new System.Drawing.Point(-4, 50);
            this.iP4StartOptionsPanel.Name = "iP4StartOptionsPanel";
            this.iP4StartOptionsPanel.Size = new System.Drawing.Size(1243, 608);
            this.iP4StartOptionsPanel.TabIndex = 145;
            this.iP4StartOptionsPanel.VerticalScrollbarBarColor = true;
            this.iP4StartOptionsPanel.VerticalScrollbarHighlightOnWheel = false;
            this.iP4StartOptionsPanel.VerticalScrollbarSize = 10;
            // 
            // iP4StartOptionsLbl
            // 
            this.iP4StartOptionsLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP4StartOptionsLbl.AutoSize = true;
            this.iP4StartOptionsLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP4StartOptionsLbl.Location = new System.Drawing.Point(311, 21);
            this.iP4StartOptionsLbl.Name = "iP4StartOptionsLbl";
            this.iP4StartOptionsLbl.Size = new System.Drawing.Size(131, 25);
            this.iP4StartOptionsLbl.TabIndex = 3;
            this.iP4StartOptionsLbl.Text = "กรุณาเลือกบันทึก";
            // 
            // iP4StartOptionsCbb
            // 
            this.iP4StartOptionsCbb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP4StartOptionsCbb.FormattingEnabled = true;
            this.iP4StartOptionsCbb.ItemHeight = 23;
            this.iP4StartOptionsCbb.Location = new System.Drawing.Point(457, 20);
            this.iP4StartOptionsCbb.Name = "iP4StartOptionsCbb";
            this.iP4StartOptionsCbb.Size = new System.Drawing.Size(331, 29);
            this.iP4StartOptionsCbb.TabIndex = 2;
            this.iP4StartOptionsCbb.UseSelectable = true;
            this.iP4StartOptionsCbb.SelectedIndexChanged += new System.EventHandler(this.IP4StartOptionsCbb_SelectedIndexChanged);
            // 
            // iP5Pnl
            // 
            this.iP5Pnl.Controls.Add(this.iP5issueListCbb);
            this.iP5Pnl.Controls.Add(this.iP5TopLbl);
            this.iP5Pnl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.iP5Pnl.HorizontalScrollbarBarColor = true;
            this.iP5Pnl.HorizontalScrollbarHighlightOnWheel = false;
            this.iP5Pnl.HorizontalScrollbarSize = 10;
            this.iP5Pnl.Location = new System.Drawing.Point(0, 0);
            this.iP5Pnl.Name = "iP5Pnl";
            this.iP5Pnl.Size = new System.Drawing.Size(1238, 663);
            this.iP5Pnl.TabIndex = 197;
            this.iP5Pnl.VerticalScrollbarBarColor = true;
            this.iP5Pnl.VerticalScrollbarHighlightOnWheel = false;
            this.iP5Pnl.VerticalScrollbarSize = 10;
            // 
            // iP5issueListCbb
            // 
            this.iP5issueListCbb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP5issueListCbb.FontSize = MetroFramework.MetroComboBoxSize.Tall;
            this.iP5issueListCbb.FormattingEnabled = true;
            this.iP5issueListCbb.ItemHeight = 29;
            this.iP5issueListCbb.Location = new System.Drawing.Point(469, 92);
            this.iP5issueListCbb.Name = "iP5issueListCbb";
            this.iP5issueListCbb.Size = new System.Drawing.Size(300, 35);
            this.iP5issueListCbb.TabIndex = 3;
            this.iP5issueListCbb.UseSelectable = true;
            this.iP5issueListCbb.SelectedIndexChanged += new System.EventHandler(this.iP5issueListCbb_SelectedIndexChanged);
            // 
            // iP5TopLbl
            // 
            this.iP5TopLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iP5TopLbl.AutoSize = true;
            this.iP5TopLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.iP5TopLbl.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.iP5TopLbl.Location = new System.Drawing.Point(488, 36);
            this.iP5TopLbl.Name = "iP5TopLbl";
            this.iP5TopLbl.Size = new System.Drawing.Size(263, 25);
            this.iP5TopLbl.TabIndex = 2;
            this.iP5TopLbl.Text = "เลือกหรือสร้างรายการเบิกวัตถุดิบ";
            // 
            // delArrTabPage
            // 
            this.delArrTabPage.Controls.Add(this.dO1Pnl);
            this.delArrTabPage.Controls.Add(this.dL1Pnl);
            this.delArrTabPage.Controls.Add(this.dL3Pnl);
            this.delArrTabPage.Controls.Add(this.dL2Pnl);
            this.delArrTabPage.HorizontalScrollbarBarColor = true;
            this.delArrTabPage.HorizontalScrollbarHighlightOnWheel = false;
            this.delArrTabPage.HorizontalScrollbarSize = 0;
            this.delArrTabPage.Location = new System.Drawing.Point(4, 47);
            this.delArrTabPage.Name = "delArrTabPage";
            this.delArrTabPage.Size = new System.Drawing.Size(1238, 663);
            this.delArrTabPage.TabIndex = 2;
            this.delArrTabPage.Text = " จัดสินค้า";
            this.delArrTabPage.VerticalScrollbarBarColor = true;
            this.delArrTabPage.VerticalScrollbarHighlightOnWheel = false;
            this.delArrTabPage.VerticalScrollbarSize = 0;
            this.delArrTabPage.Enter += new System.EventHandler(this.DelArrTabPage_Enter);
            // 
            // dO1Pnl
            // 
            this.dO1Pnl.Controls.Add(this.dO1MessageLbl);
            this.dO1Pnl.Controls.Add(this.dO1TerminateDoBtn);
            this.dO1Pnl.Controls.Add(this.dO1SendToB1Btn);
            this.dO1Pnl.Controls.Add(this.dO1TopLbl);
            this.dO1Pnl.Controls.Add(this.dO1BarcodePnl);
            this.dO1Pnl.Controls.Add(this.dO1MagicRandomItemBtn);
            this.dO1Pnl.Controls.Add(this.dO1ItemDgv);
            this.dO1Pnl.Controls.Add(this.dO1TelephoneTxb);
            this.dO1Pnl.Controls.Add(this.dO1TelephoneLbl);
            this.dO1Pnl.Controls.Add(this.dO1WeightLbsUnitLbl);
            this.dO1Pnl.Controls.Add(this.dO1WeightLbsTxb);
            this.dO1Pnl.Controls.Add(this.dO1EqualLbl);
            this.dO1Pnl.Controls.Add(this.dO1NetWeightUnitLbl);
            this.dO1Pnl.Controls.Add(this.dO1NetWeightTxb);
            this.dO1Pnl.Controls.Add(this.dO1NetWeightLbl);
            this.dO1Pnl.Controls.Add(this.dO1TareLbl);
            this.dO1Pnl.Controls.Add(this.dO1TareUnitLbl);
            this.dO1Pnl.Controls.Add(this.dO1TareTxb);
            this.dO1Pnl.Controls.Add(this.dO1QtyWeightUnitLbl);
            this.dO1Pnl.Controls.Add(this.dO1QtyWeightTxb);
            this.dO1Pnl.Controls.Add(this.dO1QtyWeightLbl);
            this.dO1Pnl.Controls.Add(this.dO1QtyCountUnitLbl);
            this.dO1Pnl.Controls.Add(this.dO1QtyCountTxb);
            this.dO1Pnl.Controls.Add(this.dO1QtyCountLbl);
            this.dO1Pnl.Controls.Add(this.dO1ItemCodeTxb);
            this.dO1Pnl.Controls.Add(this.dO1ItemCodeLbl);
            this.dO1Pnl.Controls.Add(this.dO1ItemDescLbl);
            this.dO1Pnl.Controls.Add(this.dO1ItemDescTxb);
            this.dO1Pnl.Controls.Add(this.dO1CustomerTxb);
            this.dO1Pnl.Controls.Add(this.dO1AssignDateTxb);
            this.dO1Pnl.Controls.Add(this.dO1CancelBtn);
            this.dO1Pnl.Controls.Add(this.dO1PrintFormBtn);
            this.dO1Pnl.Controls.Add(this.dO1SubmitBtn);
            this.dO1Pnl.Controls.Add(this.dO1DocNoLbl);
            this.dO1Pnl.Controls.Add(this.dO1AssignDateLbl);
            this.dO1Pnl.Controls.Add(this.dO1RefNoLbl);
            this.dO1Pnl.Controls.Add(this.dO1RefNoTxb);
            this.dO1Pnl.Controls.Add(this.dO1DocNoTxb);
            this.dO1Pnl.Controls.Add(this.dO1CustomerLbl);
            this.dO1Pnl.Controls.Add(this.dO1AddressLbl);
            this.dO1Pnl.Controls.Add(this.dO1AddressTxb);
            this.dO1Pnl.Controls.Add(this.dO1RemarkTxb);
            this.dO1Pnl.Controls.Add(this.dO1RemarkLbl);
            this.dO1Pnl.HorizontalScrollbarBarColor = true;
            this.dO1Pnl.HorizontalScrollbarHighlightOnWheel = false;
            this.dO1Pnl.HorizontalScrollbarSize = 10;
            this.dO1Pnl.Location = new System.Drawing.Point(0, 0);
            this.dO1Pnl.Name = "dO1Pnl";
            this.dO1Pnl.Size = new System.Drawing.Size(1238, 663);
            this.dO1Pnl.TabIndex = 43;
            this.dO1Pnl.VerticalScrollbarBarColor = true;
            this.dO1Pnl.VerticalScrollbarHighlightOnWheel = false;
            this.dO1Pnl.VerticalScrollbarSize = 10;
            this.dO1Pnl.MouseHover += new System.EventHandler(this.DO1Pnl_MouseHover);
            this.dO1Pnl.Validated += new System.EventHandler(this.dO1Pnl_Validated);
            // 
            // dO1MessageLbl
            // 
            this.dO1MessageLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dO1MessageLbl.AutoSize = true;
            this.dO1MessageLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.dO1MessageLbl.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.dO1MessageLbl.ForeColor = System.Drawing.Color.DarkRed;
            this.dO1MessageLbl.Location = new System.Drawing.Point(525, 363);
            this.dO1MessageLbl.Name = "dO1MessageLbl";
            this.dO1MessageLbl.Size = new System.Drawing.Size(0, 0);
            this.dO1MessageLbl.TabIndex = 177;
            this.dO1MessageLbl.UseCustomForeColor = true;
            // 
            // dO1TerminateDoBtn
            // 
            this.dO1TerminateDoBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.dO1TerminateDoBtn.BackColor = System.Drawing.Color.IndianRed;
            this.dO1TerminateDoBtn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.dO1TerminateDoBtn.ForeColor = System.Drawing.Color.MistyRose;
            this.dO1TerminateDoBtn.Location = new System.Drawing.Point(13, 615);
            this.dO1TerminateDoBtn.Name = "dO1TerminateDoBtn";
            this.dO1TerminateDoBtn.Size = new System.Drawing.Size(150, 40);
            this.dO1TerminateDoBtn.TabIndex = 176;
            this.dO1TerminateDoBtn.Text = "ยกเลิกใบส่ง";
            this.dO1TerminateDoBtn.UseCustomBackColor = true;
            this.dO1TerminateDoBtn.UseCustomForeColor = true;
            this.dO1TerminateDoBtn.UseSelectable = true;
            this.dO1TerminateDoBtn.Click += new System.EventHandler(this.dO1TerminateDoBtn_Click);
            // 
            // dO1SendToB1Btn
            // 
            this.dO1SendToB1Btn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.dO1SendToB1Btn.BackColor = System.Drawing.Color.SteelBlue;
            this.dO1SendToB1Btn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.dO1SendToB1Btn.ForeColor = System.Drawing.Color.LightCyan;
            this.dO1SendToB1Btn.Location = new System.Drawing.Point(526, 615);
            this.dO1SendToB1Btn.Name = "dO1SendToB1Btn";
            this.dO1SendToB1Btn.Size = new System.Drawing.Size(150, 40);
            this.dO1SendToB1Btn.TabIndex = 175;
            this.dO1SendToB1Btn.Text = "ส่งขึ้น B1";
            this.dO1SendToB1Btn.UseCustomBackColor = true;
            this.dO1SendToB1Btn.UseCustomForeColor = true;
            this.dO1SendToB1Btn.UseSelectable = true;
            this.dO1SendToB1Btn.Click += new System.EventHandler(this.dO1SendToB1Btn_Click);
            // 
            // dO1TopLbl
            // 
            this.dO1TopLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dO1TopLbl.AutoSize = true;
            this.dO1TopLbl.BackColor = System.Drawing.Color.White;
            this.dO1TopLbl.Font = new System.Drawing.Font("Microsoft Tai Le", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dO1TopLbl.Location = new System.Drawing.Point(558, 21);
            this.dO1TopLbl.Name = "dO1TopLbl";
            this.dO1TopLbl.Size = new System.Drawing.Size(122, 34);
            this.dO1TopLbl.TabIndex = 174;
            this.dO1TopLbl.Text = "ใบส่งสินค้า";
            // 
            // dO1BarcodePnl
            // 
            this.dO1BarcodePnl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dO1BarcodePnl.BackColor = System.Drawing.Color.Transparent;
            this.dO1BarcodePnl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dO1BarcodePnl.Controls.Add(this.metroLabel5);
            this.dO1BarcodePnl.HorizontalScrollbarBarColor = true;
            this.dO1BarcodePnl.HorizontalScrollbarHighlightOnWheel = false;
            this.dO1BarcodePnl.HorizontalScrollbarSize = 10;
            this.dO1BarcodePnl.Location = new System.Drawing.Point(5, 5);
            this.dO1BarcodePnl.Name = "dO1BarcodePnl";
            this.dO1BarcodePnl.Size = new System.Drawing.Size(281, 83);
            this.dO1BarcodePnl.TabIndex = 173;
            this.dO1BarcodePnl.UseCustomBackColor = true;
            this.dO1BarcodePnl.VerticalScrollbarBarColor = true;
            this.dO1BarcodePnl.VerticalScrollbarHighlightOnWheel = false;
            this.dO1BarcodePnl.VerticalScrollbarSize = 10;
            this.dO1BarcodePnl.MouseLeave += new System.EventHandler(this.dO1BarcodePnl_MouseLeave);
            this.dO1BarcodePnl.MouseHover += new System.EventHandler(this.dO1BarcodePnl_MouseHover);
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(89, 7);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(90, 19);
            this.metroLabel5.TabIndex = 2;
            this.metroLabel5.Text = "Barcode Area";
            this.metroLabel5.UseCustomBackColor = true;
            // 
            // dO1MagicRandomItemBtn
            // 
            this.dO1MagicRandomItemBtn.DisplayFocus = true;
            this.dO1MagicRandomItemBtn.Highlight = true;
            this.dO1MagicRandomItemBtn.Location = new System.Drawing.Point(925, 160);
            this.dO1MagicRandomItemBtn.Name = "dO1MagicRandomItemBtn";
            this.dO1MagicRandomItemBtn.Size = new System.Drawing.Size(302, 47);
            this.dO1MagicRandomItemBtn.TabIndex = 172;
            this.dO1MagicRandomItemBtn.Text = "Scan some random item!";
            this.dO1MagicRandomItemBtn.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.dO1MagicRandomItemBtn.UseSelectable = true;
            this.dO1MagicRandomItemBtn.Click += new System.EventHandler(this.DO1MagicRandomItemBtn_Click);
            // 
            // dO1ItemDgv
            // 
            this.dO1ItemDgv.AllowUserToAddRows = false;
            this.dO1ItemDgv.AllowUserToDeleteRows = false;
            this.dO1ItemDgv.AllowUserToResizeRows = false;
            this.dO1ItemDgv.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dO1ItemDgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dO1ItemDgv.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dO1ItemDgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dO1ItemDgv.Location = new System.Drawing.Point(5, 248);
            this.dO1ItemDgv.Name = "dO1ItemDgv";
            this.dO1ItemDgv.ReadOnly = true;
            this.dO1ItemDgv.RowHeadersVisible = false;
            this.dO1ItemDgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dO1ItemDgv.Size = new System.Drawing.Size(1225, 302);
            this.dO1ItemDgv.TabIndex = 124;
            this.dO1ItemDgv.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DO1ItemDgv_CellClick);
            this.dO1ItemDgv.MouseHover += new System.EventHandler(this.DO1ItemDgv_MouseHover);
            // 
            // dO1TelephoneTxb
            // 
            this.dO1TelephoneTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.dO1TelephoneTxb.CustomButton.Image = null;
            this.dO1TelephoneTxb.CustomButton.Location = new System.Drawing.Point(279, 2);
            this.dO1TelephoneTxb.CustomButton.Margin = new System.Windows.Forms.Padding(2);
            this.dO1TelephoneTxb.CustomButton.Name = "";
            this.dO1TelephoneTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.dO1TelephoneTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.dO1TelephoneTxb.CustomButton.TabIndex = 1;
            this.dO1TelephoneTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.dO1TelephoneTxb.CustomButton.UseSelectable = true;
            this.dO1TelephoneTxb.CustomButton.Visible = false;
            this.dO1TelephoneTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.dO1TelephoneTxb.ForeColor = System.Drawing.Color.Gray;
            this.dO1TelephoneTxb.Lines = new string[0];
            this.dO1TelephoneTxb.Location = new System.Drawing.Point(135, 139);
            this.dO1TelephoneTxb.MaxLength = 32767;
            this.dO1TelephoneTxb.Name = "dO1TelephoneTxb";
            this.dO1TelephoneTxb.PasswordChar = '\0';
            this.dO1TelephoneTxb.ReadOnly = true;
            this.dO1TelephoneTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dO1TelephoneTxb.SelectedText = "";
            this.dO1TelephoneTxb.SelectionLength = 0;
            this.dO1TelephoneTxb.SelectionStart = 0;
            this.dO1TelephoneTxb.ShortcutsEnabled = true;
            this.dO1TelephoneTxb.Size = new System.Drawing.Size(307, 30);
            this.dO1TelephoneTxb.TabIndex = 123;
            this.dO1TelephoneTxb.UseCustomForeColor = true;
            this.dO1TelephoneTxb.UseSelectable = true;
            this.dO1TelephoneTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.dO1TelephoneTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // dO1TelephoneLbl
            // 
            this.dO1TelephoneLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dO1TelephoneLbl.AutoSize = true;
            this.dO1TelephoneLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.dO1TelephoneLbl.Location = new System.Drawing.Point(4, 139);
            this.dO1TelephoneLbl.Name = "dO1TelephoneLbl";
            this.dO1TelephoneLbl.Size = new System.Drawing.Size(97, 25);
            this.dO1TelephoneLbl.TabIndex = 122;
            this.dO1TelephoneLbl.Text = "เลขที่สัญญา";
            // 
            // dO1WeightLbsUnitLbl
            // 
            this.dO1WeightLbsUnitLbl.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.dO1WeightLbsUnitLbl.AutoSize = true;
            this.dO1WeightLbsUnitLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.dO1WeightLbsUnitLbl.Location = new System.Drawing.Point(1147, 569);
            this.dO1WeightLbsUnitLbl.Name = "dO1WeightLbsUnitLbl";
            this.dO1WeightLbsUnitLbl.Size = new System.Drawing.Size(53, 25);
            this.dO1WeightLbsUnitLbl.TabIndex = 121;
            this.dO1WeightLbsUnitLbl.Text = "ปอนด์";
            // 
            // dO1WeightLbsTxb
            // 
            this.dO1WeightLbsTxb.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            // 
            // 
            // 
            this.dO1WeightLbsTxb.CustomButton.Image = null;
            this.dO1WeightLbsTxb.CustomButton.Location = new System.Drawing.Point(62, 2);
            this.dO1WeightLbsTxb.CustomButton.Margin = new System.Windows.Forms.Padding(2);
            this.dO1WeightLbsTxb.CustomButton.Name = "";
            this.dO1WeightLbsTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.dO1WeightLbsTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.dO1WeightLbsTxb.CustomButton.TabIndex = 1;
            this.dO1WeightLbsTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.dO1WeightLbsTxb.CustomButton.UseSelectable = true;
            this.dO1WeightLbsTxb.CustomButton.Visible = false;
            this.dO1WeightLbsTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.dO1WeightLbsTxb.ForeColor = System.Drawing.Color.Gray;
            this.dO1WeightLbsTxb.Lines = new string[0];
            this.dO1WeightLbsTxb.Location = new System.Drawing.Point(1055, 566);
            this.dO1WeightLbsTxb.MaxLength = 32767;
            this.dO1WeightLbsTxb.Name = "dO1WeightLbsTxb";
            this.dO1WeightLbsTxb.PasswordChar = '\0';
            this.dO1WeightLbsTxb.ReadOnly = true;
            this.dO1WeightLbsTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dO1WeightLbsTxb.SelectedText = "";
            this.dO1WeightLbsTxb.SelectionLength = 0;
            this.dO1WeightLbsTxb.SelectionStart = 0;
            this.dO1WeightLbsTxb.ShortcutsEnabled = true;
            this.dO1WeightLbsTxb.Size = new System.Drawing.Size(90, 30);
            this.dO1WeightLbsTxb.TabIndex = 120;
            this.dO1WeightLbsTxb.Theme = MetroFramework.MetroThemeStyle.Light;
            this.dO1WeightLbsTxb.UseCustomForeColor = true;
            this.dO1WeightLbsTxb.UseSelectable = true;
            this.dO1WeightLbsTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.dO1WeightLbsTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // dO1EqualLbl
            // 
            this.dO1EqualLbl.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.dO1EqualLbl.AutoSize = true;
            this.dO1EqualLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.dO1EqualLbl.Location = new System.Drawing.Point(1025, 569);
            this.dO1EqualLbl.Name = "dO1EqualLbl";
            this.dO1EqualLbl.Size = new System.Drawing.Size(24, 25);
            this.dO1EqualLbl.TabIndex = 119;
            this.dO1EqualLbl.Text = "=";
            // 
            // dO1NetWeightUnitLbl
            // 
            this.dO1NetWeightUnitLbl.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.dO1NetWeightUnitLbl.AutoSize = true;
            this.dO1NetWeightUnitLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.dO1NetWeightUnitLbl.Location = new System.Drawing.Point(991, 569);
            this.dO1NetWeightUnitLbl.Name = "dO1NetWeightUnitLbl";
            this.dO1NetWeightUnitLbl.Size = new System.Drawing.Size(36, 25);
            this.dO1NetWeightUnitLbl.TabIndex = 118;
            this.dO1NetWeightUnitLbl.Text = "กก.";
            // 
            // dO1NetWeightTxb
            // 
            this.dO1NetWeightTxb.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            // 
            // 
            // 
            this.dO1NetWeightTxb.CustomButton.Image = null;
            this.dO1NetWeightTxb.CustomButton.Location = new System.Drawing.Point(62, 2);
            this.dO1NetWeightTxb.CustomButton.Margin = new System.Windows.Forms.Padding(2);
            this.dO1NetWeightTxb.CustomButton.Name = "";
            this.dO1NetWeightTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.dO1NetWeightTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.dO1NetWeightTxb.CustomButton.TabIndex = 1;
            this.dO1NetWeightTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.dO1NetWeightTxb.CustomButton.UseSelectable = true;
            this.dO1NetWeightTxb.CustomButton.Visible = false;
            this.dO1NetWeightTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.dO1NetWeightTxb.ForeColor = System.Drawing.Color.Gray;
            this.dO1NetWeightTxb.Lines = new string[0];
            this.dO1NetWeightTxb.Location = new System.Drawing.Point(899, 566);
            this.dO1NetWeightTxb.MaxLength = 32767;
            this.dO1NetWeightTxb.Name = "dO1NetWeightTxb";
            this.dO1NetWeightTxb.PasswordChar = '\0';
            this.dO1NetWeightTxb.ReadOnly = true;
            this.dO1NetWeightTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dO1NetWeightTxb.SelectedText = "";
            this.dO1NetWeightTxb.SelectionLength = 0;
            this.dO1NetWeightTxb.SelectionStart = 0;
            this.dO1NetWeightTxb.ShortcutsEnabled = true;
            this.dO1NetWeightTxb.Size = new System.Drawing.Size(90, 30);
            this.dO1NetWeightTxb.TabIndex = 117;
            this.dO1NetWeightTxb.Theme = MetroFramework.MetroThemeStyle.Light;
            this.dO1NetWeightTxb.UseCustomForeColor = true;
            this.dO1NetWeightTxb.UseSelectable = true;
            this.dO1NetWeightTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.dO1NetWeightTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // dO1NetWeightLbl
            // 
            this.dO1NetWeightLbl.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.dO1NetWeightLbl.AutoSize = true;
            this.dO1NetWeightLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.dO1NetWeightLbl.Location = new System.Drawing.Point(825, 569);
            this.dO1NetWeightLbl.Name = "dO1NetWeightLbl";
            this.dO1NetWeightLbl.Size = new System.Drawing.Size(72, 25);
            this.dO1NetWeightLbl.TabIndex = 116;
            this.dO1NetWeightLbl.Text = "น.น.สุทธิ";
            // 
            // dO1TareLbl
            // 
            this.dO1TareLbl.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.dO1TareLbl.AutoSize = true;
            this.dO1TareLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.dO1TareLbl.Location = new System.Drawing.Point(489, 569);
            this.dO1TareLbl.Name = "dO1TareLbl";
            this.dO1TareLbl.Size = new System.Drawing.Size(186, 25);
            this.dO1TareLbl.TabIndex = 115;
            this.dO1TareLbl.Text = "หักน.น.บรรจุภัณฑ์ทั้งหมด";
            // 
            // dO1TareUnitLbl
            // 
            this.dO1TareUnitLbl.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.dO1TareUnitLbl.AutoSize = true;
            this.dO1TareUnitLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.dO1TareUnitLbl.Location = new System.Drawing.Point(769, 569);
            this.dO1TareUnitLbl.Name = "dO1TareUnitLbl";
            this.dO1TareUnitLbl.Size = new System.Drawing.Size(36, 25);
            this.dO1TareUnitLbl.TabIndex = 114;
            this.dO1TareUnitLbl.Text = "กก.";
            // 
            // dO1TareTxb
            // 
            this.dO1TareTxb.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            // 
            // 
            // 
            this.dO1TareTxb.CustomButton.Image = null;
            this.dO1TareTxb.CustomButton.Location = new System.Drawing.Point(62, 2);
            this.dO1TareTxb.CustomButton.Margin = new System.Windows.Forms.Padding(2);
            this.dO1TareTxb.CustomButton.Name = "";
            this.dO1TareTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.dO1TareTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.dO1TareTxb.CustomButton.TabIndex = 1;
            this.dO1TareTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.dO1TareTxb.CustomButton.UseSelectable = true;
            this.dO1TareTxb.CustomButton.Visible = false;
            this.dO1TareTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.dO1TareTxb.ForeColor = System.Drawing.Color.Gray;
            this.dO1TareTxb.Lines = new string[0];
            this.dO1TareTxb.Location = new System.Drawing.Point(677, 566);
            this.dO1TareTxb.MaxLength = 32767;
            this.dO1TareTxb.Name = "dO1TareTxb";
            this.dO1TareTxb.PasswordChar = '\0';
            this.dO1TareTxb.ReadOnly = true;
            this.dO1TareTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dO1TareTxb.SelectedText = "";
            this.dO1TareTxb.SelectionLength = 0;
            this.dO1TareTxb.SelectionStart = 0;
            this.dO1TareTxb.ShortcutsEnabled = true;
            this.dO1TareTxb.Size = new System.Drawing.Size(90, 30);
            this.dO1TareTxb.TabIndex = 113;
            this.dO1TareTxb.Theme = MetroFramework.MetroThemeStyle.Light;
            this.dO1TareTxb.UseCustomForeColor = true;
            this.dO1TareTxb.UseSelectable = true;
            this.dO1TareTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.dO1TareTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // dO1QtyWeightUnitLbl
            // 
            this.dO1QtyWeightUnitLbl.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.dO1QtyWeightUnitLbl.AutoSize = true;
            this.dO1QtyWeightUnitLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.dO1QtyWeightUnitLbl.Location = new System.Drawing.Point(426, 569);
            this.dO1QtyWeightUnitLbl.Name = "dO1QtyWeightUnitLbl";
            this.dO1QtyWeightUnitLbl.Size = new System.Drawing.Size(36, 25);
            this.dO1QtyWeightUnitLbl.TabIndex = 112;
            this.dO1QtyWeightUnitLbl.Text = "กก.";
            // 
            // dO1QtyWeightTxb
            // 
            this.dO1QtyWeightTxb.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            // 
            // 
            // 
            this.dO1QtyWeightTxb.CustomButton.Image = null;
            this.dO1QtyWeightTxb.CustomButton.Location = new System.Drawing.Point(62, 2);
            this.dO1QtyWeightTxb.CustomButton.Margin = new System.Windows.Forms.Padding(2);
            this.dO1QtyWeightTxb.CustomButton.Name = "";
            this.dO1QtyWeightTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.dO1QtyWeightTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.dO1QtyWeightTxb.CustomButton.TabIndex = 1;
            this.dO1QtyWeightTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.dO1QtyWeightTxb.CustomButton.UseSelectable = true;
            this.dO1QtyWeightTxb.CustomButton.Visible = false;
            this.dO1QtyWeightTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.dO1QtyWeightTxb.ForeColor = System.Drawing.Color.Gray;
            this.dO1QtyWeightTxb.Lines = new string[0];
            this.dO1QtyWeightTxb.Location = new System.Drawing.Point(334, 566);
            this.dO1QtyWeightTxb.MaxLength = 32767;
            this.dO1QtyWeightTxb.Name = "dO1QtyWeightTxb";
            this.dO1QtyWeightTxb.PasswordChar = '\0';
            this.dO1QtyWeightTxb.ReadOnly = true;
            this.dO1QtyWeightTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dO1QtyWeightTxb.SelectedText = "";
            this.dO1QtyWeightTxb.SelectionLength = 0;
            this.dO1QtyWeightTxb.SelectionStart = 0;
            this.dO1QtyWeightTxb.ShortcutsEnabled = true;
            this.dO1QtyWeightTxb.Size = new System.Drawing.Size(90, 30);
            this.dO1QtyWeightTxb.TabIndex = 111;
            this.dO1QtyWeightTxb.Theme = MetroFramework.MetroThemeStyle.Light;
            this.dO1QtyWeightTxb.UseCustomForeColor = true;
            this.dO1QtyWeightTxb.UseSelectable = true;
            this.dO1QtyWeightTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.dO1QtyWeightTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // dO1QtyWeightLbl
            // 
            this.dO1QtyWeightLbl.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.dO1QtyWeightLbl.AutoSize = true;
            this.dO1QtyWeightLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.dO1QtyWeightLbl.Location = new System.Drawing.Point(264, 569);
            this.dO1QtyWeightLbl.Name = "dO1QtyWeightLbl";
            this.dO1QtyWeightLbl.Size = new System.Drawing.Size(68, 25);
            this.dO1QtyWeightLbl.TabIndex = 110;
            this.dO1QtyWeightLbl.Text = "น.น.รวม";
            // 
            // dO1QtyCountUnitLbl
            // 
            this.dO1QtyCountUnitLbl.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.dO1QtyCountUnitLbl.AutoSize = true;
            this.dO1QtyCountUnitLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.dO1QtyCountUnitLbl.Location = new System.Drawing.Point(170, 569);
            this.dO1QtyCountUnitLbl.Name = "dO1QtyCountUnitLbl";
            this.dO1QtyCountUnitLbl.Size = new System.Drawing.Size(66, 25);
            this.dO1QtyCountUnitLbl.TabIndex = 109;
            this.dO1QtyCountUnitLbl.Text = "กระสอบ";
            // 
            // dO1QtyCountTxb
            // 
            this.dO1QtyCountTxb.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            // 
            // 
            // 
            this.dO1QtyCountTxb.CustomButton.Image = null;
            this.dO1QtyCountTxb.CustomButton.Location = new System.Drawing.Point(62, 2);
            this.dO1QtyCountTxb.CustomButton.Margin = new System.Windows.Forms.Padding(2);
            this.dO1QtyCountTxb.CustomButton.Name = "";
            this.dO1QtyCountTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.dO1QtyCountTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.dO1QtyCountTxb.CustomButton.TabIndex = 1;
            this.dO1QtyCountTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.dO1QtyCountTxb.CustomButton.UseSelectable = true;
            this.dO1QtyCountTxb.CustomButton.Visible = false;
            this.dO1QtyCountTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.dO1QtyCountTxb.ForeColor = System.Drawing.Color.Gray;
            this.dO1QtyCountTxb.Lines = new string[0];
            this.dO1QtyCountTxb.Location = new System.Drawing.Point(78, 566);
            this.dO1QtyCountTxb.MaxLength = 32767;
            this.dO1QtyCountTxb.Name = "dO1QtyCountTxb";
            this.dO1QtyCountTxb.PasswordChar = '\0';
            this.dO1QtyCountTxb.ReadOnly = true;
            this.dO1QtyCountTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dO1QtyCountTxb.SelectedText = "";
            this.dO1QtyCountTxb.SelectionLength = 0;
            this.dO1QtyCountTxb.SelectionStart = 0;
            this.dO1QtyCountTxb.ShortcutsEnabled = true;
            this.dO1QtyCountTxb.Size = new System.Drawing.Size(90, 30);
            this.dO1QtyCountTxb.TabIndex = 108;
            this.dO1QtyCountTxb.Theme = MetroFramework.MetroThemeStyle.Light;
            this.dO1QtyCountTxb.UseCustomForeColor = true;
            this.dO1QtyCountTxb.UseSelectable = true;
            this.dO1QtyCountTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.dO1QtyCountTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // dO1QtyCountLbl
            // 
            this.dO1QtyCountLbl.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.dO1QtyCountLbl.AutoSize = true;
            this.dO1QtyCountLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.dO1QtyCountLbl.Location = new System.Drawing.Point(17, 569);
            this.dO1QtyCountLbl.Name = "dO1QtyCountLbl";
            this.dO1QtyCountLbl.Size = new System.Drawing.Size(59, 25);
            this.dO1QtyCountLbl.TabIndex = 107;
            this.dO1QtyCountLbl.Text = "จำนวน";
            // 
            // dO1ItemCodeTxb
            // 
            this.dO1ItemCodeTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.dO1ItemCodeTxb.CustomButton.Image = null;
            this.dO1ItemCodeTxb.CustomButton.Location = new System.Drawing.Point(264, 2);
            this.dO1ItemCodeTxb.CustomButton.Margin = new System.Windows.Forms.Padding(2);
            this.dO1ItemCodeTxb.CustomButton.Name = "";
            this.dO1ItemCodeTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.dO1ItemCodeTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.dO1ItemCodeTxb.CustomButton.TabIndex = 1;
            this.dO1ItemCodeTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.dO1ItemCodeTxb.CustomButton.UseSelectable = true;
            this.dO1ItemCodeTxb.CustomButton.Visible = false;
            this.dO1ItemCodeTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.dO1ItemCodeTxb.ForeColor = System.Drawing.Color.Gray;
            this.dO1ItemCodeTxb.Lines = new string[0];
            this.dO1ItemCodeTxb.Location = new System.Drawing.Point(604, 105);
            this.dO1ItemCodeTxb.MaxLength = 32767;
            this.dO1ItemCodeTxb.Name = "dO1ItemCodeTxb";
            this.dO1ItemCodeTxb.PasswordChar = '\0';
            this.dO1ItemCodeTxb.ReadOnly = true;
            this.dO1ItemCodeTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dO1ItemCodeTxb.SelectedText = "";
            this.dO1ItemCodeTxb.SelectionLength = 0;
            this.dO1ItemCodeTxb.SelectionStart = 0;
            this.dO1ItemCodeTxb.ShortcutsEnabled = true;
            this.dO1ItemCodeTxb.Size = new System.Drawing.Size(292, 30);
            this.dO1ItemCodeTxb.TabIndex = 106;
            this.dO1ItemCodeTxb.UseCustomForeColor = true;
            this.dO1ItemCodeTxb.UseSelectable = true;
            this.dO1ItemCodeTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.dO1ItemCodeTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // dO1ItemCodeLbl
            // 
            this.dO1ItemCodeLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dO1ItemCodeLbl.AutoSize = true;
            this.dO1ItemCodeLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.dO1ItemCodeLbl.Location = new System.Drawing.Point(504, 105);
            this.dO1ItemCodeLbl.Name = "dO1ItemCodeLbl";
            this.dO1ItemCodeLbl.Size = new System.Drawing.Size(80, 25);
            this.dO1ItemCodeLbl.TabIndex = 103;
            this.dO1ItemCodeLbl.Text = "รหัสสินค้า";
            // 
            // dO1ItemDescLbl
            // 
            this.dO1ItemDescLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dO1ItemDescLbl.AutoSize = true;
            this.dO1ItemDescLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.dO1ItemDescLbl.Location = new System.Drawing.Point(458, 142);
            this.dO1ItemDescLbl.Name = "dO1ItemDescLbl";
            this.dO1ItemDescLbl.Size = new System.Drawing.Size(126, 25);
            this.dO1ItemDescLbl.TabIndex = 104;
            this.dO1ItemDescLbl.Text = "รายละเอียดสินค้า";
            // 
            // dO1ItemDescTxb
            // 
            this.dO1ItemDescTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.dO1ItemDescTxb.CustomButton.Image = null;
            this.dO1ItemDescTxb.CustomButton.Location = new System.Drawing.Point(264, 2);
            this.dO1ItemDescTxb.CustomButton.Margin = new System.Windows.Forms.Padding(2);
            this.dO1ItemDescTxb.CustomButton.Name = "";
            this.dO1ItemDescTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.dO1ItemDescTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.dO1ItemDescTxb.CustomButton.TabIndex = 1;
            this.dO1ItemDescTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.dO1ItemDescTxb.CustomButton.UseSelectable = true;
            this.dO1ItemDescTxb.CustomButton.Visible = false;
            this.dO1ItemDescTxb.Enabled = false;
            this.dO1ItemDescTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.dO1ItemDescTxb.ForeColor = System.Drawing.Color.Gray;
            this.dO1ItemDescTxb.Lines = new string[0];
            this.dO1ItemDescTxb.Location = new System.Drawing.Point(604, 142);
            this.dO1ItemDescTxb.MaxLength = 32767;
            this.dO1ItemDescTxb.Name = "dO1ItemDescTxb";
            this.dO1ItemDescTxb.PasswordChar = '\0';
            this.dO1ItemDescTxb.ReadOnly = true;
            this.dO1ItemDescTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dO1ItemDescTxb.SelectedText = "";
            this.dO1ItemDescTxb.SelectionLength = 0;
            this.dO1ItemDescTxb.SelectionStart = 0;
            this.dO1ItemDescTxb.ShortcutsEnabled = true;
            this.dO1ItemDescTxb.Size = new System.Drawing.Size(292, 30);
            this.dO1ItemDescTxb.TabIndex = 105;
            this.dO1ItemDescTxb.Tag = "";
            this.dO1ItemDescTxb.UseCustomForeColor = true;
            this.dO1ItemDescTxb.UseSelectable = true;
            this.dO1ItemDescTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.dO1ItemDescTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // dO1CustomerTxb
            // 
            this.dO1CustomerTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.dO1CustomerTxb.CustomButton.Image = null;
            this.dO1CustomerTxb.CustomButton.Location = new System.Drawing.Point(279, 2);
            this.dO1CustomerTxb.CustomButton.Margin = new System.Windows.Forms.Padding(2);
            this.dO1CustomerTxb.CustomButton.Name = "";
            this.dO1CustomerTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.dO1CustomerTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.dO1CustomerTxb.CustomButton.TabIndex = 1;
            this.dO1CustomerTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.dO1CustomerTxb.CustomButton.UseSelectable = true;
            this.dO1CustomerTxb.CustomButton.Visible = false;
            this.dO1CustomerTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.dO1CustomerTxb.ForeColor = System.Drawing.Color.Gray;
            this.dO1CustomerTxb.Lines = new string[0];
            this.dO1CustomerTxb.Location = new System.Drawing.Point(135, 102);
            this.dO1CustomerTxb.MaxLength = 32767;
            this.dO1CustomerTxb.Name = "dO1CustomerTxb";
            this.dO1CustomerTxb.PasswordChar = '\0';
            this.dO1CustomerTxb.ReadOnly = true;
            this.dO1CustomerTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dO1CustomerTxb.SelectedText = "";
            this.dO1CustomerTxb.SelectionLength = 0;
            this.dO1CustomerTxb.SelectionStart = 0;
            this.dO1CustomerTxb.ShortcutsEnabled = true;
            this.dO1CustomerTxb.Size = new System.Drawing.Size(307, 30);
            this.dO1CustomerTxb.TabIndex = 47;
            this.dO1CustomerTxb.UseCustomForeColor = true;
            this.dO1CustomerTxb.UseSelectable = true;
            this.dO1CustomerTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.dO1CustomerTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // dO1AssignDateTxb
            // 
            this.dO1AssignDateTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.dO1AssignDateTxb.CustomButton.Image = null;
            this.dO1AssignDateTxb.CustomButton.Location = new System.Drawing.Point(150, 2);
            this.dO1AssignDateTxb.CustomButton.Margin = new System.Windows.Forms.Padding(2);
            this.dO1AssignDateTxb.CustomButton.Name = "";
            this.dO1AssignDateTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.dO1AssignDateTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.dO1AssignDateTxb.CustomButton.TabIndex = 1;
            this.dO1AssignDateTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.dO1AssignDateTxb.CustomButton.UseSelectable = true;
            this.dO1AssignDateTxb.CustomButton.Visible = false;
            this.dO1AssignDateTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.dO1AssignDateTxb.ForeColor = System.Drawing.Color.Gray;
            this.dO1AssignDateTxb.Lines = new string[0];
            this.dO1AssignDateTxb.Location = new System.Drawing.Point(1057, 47);
            this.dO1AssignDateTxb.MaxLength = 32767;
            this.dO1AssignDateTxb.Name = "dO1AssignDateTxb";
            this.dO1AssignDateTxb.PasswordChar = '\0';
            this.dO1AssignDateTxb.ReadOnly = true;
            this.dO1AssignDateTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dO1AssignDateTxb.SelectedText = "";
            this.dO1AssignDateTxb.SelectionLength = 0;
            this.dO1AssignDateTxb.SelectionStart = 0;
            this.dO1AssignDateTxb.ShortcutsEnabled = true;
            this.dO1AssignDateTxb.Size = new System.Drawing.Size(178, 30);
            this.dO1AssignDateTxb.TabIndex = 46;
            this.dO1AssignDateTxb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.dO1AssignDateTxb.UseCustomForeColor = true;
            this.dO1AssignDateTxb.UseSelectable = true;
            this.dO1AssignDateTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.dO1AssignDateTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // dO1CancelBtn
            // 
            this.dO1CancelBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.dO1CancelBtn.BackColor = System.Drawing.Color.IndianRed;
            this.dO1CancelBtn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.dO1CancelBtn.ForeColor = System.Drawing.Color.MistyRose;
            this.dO1CancelBtn.Location = new System.Drawing.Point(1090, 615);
            this.dO1CancelBtn.Name = "dO1CancelBtn";
            this.dO1CancelBtn.Size = new System.Drawing.Size(150, 40);
            this.dO1CancelBtn.TabIndex = 40;
            this.dO1CancelBtn.Text = "ออกจากหน้านี้";
            this.dO1CancelBtn.UseCustomBackColor = true;
            this.dO1CancelBtn.UseCustomForeColor = true;
            this.dO1CancelBtn.UseSelectable = true;
            this.dO1CancelBtn.Click += new System.EventHandler(this.DO1CancelBtn_Click);
            // 
            // dO1PrintFormBtn
            // 
            this.dO1PrintFormBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.dO1PrintFormBtn.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.dO1PrintFormBtn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.dO1PrintFormBtn.ForeColor = System.Drawing.Color.FloralWhite;
            this.dO1PrintFormBtn.Location = new System.Drawing.Point(902, 615);
            this.dO1PrintFormBtn.Name = "dO1PrintFormBtn";
            this.dO1PrintFormBtn.Size = new System.Drawing.Size(150, 40);
            this.dO1PrintFormBtn.TabIndex = 37;
            this.dO1PrintFormBtn.Text = "พิมพ์แบบฟอร์ม";
            this.dO1PrintFormBtn.UseCustomBackColor = true;
            this.dO1PrintFormBtn.UseCustomForeColor = true;
            this.dO1PrintFormBtn.UseSelectable = true;
            this.dO1PrintFormBtn.Click += new System.EventHandler(this.DO1PrintFormBtn_Click);
            // 
            // dO1SubmitBtn
            // 
            this.dO1SubmitBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.dO1SubmitBtn.BackColor = System.Drawing.Color.SteelBlue;
            this.dO1SubmitBtn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.dO1SubmitBtn.ForeColor = System.Drawing.Color.LightCyan;
            this.dO1SubmitBtn.Location = new System.Drawing.Point(714, 615);
            this.dO1SubmitBtn.Name = "dO1SubmitBtn";
            this.dO1SubmitBtn.Size = new System.Drawing.Size(150, 40);
            this.dO1SubmitBtn.TabIndex = 36;
            this.dO1SubmitBtn.Text = "บันทึกข้อมูล";
            this.dO1SubmitBtn.UseCustomBackColor = true;
            this.dO1SubmitBtn.UseCustomForeColor = true;
            this.dO1SubmitBtn.UseSelectable = true;
            this.dO1SubmitBtn.Click += new System.EventHandler(this.DO1SubmitBtn_Click);
            // 
            // dO1DocNoLbl
            // 
            this.dO1DocNoLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dO1DocNoLbl.AutoSize = true;
            this.dO1DocNoLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.dO1DocNoLbl.Location = new System.Drawing.Point(905, 15);
            this.dO1DocNoLbl.Name = "dO1DocNoLbl";
            this.dO1DocNoLbl.Size = new System.Drawing.Size(146, 25);
            this.dO1DocNoLbl.TabIndex = 0;
            this.dO1DocNoLbl.Text = "หมายเลขใบส่งสินค้า";
            // 
            // dO1AssignDateLbl
            // 
            this.dO1AssignDateLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dO1AssignDateLbl.AutoSize = true;
            this.dO1AssignDateLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.dO1AssignDateLbl.Location = new System.Drawing.Point(905, 49);
            this.dO1AssignDateLbl.Name = "dO1AssignDateLbl";
            this.dO1AssignDateLbl.Size = new System.Drawing.Size(42, 25);
            this.dO1AssignDateLbl.TabIndex = 1;
            this.dO1AssignDateLbl.Text = "วันที่";
            // 
            // dO1RefNoLbl
            // 
            this.dO1RefNoLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dO1RefNoLbl.AutoSize = true;
            this.dO1RefNoLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.dO1RefNoLbl.Location = new System.Drawing.Point(905, 83);
            this.dO1RefNoLbl.Name = "dO1RefNoLbl";
            this.dO1RefNoLbl.Size = new System.Drawing.Size(118, 25);
            this.dO1RefNoLbl.TabIndex = 2;
            this.dO1RefNoLbl.Text = "หมายเลขอ้างอิง";
            // 
            // dO1RefNoTxb
            // 
            this.dO1RefNoTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.dO1RefNoTxb.CustomButton.Image = null;
            this.dO1RefNoTxb.CustomButton.Location = new System.Drawing.Point(150, 2);
            this.dO1RefNoTxb.CustomButton.Margin = new System.Windows.Forms.Padding(2);
            this.dO1RefNoTxb.CustomButton.Name = "";
            this.dO1RefNoTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.dO1RefNoTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.dO1RefNoTxb.CustomButton.TabIndex = 1;
            this.dO1RefNoTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.dO1RefNoTxb.CustomButton.UseSelectable = true;
            this.dO1RefNoTxb.CustomButton.Visible = false;
            this.dO1RefNoTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.dO1RefNoTxb.Lines = new string[0];
            this.dO1RefNoTxb.Location = new System.Drawing.Point(1057, 81);
            this.dO1RefNoTxb.MaxLength = 32767;
            this.dO1RefNoTxb.Name = "dO1RefNoTxb";
            this.dO1RefNoTxb.PasswordChar = '\0';
            this.dO1RefNoTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dO1RefNoTxb.SelectedText = "";
            this.dO1RefNoTxb.SelectionLength = 0;
            this.dO1RefNoTxb.SelectionStart = 0;
            this.dO1RefNoTxb.ShortcutsEnabled = true;
            this.dO1RefNoTxb.Size = new System.Drawing.Size(178, 30);
            this.dO1RefNoTxb.TabIndex = 4;
            this.dO1RefNoTxb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.dO1RefNoTxb.UseSelectable = true;
            this.dO1RefNoTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.dO1RefNoTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // dO1DocNoTxb
            // 
            this.dO1DocNoTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.dO1DocNoTxb.CustomButton.Image = null;
            this.dO1DocNoTxb.CustomButton.Location = new System.Drawing.Point(150, 2);
            this.dO1DocNoTxb.CustomButton.Margin = new System.Windows.Forms.Padding(2);
            this.dO1DocNoTxb.CustomButton.Name = "";
            this.dO1DocNoTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.dO1DocNoTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.dO1DocNoTxb.CustomButton.TabIndex = 1;
            this.dO1DocNoTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.dO1DocNoTxb.CustomButton.UseSelectable = true;
            this.dO1DocNoTxb.CustomButton.Visible = false;
            this.dO1DocNoTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.dO1DocNoTxb.ForeColor = System.Drawing.Color.Gray;
            this.dO1DocNoTxb.Lines = new string[0];
            this.dO1DocNoTxb.Location = new System.Drawing.Point(1057, 13);
            this.dO1DocNoTxb.MaxLength = 32767;
            this.dO1DocNoTxb.Name = "dO1DocNoTxb";
            this.dO1DocNoTxb.PasswordChar = '\0';
            this.dO1DocNoTxb.ReadOnly = true;
            this.dO1DocNoTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dO1DocNoTxb.SelectedText = "";
            this.dO1DocNoTxb.SelectionLength = 0;
            this.dO1DocNoTxb.SelectionStart = 0;
            this.dO1DocNoTxb.ShortcutsEnabled = true;
            this.dO1DocNoTxb.Size = new System.Drawing.Size(178, 30);
            this.dO1DocNoTxb.TabIndex = 5;
            this.dO1DocNoTxb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.dO1DocNoTxb.UseCustomForeColor = true;
            this.dO1DocNoTxb.UseSelectable = true;
            this.dO1DocNoTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.dO1DocNoTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // dO1CustomerLbl
            // 
            this.dO1CustomerLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dO1CustomerLbl.AutoSize = true;
            this.dO1CustomerLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.dO1CustomerLbl.Location = new System.Drawing.Point(51, 102);
            this.dO1CustomerLbl.Name = "dO1CustomerLbl";
            this.dO1CustomerLbl.Size = new System.Drawing.Size(50, 25);
            this.dO1CustomerLbl.TabIndex = 6;
            this.dO1CustomerLbl.Text = "ลูกค้า";
            // 
            // dO1AddressLbl
            // 
            this.dO1AddressLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dO1AddressLbl.AutoSize = true;
            this.dO1AddressLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.dO1AddressLbl.Location = new System.Drawing.Point(59, 175);
            this.dO1AddressLbl.Name = "dO1AddressLbl";
            this.dO1AddressLbl.Size = new System.Drawing.Size(42, 25);
            this.dO1AddressLbl.TabIndex = 8;
            this.dO1AddressLbl.Text = "ที่อยู่";
            // 
            // dO1AddressTxb
            // 
            this.dO1AddressTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.dO1AddressTxb.CustomButton.Image = null;
            this.dO1AddressTxb.CustomButton.Location = new System.Drawing.Point(241, 1);
            this.dO1AddressTxb.CustomButton.Margin = new System.Windows.Forms.Padding(2);
            this.dO1AddressTxb.CustomButton.Name = "";
            this.dO1AddressTxb.CustomButton.Size = new System.Drawing.Size(65, 65);
            this.dO1AddressTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.dO1AddressTxb.CustomButton.TabIndex = 1;
            this.dO1AddressTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.dO1AddressTxb.CustomButton.UseSelectable = true;
            this.dO1AddressTxb.CustomButton.Visible = false;
            this.dO1AddressTxb.Enabled = false;
            this.dO1AddressTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.dO1AddressTxb.ForeColor = System.Drawing.Color.Gray;
            this.dO1AddressTxb.Lines = new string[0];
            this.dO1AddressTxb.Location = new System.Drawing.Point(135, 175);
            this.dO1AddressTxb.MaxLength = 32767;
            this.dO1AddressTxb.Multiline = true;
            this.dO1AddressTxb.Name = "dO1AddressTxb";
            this.dO1AddressTxb.PasswordChar = '\0';
            this.dO1AddressTxb.ReadOnly = true;
            this.dO1AddressTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dO1AddressTxb.SelectedText = "";
            this.dO1AddressTxb.SelectionLength = 0;
            this.dO1AddressTxb.SelectionStart = 0;
            this.dO1AddressTxb.ShortcutsEnabled = true;
            this.dO1AddressTxb.Size = new System.Drawing.Size(307, 67);
            this.dO1AddressTxb.TabIndex = 10;
            this.dO1AddressTxb.Tag = "";
            this.dO1AddressTxb.UseCustomForeColor = true;
            this.dO1AddressTxb.UseSelectable = true;
            this.dO1AddressTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.dO1AddressTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // dO1RemarkTxb
            // 
            this.dO1RemarkTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.dO1RemarkTxb.CustomButton.Image = null;
            this.dO1RemarkTxb.CustomButton.Location = new System.Drawing.Point(264, 2);
            this.dO1RemarkTxb.CustomButton.Margin = new System.Windows.Forms.Padding(2);
            this.dO1RemarkTxb.CustomButton.Name = "";
            this.dO1RemarkTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.dO1RemarkTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.dO1RemarkTxb.CustomButton.TabIndex = 1;
            this.dO1RemarkTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.dO1RemarkTxb.CustomButton.UseSelectable = true;
            this.dO1RemarkTxb.CustomButton.Visible = false;
            this.dO1RemarkTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.dO1RemarkTxb.ForeColor = System.Drawing.Color.Gray;
            this.dO1RemarkTxb.Lines = new string[0];
            this.dO1RemarkTxb.Location = new System.Drawing.Point(604, 179);
            this.dO1RemarkTxb.MaxLength = 32767;
            this.dO1RemarkTxb.Name = "dO1RemarkTxb";
            this.dO1RemarkTxb.PasswordChar = '\0';
            this.dO1RemarkTxb.ReadOnly = true;
            this.dO1RemarkTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dO1RemarkTxb.SelectedText = "";
            this.dO1RemarkTxb.SelectionLength = 0;
            this.dO1RemarkTxb.SelectionStart = 0;
            this.dO1RemarkTxb.ShortcutsEnabled = true;
            this.dO1RemarkTxb.Size = new System.Drawing.Size(292, 30);
            this.dO1RemarkTxb.TabIndex = 16;
            this.dO1RemarkTxb.Theme = MetroFramework.MetroThemeStyle.Light;
            this.dO1RemarkTxb.UseCustomForeColor = true;
            this.dO1RemarkTxb.UseSelectable = true;
            this.dO1RemarkTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.dO1RemarkTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // dO1RemarkLbl
            // 
            this.dO1RemarkLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dO1RemarkLbl.AutoSize = true;
            this.dO1RemarkLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.dO1RemarkLbl.Location = new System.Drawing.Point(508, 179);
            this.dO1RemarkLbl.Name = "dO1RemarkLbl";
            this.dO1RemarkLbl.Size = new System.Drawing.Size(76, 25);
            this.dO1RemarkLbl.TabIndex = 14;
            this.dO1RemarkLbl.Text = "หมายเหตุ";
            // 
            // dL1Pnl
            // 
            this.dL1Pnl.Controls.Add(this.dL1BarcodePnl);
            this.dL1Pnl.Controls.Add(this.metroPanel2);
            this.dL1Pnl.Controls.Add(this.dL1ScanBarcodeBtn);
            this.dL1Pnl.Controls.Add(this.dL1Or2Lbl);
            this.dL1Pnl.Controls.Add(this.dL1TopLbl);
            this.dL1Pnl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dL1Pnl.HorizontalScrollbarBarColor = true;
            this.dL1Pnl.HorizontalScrollbarHighlightOnWheel = false;
            this.dL1Pnl.HorizontalScrollbarSize = 0;
            this.dL1Pnl.Location = new System.Drawing.Point(0, 0);
            this.dL1Pnl.Name = "dL1Pnl";
            this.dL1Pnl.Size = new System.Drawing.Size(1238, 663);
            this.dL1Pnl.TabIndex = 38;
            this.dL1Pnl.VerticalScrollbarBarColor = true;
            this.dL1Pnl.VerticalScrollbarHighlightOnWheel = false;
            this.dL1Pnl.VerticalScrollbarSize = 0;
            this.dL1Pnl.MouseHover += new System.EventHandler(this.DL1Pnl_MouseHover);
            // 
            // dL1BarcodePnl
            // 
            this.dL1BarcodePnl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dL1BarcodePnl.BackColor = System.Drawing.Color.Transparent;
            this.dL1BarcodePnl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dL1BarcodePnl.Controls.Add(this.metroLabel3);
            this.dL1BarcodePnl.HorizontalScrollbarBarColor = true;
            this.dL1BarcodePnl.HorizontalScrollbarHighlightOnWheel = false;
            this.dL1BarcodePnl.HorizontalScrollbarSize = 10;
            this.dL1BarcodePnl.Location = new System.Drawing.Point(494, 302);
            this.dL1BarcodePnl.Name = "dL1BarcodePnl";
            this.dL1BarcodePnl.Size = new System.Drawing.Size(250, 85);
            this.dL1BarcodePnl.TabIndex = 45;
            this.dL1BarcodePnl.UseCustomBackColor = true;
            this.dL1BarcodePnl.VerticalScrollbarBarColor = true;
            this.dL1BarcodePnl.VerticalScrollbarHighlightOnWheel = false;
            this.dL1BarcodePnl.VerticalScrollbarSize = 10;
            this.dL1BarcodePnl.MouseLeave += new System.EventHandler(this.dL1BarcodePnl_MouseLeave);
            this.dL1BarcodePnl.MouseHover += new System.EventHandler(this.dL1BarcodePnl_MouseHover);
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(80, 5);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(90, 19);
            this.metroLabel3.TabIndex = 2;
            this.metroLabel3.Text = "Barcode Area";
            this.metroLabel3.UseCustomBackColor = true;
            // 
            // metroPanel2
            // 
            this.metroPanel2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.metroPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.metroPanel2.Controls.Add(this.dL1CreateDelBtn);
            this.metroPanel2.Controls.Add(this.dL1DelNoTxb);
            this.metroPanel2.Controls.Add(this.dL1DelNoLbl);
            this.metroPanel2.Controls.Add(this.dL1Or1Lbl);
            this.metroPanel2.Controls.Add(this.dL1SubmitBtn);
            this.metroPanel2.HorizontalScrollbarBarColor = true;
            this.metroPanel2.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel2.HorizontalScrollbarSize = 10;
            this.metroPanel2.Location = new System.Drawing.Point(334, 114);
            this.metroPanel2.Name = "metroPanel2";
            this.metroPanel2.Size = new System.Drawing.Size(570, 166);
            this.metroPanel2.TabIndex = 43;
            this.metroPanel2.VerticalScrollbarBarColor = true;
            this.metroPanel2.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel2.VerticalScrollbarSize = 10;
            // 
            // dL1CreateDelBtn
            // 
            this.dL1CreateDelBtn.BackColor = System.Drawing.Color.SeaGreen;
            this.dL1CreateDelBtn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.dL1CreateDelBtn.ForeColor = System.Drawing.Color.Honeydew;
            this.dL1CreateDelBtn.Location = new System.Drawing.Point(179, 21);
            this.dL1CreateDelBtn.Name = "dL1CreateDelBtn";
            this.dL1CreateDelBtn.Size = new System.Drawing.Size(214, 36);
            this.dL1CreateDelBtn.TabIndex = 42;
            this.dL1CreateDelBtn.Text = "สร้างใบจัดสินค้า";
            this.dL1CreateDelBtn.UseCustomBackColor = true;
            this.dL1CreateDelBtn.UseCustomForeColor = true;
            this.dL1CreateDelBtn.UseSelectable = true;
            this.dL1CreateDelBtn.Click += new System.EventHandler(this.DL1CreateDelBtn_Click);
            // 
            // dL1DelNoTxb
            // 
            // 
            // 
            // 
            this.dL1DelNoTxb.CustomButton.Image = null;
            this.dL1DelNoTxb.CustomButton.Location = new System.Drawing.Point(186, 2);
            this.dL1DelNoTxb.CustomButton.Name = "";
            this.dL1DelNoTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.dL1DelNoTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.dL1DelNoTxb.CustomButton.TabIndex = 1;
            this.dL1DelNoTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.dL1DelNoTxb.CustomButton.UseSelectable = true;
            this.dL1DelNoTxb.CustomButton.Visible = false;
            this.dL1DelNoTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.dL1DelNoTxb.Lines = new string[0];
            this.dL1DelNoTxb.Location = new System.Drawing.Point(179, 112);
            this.dL1DelNoTxb.MaxLength = 32767;
            this.dL1DelNoTxb.Name = "dL1DelNoTxb";
            this.dL1DelNoTxb.PasswordChar = '\0';
            this.dL1DelNoTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dL1DelNoTxb.SelectedText = "";
            this.dL1DelNoTxb.SelectionLength = 0;
            this.dL1DelNoTxb.SelectionStart = 0;
            this.dL1DelNoTxb.ShortcutsEnabled = true;
            this.dL1DelNoTxb.Size = new System.Drawing.Size(214, 30);
            this.dL1DelNoTxb.TabIndex = 7;
            this.dL1DelNoTxb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.dL1DelNoTxb.UseSelectable = true;
            this.dL1DelNoTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.dL1DelNoTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.dL1DelNoTxb.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dL1DelNoTxb_KeyDown);
            this.dL1DelNoTxb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dL1DelNoTxb_KeyPress);
            // 
            // dL1DelNoLbl
            // 
            this.dL1DelNoLbl.AutoSize = true;
            this.dL1DelNoLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.dL1DelNoLbl.Location = new System.Drawing.Point(18, 112);
            this.dL1DelNoLbl.Name = "dL1DelNoLbl";
            this.dL1DelNoLbl.Size = new System.Drawing.Size(148, 25);
            this.dL1DelNoLbl.TabIndex = 6;
            this.dL1DelNoLbl.Text = "หมายเลขใบจัดสินค้า";
            // 
            // dL1Or1Lbl
            // 
            this.dL1Or1Lbl.AutoSize = true;
            this.dL1Or1Lbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.dL1Or1Lbl.Location = new System.Drawing.Point(266, 73);
            this.dL1Or1Lbl.Name = "dL1Or1Lbl";
            this.dL1Or1Lbl.Size = new System.Drawing.Size(41, 25);
            this.dL1Or1Lbl.TabIndex = 8;
            this.dL1Or1Lbl.Text = "หรือ";
            // 
            // dL1SubmitBtn
            // 
            this.dL1SubmitBtn.BackColor = System.Drawing.Color.SteelBlue;
            this.dL1SubmitBtn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.dL1SubmitBtn.ForeColor = System.Drawing.Color.LightCyan;
            this.dL1SubmitBtn.Location = new System.Drawing.Point(436, 107);
            this.dL1SubmitBtn.Name = "dL1SubmitBtn";
            this.dL1SubmitBtn.Size = new System.Drawing.Size(96, 40);
            this.dL1SubmitBtn.TabIndex = 35;
            this.dL1SubmitBtn.Text = "ยืนยัน";
            this.dL1SubmitBtn.UseCustomBackColor = true;
            this.dL1SubmitBtn.UseCustomForeColor = true;
            this.dL1SubmitBtn.UseSelectable = true;
            this.dL1SubmitBtn.Click += new System.EventHandler(this.DL1SubmitBtn_Click);
            // 
            // dL1ScanBarcodeBtn
            // 
            this.dL1ScanBarcodeBtn.Location = new System.Drawing.Point(460, 655);
            this.dL1ScanBarcodeBtn.Name = "dL1ScanBarcodeBtn";
            this.dL1ScanBarcodeBtn.Size = new System.Drawing.Size(214, 30);
            this.dL1ScanBarcodeBtn.TabIndex = 41;
            this.dL1ScanBarcodeBtn.Text = "ยิงบาร์โค้ดหมายเลขใบจัดสินค้า";
            this.dL1ScanBarcodeBtn.UseSelectable = true;
            this.dL1ScanBarcodeBtn.Visible = false;
            // 
            // dL1Or2Lbl
            // 
            this.dL1Or2Lbl.AutoSize = true;
            this.dL1Or2Lbl.Location = new System.Drawing.Point(565, 249);
            this.dL1Or2Lbl.Name = "dL1Or2Lbl";
            this.dL1Or2Lbl.Size = new System.Drawing.Size(30, 19);
            this.dL1Or2Lbl.TabIndex = 39;
            this.dL1Or2Lbl.Text = "หรือ";
            this.dL1Or2Lbl.Visible = false;
            // 
            // dL1TopLbl
            // 
            this.dL1TopLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dL1TopLbl.AutoSize = true;
            this.dL1TopLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.dL1TopLbl.Location = new System.Drawing.Point(282, 74);
            this.dL1TopLbl.Name = "dL1TopLbl";
            this.dL1TopLbl.Size = new System.Drawing.Size(674, 25);
            this.dL1TopLbl.TabIndex = 36;
            this.dL1TopLbl.Text = "กรุณากดปุ่มสร้างใบจัดสินค้า หรือระบุ/ยิงหมายเลขใบจัดสินค้า  เพื่อเข้าสู่หน้าใบจัด" +
    "สินค้าเพื่อส่งลูกค้า";
            // 
            // dL3Pnl
            // 
            this.dL3Pnl.Controls.Add(this.label1);
            this.dL3Pnl.Controls.Add(this.dL3DeliveryDgv);
            this.dL3Pnl.Controls.Add(this.dL3AssignDateTxb);
            this.dL3Pnl.Controls.Add(this.dL3CancelBtn);
            this.dL3Pnl.Controls.Add(this.dL3SubmitBtn);
            this.dL3Pnl.Controls.Add(this.dL3DocNoLbl);
            this.dL3Pnl.Controls.Add(this.dL3AssignDateLbl);
            this.dL3Pnl.Controls.Add(this.dL3RefNoLbl);
            this.dL3Pnl.Controls.Add(this.dL3RefNoTxb);
            this.dL3Pnl.Controls.Add(this.dL3DocNoTxb);
            this.dL3Pnl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dL3Pnl.HorizontalScrollbarBarColor = true;
            this.dL3Pnl.HorizontalScrollbarHighlightOnWheel = false;
            this.dL3Pnl.HorizontalScrollbarSize = 10;
            this.dL3Pnl.Location = new System.Drawing.Point(0, 0);
            this.dL3Pnl.Name = "dL3Pnl";
            this.dL3Pnl.Size = new System.Drawing.Size(1238, 663);
            this.dL3Pnl.TabIndex = 39;
            this.dL3Pnl.VerticalScrollbarBarColor = true;
            this.dL3Pnl.VerticalScrollbarHighlightOnWheel = false;
            this.dL3Pnl.VerticalScrollbarSize = 10;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("Microsoft Tai Le", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(497, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(244, 34);
            this.label1.TabIndex = 106;
            this.label1.Text = "ใบจัดสินค้าเพื่อส่งลูกค้า";
            // 
            // dL3DeliveryDgv
            // 
            this.dL3DeliveryDgv.AllowUserToAddRows = false;
            this.dL3DeliveryDgv.AllowUserToDeleteRows = false;
            this.dL3DeliveryDgv.AllowUserToResizeRows = false;
            this.dL3DeliveryDgv.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dL3DeliveryDgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dL3DeliveryDgv.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dL3DeliveryDgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dL3DeliveryDgv.Location = new System.Drawing.Point(5, 178);
            this.dL3DeliveryDgv.Name = "dL3DeliveryDgv";
            this.dL3DeliveryDgv.ReadOnly = true;
            this.dL3DeliveryDgv.RowHeadersVisible = false;
            this.dL3DeliveryDgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dL3DeliveryDgv.Size = new System.Drawing.Size(1227, 426);
            this.dL3DeliveryDgv.TabIndex = 47;
            this.dL3DeliveryDgv.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DL3DeliveryDgv_CellClick);
            this.dL3DeliveryDgv.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dL3DeliveryDgv_DataBindingComplete);
            this.dL3DeliveryDgv.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.DL3DeliveryDgv_RowPrePaint);
            // 
            // dL3AssignDateTxb
            // 
            this.dL3AssignDateTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.dL3AssignDateTxb.CustomButton.Image = null;
            this.dL3AssignDateTxb.CustomButton.Location = new System.Drawing.Point(161, 2);
            this.dL3AssignDateTxb.CustomButton.Name = "";
            this.dL3AssignDateTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.dL3AssignDateTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.dL3AssignDateTxb.CustomButton.TabIndex = 1;
            this.dL3AssignDateTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.dL3AssignDateTxb.CustomButton.UseSelectable = true;
            this.dL3AssignDateTxb.CustomButton.Visible = false;
            this.dL3AssignDateTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.dL3AssignDateTxb.ForeColor = System.Drawing.Color.Gray;
            this.dL3AssignDateTxb.Lines = new string[0];
            this.dL3AssignDateTxb.Location = new System.Drawing.Point(1009, 93);
            this.dL3AssignDateTxb.MaxLength = 32767;
            this.dL3AssignDateTxb.Name = "dL3AssignDateTxb";
            this.dL3AssignDateTxb.PasswordChar = '\0';
            this.dL3AssignDateTxb.ReadOnly = true;
            this.dL3AssignDateTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dL3AssignDateTxb.SelectedText = "";
            this.dL3AssignDateTxb.SelectionLength = 0;
            this.dL3AssignDateTxb.SelectionStart = 0;
            this.dL3AssignDateTxb.ShortcutsEnabled = true;
            this.dL3AssignDateTxb.Size = new System.Drawing.Size(189, 30);
            this.dL3AssignDateTxb.TabIndex = 46;
            this.dL3AssignDateTxb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.dL3AssignDateTxb.UseCustomForeColor = true;
            this.dL3AssignDateTxb.UseSelectable = true;
            this.dL3AssignDateTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.dL3AssignDateTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // dL3CancelBtn
            // 
            this.dL3CancelBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.dL3CancelBtn.BackColor = System.Drawing.Color.IndianRed;
            this.dL3CancelBtn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.dL3CancelBtn.ForeColor = System.Drawing.Color.MistyRose;
            this.dL3CancelBtn.Location = new System.Drawing.Point(1092, 615);
            this.dL3CancelBtn.Name = "dL3CancelBtn";
            this.dL3CancelBtn.Size = new System.Drawing.Size(150, 40);
            this.dL3CancelBtn.TabIndex = 40;
            this.dL3CancelBtn.Text = "ออกจากหน้านี้";
            this.dL3CancelBtn.UseCustomBackColor = true;
            this.dL3CancelBtn.UseCustomForeColor = true;
            this.dL3CancelBtn.UseSelectable = true;
            this.dL3CancelBtn.Click += new System.EventHandler(this.MetroButton39_Click);
            // 
            // dL3SubmitBtn
            // 
            this.dL3SubmitBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.dL3SubmitBtn.BackColor = System.Drawing.Color.SteelBlue;
            this.dL3SubmitBtn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.dL3SubmitBtn.ForeColor = System.Drawing.Color.LightCyan;
            this.dL3SubmitBtn.Location = new System.Drawing.Point(899, 615);
            this.dL3SubmitBtn.Name = "dL3SubmitBtn";
            this.dL3SubmitBtn.Size = new System.Drawing.Size(150, 40);
            this.dL3SubmitBtn.TabIndex = 36;
            this.dL3SubmitBtn.Text = "บันทึกข้อมูล";
            this.dL3SubmitBtn.UseCustomBackColor = true;
            this.dL3SubmitBtn.UseCustomForeColor = true;
            this.dL3SubmitBtn.UseSelectable = true;
            this.dL3SubmitBtn.Visible = false;
            this.dL3SubmitBtn.Click += new System.EventHandler(this.DL3SubmitBtn_Click);
            // 
            // dL3DocNoLbl
            // 
            this.dL3DocNoLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dL3DocNoLbl.AutoSize = true;
            this.dL3DocNoLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.dL3DocNoLbl.Location = new System.Drawing.Point(827, 63);
            this.dL3DocNoLbl.Name = "dL3DocNoLbl";
            this.dL3DocNoLbl.Size = new System.Drawing.Size(148, 25);
            this.dL3DocNoLbl.TabIndex = 0;
            this.dL3DocNoLbl.Text = "หมายเลขใบจัดสินค้า";
            // 
            // dL3AssignDateLbl
            // 
            this.dL3AssignDateLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dL3AssignDateLbl.AutoSize = true;
            this.dL3AssignDateLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.dL3AssignDateLbl.Location = new System.Drawing.Point(925, 93);
            this.dL3AssignDateLbl.Name = "dL3AssignDateLbl";
            this.dL3AssignDateLbl.Size = new System.Drawing.Size(42, 25);
            this.dL3AssignDateLbl.TabIndex = 1;
            this.dL3AssignDateLbl.Text = "วันที่";
            // 
            // dL3RefNoLbl
            // 
            this.dL3RefNoLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dL3RefNoLbl.AutoSize = true;
            this.dL3RefNoLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.dL3RefNoLbl.Location = new System.Drawing.Point(862, 127);
            this.dL3RefNoLbl.Name = "dL3RefNoLbl";
            this.dL3RefNoLbl.Size = new System.Drawing.Size(118, 25);
            this.dL3RefNoLbl.TabIndex = 2;
            this.dL3RefNoLbl.Text = "หมายเลขอ้างอิง";
            // 
            // dL3RefNoTxb
            // 
            this.dL3RefNoTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.dL3RefNoTxb.CustomButton.Image = null;
            this.dL3RefNoTxb.CustomButton.Location = new System.Drawing.Point(161, 2);
            this.dL3RefNoTxb.CustomButton.Name = "";
            this.dL3RefNoTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.dL3RefNoTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.dL3RefNoTxb.CustomButton.TabIndex = 1;
            this.dL3RefNoTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.dL3RefNoTxb.CustomButton.UseSelectable = true;
            this.dL3RefNoTxb.CustomButton.Visible = false;
            this.dL3RefNoTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.dL3RefNoTxb.Lines = new string[0];
            this.dL3RefNoTxb.Location = new System.Drawing.Point(1009, 127);
            this.dL3RefNoTxb.MaxLength = 32767;
            this.dL3RefNoTxb.Name = "dL3RefNoTxb";
            this.dL3RefNoTxb.PasswordChar = '\0';
            this.dL3RefNoTxb.ReadOnly = true;
            this.dL3RefNoTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dL3RefNoTxb.SelectedText = "";
            this.dL3RefNoTxb.SelectionLength = 0;
            this.dL3RefNoTxb.SelectionStart = 0;
            this.dL3RefNoTxb.ShortcutsEnabled = true;
            this.dL3RefNoTxb.Size = new System.Drawing.Size(189, 30);
            this.dL3RefNoTxb.TabIndex = 4;
            this.dL3RefNoTxb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.dL3RefNoTxb.UseSelectable = true;
            this.dL3RefNoTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.dL3RefNoTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // dL3DocNoTxb
            // 
            this.dL3DocNoTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.dL3DocNoTxb.CustomButton.Image = null;
            this.dL3DocNoTxb.CustomButton.Location = new System.Drawing.Point(161, 2);
            this.dL3DocNoTxb.CustomButton.Name = "";
            this.dL3DocNoTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.dL3DocNoTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.dL3DocNoTxb.CustomButton.TabIndex = 1;
            this.dL3DocNoTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.dL3DocNoTxb.CustomButton.UseSelectable = true;
            this.dL3DocNoTxb.CustomButton.Visible = false;
            this.dL3DocNoTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.dL3DocNoTxb.ForeColor = System.Drawing.Color.Gray;
            this.dL3DocNoTxb.Lines = new string[0];
            this.dL3DocNoTxb.Location = new System.Drawing.Point(1009, 58);
            this.dL3DocNoTxb.MaxLength = 32767;
            this.dL3DocNoTxb.Name = "dL3DocNoTxb";
            this.dL3DocNoTxb.PasswordChar = '\0';
            this.dL3DocNoTxb.ReadOnly = true;
            this.dL3DocNoTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dL3DocNoTxb.SelectedText = "";
            this.dL3DocNoTxb.SelectionLength = 0;
            this.dL3DocNoTxb.SelectionStart = 0;
            this.dL3DocNoTxb.ShortcutsEnabled = true;
            this.dL3DocNoTxb.Size = new System.Drawing.Size(189, 30);
            this.dL3DocNoTxb.TabIndex = 5;
            this.dL3DocNoTxb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.dL3DocNoTxb.UseCustomForeColor = true;
            this.dL3DocNoTxb.UseSelectable = true;
            this.dL3DocNoTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.dL3DocNoTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // dL2Pnl
            // 
            this.dL2Pnl.Controls.Add(this.dL2TopLbl);
            this.dL2Pnl.Controls.Add(this.dL2ShipDateDpk);
            this.dL2Pnl.Controls.Add(this.metroLabel6);
            this.dL2Pnl.Controls.Add(this.dL2DueDateTxb);
            this.dL2Pnl.Controls.Add(this.dL2DeliveryDgv);
            this.dL2Pnl.Controls.Add(this.dL2SubmitBtn);
            this.dL2Pnl.Controls.Add(this.dL2ShipDateLbl);
            this.dL2Pnl.Controls.Add(this.dL3PrintFormBtn);
            this.dL2Pnl.Controls.Add(this.metroLabel188);
            this.dL2Pnl.Controls.Add(this.dL2ItemDeliveryMeasureUnitLbl);
            this.dL2Pnl.Controls.Add(this.dL2ItemDeliveryCountUnitLbl);
            this.dL2Pnl.Controls.Add(this.dL2ItemStockMeasureUnitLbsLbl);
            this.dL2Pnl.Controls.Add(this.dL2ItemStockMeasureUnitLbl);
            this.dL2Pnl.Controls.Add(this.dL2ItemStockCountUnitLbl);
            this.dL2Pnl.Controls.Add(this.dL2ItemCodeCbb);
            this.dL2Pnl.Controls.Add(this.dL2SaleOrderCbb);
            this.dL2Pnl.Controls.Add(this.dL2CustomerCbb);
            this.dL2Pnl.Controls.Add(this.dL2PackageTypeCbb);
            this.dL2Pnl.Controls.Add(this.dL2PackageTypeLbl);
            this.dL2Pnl.Controls.Add(this.dL2CancelBtn);
            this.dL2Pnl.Controls.Add(this.dL2AddItemBtn);
            this.dL2Pnl.Controls.Add(this.dL2ItemDeliveryMeasureLbsLbl);
            this.dL2Pnl.Controls.Add(this.dL2ItemDeliveryMeasureLbl);
            this.dL2Pnl.Controls.Add(this.dL2ItemDeliveryCountLbl);
            this.dL2Pnl.Controls.Add(this.dL2ItemStockMeasureLbsLbl);
            this.dL2Pnl.Controls.Add(this.dL2ItemStockMeasureLbl);
            this.dL2Pnl.Controls.Add(this.dL2ItemStockMeasureLbsTxb);
            this.dL2Pnl.Controls.Add(this.dL2ItemDeliveryCountTxb);
            this.dL2Pnl.Controls.Add(this.dL2ItemDeliveryMeasureTxb);
            this.dL2Pnl.Controls.Add(this.dL2ItemDeliveryMeasureLbsTxb);
            this.dL2Pnl.Controls.Add(this.dL2ItemStockMeasureTxb);
            this.dL2Pnl.Controls.Add(this.dL2ItemStockCountTxb);
            this.dL2Pnl.Controls.Add(this.dL2ItemStockCountLbl);
            this.dL2Pnl.Controls.Add(this.dL2ItemCodeLbl);
            this.dL2Pnl.Controls.Add(this.dL2CustomerLbl);
            this.dL2Pnl.Controls.Add(this.dL2SaleOrderLbl);
            this.dL2Pnl.Controls.Add(this.dL2CreateDelBtn);
            this.dL2Pnl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dL2Pnl.HorizontalScrollbarBarColor = true;
            this.dL2Pnl.HorizontalScrollbarHighlightOnWheel = false;
            this.dL2Pnl.HorizontalScrollbarSize = 10;
            this.dL2Pnl.Location = new System.Drawing.Point(0, 0);
            this.dL2Pnl.Name = "dL2Pnl";
            this.dL2Pnl.Size = new System.Drawing.Size(1238, 663);
            this.dL2Pnl.TabIndex = 42;
            this.dL2Pnl.VerticalScrollbarBarColor = true;
            this.dL2Pnl.VerticalScrollbarHighlightOnWheel = false;
            this.dL2Pnl.VerticalScrollbarSize = 10;
            // 
            // dL2TopLbl
            // 
            this.dL2TopLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dL2TopLbl.AutoSize = true;
            this.dL2TopLbl.BackColor = System.Drawing.Color.White;
            this.dL2TopLbl.Font = new System.Drawing.Font("Microsoft Tai Le", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dL2TopLbl.Location = new System.Drawing.Point(559, 21);
            this.dL2TopLbl.Name = "dL2TopLbl";
            this.dL2TopLbl.Size = new System.Drawing.Size(170, 34);
            this.dL2TopLbl.TabIndex = 193;
            this.dL2TopLbl.Text = "สร้างใบจัดสินค้า";
            // 
            // dL2ShipDateDpk
            // 
            this.dL2ShipDateDpk.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dL2ShipDateDpk.CustomFormat = "d MMMM yyyy";
            this.dL2ShipDateDpk.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dL2ShipDateDpk.Location = new System.Drawing.Point(981, 82);
            this.dL2ShipDateDpk.MinimumSize = new System.Drawing.Size(4, 29);
            this.dL2ShipDateDpk.Name = "dL2ShipDateDpk";
            this.dL2ShipDateDpk.Size = new System.Drawing.Size(169, 29);
            this.dL2ShipDateDpk.TabIndex = 192;
            // 
            // metroLabel6
            // 
            this.metroLabel6.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel6.Location = new System.Drawing.Point(888, 82);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(78, 25);
            this.metroLabel6.TabIndex = 191;
            this.metroLabel6.Text = "วันที่จัดส่ง";
            // 
            // dL2DueDateTxb
            // 
            this.dL2DueDateTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.dL2DueDateTxb.CustomButton.Image = null;
            this.dL2DueDateTxb.CustomButton.Location = new System.Drawing.Point(149, 1);
            this.dL2DueDateTxb.CustomButton.Name = "";
            this.dL2DueDateTxb.CustomButton.Size = new System.Drawing.Size(27, 27);
            this.dL2DueDateTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.dL2DueDateTxb.CustomButton.TabIndex = 1;
            this.dL2DueDateTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.dL2DueDateTxb.CustomButton.UseSelectable = true;
            this.dL2DueDateTxb.CustomButton.Visible = false;
            this.dL2DueDateTxb.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.dL2DueDateTxb.Lines = new string[0];
            this.dL2DueDateTxb.Location = new System.Drawing.Point(148, 205);
            this.dL2DueDateTxb.MaxLength = 32767;
            this.dL2DueDateTxb.Name = "dL2DueDateTxb";
            this.dL2DueDateTxb.PasswordChar = '\0';
            this.dL2DueDateTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dL2DueDateTxb.SelectedText = "";
            this.dL2DueDateTxb.SelectionLength = 0;
            this.dL2DueDateTxb.SelectionStart = 0;
            this.dL2DueDateTxb.ShortcutsEnabled = true;
            this.dL2DueDateTxb.Size = new System.Drawing.Size(177, 29);
            this.dL2DueDateTxb.TabIndex = 190;
            this.dL2DueDateTxb.UseSelectable = true;
            this.dL2DueDateTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.dL2DueDateTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // dL2DeliveryDgv
            // 
            this.dL2DeliveryDgv.AllowUserToAddRows = false;
            this.dL2DeliveryDgv.AllowUserToDeleteRows = false;
            this.dL2DeliveryDgv.AllowUserToResizeRows = false;
            this.dL2DeliveryDgv.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dL2DeliveryDgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dL2DeliveryDgv.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dL2DeliveryDgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dL2DeliveryDgv.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dL2DeliveryDgv.Location = new System.Drawing.Point(5, 302);
            this.dL2DeliveryDgv.Name = "dL2DeliveryDgv";
            this.dL2DeliveryDgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dL2DeliveryDgv.Size = new System.Drawing.Size(1227, 302);
            this.dL2DeliveryDgv.TabIndex = 189;
            this.dL2DeliveryDgv.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dL2DeliveryDgv_CellClick);
            this.dL2DeliveryDgv.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.DL2DeliveryDgv_CellEndEdit);
            this.dL2DeliveryDgv.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dL2DeliveryDgv_DataError);
            this.dL2DeliveryDgv.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.DL2DeliveryDgv_RowPostPaint);
            this.dL2DeliveryDgv.SelectionChanged += new System.EventHandler(this.DL2DeliveryDgv_SelectionChanged);
            // 
            // dL2SubmitBtn
            // 
            this.dL2SubmitBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.dL2SubmitBtn.BackColor = System.Drawing.Color.SteelBlue;
            this.dL2SubmitBtn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.dL2SubmitBtn.ForeColor = System.Drawing.Color.LightCyan;
            this.dL2SubmitBtn.Location = new System.Drawing.Point(517, 615);
            this.dL2SubmitBtn.Name = "dL2SubmitBtn";
            this.dL2SubmitBtn.Size = new System.Drawing.Size(150, 40);
            this.dL2SubmitBtn.TabIndex = 187;
            this.dL2SubmitBtn.Text = "บันทึกข้อมูล";
            this.dL2SubmitBtn.UseCustomBackColor = true;
            this.dL2SubmitBtn.UseCustomForeColor = true;
            this.dL2SubmitBtn.UseSelectable = true;
            this.dL2SubmitBtn.Click += new System.EventHandler(this.DL2SubmitBtn_Click);
            // 
            // dL2ShipDateLbl
            // 
            this.dL2ShipDateLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dL2ShipDateLbl.AutoSize = true;
            this.dL2ShipDateLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.dL2ShipDateLbl.Location = new System.Drawing.Point(40, 204);
            this.dL2ShipDateLbl.Name = "dL2ShipDateLbl";
            this.dL2ShipDateLbl.Size = new System.Drawing.Size(109, 25);
            this.dL2ShipDateLbl.TabIndex = 184;
            this.dL2ShipDateLbl.Text = "วันครบกำหนด";
            // 
            // dL3PrintFormBtn
            // 
            this.dL3PrintFormBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.dL3PrintFormBtn.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.dL3PrintFormBtn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.dL3PrintFormBtn.ForeColor = System.Drawing.Color.FloralWhite;
            this.dL3PrintFormBtn.Location = new System.Drawing.Point(708, 615);
            this.dL3PrintFormBtn.Name = "dL3PrintFormBtn";
            this.dL3PrintFormBtn.Size = new System.Drawing.Size(150, 40);
            this.dL3PrintFormBtn.TabIndex = 37;
            this.dL3PrintFormBtn.Text = "พิมพ์แบบฟอร์ม";
            this.dL3PrintFormBtn.UseCustomBackColor = true;
            this.dL3PrintFormBtn.UseCustomForeColor = true;
            this.dL3PrintFormBtn.UseSelectable = true;
            this.dL3PrintFormBtn.Click += new System.EventHandler(this.DL3PrintFormBtn_Click);
            // 
            // metroLabel188
            // 
            this.metroLabel188.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.metroLabel188.AutoSize = true;
            this.metroLabel188.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel188.Location = new System.Drawing.Point(1089, 205);
            this.metroLabel188.Name = "metroLabel188";
            this.metroLabel188.Size = new System.Drawing.Size(53, 25);
            this.metroLabel188.TabIndex = 183;
            this.metroLabel188.Text = "ปอนด์";
            // 
            // dL2ItemDeliveryMeasureUnitLbl
            // 
            this.dL2ItemDeliveryMeasureUnitLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dL2ItemDeliveryMeasureUnitLbl.AutoSize = true;
            this.dL2ItemDeliveryMeasureUnitLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.dL2ItemDeliveryMeasureUnitLbl.Location = new System.Drawing.Point(1089, 165);
            this.dL2ItemDeliveryMeasureUnitLbl.Name = "dL2ItemDeliveryMeasureUnitLbl";
            this.dL2ItemDeliveryMeasureUnitLbl.Size = new System.Drawing.Size(72, 25);
            this.dL2ItemDeliveryMeasureUnitLbl.TabIndex = 182;
            this.dL2ItemDeliveryMeasureUnitLbl.Text = "กก./หลา";
            // 
            // dL2ItemDeliveryCountUnitLbl
            // 
            this.dL2ItemDeliveryCountUnitLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dL2ItemDeliveryCountUnitLbl.AutoSize = true;
            this.dL2ItemDeliveryCountUnitLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.dL2ItemDeliveryCountUnitLbl.Location = new System.Drawing.Point(1089, 124);
            this.dL2ItemDeliveryCountUnitLbl.Name = "dL2ItemDeliveryCountUnitLbl";
            this.dL2ItemDeliveryCountUnitLbl.Size = new System.Drawing.Size(95, 25);
            this.dL2ItemDeliveryCountUnitLbl.TabIndex = 181;
            this.dL2ItemDeliveryCountUnitLbl.Text = "กระสอบ/พับ";
            // 
            // dL2ItemStockMeasureUnitLbsLbl
            // 
            this.dL2ItemStockMeasureUnitLbsLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dL2ItemStockMeasureUnitLbsLbl.AutoSize = true;
            this.dL2ItemStockMeasureUnitLbsLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.dL2ItemStockMeasureUnitLbsLbl.Location = new System.Drawing.Point(714, 166);
            this.dL2ItemStockMeasureUnitLbsLbl.Name = "dL2ItemStockMeasureUnitLbsLbl";
            this.dL2ItemStockMeasureUnitLbsLbl.Size = new System.Drawing.Size(53, 25);
            this.dL2ItemStockMeasureUnitLbsLbl.TabIndex = 180;
            this.dL2ItemStockMeasureUnitLbsLbl.Text = "ปอนด์";
            // 
            // dL2ItemStockMeasureUnitLbl
            // 
            this.dL2ItemStockMeasureUnitLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dL2ItemStockMeasureUnitLbl.AutoSize = true;
            this.dL2ItemStockMeasureUnitLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.dL2ItemStockMeasureUnitLbl.Location = new System.Drawing.Point(714, 124);
            this.dL2ItemStockMeasureUnitLbl.Name = "dL2ItemStockMeasureUnitLbl";
            this.dL2ItemStockMeasureUnitLbl.Size = new System.Drawing.Size(72, 25);
            this.dL2ItemStockMeasureUnitLbl.TabIndex = 179;
            this.dL2ItemStockMeasureUnitLbl.Text = "กก./หลา";
            // 
            // dL2ItemStockCountUnitLbl
            // 
            this.dL2ItemStockCountUnitLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dL2ItemStockCountUnitLbl.AutoSize = true;
            this.dL2ItemStockCountUnitLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.dL2ItemStockCountUnitLbl.Location = new System.Drawing.Point(714, 81);
            this.dL2ItemStockCountUnitLbl.Name = "dL2ItemStockCountUnitLbl";
            this.dL2ItemStockCountUnitLbl.Size = new System.Drawing.Size(95, 25);
            this.dL2ItemStockCountUnitLbl.TabIndex = 178;
            this.dL2ItemStockCountUnitLbl.Text = "กระสอบ/พับ";
            // 
            // dL2ItemCodeCbb
            // 
            this.dL2ItemCodeCbb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dL2ItemCodeCbb.FormattingEnabled = true;
            this.dL2ItemCodeCbb.ItemHeight = 23;
            this.dL2ItemCodeCbb.Location = new System.Drawing.Point(148, 165);
            this.dL2ItemCodeCbb.Name = "dL2ItemCodeCbb";
            this.dL2ItemCodeCbb.Size = new System.Drawing.Size(176, 29);
            this.dL2ItemCodeCbb.TabIndex = 177;
            this.dL2ItemCodeCbb.UseSelectable = true;
            this.dL2ItemCodeCbb.SelectedIndexChanged += new System.EventHandler(this.DL2ItemCodeCbb_SelectedIndexChanged);
            // 
            // dL2SaleOrderCbb
            // 
            this.dL2SaleOrderCbb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dL2SaleOrderCbb.FormattingEnabled = true;
            this.dL2SaleOrderCbb.ItemHeight = 23;
            this.dL2SaleOrderCbb.Location = new System.Drawing.Point(148, 124);
            this.dL2SaleOrderCbb.Name = "dL2SaleOrderCbb";
            this.dL2SaleOrderCbb.Size = new System.Drawing.Size(176, 29);
            this.dL2SaleOrderCbb.TabIndex = 176;
            this.dL2SaleOrderCbb.UseSelectable = true;
            this.dL2SaleOrderCbb.SelectedIndexChanged += new System.EventHandler(this.DL2SaleOrderCbb_SelectedIndexChanged);
            // 
            // dL2CustomerCbb
            // 
            this.dL2CustomerCbb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dL2CustomerCbb.FormattingEnabled = true;
            this.dL2CustomerCbb.ItemHeight = 23;
            this.dL2CustomerCbb.Location = new System.Drawing.Point(148, 81);
            this.dL2CustomerCbb.Name = "dL2CustomerCbb";
            this.dL2CustomerCbb.Size = new System.Drawing.Size(257, 29);
            this.dL2CustomerCbb.TabIndex = 175;
            this.dL2CustomerCbb.UseSelectable = true;
            this.dL2CustomerCbb.SelectedIndexChanged += new System.EventHandler(this.DL2CustomerCbb_SelectedIndexChanged);
            // 
            // dL2PackageTypeCbb
            // 
            this.dL2PackageTypeCbb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dL2PackageTypeCbb.FormattingEnabled = true;
            this.dL2PackageTypeCbb.ItemHeight = 23;
            this.dL2PackageTypeCbb.Location = new System.Drawing.Point(607, 206);
            this.dL2PackageTypeCbb.Name = "dL2PackageTypeCbb";
            this.dL2PackageTypeCbb.Size = new System.Drawing.Size(121, 29);
            this.dL2PackageTypeCbb.TabIndex = 174;
            this.dL2PackageTypeCbb.UseSelectable = true;
            this.dL2PackageTypeCbb.Visible = false;
            // 
            // dL2PackageTypeLbl
            // 
            this.dL2PackageTypeLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dL2PackageTypeLbl.AutoSize = true;
            this.dL2PackageTypeLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.dL2PackageTypeLbl.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.dL2PackageTypeLbl.Location = new System.Drawing.Point(429, 206);
            this.dL2PackageTypeLbl.Name = "dL2PackageTypeLbl";
            this.dL2PackageTypeLbl.Size = new System.Drawing.Size(165, 25);
            this.dL2PackageTypeLbl.TabIndex = 89;
            this.dL2PackageTypeLbl.Text = "ประเภทของบรรจุภัณฑ์";
            this.dL2PackageTypeLbl.Visible = false;
            // 
            // dL2CancelBtn
            // 
            this.dL2CancelBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.dL2CancelBtn.BackColor = System.Drawing.Color.IndianRed;
            this.dL2CancelBtn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.dL2CancelBtn.ForeColor = System.Drawing.Color.MistyRose;
            this.dL2CancelBtn.Location = new System.Drawing.Point(1090, 615);
            this.dL2CancelBtn.Name = "dL2CancelBtn";
            this.dL2CancelBtn.Size = new System.Drawing.Size(150, 40);
            this.dL2CancelBtn.TabIndex = 88;
            this.dL2CancelBtn.Text = "ออกจากหน้านี้";
            this.dL2CancelBtn.UseCustomBackColor = true;
            this.dL2CancelBtn.UseCustomForeColor = true;
            this.dL2CancelBtn.UseSelectable = true;
            this.dL2CancelBtn.Click += new System.EventHandler(this.MetroButton18_Click_1);
            // 
            // dL2AddItemBtn
            // 
            this.dL2AddItemBtn.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dL2AddItemBtn.BackColor = System.Drawing.Color.MediumPurple;
            this.dL2AddItemBtn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.dL2AddItemBtn.ForeColor = System.Drawing.Color.Lavender;
            this.dL2AddItemBtn.Location = new System.Drawing.Point(981, 250);
            this.dL2AddItemBtn.Name = "dL2AddItemBtn";
            this.dL2AddItemBtn.Size = new System.Drawing.Size(150, 40);
            this.dL2AddItemBtn.TabIndex = 87;
            this.dL2AddItemBtn.Text = "เพิ่มรายการ";
            this.dL2AddItemBtn.UseCustomBackColor = true;
            this.dL2AddItemBtn.UseCustomForeColor = true;
            this.dL2AddItemBtn.UseSelectable = true;
            this.dL2AddItemBtn.Click += new System.EventHandler(this.DL2AddItemBtn_Click);
            // 
            // dL2ItemDeliveryMeasureLbsLbl
            // 
            this.dL2ItemDeliveryMeasureLbsLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dL2ItemDeliveryMeasureLbsLbl.AutoSize = true;
            this.dL2ItemDeliveryMeasureLbsLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.dL2ItemDeliveryMeasureLbsLbl.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.dL2ItemDeliveryMeasureLbsLbl.Location = new System.Drawing.Point(819, 205);
            this.dL2ItemDeliveryMeasureLbsLbl.Name = "dL2ItemDeliveryMeasureLbsLbl";
            this.dL2ItemDeliveryMeasureLbsLbl.Size = new System.Drawing.Size(147, 25);
            this.dL2ItemDeliveryMeasureLbsLbl.TabIndex = 65;
            this.dL2ItemDeliveryMeasureLbsLbl.Text = "ปริมาณสินค้าที่จะส่ง";
            this.dL2ItemDeliveryMeasureLbsLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dL2ItemDeliveryMeasureLbl
            // 
            this.dL2ItemDeliveryMeasureLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dL2ItemDeliveryMeasureLbl.AutoSize = true;
            this.dL2ItemDeliveryMeasureLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.dL2ItemDeliveryMeasureLbl.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.dL2ItemDeliveryMeasureLbl.Location = new System.Drawing.Point(819, 165);
            this.dL2ItemDeliveryMeasureLbl.Name = "dL2ItemDeliveryMeasureLbl";
            this.dL2ItemDeliveryMeasureLbl.Size = new System.Drawing.Size(147, 25);
            this.dL2ItemDeliveryMeasureLbl.TabIndex = 64;
            this.dL2ItemDeliveryMeasureLbl.Text = "ปริมาณสินค้าที่จะส่ง";
            this.dL2ItemDeliveryMeasureLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dL2ItemDeliveryCountLbl
            // 
            this.dL2ItemDeliveryCountLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dL2ItemDeliveryCountLbl.AutoSize = true;
            this.dL2ItemDeliveryCountLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.dL2ItemDeliveryCountLbl.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.dL2ItemDeliveryCountLbl.Location = new System.Drawing.Point(825, 124);
            this.dL2ItemDeliveryCountLbl.Name = "dL2ItemDeliveryCountLbl";
            this.dL2ItemDeliveryCountLbl.Size = new System.Drawing.Size(141, 25);
            this.dL2ItemDeliveryCountLbl.TabIndex = 63;
            this.dL2ItemDeliveryCountLbl.Text = "จำนวนสินค้าที่จะส่ง";
            this.dL2ItemDeliveryCountLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dL2ItemStockMeasureLbsLbl
            // 
            this.dL2ItemStockMeasureLbsLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dL2ItemStockMeasureLbsLbl.AutoSize = true;
            this.dL2ItemStockMeasureLbsLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.dL2ItemStockMeasureLbsLbl.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.dL2ItemStockMeasureLbsLbl.Location = new System.Drawing.Point(429, 166);
            this.dL2ItemStockMeasureLbsLbl.Name = "dL2ItemStockMeasureLbsLbl";
            this.dL2ItemStockMeasureLbsLbl.Size = new System.Drawing.Size(170, 25);
            this.dL2ItemStockMeasureLbsLbl.TabIndex = 62;
            this.dL2ItemStockMeasureLbsLbl.Text = "ปริมาณสินค้าที่มีในคลัง";
            this.dL2ItemStockMeasureLbsLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dL2ItemStockMeasureLbl
            // 
            this.dL2ItemStockMeasureLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dL2ItemStockMeasureLbl.AutoSize = true;
            this.dL2ItemStockMeasureLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.dL2ItemStockMeasureLbl.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.dL2ItemStockMeasureLbl.Location = new System.Drawing.Point(430, 124);
            this.dL2ItemStockMeasureLbl.Name = "dL2ItemStockMeasureLbl";
            this.dL2ItemStockMeasureLbl.Size = new System.Drawing.Size(170, 25);
            this.dL2ItemStockMeasureLbl.TabIndex = 61;
            this.dL2ItemStockMeasureLbl.Text = "ปริมาณสินค้าที่มีในคลัง";
            this.dL2ItemStockMeasureLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dL2ItemStockMeasureLbsTxb
            // 
            this.dL2ItemStockMeasureLbsTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.dL2ItemStockMeasureLbsTxb.CustomButton.Image = null;
            this.dL2ItemStockMeasureLbsTxb.CustomButton.Location = new System.Drawing.Point(73, 2);
            this.dL2ItemStockMeasureLbsTxb.CustomButton.Name = "";
            this.dL2ItemStockMeasureLbsTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.dL2ItemStockMeasureLbsTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.dL2ItemStockMeasureLbsTxb.CustomButton.TabIndex = 1;
            this.dL2ItemStockMeasureLbsTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.dL2ItemStockMeasureLbsTxb.CustomButton.UseSelectable = true;
            this.dL2ItemStockMeasureLbsTxb.CustomButton.Visible = false;
            this.dL2ItemStockMeasureLbsTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.dL2ItemStockMeasureLbsTxb.Lines = new string[0];
            this.dL2ItemStockMeasureLbsTxb.Location = new System.Drawing.Point(607, 166);
            this.dL2ItemStockMeasureLbsTxb.MaxLength = 32767;
            this.dL2ItemStockMeasureLbsTxb.Name = "dL2ItemStockMeasureLbsTxb";
            this.dL2ItemStockMeasureLbsTxb.PasswordChar = '\0';
            this.dL2ItemStockMeasureLbsTxb.ReadOnly = true;
            this.dL2ItemStockMeasureLbsTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dL2ItemStockMeasureLbsTxb.SelectedText = "";
            this.dL2ItemStockMeasureLbsTxb.SelectionLength = 0;
            this.dL2ItemStockMeasureLbsTxb.SelectionStart = 0;
            this.dL2ItemStockMeasureLbsTxb.ShortcutsEnabled = true;
            this.dL2ItemStockMeasureLbsTxb.Size = new System.Drawing.Size(101, 30);
            this.dL2ItemStockMeasureLbsTxb.TabIndex = 60;
            this.dL2ItemStockMeasureLbsTxb.UseSelectable = true;
            this.dL2ItemStockMeasureLbsTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.dL2ItemStockMeasureLbsTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // dL2ItemDeliveryCountTxb
            // 
            this.dL2ItemDeliveryCountTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.dL2ItemDeliveryCountTxb.CustomButton.Image = null;
            this.dL2ItemDeliveryCountTxb.CustomButton.Location = new System.Drawing.Point(75, 2);
            this.dL2ItemDeliveryCountTxb.CustomButton.Name = "";
            this.dL2ItemDeliveryCountTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.dL2ItemDeliveryCountTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.dL2ItemDeliveryCountTxb.CustomButton.TabIndex = 1;
            this.dL2ItemDeliveryCountTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.dL2ItemDeliveryCountTxb.CustomButton.UseSelectable = true;
            this.dL2ItemDeliveryCountTxb.CustomButton.Visible = false;
            this.dL2ItemDeliveryCountTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.dL2ItemDeliveryCountTxb.Lines = new string[0];
            this.dL2ItemDeliveryCountTxb.Location = new System.Drawing.Point(981, 124);
            this.dL2ItemDeliveryCountTxb.MaxLength = 32767;
            this.dL2ItemDeliveryCountTxb.Name = "dL2ItemDeliveryCountTxb";
            this.dL2ItemDeliveryCountTxb.PasswordChar = '\0';
            this.dL2ItemDeliveryCountTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dL2ItemDeliveryCountTxb.SelectedText = "";
            this.dL2ItemDeliveryCountTxb.SelectionLength = 0;
            this.dL2ItemDeliveryCountTxb.SelectionStart = 0;
            this.dL2ItemDeliveryCountTxb.ShortcutsEnabled = true;
            this.dL2ItemDeliveryCountTxb.Size = new System.Drawing.Size(103, 30);
            this.dL2ItemDeliveryCountTxb.TabIndex = 59;
            this.dL2ItemDeliveryCountTxb.UseSelectable = true;
            this.dL2ItemDeliveryCountTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.dL2ItemDeliveryCountTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.dL2ItemDeliveryCountTxb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.DL2ItemDeliveryCountTxb_KeyPress);
            this.dL2ItemDeliveryCountTxb.KeyUp += new System.Windows.Forms.KeyEventHandler(this.DL2ItemDeliveryCountTxb_KeyUp);
            // 
            // dL2ItemDeliveryMeasureTxb
            // 
            this.dL2ItemDeliveryMeasureTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.dL2ItemDeliveryMeasureTxb.CustomButton.Image = null;
            this.dL2ItemDeliveryMeasureTxb.CustomButton.Location = new System.Drawing.Point(75, 2);
            this.dL2ItemDeliveryMeasureTxb.CustomButton.Name = "";
            this.dL2ItemDeliveryMeasureTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.dL2ItemDeliveryMeasureTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.dL2ItemDeliveryMeasureTxb.CustomButton.TabIndex = 1;
            this.dL2ItemDeliveryMeasureTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.dL2ItemDeliveryMeasureTxb.CustomButton.UseSelectable = true;
            this.dL2ItemDeliveryMeasureTxb.CustomButton.Visible = false;
            this.dL2ItemDeliveryMeasureTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.dL2ItemDeliveryMeasureTxb.Lines = new string[0];
            this.dL2ItemDeliveryMeasureTxb.Location = new System.Drawing.Point(981, 165);
            this.dL2ItemDeliveryMeasureTxb.MaxLength = 32767;
            this.dL2ItemDeliveryMeasureTxb.Name = "dL2ItemDeliveryMeasureTxb";
            this.dL2ItemDeliveryMeasureTxb.PasswordChar = '\0';
            this.dL2ItemDeliveryMeasureTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dL2ItemDeliveryMeasureTxb.SelectedText = "";
            this.dL2ItemDeliveryMeasureTxb.SelectionLength = 0;
            this.dL2ItemDeliveryMeasureTxb.SelectionStart = 0;
            this.dL2ItemDeliveryMeasureTxb.ShortcutsEnabled = true;
            this.dL2ItemDeliveryMeasureTxb.Size = new System.Drawing.Size(103, 30);
            this.dL2ItemDeliveryMeasureTxb.TabIndex = 58;
            this.dL2ItemDeliveryMeasureTxb.UseSelectable = true;
            this.dL2ItemDeliveryMeasureTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.dL2ItemDeliveryMeasureTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.dL2ItemDeliveryMeasureTxb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.DL2ItemDeliveryMeasureTxb_KeyPress);
            this.dL2ItemDeliveryMeasureTxb.KeyUp += new System.Windows.Forms.KeyEventHandler(this.DL2ItemDeliveryMeasureTxb_KeyUp);
            // 
            // dL2ItemDeliveryMeasureLbsTxb
            // 
            this.dL2ItemDeliveryMeasureLbsTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.dL2ItemDeliveryMeasureLbsTxb.CustomButton.Image = null;
            this.dL2ItemDeliveryMeasureLbsTxb.CustomButton.Location = new System.Drawing.Point(75, 2);
            this.dL2ItemDeliveryMeasureLbsTxb.CustomButton.Name = "";
            this.dL2ItemDeliveryMeasureLbsTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.dL2ItemDeliveryMeasureLbsTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.dL2ItemDeliveryMeasureLbsTxb.CustomButton.TabIndex = 1;
            this.dL2ItemDeliveryMeasureLbsTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.dL2ItemDeliveryMeasureLbsTxb.CustomButton.UseSelectable = true;
            this.dL2ItemDeliveryMeasureLbsTxb.CustomButton.Visible = false;
            this.dL2ItemDeliveryMeasureLbsTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.dL2ItemDeliveryMeasureLbsTxb.Lines = new string[0];
            this.dL2ItemDeliveryMeasureLbsTxb.Location = new System.Drawing.Point(981, 205);
            this.dL2ItemDeliveryMeasureLbsTxb.MaxLength = 32767;
            this.dL2ItemDeliveryMeasureLbsTxb.Name = "dL2ItemDeliveryMeasureLbsTxb";
            this.dL2ItemDeliveryMeasureLbsTxb.PasswordChar = '\0';
            this.dL2ItemDeliveryMeasureLbsTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dL2ItemDeliveryMeasureLbsTxb.SelectedText = "";
            this.dL2ItemDeliveryMeasureLbsTxb.SelectionLength = 0;
            this.dL2ItemDeliveryMeasureLbsTxb.SelectionStart = 0;
            this.dL2ItemDeliveryMeasureLbsTxb.ShortcutsEnabled = true;
            this.dL2ItemDeliveryMeasureLbsTxb.Size = new System.Drawing.Size(103, 30);
            this.dL2ItemDeliveryMeasureLbsTxb.TabIndex = 57;
            this.dL2ItemDeliveryMeasureLbsTxb.UseSelectable = true;
            this.dL2ItemDeliveryMeasureLbsTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.dL2ItemDeliveryMeasureLbsTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.dL2ItemDeliveryMeasureLbsTxb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.DL2ItemDeliveryMeasureLbsTxb_KeyPress);
            this.dL2ItemDeliveryMeasureLbsTxb.KeyUp += new System.Windows.Forms.KeyEventHandler(this.DL2ItemDeliveryMeasureLbsTxb_KeyUp);
            // 
            // dL2ItemStockMeasureTxb
            // 
            this.dL2ItemStockMeasureTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.dL2ItemStockMeasureTxb.CustomButton.Image = null;
            this.dL2ItemStockMeasureTxb.CustomButton.Location = new System.Drawing.Point(73, 2);
            this.dL2ItemStockMeasureTxb.CustomButton.Name = "";
            this.dL2ItemStockMeasureTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.dL2ItemStockMeasureTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.dL2ItemStockMeasureTxb.CustomButton.TabIndex = 1;
            this.dL2ItemStockMeasureTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.dL2ItemStockMeasureTxb.CustomButton.UseSelectable = true;
            this.dL2ItemStockMeasureTxb.CustomButton.Visible = false;
            this.dL2ItemStockMeasureTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.dL2ItemStockMeasureTxb.Lines = new string[0];
            this.dL2ItemStockMeasureTxb.Location = new System.Drawing.Point(607, 124);
            this.dL2ItemStockMeasureTxb.MaxLength = 32767;
            this.dL2ItemStockMeasureTxb.Name = "dL2ItemStockMeasureTxb";
            this.dL2ItemStockMeasureTxb.PasswordChar = '\0';
            this.dL2ItemStockMeasureTxb.ReadOnly = true;
            this.dL2ItemStockMeasureTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dL2ItemStockMeasureTxb.SelectedText = "";
            this.dL2ItemStockMeasureTxb.SelectionLength = 0;
            this.dL2ItemStockMeasureTxb.SelectionStart = 0;
            this.dL2ItemStockMeasureTxb.ShortcutsEnabled = true;
            this.dL2ItemStockMeasureTxb.Size = new System.Drawing.Size(101, 30);
            this.dL2ItemStockMeasureTxb.TabIndex = 56;
            this.dL2ItemStockMeasureTxb.UseSelectable = true;
            this.dL2ItemStockMeasureTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.dL2ItemStockMeasureTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // dL2ItemStockCountTxb
            // 
            this.dL2ItemStockCountTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.dL2ItemStockCountTxb.CustomButton.Image = null;
            this.dL2ItemStockCountTxb.CustomButton.Location = new System.Drawing.Point(73, 2);
            this.dL2ItemStockCountTxb.CustomButton.Name = "";
            this.dL2ItemStockCountTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.dL2ItemStockCountTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.dL2ItemStockCountTxb.CustomButton.TabIndex = 1;
            this.dL2ItemStockCountTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.dL2ItemStockCountTxb.CustomButton.UseSelectable = true;
            this.dL2ItemStockCountTxb.CustomButton.Visible = false;
            this.dL2ItemStockCountTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.dL2ItemStockCountTxb.Lines = new string[0];
            this.dL2ItemStockCountTxb.Location = new System.Drawing.Point(607, 81);
            this.dL2ItemStockCountTxb.MaxLength = 32767;
            this.dL2ItemStockCountTxb.Name = "dL2ItemStockCountTxb";
            this.dL2ItemStockCountTxb.PasswordChar = '\0';
            this.dL2ItemStockCountTxb.ReadOnly = true;
            this.dL2ItemStockCountTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dL2ItemStockCountTxb.SelectedText = "";
            this.dL2ItemStockCountTxb.SelectionLength = 0;
            this.dL2ItemStockCountTxb.SelectionStart = 0;
            this.dL2ItemStockCountTxb.ShortcutsEnabled = true;
            this.dL2ItemStockCountTxb.Size = new System.Drawing.Size(101, 30);
            this.dL2ItemStockCountTxb.TabIndex = 55;
            this.dL2ItemStockCountTxb.UseSelectable = true;
            this.dL2ItemStockCountTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.dL2ItemStockCountTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // dL2ItemStockCountLbl
            // 
            this.dL2ItemStockCountLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dL2ItemStockCountLbl.AutoSize = true;
            this.dL2ItemStockCountLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.dL2ItemStockCountLbl.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.dL2ItemStockCountLbl.Location = new System.Drawing.Point(430, 81);
            this.dL2ItemStockCountLbl.Name = "dL2ItemStockCountLbl";
            this.dL2ItemStockCountLbl.Size = new System.Drawing.Size(164, 25);
            this.dL2ItemStockCountLbl.TabIndex = 51;
            this.dL2ItemStockCountLbl.Text = "จำนวนสินค้าที่มีในคลัง";
            this.dL2ItemStockCountLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dL2ItemCodeLbl
            // 
            this.dL2ItemCodeLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dL2ItemCodeLbl.AutoSize = true;
            this.dL2ItemCodeLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.dL2ItemCodeLbl.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.dL2ItemCodeLbl.Location = new System.Drawing.Point(40, 165);
            this.dL2ItemCodeLbl.Name = "dL2ItemCodeLbl";
            this.dL2ItemCodeLbl.Size = new System.Drawing.Size(80, 25);
            this.dL2ItemCodeLbl.TabIndex = 49;
            this.dL2ItemCodeLbl.Text = "รหัสสินค้า";
            this.dL2ItemCodeLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dL2CustomerLbl
            // 
            this.dL2CustomerLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dL2CustomerLbl.AutoSize = true;
            this.dL2CustomerLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.dL2CustomerLbl.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.dL2CustomerLbl.Location = new System.Drawing.Point(40, 80);
            this.dL2CustomerLbl.Name = "dL2CustomerLbl";
            this.dL2CustomerLbl.Size = new System.Drawing.Size(50, 25);
            this.dL2CustomerLbl.TabIndex = 48;
            this.dL2CustomerLbl.Text = "ลูกค้า";
            // 
            // dL2SaleOrderLbl
            // 
            this.dL2SaleOrderLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dL2SaleOrderLbl.AutoSize = true;
            this.dL2SaleOrderLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.dL2SaleOrderLbl.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.dL2SaleOrderLbl.Location = new System.Drawing.Point(40, 123);
            this.dL2SaleOrderLbl.Name = "dL2SaleOrderLbl";
            this.dL2SaleOrderLbl.Size = new System.Drawing.Size(92, 25);
            this.dL2SaleOrderLbl.TabIndex = 47;
            this.dL2SaleOrderLbl.Text = "Sale order";
            // 
            // dL2CreateDelBtn
            // 
            this.dL2CreateDelBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.dL2CreateDelBtn.BackColor = System.Drawing.Color.MediumPurple;
            this.dL2CreateDelBtn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.dL2CreateDelBtn.ForeColor = System.Drawing.Color.Lavender;
            this.dL2CreateDelBtn.Location = new System.Drawing.Point(899, 615);
            this.dL2CreateDelBtn.Name = "dL2CreateDelBtn";
            this.dL2CreateDelBtn.Size = new System.Drawing.Size(150, 40);
            this.dL2CreateDelBtn.TabIndex = 34;
            this.dL2CreateDelBtn.Text = "สร้างใบจัดสินค้า";
            this.dL2CreateDelBtn.UseCustomBackColor = true;
            this.dL2CreateDelBtn.UseCustomForeColor = true;
            this.dL2CreateDelBtn.UseSelectable = true;
            this.dL2CreateDelBtn.Click += new System.EventHandler(this.DL2CreateDelBtn_Click);
            // 
            // transferTabPage
            // 
            this.transferTabPage.Controls.Add(this.tR2Pnl);
            this.transferTabPage.Controls.Add(this.tR1Pnl);
            this.transferTabPage.HorizontalScrollbarBarColor = true;
            this.transferTabPage.HorizontalScrollbarHighlightOnWheel = false;
            this.transferTabPage.HorizontalScrollbarSize = 0;
            this.transferTabPage.Location = new System.Drawing.Point(4, 47);
            this.transferTabPage.Name = "transferTabPage";
            this.transferTabPage.Size = new System.Drawing.Size(1238, 663);
            this.transferTabPage.TabIndex = 1;
            this.transferTabPage.Text = "โอนย้าย";
            this.transferTabPage.VerticalScrollbarBarColor = true;
            this.transferTabPage.VerticalScrollbarHighlightOnWheel = false;
            this.transferTabPage.VerticalScrollbarSize = 0;
            this.transferTabPage.Enter += new System.EventHandler(this.TransferTabPage_Enter);
            // 
            // tR2Pnl
            // 
            this.tR2Pnl.Controls.Add(this.tR2TopLbl);
            this.tR2Pnl.Controls.Add(this.tR2BarcodePnl);
            this.tR2Pnl.Controls.Add(this.tR2RefNoCbb);
            this.tR2Pnl.Controls.Add(this.tR2MagicBtn);
            this.tR2Pnl.Controls.Add(this.tR2ItemDgv);
            this.tR2Pnl.Controls.Add(this.tR2BinLocationCbb);
            this.tR2Pnl.Controls.Add(this.tR2InRdb);
            this.tR2Pnl.Controls.Add(this.tR2OutRdb);
            this.tR2Pnl.Controls.Add(this.tR2BinLocationLbl);
            this.tR2Pnl.Controls.Add(this.tR2ReceiveBtn);
            this.tR2Pnl.Controls.Add(this.tR2TotalWeightPnl);
            this.tR2Pnl.Controls.Add(this.tR2AssignDateTxb);
            this.tR2Pnl.Controls.Add(this.tR2CancelBtn);
            this.tR2Pnl.Controls.Add(this.tR2PrintFormBtn);
            this.tR2Pnl.Controls.Add(this.tR2SubmitBtn);
            this.tR2Pnl.Controls.Add(this.tR2FromWhseCbb);
            this.tR2Pnl.Controls.Add(this.tR2ToWhseCbb);
            this.tR2Pnl.Controls.Add(this.tR2DocNoLbl);
            this.tR2Pnl.Controls.Add(this.tR2AssignDateLbl);
            this.tR2Pnl.Controls.Add(this.tR2RefNoLbl);
            this.tR2Pnl.Controls.Add(this.tR2DocNoTxb);
            this.tR2Pnl.Controls.Add(this.tR2FromWhseLbl);
            this.tR2Pnl.Controls.Add(this.tR2RemarkTxb);
            this.tR2Pnl.Controls.Add(this.tR2ToWhseLbl);
            this.tR2Pnl.Controls.Add(this.tR2RemarkLbl);
            this.tR2Pnl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tR2Pnl.HorizontalScrollbarBarColor = true;
            this.tR2Pnl.HorizontalScrollbarHighlightOnWheel = false;
            this.tR2Pnl.HorizontalScrollbarSize = 10;
            this.tR2Pnl.Location = new System.Drawing.Point(0, 0);
            this.tR2Pnl.Name = "tR2Pnl";
            this.tR2Pnl.Size = new System.Drawing.Size(1238, 663);
            this.tR2Pnl.TabIndex = 37;
            this.tR2Pnl.VerticalScrollbarBarColor = true;
            this.tR2Pnl.VerticalScrollbarHighlightOnWheel = false;
            this.tR2Pnl.VerticalScrollbarSize = 10;
            this.tR2Pnl.MouseHover += new System.EventHandler(this.TR2Pnl_MouseHover);
            // 
            // tR2TopLbl
            // 
            this.tR2TopLbl.AutoSize = true;
            this.tR2TopLbl.BackColor = System.Drawing.Color.White;
            this.tR2TopLbl.Font = new System.Drawing.Font("Microsoft Tai Le", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tR2TopLbl.Location = new System.Drawing.Point(568, 20);
            this.tR2TopLbl.Name = "tR2TopLbl";
            this.tR2TopLbl.Size = new System.Drawing.Size(173, 34);
            this.tR2TopLbl.TabIndex = 175;
            this.tR2TopLbl.Text = "ใบโอนถ่ายสินค้า";
            // 
            // tR2BarcodePnl
            // 
            this.tR2BarcodePnl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.tR2BarcodePnl.BackColor = System.Drawing.Color.Transparent;
            this.tR2BarcodePnl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tR2BarcodePnl.Controls.Add(this.metroLabel8);
            this.tR2BarcodePnl.HorizontalScrollbarBarColor = true;
            this.tR2BarcodePnl.HorizontalScrollbarHighlightOnWheel = false;
            this.tR2BarcodePnl.HorizontalScrollbarSize = 10;
            this.tR2BarcodePnl.Location = new System.Drawing.Point(10, 8);
            this.tR2BarcodePnl.Name = "tR2BarcodePnl";
            this.tR2BarcodePnl.Size = new System.Drawing.Size(281, 83);
            this.tR2BarcodePnl.TabIndex = 174;
            this.tR2BarcodePnl.UseCustomBackColor = true;
            this.tR2BarcodePnl.VerticalScrollbarBarColor = true;
            this.tR2BarcodePnl.VerticalScrollbarHighlightOnWheel = false;
            this.tR2BarcodePnl.VerticalScrollbarSize = 10;
            this.tR2BarcodePnl.MouseLeave += new System.EventHandler(this.tR2BarcodePnl_MouseLeave);
            this.tR2BarcodePnl.MouseHover += new System.EventHandler(this.tR2BarcodePnl_MouseHover);
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Location = new System.Drawing.Point(87, 5);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(90, 19);
            this.metroLabel8.TabIndex = 2;
            this.metroLabel8.Text = "Barcode Area";
            this.metroLabel8.UseCustomBackColor = true;
            // 
            // tR2RefNoCbb
            // 
            this.tR2RefNoCbb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.tR2RefNoCbb.FormattingEnabled = true;
            this.tR2RefNoCbb.ItemHeight = 23;
            this.tR2RefNoCbb.Location = new System.Drawing.Point(1009, 105);
            this.tR2RefNoCbb.Name = "tR2RefNoCbb";
            this.tR2RefNoCbb.Size = new System.Drawing.Size(185, 29);
            this.tR2RefNoCbb.TabIndex = 173;
            this.tR2RefNoCbb.UseSelectable = true;
            this.tR2RefNoCbb.SelectedIndexChanged += new System.EventHandler(this.TR2RefNoCbb_SelectedIndexChanged);
            // 
            // tR2MagicBtn
            // 
            this.tR2MagicBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tR2MagicBtn.DisplayFocus = true;
            this.tR2MagicBtn.Highlight = true;
            this.tR2MagicBtn.Location = new System.Drawing.Point(14, 610);
            this.tR2MagicBtn.Name = "tR2MagicBtn";
            this.tR2MagicBtn.Size = new System.Drawing.Size(351, 47);
            this.tR2MagicBtn.TabIndex = 172;
            this.tR2MagicBtn.Text = "Scan some random item!";
            this.tR2MagicBtn.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.tR2MagicBtn.UseSelectable = true;
            this.tR2MagicBtn.Visible = false;
            this.tR2MagicBtn.Click += new System.EventHandler(this.TR2MagicBtn_Click);
            // 
            // tR2ItemDgv
            // 
            this.tR2ItemDgv.AllowUserToAddRows = false;
            this.tR2ItemDgv.AllowUserToDeleteRows = false;
            this.tR2ItemDgv.AllowUserToResizeRows = false;
            this.tR2ItemDgv.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tR2ItemDgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.tR2ItemDgv.BackgroundColor = System.Drawing.SystemColors.Window;
            this.tR2ItemDgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tR2ItemDgv.Location = new System.Drawing.Point(3, 318);
            this.tR2ItemDgv.Name = "tR2ItemDgv";
            this.tR2ItemDgv.RowHeadersVisible = false;
            this.tR2ItemDgv.Size = new System.Drawing.Size(1229, 286);
            this.tR2ItemDgv.TabIndex = 142;
            this.tR2ItemDgv.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.TR2ItemDgv_CellClick);
            this.tR2ItemDgv.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.TR2ItemDgv_CellValueChanged);
            this.tR2ItemDgv.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.TR2ItemDgv_DataBindingComplete);
            this.tR2ItemDgv.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.TR2ItemDgv_RowPrePaint);
            this.tR2ItemDgv.MouseHover += new System.EventHandler(this.TR2ItemDgv_MouseHover);
            // 
            // tR2BinLocationCbb
            // 
            this.tR2BinLocationCbb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.tR2BinLocationCbb.FormattingEnabled = true;
            this.tR2BinLocationCbb.ItemHeight = 23;
            this.tR2BinLocationCbb.Location = new System.Drawing.Point(592, 203);
            this.tR2BinLocationCbb.Name = "tR2BinLocationCbb";
            this.tR2BinLocationCbb.Size = new System.Drawing.Size(122, 29);
            this.tR2BinLocationCbb.TabIndex = 141;
            this.tR2BinLocationCbb.UseSelectable = true;
            // 
            // tR2InRdb
            // 
            this.tR2InRdb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.tR2InRdb.AutoSize = true;
            this.tR2InRdb.Enabled = false;
            this.tR2InRdb.FontSize = MetroFramework.MetroCheckBoxSize.Tall;
            this.tR2InRdb.FontWeight = MetroFramework.MetroCheckBoxWeight.Bold;
            this.tR2InRdb.Location = new System.Drawing.Point(241, 126);
            this.tR2InRdb.Name = "tR2InRdb";
            this.tR2InRdb.Size = new System.Drawing.Size(77, 25);
            this.tR2InRdb.TabIndex = 140;
            this.tR2InRdb.Text = "รับเข้า";
            this.tR2InRdb.UseSelectable = true;
            // 
            // tR2OutRdb
            // 
            this.tR2OutRdb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.tR2OutRdb.AutoSize = true;
            this.tR2OutRdb.Checked = true;
            this.tR2OutRdb.Enabled = false;
            this.tR2OutRdb.FontSize = MetroFramework.MetroCheckBoxSize.Tall;
            this.tR2OutRdb.FontWeight = MetroFramework.MetroCheckBoxWeight.Bold;
            this.tR2OutRdb.Location = new System.Drawing.Point(118, 126);
            this.tR2OutRdb.Name = "tR2OutRdb";
            this.tR2OutRdb.Size = new System.Drawing.Size(93, 25);
            this.tR2OutRdb.TabIndex = 139;
            this.tR2OutRdb.TabStop = true;
            this.tR2OutRdb.Text = "โอนออก";
            this.tR2OutRdb.UseSelectable = true;
            // 
            // tR2BinLocationLbl
            // 
            this.tR2BinLocationLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.tR2BinLocationLbl.AutoSize = true;
            this.tR2BinLocationLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.tR2BinLocationLbl.Location = new System.Drawing.Point(488, 203);
            this.tR2BinLocationLbl.Name = "tR2BinLocationLbl";
            this.tR2BinLocationLbl.Size = new System.Drawing.Size(97, 25);
            this.tR2BinLocationLbl.TabIndex = 137;
            this.tR2BinLocationLbl.Text = "ลอคที่จัดเก็บ";
            // 
            // tR2ReceiveBtn
            // 
            this.tR2ReceiveBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.tR2ReceiveBtn.BackColor = System.Drawing.Color.MediumPurple;
            this.tR2ReceiveBtn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.tR2ReceiveBtn.ForeColor = System.Drawing.Color.Lavender;
            this.tR2ReceiveBtn.Location = new System.Drawing.Point(905, 615);
            this.tR2ReceiveBtn.Name = "tR2ReceiveBtn";
            this.tR2ReceiveBtn.Size = new System.Drawing.Size(150, 40);
            this.tR2ReceiveBtn.TabIndex = 136;
            this.tR2ReceiveBtn.Text = "ยันยืนการรับสินค้า";
            this.tR2ReceiveBtn.UseCustomBackColor = true;
            this.tR2ReceiveBtn.UseCustomForeColor = true;
            this.tR2ReceiveBtn.UseSelectable = true;
            this.tR2ReceiveBtn.Click += new System.EventHandler(this.TR2ReceiveBtn_Click);
            // 
            // tR2TotalWeightPnl
            // 
            this.tR2TotalWeightPnl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tR2TotalWeightPnl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tR2TotalWeightPnl.Controls.Add(this.metroPanel17);
            this.tR2TotalWeightPnl.Controls.Add(this.metroPanel18);
            this.tR2TotalWeightPnl.Controls.Add(this.metroPanel20);
            this.tR2TotalWeightPnl.Controls.Add(this.tR2TotalWeightLbl);
            this.tR2TotalWeightPnl.HorizontalScrollbarBarColor = true;
            this.tR2TotalWeightPnl.HorizontalScrollbarHighlightOnWheel = false;
            this.tR2TotalWeightPnl.HorizontalScrollbarSize = 10;
            this.tR2TotalWeightPnl.Location = new System.Drawing.Point(744, 261);
            this.tR2TotalWeightPnl.Name = "tR2TotalWeightPnl";
            this.tR2TotalWeightPnl.Size = new System.Drawing.Size(488, 57);
            this.tR2TotalWeightPnl.TabIndex = 135;
            this.tR2TotalWeightPnl.VerticalScrollbarBarColor = true;
            this.tR2TotalWeightPnl.VerticalScrollbarHighlightOnWheel = false;
            this.tR2TotalWeightPnl.VerticalScrollbarSize = 10;
            // 
            // metroPanel17
            // 
            this.metroPanel17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.metroPanel17.Controls.Add(this.metroLabel15);
            this.metroPanel17.Controls.Add(this.tR2TotalCountTxb);
            this.metroPanel17.Controls.Add(this.tR2TotalCountUnitLbl);
            this.metroPanel17.HorizontalScrollbarBarColor = true;
            this.metroPanel17.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel17.HorizontalScrollbarSize = 10;
            this.metroPanel17.Location = new System.Drawing.Point(-1, 25);
            this.metroPanel17.Name = "metroPanel17";
            this.metroPanel17.Size = new System.Drawing.Size(181, 38);
            this.metroPanel17.TabIndex = 98;
            this.metroPanel17.VerticalScrollbarBarColor = true;
            this.metroPanel17.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel17.VerticalScrollbarSize = 10;
            // 
            // metroLabel15
            // 
            this.metroLabel15.AutoSize = true;
            this.metroLabel15.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel15.Location = new System.Drawing.Point(6, 3);
            this.metroLabel15.Name = "metroLabel15";
            this.metroLabel15.Size = new System.Drawing.Size(61, 25);
            this.metroLabel15.TabIndex = 175;
            this.metroLabel15.Text = "ทั้งหมด";
            // 
            // tR2TotalCountTxb
            // 
            // 
            // 
            // 
            this.tR2TotalCountTxb.CustomButton.Image = null;
            this.tR2TotalCountTxb.CustomButton.Location = new System.Drawing.Point(41, 2);
            this.tR2TotalCountTxb.CustomButton.Name = "";
            this.tR2TotalCountTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.tR2TotalCountTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tR2TotalCountTxb.CustomButton.TabIndex = 1;
            this.tR2TotalCountTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tR2TotalCountTxb.CustomButton.UseSelectable = true;
            this.tR2TotalCountTxb.CustomButton.Visible = false;
            this.tR2TotalCountTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.tR2TotalCountTxb.ForeColor = System.Drawing.Color.Gray;
            this.tR2TotalCountTxb.Lines = new string[0];
            this.tR2TotalCountTxb.Location = new System.Drawing.Point(69, 0);
            this.tR2TotalCountTxb.MaxLength = 32767;
            this.tR2TotalCountTxb.Name = "tR2TotalCountTxb";
            this.tR2TotalCountTxb.PasswordChar = '\0';
            this.tR2TotalCountTxb.ReadOnly = true;
            this.tR2TotalCountTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tR2TotalCountTxb.SelectedText = "";
            this.tR2TotalCountTxb.SelectionLength = 0;
            this.tR2TotalCountTxb.SelectionStart = 0;
            this.tR2TotalCountTxb.ShortcutsEnabled = true;
            this.tR2TotalCountTxb.Size = new System.Drawing.Size(69, 30);
            this.tR2TotalCountTxb.TabIndex = 96;
            this.tR2TotalCountTxb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tR2TotalCountTxb.UseCustomForeColor = true;
            this.tR2TotalCountTxb.UseSelectable = true;
            this.tR2TotalCountTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tR2TotalCountTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // tR2TotalCountUnitLbl
            // 
            this.tR2TotalCountUnitLbl.AutoSize = true;
            this.tR2TotalCountUnitLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.tR2TotalCountUnitLbl.Location = new System.Drawing.Point(144, 2);
            this.tR2TotalCountUnitLbl.Name = "tR2TotalCountUnitLbl";
            this.tR2TotalCountUnitLbl.Size = new System.Drawing.Size(34, 25);
            this.tR2TotalCountUnitLbl.TabIndex = 97;
            this.tR2TotalCountUnitLbl.Text = "ชิ้น";
            // 
            // metroPanel18
            // 
            this.metroPanel18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.metroPanel18.Controls.Add(this.metroLabel17);
            this.metroPanel18.Controls.Add(this.tR2TotalNetWeightUnitLbl);
            this.metroPanel18.Controls.Add(this.tR2TotalNetWeightTxb);
            this.metroPanel18.HorizontalScrollbarBarColor = true;
            this.metroPanel18.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel18.HorizontalScrollbarSize = 10;
            this.metroPanel18.Location = new System.Drawing.Point(332, 25);
            this.metroPanel18.Name = "metroPanel18";
            this.metroPanel18.Size = new System.Drawing.Size(155, 38);
            this.metroPanel18.TabIndex = 97;
            this.metroPanel18.VerticalScrollbarBarColor = true;
            this.metroPanel18.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel18.VerticalScrollbarSize = 10;
            // 
            // metroLabel17
            // 
            this.metroLabel17.AutoSize = true;
            this.metroLabel17.Location = new System.Drawing.Point(5, 4);
            this.metroLabel17.Name = "metroLabel17";
            this.metroLabel17.Size = new System.Drawing.Size(30, 19);
            this.metroLabel17.TabIndex = 175;
            this.metroLabel17.Text = "Net";
            // 
            // tR2TotalNetWeightUnitLbl
            // 
            this.tR2TotalNetWeightUnitLbl.AutoSize = true;
            this.tR2TotalNetWeightUnitLbl.Location = new System.Drawing.Point(121, 3);
            this.tR2TotalNetWeightUnitLbl.Name = "tR2TotalNetWeightUnitLbl";
            this.tR2TotalNetWeightUnitLbl.Size = new System.Drawing.Size(27, 19);
            this.tR2TotalNetWeightUnitLbl.TabIndex = 100;
            this.tR2TotalNetWeightUnitLbl.Text = "Kg.";
            // 
            // tR2TotalNetWeightTxb
            // 
            // 
            // 
            // 
            this.tR2TotalNetWeightTxb.CustomButton.Image = null;
            this.tR2TotalNetWeightTxb.CustomButton.Location = new System.Drawing.Point(49, 2);
            this.tR2TotalNetWeightTxb.CustomButton.Name = "";
            this.tR2TotalNetWeightTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.tR2TotalNetWeightTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tR2TotalNetWeightTxb.CustomButton.TabIndex = 1;
            this.tR2TotalNetWeightTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tR2TotalNetWeightTxb.CustomButton.UseSelectable = true;
            this.tR2TotalNetWeightTxb.CustomButton.Visible = false;
            this.tR2TotalNetWeightTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.tR2TotalNetWeightTxb.ForeColor = System.Drawing.Color.Gray;
            this.tR2TotalNetWeightTxb.Lines = new string[0];
            this.tR2TotalNetWeightTxb.Location = new System.Drawing.Point(39, 0);
            this.tR2TotalNetWeightTxb.MaxLength = 32767;
            this.tR2TotalNetWeightTxb.Name = "tR2TotalNetWeightTxb";
            this.tR2TotalNetWeightTxb.PasswordChar = '\0';
            this.tR2TotalNetWeightTxb.ReadOnly = true;
            this.tR2TotalNetWeightTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tR2TotalNetWeightTxb.SelectedText = "";
            this.tR2TotalNetWeightTxb.SelectionLength = 0;
            this.tR2TotalNetWeightTxb.SelectionStart = 0;
            this.tR2TotalNetWeightTxb.ShortcutsEnabled = true;
            this.tR2TotalNetWeightTxb.Size = new System.Drawing.Size(77, 30);
            this.tR2TotalNetWeightTxb.TabIndex = 99;
            this.tR2TotalNetWeightTxb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tR2TotalNetWeightTxb.UseCustomForeColor = true;
            this.tR2TotalNetWeightTxb.UseSelectable = true;
            this.tR2TotalNetWeightTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tR2TotalNetWeightTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroPanel20
            // 
            this.metroPanel20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.metroPanel20.Controls.Add(this.metroLabel16);
            this.metroPanel20.Controls.Add(this.tR2TotalGrossWeightTxb);
            this.metroPanel20.Controls.Add(this.tR2TotalGrossWeightUnitLbl);
            this.metroPanel20.HorizontalScrollbarBarColor = true;
            this.metroPanel20.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel20.HorizontalScrollbarSize = 10;
            this.metroPanel20.Location = new System.Drawing.Point(178, 25);
            this.metroPanel20.Name = "metroPanel20";
            this.metroPanel20.Size = new System.Drawing.Size(155, 38);
            this.metroPanel20.TabIndex = 96;
            this.metroPanel20.VerticalScrollbarBarColor = true;
            this.metroPanel20.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel20.VerticalScrollbarSize = 10;
            // 
            // metroLabel16
            // 
            this.metroLabel16.AutoSize = true;
            this.metroLabel16.Location = new System.Drawing.Point(2, 5);
            this.metroLabel16.Name = "metroLabel16";
            this.metroLabel16.Size = new System.Drawing.Size(41, 19);
            this.metroLabel16.TabIndex = 175;
            this.metroLabel16.Text = "Gross";
            // 
            // tR2TotalGrossWeightTxb
            // 
            // 
            // 
            // 
            this.tR2TotalGrossWeightTxb.CustomButton.Image = null;
            this.tR2TotalGrossWeightTxb.CustomButton.Location = new System.Drawing.Point(48, 2);
            this.tR2TotalGrossWeightTxb.CustomButton.Name = "";
            this.tR2TotalGrossWeightTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.tR2TotalGrossWeightTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tR2TotalGrossWeightTxb.CustomButton.TabIndex = 1;
            this.tR2TotalGrossWeightTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tR2TotalGrossWeightTxb.CustomButton.UseSelectable = true;
            this.tR2TotalGrossWeightTxb.CustomButton.Visible = false;
            this.tR2TotalGrossWeightTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.tR2TotalGrossWeightTxb.ForeColor = System.Drawing.Color.Gray;
            this.tR2TotalGrossWeightTxb.Lines = new string[0];
            this.tR2TotalGrossWeightTxb.Location = new System.Drawing.Point(45, 0);
            this.tR2TotalGrossWeightTxb.MaxLength = 32767;
            this.tR2TotalGrossWeightTxb.Name = "tR2TotalGrossWeightTxb";
            this.tR2TotalGrossWeightTxb.PasswordChar = '\0';
            this.tR2TotalGrossWeightTxb.ReadOnly = true;
            this.tR2TotalGrossWeightTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tR2TotalGrossWeightTxb.SelectedText = "";
            this.tR2TotalGrossWeightTxb.SelectionLength = 0;
            this.tR2TotalGrossWeightTxb.SelectionStart = 0;
            this.tR2TotalGrossWeightTxb.ShortcutsEnabled = true;
            this.tR2TotalGrossWeightTxb.Size = new System.Drawing.Size(76, 30);
            this.tR2TotalGrossWeightTxb.TabIndex = 96;
            this.tR2TotalGrossWeightTxb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tR2TotalGrossWeightTxb.UseCustomForeColor = true;
            this.tR2TotalGrossWeightTxb.UseSelectable = true;
            this.tR2TotalGrossWeightTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tR2TotalGrossWeightTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // tR2TotalGrossWeightUnitLbl
            // 
            this.tR2TotalGrossWeightUnitLbl.AutoSize = true;
            this.tR2TotalGrossWeightUnitLbl.Location = new System.Drawing.Point(127, 5);
            this.tR2TotalGrossWeightUnitLbl.Name = "tR2TotalGrossWeightUnitLbl";
            this.tR2TotalGrossWeightUnitLbl.Size = new System.Drawing.Size(27, 19);
            this.tR2TotalGrossWeightUnitLbl.TabIndex = 97;
            this.tR2TotalGrossWeightUnitLbl.Text = "Kg.";
            // 
            // tR2TotalWeightLbl
            // 
            this.tR2TotalWeightLbl.AutoSize = true;
            this.tR2TotalWeightLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.tR2TotalWeightLbl.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.tR2TotalWeightLbl.Location = new System.Drawing.Point(213, -1);
            this.tR2TotalWeightLbl.Name = "tR2TotalWeightLbl";
            this.tR2TotalWeightLbl.Size = new System.Drawing.Size(68, 25);
            this.tR2TotalWeightLbl.TabIndex = 95;
            this.tR2TotalWeightLbl.Text = "น.น.รวม";
            // 
            // tR2AssignDateTxb
            // 
            this.tR2AssignDateTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.tR2AssignDateTxb.CustomButton.Image = null;
            this.tR2AssignDateTxb.CustomButton.Location = new System.Drawing.Point(157, 2);
            this.tR2AssignDateTxb.CustomButton.Name = "";
            this.tR2AssignDateTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.tR2AssignDateTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tR2AssignDateTxb.CustomButton.TabIndex = 1;
            this.tR2AssignDateTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tR2AssignDateTxb.CustomButton.UseSelectable = true;
            this.tR2AssignDateTxb.CustomButton.Visible = false;
            this.tR2AssignDateTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.tR2AssignDateTxb.ForeColor = System.Drawing.Color.Gray;
            this.tR2AssignDateTxb.Lines = new string[0];
            this.tR2AssignDateTxb.Location = new System.Drawing.Point(1009, 71);
            this.tR2AssignDateTxb.MaxLength = 32767;
            this.tR2AssignDateTxb.Name = "tR2AssignDateTxb";
            this.tR2AssignDateTxb.PasswordChar = '\0';
            this.tR2AssignDateTxb.ReadOnly = true;
            this.tR2AssignDateTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tR2AssignDateTxb.SelectedText = "";
            this.tR2AssignDateTxb.SelectionLength = 0;
            this.tR2AssignDateTxb.SelectionStart = 0;
            this.tR2AssignDateTxb.ShortcutsEnabled = true;
            this.tR2AssignDateTxb.Size = new System.Drawing.Size(185, 30);
            this.tR2AssignDateTxb.TabIndex = 46;
            this.tR2AssignDateTxb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tR2AssignDateTxb.UseCustomForeColor = true;
            this.tR2AssignDateTxb.UseSelectable = true;
            this.tR2AssignDateTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tR2AssignDateTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // tR2CancelBtn
            // 
            this.tR2CancelBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.tR2CancelBtn.BackColor = System.Drawing.Color.IndianRed;
            this.tR2CancelBtn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.tR2CancelBtn.ForeColor = System.Drawing.Color.MistyRose;
            this.tR2CancelBtn.Location = new System.Drawing.Point(1090, 615);
            this.tR2CancelBtn.Name = "tR2CancelBtn";
            this.tR2CancelBtn.Size = new System.Drawing.Size(150, 40);
            this.tR2CancelBtn.TabIndex = 40;
            this.tR2CancelBtn.Text = "ออกจากหน้านี้";
            this.tR2CancelBtn.UseCustomBackColor = true;
            this.tR2CancelBtn.UseCustomForeColor = true;
            this.tR2CancelBtn.UseSelectable = true;
            this.tR2CancelBtn.Click += new System.EventHandler(this.MetroButton31_Click);
            // 
            // tR2PrintFormBtn
            // 
            this.tR2PrintFormBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.tR2PrintFormBtn.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.tR2PrintFormBtn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.tR2PrintFormBtn.ForeColor = System.Drawing.Color.FloralWhite;
            this.tR2PrintFormBtn.Location = new System.Drawing.Point(720, 615);
            this.tR2PrintFormBtn.Name = "tR2PrintFormBtn";
            this.tR2PrintFormBtn.Size = new System.Drawing.Size(150, 40);
            this.tR2PrintFormBtn.TabIndex = 37;
            this.tR2PrintFormBtn.Text = "พิมพ์แบบฟอร์ม";
            this.tR2PrintFormBtn.UseCustomBackColor = true;
            this.tR2PrintFormBtn.UseCustomForeColor = true;
            this.tR2PrintFormBtn.UseSelectable = true;
            this.tR2PrintFormBtn.Click += new System.EventHandler(this.TR2PrintFormBtn_Click);
            // 
            // tR2SubmitBtn
            // 
            this.tR2SubmitBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.tR2SubmitBtn.BackColor = System.Drawing.Color.SteelBlue;
            this.tR2SubmitBtn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.tR2SubmitBtn.ForeColor = System.Drawing.Color.LightCyan;
            this.tR2SubmitBtn.Location = new System.Drawing.Point(535, 615);
            this.tR2SubmitBtn.Name = "tR2SubmitBtn";
            this.tR2SubmitBtn.Size = new System.Drawing.Size(150, 40);
            this.tR2SubmitBtn.TabIndex = 36;
            this.tR2SubmitBtn.Text = "บันทึกข้อมูล";
            this.tR2SubmitBtn.UseCustomBackColor = true;
            this.tR2SubmitBtn.UseCustomForeColor = true;
            this.tR2SubmitBtn.UseSelectable = true;
            this.tR2SubmitBtn.Click += new System.EventHandler(this.TR2SubmitBtn_Click);
            // 
            // tR2FromWhseCbb
            // 
            this.tR2FromWhseCbb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.tR2FromWhseCbb.FormattingEnabled = true;
            this.tR2FromWhseCbb.ItemHeight = 23;
            this.tR2FromWhseCbb.Items.AddRange(new object[] {
            "SQC",
            "SRM",
            "SPD2",
            "SPD4",
            "SFG1",
            "SFG2",
            "SFG3",
            "WRM",
            "WPD",
            "WFG"});
            this.tR2FromWhseCbb.Location = new System.Drawing.Point(215, 165);
            this.tR2FromWhseCbb.Name = "tR2FromWhseCbb";
            this.tR2FromWhseCbb.Size = new System.Drawing.Size(226, 29);
            this.tR2FromWhseCbb.TabIndex = 30;
            this.tR2FromWhseCbb.UseSelectable = true;
            // 
            // tR2ToWhseCbb
            // 
            this.tR2ToWhseCbb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.tR2ToWhseCbb.FormattingEnabled = true;
            this.tR2ToWhseCbb.ItemHeight = 23;
            this.tR2ToWhseCbb.Items.AddRange(new object[] {
            "SQC",
            "SRM",
            "SPD2",
            "SPD4",
            "SFG1",
            "SFG2",
            "SFG3",
            "WRM",
            "WPD",
            "WFG"});
            this.tR2ToWhseCbb.Location = new System.Drawing.Point(215, 203);
            this.tR2ToWhseCbb.Name = "tR2ToWhseCbb";
            this.tR2ToWhseCbb.Size = new System.Drawing.Size(226, 29);
            this.tR2ToWhseCbb.TabIndex = 31;
            this.tR2ToWhseCbb.UseSelectable = true;
            // 
            // tR2DocNoLbl
            // 
            this.tR2DocNoLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.tR2DocNoLbl.AutoSize = true;
            this.tR2DocNoLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.tR2DocNoLbl.Location = new System.Drawing.Point(819, 37);
            this.tR2DocNoLbl.Name = "tR2DocNoLbl";
            this.tR2DocNoLbl.Size = new System.Drawing.Size(181, 25);
            this.tR2DocNoLbl.TabIndex = 0;
            this.tR2DocNoLbl.Text = "หมายเลขใบโอนถ่ายสินค้า";
            // 
            // tR2AssignDateLbl
            // 
            this.tR2AssignDateLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.tR2AssignDateLbl.AutoSize = true;
            this.tR2AssignDateLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.tR2AssignDateLbl.Location = new System.Drawing.Point(819, 71);
            this.tR2AssignDateLbl.Name = "tR2AssignDateLbl";
            this.tR2AssignDateLbl.Size = new System.Drawing.Size(42, 25);
            this.tR2AssignDateLbl.TabIndex = 1;
            this.tR2AssignDateLbl.Text = "วันที่";
            // 
            // tR2RefNoLbl
            // 
            this.tR2RefNoLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.tR2RefNoLbl.AutoSize = true;
            this.tR2RefNoLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.tR2RefNoLbl.Location = new System.Drawing.Point(819, 108);
            this.tR2RefNoLbl.Name = "tR2RefNoLbl";
            this.tR2RefNoLbl.Size = new System.Drawing.Size(136, 25);
            this.tR2RefNoLbl.TabIndex = 2;
            this.tR2RefNoLbl.Text = "Transfer Request";
            // 
            // tR2DocNoTxb
            // 
            this.tR2DocNoTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.tR2DocNoTxb.CustomButton.Image = null;
            this.tR2DocNoTxb.CustomButton.Location = new System.Drawing.Point(157, 2);
            this.tR2DocNoTxb.CustomButton.Name = "";
            this.tR2DocNoTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.tR2DocNoTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tR2DocNoTxb.CustomButton.TabIndex = 1;
            this.tR2DocNoTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tR2DocNoTxb.CustomButton.UseSelectable = true;
            this.tR2DocNoTxb.CustomButton.Visible = false;
            this.tR2DocNoTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.tR2DocNoTxb.ForeColor = System.Drawing.Color.Gray;
            this.tR2DocNoTxb.Lines = new string[0];
            this.tR2DocNoTxb.Location = new System.Drawing.Point(1009, 37);
            this.tR2DocNoTxb.MaxLength = 32767;
            this.tR2DocNoTxb.Name = "tR2DocNoTxb";
            this.tR2DocNoTxb.PasswordChar = '\0';
            this.tR2DocNoTxb.ReadOnly = true;
            this.tR2DocNoTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tR2DocNoTxb.SelectedText = "";
            this.tR2DocNoTxb.SelectionLength = 0;
            this.tR2DocNoTxb.SelectionStart = 0;
            this.tR2DocNoTxb.ShortcutsEnabled = true;
            this.tR2DocNoTxb.Size = new System.Drawing.Size(185, 30);
            this.tR2DocNoTxb.TabIndex = 5;
            this.tR2DocNoTxb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tR2DocNoTxb.UseCustomForeColor = true;
            this.tR2DocNoTxb.UseSelectable = true;
            this.tR2DocNoTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tR2DocNoTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // tR2FromWhseLbl
            // 
            this.tR2FromWhseLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.tR2FromWhseLbl.AutoSize = true;
            this.tR2FromWhseLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.tR2FromWhseLbl.Location = new System.Drawing.Point(118, 165);
            this.tR2FromWhseLbl.Name = "tR2FromWhseLbl";
            this.tR2FromWhseLbl.Size = new System.Drawing.Size(70, 25);
            this.tR2FromWhseLbl.TabIndex = 12;
            this.tR2FromWhseLbl.Text = "จากโกดัง";
            // 
            // tR2RemarkTxb
            // 
            this.tR2RemarkTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.tR2RemarkTxb.CustomButton.Image = null;
            this.tR2RemarkTxb.CustomButton.Location = new System.Drawing.Point(471, 2);
            this.tR2RemarkTxb.CustomButton.Name = "";
            this.tR2RemarkTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.tR2RemarkTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tR2RemarkTxb.CustomButton.TabIndex = 1;
            this.tR2RemarkTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tR2RemarkTxb.CustomButton.UseSelectable = true;
            this.tR2RemarkTxb.CustomButton.Visible = false;
            this.tR2RemarkTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.tR2RemarkTxb.ForeColor = System.Drawing.Color.Gray;
            this.tR2RemarkTxb.Lines = new string[0];
            this.tR2RemarkTxb.Location = new System.Drawing.Point(215, 240);
            this.tR2RemarkTxb.MaxLength = 32767;
            this.tR2RemarkTxb.Name = "tR2RemarkTxb";
            this.tR2RemarkTxb.PasswordChar = '\0';
            this.tR2RemarkTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tR2RemarkTxb.SelectedText = "";
            this.tR2RemarkTxb.SelectionLength = 0;
            this.tR2RemarkTxb.SelectionStart = 0;
            this.tR2RemarkTxb.ShortcutsEnabled = true;
            this.tR2RemarkTxb.Size = new System.Drawing.Size(499, 30);
            this.tR2RemarkTxb.TabIndex = 16;
            this.tR2RemarkTxb.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tR2RemarkTxb.UseSelectable = true;
            this.tR2RemarkTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tR2RemarkTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // tR2ToWhseLbl
            // 
            this.tR2ToWhseLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.tR2ToWhseLbl.AutoSize = true;
            this.tR2ToWhseLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.tR2ToWhseLbl.Location = new System.Drawing.Point(114, 203);
            this.tR2ToWhseLbl.Name = "tR2ToWhseLbl";
            this.tR2ToWhseLbl.Size = new System.Drawing.Size(74, 25);
            this.tR2ToWhseLbl.TabIndex = 13;
            this.tR2ToWhseLbl.Text = "ไปที่โกดัง";
            // 
            // tR2RemarkLbl
            // 
            this.tR2RemarkLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.tR2RemarkLbl.AutoSize = true;
            this.tR2RemarkLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.tR2RemarkLbl.Location = new System.Drawing.Point(112, 240);
            this.tR2RemarkLbl.Name = "tR2RemarkLbl";
            this.tR2RemarkLbl.Size = new System.Drawing.Size(76, 25);
            this.tR2RemarkLbl.TabIndex = 14;
            this.tR2RemarkLbl.Text = "หมายเหตุ";
            // 
            // tR1Pnl
            // 
            this.tR1Pnl.Controls.Add(this.tR1BarcodePnl);
            this.tR1Pnl.Controls.Add(this.metroPanel3);
            this.tR1Pnl.Controls.Add(this.tR1BarcdeBtn);
            this.tR1Pnl.Controls.Add(this.tR1Or2Lbl);
            this.tR1Pnl.Controls.Add(this.tR1TopLbl);
            this.tR1Pnl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tR1Pnl.HorizontalScrollbarBarColor = true;
            this.tR1Pnl.HorizontalScrollbarHighlightOnWheel = false;
            this.tR1Pnl.HorizontalScrollbarSize = 10;
            this.tR1Pnl.Location = new System.Drawing.Point(0, 0);
            this.tR1Pnl.Name = "tR1Pnl";
            this.tR1Pnl.Size = new System.Drawing.Size(1238, 663);
            this.tR1Pnl.TabIndex = 36;
            this.tR1Pnl.VerticalScrollbarBarColor = true;
            this.tR1Pnl.VerticalScrollbarHighlightOnWheel = false;
            this.tR1Pnl.VerticalScrollbarSize = 10;
            this.tR1Pnl.MouseHover += new System.EventHandler(this.TR1Pnl_MouseHover);
            // 
            // tR1BarcodePnl
            // 
            this.tR1BarcodePnl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.tR1BarcodePnl.BackColor = System.Drawing.Color.Transparent;
            this.tR1BarcodePnl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tR1BarcodePnl.Controls.Add(this.metroLabel2);
            this.tR1BarcodePnl.HorizontalScrollbarBarColor = true;
            this.tR1BarcodePnl.HorizontalScrollbarHighlightOnWheel = false;
            this.tR1BarcodePnl.HorizontalScrollbarSize = 10;
            this.tR1BarcodePnl.Location = new System.Drawing.Point(494, 302);
            this.tR1BarcodePnl.Name = "tR1BarcodePnl";
            this.tR1BarcodePnl.Size = new System.Drawing.Size(250, 85);
            this.tR1BarcodePnl.TabIndex = 45;
            this.tR1BarcodePnl.UseCustomBackColor = true;
            this.tR1BarcodePnl.VerticalScrollbarBarColor = true;
            this.tR1BarcodePnl.VerticalScrollbarHighlightOnWheel = false;
            this.tR1BarcodePnl.VerticalScrollbarSize = 10;
            this.tR1BarcodePnl.MouseLeave += new System.EventHandler(this.tR1BarcodePnl_MouseLeave);
            this.tR1BarcodePnl.MouseHover += new System.EventHandler(this.tR1BarcodePnl_MouseHover);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(76, 4);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(90, 19);
            this.metroLabel2.TabIndex = 2;
            this.metroLabel2.Text = "Barcode Area";
            this.metroLabel2.UseCustomBackColor = true;
            // 
            // metroPanel3
            // 
            this.metroPanel3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.metroPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.metroPanel3.Controls.Add(this.tR1CreateTransferBtn);
            this.metroPanel3.Controls.Add(this.tR1TransferNoTxb);
            this.metroPanel3.Controls.Add(this.tR1TransferNoLbl);
            this.metroPanel3.Controls.Add(this.tR1Or1Lbl);
            this.metroPanel3.Controls.Add(this.tR1SubmitBtn);
            this.metroPanel3.HorizontalScrollbarBarColor = true;
            this.metroPanel3.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel3.HorizontalScrollbarSize = 10;
            this.metroPanel3.Location = new System.Drawing.Point(274, 114);
            this.metroPanel3.Name = "metroPanel3";
            this.metroPanel3.Size = new System.Drawing.Size(691, 166);
            this.metroPanel3.TabIndex = 44;
            this.metroPanel3.VerticalScrollbarBarColor = true;
            this.metroPanel3.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel3.VerticalScrollbarSize = 10;
            // 
            // tR1CreateTransferBtn
            // 
            this.tR1CreateTransferBtn.BackColor = System.Drawing.Color.SeaGreen;
            this.tR1CreateTransferBtn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.tR1CreateTransferBtn.ForeColor = System.Drawing.Color.Honeydew;
            this.tR1CreateTransferBtn.Location = new System.Drawing.Point(237, 21);
            this.tR1CreateTransferBtn.Name = "tR1CreateTransferBtn";
            this.tR1CreateTransferBtn.Size = new System.Drawing.Size(214, 36);
            this.tR1CreateTransferBtn.TabIndex = 34;
            this.tR1CreateTransferBtn.Text = "สร้างใบโอนถ่ายสินค้า";
            this.tR1CreateTransferBtn.UseCustomBackColor = true;
            this.tR1CreateTransferBtn.UseCustomForeColor = true;
            this.tR1CreateTransferBtn.UseSelectable = true;
            this.tR1CreateTransferBtn.Click += new System.EventHandler(this.TR1CreateTransferBtn_Click);
            // 
            // tR1TransferNoTxb
            // 
            // 
            // 
            // 
            this.tR1TransferNoTxb.CustomButton.Image = null;
            this.tR1TransferNoTxb.CustomButton.Location = new System.Drawing.Point(223, 2);
            this.tR1TransferNoTxb.CustomButton.Name = "";
            this.tR1TransferNoTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.tR1TransferNoTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tR1TransferNoTxb.CustomButton.TabIndex = 1;
            this.tR1TransferNoTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tR1TransferNoTxb.CustomButton.UseSelectable = true;
            this.tR1TransferNoTxb.CustomButton.Visible = false;
            this.tR1TransferNoTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.tR1TransferNoTxb.Lines = new string[0];
            this.tR1TransferNoTxb.Location = new System.Drawing.Point(219, 117);
            this.tR1TransferNoTxb.MaxLength = 32767;
            this.tR1TransferNoTxb.Name = "tR1TransferNoTxb";
            this.tR1TransferNoTxb.PasswordChar = '\0';
            this.tR1TransferNoTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tR1TransferNoTxb.SelectedText = "";
            this.tR1TransferNoTxb.SelectionLength = 0;
            this.tR1TransferNoTxb.SelectionStart = 0;
            this.tR1TransferNoTxb.ShortcutsEnabled = true;
            this.tR1TransferNoTxb.Size = new System.Drawing.Size(251, 30);
            this.tR1TransferNoTxb.TabIndex = 7;
            this.tR1TransferNoTxb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tR1TransferNoTxb.UseSelectable = true;
            this.tR1TransferNoTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tR1TransferNoTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.tR1TransferNoTxb.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tR1TransferNoTxb_KeyDown);
            this.tR1TransferNoTxb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tR1TransferNoTxb_KeyPress);
            // 
            // tR1TransferNoLbl
            // 
            this.tR1TransferNoLbl.AutoSize = true;
            this.tR1TransferNoLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.tR1TransferNoLbl.Location = new System.Drawing.Point(18, 117);
            this.tR1TransferNoLbl.Name = "tR1TransferNoLbl";
            this.tR1TransferNoLbl.Size = new System.Drawing.Size(181, 25);
            this.tR1TransferNoLbl.TabIndex = 6;
            this.tR1TransferNoLbl.Text = "หมายเลขใบโอนถ่ายสินค้า";
            // 
            // tR1Or1Lbl
            // 
            this.tR1Or1Lbl.AutoSize = true;
            this.tR1Or1Lbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.tR1Or1Lbl.Location = new System.Drawing.Point(324, 76);
            this.tR1Or1Lbl.Name = "tR1Or1Lbl";
            this.tR1Or1Lbl.Size = new System.Drawing.Size(41, 25);
            this.tR1Or1Lbl.TabIndex = 8;
            this.tR1Or1Lbl.Text = "หรือ";
            // 
            // tR1SubmitBtn
            // 
            this.tR1SubmitBtn.BackColor = System.Drawing.Color.SteelBlue;
            this.tR1SubmitBtn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.tR1SubmitBtn.ForeColor = System.Drawing.Color.LightCyan;
            this.tR1SubmitBtn.Location = new System.Drawing.Point(512, 112);
            this.tR1SubmitBtn.Name = "tR1SubmitBtn";
            this.tR1SubmitBtn.Size = new System.Drawing.Size(96, 40);
            this.tR1SubmitBtn.TabIndex = 35;
            this.tR1SubmitBtn.Text = "ยืนยัน";
            this.tR1SubmitBtn.UseCustomBackColor = true;
            this.tR1SubmitBtn.UseCustomForeColor = true;
            this.tR1SubmitBtn.UseSelectable = true;
            this.tR1SubmitBtn.Click += new System.EventHandler(this.TR1SubmitBtn_Click);
            // 
            // tR1BarcdeBtn
            // 
            this.tR1BarcdeBtn.Location = new System.Drawing.Point(512, 642);
            this.tR1BarcdeBtn.Name = "tR1BarcdeBtn";
            this.tR1BarcdeBtn.Size = new System.Drawing.Size(214, 30);
            this.tR1BarcdeBtn.TabIndex = 41;
            this.tR1BarcdeBtn.Text = "ยิงบาร์โค้ดหมายเลขใบโอนถ่ายสินค้า";
            this.tR1BarcdeBtn.UseSelectable = true;
            this.tR1BarcdeBtn.Visible = false;
            // 
            // tR1Or2Lbl
            // 
            this.tR1Or2Lbl.AutoSize = true;
            this.tR1Or2Lbl.Location = new System.Drawing.Point(446, 238);
            this.tR1Or2Lbl.Name = "tR1Or2Lbl";
            this.tR1Or2Lbl.Size = new System.Drawing.Size(30, 19);
            this.tR1Or2Lbl.TabIndex = 39;
            this.tR1Or2Lbl.Text = "หรือ";
            this.tR1Or2Lbl.Visible = false;
            // 
            // tR1TopLbl
            // 
            this.tR1TopLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.tR1TopLbl.AutoSize = true;
            this.tR1TopLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.tR1TopLbl.Location = new System.Drawing.Point(274, 71);
            this.tR1TopLbl.Name = "tR1TopLbl";
            this.tR1TopLbl.Size = new System.Drawing.Size(691, 25);
            this.tR1TopLbl.TabIndex = 36;
            this.tR1TopLbl.Text = "กรุณากดปุ่มสร้างใบโอนถ่ายสินค้า หรือระบุ/ยิงหมายเลขใบโอนถ่ายสินค้า  เพื่อเข้าสู่ห" +
    "น้าใบโอนถ่ายสินค้า";
            // 
            // reportTabPage
            // 
            this.reportTabPage.Controls.Add(this.metroPanel14);
            this.reportTabPage.HorizontalScrollbarBarColor = true;
            this.reportTabPage.HorizontalScrollbarHighlightOnWheel = false;
            this.reportTabPage.HorizontalScrollbarSize = 0;
            this.reportTabPage.Location = new System.Drawing.Point(4, 47);
            this.reportTabPage.Name = "reportTabPage";
            this.reportTabPage.Size = new System.Drawing.Size(1238, 663);
            this.reportTabPage.TabIndex = 9;
            this.reportTabPage.Text = "รายงาน";
            this.reportTabPage.VerticalScrollbarBarColor = true;
            this.reportTabPage.VerticalScrollbarHighlightOnWheel = false;
            this.reportTabPage.VerticalScrollbarSize = 0;
            // 
            // metroPanel14
            // 
            this.metroPanel14.Controls.Add(this.whReportPnl);
            this.metroPanel14.Controls.Add(this.label2);
            this.metroPanel14.Controls.Add(this.genReportBtn);
            this.metroPanel14.Controls.Add(this.endDatePicker);
            this.metroPanel14.Controls.Add(this.metroLabel92);
            this.metroPanel14.Controls.Add(this.startDatePicker);
            this.metroPanel14.Controls.Add(this.metroLabel91);
            this.metroPanel14.Controls.Add(this.reportNameCbb);
            this.metroPanel14.Controls.Add(this.metroLabel86);
            this.metroPanel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroPanel14.HorizontalScrollbarBarColor = true;
            this.metroPanel14.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel14.HorizontalScrollbarSize = 0;
            this.metroPanel14.Location = new System.Drawing.Point(0, 0);
            this.metroPanel14.Name = "metroPanel14";
            this.metroPanel14.Size = new System.Drawing.Size(1238, 663);
            this.metroPanel14.TabIndex = 144;
            this.metroPanel14.UseCustomForeColor = true;
            this.metroPanel14.VerticalScrollbarBarColor = true;
            this.metroPanel14.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel14.VerticalScrollbarSize = 0;
            // 
            // whReportPnl
            // 
            this.whReportPnl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.whReportPnl.BackColor = System.Drawing.Color.White;
            this.whReportPnl.Controls.Add(this.toWhCbb);
            this.whReportPnl.Controls.Add(this.fromWhCbb);
            this.whReportPnl.Controls.Add(this.metroLabel19);
            this.whReportPnl.Controls.Add(this.metroLabel18);
            this.whReportPnl.Location = new System.Drawing.Point(193, 207);
            this.whReportPnl.Name = "whReportPnl";
            this.whReportPnl.Size = new System.Drawing.Size(776, 44);
            this.whReportPnl.TabIndex = 107;
            this.whReportPnl.Visible = false;
            // 
            // toWhCbb
            // 
            this.toWhCbb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.toWhCbb.FormattingEnabled = true;
            this.toWhCbb.ItemHeight = 23;
            this.toWhCbb.Items.AddRange(new object[] {
            "SQC",
            "SRM",
            "SPD2",
            "SPD4",
            "SFG"});
            this.toWhCbb.Location = new System.Drawing.Point(490, 6);
            this.toWhCbb.Name = "toWhCbb";
            this.toWhCbb.Size = new System.Drawing.Size(110, 29);
            this.toWhCbb.TabIndex = 51;
            this.toWhCbb.UseSelectable = true;
            // 
            // fromWhCbb
            // 
            this.fromWhCbb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.fromWhCbb.FormattingEnabled = true;
            this.fromWhCbb.ItemHeight = 23;
            this.fromWhCbb.Items.AddRange(new object[] {
            "SQC",
            "SRM",
            "SPD2",
            "SPD4",
            "SFG"});
            this.fromWhCbb.Location = new System.Drawing.Point(183, 6);
            this.fromWhCbb.Name = "fromWhCbb";
            this.fromWhCbb.Size = new System.Drawing.Size(110, 29);
            this.fromWhCbb.TabIndex = 50;
            this.fromWhCbb.UseSelectable = true;
            // 
            // metroLabel19
            // 
            this.metroLabel19.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.metroLabel19.AutoSize = true;
            this.metroLabel19.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel19.Location = new System.Drawing.Point(405, 6);
            this.metroLabel19.Name = "metroLabel19";
            this.metroLabel19.Size = new System.Drawing.Size(63, 25);
            this.metroLabel19.TabIndex = 49;
            this.metroLabel19.Text = "ไปโกดัง";
            // 
            // metroLabel18
            // 
            this.metroLabel18.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.metroLabel18.AutoSize = true;
            this.metroLabel18.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel18.Location = new System.Drawing.Point(68, 6);
            this.metroLabel18.Name = "metroLabel18";
            this.metroLabel18.Size = new System.Drawing.Size(70, 25);
            this.metroLabel18.TabIndex = 48;
            this.metroLabel18.Text = "จากโกดัง";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Font = new System.Drawing.Font("Microsoft Tai Le", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(546, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(134, 34);
            this.label2.TabIndex = 106;
            this.label2.Text = "ออกรายงาน";
            // 
            // genReportBtn
            // 
            this.genReportBtn.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.genReportBtn.BackColor = System.Drawing.Color.SteelBlue;
            this.genReportBtn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.genReportBtn.ForeColor = System.Drawing.Color.LightCyan;
            this.genReportBtn.Location = new System.Drawing.Point(739, 257);
            this.genReportBtn.Name = "genReportBtn";
            this.genReportBtn.Size = new System.Drawing.Size(144, 36);
            this.genReportBtn.TabIndex = 53;
            this.genReportBtn.Text = "ดูรายงาน";
            this.genReportBtn.UseCustomBackColor = true;
            this.genReportBtn.UseCustomForeColor = true;
            this.genReportBtn.UseSelectable = true;
            this.genReportBtn.Click += new System.EventHandler(this.GenReportBtn_Click);
            // 
            // endDatePicker
            // 
            this.endDatePicker.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.endDatePicker.Location = new System.Drawing.Point(683, 165);
            this.endDatePicker.MinimumSize = new System.Drawing.Size(4, 29);
            this.endDatePicker.Name = "endDatePicker";
            this.endDatePicker.Size = new System.Drawing.Size(200, 29);
            this.endDatePicker.TabIndex = 52;
            // 
            // metroLabel92
            // 
            this.metroLabel92.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.metroLabel92.AutoSize = true;
            this.metroLabel92.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel92.Location = new System.Drawing.Point(632, 165);
            this.metroLabel92.Name = "metroLabel92";
            this.metroLabel92.Size = new System.Drawing.Size(29, 25);
            this.metroLabel92.TabIndex = 51;
            this.metroLabel92.Text = "ถึง";
            // 
            // startDatePicker
            // 
            this.startDatePicker.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.startDatePicker.Location = new System.Drawing.Point(376, 165);
            this.startDatePicker.MinimumSize = new System.Drawing.Size(4, 29);
            this.startDatePicker.Name = "startDatePicker";
            this.startDatePicker.Size = new System.Drawing.Size(226, 29);
            this.startDatePicker.TabIndex = 50;
            // 
            // metroLabel91
            // 
            this.metroLabel91.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.metroLabel91.AutoSize = true;
            this.metroLabel91.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel91.Location = new System.Drawing.Point(289, 165);
            this.metroLabel91.Name = "metroLabel91";
            this.metroLabel91.Size = new System.Drawing.Size(42, 25);
            this.metroLabel91.TabIndex = 49;
            this.metroLabel91.Text = "วันที่";
            // 
            // reportNameCbb
            // 
            this.reportNameCbb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.reportNameCbb.FormattingEnabled = true;
            this.reportNameCbb.ItemHeight = 23;
            this.reportNameCbb.Items.AddRange(new object[] {
            "รายงานการโอน Lot วัตถุดิบ",
            "รายงานการเบิกวัตถุดิบ",
            "รายงานการรับสินค้าผลิตเสร็จ",
            "รายงานการโอนถ่ายสินค้า",
            "รายงานการจัดส่งสินค้า",
            "รายงานจำนวนสินค้าคงคลัง"});
            this.reportNameCbb.Location = new System.Drawing.Point(376, 111);
            this.reportNameCbb.Name = "reportNameCbb";
            this.reportNameCbb.Size = new System.Drawing.Size(507, 29);
            this.reportNameCbb.TabIndex = 48;
            this.reportNameCbb.UseSelectable = true;
            this.reportNameCbb.SelectedIndexChanged += new System.EventHandler(this.reportNameCbb_SelectedIndexChanged);
            // 
            // metroLabel86
            // 
            this.metroLabel86.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.metroLabel86.AutoSize = true;
            this.metroLabel86.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel86.Location = new System.Drawing.Point(268, 111);
            this.metroLabel86.Name = "metroLabel86";
            this.metroLabel86.Size = new System.Drawing.Size(63, 25);
            this.metroLabel86.TabIndex = 47;
            this.metroLabel86.Text = "รายงาน";
            // 
            // adminUserTabPage
            // 
            this.adminUserTabPage.Controls.Add(this.aU1Pnl);
            this.adminUserTabPage.HorizontalScrollbarBarColor = true;
            this.adminUserTabPage.HorizontalScrollbarHighlightOnWheel = false;
            this.adminUserTabPage.HorizontalScrollbarSize = 0;
            this.adminUserTabPage.Location = new System.Drawing.Point(4, 47);
            this.adminUserTabPage.Name = "adminUserTabPage";
            this.adminUserTabPage.Size = new System.Drawing.Size(1238, 663);
            this.adminUserTabPage.TabIndex = 5;
            this.adminUserTabPage.Text = "ผู้ใช้งานระบบ";
            this.adminUserTabPage.VerticalScrollbarBarColor = true;
            this.adminUserTabPage.VerticalScrollbarHighlightOnWheel = false;
            this.adminUserTabPage.VerticalScrollbarSize = 0;
            this.adminUserTabPage.Enter += new System.EventHandler(this.AdminUserTabPage_Enter);
            // 
            // aU1Pnl
            // 
            this.aU1Pnl.Controls.Add(this.aU1UserListDgv);
            this.aU1Pnl.Controls.Add(this.aU1NewUserNameTxb);
            this.aU1Pnl.Controls.Add(this.aU1NewUserNameLbl);
            this.aU1Pnl.Controls.Add(this.aU1NewPasswordLbl);
            this.aU1Pnl.Controls.Add(this.aU1TopLbl);
            this.aU1Pnl.Controls.Add(this.aU1NewPasswordTxb);
            this.aU1Pnl.Controls.Add(this.aU1SubmitNewUserBtn);
            this.aU1Pnl.Controls.Add(this.aU1NewRoleCbb);
            this.aU1Pnl.Controls.Add(this.aU1NewRoleLbl);
            this.aU1Pnl.Controls.Add(this.aU1UserListLbl);
            this.aU1Pnl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.aU1Pnl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.aU1Pnl.HorizontalScrollbarBarColor = true;
            this.aU1Pnl.HorizontalScrollbarHighlightOnWheel = false;
            this.aU1Pnl.HorizontalScrollbarSize = 10;
            this.aU1Pnl.Location = new System.Drawing.Point(0, 0);
            this.aU1Pnl.Name = "aU1Pnl";
            this.aU1Pnl.Size = new System.Drawing.Size(1238, 663);
            this.aU1Pnl.TabIndex = 144;
            this.aU1Pnl.VerticalScrollbarBarColor = true;
            this.aU1Pnl.VerticalScrollbarHighlightOnWheel = false;
            this.aU1Pnl.VerticalScrollbarSize = 10;
            // 
            // aU1UserListDgv
            // 
            this.aU1UserListDgv.AllowUserToAddRows = false;
            this.aU1UserListDgv.AllowUserToDeleteRows = false;
            this.aU1UserListDgv.AllowUserToResizeColumns = false;
            this.aU1UserListDgv.AllowUserToResizeRows = false;
            this.aU1UserListDgv.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.aU1UserListDgv.BackgroundColor = System.Drawing.SystemColors.Window;
            this.aU1UserListDgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.aU1UserListDgv.Location = new System.Drawing.Point(6, 258);
            this.aU1UserListDgv.Name = "aU1UserListDgv";
            this.aU1UserListDgv.RowHeadersVisible = false;
            this.aU1UserListDgv.RowHeadersWidth = 62;
            this.aU1UserListDgv.Size = new System.Drawing.Size(1226, 396);
            this.aU1UserListDgv.TabIndex = 144;
            this.aU1UserListDgv.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.AU1UserListDgv_CellClick);
            this.aU1UserListDgv.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.AU1UserListDgv_CellEndEdit);
            this.aU1UserListDgv.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.aU1UserListDgv_DataError);
            this.aU1UserListDgv.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.aU1UserListDgv_RowPrePaint);
            // 
            // aU1NewUserNameTxb
            // 
            this.aU1NewUserNameTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.aU1NewUserNameTxb.CustomButton.Image = null;
            this.aU1NewUserNameTxb.CustomButton.Location = new System.Drawing.Point(187, 2);
            this.aU1NewUserNameTxb.CustomButton.Name = "";
            this.aU1NewUserNameTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.aU1NewUserNameTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.aU1NewUserNameTxb.CustomButton.TabIndex = 1;
            this.aU1NewUserNameTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.aU1NewUserNameTxb.CustomButton.UseSelectable = true;
            this.aU1NewUserNameTxb.CustomButton.Visible = false;
            this.aU1NewUserNameTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.aU1NewUserNameTxb.ForeColor = System.Drawing.Color.Gray;
            this.aU1NewUserNameTxb.Lines = new string[0];
            this.aU1NewUserNameTxb.Location = new System.Drawing.Point(518, 61);
            this.aU1NewUserNameTxb.MaxLength = 32767;
            this.aU1NewUserNameTxb.Name = "aU1NewUserNameTxb";
            this.aU1NewUserNameTxb.PasswordChar = '\0';
            this.aU1NewUserNameTxb.PromptText = "Username";
            this.aU1NewUserNameTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.aU1NewUserNameTxb.SelectedText = "";
            this.aU1NewUserNameTxb.SelectionLength = 0;
            this.aU1NewUserNameTxb.SelectionStart = 0;
            this.aU1NewUserNameTxb.ShortcutsEnabled = true;
            this.aU1NewUserNameTxb.Size = new System.Drawing.Size(215, 30);
            this.aU1NewUserNameTxb.TabIndex = 123;
            this.aU1NewUserNameTxb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.aU1NewUserNameTxb.UseCustomForeColor = true;
            this.aU1NewUserNameTxb.UseSelectable = true;
            this.aU1NewUserNameTxb.WaterMark = "Username";
            this.aU1NewUserNameTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.aU1NewUserNameTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // aU1NewUserNameLbl
            // 
            this.aU1NewUserNameLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.aU1NewUserNameLbl.AutoSize = true;
            this.aU1NewUserNameLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.aU1NewUserNameLbl.Location = new System.Drawing.Point(418, 61);
            this.aU1NewUserNameLbl.Name = "aU1NewUserNameLbl";
            this.aU1NewUserNameLbl.Size = new System.Drawing.Size(88, 25);
            this.aU1NewUserNameLbl.TabIndex = 121;
            this.aU1NewUserNameLbl.Text = "ชื่อผู้ใช้งาน";
            // 
            // aU1NewPasswordLbl
            // 
            this.aU1NewPasswordLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.aU1NewPasswordLbl.AutoSize = true;
            this.aU1NewPasswordLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.aU1NewPasswordLbl.Location = new System.Drawing.Point(418, 96);
            this.aU1NewPasswordLbl.Name = "aU1NewPasswordLbl";
            this.aU1NewPasswordLbl.Size = new System.Drawing.Size(71, 25);
            this.aU1NewPasswordLbl.TabIndex = 122;
            this.aU1NewPasswordLbl.Text = "รหัสผ่าน";
            // 
            // aU1TopLbl
            // 
            this.aU1TopLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.aU1TopLbl.AutoSize = true;
            this.aU1TopLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.aU1TopLbl.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.aU1TopLbl.Location = new System.Drawing.Point(553, 13);
            this.aU1TopLbl.Name = "aU1TopLbl";
            this.aU1TopLbl.Size = new System.Drawing.Size(140, 25);
            this.aU1TopLbl.TabIndex = 124;
            this.aU1TopLbl.Text = "เพิ่มผู้ใช้งานใหม่";
            // 
            // aU1NewPasswordTxb
            // 
            this.aU1NewPasswordTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.aU1NewPasswordTxb.CustomButton.Image = null;
            this.aU1NewPasswordTxb.CustomButton.Location = new System.Drawing.Point(187, 2);
            this.aU1NewPasswordTxb.CustomButton.Name = "";
            this.aU1NewPasswordTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.aU1NewPasswordTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.aU1NewPasswordTxb.CustomButton.TabIndex = 1;
            this.aU1NewPasswordTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.aU1NewPasswordTxb.CustomButton.UseSelectable = true;
            this.aU1NewPasswordTxb.CustomButton.Visible = false;
            this.aU1NewPasswordTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.aU1NewPasswordTxb.ForeColor = System.Drawing.Color.Gray;
            this.aU1NewPasswordTxb.Lines = new string[0];
            this.aU1NewPasswordTxb.Location = new System.Drawing.Point(518, 96);
            this.aU1NewPasswordTxb.MaxLength = 32767;
            this.aU1NewPasswordTxb.Name = "aU1NewPasswordTxb";
            this.aU1NewPasswordTxb.PasswordChar = '\0';
            this.aU1NewPasswordTxb.PromptText = "Password";
            this.aU1NewPasswordTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.aU1NewPasswordTxb.SelectedText = "";
            this.aU1NewPasswordTxb.SelectionLength = 0;
            this.aU1NewPasswordTxb.SelectionStart = 0;
            this.aU1NewPasswordTxb.ShortcutsEnabled = true;
            this.aU1NewPasswordTxb.Size = new System.Drawing.Size(215, 30);
            this.aU1NewPasswordTxb.TabIndex = 125;
            this.aU1NewPasswordTxb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.aU1NewPasswordTxb.UseCustomForeColor = true;
            this.aU1NewPasswordTxb.UseSelectable = true;
            this.aU1NewPasswordTxb.WaterMark = "Password";
            this.aU1NewPasswordTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.aU1NewPasswordTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // aU1SubmitNewUserBtn
            // 
            this.aU1SubmitNewUserBtn.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.aU1SubmitNewUserBtn.BackColor = System.Drawing.Color.SteelBlue;
            this.aU1SubmitNewUserBtn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.aU1SubmitNewUserBtn.ForeColor = System.Drawing.Color.LightCyan;
            this.aU1SubmitNewUserBtn.Location = new System.Drawing.Point(518, 166);
            this.aU1SubmitNewUserBtn.Name = "aU1SubmitNewUserBtn";
            this.aU1SubmitNewUserBtn.Size = new System.Drawing.Size(215, 36);
            this.aU1SubmitNewUserBtn.TabIndex = 126;
            this.aU1SubmitNewUserBtn.Text = "เพิ่มเข้าระบบ";
            this.aU1SubmitNewUserBtn.UseCustomBackColor = true;
            this.aU1SubmitNewUserBtn.UseCustomForeColor = true;
            this.aU1SubmitNewUserBtn.UseSelectable = true;
            this.aU1SubmitNewUserBtn.Click += new System.EventHandler(this.AU1SubmitNewUserBtn_Click);
            // 
            // aU1NewRoleCbb
            // 
            this.aU1NewRoleCbb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.aU1NewRoleCbb.FormattingEnabled = true;
            this.aU1NewRoleCbb.ItemHeight = 23;
            this.aU1NewRoleCbb.Items.AddRange(new object[] {
            "SQC",
            "SRM",
            "SPD2",
            "SPD4",
            "SFG"});
            this.aU1NewRoleCbb.Location = new System.Drawing.Point(518, 131);
            this.aU1NewRoleCbb.Name = "aU1NewRoleCbb";
            this.aU1NewRoleCbb.Size = new System.Drawing.Size(215, 29);
            this.aU1NewRoleCbb.TabIndex = 127;
            this.aU1NewRoleCbb.UseSelectable = true;
            // 
            // aU1NewRoleLbl
            // 
            this.aU1NewRoleLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.aU1NewRoleLbl.AutoSize = true;
            this.aU1NewRoleLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.aU1NewRoleLbl.Location = new System.Drawing.Point(418, 131);
            this.aU1NewRoleLbl.Name = "aU1NewRoleLbl";
            this.aU1NewRoleLbl.Size = new System.Drawing.Size(53, 25);
            this.aU1NewRoleLbl.TabIndex = 128;
            this.aU1NewRoleLbl.Text = "แผนก";
            // 
            // aU1UserListLbl
            // 
            this.aU1UserListLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.aU1UserListLbl.AutoSize = true;
            this.aU1UserListLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.aU1UserListLbl.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.aU1UserListLbl.Location = new System.Drawing.Point(528, 225);
            this.aU1UserListLbl.Name = "aU1UserListLbl";
            this.aU1UserListLbl.Size = new System.Drawing.Size(190, 25);
            this.aU1UserListLbl.TabIndex = 129;
            this.aU1UserListLbl.Text = "รายชื่อผู้ใช้งานในระบบ";
            // 
            // lotSetupTabPage
            // 
            this.lotSetupTabPage.Controls.Add(this.aLPnl);
            this.lotSetupTabPage.HorizontalScrollbarBarColor = true;
            this.lotSetupTabPage.HorizontalScrollbarHighlightOnWheel = false;
            this.lotSetupTabPage.HorizontalScrollbarSize = 0;
            this.lotSetupTabPage.Location = new System.Drawing.Point(4, 47);
            this.lotSetupTabPage.Name = "lotSetupTabPage";
            this.lotSetupTabPage.Size = new System.Drawing.Size(1238, 663);
            this.lotSetupTabPage.TabIndex = 6;
            this.lotSetupTabPage.Text = "ตั้งค่าเลข Lot";
            this.lotSetupTabPage.VerticalScrollbarBarColor = true;
            this.lotSetupTabPage.VerticalScrollbarHighlightOnWheel = false;
            this.lotSetupTabPage.VerticalScrollbarSize = 0;
            this.lotSetupTabPage.Enter += new System.EventHandler(this.LotSetupTabPage_Enter);
            // 
            // aLPnl
            // 
            this.aLPnl.Controls.Add(this.aL1ClearBtn);
            this.aLPnl.Controls.Add(this.aL1TopLbl);
            this.aLPnl.Controls.Add(this.aL1SubmitBtn);
            this.aLPnl.Controls.Add(this.aL1LotFormatTxb);
            this.aLPnl.Controls.Add(this.aL1LotTypeLbl);
            this.aLPnl.Controls.Add(this.aL1OptionsLsb);
            this.aLPnl.Controls.Add(this.aL1LotTypeCbb);
            this.aLPnl.Controls.Add(this.aL1SelectBtn);
            this.aLPnl.Controls.Add(this.aL1ExampleTxb);
            this.aLPnl.Controls.Add(this.aL1OptionsLbl);
            this.aLPnl.Controls.Add(this.aL1ExampleLbl);
            this.aLPnl.Controls.Add(this.aL1LotFormatLbl);
            this.aLPnl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.aLPnl.HorizontalScrollbarBarColor = true;
            this.aLPnl.HorizontalScrollbarHighlightOnWheel = false;
            this.aLPnl.HorizontalScrollbarSize = 10;
            this.aLPnl.Location = new System.Drawing.Point(0, 0);
            this.aLPnl.Name = "aLPnl";
            this.aLPnl.Size = new System.Drawing.Size(1238, 663);
            this.aLPnl.TabIndex = 122;
            this.aLPnl.VerticalScrollbarBarColor = true;
            this.aLPnl.VerticalScrollbarHighlightOnWheel = false;
            this.aLPnl.VerticalScrollbarSize = 10;
            // 
            // aL1ClearBtn
            // 
            this.aL1ClearBtn.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.aL1ClearBtn.BackColor = System.Drawing.Color.IndianRed;
            this.aL1ClearBtn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.aL1ClearBtn.ForeColor = System.Drawing.Color.MistyRose;
            this.aL1ClearBtn.Location = new System.Drawing.Point(454, 326);
            this.aL1ClearBtn.Name = "aL1ClearBtn";
            this.aL1ClearBtn.Size = new System.Drawing.Size(140, 36);
            this.aL1ClearBtn.TabIndex = 122;
            this.aL1ClearBtn.Text = "เคลียร์ค่าที่ตั้ง";
            this.aL1ClearBtn.UseCustomBackColor = true;
            this.aL1ClearBtn.UseCustomForeColor = true;
            this.aL1ClearBtn.UseSelectable = true;
            this.aL1ClearBtn.Click += new System.EventHandler(this.AL1ClearBtn_Click);
            // 
            // aL1TopLbl
            // 
            this.aL1TopLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.aL1TopLbl.AutoSize = true;
            this.aL1TopLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.aL1TopLbl.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.aL1TopLbl.Location = new System.Drawing.Point(512, 33);
            this.aL1TopLbl.Name = "aL1TopLbl";
            this.aL1TopLbl.Size = new System.Drawing.Size(227, 25);
            this.aL1TopLbl.TabIndex = 115;
            this.aL1TopLbl.Text = "ตั้งค่าการสร้างหมายเลข Lot";
            // 
            // aL1SubmitBtn
            // 
            this.aL1SubmitBtn.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.aL1SubmitBtn.BackColor = System.Drawing.Color.SteelBlue;
            this.aL1SubmitBtn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.aL1SubmitBtn.ForeColor = System.Drawing.Color.LightCyan;
            this.aL1SubmitBtn.Location = new System.Drawing.Point(607, 326);
            this.aL1SubmitBtn.Name = "aL1SubmitBtn";
            this.aL1SubmitBtn.Size = new System.Drawing.Size(172, 36);
            this.aL1SubmitBtn.TabIndex = 121;
            this.aL1SubmitBtn.Text = "ยืนยันการเปลี่ยนแปลง";
            this.aL1SubmitBtn.UseCustomBackColor = true;
            this.aL1SubmitBtn.UseCustomForeColor = true;
            this.aL1SubmitBtn.UseSelectable = true;
            this.aL1SubmitBtn.Click += new System.EventHandler(this.AL1SubmitBtn_Click);
            // 
            // aL1LotFormatTxb
            // 
            this.aL1LotFormatTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.aL1LotFormatTxb.CustomButton.Image = null;
            this.aL1LotFormatTxb.CustomButton.Location = new System.Drawing.Point(257, 2);
            this.aL1LotFormatTxb.CustomButton.Name = "";
            this.aL1LotFormatTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.aL1LotFormatTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.aL1LotFormatTxb.CustomButton.TabIndex = 1;
            this.aL1LotFormatTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.aL1LotFormatTxb.CustomButton.UseSelectable = true;
            this.aL1LotFormatTxb.CustomButton.Visible = false;
            this.aL1LotFormatTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.aL1LotFormatTxb.Lines = new string[0];
            this.aL1LotFormatTxb.Location = new System.Drawing.Point(454, 277);
            this.aL1LotFormatTxb.MaxLength = 32767;
            this.aL1LotFormatTxb.Multiline = true;
            this.aL1LotFormatTxb.Name = "aL1LotFormatTxb";
            this.aL1LotFormatTxb.PasswordChar = '\0';
            this.aL1LotFormatTxb.ReadOnly = true;
            this.aL1LotFormatTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.aL1LotFormatTxb.SelectedText = "";
            this.aL1LotFormatTxb.SelectionLength = 0;
            this.aL1LotFormatTxb.SelectionStart = 0;
            this.aL1LotFormatTxb.ShortcutsEnabled = true;
            this.aL1LotFormatTxb.Size = new System.Drawing.Size(285, 30);
            this.aL1LotFormatTxb.TabIndex = 111;
            this.aL1LotFormatTxb.UseSelectable = true;
            this.aL1LotFormatTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.aL1LotFormatTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // aL1LotTypeLbl
            // 
            this.aL1LotTypeLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.aL1LotTypeLbl.AutoSize = true;
            this.aL1LotTypeLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.aL1LotTypeLbl.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.aL1LotTypeLbl.Location = new System.Drawing.Point(239, 122);
            this.aL1LotTypeLbl.Name = "aL1LotTypeLbl";
            this.aL1LotTypeLbl.Size = new System.Drawing.Size(157, 25);
            this.aL1LotTypeLbl.TabIndex = 120;
            this.aL1LotTypeLbl.Text = "Lot ที่ต้องการตั้งค่า";
            // 
            // aL1OptionsLsb
            // 
            this.aL1OptionsLsb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.aL1OptionsLsb.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.aL1OptionsLsb.FormattingEnabled = true;
            this.aL1OptionsLsb.ItemHeight = 16;
            this.aL1OptionsLsb.Items.AddRange(new object[] {
            "[year]",
            "[month]",
            "[day]",
            "[itemcode]",
            "[factno]",
            "[machno]",
            "[shift]",
            "[formula]",
            "-",
            "_",
            "(",
            ")"});
            this.aL1OptionsLsb.Location = new System.Drawing.Point(239, 224);
            this.aL1OptionsLsb.Name = "aL1OptionsLsb";
            this.aL1OptionsLsb.Size = new System.Drawing.Size(122, 196);
            this.aL1OptionsLsb.TabIndex = 112;
            // 
            // aL1LotTypeCbb
            // 
            this.aL1LotTypeCbb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.aL1LotTypeCbb.FormattingEnabled = true;
            this.aL1LotTypeCbb.ItemHeight = 23;
            this.aL1LotTypeCbb.Location = new System.Drawing.Point(239, 151);
            this.aL1LotTypeCbb.Name = "aL1LotTypeCbb";
            this.aL1LotTypeCbb.Size = new System.Drawing.Size(194, 29);
            this.aL1LotTypeCbb.TabIndex = 119;
            this.aL1LotTypeCbb.UseSelectable = true;
            this.aL1LotTypeCbb.SelectedIndexChanged += new System.EventHandler(this.AL1LotTypeCbb_SelectedIndexChanged);
            // 
            // aL1SelectBtn
            // 
            this.aL1SelectBtn.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.aL1SelectBtn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.aL1SelectBtn.Location = new System.Drawing.Point(375, 277);
            this.aL1SelectBtn.Name = "aL1SelectBtn";
            this.aL1SelectBtn.Size = new System.Drawing.Size(58, 29);
            this.aL1SelectBtn.TabIndex = 113;
            this.aL1SelectBtn.Text = ">>";
            this.aL1SelectBtn.UseSelectable = true;
            this.aL1SelectBtn.Click += new System.EventHandler(this.AL1SelectBtn_Click);
            // 
            // aL1ExampleTxb
            // 
            this.aL1ExampleTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.aL1ExampleTxb.CustomButton.Image = null;
            this.aL1ExampleTxb.CustomButton.Location = new System.Drawing.Point(257, 2);
            this.aL1ExampleTxb.CustomButton.Name = "";
            this.aL1ExampleTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.aL1ExampleTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.aL1ExampleTxb.CustomButton.TabIndex = 1;
            this.aL1ExampleTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.aL1ExampleTxb.CustomButton.UseSelectable = true;
            this.aL1ExampleTxb.CustomButton.Visible = false;
            this.aL1ExampleTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.aL1ExampleTxb.Lines = new string[0];
            this.aL1ExampleTxb.Location = new System.Drawing.Point(765, 277);
            this.aL1ExampleTxb.MaxLength = 32767;
            this.aL1ExampleTxb.Multiline = true;
            this.aL1ExampleTxb.Name = "aL1ExampleTxb";
            this.aL1ExampleTxb.PasswordChar = '\0';
            this.aL1ExampleTxb.ReadOnly = true;
            this.aL1ExampleTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.aL1ExampleTxb.SelectedText = "";
            this.aL1ExampleTxb.SelectionLength = 0;
            this.aL1ExampleTxb.SelectionStart = 0;
            this.aL1ExampleTxb.ShortcutsEnabled = true;
            this.aL1ExampleTxb.Size = new System.Drawing.Size(285, 30);
            this.aL1ExampleTxb.TabIndex = 118;
            this.aL1ExampleTxb.UseSelectable = true;
            this.aL1ExampleTxb.WaterMarkColor = System.Drawing.Color.Gray;
            this.aL1ExampleTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // aL1OptionsLbl
            // 
            this.aL1OptionsLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.aL1OptionsLbl.AutoSize = true;
            this.aL1OptionsLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.aL1OptionsLbl.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.aL1OptionsLbl.Location = new System.Drawing.Point(235, 197);
            this.aL1OptionsLbl.Name = "aL1OptionsLbl";
            this.aL1OptionsLbl.Size = new System.Drawing.Size(158, 25);
            this.aL1OptionsLbl.TabIndex = 114;
            this.aL1OptionsLbl.Text = "ค่าทั้งหมดที่เลือกได้";
            // 
            // aL1ExampleLbl
            // 
            this.aL1ExampleLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.aL1ExampleLbl.AutoSize = true;
            this.aL1ExampleLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.aL1ExampleLbl.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.aL1ExampleLbl.Location = new System.Drawing.Point(765, 249);
            this.aL1ExampleLbl.Name = "aL1ExampleLbl";
            this.aL1ExampleLbl.Size = new System.Drawing.Size(269, 25);
            this.aL1ExampleLbl.TabIndex = 117;
            this.aL1ExampleLbl.Text = "ตัวอย่างหมายเลข Lot จากค่าที่ตั้ง";
            // 
            // aL1LotFormatLbl
            // 
            this.aL1LotFormatLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.aL1LotFormatLbl.AutoSize = true;
            this.aL1LotFormatLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.aL1LotFormatLbl.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.aL1LotFormatLbl.Location = new System.Drawing.Point(454, 250);
            this.aL1LotFormatLbl.Name = "aL1LotFormatLbl";
            this.aL1LotFormatLbl.Size = new System.Drawing.Size(65, 25);
            this.aL1LotFormatLbl.TabIndex = 116;
            this.aL1LotFormatLbl.Text = "ค่าที่ตั้ง";
            // 
            // secretTabPage
            // 
            this.secretTabPage.Controls.Add(this.metroButton13);
            this.secretTabPage.Controls.Add(this.metroButton14);
            this.secretTabPage.Controls.Add(this.metroButton15);
            this.secretTabPage.Controls.Add(this.metroButton16);
            this.secretTabPage.Controls.Add(this.metroButton17);
            this.secretTabPage.Controls.Add(this.metroButton18);
            this.secretTabPage.Controls.Add(this.metroButton19);
            this.secretTabPage.Controls.Add(this.metroButton20);
            this.secretTabPage.Controls.Add(this.metroButton22);
            this.secretTabPage.Controls.Add(this.metroButton24);
            this.secretTabPage.Controls.Add(this.metroButton12);
            this.secretTabPage.Controls.Add(this.metroButton11);
            this.secretTabPage.Controls.Add(this.metroButton10);
            this.secretTabPage.Controls.Add(this.metroButton9);
            this.secretTabPage.Controls.Add(this.metroButton8);
            this.secretTabPage.Controls.Add(this.metroButton7);
            this.secretTabPage.Controls.Add(this.metroButton6);
            this.secretTabPage.Controls.Add(this.metroButton5);
            this.secretTabPage.Controls.Add(this.metroButton4);
            this.secretTabPage.Controls.Add(this.metroButton3);
            this.secretTabPage.Controls.Add(this.metroButton2);
            this.secretTabPage.Controls.Add(this.metroButton1);
            this.secretTabPage.HorizontalScrollbarBarColor = true;
            this.secretTabPage.HorizontalScrollbarHighlightOnWheel = false;
            this.secretTabPage.HorizontalScrollbarSize = 10;
            this.secretTabPage.Location = new System.Drawing.Point(4, 47);
            this.secretTabPage.Name = "secretTabPage";
            this.secretTabPage.Size = new System.Drawing.Size(1238, 663);
            this.secretTabPage.TabIndex = 10;
            this.secretTabPage.Text = "หน้าลับ";
            this.secretTabPage.VerticalScrollbarBarColor = true;
            this.secretTabPage.VerticalScrollbarHighlightOnWheel = false;
            this.secretTabPage.VerticalScrollbarSize = 10;
            // 
            // metroButton13
            // 
            this.metroButton13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.metroButton13.BackColor = System.Drawing.Color.MediumPurple;
            this.metroButton13.ForeColor = System.Drawing.Color.Lavender;
            this.metroButton13.Location = new System.Drawing.Point(515, 479);
            this.metroButton13.Name = "metroButton13";
            this.metroButton13.Size = new System.Drawing.Size(127, 30);
            this.metroButton13.TabIndex = 60;
            this.metroButton13.Text = "บันทึกข้อมูลลงระบบ";
            this.metroButton13.UseCustomBackColor = true;
            this.metroButton13.UseCustomForeColor = true;
            this.metroButton13.UseSelectable = true;
            // 
            // metroButton14
            // 
            this.metroButton14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.metroButton14.BackColor = System.Drawing.Color.LightCyan;
            this.metroButton14.Location = new System.Drawing.Point(289, 608);
            this.metroButton14.Name = "metroButton14";
            this.metroButton14.Size = new System.Drawing.Size(127, 30);
            this.metroButton14.TabIndex = 59;
            this.metroButton14.Text = "บันทึกข้อมูลลงระบบ";
            this.metroButton14.UseCustomBackColor = true;
            this.metroButton14.UseSelectable = true;
            // 
            // metroButton15
            // 
            this.metroButton15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.metroButton15.BackColor = System.Drawing.Color.LightCyan;
            this.metroButton15.Location = new System.Drawing.Point(289, 672);
            this.metroButton15.Name = "metroButton15";
            this.metroButton15.Size = new System.Drawing.Size(127, 30);
            this.metroButton15.TabIndex = 58;
            this.metroButton15.Text = "บันทึกข้อมูลลงระบบ";
            this.metroButton15.UseCustomBackColor = true;
            this.metroButton15.UseSelectable = true;
            // 
            // metroButton16
            // 
            this.metroButton16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.metroButton16.BackColor = System.Drawing.Color.IndianRed;
            this.metroButton16.ForeColor = System.Drawing.Color.MistyRose;
            this.metroButton16.Location = new System.Drawing.Point(515, 384);
            this.metroButton16.Name = "metroButton16";
            this.metroButton16.Size = new System.Drawing.Size(127, 30);
            this.metroButton16.TabIndex = 57;
            this.metroButton16.Text = "บันทึกข้อมูลลงระบบ";
            this.metroButton16.UseCustomBackColor = true;
            this.metroButton16.UseCustomForeColor = true;
            this.metroButton16.UseSelectable = true;
            // 
            // metroButton17
            // 
            this.metroButton17.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.metroButton17.BackColor = System.Drawing.Color.LightCyan;
            this.metroButton17.Location = new System.Drawing.Point(289, 448);
            this.metroButton17.Name = "metroButton17";
            this.metroButton17.Size = new System.Drawing.Size(127, 30);
            this.metroButton17.TabIndex = 56;
            this.metroButton17.Text = "บันทึกข้อมูลลงระบบ";
            this.metroButton17.UseCustomBackColor = true;
            this.metroButton17.UseSelectable = true;
            // 
            // metroButton18
            // 
            this.metroButton18.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.metroButton18.BackColor = System.Drawing.Color.LightCyan;
            this.metroButton18.Location = new System.Drawing.Point(289, 640);
            this.metroButton18.Name = "metroButton18";
            this.metroButton18.Size = new System.Drawing.Size(127, 30);
            this.metroButton18.TabIndex = 55;
            this.metroButton18.Text = "บันทึกข้อมูลลงระบบ";
            this.metroButton18.UseCustomBackColor = true;
            this.metroButton18.UseSelectable = true;
            // 
            // metroButton19
            // 
            this.metroButton19.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.metroButton19.BackColor = System.Drawing.Color.LightCyan;
            this.metroButton19.Location = new System.Drawing.Point(289, 576);
            this.metroButton19.Name = "metroButton19";
            this.metroButton19.Size = new System.Drawing.Size(127, 30);
            this.metroButton19.TabIndex = 54;
            this.metroButton19.Text = "บันทึกข้อมูลลงระบบ";
            this.metroButton19.UseCustomBackColor = true;
            this.metroButton19.UseSelectable = true;
            // 
            // metroButton20
            // 
            this.metroButton20.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.metroButton20.BackColor = System.Drawing.Color.LightCyan;
            this.metroButton20.Location = new System.Drawing.Point(289, 512);
            this.metroButton20.Name = "metroButton20";
            this.metroButton20.Size = new System.Drawing.Size(127, 30);
            this.metroButton20.TabIndex = 53;
            this.metroButton20.Text = "บันทึกข้อมูลลงระบบ";
            this.metroButton20.UseCustomBackColor = true;
            this.metroButton20.UseSelectable = true;
            // 
            // metroButton22
            // 
            this.metroButton22.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.metroButton22.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.metroButton22.ForeColor = System.Drawing.Color.FloralWhite;
            this.metroButton22.Location = new System.Drawing.Point(515, 420);
            this.metroButton22.Name = "metroButton22";
            this.metroButton22.Size = new System.Drawing.Size(127, 30);
            this.metroButton22.TabIndex = 51;
            this.metroButton22.Text = "บันทึกข้อมูลลงระบบ";
            this.metroButton22.UseCustomBackColor = true;
            this.metroButton22.UseCustomForeColor = true;
            this.metroButton22.UseSelectable = true;
            // 
            // metroButton24
            // 
            this.metroButton24.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.metroButton24.BackColor = System.Drawing.Color.LightCyan;
            this.metroButton24.Location = new System.Drawing.Point(289, 544);
            this.metroButton24.Name = "metroButton24";
            this.metroButton24.Size = new System.Drawing.Size(127, 30);
            this.metroButton24.TabIndex = 49;
            this.metroButton24.Text = "บันทึกข้อมูลลงระบบ";
            this.metroButton24.UseCustomBackColor = true;
            this.metroButton24.UseSelectable = true;
            // 
            // metroButton12
            // 
            this.metroButton12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.metroButton12.BackColor = System.Drawing.Color.ForestGreen;
            this.metroButton12.Location = new System.Drawing.Point(289, 160);
            this.metroButton12.Name = "metroButton12";
            this.metroButton12.Size = new System.Drawing.Size(127, 30);
            this.metroButton12.TabIndex = 48;
            this.metroButton12.Text = "บันทึกข้อมูลลงระบบ";
            this.metroButton12.UseCustomBackColor = true;
            this.metroButton12.UseSelectable = true;
            // 
            // metroButton11
            // 
            this.metroButton11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.metroButton11.BackColor = System.Drawing.Color.CornflowerBlue;
            this.metroButton11.ForeColor = System.Drawing.Color.LightCyan;
            this.metroButton11.Location = new System.Drawing.Point(289, 288);
            this.metroButton11.Name = "metroButton11";
            this.metroButton11.Size = new System.Drawing.Size(127, 30);
            this.metroButton11.TabIndex = 47;
            this.metroButton11.Text = "บันทึกข้อมูลลงระบบ";
            this.metroButton11.UseCustomBackColor = true;
            this.metroButton11.UseCustomForeColor = true;
            this.metroButton11.UseSelectable = true;
            // 
            // metroButton10
            // 
            this.metroButton10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.metroButton10.BackColor = System.Drawing.Color.YellowGreen;
            this.metroButton10.Location = new System.Drawing.Point(289, 352);
            this.metroButton10.Name = "metroButton10";
            this.metroButton10.Size = new System.Drawing.Size(127, 30);
            this.metroButton10.TabIndex = 46;
            this.metroButton10.Text = "บันทึกข้อมูลลงระบบ";
            this.metroButton10.UseCustomBackColor = true;
            this.metroButton10.UseSelectable = true;
            // 
            // metroButton9
            // 
            this.metroButton9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.metroButton9.BackColor = System.Drawing.Color.MediumTurquoise;
            this.metroButton9.Location = new System.Drawing.Point(289, 64);
            this.metroButton9.Name = "metroButton9";
            this.metroButton9.Size = new System.Drawing.Size(127, 30);
            this.metroButton9.TabIndex = 45;
            this.metroButton9.Text = "บันทึกข้อมูลลงระบบ";
            this.metroButton9.UseCustomBackColor = true;
            this.metroButton9.UseSelectable = true;
            // 
            // metroButton8
            // 
            this.metroButton8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.metroButton8.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.metroButton8.Location = new System.Drawing.Point(289, 128);
            this.metroButton8.Name = "metroButton8";
            this.metroButton8.Size = new System.Drawing.Size(127, 30);
            this.metroButton8.TabIndex = 44;
            this.metroButton8.Text = "บันทึกข้อมูลลงระบบ";
            this.metroButton8.UseCustomBackColor = true;
            this.metroButton8.UseSelectable = true;
            // 
            // metroButton7
            // 
            this.metroButton7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.metroButton7.BackColor = System.Drawing.Color.Plum;
            this.metroButton7.Location = new System.Drawing.Point(289, 320);
            this.metroButton7.Name = "metroButton7";
            this.metroButton7.Size = new System.Drawing.Size(127, 30);
            this.metroButton7.TabIndex = 43;
            this.metroButton7.Text = "บันทึกข้อมูลลงระบบ";
            this.metroButton7.UseCustomBackColor = true;
            this.metroButton7.UseSelectable = true;
            // 
            // metroButton6
            // 
            this.metroButton6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.metroButton6.BackColor = System.Drawing.Color.DodgerBlue;
            this.metroButton6.Location = new System.Drawing.Point(289, 256);
            this.metroButton6.Name = "metroButton6";
            this.metroButton6.Size = new System.Drawing.Size(127, 30);
            this.metroButton6.TabIndex = 42;
            this.metroButton6.Text = "บันทึกข้อมูลลงระบบ";
            this.metroButton6.UseCustomBackColor = true;
            this.metroButton6.UseCustomForeColor = true;
            this.metroButton6.UseSelectable = true;
            // 
            // metroButton5
            // 
            this.metroButton5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.metroButton5.BackColor = System.Drawing.Color.MediumAquamarine;
            this.metroButton5.Location = new System.Drawing.Point(289, 192);
            this.metroButton5.Name = "metroButton5";
            this.metroButton5.Size = new System.Drawing.Size(127, 30);
            this.metroButton5.TabIndex = 41;
            this.metroButton5.Text = "บันทึกข้อมูลลงระบบ";
            this.metroButton5.UseCustomBackColor = true;
            this.metroButton5.UseSelectable = true;
            // 
            // metroButton4
            // 
            this.metroButton4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.metroButton4.BackColor = System.Drawing.Color.Azure;
            this.metroButton4.Location = new System.Drawing.Point(289, 32);
            this.metroButton4.Name = "metroButton4";
            this.metroButton4.Size = new System.Drawing.Size(127, 30);
            this.metroButton4.TabIndex = 40;
            this.metroButton4.Text = "บันทึกข้อมูลลงระบบ";
            this.metroButton4.UseCustomBackColor = true;
            this.metroButton4.UseSelectable = true;
            // 
            // metroButton3
            // 
            this.metroButton3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.metroButton3.BackColor = System.Drawing.Color.MintCream;
            this.metroButton3.Location = new System.Drawing.Point(289, 96);
            this.metroButton3.Name = "metroButton3";
            this.metroButton3.Size = new System.Drawing.Size(127, 30);
            this.metroButton3.TabIndex = 39;
            this.metroButton3.Text = "บันทึกข้อมูลลงระบบ";
            this.metroButton3.UseCustomBackColor = true;
            this.metroButton3.UseSelectable = true;
            // 
            // metroButton2
            // 
            this.metroButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.metroButton2.BackColor = System.Drawing.Color.LightCyan;
            this.metroButton2.Location = new System.Drawing.Point(289, 0);
            this.metroButton2.Name = "metroButton2";
            this.metroButton2.Size = new System.Drawing.Size(127, 30);
            this.metroButton2.TabIndex = 38;
            this.metroButton2.Text = "บันทึกข้อมูลลงระบบ";
            this.metroButton2.UseCustomBackColor = true;
            this.metroButton2.UseSelectable = true;
            // 
            // metroButton1
            // 
            this.metroButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.metroButton1.BackColor = System.Drawing.Color.SteelBlue;
            this.metroButton1.ForeColor = System.Drawing.Color.LightCyan;
            this.metroButton1.Location = new System.Drawing.Point(515, 228);
            this.metroButton1.Name = "metroButton1";
            this.metroButton1.Size = new System.Drawing.Size(127, 30);
            this.metroButton1.TabIndex = 37;
            this.metroButton1.Text = "บันทึกข้อมูลลงระบบ";
            this.metroButton1.UseCustomBackColor = true;
            this.metroButton1.UseCustomForeColor = true;
            this.metroButton1.UseSelectable = true;
            // 
            // itemSetupTabPage
            // 
            this.itemSetupTabPage.Controls.Add(this.aIPnl);
            this.itemSetupTabPage.HorizontalScrollbarBarColor = true;
            this.itemSetupTabPage.HorizontalScrollbarHighlightOnWheel = false;
            this.itemSetupTabPage.HorizontalScrollbarSize = 0;
            this.itemSetupTabPage.Location = new System.Drawing.Point(4, 47);
            this.itemSetupTabPage.Name = "itemSetupTabPage";
            this.itemSetupTabPage.Size = new System.Drawing.Size(1238, 663);
            this.itemSetupTabPage.TabIndex = 7;
            this.itemSetupTabPage.Text = "ตั้งค่าวัตถุดิบ/สินค้า";
            this.itemSetupTabPage.VerticalScrollbarBarColor = true;
            this.itemSetupTabPage.VerticalScrollbarHighlightOnWheel = false;
            this.itemSetupTabPage.VerticalScrollbarSize = 0;
            this.itemSetupTabPage.Enter += new System.EventHandler(this.ItemSetupTabPage_Enter);
            // 
            // aIPnl
            // 
            this.aIPnl.Controls.Add(this.aI1SyncBtn);
            this.aIPnl.Controls.Add(this.aI1SubmitBtn);
            this.aIPnl.Controls.Add(this.aI1TypeLbl);
            this.aIPnl.Controls.Add(this.aI1CategoryLbl);
            this.aIPnl.Controls.Add(this.aI1TopLbl);
            this.aIPnl.Controls.Add(this.aI1TypeDgv);
            this.aIPnl.Controls.Add(this.aI1CategoryDgv);
            this.aIPnl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.aIPnl.HorizontalScrollbarBarColor = true;
            this.aIPnl.HorizontalScrollbarHighlightOnWheel = false;
            this.aIPnl.HorizontalScrollbarSize = 10;
            this.aIPnl.Location = new System.Drawing.Point(0, 0);
            this.aIPnl.Name = "aIPnl";
            this.aIPnl.Size = new System.Drawing.Size(1238, 663);
            this.aIPnl.TabIndex = 144;
            this.aIPnl.VerticalScrollbarBarColor = true;
            this.aIPnl.VerticalScrollbarHighlightOnWheel = false;
            this.aIPnl.VerticalScrollbarSize = 10;
            this.aIPnl.Enter += new System.EventHandler(this.AIPnl_Enter);
            // 
            // aI1SyncBtn
            // 
            this.aI1SyncBtn.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.aI1SyncBtn.BackColor = System.Drawing.Color.MediumPurple;
            this.aI1SyncBtn.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.aI1SyncBtn.ForeColor = System.Drawing.Color.Lavender;
            this.aI1SyncBtn.Location = new System.Drawing.Point(363, 623);
            this.aI1SyncBtn.Name = "aI1SyncBtn";
            this.aI1SyncBtn.Size = new System.Drawing.Size(213, 40);
            this.aI1SyncBtn.TabIndex = 123;
            this.aI1SyncBtn.Text = "Sync item categories from B1";
            this.aI1SyncBtn.UseCustomBackColor = true;
            this.aI1SyncBtn.UseCustomForeColor = true;
            this.aI1SyncBtn.UseSelectable = true;
            this.aI1SyncBtn.Click += new System.EventHandler(this.aI1SyncBtn_Click);
            // 
            // aI1SubmitBtn
            // 
            this.aI1SubmitBtn.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.aI1SubmitBtn.BackColor = System.Drawing.Color.SteelBlue;
            this.aI1SubmitBtn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.aI1SubmitBtn.ForeColor = System.Drawing.Color.LightCyan;
            this.aI1SubmitBtn.Location = new System.Drawing.Point(859, 623);
            this.aI1SubmitBtn.Name = "aI1SubmitBtn";
            this.aI1SubmitBtn.Size = new System.Drawing.Size(191, 40);
            this.aI1SubmitBtn.TabIndex = 122;
            this.aI1SubmitBtn.Text = "บันทึกการเปลี่ยนแปลง";
            this.aI1SubmitBtn.UseCustomBackColor = true;
            this.aI1SubmitBtn.UseCustomForeColor = true;
            this.aI1SubmitBtn.UseSelectable = true;
            this.aI1SubmitBtn.Click += new System.EventHandler(this.aI1SubmitBtn_Click);
            // 
            // aI1TypeLbl
            // 
            this.aI1TypeLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.aI1TypeLbl.AutoSize = true;
            this.aI1TypeLbl.Location = new System.Drawing.Point(600, 73);
            this.aI1TypeLbl.Name = "aI1TypeLbl";
            this.aI1TypeLbl.Size = new System.Drawing.Size(144, 19);
            this.aI1TypeLbl.TabIndex = 115;
            this.aI1TypeLbl.Text = "Item Type UOM setting";
            // 
            // aI1CategoryLbl
            // 
            this.aI1CategoryLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.aI1CategoryLbl.AutoSize = true;
            this.aI1CategoryLbl.Location = new System.Drawing.Point(126, 73);
            this.aI1CategoryLbl.Name = "aI1CategoryLbl";
            this.aI1CategoryLbl.Size = new System.Drawing.Size(94, 19);
            this.aI1CategoryLbl.TabIndex = 114;
            this.aI1CategoryLbl.Text = "Item Category";
            // 
            // aI1TopLbl
            // 
            this.aI1TopLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.aI1TopLbl.AutoSize = true;
            this.aI1TopLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.aI1TopLbl.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.aI1TopLbl.Location = new System.Drawing.Point(500, 16);
            this.aI1TopLbl.Name = "aI1TopLbl";
            this.aI1TopLbl.Size = new System.Drawing.Size(169, 25);
            this.aI1TopLbl.TabIndex = 108;
            this.aI1TopLbl.Text = "ตั้งค่าวัตถุดิบ / สินค้า";
            // 
            // aI1TypeDgv
            // 
            this.aI1TypeDgv.AllowUserToAddRows = false;
            this.aI1TypeDgv.AllowUserToDeleteRows = false;
            this.aI1TypeDgv.AllowUserToResizeColumns = false;
            this.aI1TypeDgv.AllowUserToResizeRows = false;
            this.aI1TypeDgv.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.aI1TypeDgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.aI1TypeDgv.BackgroundColor = System.Drawing.SystemColors.Window;
            this.aI1TypeDgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.aI1TypeDgv.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.aI1TypeDgv.Location = new System.Drawing.Point(600, 95);
            this.aI1TypeDgv.Name = "aI1TypeDgv";
            this.aI1TypeDgv.RowHeadersVisible = false;
            this.aI1TypeDgv.RowHeadersWidth = 62;
            this.aI1TypeDgv.Size = new System.Drawing.Size(450, 510);
            this.aI1TypeDgv.TabIndex = 112;
            this.aI1TypeDgv.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.AI1TypeDgv_CellEndEdit);
            this.aI1TypeDgv.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.aI1TypeDgv_DataError);
            // 
            // aI1CategoryDgv
            // 
            this.aI1CategoryDgv.AllowUserToAddRows = false;
            this.aI1CategoryDgv.AllowUserToDeleteRows = false;
            this.aI1CategoryDgv.AllowUserToResizeColumns = false;
            this.aI1CategoryDgv.AllowUserToResizeRows = false;
            this.aI1CategoryDgv.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.aI1CategoryDgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.aI1CategoryDgv.BackgroundColor = System.Drawing.SystemColors.Window;
            this.aI1CategoryDgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.aI1CategoryDgv.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.aI1CategoryDgv.Location = new System.Drawing.Point(126, 95);
            this.aI1CategoryDgv.Name = "aI1CategoryDgv";
            this.aI1CategoryDgv.ReadOnly = true;
            this.aI1CategoryDgv.RowHeadersVisible = false;
            this.aI1CategoryDgv.RowHeadersWidth = 62;
            this.aI1CategoryDgv.Size = new System.Drawing.Size(450, 510);
            this.aI1CategoryDgv.TabIndex = 113;
            this.aI1CategoryDgv.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.AI1CategoryDgv_CellEndEdit);
            // 
            // docSetupTabPage
            // 
            this.docSetupTabPage.Controls.Add(this.aDPnl);
            this.docSetupTabPage.HorizontalScrollbarBarColor = true;
            this.docSetupTabPage.HorizontalScrollbarHighlightOnWheel = false;
            this.docSetupTabPage.HorizontalScrollbarSize = 0;
            this.docSetupTabPage.Location = new System.Drawing.Point(4, 47);
            this.docSetupTabPage.Name = "docSetupTabPage";
            this.docSetupTabPage.Size = new System.Drawing.Size(1238, 663);
            this.docSetupTabPage.TabIndex = 8;
            this.docSetupTabPage.Text = "ตั้งค่าเลขเอกสาร";
            this.docSetupTabPage.VerticalScrollbarBarColor = true;
            this.docSetupTabPage.VerticalScrollbarHighlightOnWheel = false;
            this.docSetupTabPage.VerticalScrollbarSize = 0;
            // 
            // aDPnl
            // 
            this.aDPnl.Controls.Add(this.aD1ClearBtn);
            this.aDPnl.Controls.Add(this.aD1TopLbl);
            this.aDPnl.Controls.Add(this.aD1SubmitBtn);
            this.aDPnl.Controls.Add(this.aD1DocFormatTxb);
            this.aDPnl.Controls.Add(this.aD1DocTypeLbl);
            this.aDPnl.Controls.Add(this.aD1OptionsLsb);
            this.aDPnl.Controls.Add(this.aD1DocTypeCbb);
            this.aDPnl.Controls.Add(this.aD1SelectBtn);
            this.aDPnl.Controls.Add(this.aD1ExampleTxb);
            this.aDPnl.Controls.Add(this.aD1OptionsLbl);
            this.aDPnl.Controls.Add(this.aD1ExampleLbl);
            this.aDPnl.Controls.Add(this.aD1DocFormatLbl);
            this.aDPnl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.aDPnl.HorizontalScrollbarBarColor = true;
            this.aDPnl.HorizontalScrollbarHighlightOnWheel = false;
            this.aDPnl.HorizontalScrollbarSize = 10;
            this.aDPnl.Location = new System.Drawing.Point(0, 0);
            this.aDPnl.Name = "aDPnl";
            this.aDPnl.Size = new System.Drawing.Size(1238, 663);
            this.aDPnl.TabIndex = 122;
            this.aDPnl.VerticalScrollbarBarColor = true;
            this.aDPnl.VerticalScrollbarHighlightOnWheel = false;
            this.aDPnl.VerticalScrollbarSize = 10;
            // 
            // aD1ClearBtn
            // 
            this.aD1ClearBtn.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.aD1ClearBtn.BackColor = System.Drawing.Color.IndianRed;
            this.aD1ClearBtn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.aD1ClearBtn.ForeColor = System.Drawing.Color.MistyRose;
            this.aD1ClearBtn.Location = new System.Drawing.Point(449, 319);
            this.aD1ClearBtn.Name = "aD1ClearBtn";
            this.aD1ClearBtn.Size = new System.Drawing.Size(140, 36);
            this.aD1ClearBtn.TabIndex = 122;
            this.aD1ClearBtn.Text = "เคลียร์ค่าที่ตั้ง";
            this.aD1ClearBtn.UseCustomBackColor = true;
            this.aD1ClearBtn.UseCustomForeColor = true;
            this.aD1ClearBtn.UseSelectable = true;
            this.aD1ClearBtn.Click += new System.EventHandler(this.AD1ClearBtn_Click);
            // 
            // aD1TopLbl
            // 
            this.aD1TopLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.aD1TopLbl.AutoSize = true;
            this.aD1TopLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.aD1TopLbl.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.aD1TopLbl.Location = new System.Drawing.Point(486, 38);
            this.aD1TopLbl.Name = "aD1TopLbl";
            this.aD1TopLbl.Size = new System.Drawing.Size(253, 25);
            this.aD1TopLbl.TabIndex = 115;
            this.aD1TopLbl.Text = "ตั้งค่าการสร้างหมายเลขเอกสาร";
            // 
            // aD1SubmitBtn
            // 
            this.aD1SubmitBtn.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.aD1SubmitBtn.BackColor = System.Drawing.Color.SteelBlue;
            this.aD1SubmitBtn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.aD1SubmitBtn.ForeColor = System.Drawing.Color.LightCyan;
            this.aD1SubmitBtn.Location = new System.Drawing.Point(603, 319);
            this.aD1SubmitBtn.Name = "aD1SubmitBtn";
            this.aD1SubmitBtn.Size = new System.Drawing.Size(179, 36);
            this.aD1SubmitBtn.TabIndex = 121;
            this.aD1SubmitBtn.Text = "ยืนยันการเปลี่ยนแปลง";
            this.aD1SubmitBtn.UseCustomBackColor = true;
            this.aD1SubmitBtn.UseCustomForeColor = true;
            this.aD1SubmitBtn.UseSelectable = true;
            this.aD1SubmitBtn.Click += new System.EventHandler(this.AD1SubmitBtn_Click);
            // 
            // aD1DocFormatTxb
            // 
            this.aD1DocFormatTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.aD1DocFormatTxb.CustomButton.Image = null;
            this.aD1DocFormatTxb.CustomButton.Location = new System.Drawing.Point(258, 1);
            this.aD1DocFormatTxb.CustomButton.Name = "";
            this.aD1DocFormatTxb.CustomButton.Size = new System.Drawing.Size(27, 27);
            this.aD1DocFormatTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.aD1DocFormatTxb.CustomButton.TabIndex = 1;
            this.aD1DocFormatTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.aD1DocFormatTxb.CustomButton.UseSelectable = true;
            this.aD1DocFormatTxb.CustomButton.Visible = false;
            this.aD1DocFormatTxb.Lines = new string[0];
            this.aD1DocFormatTxb.Location = new System.Drawing.Point(449, 261);
            this.aD1DocFormatTxb.MaxLength = 32767;
            this.aD1DocFormatTxb.Multiline = true;
            this.aD1DocFormatTxb.Name = "aD1DocFormatTxb";
            this.aD1DocFormatTxb.PasswordChar = '\0';
            this.aD1DocFormatTxb.ReadOnly = true;
            this.aD1DocFormatTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.aD1DocFormatTxb.SelectedText = "";
            this.aD1DocFormatTxb.SelectionLength = 0;
            this.aD1DocFormatTxb.SelectionStart = 0;
            this.aD1DocFormatTxb.ShortcutsEnabled = true;
            this.aD1DocFormatTxb.Size = new System.Drawing.Size(286, 29);
            this.aD1DocFormatTxb.TabIndex = 111;
            this.aD1DocFormatTxb.UseSelectable = true;
            this.aD1DocFormatTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.aD1DocFormatTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // aD1DocTypeLbl
            // 
            this.aD1DocTypeLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.aD1DocTypeLbl.AutoSize = true;
            this.aD1DocTypeLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.aD1DocTypeLbl.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.aD1DocTypeLbl.Location = new System.Drawing.Point(250, 117);
            this.aD1DocTypeLbl.Name = "aD1DocTypeLbl";
            this.aD1DocTypeLbl.Size = new System.Drawing.Size(228, 25);
            this.aD1DocTypeLbl.TabIndex = 120;
            this.aD1DocTypeLbl.Text = "หมวดเอกสารที่ต้องการตั้งค่า";
            // 
            // aD1OptionsLsb
            // 
            this.aD1OptionsLsb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.aD1OptionsLsb.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.aD1OptionsLsb.FormattingEnabled = true;
            this.aD1OptionsLsb.ItemHeight = 16;
            this.aD1OptionsLsb.Items.AddRange(new object[] {
            "[year]",
            "[month]",
            "[day]",
            "[factno]",
            "[machno]",
            "[shift]",
            "[formula]",
            "-",
            "_",
            "(",
            ")"});
            this.aD1OptionsLsb.Location = new System.Drawing.Point(249, 217);
            this.aD1OptionsLsb.Name = "aD1OptionsLsb";
            this.aD1OptionsLsb.Size = new System.Drawing.Size(122, 180);
            this.aD1OptionsLsb.TabIndex = 112;
            // 
            // aD1DocTypeCbb
            // 
            this.aD1DocTypeCbb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.aD1DocTypeCbb.FormattingEnabled = true;
            this.aD1DocTypeCbb.ItemHeight = 23;
            this.aD1DocTypeCbb.Location = new System.Drawing.Point(249, 144);
            this.aD1DocTypeCbb.Name = "aD1DocTypeCbb";
            this.aD1DocTypeCbb.Size = new System.Drawing.Size(194, 29);
            this.aD1DocTypeCbb.TabIndex = 119;
            this.aD1DocTypeCbb.UseSelectable = true;
            this.aD1DocTypeCbb.SelectedIndexChanged += new System.EventHandler(this.AD1DocTypeCbb_SelectedIndexChanged);
            // 
            // aD1SelectBtn
            // 
            this.aD1SelectBtn.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.aD1SelectBtn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.aD1SelectBtn.Location = new System.Drawing.Point(377, 261);
            this.aD1SelectBtn.Name = "aD1SelectBtn";
            this.aD1SelectBtn.Size = new System.Drawing.Size(66, 29);
            this.aD1SelectBtn.TabIndex = 113;
            this.aD1SelectBtn.Text = ">>";
            this.aD1SelectBtn.UseSelectable = true;
            this.aD1SelectBtn.Click += new System.EventHandler(this.AD1SelectBtn_Click);
            // 
            // aD1ExampleTxb
            // 
            this.aD1ExampleTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.aD1ExampleTxb.CustomButton.Image = null;
            this.aD1ExampleTxb.CustomButton.Location = new System.Drawing.Point(238, 1);
            this.aD1ExampleTxb.CustomButton.Name = "";
            this.aD1ExampleTxb.CustomButton.Size = new System.Drawing.Size(27, 27);
            this.aD1ExampleTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.aD1ExampleTxb.CustomButton.TabIndex = 1;
            this.aD1ExampleTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.aD1ExampleTxb.CustomButton.UseSelectable = true;
            this.aD1ExampleTxb.CustomButton.Visible = false;
            this.aD1ExampleTxb.Lines = new string[0];
            this.aD1ExampleTxb.Location = new System.Drawing.Point(775, 261);
            this.aD1ExampleTxb.MaxLength = 32767;
            this.aD1ExampleTxb.Multiline = true;
            this.aD1ExampleTxb.Name = "aD1ExampleTxb";
            this.aD1ExampleTxb.PasswordChar = '\0';
            this.aD1ExampleTxb.ReadOnly = true;
            this.aD1ExampleTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.aD1ExampleTxb.SelectedText = "";
            this.aD1ExampleTxb.SelectionLength = 0;
            this.aD1ExampleTxb.SelectionStart = 0;
            this.aD1ExampleTxb.ShortcutsEnabled = true;
            this.aD1ExampleTxb.Size = new System.Drawing.Size(266, 29);
            this.aD1ExampleTxb.TabIndex = 118;
            this.aD1ExampleTxb.UseSelectable = true;
            this.aD1ExampleTxb.WaterMarkColor = System.Drawing.Color.Gray;
            this.aD1ExampleTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // aD1OptionsLbl
            // 
            this.aD1OptionsLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.aD1OptionsLbl.AutoSize = true;
            this.aD1OptionsLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.aD1OptionsLbl.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.aD1OptionsLbl.Location = new System.Drawing.Point(249, 191);
            this.aD1OptionsLbl.Name = "aD1OptionsLbl";
            this.aD1OptionsLbl.Size = new System.Drawing.Size(158, 25);
            this.aD1OptionsLbl.TabIndex = 114;
            this.aD1OptionsLbl.Text = "ค่าทั้งหมดที่เลือกได้";
            // 
            // aD1ExampleLbl
            // 
            this.aD1ExampleLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.aD1ExampleLbl.AutoSize = true;
            this.aD1ExampleLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.aD1ExampleLbl.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.aD1ExampleLbl.Location = new System.Drawing.Point(775, 234);
            this.aD1ExampleLbl.Name = "aD1ExampleLbl";
            this.aD1ExampleLbl.Size = new System.Drawing.Size(290, 25);
            this.aD1ExampleLbl.TabIndex = 117;
            this.aD1ExampleLbl.Text = "ตัวอย่างหมายเลขเอกสารจากค่าที่ตั้ง";
            // 
            // aD1DocFormatLbl
            // 
            this.aD1DocFormatLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.aD1DocFormatLbl.AutoSize = true;
            this.aD1DocFormatLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.aD1DocFormatLbl.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.aD1DocFormatLbl.Location = new System.Drawing.Point(449, 234);
            this.aD1DocFormatLbl.Name = "aD1DocFormatLbl";
            this.aD1DocFormatLbl.Size = new System.Drawing.Size(65, 25);
            this.aD1DocFormatLbl.TabIndex = 116;
            this.aD1DocFormatLbl.Text = "ค่าที่ตั้ง";
            // 
            // reprintTabPage
            // 
            this.reprintTabPage.Controls.Add(this.reprintPanel);
            this.reprintTabPage.HorizontalScrollbarBarColor = true;
            this.reprintTabPage.HorizontalScrollbarHighlightOnWheel = false;
            this.reprintTabPage.HorizontalScrollbarSize = 0;
            this.reprintTabPage.Location = new System.Drawing.Point(4, 47);
            this.reprintTabPage.Name = "reprintTabPage";
            this.reprintTabPage.Size = new System.Drawing.Size(1238, 663);
            this.reprintTabPage.TabIndex = 11;
            this.reprintTabPage.Text = "พิมพ์บาร์โค้ดจาก ID";
            this.reprintTabPage.VerticalScrollbarBarColor = true;
            this.reprintTabPage.VerticalScrollbarHighlightOnWheel = false;
            this.reprintTabPage.VerticalScrollbarSize = 0;
            // 
            // reprintPanel
            // 
            this.reprintPanel.Controls.Add(this.reprintInfoPnl);
            this.reprintPanel.Controls.Add(this.checkIdBtn);
            this.reprintPanel.Controls.Add(this.metroLabel24);
            this.reprintPanel.Controls.Add(this.itemIdTxb);
            this.reprintPanel.Controls.Add(this.metroLabel25);
            this.reprintPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.reprintPanel.HorizontalScrollbarBarColor = true;
            this.reprintPanel.HorizontalScrollbarHighlightOnWheel = false;
            this.reprintPanel.HorizontalScrollbarSize = 10;
            this.reprintPanel.Location = new System.Drawing.Point(0, 0);
            this.reprintPanel.Name = "reprintPanel";
            this.reprintPanel.Size = new System.Drawing.Size(1238, 663);
            this.reprintPanel.TabIndex = 124;
            this.reprintPanel.VerticalScrollbarBarColor = true;
            this.reprintPanel.VerticalScrollbarHighlightOnWheel = false;
            this.reprintPanel.VerticalScrollbarSize = 10;
            // 
            // reprintInfoPnl
            // 
            this.reprintInfoPnl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.reprintInfoPnl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.reprintInfoPnl.Controls.Add(this.rePrintBcBtn);
            this.reprintInfoPnl.Controls.Add(this.metroLabel20);
            this.reprintInfoPnl.Controls.Add(this.grossWgTxb);
            this.reprintInfoPnl.Controls.Add(this.metroLabel21);
            this.reprintInfoPnl.Controls.Add(this.createDateTxb);
            this.reprintInfoPnl.Controls.Add(this.metroLabel22);
            this.reprintInfoPnl.Controls.Add(this.lotNoTxb);
            this.reprintInfoPnl.Controls.Add(this.metroLabel23);
            this.reprintInfoPnl.Controls.Add(this.itemCodetxb);
            this.reprintInfoPnl.Enabled = false;
            this.reprintInfoPnl.HorizontalScrollbarBarColor = true;
            this.reprintInfoPnl.HorizontalScrollbarHighlightOnWheel = false;
            this.reprintInfoPnl.HorizontalScrollbarSize = 6;
            this.reprintInfoPnl.Location = new System.Drawing.Point(387, 192);
            this.reprintInfoPnl.Margin = new System.Windows.Forms.Padding(2);
            this.reprintInfoPnl.Name = "reprintInfoPnl";
            this.reprintInfoPnl.Size = new System.Drawing.Size(465, 240);
            this.reprintInfoPnl.TabIndex = 119;
            this.reprintInfoPnl.VerticalScrollbarBarColor = true;
            this.reprintInfoPnl.VerticalScrollbarHighlightOnWheel = false;
            this.reprintInfoPnl.VerticalScrollbarSize = 7;
            // 
            // rePrintBcBtn
            // 
            this.rePrintBcBtn.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.rePrintBcBtn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.rePrintBcBtn.ForeColor = System.Drawing.Color.FloralWhite;
            this.rePrintBcBtn.Location = new System.Drawing.Point(303, 191);
            this.rePrintBcBtn.Margin = new System.Windows.Forms.Padding(2);
            this.rePrintBcBtn.Name = "rePrintBcBtn";
            this.rePrintBcBtn.Size = new System.Drawing.Size(126, 40);
            this.rePrintBcBtn.TabIndex = 126;
            this.rePrintBcBtn.Text = "พิมพ์บาร์โค้ด";
            this.rePrintBcBtn.UseCustomBackColor = true;
            this.rePrintBcBtn.UseCustomForeColor = true;
            this.rePrintBcBtn.UseSelectable = true;
            this.rePrintBcBtn.Click += new System.EventHandler(this.rePrintBcBtn_Click);
            // 
            // metroLabel20
            // 
            this.metroLabel20.AutoSize = true;
            this.metroLabel20.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel20.Location = new System.Drawing.Point(25, 150);
            this.metroLabel20.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel20.Name = "metroLabel20";
            this.metroLabel20.Size = new System.Drawing.Size(165, 25);
            this.metroLabel20.TabIndex = 125;
            this.metroLabel20.Text = "ปริมาณรวมบรรจุภัณฑ์";
            // 
            // grossWgTxb
            // 
            // 
            // 
            // 
            this.grossWgTxb.CustomButton.Image = null;
            this.grossWgTxb.CustomButton.Location = new System.Drawing.Point(208, 2);
            this.grossWgTxb.CustomButton.Margin = new System.Windows.Forms.Padding(1);
            this.grossWgTxb.CustomButton.Name = "";
            this.grossWgTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.grossWgTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.grossWgTxb.CustomButton.TabIndex = 1;
            this.grossWgTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.grossWgTxb.CustomButton.UseSelectable = true;
            this.grossWgTxb.CustomButton.Visible = false;
            this.grossWgTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.grossWgTxb.Lines = new string[0];
            this.grossWgTxb.Location = new System.Drawing.Point(194, 150);
            this.grossWgTxb.Margin = new System.Windows.Forms.Padding(2);
            this.grossWgTxb.MaxLength = 32767;
            this.grossWgTxb.Name = "grossWgTxb";
            this.grossWgTxb.PasswordChar = '\0';
            this.grossWgTxb.ReadOnly = true;
            this.grossWgTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.grossWgTxb.SelectedText = "";
            this.grossWgTxb.SelectionLength = 0;
            this.grossWgTxb.SelectionStart = 0;
            this.grossWgTxb.ShortcutsEnabled = true;
            this.grossWgTxb.Size = new System.Drawing.Size(236, 30);
            this.grossWgTxb.TabIndex = 124;
            this.grossWgTxb.UseSelectable = true;
            this.grossWgTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.grossWgTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel21
            // 
            this.metroLabel21.AutoSize = true;
            this.metroLabel21.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel21.Location = new System.Drawing.Point(25, 107);
            this.metroLabel21.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel21.Name = "metroLabel21";
            this.metroLabel21.Size = new System.Drawing.Size(73, 25);
            this.metroLabel21.TabIndex = 123;
            this.metroLabel21.Text = "วันที่ผลิต";
            // 
            // createDateTxb
            // 
            // 
            // 
            // 
            this.createDateTxb.CustomButton.Image = null;
            this.createDateTxb.CustomButton.Location = new System.Drawing.Point(208, 2);
            this.createDateTxb.CustomButton.Margin = new System.Windows.Forms.Padding(1);
            this.createDateTxb.CustomButton.Name = "";
            this.createDateTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.createDateTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.createDateTxb.CustomButton.TabIndex = 1;
            this.createDateTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.createDateTxb.CustomButton.UseSelectable = true;
            this.createDateTxb.CustomButton.Visible = false;
            this.createDateTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.createDateTxb.Lines = new string[0];
            this.createDateTxb.Location = new System.Drawing.Point(194, 107);
            this.createDateTxb.Margin = new System.Windows.Forms.Padding(2);
            this.createDateTxb.MaxLength = 32767;
            this.createDateTxb.Name = "createDateTxb";
            this.createDateTxb.PasswordChar = '\0';
            this.createDateTxb.ReadOnly = true;
            this.createDateTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.createDateTxb.SelectedText = "";
            this.createDateTxb.SelectionLength = 0;
            this.createDateTxb.SelectionStart = 0;
            this.createDateTxb.ShortcutsEnabled = true;
            this.createDateTxb.Size = new System.Drawing.Size(236, 30);
            this.createDateTxb.TabIndex = 122;
            this.createDateTxb.UseSelectable = true;
            this.createDateTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.createDateTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel22
            // 
            this.metroLabel22.AutoSize = true;
            this.metroLabel22.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel22.Location = new System.Drawing.Point(25, 64);
            this.metroLabel22.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel22.Name = "metroLabel22";
            this.metroLabel22.Size = new System.Drawing.Size(35, 25);
            this.metroLabel22.TabIndex = 121;
            this.metroLabel22.Text = "Lot";
            // 
            // lotNoTxb
            // 
            // 
            // 
            // 
            this.lotNoTxb.CustomButton.Image = null;
            this.lotNoTxb.CustomButton.Location = new System.Drawing.Point(209, 2);
            this.lotNoTxb.CustomButton.Margin = new System.Windows.Forms.Padding(1);
            this.lotNoTxb.CustomButton.Name = "";
            this.lotNoTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.lotNoTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.lotNoTxb.CustomButton.TabIndex = 1;
            this.lotNoTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.lotNoTxb.CustomButton.UseSelectable = true;
            this.lotNoTxb.CustomButton.Visible = false;
            this.lotNoTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.lotNoTxb.Lines = new string[0];
            this.lotNoTxb.Location = new System.Drawing.Point(193, 64);
            this.lotNoTxb.Margin = new System.Windows.Forms.Padding(2);
            this.lotNoTxb.MaxLength = 32767;
            this.lotNoTxb.Name = "lotNoTxb";
            this.lotNoTxb.PasswordChar = '\0';
            this.lotNoTxb.ReadOnly = true;
            this.lotNoTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.lotNoTxb.SelectedText = "";
            this.lotNoTxb.SelectionLength = 0;
            this.lotNoTxb.SelectionStart = 0;
            this.lotNoTxb.ShortcutsEnabled = true;
            this.lotNoTxb.Size = new System.Drawing.Size(237, 30);
            this.lotNoTxb.TabIndex = 120;
            this.lotNoTxb.UseSelectable = true;
            this.lotNoTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.lotNoTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel23
            // 
            this.metroLabel23.AutoSize = true;
            this.metroLabel23.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel23.Location = new System.Drawing.Point(25, 27);
            this.metroLabel23.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel23.Name = "metroLabel23";
            this.metroLabel23.Size = new System.Drawing.Size(80, 25);
            this.metroLabel23.TabIndex = 119;
            this.metroLabel23.Text = "รหัสสินค้า";
            // 
            // itemCodetxb
            // 
            // 
            // 
            // 
            this.itemCodetxb.CustomButton.Image = null;
            this.itemCodetxb.CustomButton.Location = new System.Drawing.Point(208, 2);
            this.itemCodetxb.CustomButton.Margin = new System.Windows.Forms.Padding(1);
            this.itemCodetxb.CustomButton.Name = "";
            this.itemCodetxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.itemCodetxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.itemCodetxb.CustomButton.TabIndex = 1;
            this.itemCodetxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.itemCodetxb.CustomButton.UseSelectable = true;
            this.itemCodetxb.CustomButton.Visible = false;
            this.itemCodetxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.itemCodetxb.Lines = new string[0];
            this.itemCodetxb.Location = new System.Drawing.Point(194, 27);
            this.itemCodetxb.Margin = new System.Windows.Forms.Padding(2);
            this.itemCodetxb.MaxLength = 32767;
            this.itemCodetxb.Name = "itemCodetxb";
            this.itemCodetxb.PasswordChar = '\0';
            this.itemCodetxb.ReadOnly = true;
            this.itemCodetxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.itemCodetxb.SelectedText = "";
            this.itemCodetxb.SelectionLength = 0;
            this.itemCodetxb.SelectionStart = 0;
            this.itemCodetxb.ShortcutsEnabled = true;
            this.itemCodetxb.Size = new System.Drawing.Size(236, 30);
            this.itemCodetxb.TabIndex = 118;
            this.itemCodetxb.UseSelectable = true;
            this.itemCodetxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.itemCodetxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // checkIdBtn
            // 
            this.checkIdBtn.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.checkIdBtn.BackColor = System.Drawing.Color.MediumPurple;
            this.checkIdBtn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.checkIdBtn.ForeColor = System.Drawing.Color.Lavender;
            this.checkIdBtn.Location = new System.Drawing.Point(712, 121);
            this.checkIdBtn.Margin = new System.Windows.Forms.Padding(2);
            this.checkIdBtn.Name = "checkIdBtn";
            this.checkIdBtn.Size = new System.Drawing.Size(133, 40);
            this.checkIdBtn.TabIndex = 118;
            this.checkIdBtn.Text = "ตรวจสอบข้อมูล";
            this.checkIdBtn.UseCustomBackColor = true;
            this.checkIdBtn.UseCustomForeColor = true;
            this.checkIdBtn.UseSelectable = true;
            this.checkIdBtn.Click += new System.EventHandler(this.checkIdBtn_Click);
            // 
            // metroLabel24
            // 
            this.metroLabel24.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.metroLabel24.AutoSize = true;
            this.metroLabel24.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel24.Location = new System.Drawing.Point(389, 129);
            this.metroLabel24.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel24.Name = "metroLabel24";
            this.metroLabel24.Size = new System.Drawing.Size(72, 25);
            this.metroLabel24.TabIndex = 117;
            this.metroLabel24.Text = "ID สินค้า";
            // 
            // itemIdTxb
            // 
            this.itemIdTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.itemIdTxb.CustomButton.Image = null;
            this.itemIdTxb.CustomButton.Location = new System.Drawing.Point(186, 2);
            this.itemIdTxb.CustomButton.Margin = new System.Windows.Forms.Padding(1);
            this.itemIdTxb.CustomButton.Name = "";
            this.itemIdTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.itemIdTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.itemIdTxb.CustomButton.TabIndex = 1;
            this.itemIdTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.itemIdTxb.CustomButton.UseSelectable = true;
            this.itemIdTxb.CustomButton.Visible = false;
            this.itemIdTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.itemIdTxb.Lines = new string[0];
            this.itemIdTxb.Location = new System.Drawing.Point(479, 126);
            this.itemIdTxb.Margin = new System.Windows.Forms.Padding(2);
            this.itemIdTxb.MaxLength = 32767;
            this.itemIdTxb.Name = "itemIdTxb";
            this.itemIdTxb.PasswordChar = '\0';
            this.itemIdTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.itemIdTxb.SelectedText = "";
            this.itemIdTxb.SelectionLength = 0;
            this.itemIdTxb.SelectionStart = 0;
            this.itemIdTxb.ShortcutsEnabled = true;
            this.itemIdTxb.Size = new System.Drawing.Size(214, 30);
            this.itemIdTxb.TabIndex = 116;
            this.itemIdTxb.UseSelectable = true;
            this.itemIdTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.itemIdTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel25
            // 
            this.metroLabel25.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.metroLabel25.AutoSize = true;
            this.metroLabel25.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel25.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel25.Location = new System.Drawing.Point(536, 35);
            this.metroLabel25.Name = "metroLabel25";
            this.metroLabel25.Size = new System.Drawing.Size(167, 25);
            this.metroLabel25.TabIndex = 115;
            this.metroLabel25.Text = "พิมพ์บาร์โค้ดจาก ID";
            // 
            // lGFormPnl
            // 
            this.lGFormPnl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lGFormPnl.BackColor = System.Drawing.Color.White;
            this.lGFormPnl.Controls.Add(this.versionLbl);
            this.lGFormPnl.Controls.Add(this.lGMagicBtn);
            this.lGFormPnl.Controls.Add(this.lGSubmitBtn);
            this.lGFormPnl.Controls.Add(this.lGPasswordTxb);
            this.lGFormPnl.Controls.Add(this.lGTopLbl);
            this.lGFormPnl.Controls.Add(this.lGUserNameTxb);
            this.lGFormPnl.Controls.Add(this.lGPasswordLbl);
            this.lGFormPnl.Controls.Add(this.lGUserNameLbl);
            this.lGFormPnl.Controls.Add(this.lGFirstLogPnl);
            this.lGFormPnl.HorizontalScrollbarBarColor = true;
            this.lGFormPnl.HorizontalScrollbarHighlightOnWheel = false;
            this.lGFormPnl.HorizontalScrollbarSize = 10;
            this.lGFormPnl.Location = new System.Drawing.Point(12, 50);
            this.lGFormPnl.Name = "lGFormPnl";
            this.lGFormPnl.Size = new System.Drawing.Size(1254, 713);
            this.lGFormPnl.TabIndex = 143;
            this.lGFormPnl.UseCustomBackColor = true;
            this.lGFormPnl.VerticalScrollbarBarColor = true;
            this.lGFormPnl.VerticalScrollbarHighlightOnWheel = false;
            this.lGFormPnl.VerticalScrollbarSize = 10;
            // 
            // versionLbl
            // 
            this.versionLbl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.versionLbl.AutoSize = true;
            this.versionLbl.Location = new System.Drawing.Point(1173, 681);
            this.versionLbl.Name = "versionLbl";
            this.versionLbl.Size = new System.Drawing.Size(70, 19);
            this.versionLbl.TabIndex = 95;
            this.versionLbl.Text = "Ver. 0.0.0.0";
            // 
            // lGMagicBtn
            // 
            this.lGMagicBtn.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lGMagicBtn.DisplayFocus = true;
            this.lGMagicBtn.Highlight = true;
            this.lGMagicBtn.Location = new System.Drawing.Point(464, 432);
            this.lGMagicBtn.Name = "lGMagicBtn";
            this.lGMagicBtn.Size = new System.Drawing.Size(351, 47);
            this.lGMagicBtn.TabIndex = 94;
            this.lGMagicBtn.Text = "Log me in as Admin!";
            this.lGMagicBtn.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.lGMagicBtn.UseSelectable = true;
            this.lGMagicBtn.Click += new System.EventHandler(this.LGMagicBtn_Click);
            // 
            // lGSubmitBtn
            // 
            this.lGSubmitBtn.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lGSubmitBtn.BackColor = System.Drawing.Color.SteelBlue;
            this.lGSubmitBtn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.lGSubmitBtn.ForeColor = System.Drawing.Color.LightCyan;
            this.lGSubmitBtn.Location = new System.Drawing.Point(533, 368);
            this.lGSubmitBtn.Name = "lGSubmitBtn";
            this.lGSubmitBtn.Size = new System.Drawing.Size(212, 40);
            this.lGSubmitBtn.TabIndex = 92;
            this.lGSubmitBtn.Text = "ยืนยัน";
            this.lGSubmitBtn.UseCustomBackColor = true;
            this.lGSubmitBtn.UseCustomForeColor = true;
            this.lGSubmitBtn.UseSelectable = true;
            this.lGSubmitBtn.Click += new System.EventHandler(this.lGSubmitBtn_Click);
            // 
            // lGPasswordTxb
            // 
            this.lGPasswordTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.lGPasswordTxb.CustomButton.Image = null;
            this.lGPasswordTxb.CustomButton.Location = new System.Drawing.Point(187, 2);
            this.lGPasswordTxb.CustomButton.Name = "";
            this.lGPasswordTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.lGPasswordTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.lGPasswordTxb.CustomButton.TabIndex = 1;
            this.lGPasswordTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.lGPasswordTxb.CustomButton.UseSelectable = true;
            this.lGPasswordTxb.CustomButton.Visible = false;
            this.lGPasswordTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.lGPasswordTxb.ForeColor = System.Drawing.Color.Gray;
            this.lGPasswordTxb.Lines = new string[0];
            this.lGPasswordTxb.Location = new System.Drawing.Point(532, 294);
            this.lGPasswordTxb.MaxLength = 32767;
            this.lGPasswordTxb.Name = "lGPasswordTxb";
            this.lGPasswordTxb.PasswordChar = '*';
            this.lGPasswordTxb.PromptText = "Password";
            this.lGPasswordTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.lGPasswordTxb.SelectedText = "";
            this.lGPasswordTxb.SelectionLength = 0;
            this.lGPasswordTxb.SelectionStart = 0;
            this.lGPasswordTxb.ShortcutsEnabled = true;
            this.lGPasswordTxb.Size = new System.Drawing.Size(215, 30);
            this.lGPasswordTxb.TabIndex = 91;
            this.lGPasswordTxb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.lGPasswordTxb.UseCustomForeColor = true;
            this.lGPasswordTxb.UseSelectable = true;
            this.lGPasswordTxb.WaterMark = "Password";
            this.lGPasswordTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.lGPasswordTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.lGPasswordTxb.Enter += new System.EventHandler(this.lGPasswordTxb_Enter);
            this.lGPasswordTxb.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lGPasswordTxb_KeyDown);
            this.lGPasswordTxb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.lGPasswordTxb_KeyPress);
            // 
            // lGTopLbl
            // 
            this.lGTopLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lGTopLbl.AutoSize = true;
            this.lGTopLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lGTopLbl.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lGTopLbl.Location = new System.Drawing.Point(594, 177);
            this.lGTopLbl.Name = "lGTopLbl";
            this.lGTopLbl.Size = new System.Drawing.Size(90, 25);
            this.lGTopLbl.TabIndex = 90;
            this.lGTopLbl.Text = "เข้าสู่ระบบ";
            // 
            // lGUserNameTxb
            // 
            this.lGUserNameTxb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.lGUserNameTxb.CustomButton.Image = null;
            this.lGUserNameTxb.CustomButton.Location = new System.Drawing.Point(187, 2);
            this.lGUserNameTxb.CustomButton.Name = "";
            this.lGUserNameTxb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.lGUserNameTxb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.lGUserNameTxb.CustomButton.TabIndex = 1;
            this.lGUserNameTxb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.lGUserNameTxb.CustomButton.UseSelectable = true;
            this.lGUserNameTxb.CustomButton.Visible = false;
            this.lGUserNameTxb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.lGUserNameTxb.ForeColor = System.Drawing.Color.Gray;
            this.lGUserNameTxb.Lines = new string[0];
            this.lGUserNameTxb.Location = new System.Drawing.Point(532, 257);
            this.lGUserNameTxb.MaxLength = 32767;
            this.lGUserNameTxb.Name = "lGUserNameTxb";
            this.lGUserNameTxb.PasswordChar = '\0';
            this.lGUserNameTxb.PromptText = "Username";
            this.lGUserNameTxb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.lGUserNameTxb.SelectedText = "";
            this.lGUserNameTxb.SelectionLength = 0;
            this.lGUserNameTxb.SelectionStart = 0;
            this.lGUserNameTxb.ShortcutsEnabled = true;
            this.lGUserNameTxb.Size = new System.Drawing.Size(215, 30);
            this.lGUserNameTxb.TabIndex = 89;
            this.lGUserNameTxb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.lGUserNameTxb.UseCustomForeColor = true;
            this.lGUserNameTxb.UseSelectable = true;
            this.lGUserNameTxb.WaterMark = "Username";
            this.lGUserNameTxb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.lGUserNameTxb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.lGUserNameTxb.Enter += new System.EventHandler(this.lGUserNameTxb_Enter);
            // 
            // lGPasswordLbl
            // 
            this.lGPasswordLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lGPasswordLbl.AutoSize = true;
            this.lGPasswordLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lGPasswordLbl.Location = new System.Drawing.Point(443, 297);
            this.lGPasswordLbl.Name = "lGPasswordLbl";
            this.lGPasswordLbl.Size = new System.Drawing.Size(71, 25);
            this.lGPasswordLbl.TabIndex = 88;
            this.lGPasswordLbl.Text = "รหัสผ่าน";
            // 
            // lGUserNameLbl
            // 
            this.lGUserNameLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lGUserNameLbl.AutoSize = true;
            this.lGUserNameLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lGUserNameLbl.Location = new System.Drawing.Point(443, 260);
            this.lGUserNameLbl.Name = "lGUserNameLbl";
            this.lGUserNameLbl.Size = new System.Drawing.Size(88, 25);
            this.lGUserNameLbl.TabIndex = 87;
            this.lGUserNameLbl.Text = "ชื่อผู้ใช้งาน";
            // 
            // lGFirstLogPnl
            // 
            this.lGFirstLogPnl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lGFirstLogPnl.Controls.Add(this.lGFirstLogCurrentUserLbl);
            this.lGFirstLogPnl.Controls.Add(this.lGFirstLogPassword2Lbl);
            this.lGFirstLogPnl.Controls.Add(this.lGFirstLogSubmitBtn);
            this.lGFirstLogPnl.Controls.Add(this.lGFirstLogPassword2Txb);
            this.lGFirstLogPnl.Controls.Add(this.lGFirstLogLbl);
            this.lGFirstLogPnl.Controls.Add(this.lGFirstLogPassword1Txb);
            this.lGFirstLogPnl.Controls.Add(this.lGFirstLogPassword1Lbl);
            this.lGFirstLogPnl.Controls.Add(this.lGFirstLogUsernameLbl);
            this.lGFirstLogPnl.HorizontalScrollbarBarColor = true;
            this.lGFirstLogPnl.HorizontalScrollbarHighlightOnWheel = false;
            this.lGFirstLogPnl.HorizontalScrollbarSize = 10;
            this.lGFirstLogPnl.Location = new System.Drawing.Point(446, 177);
            this.lGFirstLogPnl.Name = "lGFirstLogPnl";
            this.lGFirstLogPnl.Size = new System.Drawing.Size(387, 237);
            this.lGFirstLogPnl.TabIndex = 96;
            this.lGFirstLogPnl.VerticalScrollbarBarColor = true;
            this.lGFirstLogPnl.VerticalScrollbarHighlightOnWheel = false;
            this.lGFirstLogPnl.VerticalScrollbarSize = 10;
            this.lGFirstLogPnl.Visible = false;
            // 
            // lGFirstLogCurrentUserLbl
            // 
            this.lGFirstLogCurrentUserLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lGFirstLogCurrentUserLbl.AutoSize = true;
            this.lGFirstLogCurrentUserLbl.Location = new System.Drawing.Point(166, 77);
            this.lGFirstLogCurrentUserLbl.Name = "lGFirstLogCurrentUserLbl";
            this.lGFirstLogCurrentUserLbl.Size = new System.Drawing.Size(15, 19);
            this.lGFirstLogCurrentUserLbl.TabIndex = 102;
            this.lGFirstLogCurrentUserLbl.Text = "-";
            // 
            // lGFirstLogPassword2Lbl
            // 
            this.lGFirstLogPassword2Lbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lGFirstLogPassword2Lbl.AutoSize = true;
            this.lGFirstLogPassword2Lbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lGFirstLogPassword2Lbl.Location = new System.Drawing.Point(4, 146);
            this.lGFirstLogPassword2Lbl.Name = "lGFirstLogPassword2Lbl";
            this.lGFirstLogPassword2Lbl.Size = new System.Drawing.Size(111, 25);
            this.lGFirstLogPassword2Lbl.TabIndex = 101;
            this.lGFirstLogPassword2Lbl.Text = "ยืนยันรหัสผ่าน";
            // 
            // lGFirstLogSubmitBtn
            // 
            this.lGFirstLogSubmitBtn.BackColor = System.Drawing.Color.SteelBlue;
            this.lGFirstLogSubmitBtn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.lGFirstLogSubmitBtn.ForeColor = System.Drawing.Color.LightCyan;
            this.lGFirstLogSubmitBtn.Location = new System.Drawing.Point(195, 198);
            this.lGFirstLogSubmitBtn.Name = "lGFirstLogSubmitBtn";
            this.lGFirstLogSubmitBtn.Size = new System.Drawing.Size(145, 40);
            this.lGFirstLogSubmitBtn.TabIndex = 97;
            this.lGFirstLogSubmitBtn.Text = "ยืนยัน";
            this.lGFirstLogSubmitBtn.UseCustomBackColor = true;
            this.lGFirstLogSubmitBtn.UseCustomForeColor = true;
            this.lGFirstLogSubmitBtn.UseSelectable = true;
            this.lGFirstLogSubmitBtn.Click += new System.EventHandler(this.lGFirstLogSubmitBtn_Click);
            // 
            // lGFirstLogPassword2Txb
            // 
            this.lGFirstLogPassword2Txb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.lGFirstLogPassword2Txb.CustomButton.Image = null;
            this.lGFirstLogPassword2Txb.CustomButton.Location = new System.Drawing.Point(161, 2);
            this.lGFirstLogPassword2Txb.CustomButton.Name = "";
            this.lGFirstLogPassword2Txb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.lGFirstLogPassword2Txb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.lGFirstLogPassword2Txb.CustomButton.TabIndex = 1;
            this.lGFirstLogPassword2Txb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.lGFirstLogPassword2Txb.CustomButton.UseSelectable = true;
            this.lGFirstLogPassword2Txb.CustomButton.Visible = false;
            this.lGFirstLogPassword2Txb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.lGFirstLogPassword2Txb.ForeColor = System.Drawing.Color.Gray;
            this.lGFirstLogPassword2Txb.Lines = new string[0];
            this.lGFirstLogPassword2Txb.Location = new System.Drawing.Point(166, 143);
            this.lGFirstLogPassword2Txb.MaxLength = 32767;
            this.lGFirstLogPassword2Txb.Name = "lGFirstLogPassword2Txb";
            this.lGFirstLogPassword2Txb.PasswordChar = '*';
            this.lGFirstLogPassword2Txb.PromptText = "รหัสผ่านใหม่ซ้ำอีกครั้ง";
            this.lGFirstLogPassword2Txb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.lGFirstLogPassword2Txb.SelectedText = "";
            this.lGFirstLogPassword2Txb.SelectionLength = 0;
            this.lGFirstLogPassword2Txb.SelectionStart = 0;
            this.lGFirstLogPassword2Txb.ShortcutsEnabled = true;
            this.lGFirstLogPassword2Txb.Size = new System.Drawing.Size(189, 30);
            this.lGFirstLogPassword2Txb.TabIndex = 100;
            this.lGFirstLogPassword2Txb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.lGFirstLogPassword2Txb.UseCustomForeColor = true;
            this.lGFirstLogPassword2Txb.UseSelectable = true;
            this.lGFirstLogPassword2Txb.WaterMark = "รหัสผ่านใหม่ซ้ำอีกครั้ง";
            this.lGFirstLogPassword2Txb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.lGFirstLogPassword2Txb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // lGFirstLogLbl
            // 
            this.lGFirstLogLbl.AutoSize = true;
            this.lGFirstLogLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lGFirstLogLbl.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lGFirstLogLbl.Location = new System.Drawing.Point(84, 20);
            this.lGFirstLogLbl.Name = "lGFirstLogLbl";
            this.lGFirstLogLbl.Size = new System.Drawing.Size(214, 25);
            this.lGFirstLogLbl.TabIndex = 2;
            this.lGFirstLogLbl.Text = "กรุณาตั้งรหัสผ่านของท่าน";
            // 
            // lGFirstLogPassword1Txb
            // 
            this.lGFirstLogPassword1Txb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            // 
            // 
            // 
            this.lGFirstLogPassword1Txb.CustomButton.Image = null;
            this.lGFirstLogPassword1Txb.CustomButton.Location = new System.Drawing.Point(161, 2);
            this.lGFirstLogPassword1Txb.CustomButton.Name = "";
            this.lGFirstLogPassword1Txb.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.lGFirstLogPassword1Txb.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.lGFirstLogPassword1Txb.CustomButton.TabIndex = 1;
            this.lGFirstLogPassword1Txb.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.lGFirstLogPassword1Txb.CustomButton.UseSelectable = true;
            this.lGFirstLogPassword1Txb.CustomButton.Visible = false;
            this.lGFirstLogPassword1Txb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.lGFirstLogPassword1Txb.ForeColor = System.Drawing.Color.Gray;
            this.lGFirstLogPassword1Txb.Lines = new string[0];
            this.lGFirstLogPassword1Txb.Location = new System.Drawing.Point(166, 107);
            this.lGFirstLogPassword1Txb.MaxLength = 32767;
            this.lGFirstLogPassword1Txb.Name = "lGFirstLogPassword1Txb";
            this.lGFirstLogPassword1Txb.PasswordChar = '*';
            this.lGFirstLogPassword1Txb.PromptText = "รหัสผ่านใหม่";
            this.lGFirstLogPassword1Txb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.lGFirstLogPassword1Txb.SelectedText = "";
            this.lGFirstLogPassword1Txb.SelectionLength = 0;
            this.lGFirstLogPassword1Txb.SelectionStart = 0;
            this.lGFirstLogPassword1Txb.ShortcutsEnabled = true;
            this.lGFirstLogPassword1Txb.Size = new System.Drawing.Size(189, 30);
            this.lGFirstLogPassword1Txb.TabIndex = 99;
            this.lGFirstLogPassword1Txb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.lGFirstLogPassword1Txb.UseCustomForeColor = true;
            this.lGFirstLogPassword1Txb.UseSelectable = true;
            this.lGFirstLogPassword1Txb.WaterMark = "รหัสผ่านใหม่";
            this.lGFirstLogPassword1Txb.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.lGFirstLogPassword1Txb.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // lGFirstLogPassword1Lbl
            // 
            this.lGFirstLogPassword1Lbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lGFirstLogPassword1Lbl.AutoSize = true;
            this.lGFirstLogPassword1Lbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lGFirstLogPassword1Lbl.Location = new System.Drawing.Point(4, 110);
            this.lGFirstLogPassword1Lbl.Name = "lGFirstLogPassword1Lbl";
            this.lGFirstLogPassword1Lbl.Size = new System.Drawing.Size(134, 25);
            this.lGFirstLogPassword1Lbl.TabIndex = 98;
            this.lGFirstLogPassword1Lbl.Text = "กรอกรหัสผ่านใหม่";
            // 
            // lGFirstLogUsernameLbl
            // 
            this.lGFirstLogUsernameLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lGFirstLogUsernameLbl.AutoSize = true;
            this.lGFirstLogUsernameLbl.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lGFirstLogUsernameLbl.Location = new System.Drawing.Point(4, 74);
            this.lGFirstLogUsernameLbl.Name = "lGFirstLogUsernameLbl";
            this.lGFirstLogUsernameLbl.Size = new System.Drawing.Size(88, 25);
            this.lGFirstLogUsernameLbl.TabIndex = 97;
            this.lGFirstLogUsernameLbl.Text = "ชื่อผู้ใช้งาน";
            // 
            // logoutLnk
            // 
            this.logoutLnk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.logoutLnk.FontSize = MetroFramework.MetroLinkSize.Tall;
            this.logoutLnk.Location = new System.Drawing.Point(1155, 61);
            this.logoutLnk.Name = "logoutLnk";
            this.logoutLnk.Size = new System.Drawing.Size(119, 23);
            this.logoutLnk.TabIndex = 144;
            this.logoutLnk.Text = "ออกจากระบบ";
            this.logoutLnk.UseSelectable = true;
            this.logoutLnk.Click += new System.EventHandler(this.LogoutLnk_Click);
            // 
            // Inventory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1278, 771);
            this.Controls.Add(this.logoutLnk);
            this.Controls.Add(this.mainTabControl);
            this.Controls.Add(this.lGFormPnl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(1200, 675);
            this.Name = "Inventory";
            this.ShadowType = MetroFramework.Forms.MetroFormShadowType.AeroShadow;
            this.Style = MetroFramework.MetroColorStyle.Orange;
            this.Text = " SWS Barcode System";
            this.TransparencyKey = System.Drawing.Color.Chocolate;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Inventory_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.mainTabControl.ResumeLayout(false);
            this.genLotTabPage.ResumeLayout(false);
            this.gL1Pnl.ResumeLayout(false);
            this.gL1Pnl.PerformLayout();
            this.gL1InputPnl.ResumeLayout(false);
            this.gL1InputPnl.PerformLayout();
            this.gL1BarcodePnl.ResumeLayout(false);
            this.gL1BarcodePnl.PerformLayout();
            this.gL2Pnl.ResumeLayout(false);
            this.gL2Pnl.PerformLayout();
            this.gL2TotalWeightPnl.ResumeLayout(false);
            this.gL2TotalWeightPnl.PerformLayout();
            this.metroPanel8.ResumeLayout(false);
            this.metroPanel8.PerformLayout();
            this.metroPanel9.ResumeLayout(false);
            this.metroPanel9.PerformLayout();
            this.metroPanel4.ResumeLayout(false);
            this.metroPanel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gL2GenLotTableDGV)).EndInit();
            this.issueTabPage.ResumeLayout(false);
            this.iP3Pnl.ResumeLayout(false);
            this.iP3Pnl.PerformLayout();
            this.iP3BarcodePnl.ResumeLayout(false);
            this.iP3BarcodePnl.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iP3ScannedItemDgv)).EndInit();
            this.iP3TotalPnl.ResumeLayout(false);
            this.iP3TotalPnl.PerformLayout();
            this.metroPanel6.ResumeLayout(false);
            this.metroPanel6.PerformLayout();
            this.iP3TotalGrossWeightPnl.ResumeLayout(false);
            this.iP3TotalGrossWeightPnl.PerformLayout();
            this.iP2Pnl.ResumeLayout(false);
            this.iP2Pnl.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iP2IssueProductionItemDgv)).EndInit();
            this.iP1Pnl.ResumeLayout(false);
            this.iP1Pnl.PerformLayout();
            this.iP1BarcodePnl.ResumeLayout(false);
            this.iP1BarcodePnl.PerformLayout();
            this.metroPanel5.ResumeLayout(false);
            this.metroPanel5.PerformLayout();
            this.iP4Pnl.ResumeLayout(false);
            this.iP4Pnl.PerformLayout();
            this.iP4TareInfoPnl.ResumeLayout(false);
            this.iP4TareInfoPnl.PerformLayout();
            this.metroPanel21.ResumeLayout(false);
            this.metroPanel21.PerformLayout();
            this.iP4TotalWeightPnl.ResumeLayout(false);
            this.iP4TotalWeightPnl.PerformLayout();
            this.metroPanel7.ResumeLayout(false);
            this.metroPanel7.PerformLayout();
            this.metroPanel15.ResumeLayout(false);
            this.metroPanel15.PerformLayout();
            this.metroPanel19.ResumeLayout(false);
            this.metroPanel19.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iP4JobReceiveItemDgv)).EndInit();
            this.iP4StartOptionsPanel.ResumeLayout(false);
            this.iP4StartOptionsPanel.PerformLayout();
            this.iP5Pnl.ResumeLayout(false);
            this.iP5Pnl.PerformLayout();
            this.delArrTabPage.ResumeLayout(false);
            this.dO1Pnl.ResumeLayout(false);
            this.dO1Pnl.PerformLayout();
            this.dO1BarcodePnl.ResumeLayout(false);
            this.dO1BarcodePnl.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dO1ItemDgv)).EndInit();
            this.dL1Pnl.ResumeLayout(false);
            this.dL1Pnl.PerformLayout();
            this.dL1BarcodePnl.ResumeLayout(false);
            this.dL1BarcodePnl.PerformLayout();
            this.metroPanel2.ResumeLayout(false);
            this.metroPanel2.PerformLayout();
            this.dL3Pnl.ResumeLayout(false);
            this.dL3Pnl.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dL3DeliveryDgv)).EndInit();
            this.dL2Pnl.ResumeLayout(false);
            this.dL2Pnl.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dL2DeliveryDgv)).EndInit();
            this.transferTabPage.ResumeLayout(false);
            this.tR2Pnl.ResumeLayout(false);
            this.tR2Pnl.PerformLayout();
            this.tR2BarcodePnl.ResumeLayout(false);
            this.tR2BarcodePnl.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tR2ItemDgv)).EndInit();
            this.tR2TotalWeightPnl.ResumeLayout(false);
            this.tR2TotalWeightPnl.PerformLayout();
            this.metroPanel17.ResumeLayout(false);
            this.metroPanel17.PerformLayout();
            this.metroPanel18.ResumeLayout(false);
            this.metroPanel18.PerformLayout();
            this.metroPanel20.ResumeLayout(false);
            this.metroPanel20.PerformLayout();
            this.tR1Pnl.ResumeLayout(false);
            this.tR1Pnl.PerformLayout();
            this.tR1BarcodePnl.ResumeLayout(false);
            this.tR1BarcodePnl.PerformLayout();
            this.metroPanel3.ResumeLayout(false);
            this.metroPanel3.PerformLayout();
            this.reportTabPage.ResumeLayout(false);
            this.metroPanel14.ResumeLayout(false);
            this.metroPanel14.PerformLayout();
            this.whReportPnl.ResumeLayout(false);
            this.whReportPnl.PerformLayout();
            this.adminUserTabPage.ResumeLayout(false);
            this.aU1Pnl.ResumeLayout(false);
            this.aU1Pnl.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.aU1UserListDgv)).EndInit();
            this.lotSetupTabPage.ResumeLayout(false);
            this.aLPnl.ResumeLayout(false);
            this.aLPnl.PerformLayout();
            this.secretTabPage.ResumeLayout(false);
            this.itemSetupTabPage.ResumeLayout(false);
            this.aIPnl.ResumeLayout(false);
            this.aIPnl.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.aI1TypeDgv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aI1CategoryDgv)).EndInit();
            this.docSetupTabPage.ResumeLayout(false);
            this.aDPnl.ResumeLayout(false);
            this.aDPnl.PerformLayout();
            this.reprintTabPage.ResumeLayout(false);
            this.reprintPanel.ResumeLayout(false);
            this.reprintPanel.PerformLayout();
            this.reprintInfoPnl.ResumeLayout(false);
            this.reprintInfoPnl.PerformLayout();
            this.lGFormPnl.ResumeLayout(false);
            this.lGFormPnl.PerformLayout();
            this.lGFirstLogPnl.ResumeLayout(false);
            this.lGFirstLogPnl.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private MetroFramework.Controls.MetroTabPage genLotTabPage;
        private MetroFramework.Controls.MetroTabPage transferTabPage;
        private MetroFramework.Controls.MetroTabPage delArrTabPage;
        private MetroFramework.Controls.MetroPanel gL2Pnl;
        private MetroFramework.Controls.MetroButton gL2PrintBarcodeBtn;
        private MetroFramework.Controls.MetroButton gL2PrintFormBtn;
        private MetroFramework.Controls.MetroButton gL2SubmitBtn;
        private MetroFramework.Controls.MetroButton gL2GenerateLotBtn;
        private MetroFramework.Controls.MetroComboBox gL2FromWhseTxb;
        private MetroFramework.Controls.MetroComboBox gL2ToWhseTxb;
        private MetroFramework.Controls.MetroLabel gL2DocNoLbl;
        private MetroFramework.Controls.MetroLabel gL2WeightPerTareLbl;
        private MetroFramework.Controls.MetroLabel gL2AssignDateLbl;
        private MetroFramework.Controls.MetroLabel gL2RefNoLbl;
        private MetroFramework.Controls.MetroLabel gL2Kgs2Lbl;
        private MetroFramework.Controls.MetroTextBox gL2TareTxb;
        private MetroFramework.Controls.MetroTextBox gL2RefNoTxb;
        private MetroFramework.Controls.MetroLabel gL2TareLbl;
        private MetroFramework.Controls.MetroTextBox gL2DocNoTxb;
        private MetroFramework.Controls.MetroLabel gL2DifferenceLbl;
        private MetroFramework.Controls.MetroLabel gL2ItemCodeLbl;
        private MetroFramework.Controls.MetroLabel gL2QtyCountLbl;
        private MetroFramework.Controls.MetroLabel gL2KgsLbl;
        private MetroFramework.Controls.MetroLabel gL2ItemLotNoLbl;
        private MetroFramework.Controls.MetroTextBox gL2ItemLotNoTxb;
        private MetroFramework.Controls.MetroLabel gL2QtyWeightLbl;
        private MetroFramework.Controls.MetroTextBox gL2QtyCountTxb;
        private MetroFramework.Controls.MetroLabel gL2FromWhseLbl;
        private MetroFramework.Controls.MetroTextBox gL2LotAttrTxb;
        private MetroFramework.Controls.MetroLabel gL2ToWhseLbl;
        private MetroFramework.Controls.MetroLabel gL2LotAttrLbl;
        private MetroFramework.Controls.MetroButton gL2RefreshBtn;
        private MetroFramework.Controls.MetroButton gL2ExitBtn;
        private MetroFramework.Controls.MetroLabel gL2ToLbl;
        private MetroFramework.Controls.MetroLabel gL2FromLbl;
        private MetroFramework.Controls.MetroTextBox gL2PrintToTxb;
        private MetroFramework.Controls.MetroTextBox gL2PrintStartTxb;
        private MetroFramework.Controls.MetroCheckBox gL2PrintAllChk;
        private MetroFramework.Controls.MetroTabPage issueTabPage;
        private MetroFramework.Controls.MetroPanel iP2Pnl;
        private MetroFramework.Controls.MetroLabel iP2DueDateLbl;
        private MetroFramework.Controls.MetroLabel iP2CustomerLbl;
        private MetroFramework.Controls.MetroButton iP2BackBtn;
        private MetroFramework.Controls.MetroButton iP2PrintFormBtn;
        private MetroFramework.Controls.MetroLabel iP2JobOrderNoLbl;
        private MetroFramework.Controls.MetroLabel iP2AssignDateLbl;
        private MetroFramework.Controls.MetroPanel metroPanel4;
        private MetroFramework.Controls.MetroTextBox gL2ItemCodeTxb;
        private MetroFramework.Controls.MetroTextBox gL2AssignDateTxb;
        private MetroFramework.Controls.MetroTextBox gL2WeightPerTareTxb;
        private MetroFramework.Controls.MetroTextBox gL2DifferenceTxb;
        private MetroFramework.Controls.MetroTextBox gL2QtyWeightTxb;
        private MetroFramework.Controls.MetroPanel gL1Pnl;
        private MetroFramework.Controls.MetroLabel gL1TopLbl;
        private MetroFramework.Controls.MetroButton gL1SubmitBtn;
        private MetroFramework.Controls.MetroButton gL1BarcodeInputBtn;
        private MetroFramework.Controls.MetroLabel gL1OrLbl;
        private MetroFramework.Controls.MetroLabel gL1GrNoLbl;
        private MetroFramework.Controls.MetroTextBox gL1GrNoTxb;
        private MetroFramework.Controls.MetroPanel iP1Pnl;
        private MetroFramework.Controls.MetroLabel iP1TopLbl;
        private MetroFramework.Controls.MetroButton iP1SubmitBtn;
        private MetroFramework.Controls.MetroButton iP1BarcodeBtn;
        private MetroFramework.Controls.MetroLabel iP1OrLbl;
        private MetroFramework.Controls.MetroLabel iP1JobNoLbl;
        private MetroFramework.Controls.MetroTextBox iP1JobNoTxb;
        private MetroFramework.Controls.MetroLabel iP2OpenQtyUnitLbl;
        private MetroFramework.Controls.MetroLabel iP2RejectedQtyUnitLbl;
        private MetroFramework.Controls.MetroTextBox iP2RejectedQtyCountTxb;
        private MetroFramework.Controls.MetroLabel iP2RejectedQtyCountUnitLbl;
        private MetroFramework.Controls.MetroTextBox iP2CompletedQty2Txb;
        private MetroFramework.Controls.MetroLabel iP2CompletedQtyCountUnitLbl;
        private MetroFramework.Controls.MetroTextBox iP2DueDateTxb;
        private MetroFramework.Controls.MetroTextBox iP2CustomerTxb;
        private MetroFramework.Controls.MetroTextBox iP2RejectedQtyTxb;
        private MetroFramework.Controls.MetroTextBox iP2CompletedQtyCountTxb;
        private MetroFramework.Controls.MetroTextBox iP2OpenQtyTxb;
        private MetroFramework.Controls.MetroTextBox iP2ItemCodeTxb;
        private MetroFramework.Controls.MetroTextBox iP2AssignDateTxb;
        private MetroFramework.Controls.MetroComboBox iP2WhseCbb;
        private MetroFramework.Controls.MetroLabel iP2OpenQtyLbl;
        private MetroFramework.Controls.MetroLabel iP2RefNoLbl;
        private MetroFramework.Controls.MetroLabel iP2CompletedQtyUnitLbl;
        private MetroFramework.Controls.MetroTextBox iP2CompletedQtyTxb;
        private MetroFramework.Controls.MetroTextBox iP2RefNoTxb;
        private MetroFramework.Controls.MetroLabel iP2CompletedQtyLbl;
        private MetroFramework.Controls.MetroTextBox iP2JobOrderNoTxb;
        private MetroFramework.Controls.MetroLabel iP2RejectedQtyLbl;
        private MetroFramework.Controls.MetroLabel iP2ItemCodeLbl;
        private MetroFramework.Controls.MetroLabel iP2PlannedQtyLbl;
        private MetroFramework.Controls.MetroLabel iP2CompletedQty2UnitLbl;
        private MetroFramework.Controls.MetroLabel iP2ItemDescLbl;
        private MetroFramework.Controls.MetroTextBox iP2ItemDescTxb;
        private MetroFramework.Controls.MetroLabel iP2PlannedQtyUnitLbl;
        private MetroFramework.Controls.MetroTextBox iP2PlannedQtyTxb;
        private MetroFramework.Controls.MetroLabel iP2WhseLbl;
        private MetroFramework.Controls.MetroButton iP2ScanBarcodeIssueBtn;
        private MetroFramework.Controls.MetroPanel iP3Pnl;
        private MetroFramework.Controls.MetroButton iP3CancelBtn;
        private MetroFramework.Controls.MetroButton iP3SubmitBtn;
        private MetroFramework.Controls.MetroPanel iP3TotalPnl;
        private MetroFramework.Controls.MetroPanel metroPanel6;
        private MetroFramework.Controls.MetroLabel iP3TotalNetWeightLbl;
        private MetroFramework.Controls.MetroTextBox iP3TotalNetWeightTxb;
        private MetroFramework.Controls.MetroPanel iP3TotalGrossWeightPnl;
        private MetroFramework.Controls.MetroTextBox iP3TotalGrossWeightTxb;
        private MetroFramework.Controls.MetroLabel iP3TotalGrossWeightLbl;
        private MetroFramework.Controls.MetroLabel iP3TotalWeightLbl;
        private MetroFramework.Controls.MetroPanel gL2TotalWeightPnl;
        private MetroFramework.Controls.MetroPanel metroPanel8;
        private MetroFramework.Controls.MetroLabel gL2Kgs4Lbl;
        private MetroFramework.Controls.MetroTextBox gL2TotalNetWeightTxb;
        private MetroFramework.Controls.MetroPanel metroPanel9;
        private MetroFramework.Controls.MetroTextBox gL2TotalGrossWeightTxb;
        private MetroFramework.Controls.MetroLabel gL2Kgs3Lbl;
        private MetroFramework.Controls.MetroLabel gL2TotalWeightLbl;
        private MetroFramework.Controls.MetroButton iP2JobReceiveBtn;
        private MetroFramework.Controls.MetroPanel tR2Pnl;
        private MetroFramework.Controls.MetroTextBox tR2AssignDateTxb;
        private MetroFramework.Controls.MetroButton tR2CancelBtn;
        private MetroFramework.Controls.MetroButton tR2PrintFormBtn;
        private MetroFramework.Controls.MetroButton tR2SubmitBtn;
        private MetroFramework.Controls.MetroComboBox tR2FromWhseCbb;
        private MetroFramework.Controls.MetroComboBox tR2ToWhseCbb;
        private MetroFramework.Controls.MetroLabel tR2DocNoLbl;
        private MetroFramework.Controls.MetroLabel tR2AssignDateLbl;
        private MetroFramework.Controls.MetroLabel tR2RefNoLbl;
        private MetroFramework.Controls.MetroTextBox tR2DocNoTxb;
        private MetroFramework.Controls.MetroLabel tR2FromWhseLbl;
        private MetroFramework.Controls.MetroTextBox tR2RemarkTxb;
        private MetroFramework.Controls.MetroLabel tR2ToWhseLbl;
        private MetroFramework.Controls.MetroLabel tR2RemarkLbl;
        private MetroFramework.Controls.MetroPanel tR1Pnl;
        private MetroFramework.Controls.MetroButton tR1BarcdeBtn;
        private MetroFramework.Controls.MetroLabel tR1Or2Lbl;
        private MetroFramework.Controls.MetroLabel tR1TopLbl;
        private MetroFramework.Controls.MetroButton tR1SubmitBtn;
        private MetroFramework.Controls.MetroButton tR1CreateTransferBtn;
        private MetroFramework.Controls.MetroLabel tR1Or1Lbl;
        private MetroFramework.Controls.MetroLabel tR1TransferNoLbl;
        private MetroFramework.Controls.MetroTextBox tR1TransferNoTxb;
        private MetroFramework.Controls.MetroPanel tR2TotalWeightPnl;
        private MetroFramework.Controls.MetroPanel metroPanel17;
        private MetroFramework.Controls.MetroTextBox tR2TotalCountTxb;
        private MetroFramework.Controls.MetroLabel tR2TotalCountUnitLbl;
        private MetroFramework.Controls.MetroPanel metroPanel18;
        private MetroFramework.Controls.MetroLabel tR2TotalNetWeightUnitLbl;
        private MetroFramework.Controls.MetroTextBox tR2TotalNetWeightTxb;
        private MetroFramework.Controls.MetroPanel metroPanel20;
        private MetroFramework.Controls.MetroTextBox tR2TotalGrossWeightTxb;
        private MetroFramework.Controls.MetroLabel tR2TotalGrossWeightUnitLbl;
        private MetroFramework.Controls.MetroLabel tR2TotalWeightLbl;
        private MetroFramework.Controls.MetroButton tR2ReceiveBtn;
        private MetroFramework.Controls.MetroPanel dL1Pnl;
        private MetroFramework.Controls.MetroPanel dL2Pnl;
        private MetroFramework.Controls.MetroTextBox dL2ItemStockMeasureTxb;
        private MetroFramework.Controls.MetroTextBox dL2ItemStockCountTxb;
        private MetroFramework.Controls.MetroLabel dL2ItemStockCountLbl;
        private MetroFramework.Controls.MetroLabel dL2ItemCodeLbl;
        private MetroFramework.Controls.MetroLabel dL2CustomerLbl;
        private MetroFramework.Controls.MetroLabel dL2SaleOrderLbl;
        private MetroFramework.Controls.MetroButton dL2CreateDelBtn;
        private MetroFramework.Controls.MetroButton dL1ScanBarcodeBtn;
        private MetroFramework.Controls.MetroLabel dL1Or2Lbl;
        private MetroFramework.Controls.MetroLabel dL1TopLbl;
        private MetroFramework.Controls.MetroButton dL1SubmitBtn;
        private MetroFramework.Controls.MetroLabel dL1Or1Lbl;
        private MetroFramework.Controls.MetroLabel dL1DelNoLbl;
        private MetroFramework.Controls.MetroTextBox dL1DelNoTxb;
        private MetroFramework.Controls.MetroPanel dL3Pnl;
        private MetroFramework.Controls.MetroTextBox dL3AssignDateTxb;
        private MetroFramework.Controls.MetroButton dL3CancelBtn;
        private MetroFramework.Controls.MetroButton dL3PrintFormBtn;
        private MetroFramework.Controls.MetroButton dL3SubmitBtn;
        private MetroFramework.Controls.MetroLabel dL3DocNoLbl;
        private MetroFramework.Controls.MetroLabel dL3AssignDateLbl;
        private MetroFramework.Controls.MetroLabel dL3RefNoLbl;
        private MetroFramework.Controls.MetroTextBox dL3RefNoTxb;
        private MetroFramework.Controls.MetroTextBox dL3DocNoTxb;
        private MetroFramework.Controls.MetroButton dL2AddItemBtn;
        private MetroFramework.Controls.MetroLabel dL2ItemDeliveryMeasureLbsLbl;
        private MetroFramework.Controls.MetroLabel dL2ItemDeliveryMeasureLbl;
        private MetroFramework.Controls.MetroLabel dL2ItemDeliveryCountLbl;
        private MetroFramework.Controls.MetroLabel dL2ItemStockMeasureLbsLbl;
        private MetroFramework.Controls.MetroLabel dL2ItemStockMeasureLbl;
        private MetroFramework.Controls.MetroTextBox dL2ItemStockMeasureLbsTxb;
        private MetroFramework.Controls.MetroTextBox dL2ItemDeliveryCountTxb;
        private MetroFramework.Controls.MetroTextBox dL2ItemDeliveryMeasureTxb;
        private MetroFramework.Controls.MetroTextBox dL2ItemDeliveryMeasureLbsTxb;
        private MetroFramework.Controls.MetroButton dL1CreateDelBtn;
        private MetroFramework.Controls.MetroButton dL2CancelBtn;
        private MetroFramework.Controls.MetroPanel dO1Pnl;
        private MetroFramework.Controls.MetroTextBox dO1ItemCodeTxb;
        private MetroFramework.Controls.MetroLabel dO1ItemCodeLbl;
        private MetroFramework.Controls.MetroLabel dO1ItemDescLbl;
        private MetroFramework.Controls.MetroTextBox dO1ItemDescTxb;
        private MetroFramework.Controls.MetroTextBox dO1CustomerTxb;
        private MetroFramework.Controls.MetroTextBox dO1AssignDateTxb;
        private MetroFramework.Controls.MetroButton dO1CancelBtn;
        private MetroFramework.Controls.MetroButton dO1PrintFormBtn;
        private MetroFramework.Controls.MetroButton dO1SubmitBtn;
        private MetroFramework.Controls.MetroLabel dO1DocNoLbl;
        private MetroFramework.Controls.MetroLabel dO1AssignDateLbl;
        private MetroFramework.Controls.MetroLabel dO1RefNoLbl;
        private MetroFramework.Controls.MetroTextBox dO1RefNoTxb;
        private MetroFramework.Controls.MetroTextBox dO1DocNoTxb;
        private MetroFramework.Controls.MetroLabel dO1CustomerLbl;
        private MetroFramework.Controls.MetroLabel dO1AddressLbl;
        private MetroFramework.Controls.MetroTextBox dO1AddressTxb;
        private MetroFramework.Controls.MetroTextBox dO1RemarkTxb;
        private MetroFramework.Controls.MetroLabel dO1RemarkLbl;
        private MetroFramework.Controls.MetroLabel dO1WeightLbsUnitLbl;
        private MetroFramework.Controls.MetroTextBox dO1WeightLbsTxb;
        private MetroFramework.Controls.MetroLabel dO1EqualLbl;
        private MetroFramework.Controls.MetroLabel dO1NetWeightUnitLbl;
        private MetroFramework.Controls.MetroTextBox dO1NetWeightTxb;
        private MetroFramework.Controls.MetroLabel dO1NetWeightLbl;
        private MetroFramework.Controls.MetroLabel dO1TareLbl;
        private MetroFramework.Controls.MetroLabel dO1TareUnitLbl;
        private MetroFramework.Controls.MetroTextBox dO1TareTxb;
        private MetroFramework.Controls.MetroLabel dO1QtyWeightUnitLbl;
        private MetroFramework.Controls.MetroTextBox dO1QtyWeightTxb;
        private MetroFramework.Controls.MetroLabel dO1QtyWeightLbl;
        private MetroFramework.Controls.MetroLabel dO1QtyCountUnitLbl;
        private MetroFramework.Controls.MetroTextBox dO1QtyCountTxb;
        private MetroFramework.Controls.MetroLabel dO1QtyCountLbl;
        private MetroFramework.Controls.MetroPanel gL1InputPnl;
        private MetroFramework.Controls.MetroLabel gL1AndLbl;
        private MetroFramework.Controls.MetroLabel gL1ItemCodeLbl;
        private MetroFramework.Controls.MetroTextBox iP2SORefTxb;
        private MetroFramework.Controls.MetroLabel iP2SORefLbl;
        private MetroFramework.Controls.MetroTextBox dO1TelephoneTxb;
        private MetroFramework.Controls.MetroLabel dO1TelephoneLbl;
        private MetroFramework.Controls.MetroPanel iP4Pnl;
        private MetroFramework.Controls.MetroDateTime iP4DateCbb;
        private MetroFramework.Controls.MetroLabel iP4DateLbl;
        private MetroFramework.Controls.MetroLabel iP4MiscPackagingCountUnitLbl;
        private MetroFramework.Controls.MetroTextBox iP4MiscPackagingCountTxb;
        private MetroFramework.Controls.MetroLabel iP4MiscPackagingCountLbl;
        private MetroFramework.Controls.MetroLabel iP4InnerPackagingCountUnitLbl;
        private MetroFramework.Controls.MetroTextBox iP4InnerPackagingCountTxb;
        private MetroFramework.Controls.MetroLabel iP4InnerPackagingCountLbl;
        private MetroFramework.Controls.MetroLabel iP4OuterPackagingCountUnitLbl;
        private MetroFramework.Controls.MetroTextBox iP4OuterPackagingCountTxb;
        private MetroFramework.Controls.MetroLabel iP4OuterPackagingCountLbl;
        private MetroFramework.Controls.MetroLabel iP4MiscPackagingWeightUnitLbl;
        private MetroFramework.Controls.MetroTextBox iP4MiscPackagingWeightTxb;
        private MetroFramework.Controls.MetroLabel iP4MiscPackagingLbl;
        private MetroFramework.Controls.MetroLabel iP4InnerPackagingWeightUnitLbl;
        private MetroFramework.Controls.MetroTextBox iP4InnerPackagingWeightTxb;
        private MetroFramework.Controls.MetroLabel iP4InnerPackagingLbl;
        private MetroFramework.Controls.MetroLabel iP4OuterPackagingWeightUnitLbl;
        private MetroFramework.Controls.MetroTextBox iP4OuterPackagingWeightTxb;
        private MetroFramework.Controls.MetroLabel iP4OuterPackagingLbl;
        private MetroFramework.Controls.MetroLabel iP4CompletedMeasureLbsUnitTxb;
        private MetroFramework.Controls.MetroTextBox iP4CompletedMeasureLbsTxb;
        private MetroFramework.Controls.MetroComboBox iP4ShiftCbb;
        private MetroFramework.Controls.MetroComboBox iP4FactoryCbb;
        private MetroFramework.Controls.MetroLabel iP4FactoryLbl;
        private MetroFramework.Controls.MetroTextBox iP4JobNoTxb;
        private MetroFramework.Controls.MetroLabel iP4JobNoLbl;
        private MetroFramework.Controls.MetroLabel iP4ShiftLbl;
        private MetroFramework.Controls.MetroTextBox iP4LotNoTxb;
        private MetroFramework.Controls.MetroLabel iP4CompletedMeasureUnitLbl;
        private MetroFramework.Controls.MetroTextBox iP4CompletedMeasureTxb;
        private MetroFramework.Controls.MetroTextBox iP4ItemCodeTxb;
        private MetroFramework.Controls.MetroLabel iP4CompletedQtyLbl;
        private MetroFramework.Controls.MetroLabel iP4MachineLbl;
        private MetroFramework.Controls.MetroLabel iP4LotNoLbl;
        private MetroFramework.Controls.MetroLabel iP4ItemCodeLbl;
        private MetroFramework.Controls.MetroLabel iP4CompletedCountUnitLbl;
        private MetroFramework.Controls.MetroTextBox iP4CompletedCountQtyTxb;
        private MetroFramework.Controls.MetroDateTime iP3DateCbb;
        private MetroFramework.Controls.MetroLabel iP3DateLbl;
        private MetroFramework.Controls.MetroLabel tR2BinLocationLbl;
        private MetroFramework.Controls.MetroComboBox iP4PackagingCbb;
        private MetroFramework.Controls.MetroLabel iP4PackagingLbl;
        private MetroFramework.Controls.MetroLabel iP4BinLocationLbl;
        private MetroFramework.Controls.MetroLabel iP4ToWhseLbl;
        private MetroFramework.Controls.MetroComboBox dL2PackageTypeCbb;
        private MetroFramework.Controls.MetroLabel dL2PackageTypeLbl;
        private MetroFramework.Controls.MetroLabel dL2ShipDateLbl;
        private MetroFramework.Controls.MetroLabel metroLabel188;
        private MetroFramework.Controls.MetroLabel dL2ItemDeliveryMeasureUnitLbl;
        private MetroFramework.Controls.MetroLabel dL2ItemDeliveryCountUnitLbl;
        private MetroFramework.Controls.MetroLabel dL2ItemStockMeasureUnitLbsLbl;
        private MetroFramework.Controls.MetroLabel dL2ItemStockMeasureUnitLbl;
        private MetroFramework.Controls.MetroLabel dL2ItemStockCountUnitLbl;
        private MetroFramework.Controls.MetroComboBox dL2ItemCodeCbb;
        private MetroFramework.Controls.MetroComboBox dL2SaleOrderCbb;
        private MetroFramework.Controls.MetroComboBox dL2CustomerCbb;
        private MetroFramework.Controls.MetroButton dL2SubmitBtn;
        private MetroFramework.Controls.MetroTabPage adminUserTabPage;
        private MetroFramework.Controls.MetroTabPage lotSetupTabPage;
        private MetroFramework.Controls.MetroTabPage itemSetupTabPage;
        private MetroFramework.Controls.MetroTabPage docSetupTabPage;
        private MetroFramework.Controls.MetroLabel aU1UserListLbl;
        private MetroFramework.Controls.MetroLabel aU1NewRoleLbl;
        private MetroFramework.Controls.MetroComboBox aU1NewRoleCbb;
        private MetroFramework.Controls.MetroButton aU1SubmitNewUserBtn;
        private MetroFramework.Controls.MetroTextBox aU1NewPasswordTxb;
        private MetroFramework.Controls.MetroLabel aU1TopLbl;
        private MetroFramework.Controls.MetroTextBox aU1NewUserNameTxb;
        private MetroFramework.Controls.MetroLabel aU1NewPasswordLbl;
        private MetroFramework.Controls.MetroLabel aU1NewUserNameLbl;
        private MetroFramework.Controls.MetroButton aL1SubmitBtn;
        private MetroFramework.Controls.MetroLabel aL1LotTypeLbl;
        private MetroFramework.Controls.MetroComboBox aL1LotTypeCbb;
        private MetroFramework.Controls.MetroTextBox aL1ExampleTxb;
        private MetroFramework.Controls.MetroLabel aL1ExampleLbl;
        private MetroFramework.Controls.MetroLabel aL1LotFormatLbl;
        private MetroFramework.Controls.MetroLabel aL1OptionsLbl;
        private MetroFramework.Controls.MetroLabel aL1TopLbl;
        private MetroFramework.Controls.MetroButton aL1SelectBtn;
        private System.Windows.Forms.ListBox aL1OptionsLsb;
        private MetroFramework.Controls.MetroTextBox aL1LotFormatTxb;
        private MetroFramework.Controls.MetroButton aD1SubmitBtn;
        private MetroFramework.Controls.MetroLabel aD1DocTypeLbl;
        private MetroFramework.Controls.MetroComboBox aD1DocTypeCbb;
        private MetroFramework.Controls.MetroTextBox aD1ExampleTxb;
        private MetroFramework.Controls.MetroLabel aD1ExampleLbl;
        private MetroFramework.Controls.MetroLabel aD1DocFormatLbl;
        private MetroFramework.Controls.MetroLabel aD1OptionsLbl;
        private MetroFramework.Controls.MetroLabel aD1TopLbl;
        private MetroFramework.Controls.MetroButton aD1SelectBtn;
        private System.Windows.Forms.ListBox aD1OptionsLsb;
        private MetroFramework.Controls.MetroTextBox aD1DocFormatTxb;
        private MetroFramework.Controls.MetroLabel aI1TopLbl;
        private MetroFramework.Controls.MetroPanel lGFormPnl;
        private MetroFramework.Controls.MetroButton lGSubmitBtn;
        private MetroFramework.Controls.MetroTextBox lGPasswordTxb;
        private MetroFramework.Controls.MetroLabel lGTopLbl;
        private MetroFramework.Controls.MetroTextBox lGUserNameTxb;
        private MetroFramework.Controls.MetroLabel lGPasswordLbl;
        private MetroFramework.Controls.MetroLabel lGUserNameLbl;
        private MetroFramework.Controls.MetroTabPage reportTabPage;
        private MetroFramework.Controls.MetroPanel metroPanel14;
        private MetroFramework.Controls.MetroButton genReportBtn;
        private MetroFramework.Controls.MetroDateTime endDatePicker;
        private MetroFramework.Controls.MetroLabel metroLabel92;
        private MetroFramework.Controls.MetroDateTime startDatePicker;
        private MetroFramework.Controls.MetroLabel metroLabel91;
        private MetroFramework.Controls.MetroComboBox reportNameCbb;
        private MetroFramework.Controls.MetroLabel metroLabel86;
        private MetroFramework.Controls.MetroPanel aU1Pnl;
        private System.Windows.Forms.DataGridView gL2GenLotTableDGV;
        private MetroFramework.Controls.MetroButton lGMagicBtn;
        private System.Windows.Forms.DataGridView aU1UserListDgv;
        private MetroFramework.Controls.MetroPanel aLPnl;
        private MetroFramework.Controls.MetroButton aL1ClearBtn;
        private MetroFramework.Controls.MetroPanel aDPnl;
        private MetroFramework.Controls.MetroButton aD1ClearBtn;
        private System.Windows.Forms.DataGridView aI1CategoryDgv;
        private System.Windows.Forms.DataGridView aI1TypeDgv;
        private MetroFramework.Controls.MetroLabel aI1TypeLbl;
        private MetroFramework.Controls.MetroLabel aI1CategoryLbl;
        private MetroFramework.Controls.MetroPanel aIPnl;
        private MetroFramework.Controls.MetroLink logoutLnk;
        private System.Windows.Forms.DataGridView iP2IssueProductionItemDgv;
        private MetroFramework.Controls.MetroButton iPMagicBtn;
        private System.Windows.Forms.DataGridView iP3ScannedItemDgv;
        private MetroFramework.Controls.MetroPanel iP4TotalWeightPnl;
        private MetroFramework.Controls.MetroPanel metroPanel7;
        private MetroFramework.Controls.MetroLabel iP4TotalCountLbl;
        private MetroFramework.Controls.MetroTextBox iP4TotalCountTxb;
        private MetroFramework.Controls.MetroLabel iP4TotalCountUnitLbl;
        private MetroFramework.Controls.MetroPanel metroPanel15;
        private MetroFramework.Controls.MetroLabel iP4TotalNetWeightUnitLbl;
        private MetroFramework.Controls.MetroTextBox iP4TotalNetWeightTxb;
        private MetroFramework.Controls.MetroPanel metroPanel19;
        private MetroFramework.Controls.MetroTextBox iP4TotalGrossWeightTxb;
        private MetroFramework.Controls.MetroLabel iP4TotalGrossWeightUnitLbl;
        private MetroFramework.Controls.MetroLabel iP4TotalWeightLbl;
        private MetroFramework.Controls.MetroComboBox iP4ToWhseCbb;
        private MetroFramework.Controls.MetroComboBox iP4BinLocationCbb;
        private System.Windows.Forms.DataGridView iP4JobReceiveItemDgv;
        private MetroFramework.Controls.MetroPanel iP4StartOptionsPanel;
        private MetroFramework.Controls.MetroLabel iP4StartOptionsLbl;
        private MetroFramework.Controls.MetroComboBox iP4StartOptionsCbb;
        private MetroFramework.Controls.MetroButton dO1MagicRandomItemBtn;
        private MetroFramework.Controls.MetroRadioButton tR2InRdb;
        private MetroFramework.Controls.MetroRadioButton tR2OutRdb;
        private MetroFramework.Controls.MetroComboBox tR2BinLocationCbb;
        private MetroFramework.Controls.MetroButton tR2MagicBtn;
        private MetroFramework.Controls.MetroComboBox tR2RefNoCbb;
        private MetroFramework.Controls.MetroComboBox gL1ItemCodeCbb;
        private MetroFramework.Controls.MetroTextBox dL2DueDateTxb;
        private MetroFramework.Controls.MetroDateTime dL2ShipDateDpk;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel iP2BcColorLbl;
        private MetroFramework.Controls.MetroTextBox iP2BcColorTxb;
        private MetroFramework.Controls.MetroPanel iP4TareInfoPnl;
        private System.Windows.Forms.DataGridView tR2ItemDgv;
        private System.Windows.Forms.DataGridView dO1ItemDgv;
        private System.Windows.Forms.DataGridView dL3DeliveryDgv;
        private System.Windows.Forms.DataGridView dL2DeliveryDgv;
        private MetroFramework.Controls.MetroTabControl mainTabControl;
        private MetroFramework.Controls.MetroLabel iP4InnerPackageWeightPerPieceUnitLbl;
        private MetroFramework.Controls.MetroLabel iP4InnerPackageWeightPerPieceLbl;
        private MetroFramework.Controls.MetroTextBox iP4InnerPackageWeightPerPieceTxb;
        private MetroFramework.Controls.MetroLabel iP4TareWeightPerPieceUnitLbl;
        private MetroFramework.Controls.MetroLabel iP4TareWeightPerPieceLbl;
        private MetroFramework.Controls.MetroTextBox iP4TareWeightPerPieceTxb;
        private MetroFramework.Controls.MetroLabel iP4AmountPerPackageLbl;
        private MetroFramework.Controls.MetroTextBox iP4AmountPerPackageTxb;
        private MetroFramework.Controls.MetroLabel iP4AmountPerPackageUnitLbl;
        private MetroFramework.Controls.MetroTextBox iP4MachineTxb;
        private MetroFramework.Controls.MetroComboBox iP4FormulaCbb;
        private MetroFramework.Controls.MetroLabel iP4FormulaLbl;
        private MetroFramework.Controls.MetroTabPage secretTabPage;
        private MetroLabel iP4CompletedMeasure2Lbl;
        private MetroLabel iP4CompletedMeasure1Lbl;
        private MetroLabel versionLbl;
        private MetroButton aI1SubmitBtn;
        private MetroButton aI1SyncBtn;
        private MetroPanel lGFirstLogPnl;
        private MetroLabel lGFirstLogCurrentUserLbl;
        private MetroLabel lGFirstLogPassword2Lbl;
        private MetroButton lGFirstLogSubmitBtn;
        private MetroTextBox lGFirstLogPassword2Txb;
        private MetroLabel lGFirstLogLbl;
        private MetroTextBox lGFirstLogPassword1Txb;
        private MetroLabel lGFirstLogPassword1Lbl;
        private MetroLabel lGFirstLogUsernameLbl;
        private MetroPanel metroPanel5;
        private MetroPanel metroPanel2;
        private MetroPanel metroPanel3;
        private MetroButton metroButton13;
        private MetroButton metroButton14;
        private MetroButton metroButton15;
        private MetroButton metroButton16;
        private MetroButton metroButton17;
        private MetroButton metroButton18;
        private MetroButton metroButton19;
        private MetroButton metroButton20;
        private MetroButton metroButton22;
        private MetroButton metroButton24;
        private MetroButton metroButton12;
        private MetroButton metroButton11;
        private MetroButton metroButton10;
        private MetroButton metroButton9;
        private MetroButton metroButton8;
        private MetroButton metroButton7;
        private MetroButton metroButton6;
        private MetroButton metroButton5;
        private MetroButton metroButton4;
        private MetroButton metroButton3;
        private MetroButton metroButton2;
        private MetroButton metroButton1;
        private MetroPanel gL1BarcodePnl;
        private MetroLabel metroLabel1;
        private MetroPanel iP1BarcodePnl;
        private MetroLabel metroLabel4;
        private MetroPanel dL1BarcodePnl;
        private MetroLabel metroLabel3;
        private MetroPanel tR1BarcodePnl;
        private MetroLabel metroLabel2;
        private MetroPanel iP3BarcodePnl;
        private MetroLabel metroLabel7;
        private MetroPanel dO1BarcodePnl;
        private MetroLabel metroLabel5;
        private MetroPanel tR2BarcodePnl;
        private MetroLabel metroLabel8;
        private MetroButton iP4SubmitBtn;
        private MetroButton iP4PrintBarcodeBtn;
        private MetroPanel metroPanel21;
        private MetroCheckBox iP4PrintAllChk;
        private MetroLabel iP4PrintToLbl;
        private MetroTextBox iP4PrintFromTxb;
        private MetroTextBox iP4PrintToTxb;
        private MetroLabel iP4PrintFromLbl;
        private MetroButton iP4CancelBtn;
        private MetroButton iP4GenerateLotBtn;
        private MetroLabel metroLabel10;
        private MetroLabel metroLabel9;
        private Label iP2TopLbl;
        private Label gL2TopLbl;
        private MetroLabel metroLabel11;
        private MetroLabel metroLabel12;
        private Label iP3TopLbl;
        private Label iP4TopLbl;
        private MetroLabel metroLabel14;
        private MetroLabel metroLabel13;
        private Label dL2TopLbl;
        private Label label1;
        private Label dO1TopLbl;
        private MetroLabel metroLabel15;
        private MetroLabel metroLabel17;
        private MetroLabel metroLabel16;
        private Label tR2TopLbl;
        private Panel whReportPnl;
        private MetroComboBox toWhCbb;
        private MetroComboBox fromWhCbb;
        private MetroLabel metroLabel19;
        private MetroLabel metroLabel18;
        private Label label2;
        private MetroPanel iP5Pnl;
        private MetroComboBox iP5issueListCbb;
        private MetroLabel iP5TopLbl;
        private MetroButton iP3SendB1Btn;
        private MetroButton dO1SendToB1Btn;
        private MetroButton dO1TerminateDoBtn;
        private MetroLabel dO1MessageLbl;
        private MetroCheckBox iP4SpiralChk;
        private MetroCheckBox iP4PlasticChk;
        private MetroComboBox iP4ConeCbb;
        private MetroLabel iP4ConeLbl;
        private MetroPanel reprintPanel;
        private MetroPanel reprintInfoPnl;
        private MetroButton rePrintBcBtn;
        private MetroLabel metroLabel20;
        private MetroTextBox grossWgTxb;
        private MetroLabel metroLabel21;
        private MetroTextBox createDateTxb;
        private MetroLabel metroLabel22;
        private MetroTextBox lotNoTxb;
        private MetroLabel metroLabel23;
        private MetroTextBox itemCodetxb;
        private MetroButton checkIdBtn;
        private MetroLabel metroLabel24;
        private MetroTextBox itemIdTxb;
        private MetroLabel metroLabel25;
        private MetroTabPage reprintTabPage;
    }
}

