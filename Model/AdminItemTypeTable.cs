﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWS.Model
{
    public class AdminItemTypeTable
    {
        [Browsable(false)]
        public long ID { get; set; }
        [DisplayName("ประเภท")]
        public string Name { get; set; }
        [DisplayName("หน่วยนับ")]
        public string UOMCount { get; set; }
        [DisplayName("หน่วยวัด")]
        public string UOMMeasure { get; set; }
        [DisplayName("แตก Lot")]
        public bool itemizable { get; set; }
    }
}
