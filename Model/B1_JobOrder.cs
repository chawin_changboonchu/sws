﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWS.Model
{
    public class B1_JobOrder
    {
        public int JO_DocNum { get; set; }
        public int JO_DocEntry { get; set; }
        public string JO_Status { get; set; }
        public string JO_ItemCode { get; set; }
        public string JO_FGName { get; set; }
        public string JO_ProdName { get; set; }
        public string JO_Warehouse { get; set; }
        public DateTime JO_StartDate { get; set; }
        public DateTime JO_DueDate { get; set; }
        public Nullable<decimal> JO_PlannedQty { get; set; }
        public Nullable<decimal> JO_CmpltQty { get; set; }
        public Nullable<decimal> JO_OpenQty_FG { get; set; }
        public Nullable<decimal> JO_RejectQty_FG { get; set; }
        public string JO_Uom { get; set; }
        public Nullable<int> JO_SaleOrder { get; set; }
        public string JO_CustomerID { get; set; }
        public string J0_CustomenName { get; set; }      
        public string Barcode_ColorTag { get; set; }
        public Nullable<int> RM_LineNum { get; set; }
        public string RM_ItemCode { get; set; }
        public string RM_ItemName { get; set; }
        public Nullable<decimal> RM_PlannedQty { get; set; }
        public Nullable<decimal> RM_IssuedQty { get; set; }
        public string RM_IssueType { get; set; }
        public string RM_wareHouse { get; set; }
        public Nullable<int> RM_ItemType { get; set; }
        public string RM_InvntryUom { get; set; }
        public Nullable<short> RM_ItemGroup { get; set; }
        public string RM_LotSuggest { get; set; }
        public Nullable<decimal> RM_LotSuggest_Qty { get; set; }
        public string RM_LotSuggest_Attribute { get; set; }
    }
}
