//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SWS.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class Item
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Item()
        {
            this.DeliveryOrderItem = new HashSet<DeliveryOrderItem>();
            this.GenLotItem = new HashSet<GenLotItem>();
            this.IssueProductionItem = new HashSet<IssueProductionItem>();
            this.JobReceiveItem = new HashSet<JobReceiveItem>();
            this.TransferItem = new HashSet<TransferItem>();
        }
    
        public long ID { get; set; }
        public Nullable<long> ItemTypeID { get; set; }
        public Nullable<long> ItemCategoryID { get; set; }
        public string Attributes { get; set; }
        public Nullable<decimal> Count { get; set; }
        public Nullable<decimal> Measure { get; set; }
        public string Barcode { get; set; }
        public string Whse { get; set; }
        public string BinLoc { get; set; }
        public Nullable<int> InnerPackageCount { get; set; }
        public Nullable<decimal> InnerPackageWeight { get; set; }
        public Nullable<decimal> OuterPackageWeight { get; set; }
        public Nullable<int> MiscPackageCount { get; set; }
        public Nullable<decimal> MiscPackageWeight { get; set; }
        public string VendorRef { get; set; }
        public string LotNo { get; set; }
        public Nullable<System.DateTime> ReceiveDate { get; set; }
        public Nullable<int> OuterPackageCount { get; set; }
        public string PackageType { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DeliveryOrderItem> DeliveryOrderItem { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<GenLotItem> GenLotItem { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IssueProductionItem> IssueProductionItem { get; set; }
        public virtual ItemCategory ItemCategory { get; set; }
        public virtual ItemType ItemType { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<JobReceiveItem> JobReceiveItem { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TransferItem> TransferItem { get; set; }
    }
}
