﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWS.Model
{
    public class GenLotItemTable
    {
        [Browsable(false)]
        public long ID { get; set; }
        [DisplayName("#")]
        public int Row { get; set; }
        [DisplayName("หมายเลข Lot")]
        public string LotNumber { get; set; }
        [DisplayName("คุณสมบัติของสินค้า")]
        public string Attributes { get; set; }
        [DisplayName("น.น.รวมบรรจุภัณฑ์")]
        public double GrossWeight { get; set; }
        [DisplayName("น.น.สุทธิ")]
        public double NetWeight { get; set; }
        [DisplayName("หมายเลขอ้างอิง Vendor")]
        public string VendorRef { get; set; }
        [Browsable(false)]
        public bool IsPrinted { get; set; }
        [Browsable(false)]
        public bool IsSentToB1 { get; set; }
    }
}
