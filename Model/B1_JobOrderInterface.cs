﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Animation;

namespace SWS.Model
{
    public class B1_JobOrderInterface
    {
        public long JobIssueID { get; set; }
        public int DocEntry { get; set; }
        public int LineNo { get; set; }
        public string ItemCode { get; set; }
        public string FromWhseCode { get; set; }
        public string LotNo { get; set; }
        public decimal NetPerLot { get; set; }
    }
}
