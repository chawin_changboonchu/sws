﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWS.Model
{
    public class B1_ItemInWhse
    {
        public string ItemCode { get; set; }
        public string WhsCode { get; set; }
        public decimal TotalQty { get; set; }
    }
}
