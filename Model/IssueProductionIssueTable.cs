﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWS.Model
{
    public class IssueProductionIssueTable
    {
        [Browsable(false)]
        public long ID { get; set; }

        [DisplayName("#")]
        public int RowNo { get; set; }

        [DisplayName("รหัสสินค้า")]
        public string ItemCode { get; set; }

        [DisplayName("รายละเอียดสินค้า")]
        public string Description { get; set; }

        [DisplayName("หมายเลข Lot")]
        public string LotNo { get; set; }

        [DisplayName("จำนวน")]
        public int Count { get; set; }

        [DisplayName("น.น.รวมบรรจุภัณฑ์")]
        public decimal GrossWeight { get; set; }

        [DisplayName("น.น.สุทธิ")]
        public decimal NetWeight { get; set; }

        [DisplayName("โกดัง")]
        public string Whse { get; set; }
    }
}
