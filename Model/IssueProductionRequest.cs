//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SWS.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class IssueProductionRequest
    {
        public long ID { get; set; }
        public Nullable<long> ItemCategoryID { get; set; }
        public Nullable<long> IssueProductionID { get; set; }
        public Nullable<decimal> Count { get; set; }
        public Nullable<decimal> Measure { get; set; }
        public string Whse { get; set; }
        public string SuggestedLot { get; set; }
    
        public virtual IssueProduction IssueProduction { get; set; }
        public virtual ItemCategory ItemCategory { get; set; }
    }
}
