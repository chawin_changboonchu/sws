﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWS.Model
{
    public class AdminUserUserTable
    {
        [DisplayName("ชื่อผู้ใช้")]
        public string UserName { get; set; }
        [Browsable(false)]
        public long RoleID { get; set; }
        [DisplayName("สร้างเมื่อ")]
        public DateTime CreationDate { get; set; }
        [DisplayName("วันที่เข้าใช้งานล่าสุด")]
        public DateTime LastLoginDate { get; set; }
    }
}
