﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWS.Model
{
    public class DeliveryOrderTable
    {
        [Browsable(false)]
        public long ItemID { get; set; }

        [DisplayName("#")]
        public string RowNo { get; set; }

        [DisplayName("น.น.")]
        public decimal GrossWeight { get; set; }

        [DisplayName("หมายเลข Lot")]
        public string LotNo { get; set; }
    }
}
