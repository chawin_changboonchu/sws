﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWS.Model
{
    public class IssueProductionRequestTable
    {
        [Browsable(false)]
        public long ID { get; set; }
        [DisplayName("#")]
        public int RowNo { get; set; }
        [DisplayName("รหัสสินค้า")]
        public string ItemCode { get; set; }
        [DisplayName("รายละเอียดสินค้า")]
        public string Description { get; set; }
        [DisplayName("Lot แนะนำ")]
        public string SuggestedLot { get; set; }
        [DisplayName("คุณสมบัติสินค้า")]
        public string LotAttributes { get; set; }
        [DisplayName("จำนวน")]
        public decimal Count { get; set; }
        [DisplayName("หน่วย")]
        public string CountUnit { get; set; }
        [DisplayName("ปริมาณ")]
        public decimal Measure { get; set; }
        [DisplayName("หน่วย")]
        public string MeasureUnit { get; set; }
        [DisplayName("โกดัง")]
        public string Whse { get; set; }
    }
}
