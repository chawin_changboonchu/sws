﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWS.Model
{
    public class DeliveryArrangeTable
    {
        [DisplayName("เส้นทาง")]
        public string Route { get; set; }

        [DisplayName("#")]
        public string RowNo { get; set; }

        [DisplayName("ลูกค้า")]
        public string Customer { get; set; }

        [DisplayName("ที่อยู่จัดส่ง")]
        public string BillAddress { get; set; }

        [DisplayName("SO อ้างอิง")]
        public string SaleOrder { get; set; }

        [DisplayName("รหัสสินค้า")]
        public string ItemCode { get; set; }

        [DisplayName("รายละเอียด")]
        public string Description { get; set; }

        [DisplayName("Lot แนะนำ")]
        public string SuggestedLot { get; set; }

        [DisplayName("จำนวน")]
        public decimal Count { get; set; }

        [DisplayName("หน่วย")]
        public string UOM_Count { get; set; }

        [DisplayName("ปริมาณ")]
        public decimal Measure { get; set; }

        [DisplayName("หน่วย")]
        public string UOM_Weight { get; set; }

        [DisplayName("โกดัง")]
        public string Whse { get; set; }

        [DisplayName("ลอคที่")]
        public string BinLocation { get; set; }

        [Browsable(false)]
        public string Contact { get; set; }

        [Browsable(false)]
        public long DeliveryOrderID { get; set; }

        [Browsable(false)]
        public bool IsValid { get; set; }

        [Browsable(false)]
        public string PackageType { get; set; }

        [Browsable(false)]
        public string LineNum { get; set; }
        [Browsable(false)]
        public string LineItemWhse { get; set; }

    }
}
