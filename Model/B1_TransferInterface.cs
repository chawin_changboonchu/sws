﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWS.Model
{
    public class B1_TransferInterface
    {
        public string TransferDocNum { get; set; }
        public string FromWhseCode { get; set; }
        public string ToWhseCode { get; set; }
        public string TransferRequest { get; set; }
        public string TfrDocEntry { get; set; }
        public string TfrLineNo { get; set; }
        public string ItemCode { get; set; }
        public string LotNo { get; set; }
        public decimal NetPerLot { get; set; }
        public string unitMs { get; set; }

    }
}
