﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWS.Model
{
    public class B1_TransferReq
    {
        public int DocEntry { get; set; }
        public int DocNum { get; set; }
        public Nullable<System.DateTime> DocDate { get; set; }
        public Nullable<System.DateTime> DocDueDate { get; set; }
        public string CardCode { get; set; }
        public string CardName { get; set; }
        public string FromWhs { get; set; }
        public string ToWhsCode { get; set; }
        public int LineNum { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public Nullable<decimal> Order_Qty { get; set; }
        public Nullable<decimal> OpenQty { get; set; }
        public string unitMsr { get; set; }
        public string Item_FromWhs { get; set; }
    }
}
