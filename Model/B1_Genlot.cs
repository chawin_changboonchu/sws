﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWS.Model
{
    public class B1_Genlot
    {
        public int GR_NO { get; set; }
        public int GR_DocEntry { get; set; }
        public string GR_DocStatus { get; set; }
        public DateTime GR_CreateDate { get; set; }
        public string Customer_Code { get; set; }
        public string Customer_Name { get; set; }
        public int GR_Item_RowNo { get; set; }
        public string GR_Item_RowStatus { get; set; }
        public string GR_Item_Code { get; set; }
        public string GR_Item_Name { get; set; }
        public decimal GR_Item_Qty { get; set; }
        public string Uom { get; set; }
        public string GR_Item_WH { get; set; }
        public string Batch_No { get; set; }
        public string Batch_Att_1 { get; set; }
        public DateTime Batch_Adm_Date { get; set; }
        public Int16 Item_Group_Code { get; set; }
        //public string Item_Group_Name { get; set; }
        //public string Item_Batch_Name { get; set; }
        //public string Item_Batch_Att_1 { get; set; }
        //public DateTime Item_Batch_Adm_Date { get; set; }
        //public string Item_Batch_WH { get; set; }
        //public decimal Item_Batch_Qty { get; set; }
        public int PO_No { get; set; }
        public int PO_DocEntry { get; set; }


    }
}
