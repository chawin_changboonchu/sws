﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWS.Model
{
    public class JobReceiveTable
    {
        [Browsable(false)]
        public long? ItemID { get; set; }

        [DisplayName("#")]
        public int RowNo { get; set; }


        [DisplayName("หมายเลข Lot")]
        public string LotNo { get; set; }

        [Browsable(false)]
        [DisplayName("คุณสมบัติสินค้า")]
        public string Attributes { get; set; }


        [DisplayName("จำนวนต่อ 1 แพคเกจ")]
        public int AmountPerPackage { get; set; }


        [DisplayName("ปริมาณรวมบรรจุภัณฑ์")]
        public decimal GrossWeight { get; set; }


        [DisplayName("ปริมาณสุทธิ")]
        public decimal NetWeight { get; set; }

        [Browsable(false)]
        public bool IsPrinted { get; set; }

        [Browsable(false)]
        public bool IsSentToB1 { get; set; }
    }
}
