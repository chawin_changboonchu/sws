﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWS.Model
{
    public class B1_Delivery
    {
        public int DocEntry { get; set; }
        public int DocNum { get; set; }
        public string DocType { get; set; }
        public string DocStatus { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime DocDueDate { get; set; }
        public string Customer_Code { get; set; }
        public string Customer_Name { get; set; }
        public string Cusmoter_ContractNo { get; set; }
        public string BillTo { get; set; }
        public string ShipTo { get; set; }
        public int LineNum { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public Nullable<int> ItemType { get; set; }
        public string WhsCode { get; set; }
        public DateTime ShipDate { get; set; }
        public Nullable<decimal> Order_Qty { get; set; }
        public Nullable<decimal> OpenQty { get; set; }
        public Nullable<decimal> Item_AllOnHand { get; set; }
        public Nullable<decimal> Item_InWhOnHand { get; set; }
        public string LotSuggest { get; set; }
        public Nullable<decimal> LotSuggest_Qty { get; set; }
    }
}
