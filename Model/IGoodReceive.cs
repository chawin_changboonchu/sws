using System;

namespace SWS.Model
{
    public interface IGoodReceive
    {
        string GrNo { get; set; }
        Nullable<decimal> BaleWeight { get; set; }
        Nullable<System.DateTime> CreateDate { get; set; }
    }
}