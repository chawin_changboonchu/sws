﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWS.Model
{
    public class AdminItemCategoryTable
    {
        [Browsable(false)]
        public long ID { get; set; }
        [DisplayName("หมวดหมู่")]
        public string Group { get; set; }
        [DisplayName("รหัสสินค้า")]
        public string Code { get; set; }
        [DisplayName("รายละเอียด")]
        public string Description { get; set; }
        [DisplayName("คุณสมบัติ")]
        public string Attributes { get; set; }

    }
}
