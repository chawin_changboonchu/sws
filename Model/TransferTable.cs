﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWS.Model
{
    public class TransferTable
    {
        [Browsable(false)]
        public long ItemID { get; set; }

        [DisplayName("#")]
        public string RowNo { get; set; }

        [DisplayName("รหัสสินค้า")]
        public string ItemCode { get; set; }

        [DisplayName("รายละเอียด")]
        public string Description { get; set; }

        [DisplayName("หมายเลข Lot")]
        public string LotNo { get; set; }

        [DisplayName("คุณสมบัติ")]
        public string Attributes { get; set; }

        [DisplayName("จำนวน")]
        public decimal Count { get; set; }

        [DisplayName("น.น.รวมบรรจุภัณฑ์")]
        public decimal GrossWeight { get; set; }

        [DisplayName("น.น.สุทธิ")]
        public decimal NetWeight { get; set; }

        [Browsable(false)]
        public bool IsChecked { get; set; }


    }
}
