﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWS.Model
{
    public class B1_ItemStandardWeight
    {
        public string ItemCode { get; set; }
        public decimal IWeight1 { get; set; }
        public string InvntryUom { get; set; }
    }
}
