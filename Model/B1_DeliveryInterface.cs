﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWS.Model
{
    public class B1_DeliveryInterface
    {
        public string DeliveryDocNum { get; set; }
        public int SoDocEntry { get; set; }
        public DateTime DueDate { get; set; }
        public string CustomerCode { get; set; }
        public string ContractNo { get; set; }
        public string ItemCode { get; set; }
        public int? LineNo { get; set; }        
        public string LotNo { get; set; }
        public string ItemLotFromWhseCode { get; set; }
        public decimal NetPerLot { get; set; }
    }
}
