﻿namespace SWS
{
    partial class FormAdmin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAdmin));
            this.metroTabControl1 = new MetroFramework.Controls.MetroTabControl();
            this.genLotTabPage = new MetroFramework.Controls.MetroTabPage();
            this.genLotPanel1 = new MetroFramework.Controls.MetroPanel();
            this.metroLabel38 = new MetroFramework.Controls.MetroLabel();
            this.metroButton15 = new MetroFramework.Controls.MetroButton();
            this.metroButton10 = new MetroFramework.Controls.MetroButton();
            this.metroLabel35 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel30 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox26 = new MetroFramework.Controls.MetroTextBox();
            this.genLotPanel2 = new MetroFramework.Controls.MetroPanel();
            this.metroPanel7 = new MetroFramework.Controls.MetroPanel();
            this.metroPanel8 = new MetroFramework.Controls.MetroPanel();
            this.metroLabel37 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox21 = new MetroFramework.Controls.MetroTextBox();
            this.metroPanel9 = new MetroFramework.Controls.MetroPanel();
            this.metroTextBox22 = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel41 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel45 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox25 = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBox24 = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBox23 = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBox20 = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBox18 = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel29 = new MetroFramework.Controls.MetroLabel();
            this.metroPanel4 = new MetroFramework.Controls.MetroPanel();
            this.metroCheckBox1 = new MetroFramework.Controls.MetroCheckBox();
            this.metroLabel22 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox11 = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBox12 = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel21 = new MetroFramework.Controls.MetroLabel();
            this.metroButton8 = new MetroFramework.Controls.MetroButton();
            this.metroButton4 = new MetroFramework.Controls.MetroButton();
            this.metroButton5 = new MetroFramework.Controls.MetroButton();
            this.metroButton6 = new MetroFramework.Controls.MetroButton();
            this.metroButton7 = new MetroFramework.Controls.MetroButton();
            this.metroComboBox1 = new MetroFramework.Controls.MetroComboBox();
            this.metroComboBox2 = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel14 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel15 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox10 = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBox5 = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel13 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox6 = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel12 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel11 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel16 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox7 = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel17 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox8 = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel18 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox9 = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel19 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel20 = new MetroFramework.Controls.MetroLabel();
            this.issueTabPage = new MetroFramework.Controls.MetroTabPage();
            this.issuePanel1 = new MetroFramework.Controls.MetroPanel();
            this.metroLabel39 = new MetroFramework.Controls.MetroLabel();
            this.metroButton16 = new MetroFramework.Controls.MetroButton();
            this.metroButton17 = new MetroFramework.Controls.MetroButton();
            this.metroLabel42 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel43 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox27 = new MetroFramework.Controls.MetroTextBox();
            this.issuePanel2 = new MetroFramework.Controls.MetroPanel();
            this.metroButton19 = new MetroFramework.Controls.MetroButton();
            this.metroButton18 = new MetroFramework.Controls.MetroButton();
            this.metroLabel33 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel32 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox19 = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel31 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox17 = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel28 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox15 = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBox16 = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBox28 = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBox29 = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBox30 = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBox31 = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBox32 = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel44 = new MetroFramework.Controls.MetroLabel();
            this.metroComboBox3 = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel46 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel48 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel49 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox33 = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBox34 = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel50 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox35 = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel51 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel52 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel53 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel54 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel55 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox36 = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel56 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox37 = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel57 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel26 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel40 = new MetroFramework.Controls.MetroLabel();
            this.metroButton11 = new MetroFramework.Controls.MetroButton();
            this.metroButton13 = new MetroFramework.Controls.MetroButton();
            this.metroButton14 = new MetroFramework.Controls.MetroButton();
            this.metroLabel25 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel27 = new MetroFramework.Controls.MetroLabel();
            this.transferTabPage = new MetroFramework.Controls.MetroTabPage();
            this.delArrTabPage = new MetroFramework.Controls.MetroTabPage();
            this.metroTabPage5 = new MetroFramework.Controls.MetroTabPage();
            this.metroTabPage1 = new MetroFramework.Controls.MetroTabPage();
            this.metroButton33 = new MetroFramework.Controls.MetroButton();
            this.metroButton34 = new MetroFramework.Controls.MetroButton();
            this.metroButton35 = new MetroFramework.Controls.MetroButton();
            this.metroButton36 = new MetroFramework.Controls.MetroButton();
            this.metroButton37 = new MetroFramework.Controls.MetroButton();
            this.metroButton38 = new MetroFramework.Controls.MetroButton();
            this.metroButton39 = new MetroFramework.Controls.MetroButton();
            this.metroButton40 = new MetroFramework.Controls.MetroButton();
            this.metroButton25 = new MetroFramework.Controls.MetroButton();
            this.metroButton26 = new MetroFramework.Controls.MetroButton();
            this.metroButton27 = new MetroFramework.Controls.MetroButton();
            this.metroButton28 = new MetroFramework.Controls.MetroButton();
            this.metroButton29 = new MetroFramework.Controls.MetroButton();
            this.metroButton30 = new MetroFramework.Controls.MetroButton();
            this.metroButton31 = new MetroFramework.Controls.MetroButton();
            this.metroButton32 = new MetroFramework.Controls.MetroButton();
            this.metroButton21 = new MetroFramework.Controls.MetroButton();
            this.metroButton22 = new MetroFramework.Controls.MetroButton();
            this.metroButton23 = new MetroFramework.Controls.MetroButton();
            this.metroButton24 = new MetroFramework.Controls.MetroButton();
            this.metroButton12 = new MetroFramework.Controls.MetroButton();
            this.metroButton20 = new MetroFramework.Controls.MetroButton();
            this.metroButton3 = new MetroFramework.Controls.MetroButton();
            this.metroButton2 = new MetroFramework.Controls.MetroButton();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroComboBox4 = new MetroFramework.Controls.MetroComboBox();
            this.metroButton1 = new MetroFramework.Controls.MetroButton();
            this.metroTextBox1 = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox2 = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroTabPage2 = new MetroFramework.Controls.MetroTabPage();
            this.metroButton46 = new MetroFramework.Controls.MetroButton();
            this.metroLabel61 = new MetroFramework.Controls.MetroLabel();
            this.metroComboBox6 = new MetroFramework.Controls.MetroComboBox();
            this.metroTextBox14 = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel60 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel59 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel58 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel47 = new MetroFramework.Controls.MetroLabel();
            this.metroButton43 = new MetroFramework.Controls.MetroButton();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.metroTextBox13 = new MetroFramework.Controls.MetroTextBox();
            this.metroTabPage3 = new MetroFramework.Controls.MetroTabPage();
            this.metroButton44 = new MetroFramework.Controls.MetroButton();
            this.metroButton45 = new MetroFramework.Controls.MetroButton();
            this.metroLabel62 = new MetroFramework.Controls.MetroLabel();
            this.metroTabPage4 = new MetroFramework.Controls.MetroTabPage();
            this.metroButton47 = new MetroFramework.Controls.MetroButton();
            this.metroButton48 = new MetroFramework.Controls.MetroButton();
            this.metroLabel63 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel23 = new MetroFramework.Controls.MetroLabel();
            this.metroComboBox5 = new MetroFramework.Controls.MetroComboBox();
            this.metroButton41 = new MetroFramework.Controls.MetroButton();
            this.metroTextBox3 = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel24 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox4 = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel34 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel36 = new MetroFramework.Controls.MetroLabel();
            this.metroButton42 = new MetroFramework.Controls.MetroButton();
            this.metroTextBox38 = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel64 = new MetroFramework.Controls.MetroLabel();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.metroButton9 = new MetroFramework.Controls.MetroButton();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.metroTabControl1.SuspendLayout();
            this.genLotTabPage.SuspendLayout();
            this.genLotPanel1.SuspendLayout();
            this.genLotPanel2.SuspendLayout();
            this.metroPanel7.SuspendLayout();
            this.metroPanel8.SuspendLayout();
            this.metroPanel9.SuspendLayout();
            this.metroPanel4.SuspendLayout();
            this.issueTabPage.SuspendLayout();
            this.issuePanel1.SuspendLayout();
            this.issuePanel2.SuspendLayout();
            this.metroTabPage1.SuspendLayout();
            this.metroTabPage2.SuspendLayout();
            this.metroTabPage3.SuspendLayout();
            this.metroTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            this.SuspendLayout();
            // 
            // metroTabControl1
            // 
            this.metroTabControl1.Controls.Add(this.genLotTabPage);
            this.metroTabControl1.Controls.Add(this.issueTabPage);
            this.metroTabControl1.Controls.Add(this.transferTabPage);
            this.metroTabControl1.Controls.Add(this.delArrTabPage);
            this.metroTabControl1.Controls.Add(this.metroTabPage5);
            this.metroTabControl1.Controls.Add(this.metroTabPage1);
            this.metroTabControl1.Controls.Add(this.metroTabPage2);
            this.metroTabControl1.Controls.Add(this.metroTabPage3);
            this.metroTabControl1.Controls.Add(this.metroTabPage4);
            this.metroTabControl1.Location = new System.Drawing.Point(23, 63);
            this.metroTabControl1.Name = "metroTabControl1";
            this.metroTabControl1.SelectedIndex = 7;
            this.metroTabControl1.Size = new System.Drawing.Size(978, 682);
            this.metroTabControl1.TabIndex = 20;
            this.metroTabControl1.UseSelectable = true;
            // 
            // genLotTabPage
            // 
            this.genLotTabPage.Controls.Add(this.genLotPanel1);
            this.genLotTabPage.Controls.Add(this.genLotPanel2);
            this.genLotTabPage.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.genLotTabPage.HorizontalScrollbarBarColor = true;
            this.genLotTabPage.HorizontalScrollbarHighlightOnWheel = false;
            this.genLotTabPage.HorizontalScrollbarSize = 10;
            this.genLotTabPage.Location = new System.Drawing.Point(4, 38);
            this.genLotTabPage.Name = "genLotTabPage";
            this.genLotTabPage.Size = new System.Drawing.Size(970, 640);
            this.genLotTabPage.TabIndex = 0;
            this.genLotTabPage.Text = "สร้าง Lot";
            this.genLotTabPage.VerticalScrollbarBarColor = true;
            this.genLotTabPage.VerticalScrollbarHighlightOnWheel = false;
            this.genLotTabPage.VerticalScrollbarSize = 10;
            // 
            // genLotPanel1
            // 
            this.genLotPanel1.Controls.Add(this.metroLabel38);
            this.genLotPanel1.Controls.Add(this.metroButton15);
            this.genLotPanel1.Controls.Add(this.metroButton10);
            this.genLotPanel1.Controls.Add(this.metroLabel35);
            this.genLotPanel1.Controls.Add(this.metroLabel30);
            this.genLotPanel1.Controls.Add(this.metroTextBox26);
            this.genLotPanel1.HorizontalScrollbarBarColor = true;
            this.genLotPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.genLotPanel1.HorizontalScrollbarSize = 10;
            this.genLotPanel1.Location = new System.Drawing.Point(23, 13);
            this.genLotPanel1.Name = "genLotPanel1";
            this.genLotPanel1.Size = new System.Drawing.Size(805, 234);
            this.genLotPanel1.TabIndex = 20;
            this.genLotPanel1.VerticalScrollbarBarColor = true;
            this.genLotPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.genLotPanel1.VerticalScrollbarSize = 10;
            // 
            // metroLabel38
            // 
            this.metroLabel38.AutoSize = true;
            this.metroLabel38.Location = new System.Drawing.Point(187, 30);
            this.metroLabel38.Name = "metroLabel38";
            this.metroLabel38.Size = new System.Drawing.Size(564, 19);
            this.metroLabel38.TabIndex = 36;
            this.metroLabel38.Text = "กรุณาระบุหมายเลข PO  หรือเลือกยิงบาร์โค้ดของหมายเลขใบโอนถ่ายสินค้า เพื่อเข้าสู่หน" +
    "้าใบสรุปการแตก Lot";
            // 
            // metroButton15
            // 
            this.metroButton15.Location = new System.Drawing.Point(607, 88);
            this.metroButton15.Name = "metroButton15";
            this.metroButton15.Size = new System.Drawing.Size(67, 23);
            this.metroButton15.TabIndex = 35;
            this.metroButton15.Text = "ยืนยัน";
            this.metroButton15.UseSelectable = true;
            // 
            // metroButton10
            // 
            this.metroButton10.Location = new System.Drawing.Point(375, 186);
            this.metroButton10.Name = "metroButton10";
            this.metroButton10.Size = new System.Drawing.Size(214, 30);
            this.metroButton10.TabIndex = 34;
            this.metroButton10.Text = "ยิงบาร์โค้ดของหมายเลขใบโอนถ่ายสินค้า ";
            this.metroButton10.UseSelectable = true;
            // 
            // metroLabel35
            // 
            this.metroLabel35.AutoSize = true;
            this.metroLabel35.Location = new System.Drawing.Point(462, 139);
            this.metroLabel35.Name = "metroLabel35";
            this.metroLabel35.Size = new System.Drawing.Size(30, 19);
            this.metroLabel35.TabIndex = 8;
            this.metroLabel35.Text = "หรือ";
            // 
            // metroLabel30
            // 
            this.metroLabel30.AutoSize = true;
            this.metroLabel30.Location = new System.Drawing.Point(255, 88);
            this.metroLabel30.Name = "metroLabel30";
            this.metroLabel30.Size = new System.Drawing.Size(100, 19);
            this.metroLabel30.TabIndex = 6;
            this.metroLabel30.Text = "ระบุหมายเลข PO";
            // 
            // metroTextBox26
            // 
            // 
            // 
            // 
            this.metroTextBox26.CustomButton.Image = null;
            this.metroTextBox26.CustomButton.Location = new System.Drawing.Point(192, 1);
            this.metroTextBox26.CustomButton.Name = "";
            this.metroTextBox26.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox26.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox26.CustomButton.TabIndex = 1;
            this.metroTextBox26.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox26.CustomButton.UseSelectable = true;
            this.metroTextBox26.CustomButton.Visible = false;
            this.metroTextBox26.Lines = new string[] {
        "TRL-1907-001"};
            this.metroTextBox26.Location = new System.Drawing.Point(375, 88);
            this.metroTextBox26.MaxLength = 32767;
            this.metroTextBox26.Name = "metroTextBox26";
            this.metroTextBox26.PasswordChar = '\0';
            this.metroTextBox26.ReadOnly = true;
            this.metroTextBox26.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox26.SelectedText = "";
            this.metroTextBox26.SelectionLength = 0;
            this.metroTextBox26.SelectionStart = 0;
            this.metroTextBox26.ShortcutsEnabled = true;
            this.metroTextBox26.Size = new System.Drawing.Size(214, 23);
            this.metroTextBox26.TabIndex = 7;
            this.metroTextBox26.Text = "TRL-1907-001";
            this.metroTextBox26.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.metroTextBox26.UseSelectable = true;
            this.metroTextBox26.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox26.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // genLotPanel2
            // 
            this.genLotPanel2.Controls.Add(this.metroPanel7);
            this.genLotPanel2.Controls.Add(this.metroTextBox25);
            this.genLotPanel2.Controls.Add(this.metroTextBox24);
            this.genLotPanel2.Controls.Add(this.metroTextBox23);
            this.genLotPanel2.Controls.Add(this.metroTextBox20);
            this.genLotPanel2.Controls.Add(this.metroTextBox18);
            this.genLotPanel2.Controls.Add(this.metroLabel29);
            this.genLotPanel2.Controls.Add(this.pictureBox4);
            this.genLotPanel2.Controls.Add(this.pictureBox3);
            this.genLotPanel2.Controls.Add(this.metroPanel4);
            this.genLotPanel2.Controls.Add(this.metroButton9);
            this.genLotPanel2.Controls.Add(this.metroButton8);
            this.genLotPanel2.Controls.Add(this.metroButton4);
            this.genLotPanel2.Controls.Add(this.metroButton5);
            this.genLotPanel2.Controls.Add(this.metroButton6);
            this.genLotPanel2.Controls.Add(this.metroButton7);
            this.genLotPanel2.Controls.Add(this.metroComboBox1);
            this.genLotPanel2.Controls.Add(this.metroComboBox2);
            this.genLotPanel2.Controls.Add(this.metroLabel6);
            this.genLotPanel2.Controls.Add(this.metroLabel14);
            this.genLotPanel2.Controls.Add(this.metroLabel7);
            this.genLotPanel2.Controls.Add(this.metroLabel8);
            this.genLotPanel2.Controls.Add(this.metroLabel15);
            this.genLotPanel2.Controls.Add(this.metroTextBox10);
            this.genLotPanel2.Controls.Add(this.metroTextBox5);
            this.genLotPanel2.Controls.Add(this.metroLabel13);
            this.genLotPanel2.Controls.Add(this.metroTextBox6);
            this.genLotPanel2.Controls.Add(this.metroLabel12);
            this.genLotPanel2.Controls.Add(this.metroLabel9);
            this.genLotPanel2.Controls.Add(this.metroLabel10);
            this.genLotPanel2.Controls.Add(this.metroLabel11);
            this.genLotPanel2.Controls.Add(this.metroLabel16);
            this.genLotPanel2.Controls.Add(this.metroTextBox7);
            this.genLotPanel2.Controls.Add(this.metroLabel17);
            this.genLotPanel2.Controls.Add(this.metroTextBox8);
            this.genLotPanel2.Controls.Add(this.metroLabel18);
            this.genLotPanel2.Controls.Add(this.metroTextBox9);
            this.genLotPanel2.Controls.Add(this.metroLabel19);
            this.genLotPanel2.Controls.Add(this.metroLabel20);
            this.genLotPanel2.HorizontalScrollbarBarColor = true;
            this.genLotPanel2.HorizontalScrollbarHighlightOnWheel = false;
            this.genLotPanel2.HorizontalScrollbarSize = 10;
            this.genLotPanel2.Location = new System.Drawing.Point(0, 277);
            this.genLotPanel2.Name = "genLotPanel2";
            this.genLotPanel2.Size = new System.Drawing.Size(944, 617);
            this.genLotPanel2.TabIndex = 33;
            this.genLotPanel2.VerticalScrollbarBarColor = true;
            this.genLotPanel2.VerticalScrollbarHighlightOnWheel = false;
            this.genLotPanel2.VerticalScrollbarSize = 10;
            // 
            // metroPanel7
            // 
            this.metroPanel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.metroPanel7.Controls.Add(this.metroPanel8);
            this.metroPanel7.Controls.Add(this.metroPanel9);
            this.metroPanel7.Controls.Add(this.metroLabel45);
            this.metroPanel7.HorizontalScrollbarBarColor = true;
            this.metroPanel7.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel7.HorizontalScrollbarSize = 10;
            this.metroPanel7.Location = new System.Drawing.Point(479, 313);
            this.metroPanel7.Name = "metroPanel7";
            this.metroPanel7.Size = new System.Drawing.Size(415, 61);
            this.metroPanel7.TabIndex = 102;
            this.metroPanel7.VerticalScrollbarBarColor = true;
            this.metroPanel7.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel7.VerticalScrollbarSize = 10;
            // 
            // metroPanel8
            // 
            this.metroPanel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.metroPanel8.Controls.Add(this.metroLabel37);
            this.metroPanel8.Controls.Add(this.metroTextBox21);
            this.metroPanel8.HorizontalScrollbarBarColor = true;
            this.metroPanel8.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel8.HorizontalScrollbarSize = 10;
            this.metroPanel8.Location = new System.Drawing.Point(196, 28);
            this.metroPanel8.Name = "metroPanel8";
            this.metroPanel8.Size = new System.Drawing.Size(218, 35);
            this.metroPanel8.TabIndex = 97;
            this.metroPanel8.VerticalScrollbarBarColor = true;
            this.metroPanel8.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel8.VerticalScrollbarSize = 10;
            // 
            // metroLabel37
            // 
            this.metroLabel37.AutoSize = true;
            this.metroLabel37.Location = new System.Drawing.Point(125, 4);
            this.metroLabel37.Name = "metroLabel37";
            this.metroLabel37.Size = new System.Drawing.Size(40, 19);
            this.metroLabel37.TabIndex = 100;
            this.metroLabel37.Text = "ปอนด์";
            // 
            // metroTextBox21
            // 
            // 
            // 
            // 
            this.metroTextBox21.CustomButton.Image = null;
            this.metroTextBox21.CustomButton.Location = new System.Drawing.Point(47, 1);
            this.metroTextBox21.CustomButton.Name = "";
            this.metroTextBox21.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox21.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox21.CustomButton.TabIndex = 1;
            this.metroTextBox21.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox21.CustomButton.UseSelectable = true;
            this.metroTextBox21.CustomButton.Visible = false;
            this.metroTextBox21.ForeColor = System.Drawing.Color.Gray;
            this.metroTextBox21.Lines = new string[] {
        "156.00"};
            this.metroTextBox21.Location = new System.Drawing.Point(55, 4);
            this.metroTextBox21.MaxLength = 32767;
            this.metroTextBox21.Name = "metroTextBox21";
            this.metroTextBox21.PasswordChar = '\0';
            this.metroTextBox21.ReadOnly = true;
            this.metroTextBox21.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox21.SelectedText = "";
            this.metroTextBox21.SelectionLength = 0;
            this.metroTextBox21.SelectionStart = 0;
            this.metroTextBox21.ShortcutsEnabled = true;
            this.metroTextBox21.Size = new System.Drawing.Size(69, 23);
            this.metroTextBox21.TabIndex = 99;
            this.metroTextBox21.Text = "156.00";
            this.metroTextBox21.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.metroTextBox21.UseCustomForeColor = true;
            this.metroTextBox21.UseSelectable = true;
            this.metroTextBox21.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox21.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroPanel9
            // 
            this.metroPanel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.metroPanel9.Controls.Add(this.metroTextBox22);
            this.metroPanel9.Controls.Add(this.metroLabel41);
            this.metroPanel9.HorizontalScrollbarBarColor = true;
            this.metroPanel9.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel9.HorizontalScrollbarSize = 10;
            this.metroPanel9.Location = new System.Drawing.Point(-1, 28);
            this.metroPanel9.Name = "metroPanel9";
            this.metroPanel9.Size = new System.Drawing.Size(203, 35);
            this.metroPanel9.TabIndex = 96;
            this.metroPanel9.VerticalScrollbarBarColor = true;
            this.metroPanel9.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel9.VerticalScrollbarSize = 10;
            // 
            // metroTextBox22
            // 
            // 
            // 
            // 
            this.metroTextBox22.CustomButton.Image = null;
            this.metroTextBox22.CustomButton.Location = new System.Drawing.Point(39, 1);
            this.metroTextBox22.CustomButton.Name = "";
            this.metroTextBox22.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox22.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox22.CustomButton.TabIndex = 1;
            this.metroTextBox22.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox22.CustomButton.UseSelectable = true;
            this.metroTextBox22.CustomButton.Visible = false;
            this.metroTextBox22.ForeColor = System.Drawing.Color.Gray;
            this.metroTextBox22.Lines = new string[] {
        "296.00"};
            this.metroTextBox22.Location = new System.Drawing.Point(36, 4);
            this.metroTextBox22.MaxLength = 32767;
            this.metroTextBox22.Name = "metroTextBox22";
            this.metroTextBox22.PasswordChar = '\0';
            this.metroTextBox22.ReadOnly = true;
            this.metroTextBox22.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox22.SelectedText = "";
            this.metroTextBox22.SelectionLength = 0;
            this.metroTextBox22.SelectionStart = 0;
            this.metroTextBox22.ShortcutsEnabled = true;
            this.metroTextBox22.Size = new System.Drawing.Size(61, 23);
            this.metroTextBox22.TabIndex = 96;
            this.metroTextBox22.Text = "296.00";
            this.metroTextBox22.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.metroTextBox22.UseCustomForeColor = true;
            this.metroTextBox22.UseSelectable = true;
            this.metroTextBox22.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox22.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel41
            // 
            this.metroLabel41.AutoSize = true;
            this.metroLabel41.Location = new System.Drawing.Point(99, 4);
            this.metroLabel41.Name = "metroLabel41";
            this.metroLabel41.Size = new System.Drawing.Size(40, 19);
            this.metroLabel41.TabIndex = 97;
            this.metroLabel41.Text = "ปอนด์";
            // 
            // metroLabel45
            // 
            this.metroLabel45.AutoSize = true;
            this.metroLabel45.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel45.Location = new System.Drawing.Point(162, 5);
            this.metroLabel45.Name = "metroLabel45";
            this.metroLabel45.Size = new System.Drawing.Size(77, 19);
            this.metroLabel45.TabIndex = 95;
            this.metroLabel45.Text = "น.น.รวม";
            // 
            // metroTextBox25
            // 
            // 
            // 
            // 
            this.metroTextBox25.CustomButton.Image = null;
            this.metroTextBox25.CustomButton.Location = new System.Drawing.Point(49, 1);
            this.metroTextBox25.CustomButton.Name = "";
            this.metroTextBox25.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox25.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox25.CustomButton.TabIndex = 1;
            this.metroTextBox25.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox25.CustomButton.UseSelectable = true;
            this.metroTextBox25.CustomButton.Visible = false;
            this.metroTextBox25.ForeColor = System.Drawing.Color.Gray;
            this.metroTextBox25.Lines = new string[] {
        "(38.80)"};
            this.metroTextBox25.Location = new System.Drawing.Point(729, 157);
            this.metroTextBox25.MaxLength = 32767;
            this.metroTextBox25.Name = "metroTextBox25";
            this.metroTextBox25.PasswordChar = '\0';
            this.metroTextBox25.ReadOnly = true;
            this.metroTextBox25.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox25.SelectedText = "";
            this.metroTextBox25.SelectionLength = 0;
            this.metroTextBox25.SelectionStart = 0;
            this.metroTextBox25.ShortcutsEnabled = true;
            this.metroTextBox25.Size = new System.Drawing.Size(71, 23);
            this.metroTextBox25.TabIndex = 51;
            this.metroTextBox25.Text = "(38.80)";
            this.metroTextBox25.UseCustomForeColor = true;
            this.metroTextBox25.UseSelectable = true;
            this.metroTextBox25.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox25.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBox24
            // 
            // 
            // 
            // 
            this.metroTextBox24.CustomButton.Image = null;
            this.metroTextBox24.CustomButton.Location = new System.Drawing.Point(49, 1);
            this.metroTextBox24.CustomButton.Name = "";
            this.metroTextBox24.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox24.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox24.CustomButton.TabIndex = 1;
            this.metroTextBox24.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox24.CustomButton.UseSelectable = true;
            this.metroTextBox24.CustomButton.Visible = false;
            this.metroTextBox24.ForeColor = System.Drawing.Color.Gray;
            this.metroTextBox24.Lines = new string[] {
        "150,538.80"};
            this.metroTextBox24.Location = new System.Drawing.Point(729, 132);
            this.metroTextBox24.MaxLength = 32767;
            this.metroTextBox24.Name = "metroTextBox24";
            this.metroTextBox24.PasswordChar = '\0';
            this.metroTextBox24.ReadOnly = true;
            this.metroTextBox24.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox24.SelectedText = "";
            this.metroTextBox24.SelectionLength = 0;
            this.metroTextBox24.SelectionStart = 0;
            this.metroTextBox24.ShortcutsEnabled = true;
            this.metroTextBox24.Size = new System.Drawing.Size(71, 23);
            this.metroTextBox24.TabIndex = 50;
            this.metroTextBox24.Text = "150,538.80";
            this.metroTextBox24.UseCustomForeColor = true;
            this.metroTextBox24.UseSelectable = true;
            this.metroTextBox24.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox24.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBox23
            // 
            // 
            // 
            // 
            this.metroTextBox23.CustomButton.Image = null;
            this.metroTextBox23.CustomButton.Location = new System.Drawing.Point(45, 1);
            this.metroTextBox23.CustomButton.Name = "";
            this.metroTextBox23.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox23.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox23.CustomButton.TabIndex = 1;
            this.metroTextBox23.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox23.CustomButton.UseSelectable = true;
            this.metroTextBox23.CustomButton.Visible = false;
            this.metroTextBox23.ForeColor = System.Drawing.Color.Gray;
            this.metroTextBox23.Lines = new string[] {
        "0.767"};
            this.metroTextBox23.Location = new System.Drawing.Point(844, 194);
            this.metroTextBox23.MaxLength = 32767;
            this.metroTextBox23.Name = "metroTextBox23";
            this.metroTextBox23.PasswordChar = '\0';
            this.metroTextBox23.ReadOnly = true;
            this.metroTextBox23.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox23.SelectedText = "";
            this.metroTextBox23.SelectionLength = 0;
            this.metroTextBox23.SelectionStart = 0;
            this.metroTextBox23.ShortcutsEnabled = true;
            this.metroTextBox23.Size = new System.Drawing.Size(67, 23);
            this.metroTextBox23.TabIndex = 48;
            this.metroTextBox23.Text = "0.767";
            this.metroTextBox23.UseCustomForeColor = true;
            this.metroTextBox23.UseSelectable = true;
            this.metroTextBox23.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox23.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBox20
            // 
            // 
            // 
            // 
            this.metroTextBox20.CustomButton.Image = null;
            this.metroTextBox20.CustomButton.Location = new System.Drawing.Point(204, 1);
            this.metroTextBox20.CustomButton.Name = "";
            this.metroTextBox20.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox20.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox20.CustomButton.TabIndex = 1;
            this.metroTextBox20.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox20.CustomButton.UseSelectable = true;
            this.metroTextBox20.CustomButton.Visible = false;
            this.metroTextBox20.ForeColor = System.Drawing.Color.Gray;
            this.metroTextBox20.Lines = new string[] {
        "RM-AUS-SM"};
            this.metroTextBox20.Location = new System.Drawing.Point(180, 132);
            this.metroTextBox20.MaxLength = 32767;
            this.metroTextBox20.Name = "metroTextBox20";
            this.metroTextBox20.PasswordChar = '\0';
            this.metroTextBox20.ReadOnly = true;
            this.metroTextBox20.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox20.SelectedText = "";
            this.metroTextBox20.SelectionLength = 0;
            this.metroTextBox20.SelectionStart = 0;
            this.metroTextBox20.ShortcutsEnabled = true;
            this.metroTextBox20.Size = new System.Drawing.Size(226, 23);
            this.metroTextBox20.TabIndex = 47;
            this.metroTextBox20.Text = "RM-AUS-SM";
            this.metroTextBox20.UseCustomForeColor = true;
            this.metroTextBox20.UseSelectable = true;
            this.metroTextBox20.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox20.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBox18
            // 
            // 
            // 
            // 
            this.metroTextBox18.CustomButton.Image = null;
            this.metroTextBox18.CustomButton.Location = new System.Drawing.Point(129, 1);
            this.metroTextBox18.CustomButton.Name = "";
            this.metroTextBox18.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox18.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox18.CustomButton.TabIndex = 1;
            this.metroTextBox18.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox18.CustomButton.UseSelectable = true;
            this.metroTextBox18.CustomButton.Visible = false;
            this.metroTextBox18.ForeColor = System.Drawing.Color.Gray;
            this.metroTextBox18.Lines = new string[] {
        "1 สิงหาคม 2562"};
            this.metroTextBox18.Location = new System.Drawing.Point(760, 47);
            this.metroTextBox18.MaxLength = 32767;
            this.metroTextBox18.Name = "metroTextBox18";
            this.metroTextBox18.PasswordChar = '\0';
            this.metroTextBox18.ReadOnly = true;
            this.metroTextBox18.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox18.SelectedText = "";
            this.metroTextBox18.SelectionLength = 0;
            this.metroTextBox18.SelectionStart = 0;
            this.metroTextBox18.ShortcutsEnabled = true;
            this.metroTextBox18.Size = new System.Drawing.Size(151, 23);
            this.metroTextBox18.TabIndex = 46;
            this.metroTextBox18.Text = "1 สิงหาคม 2562";
            this.metroTextBox18.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.metroTextBox18.UseCustomForeColor = true;
            this.metroTextBox18.UseSelectable = true;
            this.metroTextBox18.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox18.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel29
            // 
            this.metroLabel29.AutoSize = true;
            this.metroLabel29.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel29.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel29.Location = new System.Drawing.Point(312, 16);
            this.metroLabel29.Name = "metroLabel29";
            this.metroLabel29.Size = new System.Drawing.Size(157, 25);
            this.metroLabel29.TabIndex = 45;
            this.metroLabel29.Text = "ใบสรุปการแตก Lot";
            // 
            // metroPanel4
            // 
            this.metroPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.metroPanel4.Controls.Add(this.metroCheckBox1);
            this.metroPanel4.Controls.Add(this.metroLabel22);
            this.metroPanel4.Controls.Add(this.metroTextBox11);
            this.metroPanel4.Controls.Add(this.metroTextBox12);
            this.metroPanel4.Controls.Add(this.metroLabel21);
            this.metroPanel4.HorizontalScrollbarBarColor = true;
            this.metroPanel4.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel4.HorizontalScrollbarSize = 10;
            this.metroPanel4.Location = new System.Drawing.Point(470, 601);
            this.metroPanel4.Name = "metroPanel4";
            this.metroPanel4.Size = new System.Drawing.Size(290, 33);
            this.metroPanel4.TabIndex = 42;
            this.metroPanel4.VerticalScrollbarBarColor = true;
            this.metroPanel4.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel4.VerticalScrollbarSize = 10;
            // 
            // metroCheckBox1
            // 
            this.metroCheckBox1.AutoSize = true;
            this.metroCheckBox1.Checked = true;
            this.metroCheckBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.metroCheckBox1.Location = new System.Drawing.Point(8, 8);
            this.metroCheckBox1.Name = "metroCheckBox1";
            this.metroCheckBox1.Size = new System.Drawing.Size(79, 15);
            this.metroCheckBox1.TabIndex = 18;
            this.metroCheckBox1.Text = "พิมพ์ทั้งหมด";
            this.metroCheckBox1.UseSelectable = true;
            // 
            // metroLabel22
            // 
            this.metroLabel22.AutoSize = true;
            this.metroLabel22.Location = new System.Drawing.Point(191, 6);
            this.metroLabel22.Name = "metroLabel22";
            this.metroLabel22.Size = new System.Drawing.Size(22, 19);
            this.metroLabel22.TabIndex = 22;
            this.metroLabel22.Text = "ถึง";
            // 
            // metroTextBox11
            // 
            // 
            // 
            // 
            this.metroTextBox11.CustomButton.Image = null;
            this.metroTextBox11.CustomButton.Location = new System.Drawing.Point(40, 1);
            this.metroTextBox11.CustomButton.Name = "";
            this.metroTextBox11.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox11.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox11.CustomButton.TabIndex = 1;
            this.metroTextBox11.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox11.CustomButton.UseSelectable = true;
            this.metroTextBox11.CustomButton.Visible = false;
            this.metroTextBox11.Lines = new string[] {
        "1"};
            this.metroTextBox11.Location = new System.Drawing.Point(126, 4);
            this.metroTextBox11.MaxLength = 32767;
            this.metroTextBox11.Name = "metroTextBox11";
            this.metroTextBox11.PasswordChar = '\0';
            this.metroTextBox11.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox11.SelectedText = "";
            this.metroTextBox11.SelectionLength = 0;
            this.metroTextBox11.SelectionStart = 0;
            this.metroTextBox11.ShortcutsEnabled = true;
            this.metroTextBox11.Size = new System.Drawing.Size(62, 23);
            this.metroTextBox11.TabIndex = 19;
            this.metroTextBox11.Text = "1";
            this.metroTextBox11.UseSelectable = true;
            this.metroTextBox11.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox11.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBox12
            // 
            // 
            // 
            // 
            this.metroTextBox12.CustomButton.Image = null;
            this.metroTextBox12.CustomButton.Location = new System.Drawing.Point(42, 1);
            this.metroTextBox12.CustomButton.Name = "";
            this.metroTextBox12.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox12.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox12.CustomButton.TabIndex = 1;
            this.metroTextBox12.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox12.CustomButton.UseSelectable = true;
            this.metroTextBox12.CustomButton.Visible = false;
            this.metroTextBox12.Lines = new string[] {
        "100"};
            this.metroTextBox12.Location = new System.Drawing.Point(219, 4);
            this.metroTextBox12.MaxLength = 32767;
            this.metroTextBox12.Name = "metroTextBox12";
            this.metroTextBox12.PasswordChar = '\0';
            this.metroTextBox12.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox12.SelectedText = "";
            this.metroTextBox12.SelectionLength = 0;
            this.metroTextBox12.SelectionStart = 0;
            this.metroTextBox12.ShortcutsEnabled = true;
            this.metroTextBox12.Size = new System.Drawing.Size(64, 23);
            this.metroTextBox12.TabIndex = 20;
            this.metroTextBox12.Text = "100";
            this.metroTextBox12.UseSelectable = true;
            this.metroTextBox12.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox12.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel21
            // 
            this.metroLabel21.AutoSize = true;
            this.metroLabel21.Location = new System.Drawing.Point(90, 6);
            this.metroLabel21.Name = "metroLabel21";
            this.metroLabel21.Size = new System.Drawing.Size(30, 19);
            this.metroLabel21.TabIndex = 21;
            this.metroLabel21.Text = "จาก";
            // 
            // metroButton8
            // 
            this.metroButton8.Location = new System.Drawing.Point(806, 603);
            this.metroButton8.Name = "metroButton8";
            this.metroButton8.Size = new System.Drawing.Size(105, 30);
            this.metroButton8.TabIndex = 40;
            this.metroButton8.Text = "ออกจากหน้านี้";
            this.metroButton8.UseSelectable = true;
            // 
            // metroButton4
            // 
            this.metroButton4.Location = new System.Drawing.Point(368, 603);
            this.metroButton4.Name = "metroButton4";
            this.metroButton4.Size = new System.Drawing.Size(101, 30);
            this.metroButton4.TabIndex = 38;
            this.metroButton4.Text = "พิมพ์บาร์โค้ด";
            this.metroButton4.UseSelectable = true;
            // 
            // metroButton5
            // 
            this.metroButton5.Location = new System.Drawing.Point(193, 603);
            this.metroButton5.Name = "metroButton5";
            this.metroButton5.Size = new System.Drawing.Size(122, 30);
            this.metroButton5.TabIndex = 37;
            this.metroButton5.Text = "พิมพ์แบบฟอร์มนี้";
            this.metroButton5.UseSelectable = true;
            // 
            // metroButton6
            // 
            this.metroButton6.Location = new System.Drawing.Point(16, 603);
            this.metroButton6.Name = "metroButton6";
            this.metroButton6.Size = new System.Drawing.Size(127, 30);
            this.metroButton6.TabIndex = 36;
            this.metroButton6.Text = "บันทึกข้อมูลลงระบบ";
            this.metroButton6.UseSelectable = true;
            // 
            // metroButton7
            // 
            this.metroButton7.Location = new System.Drawing.Point(778, 268);
            this.metroButton7.Name = "metroButton7";
            this.metroButton7.Size = new System.Drawing.Size(133, 30);
            this.metroButton7.TabIndex = 33;
            this.metroButton7.Text = "สร้างหมายเลข Lot";
            this.metroButton7.UseSelectable = true;
            // 
            // metroComboBox1
            // 
            this.metroComboBox1.FormattingEnabled = true;
            this.metroComboBox1.ItemHeight = 23;
            this.metroComboBox1.Items.AddRange(new object[] {
            "SQC",
            "SRM",
            "SPD2",
            "SPD4",
            "SFG"});
            this.metroComboBox1.Location = new System.Drawing.Point(180, 224);
            this.metroComboBox1.Name = "metroComboBox1";
            this.metroComboBox1.Size = new System.Drawing.Size(226, 29);
            this.metroComboBox1.TabIndex = 30;
            this.metroComboBox1.UseSelectable = true;
            // 
            // metroComboBox2
            // 
            this.metroComboBox2.FormattingEnabled = true;
            this.metroComboBox2.ItemHeight = 23;
            this.metroComboBox2.Items.AddRange(new object[] {
            "SQC",
            "SRM",
            "SPD2",
            "SPD4",
            "SFG"});
            this.metroComboBox2.Location = new System.Drawing.Point(619, 224);
            this.metroComboBox2.Name = "metroComboBox2";
            this.metroComboBox2.Size = new System.Drawing.Size(292, 29);
            this.metroComboBox2.TabIndex = 31;
            this.metroComboBox2.UseSelectable = true;
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(615, 16);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(139, 19);
            this.metroLabel6.TabIndex = 0;
            this.metroLabel6.Text = "หมายเลขใบโอนถ่ายสินค้า";
            // 
            // metroLabel14
            // 
            this.metroLabel14.AutoSize = true;
            this.metroLabel14.Location = new System.Drawing.Point(731, 194);
            this.metroLabel14.Name = "metroLabel14";
            this.metroLabel14.Size = new System.Drawing.Size(116, 19);
            this.metroLabel14.TabIndex = 29;
            this.metroLabel14.Text = "น.น.ต่อ 1 Tare = ";
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(615, 47);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(31, 19);
            this.metroLabel7.TabIndex = 1;
            this.metroLabel7.Text = "วันที่";
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Location = new System.Drawing.Point(615, 78);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(88, 19);
            this.metroLabel8.TabIndex = 2;
            this.metroLabel8.Text = "หมายเลขอ้างอิง";
            // 
            // metroLabel15
            // 
            this.metroLabel15.AutoSize = true;
            this.metroLabel15.Location = new System.Drawing.Point(694, 194);
            this.metroLabel15.Name = "metroLabel15";
            this.metroLabel15.Size = new System.Drawing.Size(40, 19);
            this.metroLabel15.TabIndex = 27;
            this.metroLabel15.Text = "ปอนด์";
            // 
            // metroTextBox10
            // 
            // 
            // 
            // 
            this.metroTextBox10.CustomButton.Image = null;
            this.metroTextBox10.CustomButton.Location = new System.Drawing.Point(47, 1);
            this.metroTextBox10.CustomButton.Name = "";
            this.metroTextBox10.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox10.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox10.CustomButton.TabIndex = 1;
            this.metroTextBox10.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox10.CustomButton.UseSelectable = true;
            this.metroTextBox10.CustomButton.Visible = false;
            this.metroTextBox10.Lines = new string[] {
        "230"};
            this.metroTextBox10.Location = new System.Drawing.Point(619, 194);
            this.metroTextBox10.MaxLength = 32767;
            this.metroTextBox10.Name = "metroTextBox10";
            this.metroTextBox10.PasswordChar = '\0';
            this.metroTextBox10.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox10.SelectedText = "";
            this.metroTextBox10.SelectionLength = 0;
            this.metroTextBox10.SelectionStart = 0;
            this.metroTextBox10.ShortcutsEnabled = true;
            this.metroTextBox10.Size = new System.Drawing.Size(69, 23);
            this.metroTextBox10.TabIndex = 26;
            this.metroTextBox10.Text = "230";
            this.metroTextBox10.UseSelectable = true;
            this.metroTextBox10.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox10.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBox5
            // 
            // 
            // 
            // 
            this.metroTextBox5.CustomButton.Image = null;
            this.metroTextBox5.CustomButton.Location = new System.Drawing.Point(129, 1);
            this.metroTextBox5.CustomButton.Name = "";
            this.metroTextBox5.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox5.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox5.CustomButton.TabIndex = 1;
            this.metroTextBox5.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox5.CustomButton.UseSelectable = true;
            this.metroTextBox5.CustomButton.Visible = false;
            this.metroTextBox5.Lines = new string[0];
            this.metroTextBox5.Location = new System.Drawing.Point(760, 78);
            this.metroTextBox5.MaxLength = 32767;
            this.metroTextBox5.Name = "metroTextBox5";
            this.metroTextBox5.PasswordChar = '\0';
            this.metroTextBox5.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox5.SelectedText = "";
            this.metroTextBox5.SelectionLength = 0;
            this.metroTextBox5.SelectionStart = 0;
            this.metroTextBox5.ShortcutsEnabled = true;
            this.metroTextBox5.Size = new System.Drawing.Size(151, 23);
            this.metroTextBox5.TabIndex = 4;
            this.metroTextBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.metroTextBox5.UseSelectable = true;
            this.metroTextBox5.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox5.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel13
            // 
            this.metroLabel13.AutoSize = true;
            this.metroLabel13.Location = new System.Drawing.Point(455, 194);
            this.metroLabel13.Name = "metroLabel13";
            this.metroLabel13.Size = new System.Drawing.Size(136, 19);
            this.metroLabel13.TabIndex = 25;
            this.metroLabel13.Text = "น.น. Tare รวมทั้งหมด";
            // 
            // metroTextBox6
            // 
            // 
            // 
            // 
            this.metroTextBox6.CustomButton.Image = null;
            this.metroTextBox6.CustomButton.Location = new System.Drawing.Point(129, 1);
            this.metroTextBox6.CustomButton.Name = "";
            this.metroTextBox6.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox6.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox6.CustomButton.TabIndex = 1;
            this.metroTextBox6.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox6.CustomButton.UseSelectable = true;
            this.metroTextBox6.CustomButton.Visible = false;
            this.metroTextBox6.ForeColor = System.Drawing.Color.Gray;
            this.metroTextBox6.Lines = new string[] {
        "TRL-1907-001"};
            this.metroTextBox6.Location = new System.Drawing.Point(760, 16);
            this.metroTextBox6.MaxLength = 32767;
            this.metroTextBox6.Name = "metroTextBox6";
            this.metroTextBox6.PasswordChar = '\0';
            this.metroTextBox6.ReadOnly = true;
            this.metroTextBox6.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox6.SelectedText = "";
            this.metroTextBox6.SelectionLength = 0;
            this.metroTextBox6.SelectionStart = 0;
            this.metroTextBox6.ShortcutsEnabled = true;
            this.metroTextBox6.Size = new System.Drawing.Size(151, 23);
            this.metroTextBox6.TabIndex = 5;
            this.metroTextBox6.Text = "TRL-1907-001";
            this.metroTextBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.metroTextBox6.UseCustomForeColor = true;
            this.metroTextBox6.UseSelectable = true;
            this.metroTextBox6.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox6.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel12
            // 
            this.metroLabel12.AutoSize = true;
            this.metroLabel12.Location = new System.Drawing.Point(694, 157);
            this.metroLabel12.Name = "metroLabel12";
            this.metroLabel12.Size = new System.Drawing.Size(34, 19);
            this.metroLabel12.TabIndex = 24;
            this.metroLabel12.Text = "Diffs";
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.Location = new System.Drawing.Point(16, 132);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(61, 19);
            this.metroLabel9.TabIndex = 6;
            this.metroLabel9.Text = "รหัสสินค้า";
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.Location = new System.Drawing.Point(455, 132);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(44, 19);
            this.metroLabel10.TabIndex = 7;
            this.metroLabel10.Text = "จำนวน";
            // 
            // metroLabel11
            // 
            this.metroLabel11.AutoSize = true;
            this.metroLabel11.Location = new System.Drawing.Point(799, 132);
            this.metroLabel11.Name = "metroLabel11";
            this.metroLabel11.Size = new System.Drawing.Size(40, 19);
            this.metroLabel11.TabIndex = 22;
            this.metroLabel11.Text = "ปอนด์";
            // 
            // metroLabel16
            // 
            this.metroLabel16.AutoSize = true;
            this.metroLabel16.Location = new System.Drawing.Point(16, 163);
            this.metroLabel16.Name = "metroLabel16";
            this.metroLabel16.Size = new System.Drawing.Size(80, 19);
            this.metroLabel16.TabIndex = 8;
            this.metroLabel16.Text = "หมายเลข Lot";
            // 
            // metroTextBox7
            // 
            // 
            // 
            // 
            this.metroTextBox7.CustomButton.Image = null;
            this.metroTextBox7.CustomButton.Location = new System.Drawing.Point(204, 1);
            this.metroTextBox7.CustomButton.Name = "";
            this.metroTextBox7.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox7.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox7.CustomButton.TabIndex = 1;
            this.metroTextBox7.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox7.CustomButton.UseSelectable = true;
            this.metroTextBox7.CustomButton.Visible = false;
            this.metroTextBox7.Enabled = false;
            this.metroTextBox7.ForeColor = System.Drawing.Color.Gray;
            this.metroTextBox7.Lines = new string[] {
        "US-F-SM-620706"};
            this.metroTextBox7.Location = new System.Drawing.Point(180, 163);
            this.metroTextBox7.MaxLength = 32767;
            this.metroTextBox7.Name = "metroTextBox7";
            this.metroTextBox7.PasswordChar = '\0';
            this.metroTextBox7.ReadOnly = true;
            this.metroTextBox7.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox7.SelectedText = "";
            this.metroTextBox7.SelectionLength = 0;
            this.metroTextBox7.SelectionStart = 0;
            this.metroTextBox7.ShortcutsEnabled = true;
            this.metroTextBox7.Size = new System.Drawing.Size(226, 23);
            this.metroTextBox7.TabIndex = 10;
            this.metroTextBox7.Tag = "";
            this.metroTextBox7.Text = "US-F-SM-620706";
            this.metroTextBox7.UseCustomForeColor = true;
            this.metroTextBox7.UseSelectable = true;
            this.metroTextBox7.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox7.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel17
            // 
            this.metroLabel17.AutoSize = true;
            this.metroLabel17.Location = new System.Drawing.Point(694, 132);
            this.metroLabel17.Name = "metroLabel17";
            this.metroLabel17.Size = new System.Drawing.Size(29, 19);
            this.metroLabel17.TabIndex = 20;
            this.metroLabel17.Text = "เบล";
            // 
            // metroTextBox8
            // 
            // 
            // 
            // 
            this.metroTextBox8.CustomButton.Image = null;
            this.metroTextBox8.CustomButton.Location = new System.Drawing.Point(49, 1);
            this.metroTextBox8.CustomButton.Name = "";
            this.metroTextBox8.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox8.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox8.CustomButton.TabIndex = 1;
            this.metroTextBox8.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox8.CustomButton.UseSelectable = true;
            this.metroTextBox8.CustomButton.Visible = false;
            this.metroTextBox8.Lines = new string[] {
        "300"};
            this.metroTextBox8.Location = new System.Drawing.Point(619, 132);
            this.metroTextBox8.MaxLength = 32767;
            this.metroTextBox8.Name = "metroTextBox8";
            this.metroTextBox8.PasswordChar = '\0';
            this.metroTextBox8.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox8.SelectedText = "";
            this.metroTextBox8.SelectionLength = 0;
            this.metroTextBox8.SelectionStart = 0;
            this.metroTextBox8.ShortcutsEnabled = true;
            this.metroTextBox8.Size = new System.Drawing.Size(71, 23);
            this.metroTextBox8.TabIndex = 18;
            this.metroTextBox8.Text = "300";
            this.metroTextBox8.UseSelectable = true;
            this.metroTextBox8.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox8.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel18
            // 
            this.metroLabel18.AutoSize = true;
            this.metroLabel18.Location = new System.Drawing.Point(16, 228);
            this.metroLabel18.Name = "metroLabel18";
            this.metroLabel18.Size = new System.Drawing.Size(55, 19);
            this.metroLabel18.TabIndex = 12;
            this.metroLabel18.Text = "จากโกดัง";
            // 
            // metroTextBox9
            // 
            // 
            // 
            // 
            this.metroTextBox9.CustomButton.Image = null;
            this.metroTextBox9.CustomButton.Location = new System.Drawing.Point(204, 1);
            this.metroTextBox9.CustomButton.Name = "";
            this.metroTextBox9.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox9.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox9.CustomButton.TabIndex = 1;
            this.metroTextBox9.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox9.CustomButton.UseSelectable = true;
            this.metroTextBox9.CustomButton.Visible = false;
            this.metroTextBox9.ForeColor = System.Drawing.Color.Gray;
            this.metroTextBox9.Lines = new string[] {
        "L=1 1/8,M=3.5-4.9,NEP<450,SEED<105"};
            this.metroTextBox9.Location = new System.Drawing.Point(180, 194);
            this.metroTextBox9.MaxLength = 32767;
            this.metroTextBox9.Name = "metroTextBox9";
            this.metroTextBox9.PasswordChar = '\0';
            this.metroTextBox9.ReadOnly = true;
            this.metroTextBox9.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox9.SelectedText = "";
            this.metroTextBox9.SelectionLength = 0;
            this.metroTextBox9.SelectionStart = 0;
            this.metroTextBox9.ShortcutsEnabled = true;
            this.metroTextBox9.Size = new System.Drawing.Size(226, 23);
            this.metroTextBox9.TabIndex = 16;
            this.metroTextBox9.Text = "L=1 1/8,M=3.5-4.9,NEP<450,SEED<105";
            this.metroTextBox9.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox9.UseCustomForeColor = true;
            this.metroTextBox9.UseSelectable = true;
            this.metroTextBox9.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox9.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel19
            // 
            this.metroLabel19.AutoSize = true;
            this.metroLabel19.Location = new System.Drawing.Point(455, 228);
            this.metroLabel19.Name = "metroLabel19";
            this.metroLabel19.Size = new System.Drawing.Size(58, 19);
            this.metroLabel19.TabIndex = 13;
            this.metroLabel19.Text = "ไปที่โกดัง";
            // 
            // metroLabel20
            // 
            this.metroLabel20.AutoSize = true;
            this.metroLabel20.Location = new System.Drawing.Point(16, 195);
            this.metroLabel20.Name = "metroLabel20";
            this.metroLabel20.Size = new System.Drawing.Size(145, 19);
            this.metroLabel20.TabIndex = 14;
            this.metroLabel20.Text = "คุณสมบัติของสินค้า Lot นี้";
            // 
            // issueTabPage
            // 
            this.issueTabPage.Controls.Add(this.issuePanel1);
            this.issueTabPage.Controls.Add(this.issuePanel2);
            this.issueTabPage.HorizontalScrollbarBarColor = true;
            this.issueTabPage.HorizontalScrollbarHighlightOnWheel = false;
            this.issueTabPage.HorizontalScrollbarSize = 10;
            this.issueTabPage.Location = new System.Drawing.Point(4, 38);
            this.issueTabPage.Name = "issueTabPage";
            this.issueTabPage.Size = new System.Drawing.Size(970, 640);
            this.issueTabPage.TabIndex = 3;
            this.issueTabPage.Text = "ผลิต/เบิก";
            this.issueTabPage.VerticalScrollbarBarColor = true;
            this.issueTabPage.VerticalScrollbarHighlightOnWheel = false;
            this.issueTabPage.VerticalScrollbarSize = 10;
            // 
            // issuePanel1
            // 
            this.issuePanel1.Controls.Add(this.metroLabel39);
            this.issuePanel1.Controls.Add(this.metroButton16);
            this.issuePanel1.Controls.Add(this.metroButton17);
            this.issuePanel1.Controls.Add(this.metroLabel42);
            this.issuePanel1.Controls.Add(this.metroLabel43);
            this.issuePanel1.Controls.Add(this.metroTextBox27);
            this.issuePanel1.HorizontalScrollbarBarColor = true;
            this.issuePanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.issuePanel1.HorizontalScrollbarSize = 10;
            this.issuePanel1.Location = new System.Drawing.Point(4, 13);
            this.issuePanel1.Name = "issuePanel1";
            this.issuePanel1.Size = new System.Drawing.Size(805, 234);
            this.issuePanel1.TabIndex = 35;
            this.issuePanel1.VerticalScrollbarBarColor = true;
            this.issuePanel1.VerticalScrollbarHighlightOnWheel = false;
            this.issuePanel1.VerticalScrollbarSize = 10;
            // 
            // metroLabel39
            // 
            this.metroLabel39.AutoSize = true;
            this.metroLabel39.Location = new System.Drawing.Point(187, 30);
            this.metroLabel39.Name = "metroLabel39";
            this.metroLabel39.Size = new System.Drawing.Size(525, 19);
            this.metroLabel39.TabIndex = 36;
            this.metroLabel39.Text = "กรุณาระบุหมายเลขใบงานหรือเลือกยิงบาร์โค้ดของหมายเลขใบงาน เพื่อเข้าสู่หน้าใบสั่งผล" +
    "ิต/เบิกวัตถุดิบ";
            // 
            // metroButton16
            // 
            this.metroButton16.Location = new System.Drawing.Point(607, 88);
            this.metroButton16.Name = "metroButton16";
            this.metroButton16.Size = new System.Drawing.Size(67, 23);
            this.metroButton16.TabIndex = 35;
            this.metroButton16.Text = "ยืนยัน";
            this.metroButton16.UseSelectable = true;
            // 
            // metroButton17
            // 
            this.metroButton17.Location = new System.Drawing.Point(375, 186);
            this.metroButton17.Name = "metroButton17";
            this.metroButton17.Size = new System.Drawing.Size(214, 30);
            this.metroButton17.TabIndex = 34;
            this.metroButton17.Text = "ยิงบาร์โค้ดของหมายเลขใบงาน";
            this.metroButton17.UseSelectable = true;
            // 
            // metroLabel42
            // 
            this.metroLabel42.AutoSize = true;
            this.metroLabel42.Location = new System.Drawing.Point(462, 139);
            this.metroLabel42.Name = "metroLabel42";
            this.metroLabel42.Size = new System.Drawing.Size(30, 19);
            this.metroLabel42.TabIndex = 8;
            this.metroLabel42.Text = "หรือ";
            // 
            // metroLabel43
            // 
            this.metroLabel43.AutoSize = true;
            this.metroLabel43.Location = new System.Drawing.Point(255, 88);
            this.metroLabel43.Name = "metroLabel43";
            this.metroLabel43.Size = new System.Drawing.Size(107, 19);
            this.metroLabel43.TabIndex = 6;
            this.metroLabel43.Text = "ระบุหมายเลขใบงาน";
            // 
            // metroTextBox27
            // 
            // 
            // 
            // 
            this.metroTextBox27.CustomButton.Image = null;
            this.metroTextBox27.CustomButton.Location = new System.Drawing.Point(192, 1);
            this.metroTextBox27.CustomButton.Name = "";
            this.metroTextBox27.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox27.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox27.CustomButton.TabIndex = 1;
            this.metroTextBox27.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox27.CustomButton.UseSelectable = true;
            this.metroTextBox27.CustomButton.Visible = false;
            this.metroTextBox27.Lines = new string[] {
        "119070001"};
            this.metroTextBox27.Location = new System.Drawing.Point(375, 88);
            this.metroTextBox27.MaxLength = 32767;
            this.metroTextBox27.Name = "metroTextBox27";
            this.metroTextBox27.PasswordChar = '\0';
            this.metroTextBox27.ReadOnly = true;
            this.metroTextBox27.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox27.SelectedText = "";
            this.metroTextBox27.SelectionLength = 0;
            this.metroTextBox27.SelectionStart = 0;
            this.metroTextBox27.ShortcutsEnabled = true;
            this.metroTextBox27.Size = new System.Drawing.Size(214, 23);
            this.metroTextBox27.TabIndex = 7;
            this.metroTextBox27.Text = "119070001";
            this.metroTextBox27.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.metroTextBox27.UseSelectable = true;
            this.metroTextBox27.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox27.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // issuePanel2
            // 
            this.issuePanel2.Controls.Add(this.metroButton19);
            this.issuePanel2.Controls.Add(this.metroButton18);
            this.issuePanel2.Controls.Add(this.pictureBox5);
            this.issuePanel2.Controls.Add(this.pictureBox6);
            this.issuePanel2.Controls.Add(this.metroLabel33);
            this.issuePanel2.Controls.Add(this.metroLabel32);
            this.issuePanel2.Controls.Add(this.metroTextBox19);
            this.issuePanel2.Controls.Add(this.metroLabel31);
            this.issuePanel2.Controls.Add(this.metroTextBox17);
            this.issuePanel2.Controls.Add(this.metroLabel28);
            this.issuePanel2.Controls.Add(this.metroTextBox15);
            this.issuePanel2.Controls.Add(this.metroTextBox16);
            this.issuePanel2.Controls.Add(this.metroTextBox28);
            this.issuePanel2.Controls.Add(this.metroTextBox29);
            this.issuePanel2.Controls.Add(this.metroTextBox30);
            this.issuePanel2.Controls.Add(this.metroTextBox31);
            this.issuePanel2.Controls.Add(this.metroTextBox32);
            this.issuePanel2.Controls.Add(this.metroLabel44);
            this.issuePanel2.Controls.Add(this.metroComboBox3);
            this.issuePanel2.Controls.Add(this.metroLabel46);
            this.issuePanel2.Controls.Add(this.metroLabel48);
            this.issuePanel2.Controls.Add(this.metroLabel49);
            this.issuePanel2.Controls.Add(this.metroTextBox33);
            this.issuePanel2.Controls.Add(this.metroTextBox34);
            this.issuePanel2.Controls.Add(this.metroLabel50);
            this.issuePanel2.Controls.Add(this.metroTextBox35);
            this.issuePanel2.Controls.Add(this.metroLabel51);
            this.issuePanel2.Controls.Add(this.metroLabel52);
            this.issuePanel2.Controls.Add(this.metroLabel53);
            this.issuePanel2.Controls.Add(this.metroLabel54);
            this.issuePanel2.Controls.Add(this.metroLabel55);
            this.issuePanel2.Controls.Add(this.metroTextBox36);
            this.issuePanel2.Controls.Add(this.metroLabel56);
            this.issuePanel2.Controls.Add(this.metroTextBox37);
            this.issuePanel2.Controls.Add(this.metroLabel57);
            this.issuePanel2.Controls.Add(this.metroLabel26);
            this.issuePanel2.Controls.Add(this.metroLabel40);
            this.issuePanel2.Controls.Add(this.metroButton11);
            this.issuePanel2.Controls.Add(this.metroButton13);
            this.issuePanel2.Controls.Add(this.metroButton14);
            this.issuePanel2.Controls.Add(this.metroLabel25);
            this.issuePanel2.Controls.Add(this.metroLabel27);
            this.issuePanel2.HorizontalScrollbarBarColor = true;
            this.issuePanel2.HorizontalScrollbarHighlightOnWheel = false;
            this.issuePanel2.HorizontalScrollbarSize = 10;
            this.issuePanel2.Location = new System.Drawing.Point(4, 253);
            this.issuePanel2.Name = "issuePanel2";
            this.issuePanel2.Size = new System.Drawing.Size(972, 651);
            this.issuePanel2.TabIndex = 34;
            this.issuePanel2.Tag = " ";
            this.issuePanel2.VerticalScrollbarBarColor = true;
            this.issuePanel2.VerticalScrollbarHighlightOnWheel = false;
            this.issuePanel2.VerticalScrollbarSize = 10;
            // 
            // metroButton19
            // 
            this.metroButton19.Location = new System.Drawing.Point(266, 513);
            this.metroButton19.Name = "metroButton19";
            this.metroButton19.Size = new System.Drawing.Size(147, 30);
            this.metroButton19.TabIndex = 95;
            this.metroButton19.Text = "ยิงบาร์โค้ดเพื่อเบิกสินค้า";
            this.metroButton19.UseSelectable = true;
            // 
            // metroButton18
            // 
            this.metroButton18.Location = new System.Drawing.Point(444, 513);
            this.metroButton18.Name = "metroButton18";
            this.metroButton18.Size = new System.Drawing.Size(147, 30);
            this.metroButton18.TabIndex = 94;
            this.metroButton18.Text = "ยิงบาร์โค้ดเพื่อรับสินค้า";
            this.metroButton18.UseSelectable = true;
            // 
            // metroLabel33
            // 
            this.metroLabel33.AutoSize = true;
            this.metroLabel33.Location = new System.Drawing.Point(728, 277);
            this.metroLabel33.Name = "metroLabel33";
            this.metroLabel33.Size = new System.Drawing.Size(40, 19);
            this.metroLabel33.TabIndex = 91;
            this.metroLabel33.Text = "ปอนด์";
            // 
            // metroLabel32
            // 
            this.metroLabel32.AutoSize = true;
            this.metroLabel32.Location = new System.Drawing.Point(728, 242);
            this.metroLabel32.Name = "metroLabel32";
            this.metroLabel32.Size = new System.Drawing.Size(40, 19);
            this.metroLabel32.TabIndex = 90;
            this.metroLabel32.Text = "ปอนด์";
            // 
            // metroTextBox19
            // 
            // 
            // 
            // 
            this.metroTextBox19.CustomButton.Image = null;
            this.metroTextBox19.CustomButton.Location = new System.Drawing.Point(49, 1);
            this.metroTextBox19.CustomButton.Name = "";
            this.metroTextBox19.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox19.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox19.CustomButton.TabIndex = 1;
            this.metroTextBox19.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox19.CustomButton.UseSelectable = true;
            this.metroTextBox19.CustomButton.Visible = false;
            this.metroTextBox19.Lines = new string[] {
        "0"};
            this.metroTextBox19.Location = new System.Drawing.Point(787, 242);
            this.metroTextBox19.MaxLength = 32767;
            this.metroTextBox19.Name = "metroTextBox19";
            this.metroTextBox19.PasswordChar = '\0';
            this.metroTextBox19.ReadOnly = true;
            this.metroTextBox19.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox19.SelectedText = "";
            this.metroTextBox19.SelectionLength = 0;
            this.metroTextBox19.SelectionStart = 0;
            this.metroTextBox19.ShortcutsEnabled = true;
            this.metroTextBox19.Size = new System.Drawing.Size(71, 23);
            this.metroTextBox19.TabIndex = 89;
            this.metroTextBox19.Text = "0";
            this.metroTextBox19.UseSelectable = true;
            this.metroTextBox19.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox19.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel31
            // 
            this.metroLabel31.AutoSize = true;
            this.metroLabel31.Location = new System.Drawing.Point(859, 242);
            this.metroLabel31.Name = "metroLabel31";
            this.metroLabel31.Size = new System.Drawing.Size(51, 19);
            this.metroLabel31.TabIndex = 88;
            this.metroLabel31.Text = "กระสอบ";
            // 
            // metroTextBox17
            // 
            // 
            // 
            // 
            this.metroTextBox17.CustomButton.Image = null;
            this.metroTextBox17.CustomButton.Location = new System.Drawing.Point(49, 1);
            this.metroTextBox17.CustomButton.Name = "";
            this.metroTextBox17.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox17.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox17.CustomButton.TabIndex = 1;
            this.metroTextBox17.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox17.CustomButton.UseSelectable = true;
            this.metroTextBox17.CustomButton.Visible = false;
            this.metroTextBox17.Lines = new string[] {
        "2,720.00"};
            this.metroTextBox17.Location = new System.Drawing.Point(651, 208);
            this.metroTextBox17.MaxLength = 32767;
            this.metroTextBox17.Name = "metroTextBox17";
            this.metroTextBox17.PasswordChar = '\0';
            this.metroTextBox17.ReadOnly = true;
            this.metroTextBox17.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox17.SelectedText = "";
            this.metroTextBox17.SelectionLength = 0;
            this.metroTextBox17.SelectionStart = 0;
            this.metroTextBox17.ShortcutsEnabled = true;
            this.metroTextBox17.Size = new System.Drawing.Size(71, 23);
            this.metroTextBox17.TabIndex = 87;
            this.metroTextBox17.Text = "2,720.00";
            this.metroTextBox17.UseSelectable = true;
            this.metroTextBox17.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox17.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel28
            // 
            this.metroLabel28.AutoSize = true;
            this.metroLabel28.Location = new System.Drawing.Point(859, 177);
            this.metroLabel28.Name = "metroLabel28";
            this.metroLabel28.Size = new System.Drawing.Size(51, 19);
            this.metroLabel28.TabIndex = 86;
            this.metroLabel28.Text = "กระสอบ";
            // 
            // metroTextBox15
            // 
            // 
            // 
            // 
            this.metroTextBox15.CustomButton.Image = null;
            this.metroTextBox15.CustomButton.Location = new System.Drawing.Point(204, 1);
            this.metroTextBox15.CustomButton.Name = "";
            this.metroTextBox15.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox15.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox15.CustomButton.TabIndex = 1;
            this.metroTextBox15.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox15.CustomButton.UseSelectable = true;
            this.metroTextBox15.CustomButton.Visible = false;
            this.metroTextBox15.Enabled = false;
            this.metroTextBox15.ForeColor = System.Drawing.Color.Gray;
            this.metroTextBox15.Lines = new string[] {
        "29 สิงหาคม 2562"};
            this.metroTextBox15.Location = new System.Drawing.Point(156, 277);
            this.metroTextBox15.MaxLength = 32767;
            this.metroTextBox15.Name = "metroTextBox15";
            this.metroTextBox15.PasswordChar = '\0';
            this.metroTextBox15.ReadOnly = true;
            this.metroTextBox15.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox15.SelectedText = "";
            this.metroTextBox15.SelectionLength = 0;
            this.metroTextBox15.SelectionStart = 0;
            this.metroTextBox15.ShortcutsEnabled = true;
            this.metroTextBox15.Size = new System.Drawing.Size(226, 23);
            this.metroTextBox15.TabIndex = 85;
            this.metroTextBox15.Tag = "";
            this.metroTextBox15.Text = "29 สิงหาคม 2562";
            this.metroTextBox15.UseCustomForeColor = true;
            this.metroTextBox15.UseSelectable = true;
            this.metroTextBox15.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox15.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBox16
            // 
            // 
            // 
            // 
            this.metroTextBox16.CustomButton.Image = null;
            this.metroTextBox16.CustomButton.Location = new System.Drawing.Point(204, 1);
            this.metroTextBox16.CustomButton.Name = "";
            this.metroTextBox16.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox16.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox16.CustomButton.TabIndex = 1;
            this.metroTextBox16.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox16.CustomButton.UseSelectable = true;
            this.metroTextBox16.CustomButton.Visible = false;
            this.metroTextBox16.ForeColor = System.Drawing.Color.Gray;
            this.metroTextBox16.Lines = new string[] {
        "บริษัท นันยาง  จำกัด (มหาชน)"};
            this.metroTextBox16.Location = new System.Drawing.Point(156, 144);
            this.metroTextBox16.MaxLength = 32767;
            this.metroTextBox16.Name = "metroTextBox16";
            this.metroTextBox16.PasswordChar = '\0';
            this.metroTextBox16.ReadOnly = true;
            this.metroTextBox16.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox16.SelectedText = "";
            this.metroTextBox16.SelectionLength = 0;
            this.metroTextBox16.SelectionStart = 0;
            this.metroTextBox16.ShortcutsEnabled = true;
            this.metroTextBox16.Size = new System.Drawing.Size(226, 23);
            this.metroTextBox16.TabIndex = 84;
            this.metroTextBox16.Text = "บริษัท นันยาง  จำกัด (มหาชน)";
            this.metroTextBox16.UseCustomForeColor = true;
            this.metroTextBox16.UseSelectable = true;
            this.metroTextBox16.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox16.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBox28
            // 
            // 
            // 
            // 
            this.metroTextBox28.CustomButton.Image = null;
            this.metroTextBox28.CustomButton.Location = new System.Drawing.Point(49, 1);
            this.metroTextBox28.CustomButton.Name = "";
            this.metroTextBox28.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox28.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox28.CustomButton.TabIndex = 1;
            this.metroTextBox28.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox28.CustomButton.UseSelectable = true;
            this.metroTextBox28.CustomButton.Visible = false;
            this.metroTextBox28.Lines = new string[] {
        "0"};
            this.metroTextBox28.Location = new System.Drawing.Point(651, 242);
            this.metroTextBox28.MaxLength = 32767;
            this.metroTextBox28.Name = "metroTextBox28";
            this.metroTextBox28.PasswordChar = '\0';
            this.metroTextBox28.ReadOnly = true;
            this.metroTextBox28.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox28.SelectedText = "";
            this.metroTextBox28.SelectionLength = 0;
            this.metroTextBox28.SelectionStart = 0;
            this.metroTextBox28.ShortcutsEnabled = true;
            this.metroTextBox28.Size = new System.Drawing.Size(71, 23);
            this.metroTextBox28.TabIndex = 83;
            this.metroTextBox28.Text = "0";
            this.metroTextBox28.UseSelectable = true;
            this.metroTextBox28.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox28.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBox29
            // 
            // 
            // 
            // 
            this.metroTextBox29.CustomButton.Image = null;
            this.metroTextBox29.CustomButton.Location = new System.Drawing.Point(49, 1);
            this.metroTextBox29.CustomButton.Name = "";
            this.metroTextBox29.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox29.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox29.CustomButton.TabIndex = 1;
            this.metroTextBox29.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox29.CustomButton.UseSelectable = true;
            this.metroTextBox29.CustomButton.Visible = false;
            this.metroTextBox29.Lines = new string[] {
        "80"};
            this.metroTextBox29.Location = new System.Drawing.Point(787, 177);
            this.metroTextBox29.MaxLength = 32767;
            this.metroTextBox29.Name = "metroTextBox29";
            this.metroTextBox29.PasswordChar = '\0';
            this.metroTextBox29.ReadOnly = true;
            this.metroTextBox29.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox29.SelectedText = "";
            this.metroTextBox29.SelectionLength = 0;
            this.metroTextBox29.SelectionStart = 0;
            this.metroTextBox29.ShortcutsEnabled = true;
            this.metroTextBox29.Size = new System.Drawing.Size(71, 23);
            this.metroTextBox29.TabIndex = 82;
            this.metroTextBox29.Text = "80";
            this.metroTextBox29.UseSelectable = true;
            this.metroTextBox29.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox29.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBox30
            // 
            // 
            // 
            // 
            this.metroTextBox30.CustomButton.Image = null;
            this.metroTextBox30.CustomButton.Location = new System.Drawing.Point(45, 1);
            this.metroTextBox30.CustomButton.Name = "";
            this.metroTextBox30.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox30.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox30.CustomButton.TabIndex = 1;
            this.metroTextBox30.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox30.CustomButton.UseSelectable = true;
            this.metroTextBox30.CustomButton.Visible = false;
            this.metroTextBox30.ForeColor = System.Drawing.Color.Red;
            this.metroTextBox30.Lines = new string[] {
        "100,000.00"};
            this.metroTextBox30.Location = new System.Drawing.Point(651, 277);
            this.metroTextBox30.MaxLength = 32767;
            this.metroTextBox30.Name = "metroTextBox30";
            this.metroTextBox30.PasswordChar = '\0';
            this.metroTextBox30.ReadOnly = true;
            this.metroTextBox30.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox30.SelectedText = "";
            this.metroTextBox30.SelectionLength = 0;
            this.metroTextBox30.SelectionStart = 0;
            this.metroTextBox30.ShortcutsEnabled = true;
            this.metroTextBox30.Size = new System.Drawing.Size(67, 23);
            this.metroTextBox30.TabIndex = 80;
            this.metroTextBox30.Text = "100,000.00";
            this.metroTextBox30.UseSelectable = true;
            this.metroTextBox30.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox30.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBox31
            // 
            // 
            // 
            // 
            this.metroTextBox31.CustomButton.Image = null;
            this.metroTextBox31.CustomButton.Location = new System.Drawing.Point(204, 1);
            this.metroTextBox31.CustomButton.Name = "";
            this.metroTextBox31.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox31.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox31.CustomButton.TabIndex = 1;
            this.metroTextBox31.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox31.CustomButton.UseSelectable = true;
            this.metroTextBox31.CustomButton.Visible = false;
            this.metroTextBox31.ForeColor = System.Drawing.Color.Gray;
            this.metroTextBox31.Lines = new string[] {
        "FWIP Card Com R2"};
            this.metroTextBox31.Location = new System.Drawing.Point(156, 177);
            this.metroTextBox31.MaxLength = 32767;
            this.metroTextBox31.Name = "metroTextBox31";
            this.metroTextBox31.PasswordChar = '\0';
            this.metroTextBox31.ReadOnly = true;
            this.metroTextBox31.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox31.SelectedText = "";
            this.metroTextBox31.SelectionLength = 0;
            this.metroTextBox31.SelectionStart = 0;
            this.metroTextBox31.ShortcutsEnabled = true;
            this.metroTextBox31.Size = new System.Drawing.Size(226, 23);
            this.metroTextBox31.TabIndex = 79;
            this.metroTextBox31.Text = "FWIP Card Com R2";
            this.metroTextBox31.UseCustomForeColor = true;
            this.metroTextBox31.UseSelectable = true;
            this.metroTextBox31.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox31.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBox32
            // 
            // 
            // 
            // 
            this.metroTextBox32.CustomButton.Image = null;
            this.metroTextBox32.CustomButton.Location = new System.Drawing.Point(129, 1);
            this.metroTextBox32.CustomButton.Name = "";
            this.metroTextBox32.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox32.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox32.CustomButton.TabIndex = 1;
            this.metroTextBox32.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox32.CustomButton.UseSelectable = true;
            this.metroTextBox32.CustomButton.Visible = false;
            this.metroTextBox32.ForeColor = System.Drawing.Color.Gray;
            this.metroTextBox32.Lines = new string[] {
        "1 สิงหาคม 2562"};
            this.metroTextBox32.Location = new System.Drawing.Point(796, 60);
            this.metroTextBox32.MaxLength = 32767;
            this.metroTextBox32.Name = "metroTextBox32";
            this.metroTextBox32.PasswordChar = '\0';
            this.metroTextBox32.ReadOnly = true;
            this.metroTextBox32.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox32.SelectedText = "";
            this.metroTextBox32.SelectionLength = 0;
            this.metroTextBox32.SelectionStart = 0;
            this.metroTextBox32.ShortcutsEnabled = true;
            this.metroTextBox32.Size = new System.Drawing.Size(151, 23);
            this.metroTextBox32.TabIndex = 78;
            this.metroTextBox32.Text = "1 สิงหาคม 2562";
            this.metroTextBox32.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.metroTextBox32.UseCustomForeColor = true;
            this.metroTextBox32.UseSelectable = true;
            this.metroTextBox32.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox32.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel44
            // 
            this.metroLabel44.AutoSize = true;
            this.metroLabel44.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel44.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel44.Location = new System.Drawing.Point(348, 29);
            this.metroLabel44.Name = "metroLabel44";
            this.metroLabel44.Size = new System.Drawing.Size(175, 25);
            this.metroLabel44.TabIndex = 77;
            this.metroLabel44.Text = "ใบสั่งผลิต/เบิกวัตถุดิบ";
            // 
            // metroComboBox3
            // 
            this.metroComboBox3.FormattingEnabled = true;
            this.metroComboBox3.ItemHeight = 23;
            this.metroComboBox3.Items.AddRange(new object[] {
            "SQC",
            "SRM",
            "SPD2",
            "SPD4",
            "SFG"});
            this.metroComboBox3.Location = new System.Drawing.Point(156, 242);
            this.metroComboBox3.Name = "metroComboBox3";
            this.metroComboBox3.Size = new System.Drawing.Size(226, 29);
            this.metroComboBox3.TabIndex = 73;
            this.metroComboBox3.UseSelectable = true;
            // 
            // metroLabel46
            // 
            this.metroLabel46.AutoSize = true;
            this.metroLabel46.Location = new System.Drawing.Point(511, 277);
            this.metroLabel46.Name = "metroLabel46";
            this.metroLabel46.Size = new System.Drawing.Size(116, 19);
            this.metroLabel46.TabIndex = 72;
            this.metroLabel46.Text = "จำนวนที่เปิดการผลิต";
            // 
            // metroLabel48
            // 
            this.metroLabel48.AutoSize = true;
            this.metroLabel48.Location = new System.Drawing.Point(651, 91);
            this.metroLabel48.Name = "metroLabel48";
            this.metroLabel48.Size = new System.Drawing.Size(88, 19);
            this.metroLabel48.TabIndex = 54;
            this.metroLabel48.Text = "หมายเลขอ้างอิง";
            // 
            // metroLabel49
            // 
            this.metroLabel49.AutoSize = true;
            this.metroLabel49.Location = new System.Drawing.Point(728, 177);
            this.metroLabel49.Name = "metroLabel49";
            this.metroLabel49.Size = new System.Drawing.Size(40, 19);
            this.metroLabel49.TabIndex = 71;
            this.metroLabel49.Text = "ปอนด์";
            // 
            // metroTextBox33
            // 
            // 
            // 
            // 
            this.metroTextBox33.CustomButton.Image = null;
            this.metroTextBox33.CustomButton.Location = new System.Drawing.Point(47, 1);
            this.metroTextBox33.CustomButton.Name = "";
            this.metroTextBox33.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox33.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox33.CustomButton.TabIndex = 1;
            this.metroTextBox33.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox33.CustomButton.UseSelectable = true;
            this.metroTextBox33.CustomButton.Visible = false;
            this.metroTextBox33.Lines = new string[] {
        "5,996.51"};
            this.metroTextBox33.Location = new System.Drawing.Point(651, 177);
            this.metroTextBox33.MaxLength = 32767;
            this.metroTextBox33.Name = "metroTextBox33";
            this.metroTextBox33.PasswordChar = '\0';
            this.metroTextBox33.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox33.SelectedText = "";
            this.metroTextBox33.SelectionLength = 0;
            this.metroTextBox33.SelectionStart = 0;
            this.metroTextBox33.ShortcutsEnabled = true;
            this.metroTextBox33.Size = new System.Drawing.Size(69, 23);
            this.metroTextBox33.TabIndex = 70;
            this.metroTextBox33.Text = "5,996.51";
            this.metroTextBox33.UseSelectable = true;
            this.metroTextBox33.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox33.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBox34
            // 
            // 
            // 
            // 
            this.metroTextBox34.CustomButton.Image = null;
            this.metroTextBox34.CustomButton.Location = new System.Drawing.Point(129, 1);
            this.metroTextBox34.CustomButton.Name = "";
            this.metroTextBox34.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox34.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox34.CustomButton.TabIndex = 1;
            this.metroTextBox34.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox34.CustomButton.UseSelectable = true;
            this.metroTextBox34.CustomButton.Visible = false;
            this.metroTextBox34.ForeColor = System.Drawing.Color.Gray;
            this.metroTextBox34.Lines = new string[0];
            this.metroTextBox34.Location = new System.Drawing.Point(796, 91);
            this.metroTextBox34.MaxLength = 32767;
            this.metroTextBox34.Name = "metroTextBox34";
            this.metroTextBox34.PasswordChar = '\0';
            this.metroTextBox34.ReadOnly = true;
            this.metroTextBox34.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox34.SelectedText = "";
            this.metroTextBox34.SelectionLength = 0;
            this.metroTextBox34.SelectionStart = 0;
            this.metroTextBox34.ShortcutsEnabled = true;
            this.metroTextBox34.Size = new System.Drawing.Size(151, 23);
            this.metroTextBox34.TabIndex = 55;
            this.metroTextBox34.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.metroTextBox34.UseCustomForeColor = true;
            this.metroTextBox34.UseSelectable = true;
            this.metroTextBox34.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox34.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel50
            // 
            this.metroLabel50.AutoSize = true;
            this.metroLabel50.Location = new System.Drawing.Point(511, 177);
            this.metroLabel50.Name = "metroLabel50";
            this.metroLabel50.Size = new System.Drawing.Size(101, 19);
            this.metroLabel50.TabIndex = 69;
            this.metroLabel50.Text = "จำนวนที่ผลิตเสร็จ";
            // 
            // metroTextBox35
            // 
            // 
            // 
            // 
            this.metroTextBox35.CustomButton.Image = null;
            this.metroTextBox35.CustomButton.Location = new System.Drawing.Point(129, 1);
            this.metroTextBox35.CustomButton.Name = "";
            this.metroTextBox35.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox35.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox35.CustomButton.TabIndex = 1;
            this.metroTextBox35.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox35.CustomButton.UseSelectable = true;
            this.metroTextBox35.CustomButton.Visible = false;
            this.metroTextBox35.ForeColor = System.Drawing.Color.Gray;
            this.metroTextBox35.Lines = new string[] {
        "119070001"};
            this.metroTextBox35.Location = new System.Drawing.Point(796, 29);
            this.metroTextBox35.MaxLength = 32767;
            this.metroTextBox35.Name = "metroTextBox35";
            this.metroTextBox35.PasswordChar = '\0';
            this.metroTextBox35.ReadOnly = true;
            this.metroTextBox35.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox35.SelectedText = "";
            this.metroTextBox35.SelectionLength = 0;
            this.metroTextBox35.SelectionStart = 0;
            this.metroTextBox35.ShortcutsEnabled = true;
            this.metroTextBox35.Size = new System.Drawing.Size(151, 23);
            this.metroTextBox35.TabIndex = 56;
            this.metroTextBox35.Text = "119070001";
            this.metroTextBox35.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.metroTextBox35.UseCustomForeColor = true;
            this.metroTextBox35.UseSelectable = true;
            this.metroTextBox35.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox35.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel51
            // 
            this.metroLabel51.AutoSize = true;
            this.metroLabel51.Location = new System.Drawing.Point(511, 242);
            this.metroLabel51.Name = "metroLabel51";
            this.metroLabel51.Size = new System.Drawing.Size(93, 19);
            this.metroLabel51.TabIndex = 68;
            this.metroLabel51.Text = "จำนวนที่ใช้ไม่ได้";
            // 
            // metroLabel52
            // 
            this.metroLabel52.AutoSize = true;
            this.metroLabel52.Location = new System.Drawing.Point(35, 177);
            this.metroLabel52.Name = "metroLabel52";
            this.metroLabel52.Size = new System.Drawing.Size(61, 19);
            this.metroLabel52.TabIndex = 57;
            this.metroLabel52.Text = "รหัสสินค้า";
            // 
            // metroLabel53
            // 
            this.metroLabel53.AutoSize = true;
            this.metroLabel53.Location = new System.Drawing.Point(511, 144);
            this.metroLabel53.Name = "metroLabel53";
            this.metroLabel53.Size = new System.Drawing.Size(116, 19);
            this.metroLabel53.TabIndex = 58;
            this.metroLabel53.Text = "จำนวนที่วางแผนผลิต";
            // 
            // metroLabel54
            // 
            this.metroLabel54.AutoSize = true;
            this.metroLabel54.Location = new System.Drawing.Point(728, 208);
            this.metroLabel54.Name = "metroLabel54";
            this.metroLabel54.Size = new System.Drawing.Size(53, 19);
            this.metroLabel54.TabIndex = 67;
            this.metroLabel54.Text = "กก.";
            // 
            // metroLabel55
            // 
            this.metroLabel55.AutoSize = true;
            this.metroLabel55.Location = new System.Drawing.Point(35, 208);
            this.metroLabel55.Name = "metroLabel55";
            this.metroLabel55.Size = new System.Drawing.Size(97, 19);
            this.metroLabel55.TabIndex = 59;
            this.metroLabel55.Text = "รายละเอียดสินค้า";
            // 
            // metroTextBox36
            // 
            // 
            // 
            // 
            this.metroTextBox36.CustomButton.Image = null;
            this.metroTextBox36.CustomButton.Location = new System.Drawing.Point(204, 1);
            this.metroTextBox36.CustomButton.Name = "";
            this.metroTextBox36.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox36.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox36.CustomButton.TabIndex = 1;
            this.metroTextBox36.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox36.CustomButton.UseSelectable = true;
            this.metroTextBox36.CustomButton.Visible = false;
            this.metroTextBox36.Enabled = false;
            this.metroTextBox36.ForeColor = System.Drawing.Color.Gray;
            this.metroTextBox36.Lines = new string[] {
        "FWIP Card Com R2"};
            this.metroTextBox36.Location = new System.Drawing.Point(156, 208);
            this.metroTextBox36.MaxLength = 32767;
            this.metroTextBox36.Name = "metroTextBox36";
            this.metroTextBox36.PasswordChar = '\0';
            this.metroTextBox36.ReadOnly = true;
            this.metroTextBox36.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox36.SelectedText = "";
            this.metroTextBox36.SelectionLength = 0;
            this.metroTextBox36.SelectionStart = 0;
            this.metroTextBox36.ShortcutsEnabled = true;
            this.metroTextBox36.Size = new System.Drawing.Size(226, 23);
            this.metroTextBox36.TabIndex = 60;
            this.metroTextBox36.Tag = "";
            this.metroTextBox36.Text = "FWIP Card Com R2";
            this.metroTextBox36.UseCustomForeColor = true;
            this.metroTextBox36.UseSelectable = true;
            this.metroTextBox36.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox36.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel56
            // 
            this.metroLabel56.AutoSize = true;
            this.metroLabel56.Location = new System.Drawing.Point(728, 144);
            this.metroLabel56.Name = "metroLabel56";
            this.metroLabel56.Size = new System.Drawing.Size(40, 19);
            this.metroLabel56.TabIndex = 66;
            this.metroLabel56.Text = "ปอนด์";
            // 
            // metroTextBox37
            // 
            // 
            // 
            // 
            this.metroTextBox37.CustomButton.Image = null;
            this.metroTextBox37.CustomButton.Location = new System.Drawing.Point(49, 1);
            this.metroTextBox37.CustomButton.Name = "";
            this.metroTextBox37.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox37.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox37.CustomButton.TabIndex = 1;
            this.metroTextBox37.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox37.CustomButton.UseSelectable = true;
            this.metroTextBox37.CustomButton.Visible = false;
            this.metroTextBox37.Lines = new string[] {
        "100,000.00"};
            this.metroTextBox37.Location = new System.Drawing.Point(651, 144);
            this.metroTextBox37.MaxLength = 32767;
            this.metroTextBox37.Name = "metroTextBox37";
            this.metroTextBox37.PasswordChar = '\0';
            this.metroTextBox37.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox37.SelectedText = "";
            this.metroTextBox37.SelectionLength = 0;
            this.metroTextBox37.SelectionStart = 0;
            this.metroTextBox37.ShortcutsEnabled = true;
            this.metroTextBox37.Size = new System.Drawing.Size(71, 23);
            this.metroTextBox37.TabIndex = 65;
            this.metroTextBox37.Text = "100,000.00";
            this.metroTextBox37.UseSelectable = true;
            this.metroTextBox37.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox37.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel57
            // 
            this.metroLabel57.AutoSize = true;
            this.metroLabel57.Location = new System.Drawing.Point(35, 242);
            this.metroLabel57.Name = "metroLabel57";
            this.metroLabel57.Size = new System.Drawing.Size(36, 19);
            this.metroLabel57.TabIndex = 61;
            this.metroLabel57.Text = "โกดัง";
            // 
            // metroLabel26
            // 
            this.metroLabel26.AutoSize = true;
            this.metroLabel26.Location = new System.Drawing.Point(35, 277);
            this.metroLabel26.Name = "metroLabel26";
            this.metroLabel26.Size = new System.Drawing.Size(78, 19);
            this.metroLabel26.TabIndex = 46;
            this.metroLabel26.Text = "วันส่งมอบงาน";
            // 
            // metroLabel40
            // 
            this.metroLabel40.AutoSize = true;
            this.metroLabel40.Location = new System.Drawing.Point(35, 144);
            this.metroLabel40.Name = "metroLabel40";
            this.metroLabel40.Size = new System.Drawing.Size(39, 19);
            this.metroLabel40.TabIndex = 42;
            this.metroLabel40.Text = "ลูกค้า";
            // 
            // metroButton11
            // 
            this.metroButton11.Location = new System.Drawing.Point(796, 589);
            this.metroButton11.Name = "metroButton11";
            this.metroButton11.Size = new System.Drawing.Size(151, 30);
            this.metroButton11.TabIndex = 40;
            this.metroButton11.Text = "ออกจากหน้านี้";
            this.metroButton11.UseSelectable = true;
            // 
            // metroButton13
            // 
            this.metroButton13.Location = new System.Drawing.Point(796, 513);
            this.metroButton13.Name = "metroButton13";
            this.metroButton13.Size = new System.Drawing.Size(151, 30);
            this.metroButton13.TabIndex = 37;
            this.metroButton13.Text = "พิมพ์แบบฟอร์มนี้";
            this.metroButton13.UseSelectable = true;
            // 
            // metroButton14
            // 
            this.metroButton14.Location = new System.Drawing.Point(621, 513);
            this.metroButton14.Name = "metroButton14";
            this.metroButton14.Size = new System.Drawing.Size(147, 30);
            this.metroButton14.TabIndex = 36;
            this.metroButton14.Text = "บันทึกการเปลี่ยนแปลง";
            this.metroButton14.UseSelectable = true;
            // 
            // metroLabel25
            // 
            this.metroLabel25.AutoSize = true;
            this.metroLabel25.Location = new System.Drawing.Point(651, 29);
            this.metroLabel25.Name = "metroLabel25";
            this.metroLabel25.Size = new System.Drawing.Size(88, 19);
            this.metroLabel25.TabIndex = 0;
            this.metroLabel25.Text = "หมายเลขใบงาน";
            // 
            // metroLabel27
            // 
            this.metroLabel27.AutoSize = true;
            this.metroLabel27.Location = new System.Drawing.Point(651, 60);
            this.metroLabel27.Name = "metroLabel27";
            this.metroLabel27.Size = new System.Drawing.Size(86, 19);
            this.metroLabel27.TabIndex = 1;
            this.metroLabel27.Text = "วันที่สร้างใบงาน";
            // 
            // transferTabPage
            // 
            this.transferTabPage.HorizontalScrollbarBarColor = true;
            this.transferTabPage.HorizontalScrollbarHighlightOnWheel = false;
            this.transferTabPage.HorizontalScrollbarSize = 10;
            this.transferTabPage.Location = new System.Drawing.Point(4, 38);
            this.transferTabPage.Name = "transferTabPage";
            this.transferTabPage.Size = new System.Drawing.Size(970, 640);
            this.transferTabPage.TabIndex = 1;
            this.transferTabPage.Text = "โอนย้าย";
            this.transferTabPage.VerticalScrollbarBarColor = true;
            this.transferTabPage.VerticalScrollbarHighlightOnWheel = false;
            this.transferTabPage.VerticalScrollbarSize = 10;
            // 
            // delArrTabPage
            // 
            this.delArrTabPage.HorizontalScrollbarBarColor = true;
            this.delArrTabPage.HorizontalScrollbarHighlightOnWheel = false;
            this.delArrTabPage.HorizontalScrollbarSize = 10;
            this.delArrTabPage.Location = new System.Drawing.Point(4, 38);
            this.delArrTabPage.Name = "delArrTabPage";
            this.delArrTabPage.Size = new System.Drawing.Size(970, 640);
            this.delArrTabPage.TabIndex = 2;
            this.delArrTabPage.Text = "จัดสินค้า";
            this.delArrTabPage.VerticalScrollbarBarColor = true;
            this.delArrTabPage.VerticalScrollbarHighlightOnWheel = false;
            this.delArrTabPage.VerticalScrollbarSize = 10;
            // 
            // metroTabPage5
            // 
            this.metroTabPage5.HorizontalScrollbarBarColor = true;
            this.metroTabPage5.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage5.HorizontalScrollbarSize = 10;
            this.metroTabPage5.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage5.Name = "metroTabPage5";
            this.metroTabPage5.Size = new System.Drawing.Size(970, 640);
            this.metroTabPage5.TabIndex = 8;
            this.metroTabPage5.Text = "รายงาน";
            this.metroTabPage5.VerticalScrollbarBarColor = true;
            this.metroTabPage5.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage5.VerticalScrollbarSize = 10;
            // 
            // metroTabPage1
            // 
            this.metroTabPage1.Controls.Add(this.metroButton33);
            this.metroTabPage1.Controls.Add(this.metroButton34);
            this.metroTabPage1.Controls.Add(this.metroButton35);
            this.metroTabPage1.Controls.Add(this.metroButton36);
            this.metroTabPage1.Controls.Add(this.metroButton37);
            this.metroTabPage1.Controls.Add(this.metroButton38);
            this.metroTabPage1.Controls.Add(this.metroButton39);
            this.metroTabPage1.Controls.Add(this.metroButton40);
            this.metroTabPage1.Controls.Add(this.metroButton25);
            this.metroTabPage1.Controls.Add(this.metroButton26);
            this.metroTabPage1.Controls.Add(this.metroButton27);
            this.metroTabPage1.Controls.Add(this.metroButton28);
            this.metroTabPage1.Controls.Add(this.metroButton29);
            this.metroTabPage1.Controls.Add(this.metroButton30);
            this.metroTabPage1.Controls.Add(this.metroButton31);
            this.metroTabPage1.Controls.Add(this.metroButton32);
            this.metroTabPage1.Controls.Add(this.metroButton21);
            this.metroTabPage1.Controls.Add(this.metroButton22);
            this.metroTabPage1.Controls.Add(this.metroButton23);
            this.metroTabPage1.Controls.Add(this.metroButton24);
            this.metroTabPage1.Controls.Add(this.metroButton12);
            this.metroTabPage1.Controls.Add(this.metroButton20);
            this.metroTabPage1.Controls.Add(this.metroButton3);
            this.metroTabPage1.Controls.Add(this.metroButton2);
            this.metroTabPage1.Controls.Add(this.pictureBox1);
            this.metroTabPage1.Controls.Add(this.metroLabel5);
            this.metroTabPage1.Controls.Add(this.metroLabel4);
            this.metroTabPage1.Controls.Add(this.metroComboBox4);
            this.metroTabPage1.Controls.Add(this.metroButton1);
            this.metroTabPage1.Controls.Add(this.metroTextBox1);
            this.metroTabPage1.Controls.Add(this.metroLabel1);
            this.metroTabPage1.Controls.Add(this.metroTextBox2);
            this.metroTabPage1.Controls.Add(this.metroLabel2);
            this.metroTabPage1.Controls.Add(this.metroLabel3);
            this.metroTabPage1.HorizontalScrollbarBarColor = true;
            this.metroTabPage1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage1.HorizontalScrollbarSize = 10;
            this.metroTabPage1.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage1.Name = "metroTabPage1";
            this.metroTabPage1.Size = new System.Drawing.Size(970, 640);
            this.metroTabPage1.TabIndex = 4;
            this.metroTabPage1.Text = "จัดการผู้ใช้";
            this.metroTabPage1.VerticalScrollbarBarColor = true;
            this.metroTabPage1.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage1.VerticalScrollbarSize = 10;
            this.metroTabPage1.Click += new System.EventHandler(this.MetroTabPage1_Click);
            // 
            // metroButton33
            // 
            this.metroButton33.Location = new System.Drawing.Point(816, 639);
            this.metroButton33.Name = "metroButton33";
            this.metroButton33.Size = new System.Drawing.Size(109, 30);
            this.metroButton33.TabIndex = 120;
            this.metroButton33.Text = "เปลี่ยนรหัสผ่าน";
            this.metroButton33.UseSelectable = true;
            // 
            // metroButton34
            // 
            this.metroButton34.Location = new System.Drawing.Point(931, 639);
            this.metroButton34.Name = "metroButton34";
            this.metroButton34.Size = new System.Drawing.Size(30, 30);
            this.metroButton34.TabIndex = 119;
            this.metroButton34.Text = "X";
            this.metroButton34.UseSelectable = true;
            // 
            // metroButton35
            // 
            this.metroButton35.Location = new System.Drawing.Point(862, 609);
            this.metroButton35.Name = "metroButton35";
            this.metroButton35.Size = new System.Drawing.Size(63, 30);
            this.metroButton35.TabIndex = 118;
            this.metroButton35.Text = "แก้ไข";
            this.metroButton35.UseSelectable = true;
            // 
            // metroButton36
            // 
            this.metroButton36.Location = new System.Drawing.Point(931, 609);
            this.metroButton36.Name = "metroButton36";
            this.metroButton36.Size = new System.Drawing.Size(30, 30);
            this.metroButton36.TabIndex = 117;
            this.metroButton36.Text = "X";
            this.metroButton36.UseSelectable = true;
            // 
            // metroButton37
            // 
            this.metroButton37.Location = new System.Drawing.Point(862, 579);
            this.metroButton37.Name = "metroButton37";
            this.metroButton37.Size = new System.Drawing.Size(63, 30);
            this.metroButton37.TabIndex = 116;
            this.metroButton37.Text = "แก้ไข";
            this.metroButton37.UseSelectable = true;
            // 
            // metroButton38
            // 
            this.metroButton38.Location = new System.Drawing.Point(931, 579);
            this.metroButton38.Name = "metroButton38";
            this.metroButton38.Size = new System.Drawing.Size(30, 30);
            this.metroButton38.TabIndex = 115;
            this.metroButton38.Text = "X";
            this.metroButton38.UseSelectable = true;
            // 
            // metroButton39
            // 
            this.metroButton39.Location = new System.Drawing.Point(862, 549);
            this.metroButton39.Name = "metroButton39";
            this.metroButton39.Size = new System.Drawing.Size(63, 30);
            this.metroButton39.TabIndex = 114;
            this.metroButton39.Text = "แก้ไข";
            this.metroButton39.UseSelectable = true;
            // 
            // metroButton40
            // 
            this.metroButton40.Location = new System.Drawing.Point(931, 549);
            this.metroButton40.Name = "metroButton40";
            this.metroButton40.Size = new System.Drawing.Size(30, 30);
            this.metroButton40.TabIndex = 113;
            this.metroButton40.Text = "X";
            this.metroButton40.UseSelectable = true;
            // 
            // metroButton25
            // 
            this.metroButton25.Location = new System.Drawing.Point(862, 519);
            this.metroButton25.Name = "metroButton25";
            this.metroButton25.Size = new System.Drawing.Size(63, 30);
            this.metroButton25.TabIndex = 112;
            this.metroButton25.Text = "แก้ไข";
            this.metroButton25.UseSelectable = true;
            // 
            // metroButton26
            // 
            this.metroButton26.Location = new System.Drawing.Point(931, 519);
            this.metroButton26.Name = "metroButton26";
            this.metroButton26.Size = new System.Drawing.Size(30, 30);
            this.metroButton26.TabIndex = 111;
            this.metroButton26.Text = "X";
            this.metroButton26.UseSelectable = true;
            // 
            // metroButton27
            // 
            this.metroButton27.Location = new System.Drawing.Point(862, 489);
            this.metroButton27.Name = "metroButton27";
            this.metroButton27.Size = new System.Drawing.Size(63, 30);
            this.metroButton27.TabIndex = 110;
            this.metroButton27.Text = "แก้ไข";
            this.metroButton27.UseSelectable = true;
            // 
            // metroButton28
            // 
            this.metroButton28.Location = new System.Drawing.Point(931, 489);
            this.metroButton28.Name = "metroButton28";
            this.metroButton28.Size = new System.Drawing.Size(30, 30);
            this.metroButton28.TabIndex = 109;
            this.metroButton28.Text = "X";
            this.metroButton28.UseSelectable = true;
            // 
            // metroButton29
            // 
            this.metroButton29.Location = new System.Drawing.Point(862, 459);
            this.metroButton29.Name = "metroButton29";
            this.metroButton29.Size = new System.Drawing.Size(63, 30);
            this.metroButton29.TabIndex = 108;
            this.metroButton29.Text = "แก้ไข";
            this.metroButton29.UseSelectable = true;
            // 
            // metroButton30
            // 
            this.metroButton30.Location = new System.Drawing.Point(931, 459);
            this.metroButton30.Name = "metroButton30";
            this.metroButton30.Size = new System.Drawing.Size(30, 30);
            this.metroButton30.TabIndex = 107;
            this.metroButton30.Text = "X";
            this.metroButton30.UseSelectable = true;
            // 
            // metroButton31
            // 
            this.metroButton31.Location = new System.Drawing.Point(862, 429);
            this.metroButton31.Name = "metroButton31";
            this.metroButton31.Size = new System.Drawing.Size(63, 30);
            this.metroButton31.TabIndex = 106;
            this.metroButton31.Text = "แก้ไข";
            this.metroButton31.UseSelectable = true;
            // 
            // metroButton32
            // 
            this.metroButton32.Location = new System.Drawing.Point(931, 429);
            this.metroButton32.Name = "metroButton32";
            this.metroButton32.Size = new System.Drawing.Size(30, 30);
            this.metroButton32.TabIndex = 105;
            this.metroButton32.Text = "X";
            this.metroButton32.UseSelectable = true;
            // 
            // metroButton21
            // 
            this.metroButton21.Location = new System.Drawing.Point(862, 399);
            this.metroButton21.Name = "metroButton21";
            this.metroButton21.Size = new System.Drawing.Size(63, 30);
            this.metroButton21.TabIndex = 104;
            this.metroButton21.Text = "แก้ไข";
            this.metroButton21.UseSelectable = true;
            // 
            // metroButton22
            // 
            this.metroButton22.Location = new System.Drawing.Point(931, 399);
            this.metroButton22.Name = "metroButton22";
            this.metroButton22.Size = new System.Drawing.Size(30, 30);
            this.metroButton22.TabIndex = 103;
            this.metroButton22.Text = "X";
            this.metroButton22.UseSelectable = true;
            // 
            // metroButton23
            // 
            this.metroButton23.Location = new System.Drawing.Point(862, 369);
            this.metroButton23.Name = "metroButton23";
            this.metroButton23.Size = new System.Drawing.Size(63, 30);
            this.metroButton23.TabIndex = 102;
            this.metroButton23.Text = "แก้ไข";
            this.metroButton23.UseSelectable = true;
            // 
            // metroButton24
            // 
            this.metroButton24.Location = new System.Drawing.Point(931, 369);
            this.metroButton24.Name = "metroButton24";
            this.metroButton24.Size = new System.Drawing.Size(30, 30);
            this.metroButton24.TabIndex = 101;
            this.metroButton24.Text = "X";
            this.metroButton24.UseSelectable = true;
            // 
            // metroButton12
            // 
            this.metroButton12.Location = new System.Drawing.Point(862, 339);
            this.metroButton12.Name = "metroButton12";
            this.metroButton12.Size = new System.Drawing.Size(63, 30);
            this.metroButton12.TabIndex = 100;
            this.metroButton12.Text = "แก้ไข";
            this.metroButton12.UseSelectable = true;
            // 
            // metroButton20
            // 
            this.metroButton20.Location = new System.Drawing.Point(931, 339);
            this.metroButton20.Name = "metroButton20";
            this.metroButton20.Size = new System.Drawing.Size(30, 30);
            this.metroButton20.TabIndex = 99;
            this.metroButton20.Text = "X";
            this.metroButton20.UseSelectable = true;
            // 
            // metroButton3
            // 
            this.metroButton3.Location = new System.Drawing.Point(862, 309);
            this.metroButton3.Name = "metroButton3";
            this.metroButton3.Size = new System.Drawing.Size(63, 30);
            this.metroButton3.TabIndex = 98;
            this.metroButton3.Text = "แก้ไข";
            this.metroButton3.UseSelectable = true;
            // 
            // metroButton2
            // 
            this.metroButton2.Location = new System.Drawing.Point(931, 309);
            this.metroButton2.Name = "metroButton2";
            this.metroButton2.Size = new System.Drawing.Size(30, 30);
            this.metroButton2.TabIndex = 97;
            this.metroButton2.Text = "X";
            this.metroButton2.UseSelectable = true;
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel5.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel5.Location = new System.Drawing.Point(418, 229);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(190, 25);
            this.metroLabel5.TabIndex = 95;
            this.metroLabel5.Text = "รายชื่อผู้ใช้งานในระบบ";
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(337, 135);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(40, 19);
            this.metroLabel4.TabIndex = 94;
            this.metroLabel4.Text = "แผนก";
            // 
            // metroComboBox4
            // 
            this.metroComboBox4.FormattingEnabled = true;
            this.metroComboBox4.ItemHeight = 23;
            this.metroComboBox4.Items.AddRange(new object[] {
            "SQC",
            "SRM",
            "SPD2",
            "SPD4",
            "SFG"});
            this.metroComboBox4.Location = new System.Drawing.Point(418, 135);
            this.metroComboBox4.Name = "metroComboBox4";
            this.metroComboBox4.Size = new System.Drawing.Size(215, 29);
            this.metroComboBox4.TabIndex = 93;
            this.metroComboBox4.UseSelectable = true;
            // 
            // metroButton1
            // 
            this.metroButton1.Location = new System.Drawing.Point(418, 170);
            this.metroButton1.Name = "metroButton1";
            this.metroButton1.Size = new System.Drawing.Size(215, 30);
            this.metroButton1.TabIndex = 92;
            this.metroButton1.Text = "เพิ่มเข้าระบบ";
            this.metroButton1.UseSelectable = true;
            // 
            // metroTextBox1
            // 
            // 
            // 
            // 
            this.metroTextBox1.CustomButton.Image = null;
            this.metroTextBox1.CustomButton.Location = new System.Drawing.Point(187, 1);
            this.metroTextBox1.CustomButton.Name = "";
            this.metroTextBox1.CustomButton.Size = new System.Drawing.Size(27, 27);
            this.metroTextBox1.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox1.CustomButton.TabIndex = 1;
            this.metroTextBox1.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox1.CustomButton.UseSelectable = true;
            this.metroTextBox1.CustomButton.Visible = false;
            this.metroTextBox1.ForeColor = System.Drawing.Color.Gray;
            this.metroTextBox1.Lines = new string[] {
        "Password"};
            this.metroTextBox1.Location = new System.Drawing.Point(418, 100);
            this.metroTextBox1.MaxLength = 32767;
            this.metroTextBox1.Name = "metroTextBox1";
            this.metroTextBox1.PasswordChar = '\0';
            this.metroTextBox1.ReadOnly = true;
            this.metroTextBox1.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox1.SelectedText = "";
            this.metroTextBox1.SelectionLength = 0;
            this.metroTextBox1.SelectionStart = 0;
            this.metroTextBox1.ShortcutsEnabled = true;
            this.metroTextBox1.Size = new System.Drawing.Size(215, 29);
            this.metroTextBox1.TabIndex = 91;
            this.metroTextBox1.Text = "Password";
            this.metroTextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.metroTextBox1.UseCustomForeColor = true;
            this.metroTextBox1.UseSelectable = true;
            this.metroTextBox1.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox1.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel1.Location = new System.Drawing.Point(418, 17);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(140, 25);
            this.metroLabel1.TabIndex = 90;
            this.metroLabel1.Text = "เพิ่มผู้ใช้งานใหม่";
            // 
            // metroTextBox2
            // 
            // 
            // 
            // 
            this.metroTextBox2.CustomButton.Image = null;
            this.metroTextBox2.CustomButton.Location = new System.Drawing.Point(187, 1);
            this.metroTextBox2.CustomButton.Name = "";
            this.metroTextBox2.CustomButton.Size = new System.Drawing.Size(27, 27);
            this.metroTextBox2.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox2.CustomButton.TabIndex = 1;
            this.metroTextBox2.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox2.CustomButton.UseSelectable = true;
            this.metroTextBox2.CustomButton.Visible = false;
            this.metroTextBox2.ForeColor = System.Drawing.Color.Gray;
            this.metroTextBox2.Lines = new string[] {
        "Username"};
            this.metroTextBox2.Location = new System.Drawing.Point(418, 65);
            this.metroTextBox2.MaxLength = 32767;
            this.metroTextBox2.Name = "metroTextBox2";
            this.metroTextBox2.PasswordChar = '\0';
            this.metroTextBox2.ReadOnly = true;
            this.metroTextBox2.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox2.SelectedText = "";
            this.metroTextBox2.SelectionLength = 0;
            this.metroTextBox2.SelectionStart = 0;
            this.metroTextBox2.ShortcutsEnabled = true;
            this.metroTextBox2.Size = new System.Drawing.Size(215, 29);
            this.metroTextBox2.TabIndex = 89;
            this.metroTextBox2.Text = "Username";
            this.metroTextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.metroTextBox2.UseCustomForeColor = true;
            this.metroTextBox2.UseSelectable = true;
            this.metroTextBox2.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox2.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(337, 100);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(53, 19);
            this.metroLabel2.TabIndex = 88;
            this.metroLabel2.Text = "รหัสผ่าน";
            this.metroLabel2.Click += new System.EventHandler(this.MetroLabel2_Click);
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(337, 65);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(64, 19);
            this.metroLabel3.TabIndex = 87;
            this.metroLabel3.Text = "ชื่อผู้ใช้งาน";
            // 
            // metroTabPage2
            // 
            this.metroTabPage2.Controls.Add(this.metroButton46);
            this.metroTabPage2.Controls.Add(this.metroLabel61);
            this.metroTabPage2.Controls.Add(this.metroComboBox6);
            this.metroTabPage2.Controls.Add(this.metroTextBox14);
            this.metroTabPage2.Controls.Add(this.metroLabel60);
            this.metroTabPage2.Controls.Add(this.metroLabel59);
            this.metroTabPage2.Controls.Add(this.metroLabel58);
            this.metroTabPage2.Controls.Add(this.metroLabel47);
            this.metroTabPage2.Controls.Add(this.metroButton43);
            this.metroTabPage2.Controls.Add(this.listBox1);
            this.metroTabPage2.Controls.Add(this.metroTextBox13);
            this.metroTabPage2.HorizontalScrollbarBarColor = true;
            this.metroTabPage2.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage2.HorizontalScrollbarSize = 10;
            this.metroTabPage2.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage2.Name = "metroTabPage2";
            this.metroTabPage2.Size = new System.Drawing.Size(970, 640);
            this.metroTabPage2.TabIndex = 5;
            this.metroTabPage2.Text = "ตั้งค่า Lot";
            this.metroTabPage2.VerticalScrollbarBarColor = true;
            this.metroTabPage2.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage2.VerticalScrollbarSize = 10;
            // 
            // metroButton46
            // 
            this.metroButton46.Location = new System.Drawing.Point(642, 297);
            this.metroButton46.Name = "metroButton46";
            this.metroButton46.Size = new System.Drawing.Size(130, 30);
            this.metroButton46.TabIndex = 110;
            this.metroButton46.Text = "ยืนยันการเปลี่ยนแปลง";
            this.metroButton46.UseSelectable = true;
            // 
            // metroLabel61
            // 
            this.metroLabel61.AutoSize = true;
            this.metroLabel61.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel61.Location = new System.Drawing.Point(151, 100);
            this.metroLabel61.Name = "metroLabel61";
            this.metroLabel61.Size = new System.Drawing.Size(159, 19);
            this.metroLabel61.TabIndex = 109;
            this.metroLabel61.Text = "หมวด Lot ที่ต้องการตั้งค่า";
            // 
            // metroComboBox6
            // 
            this.metroComboBox6.FormattingEnabled = true;
            this.metroComboBox6.ItemHeight = 23;
            this.metroComboBox6.Items.AddRange(new object[] {
            "FWIP-CARD-COM"});
            this.metroComboBox6.Location = new System.Drawing.Point(150, 122);
            this.metroComboBox6.Name = "metroComboBox6";
            this.metroComboBox6.Size = new System.Drawing.Size(194, 29);
            this.metroComboBox6.TabIndex = 108;
            this.metroComboBox6.UseSelectable = true;
            // 
            // metroTextBox14
            // 
            // 
            // 
            // 
            this.metroTextBox14.CustomButton.Image = null;
            this.metroTextBox14.CustomButton.Location = new System.Drawing.Point(170, 1);
            this.metroTextBox14.CustomButton.Name = "";
            this.metroTextBox14.CustomButton.Size = new System.Drawing.Size(27, 27);
            this.metroTextBox14.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox14.CustomButton.TabIndex = 1;
            this.metroTextBox14.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox14.CustomButton.UseSelectable = true;
            this.metroTextBox14.CustomButton.Visible = false;
            this.metroTextBox14.Lines = new string[0];
            this.metroTextBox14.Location = new System.Drawing.Point(574, 239);
            this.metroTextBox14.MaxLength = 32767;
            this.metroTextBox14.Multiline = true;
            this.metroTextBox14.Name = "metroTextBox14";
            this.metroTextBox14.PasswordChar = '\0';
#pragma warning disable CS0618 // 'MetroTextBox.PromptText' is obsolete: 'Use watermark'
            this.metroTextBox14.PromptText = "6220805-A";
#pragma warning restore CS0618 // 'MetroTextBox.PromptText' is obsolete: 'Use watermark'
            this.metroTextBox14.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox14.SelectedText = "";
            this.metroTextBox14.SelectionLength = 0;
            this.metroTextBox14.SelectionStart = 0;
            this.metroTextBox14.ShortcutsEnabled = true;
            this.metroTextBox14.Size = new System.Drawing.Size(198, 29);
            this.metroTextBox14.TabIndex = 107;
            this.metroTextBox14.UseSelectable = true;
            this.metroTextBox14.WaterMark = "6220805-A";
            this.metroTextBox14.WaterMarkColor = System.Drawing.Color.Gray;
            this.metroTextBox14.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel60
            // 
            this.metroLabel60.AutoSize = true;
            this.metroLabel60.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel60.Location = new System.Drawing.Point(574, 217);
            this.metroLabel60.Name = "metroLabel60";
            this.metroLabel60.Size = new System.Drawing.Size(204, 19);
            this.metroLabel60.TabIndex = 106;
            this.metroLabel60.Text = "ตัวอย่างหมายเลข Lot จากค่าที่ตั้ง";
            // 
            // metroLabel59
            // 
            this.metroLabel59.AutoSize = true;
            this.metroLabel59.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel59.Location = new System.Drawing.Point(345, 217);
            this.metroLabel59.Name = "metroLabel59";
            this.metroLabel59.Size = new System.Drawing.Size(50, 19);
            this.metroLabel59.TabIndex = 105;
            this.metroLabel59.Text = "ค่าที่ตั้ง";
            // 
            // metroLabel58
            // 
            this.metroLabel58.AutoSize = true;
            this.metroLabel58.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel58.Location = new System.Drawing.Point(150, 173);
            this.metroLabel58.Name = "metroLabel58";
            this.metroLabel58.Size = new System.Drawing.Size(121, 19);
            this.metroLabel58.TabIndex = 104;
            this.metroLabel58.Text = "ค่าทั้งหมดที่เลือกได้";
            // 
            // metroLabel47
            // 
            this.metroLabel47.AutoSize = true;
            this.metroLabel47.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel47.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel47.Location = new System.Drawing.Point(342, 16);
            this.metroLabel47.Name = "metroLabel47";
            this.metroLabel47.Size = new System.Drawing.Size(227, 25);
            this.metroLabel47.TabIndex = 104;
            this.metroLabel47.Text = "ตั้งค่าการสร้างหมายเลข Lot";
            // 
            // metroButton43
            // 
            this.metroButton43.Location = new System.Drawing.Point(293, 239);
            this.metroButton43.Name = "metroButton43";
            this.metroButton43.Size = new System.Drawing.Size(24, 23);
            this.metroButton43.TabIndex = 5;
            this.metroButton43.Text = ">>";
            this.metroButton43.UseSelectable = true;
            // 
            // listBox1
            // 
            this.listBox1.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 16;
            this.listBox1.Items.AddRange(new object[] {
            "[year]",
            "[month]",
            "[day]",
            "[factno]",
            "[machno]",
            "[shift]",
            "[weight]",
            "[length]"});
            this.listBox1.Location = new System.Drawing.Point(150, 195);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(122, 132);
            this.listBox1.TabIndex = 4;
            // 
            // metroTextBox13
            // 
            // 
            // 
            // 
            this.metroTextBox13.CustomButton.Image = null;
            this.metroTextBox13.CustomButton.Location = new System.Drawing.Point(170, 1);
            this.metroTextBox13.CustomButton.Name = "";
            this.metroTextBox13.CustomButton.Size = new System.Drawing.Size(27, 27);
            this.metroTextBox13.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox13.CustomButton.TabIndex = 1;
            this.metroTextBox13.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox13.CustomButton.UseSelectable = true;
            this.metroTextBox13.CustomButton.Visible = false;
            this.metroTextBox13.Lines = new string[] {
        "[year][factory][month][day]-[shift]"};
            this.metroTextBox13.Location = new System.Drawing.Point(345, 239);
            this.metroTextBox13.MaxLength = 32767;
            this.metroTextBox13.Multiline = true;
            this.metroTextBox13.Name = "metroTextBox13";
            this.metroTextBox13.PasswordChar = '\0';
            this.metroTextBox13.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox13.SelectedText = "";
            this.metroTextBox13.SelectionLength = 0;
            this.metroTextBox13.SelectionStart = 0;
            this.metroTextBox13.ShortcutsEnabled = true;
            this.metroTextBox13.Size = new System.Drawing.Size(198, 29);
            this.metroTextBox13.TabIndex = 3;
            this.metroTextBox13.Text = "[year][factory][month][day]-[shift]";
            this.metroTextBox13.UseSelectable = true;
            this.metroTextBox13.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox13.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTabPage3
            // 
            this.metroTabPage3.Controls.Add(this.metroButton44);
            this.metroTabPage3.Controls.Add(this.metroButton45);
            this.metroTabPage3.Controls.Add(this.pictureBox2);
            this.metroTabPage3.Controls.Add(this.metroLabel62);
            this.metroTabPage3.HorizontalScrollbarBarColor = true;
            this.metroTabPage3.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage3.HorizontalScrollbarSize = 10;
            this.metroTabPage3.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage3.Name = "metroTabPage3";
            this.metroTabPage3.Size = new System.Drawing.Size(970, 640);
            this.metroTabPage3.TabIndex = 6;
            this.metroTabPage3.Text = "ตั้งค่าวัตถุดิบ/สินค้า";
            this.metroTabPage3.VerticalScrollbarBarColor = true;
            this.metroTabPage3.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage3.VerticalScrollbarSize = 10;
            // 
            // metroButton44
            // 
            this.metroButton44.Location = new System.Drawing.Point(833, 596);
            this.metroButton44.Name = "metroButton44";
            this.metroButton44.Size = new System.Drawing.Size(79, 30);
            this.metroButton44.TabIndex = 107;
            this.metroButton44.Text = "ยกเลิก";
            this.metroButton44.UseSelectable = true;
            // 
            // metroButton45
            // 
            this.metroButton45.Location = new System.Drawing.Point(697, 596);
            this.metroButton45.Name = "metroButton45";
            this.metroButton45.Size = new System.Drawing.Size(130, 30);
            this.metroButton45.TabIndex = 106;
            this.metroButton45.Text = "ยืนยันการเปลี่ยนแปลง";
            this.metroButton45.UseSelectable = true;
            // 
            // metroLabel62
            // 
            this.metroLabel62.AutoSize = true;
            this.metroLabel62.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel62.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel62.Location = new System.Drawing.Point(369, 27);
            this.metroLabel62.Name = "metroLabel62";
            this.metroLabel62.Size = new System.Drawing.Size(169, 25);
            this.metroLabel62.TabIndex = 104;
            this.metroLabel62.Text = "ตั้งค่าวัตถุดิบ / สินค้า";
            // 
            // metroTabPage4
            // 
            this.metroTabPage4.Controls.Add(this.metroButton47);
            this.metroTabPage4.Controls.Add(this.metroButton48);
            this.metroTabPage4.Controls.Add(this.pictureBox7);
            this.metroTabPage4.Controls.Add(this.metroLabel63);
            this.metroTabPage4.HorizontalScrollbarBarColor = true;
            this.metroTabPage4.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage4.HorizontalScrollbarSize = 10;
            this.metroTabPage4.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage4.Name = "metroTabPage4";
            this.metroTabPage4.Size = new System.Drawing.Size(970, 640);
            this.metroTabPage4.TabIndex = 7;
            this.metroTabPage4.Text = "จัดการบรรจุภัณฑ์";
            this.metroTabPage4.VerticalScrollbarBarColor = true;
            this.metroTabPage4.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage4.VerticalScrollbarSize = 10;
            // 
            // metroButton47
            // 
            this.metroButton47.Location = new System.Drawing.Point(847, 595);
            this.metroButton47.Name = "metroButton47";
            this.metroButton47.Size = new System.Drawing.Size(79, 30);
            this.metroButton47.TabIndex = 105;
            this.metroButton47.Text = "ยกเลิก";
            this.metroButton47.UseSelectable = true;
            // 
            // metroButton48
            // 
            this.metroButton48.Location = new System.Drawing.Point(711, 595);
            this.metroButton48.Name = "metroButton48";
            this.metroButton48.Size = new System.Drawing.Size(130, 30);
            this.metroButton48.TabIndex = 104;
            this.metroButton48.Text = "ยืนยันการเปลี่ยนแปลง";
            this.metroButton48.UseSelectable = true;
            // 
            // metroLabel63
            // 
            this.metroLabel63.AutoSize = true;
            this.metroLabel63.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel63.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel63.Location = new System.Drawing.Point(397, 28);
            this.metroLabel63.Name = "metroLabel63";
            this.metroLabel63.Size = new System.Drawing.Size(136, 25);
            this.metroLabel63.TabIndex = 99;
            this.metroLabel63.Text = "ตั้งค่าบรรจุภัณฑ์";
            // 
            // metroLabel23
            // 
            this.metroLabel23.AutoSize = true;
            this.metroLabel23.Location = new System.Drawing.Point(1139, 530);
            this.metroLabel23.Name = "metroLabel23";
            this.metroLabel23.Size = new System.Drawing.Size(40, 19);
            this.metroLabel23.TabIndex = 102;
            this.metroLabel23.Text = "แผนก";
            // 
            // metroComboBox5
            // 
            this.metroComboBox5.DisplayMember = "SRM";
            this.metroComboBox5.FormattingEnabled = true;
            this.metroComboBox5.ItemHeight = 23;
            this.metroComboBox5.Items.AddRange(new object[] {
            "SQC",
            "SRM",
            "SPD2",
            "SPD4",
            "SFG"});
            this.metroComboBox5.Location = new System.Drawing.Point(1264, 530);
            this.metroComboBox5.Name = "metroComboBox5";
            this.metroComboBox5.Size = new System.Drawing.Size(215, 29);
            this.metroComboBox5.TabIndex = 101;
            this.metroComboBox5.UseSelectable = true;
            this.metroComboBox5.ValueMember = "SRM";
            // 
            // metroButton41
            // 
            this.metroButton41.Location = new System.Drawing.Point(1264, 565);
            this.metroButton41.Name = "metroButton41";
            this.metroButton41.Size = new System.Drawing.Size(130, 30);
            this.metroButton41.TabIndex = 100;
            this.metroButton41.Text = "ยืนยันการเปลี่ยนแปลง";
            this.metroButton41.UseSelectable = true;
            // 
            // metroTextBox3
            // 
            // 
            // 
            // 
            this.metroTextBox3.CustomButton.Image = null;
            this.metroTextBox3.CustomButton.Location = new System.Drawing.Point(187, 1);
            this.metroTextBox3.CustomButton.Name = "";
            this.metroTextBox3.CustomButton.Size = new System.Drawing.Size(27, 27);
            this.metroTextBox3.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox3.CustomButton.TabIndex = 1;
            this.metroTextBox3.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox3.CustomButton.UseSelectable = true;
            this.metroTextBox3.CustomButton.Visible = false;
            this.metroTextBox3.ForeColor = System.Drawing.Color.Gray;
            this.metroTextBox3.Lines = new string[0];
            this.metroTextBox3.Location = new System.Drawing.Point(1264, 459);
            this.metroTextBox3.MaxLength = 32767;
            this.metroTextBox3.Name = "metroTextBox3";
            this.metroTextBox3.PasswordChar = '\0';
            this.metroTextBox3.ReadOnly = true;
            this.metroTextBox3.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox3.SelectedText = "";
            this.metroTextBox3.SelectionLength = 0;
            this.metroTextBox3.SelectionStart = 0;
            this.metroTextBox3.ShortcutsEnabled = true;
            this.metroTextBox3.Size = new System.Drawing.Size(215, 29);
            this.metroTextBox3.TabIndex = 99;
            this.metroTextBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.metroTextBox3.UseCustomForeColor = true;
            this.metroTextBox3.UseSelectable = true;
            this.metroTextBox3.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox3.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel24
            // 
            this.metroLabel24.AutoSize = true;
            this.metroLabel24.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel24.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel24.Location = new System.Drawing.Point(1242, 376);
            this.metroLabel24.Name = "metroLabel24";
            this.metroLabel24.Size = new System.Drawing.Size(121, 25);
            this.metroLabel24.TabIndex = 98;
            this.metroLabel24.Text = "แก้ไขผู้ใช้งาน";
            // 
            // metroTextBox4
            // 
            // 
            // 
            // 
            this.metroTextBox4.CustomButton.Image = null;
            this.metroTextBox4.CustomButton.Location = new System.Drawing.Point(187, 1);
            this.metroTextBox4.CustomButton.Name = "";
            this.metroTextBox4.CustomButton.Size = new System.Drawing.Size(27, 27);
            this.metroTextBox4.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox4.CustomButton.TabIndex = 1;
            this.metroTextBox4.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox4.CustomButton.UseSelectable = true;
            this.metroTextBox4.CustomButton.Visible = false;
            this.metroTextBox4.ForeColor = System.Drawing.Color.Gray;
            this.metroTextBox4.Lines = new string[] {
        "john66"};
            this.metroTextBox4.Location = new System.Drawing.Point(1264, 424);
            this.metroTextBox4.MaxLength = 32767;
            this.metroTextBox4.Name = "metroTextBox4";
            this.metroTextBox4.PasswordChar = '\0';
            this.metroTextBox4.ReadOnly = true;
            this.metroTextBox4.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox4.SelectedText = "";
            this.metroTextBox4.SelectionLength = 0;
            this.metroTextBox4.SelectionStart = 0;
            this.metroTextBox4.ShortcutsEnabled = true;
            this.metroTextBox4.Size = new System.Drawing.Size(215, 29);
            this.metroTextBox4.TabIndex = 97;
            this.metroTextBox4.Text = "john66";
            this.metroTextBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.metroTextBox4.UseCustomForeColor = true;
            this.metroTextBox4.UseSelectable = true;
            this.metroTextBox4.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox4.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel34
            // 
            this.metroLabel34.AutoSize = true;
            this.metroLabel34.Location = new System.Drawing.Point(1139, 459);
            this.metroLabel34.Name = "metroLabel34";
            this.metroLabel34.Size = new System.Drawing.Size(72, 19);
            this.metroLabel34.TabIndex = 96;
            this.metroLabel34.Text = "รหัสผ่านใหม่";
            // 
            // metroLabel36
            // 
            this.metroLabel36.AutoSize = true;
            this.metroLabel36.Location = new System.Drawing.Point(1139, 424);
            this.metroLabel36.Name = "metroLabel36";
            this.metroLabel36.Size = new System.Drawing.Size(64, 19);
            this.metroLabel36.TabIndex = 95;
            this.metroLabel36.Text = "ชื่อผู้ใช้งาน";
            // 
            // metroButton42
            // 
            this.metroButton42.Location = new System.Drawing.Point(1400, 565);
            this.metroButton42.Name = "metroButton42";
            this.metroButton42.Size = new System.Drawing.Size(79, 30);
            this.metroButton42.TabIndex = 103;
            this.metroButton42.Text = "ยกเลิก";
            this.metroButton42.UseSelectable = true;
            // 
            // metroTextBox38
            // 
            // 
            // 
            // 
            this.metroTextBox38.CustomButton.Image = null;
            this.metroTextBox38.CustomButton.Location = new System.Drawing.Point(187, 1);
            this.metroTextBox38.CustomButton.Name = "";
            this.metroTextBox38.CustomButton.Size = new System.Drawing.Size(27, 27);
            this.metroTextBox38.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox38.CustomButton.TabIndex = 1;
            this.metroTextBox38.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox38.CustomButton.UseSelectable = true;
            this.metroTextBox38.CustomButton.Visible = false;
            this.metroTextBox38.ForeColor = System.Drawing.Color.Gray;
            this.metroTextBox38.Lines = new string[0];
            this.metroTextBox38.Location = new System.Drawing.Point(1264, 495);
            this.metroTextBox38.MaxLength = 32767;
            this.metroTextBox38.Name = "metroTextBox38";
            this.metroTextBox38.PasswordChar = '\0';
            this.metroTextBox38.ReadOnly = true;
            this.metroTextBox38.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox38.SelectedText = "";
            this.metroTextBox38.SelectionLength = 0;
            this.metroTextBox38.SelectionStart = 0;
            this.metroTextBox38.ShortcutsEnabled = true;
            this.metroTextBox38.Size = new System.Drawing.Size(215, 29);
            this.metroTextBox38.TabIndex = 105;
            this.metroTextBox38.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.metroTextBox38.UseCustomForeColor = true;
            this.metroTextBox38.UseSelectable = true;
            this.metroTextBox38.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox38.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel64
            // 
            this.metroLabel64.AutoSize = true;
            this.metroLabel64.Location = new System.Drawing.Point(1139, 495);
            this.metroLabel64.Name = "metroLabel64";
            this.metroLabel64.Size = new System.Drawing.Size(117, 19);
            this.metroLabel64.TabIndex = 104;
            this.metroLabel64.Text = "ยืนยันรหัสผ่านอีกครั้ง";
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::SWS.Properties.Resources.scrollbar;
            this.pictureBox4.Location = new System.Drawing.Point(894, 374);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(17, 215);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 44;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::SWS.Properties.Resources.lotlist2;
            this.pictureBox3.Location = new System.Drawing.Point(16, 374);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(878, 215);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 43;
            this.pictureBox3.TabStop = false;
            // 
            // metroButton9
            // 
            this.metroButton9.BackgroundImage = global::SWS.Properties.Resources.refresh_arrow;
            this.metroButton9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.metroButton9.Location = new System.Drawing.Point(844, 132);
            this.metroButton9.Margin = new System.Windows.Forms.Padding(10);
            this.metroButton9.Name = "metroButton9";
            this.metroButton9.Size = new System.Drawing.Size(67, 23);
            this.metroButton9.TabIndex = 41;
            this.metroButton9.UseSelectable = true;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::SWS.Properties.Resources.scrollbar;
            this.pictureBox5.Location = new System.Drawing.Point(926, 338);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(17, 139);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 93;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = global::SWS.Properties.Resources.issueItemList1;
            this.pictureBox6.Location = new System.Drawing.Point(35, 338);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(891, 139);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 92;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(3, 275);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(853, 490);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 96;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(21, 67);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(919, 512);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 105;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox7.Image")));
            this.pictureBox7.Location = new System.Drawing.Point(25, 81);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(919, 401);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox7.TabIndex = 100;
            this.pictureBox7.TabStop = false;
            // 
            // FormAdmin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1024, 768);
            this.Controls.Add(this.metroTextBox38);
            this.Controls.Add(this.metroLabel64);
            this.Controls.Add(this.metroButton42);
            this.Controls.Add(this.metroLabel23);
            this.Controls.Add(this.metroComboBox5);
            this.Controls.Add(this.metroButton41);
            this.Controls.Add(this.metroTextBox3);
            this.Controls.Add(this.metroLabel24);
            this.Controls.Add(this.metroTextBox4);
            this.Controls.Add(this.metroLabel34);
            this.Controls.Add(this.metroLabel36);
            this.Controls.Add(this.metroTabControl1);
            this.Name = "FormAdmin";
            this.Style = MetroFramework.MetroColorStyle.Lime;
            this.Text = "SWS Barcode System";
            this.Load += new System.EventHandler(this.FormAdmin_Load);
            this.metroTabControl1.ResumeLayout(false);
            this.genLotTabPage.ResumeLayout(false);
            this.genLotPanel1.ResumeLayout(false);
            this.genLotPanel1.PerformLayout();
            this.genLotPanel2.ResumeLayout(false);
            this.genLotPanel2.PerformLayout();
            this.metroPanel7.ResumeLayout(false);
            this.metroPanel7.PerformLayout();
            this.metroPanel8.ResumeLayout(false);
            this.metroPanel8.PerformLayout();
            this.metroPanel9.ResumeLayout(false);
            this.metroPanel9.PerformLayout();
            this.metroPanel4.ResumeLayout(false);
            this.metroPanel4.PerformLayout();
            this.issueTabPage.ResumeLayout(false);
            this.issuePanel1.ResumeLayout(false);
            this.issuePanel1.PerformLayout();
            this.issuePanel2.ResumeLayout(false);
            this.issuePanel2.PerformLayout();
            this.metroTabPage1.ResumeLayout(false);
            this.metroTabPage1.PerformLayout();
            this.metroTabPage2.ResumeLayout(false);
            this.metroTabPage2.PerformLayout();
            this.metroTabPage3.ResumeLayout(false);
            this.metroTabPage3.PerformLayout();
            this.metroTabPage4.ResumeLayout(false);
            this.metroTabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public MetroFramework.Controls.MetroTabControl metroTabControl1;
        private MetroFramework.Controls.MetroTabPage genLotTabPage;
        private MetroFramework.Controls.MetroPanel genLotPanel1;
        private MetroFramework.Controls.MetroLabel metroLabel38;
        private MetroFramework.Controls.MetroButton metroButton15;
        private MetroFramework.Controls.MetroButton metroButton10;
        private MetroFramework.Controls.MetroLabel metroLabel35;
        private MetroFramework.Controls.MetroLabel metroLabel30;
        private MetroFramework.Controls.MetroTextBox metroTextBox26;
        private MetroFramework.Controls.MetroPanel genLotPanel2;
        private MetroFramework.Controls.MetroPanel metroPanel7;
        private MetroFramework.Controls.MetroPanel metroPanel8;
        private MetroFramework.Controls.MetroLabel metroLabel37;
        private MetroFramework.Controls.MetroTextBox metroTextBox21;
        private MetroFramework.Controls.MetroPanel metroPanel9;
        private MetroFramework.Controls.MetroTextBox metroTextBox22;
        private MetroFramework.Controls.MetroLabel metroLabel41;
        private MetroFramework.Controls.MetroLabel metroLabel45;
        private MetroFramework.Controls.MetroTextBox metroTextBox25;
        private MetroFramework.Controls.MetroTextBox metroTextBox24;
        private MetroFramework.Controls.MetroTextBox metroTextBox23;
        private MetroFramework.Controls.MetroTextBox metroTextBox20;
        private MetroFramework.Controls.MetroTextBox metroTextBox18;
        private MetroFramework.Controls.MetroLabel metroLabel29;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private MetroFramework.Controls.MetroPanel metroPanel4;
        private MetroFramework.Controls.MetroCheckBox metroCheckBox1;
        private MetroFramework.Controls.MetroLabel metroLabel22;
        private MetroFramework.Controls.MetroTextBox metroTextBox11;
        private MetroFramework.Controls.MetroTextBox metroTextBox12;
        private MetroFramework.Controls.MetroLabel metroLabel21;
        private MetroFramework.Controls.MetroButton metroButton9;
        private MetroFramework.Controls.MetroButton metroButton8;
        private MetroFramework.Controls.MetroButton metroButton4;
        private MetroFramework.Controls.MetroButton metroButton5;
        private MetroFramework.Controls.MetroButton metroButton6;
        private MetroFramework.Controls.MetroButton metroButton7;
        private MetroFramework.Controls.MetroComboBox metroComboBox1;
        private MetroFramework.Controls.MetroComboBox metroComboBox2;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel metroLabel14;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroLabel metroLabel15;
        private MetroFramework.Controls.MetroTextBox metroTextBox10;
        private MetroFramework.Controls.MetroTextBox metroTextBox5;
        private MetroFramework.Controls.MetroLabel metroLabel13;
        private MetroFramework.Controls.MetroTextBox metroTextBox6;
        private MetroFramework.Controls.MetroLabel metroLabel12;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private MetroFramework.Controls.MetroLabel metroLabel11;
        private MetroFramework.Controls.MetroLabel metroLabel16;
        private MetroFramework.Controls.MetroTextBox metroTextBox7;
        private MetroFramework.Controls.MetroLabel metroLabel17;
        private MetroFramework.Controls.MetroTextBox metroTextBox8;
        private MetroFramework.Controls.MetroLabel metroLabel18;
        private MetroFramework.Controls.MetroTextBox metroTextBox9;
        private MetroFramework.Controls.MetroLabel metroLabel19;
        private MetroFramework.Controls.MetroLabel metroLabel20;
        private MetroFramework.Controls.MetroTabPage issueTabPage;
        private MetroFramework.Controls.MetroPanel issuePanel1;
        private MetroFramework.Controls.MetroLabel metroLabel39;
        private MetroFramework.Controls.MetroButton metroButton16;
        private MetroFramework.Controls.MetroButton metroButton17;
        private MetroFramework.Controls.MetroLabel metroLabel42;
        private MetroFramework.Controls.MetroLabel metroLabel43;
        private MetroFramework.Controls.MetroTextBox metroTextBox27;
        private MetroFramework.Controls.MetroPanel issuePanel2;
        private MetroFramework.Controls.MetroButton metroButton19;
        private MetroFramework.Controls.MetroButton metroButton18;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private MetroFramework.Controls.MetroLabel metroLabel33;
        private MetroFramework.Controls.MetroLabel metroLabel32;
        private MetroFramework.Controls.MetroTextBox metroTextBox19;
        private MetroFramework.Controls.MetroLabel metroLabel31;
        private MetroFramework.Controls.MetroTextBox metroTextBox17;
        private MetroFramework.Controls.MetroLabel metroLabel28;
        private MetroFramework.Controls.MetroTextBox metroTextBox15;
        private MetroFramework.Controls.MetroTextBox metroTextBox16;
        private MetroFramework.Controls.MetroTextBox metroTextBox28;
        private MetroFramework.Controls.MetroTextBox metroTextBox29;
        private MetroFramework.Controls.MetroTextBox metroTextBox30;
        private MetroFramework.Controls.MetroTextBox metroTextBox31;
        private MetroFramework.Controls.MetroTextBox metroTextBox32;
        private MetroFramework.Controls.MetroLabel metroLabel44;
        private MetroFramework.Controls.MetroComboBox metroComboBox3;
        private MetroFramework.Controls.MetroLabel metroLabel46;
        private MetroFramework.Controls.MetroLabel metroLabel48;
        private MetroFramework.Controls.MetroLabel metroLabel49;
        private MetroFramework.Controls.MetroTextBox metroTextBox33;
        private MetroFramework.Controls.MetroTextBox metroTextBox34;
        private MetroFramework.Controls.MetroLabel metroLabel50;
        private MetroFramework.Controls.MetroTextBox metroTextBox35;
        private MetroFramework.Controls.MetroLabel metroLabel51;
        private MetroFramework.Controls.MetroLabel metroLabel52;
        private MetroFramework.Controls.MetroLabel metroLabel53;
        private MetroFramework.Controls.MetroLabel metroLabel54;
        private MetroFramework.Controls.MetroLabel metroLabel55;
        private MetroFramework.Controls.MetroTextBox metroTextBox36;
        private MetroFramework.Controls.MetroLabel metroLabel56;
        private MetroFramework.Controls.MetroTextBox metroTextBox37;
        private MetroFramework.Controls.MetroLabel metroLabel57;
        private MetroFramework.Controls.MetroLabel metroLabel26;
        private MetroFramework.Controls.MetroLabel metroLabel40;
        private MetroFramework.Controls.MetroButton metroButton11;
        private MetroFramework.Controls.MetroButton metroButton13;
        private MetroFramework.Controls.MetroButton metroButton14;
        private MetroFramework.Controls.MetroLabel metroLabel25;
        private MetroFramework.Controls.MetroLabel metroLabel27;
        private MetroFramework.Controls.MetroTabPage transferTabPage;
        private MetroFramework.Controls.MetroTabPage delArrTabPage;
        private MetroFramework.Controls.MetroTabPage metroTabPage1;
        private MetroFramework.Controls.MetroTabPage metroTabPage2;
        private MetroFramework.Controls.MetroTabPage metroTabPage3;
        private MetroFramework.Controls.MetroTabPage metroTabPage4;
        private MetroFramework.Controls.MetroComboBox metroComboBox4;
        private MetroFramework.Controls.MetroButton metroButton1;
        private MetroFramework.Controls.MetroTextBox metroTextBox1;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroTextBox metroTextBox2;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroButton metroButton21;
        private MetroFramework.Controls.MetroButton metroButton22;
        private MetroFramework.Controls.MetroButton metroButton23;
        private MetroFramework.Controls.MetroButton metroButton24;
        private MetroFramework.Controls.MetroButton metroButton12;
        private MetroFramework.Controls.MetroButton metroButton20;
        private MetroFramework.Controls.MetroButton metroButton3;
        private MetroFramework.Controls.MetroButton metroButton2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private MetroFramework.Controls.MetroButton metroButton33;
        private MetroFramework.Controls.MetroButton metroButton34;
        private MetroFramework.Controls.MetroButton metroButton35;
        private MetroFramework.Controls.MetroButton metroButton36;
        private MetroFramework.Controls.MetroButton metroButton37;
        private MetroFramework.Controls.MetroButton metroButton38;
        private MetroFramework.Controls.MetroButton metroButton39;
        private MetroFramework.Controls.MetroButton metroButton40;
        private MetroFramework.Controls.MetroButton metroButton25;
        private MetroFramework.Controls.MetroButton metroButton26;
        private MetroFramework.Controls.MetroButton metroButton27;
        private MetroFramework.Controls.MetroButton metroButton28;
        private MetroFramework.Controls.MetroButton metroButton29;
        private MetroFramework.Controls.MetroButton metroButton30;
        private MetroFramework.Controls.MetroButton metroButton31;
        private MetroFramework.Controls.MetroButton metroButton32;
        private MetroFramework.Controls.MetroLabel metroLabel23;
        private MetroFramework.Controls.MetroComboBox metroComboBox5;
        private MetroFramework.Controls.MetroButton metroButton41;
        private MetroFramework.Controls.MetroTextBox metroTextBox3;
        private MetroFramework.Controls.MetroLabel metroLabel24;
        private MetroFramework.Controls.MetroTextBox metroTextBox4;
        private MetroFramework.Controls.MetroLabel metroLabel34;
        private MetroFramework.Controls.MetroLabel metroLabel36;
        private MetroFramework.Controls.MetroButton metroButton42;
        private System.Windows.Forms.ListBox listBox1;
        private MetroFramework.Controls.MetroTextBox metroTextBox13;
        private MetroFramework.Controls.MetroTextBox metroTextBox14;
        private MetroFramework.Controls.MetroLabel metroLabel60;
        private MetroFramework.Controls.MetroLabel metroLabel59;
        private MetroFramework.Controls.MetroLabel metroLabel58;
        private MetroFramework.Controls.MetroLabel metroLabel47;
        private MetroFramework.Controls.MetroButton metroButton43;
        private MetroFramework.Controls.MetroLabel metroLabel61;
        private MetroFramework.Controls.MetroComboBox metroComboBox6;
        private MetroFramework.Controls.MetroButton metroButton46;
        private MetroFramework.Controls.MetroButton metroButton44;
        private MetroFramework.Controls.MetroButton metroButton45;
        private System.Windows.Forms.PictureBox pictureBox2;
        private MetroFramework.Controls.MetroLabel metroLabel62;
        private MetroFramework.Controls.MetroButton metroButton47;
        private MetroFramework.Controls.MetroButton metroButton48;
        private System.Windows.Forms.PictureBox pictureBox7;
        private MetroFramework.Controls.MetroLabel metroLabel63;
        private MetroFramework.Controls.MetroTextBox metroTextBox38;
        private MetroFramework.Controls.MetroLabel metroLabel64;
        private MetroFramework.Controls.MetroTabPage metroTabPage5;
    }
}