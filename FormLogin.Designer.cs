﻿using MetroFramework.Forms;

namespace SWS
{
    partial class FormLogin : MetroForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroTextBox32 = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel44 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox35 = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel25 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel27 = new MetroFramework.Controls.MetroLabel();
            this.metroPanel2 = new MetroFramework.Controls.MetroPanel();
            this.metroButton17 = new MetroFramework.Controls.MetroButton();
            this.metroPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroTextBox32
            // 
            // 
            // 
            // 
            this.metroTextBox32.CustomButton.Image = null;
            this.metroTextBox32.CustomButton.Location = new System.Drawing.Point(193, 1);
            this.metroTextBox32.CustomButton.Name = "";
            this.metroTextBox32.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox32.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox32.CustomButton.TabIndex = 1;
            this.metroTextBox32.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox32.CustomButton.UseSelectable = true;
            this.metroTextBox32.CustomButton.Visible = false;
            this.metroTextBox32.ForeColor = System.Drawing.Color.Gray;
            this.metroTextBox32.Lines = new string[] {
        "Password"};
            this.metroTextBox32.Location = new System.Drawing.Point(408, 276);
            this.metroTextBox32.MaxLength = 32767;
            this.metroTextBox32.Name = "metroTextBox32";
            this.metroTextBox32.PasswordChar = '\0';
            this.metroTextBox32.ReadOnly = true;
            this.metroTextBox32.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox32.SelectedText = "";
            this.metroTextBox32.SelectionLength = 0;
            this.metroTextBox32.SelectionStart = 0;
            this.metroTextBox32.ShortcutsEnabled = true;
            this.metroTextBox32.Size = new System.Drawing.Size(215, 23);
            this.metroTextBox32.TabIndex = 85;
            this.metroTextBox32.Text = "Password";
            this.metroTextBox32.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.metroTextBox32.UseCustomForeColor = true;
            this.metroTextBox32.UseSelectable = true;
            this.metroTextBox32.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox32.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel44
            // 
            this.metroLabel44.AutoSize = true;
            this.metroLabel44.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel44.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel44.Location = new System.Drawing.Point(430, 165);
            this.metroLabel44.Name = "metroLabel44";
            this.metroLabel44.Size = new System.Drawing.Size(90, 25);
            this.metroLabel44.TabIndex = 84;
            this.metroLabel44.Text = "เข้าสู่ระบบ";
            // 
            // metroTextBox35
            // 
            // 
            // 
            // 
            this.metroTextBox35.CustomButton.Image = null;
            this.metroTextBox35.CustomButton.Location = new System.Drawing.Point(193, 1);
            this.metroTextBox35.CustomButton.Name = "";
            this.metroTextBox35.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox35.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox35.CustomButton.TabIndex = 1;
            this.metroTextBox35.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox35.CustomButton.UseSelectable = true;
            this.metroTextBox35.CustomButton.Visible = false;
            this.metroTextBox35.ForeColor = System.Drawing.Color.Gray;
            this.metroTextBox35.Lines = new string[] {
        "Username"};
            this.metroTextBox35.Location = new System.Drawing.Point(408, 245);
            this.metroTextBox35.MaxLength = 32767;
            this.metroTextBox35.Name = "metroTextBox35";
            this.metroTextBox35.PasswordChar = '\0';
            this.metroTextBox35.ReadOnly = true;
            this.metroTextBox35.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox35.SelectedText = "";
            this.metroTextBox35.SelectionLength = 0;
            this.metroTextBox35.SelectionStart = 0;
            this.metroTextBox35.ShortcutsEnabled = true;
            this.metroTextBox35.Size = new System.Drawing.Size(215, 23);
            this.metroTextBox35.TabIndex = 83;
            this.metroTextBox35.Text = "Username";
            this.metroTextBox35.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.metroTextBox35.UseCustomForeColor = true;
            this.metroTextBox35.UseSelectable = true;
            this.metroTextBox35.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox35.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel25
            // 
            this.metroLabel25.AutoSize = true;
            this.metroLabel25.Location = new System.Drawing.Point(327, 245);
            this.metroLabel25.Name = "metroLabel25";
            this.metroLabel25.Size = new System.Drawing.Size(64, 19);
            this.metroLabel25.TabIndex = 79;
            this.metroLabel25.Text = "ชื่อผู้ใช้งาน";
            // 
            // metroLabel27
            // 
            this.metroLabel27.AutoSize = true;
            this.metroLabel27.Location = new System.Drawing.Point(327, 276);
            this.metroLabel27.Name = "metroLabel27";
            this.metroLabel27.Size = new System.Drawing.Size(53, 19);
            this.metroLabel27.TabIndex = 80;
            this.metroLabel27.Text = "รหัสผ่าน";
            // 
            // metroPanel2
            // 
            this.metroPanel2.Controls.Add(this.metroButton17);
            this.metroPanel2.Controls.Add(this.metroTextBox32);
            this.metroPanel2.Controls.Add(this.metroLabel44);
            this.metroPanel2.Controls.Add(this.metroTextBox35);
            this.metroPanel2.Controls.Add(this.metroLabel27);
            this.metroPanel2.Controls.Add(this.metroLabel25);
            this.metroPanel2.HorizontalScrollbarBarColor = true;
            this.metroPanel2.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel2.HorizontalScrollbarSize = 10;
            this.metroPanel2.Location = new System.Drawing.Point(23, 63);
            this.metroPanel2.Name = "metroPanel2";
            this.metroPanel2.Size = new System.Drawing.Size(978, 682);
            this.metroPanel2.TabIndex = 86;
            this.metroPanel2.VerticalScrollbarBarColor = true;
            this.metroPanel2.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel2.VerticalScrollbarSize = 10;
            // 
            // metroButton17
            // 
            this.metroButton17.Location = new System.Drawing.Point(408, 362);
            this.metroButton17.Name = "metroButton17";
            this.metroButton17.Size = new System.Drawing.Size(161, 30);
            this.metroButton17.TabIndex = 86;
            this.metroButton17.Text = "ยืนยัน";
            this.metroButton17.UseSelectable = true;
            // 
            // FormLogin
            // 
            this.ClientSize = new System.Drawing.Size(1024, 768);
            this.Controls.Add(this.metroPanel2);
            this.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "FormLogin";
            this.Style = MetroFramework.MetroColorStyle.Default;
            this.Text = "SWS Barcode System";
            this.metroPanel2.ResumeLayout(false);
            this.metroPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

#pragma warning disable CS0169 // The field 'FormLogin.metroTextBox1' is never used
        private MetroFramework.Controls.MetroTextBox metroTextBox1;
#pragma warning restore CS0169 // The field 'FormLogin.metroTextBox1' is never used
#pragma warning disable CS0169 // The field 'FormLogin.metroLabel1' is never used
        private MetroFramework.Controls.MetroLabel metroLabel1;
#pragma warning restore CS0169 // The field 'FormLogin.metroLabel1' is never used
#pragma warning disable CS0169 // The field 'FormLogin.metroLabel2' is never used
        private MetroFramework.Controls.MetroLabel metroLabel2;
#pragma warning restore CS0169 // The field 'FormLogin.metroLabel2' is never used
#pragma warning disable CS0169 // The field 'FormLogin.metroTextBox3' is never used
        private MetroFramework.Controls.MetroTextBox metroTextBox3;
#pragma warning restore CS0169 // The field 'FormLogin.metroTextBox3' is never used
#pragma warning disable CS0169 // The field 'FormLogin.metroLabel29' is never used
        private MetroFramework.Controls.MetroLabel metroLabel29;
#pragma warning restore CS0169 // The field 'FormLogin.metroLabel29' is never used
#pragma warning disable CS0169 // The field 'FormLogin.metroButton7' is never used
        private MetroFramework.Controls.MetroButton metroButton7;
#pragma warning restore CS0169 // The field 'FormLogin.metroButton7' is never used
#pragma warning disable CS0169 // The field 'FormLogin.metroPanel1' is never used
        private MetroFramework.Controls.MetroPanel metroPanel1;
#pragma warning restore CS0169 // The field 'FormLogin.metroPanel1' is never used
        private MetroFramework.Controls.MetroTextBox metroTextBox32;
        private MetroFramework.Controls.MetroLabel metroLabel44;
        private MetroFramework.Controls.MetroTextBox metroTextBox35;
        private MetroFramework.Controls.MetroLabel metroLabel25;
        private MetroFramework.Controls.MetroLabel metroLabel27;
        private MetroFramework.Controls.MetroPanel metroPanel2;
        private MetroFramework.Controls.MetroButton metroButton17;
    }
}