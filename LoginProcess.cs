﻿using System;
using System.Linq;
using System.Windows.Forms;
using MetroFramework.Controls;
using SWS.Model;

namespace SWS
{
    public partial class Inventory
    {
        private void lGSubmitBtn_Click(object sender, EventArgs e)
        {
            log.Info("Start Method: lGSubmitBtn_Click() No parameter");
            Cursor.Current = Cursors.WaitCursor;

            try
            {
                var username = lGUserNameTxb.Text;
                var password = lGPasswordTxb.Text;

                var hashedPswd = ComputeSha256Hash(password);

                _currentUser = _entities.User.FirstOrDefault(u =>
                    u.UserName.ToUpper() == username.ToUpper() && u.Password == hashedPswd);

                if (_currentUser == null)
                {
                    MessageBox.Show("ไม่พบผู้ใช้งาน หรือ รหัสผ่านไม่ถูกต้อง");
                    return;
                }

                if (_currentUser.FirstLogin ?? false)
                {
                    lGFirstLogCurrentUserLbl.Text = _currentUser.UserName;
                    lGFirstLogPnl.BringToFront();
                    lGFirstLogPnl.Visible = true;
                    return;
                }

                HandleLoginSequence();
            }
            catch (Exception ex)
            {
                log.Error("Method: lGSubmitBtn_Click() Error: " + ex.Message); MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                WriteLog(0, "Log in", "Log in", $"user:{_currentUser?.UserName} เข้าสู่ระบบ Role:{_currentUser?.Role?.Name}");
                log.Info("End Method: lGSubmitBtn_Click()");
                Cursor.Current = Cursors.Default;
            }
        }

        private void HandleLoginSequence()
        {
            _currentUser.LastLoginDate = DateTime.Now;
            _entities.SaveChanges();

            RemoveAllTabPages();
            mainTabControl.TabPages.Add(secretTabPage);
            mainTabControl.SelectedTab = secretTabPage;

            switch (_currentUser.Role.Name.Trim())
            {
                case "ADM":
                    mainTabControl.TabPages.Add(genLotTabPage);
                    mainTabControl.TabPages.Add(issueTabPage);
                    mainTabControl.TabPages.Add(delArrTabPage);
                    mainTabControl.TabPages.Add(transferTabPage);
                    mainTabControl.TabPages.Add(reportTabPage);
                    mainTabControl.TabPages.Add(adminUserTabPage);
                    mainTabControl.TabPages.Add(lotSetupTabPage);
                    mainTabControl.TabPages.Add(itemSetupTabPage);
                    mainTabControl.TabPages.Add(docSetupTabPage);
                    mainTabControl.TabPages.Add(reprintTabPage);

                    mainTabControl.SelectedTab = genLotTabPage;
                    break;
                case "SRM":
                    mainTabControl.TabPages.Add(genLotTabPage);
                    mainTabControl.TabPages.Add(issueTabPage);
                    mainTabControl.TabPages.Add(transferTabPage);
                    mainTabControl.TabPages.Add(reportTabPage);
                    mainTabControl.TabPages.Add(reprintTabPage);

                    mainTabControl.SelectedTab = genLotTabPage;
                    break;
                case "SP2":
                    mainTabControl.TabPages.Add(issueTabPage);
                    mainTabControl.TabPages.Add(transferTabPage);
                    mainTabControl.TabPages.Add(reportTabPage);
                    mainTabControl.TabPages.Add(reprintTabPage);

                    mainTabControl.SelectedTab = issueTabPage;
                    break;
                case "SP4":
                    mainTabControl.TabPages.Add(issueTabPage);
                    mainTabControl.TabPages.Add(transferTabPage);
                    mainTabControl.TabPages.Add(reportTabPage);
                    mainTabControl.TabPages.Add(reprintTabPage);

                    mainTabControl.SelectedTab = issueTabPage;
                    break;
                case "SMG":
                    mainTabControl.TabPages.Add(genLotTabPage);
                    mainTabControl.TabPages.Add(issueTabPage);
                    mainTabControl.TabPages.Add(delArrTabPage);
                    mainTabControl.TabPages.Add(transferTabPage);
                    mainTabControl.TabPages.Add(reportTabPage);
                    mainTabControl.TabPages.Add(reprintTabPage);

                    mainTabControl.SelectedTab = genLotTabPage;
                    break;
                case "WRM":
                    mainTabControl.TabPages.Add(issueTabPage);
                    mainTabControl.TabPages.Add(transferTabPage);
                    mainTabControl.TabPages.Add(reportTabPage);
                    mainTabControl.TabPages.Add(reprintTabPage);

                    mainTabControl.SelectedTab = issueTabPage;
                    break;
                case "WPD":
                    mainTabControl.TabPages.Add(issueTabPage);
                    mainTabControl.TabPages.Add(transferTabPage);
                    mainTabControl.TabPages.Add(reportTabPage);
                    mainTabControl.TabPages.Add(reprintTabPage);

                    mainTabControl.SelectedTab = issueTabPage;
                    break;
                case "WMG":
                    mainTabControl.TabPages.Add(issueTabPage);
                    mainTabControl.TabPages.Add(delArrTabPage);
                    mainTabControl.TabPages.Add(transferTabPage);
                    mainTabControl.TabPages.Add(reportTabPage);
                    mainTabControl.TabPages.Add(reprintTabPage);

                    mainTabControl.SelectedTab = issueTabPage;
                    break;
                case "DEL":
                    mainTabControl.TabPages.Add(delArrTabPage);
                    mainTabControl.TabPages.Add(transferTabPage);
                    mainTabControl.TabPages.Add(reportTabPage);

                    mainTabControl.SelectedTab = delArrTabPage;
                    break;
                case "JRO":
                    mainTabControl.TabPages.Add(issueTabPage);

                    iP2PrintFormBtn.Visible = false;
                    iP2ScanBarcodeIssueBtn.Visible = false;

                    mainTabControl.SelectedTab = issueTabPage;
                    break;
            }

            mainTabControl.TabPages.Remove(secretTabPage);
            lGFormPnl.Hide();
            logoutLnk.Visible = true;
            lGPasswordTxb.Text = "";
            mainTabControl.Show();
        }

        private void RefreshTabPages()
        {
            //mainTabControl.SelectedTab = genLotTabPage;
            //mainTabControl.SelectedTab = transferTabPage;
            //mainTabControl.SelectedTab = issueTabPage;
            //mainTabControl.SelectedTab = delArrTabPage;
            //mainTabControl.SelectedTab = adminUserTabPage;
        }

        private void RemoveAllTabPages()
        {

            mainTabControl.TabPages.Clear();

            //mainTabControl.TabPages.Remove(secretTabPage);
            //mainTabControl.TabPages.Remove(genLotTabPage);
            //mainTabControl.TabPages.Remove(issueTabPage);
            //mainTabControl.TabPages.Remove(transferTabPage);
            //mainTabControl.TabPages.Remove(delArrTabPage);
            //mainTabControl.TabPages.Remove(reportTabPage);
            //mainTabControl.TabPages.Remove(adminUserTabPage);
            //mainTabControl.TabPages.Remove(lotSetupTabPage);
            //mainTabControl.TabPages.Remove(itemSetupTabPage);
            //mainTabControl.TabPages.Remove(docSetupTabPage);
        }

        private void LGMagicBtn_Click(object sender, EventArgs e)
        {
            log.Info("Start Method: LGMagicBtn_Click() No parameter");

            try
            {

                lGUserNameTxb.Text = @"admin";
                lGPasswordTxb.Text = @"P@ssw0rd";

                //gL1GrNoTxb.Text = "19070083";
                //gL1ItemCodeCbb.Items.Add("RM-USA-MD");
                //gL1ItemCodeCbb.SelectedItem = "RM-USA-MD";

                lGSubmitBtn_Click(sender, e);

                //_currentUser = _entities.User.FirstOrDefault(u => u.UserName == "admin");

                //if (_currentUser == null)
                //{
                //    var role = _entities.Role.FirstOrDefault(r => r.ID == 1 && r.Name.ToUpper() == "ADMIN");

                //    if (role == null)
                //    {
                //        role = new Role()
                //        {
                //            Name = "admin",
                //            ID = 1,
                //        };
                //        _entities.Role.Add(role);
                //    }

                //    _entities.User.Add(new User()
                //    {
                //        UserName = "admin",
                //        Password = ComputeSha256Hash("P@ssw0rd"),
                //        CreationDate = DateTime.Now,
                //        FirstLogin = false,
                //        Role = role
                //    });
                //    _entities.SaveChanges();
                //}

                //lGFormPnl.Hide();
                //mainTabControl.Show();
                //logoutLnk.Visible = true;
                //mainTabControl.SelectedTab = genLotTabPage;
            }
            catch (Exception ex)
            {
                log.Error("Method: LGMagicBtn_Click() Error: " + ex.Message); MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: LGMagicBtn_Click()");
            }
        }

    }
}