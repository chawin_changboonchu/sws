﻿using MetroFramework.Controls;
using Spire.Xls;
using SWS.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;

namespace SWS
{
    public partial class Inventory
    {
        public void TR2Pnl_KeyPress(Object sender, KeyPressEventArgs e)
        {
            log.Info("Start Method: TR2Pnl_KeyPress() No parameter");

            try
            {
                if (e.KeyChar != (char)Keys.Return)
                {
                    _tr2BarcodeBuffer += e.KeyChar;
                    return;
                }

                HandleTR2ScanAction(_tr2BarcodeBuffer);

                _tr2BarcodeBuffer = "";
            }
            catch (Exception ex)
            {
                log.Error("Method: TR2Pnl_KeyPress() Error: " + ex.Message); MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: TR2Pnl_KeyPress()");
            }
        }

        public void TR1Pnl_KeyPress(Object sender, KeyPressEventArgs e)
        {
            log.Info("Start Method: TR1Pnl_KeyPress() No parameter");

            try
            {
                if (e.KeyChar != (char)Keys.Return)
                {
                    _tr1BarcodeBuffer += e.KeyChar;
                    return;
                }

                HandleTR1SubmitAction();

                _tr1BarcodeBuffer = "";
            }
            catch (Exception ex)
            {
                log.Error("Method: TR1Pnl_KeyPress() Error: " + ex.Message); MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: TR1Pnl_KeyPress()");
            }
        }

        private void HandleTR1SubmitAction()
        {
            log.Info("Start Method: HandleTR1SubmitAction() No parameter");

            try
            {
                var trNo = tR1TransferNoTxb.Text.Trim();

                if (trNo == "" && string.IsNullOrEmpty(_tr1BarcodeBuffer))
                {
                    MessageBox.Show("กรุณากรอกหมายเลขใบจัดของ");
                    return;
                }

                if (!string.IsNullOrEmpty(_tr1BarcodeBuffer))
                {
                    var docNo = _tr1BarcodeBuffer.Trim();
                    _transferInstance = _entities.Transfer.FirstOrDefault(tr => tr.DocNo == docNo);
                    if (_transferInstance == null)
                    {
                        if (int.TryParse(docNo, out var docNoInt))
                        {
                            var matchedTrList = _b1TransferReqList.FirstOrDefault(rq => rq.DocNum == docNoInt);

                            if (matchedTrList != null)
                            {
                                tR2TotalCountTxb.Text = "";
                                tR2TotalGrossWeightTxb.Text = "";
                                tR2TotalNetWeightTxb.Text = "";
                                TRShowPanel(ref tR1Pnl);
                                tR1CreateTransferBtn.PerformClick();
                                tR2RefNoCbb.SelectedItem = docNoInt.ToString();
                                return;
                            }
                            else
                            {
                                _b1TransferReqList = GetTransferRequest("", "");
                                jobList = GetJobOrderDetail("", "");
                                matchedTrList = _b1TransferReqList.FirstOrDefault(rq => rq.DocNum == docNoInt);

                                if (matchedTrList != null)
                                {
                                    tR2TotalCountTxb.Text = "";
                                    tR2TotalGrossWeightTxb.Text = "";
                                    tR2TotalNetWeightTxb.Text = "";
                                    TRShowPanel(ref tR1Pnl);
                                    tR1CreateTransferBtn.PerformClick();
                                    tR2RefNoCbb.SelectedItem = docNoInt.ToString();
                                    return;
                                }
                            }
                        }

                        MessageBox.Show("ไม่พบหมายเลขเอกสารที่แสกน");
                        return;
                    }
                    _tr1BarcodeBuffer = "";
                }
                else
                {
                    _transferInstance = _entities.Transfer.FirstOrDefault(tr => tr.DocNo == tR1TransferNoTxb.Text.Trim());
                }

                if (_transferInstance == null)
                {
                    MessageBox.Show("ไม่พบหมายเลชใบโอนถ่ายสินค้าที่กรอก");
                    return;
                }

                if (_transferInstance.IsReceived.HasValue && _transferInstance.IsReceived.Value)
                {
                    MessageBox.Show("การโอนย้ายนี้ดำเนินการสำเร็จไปแล้ว");
                    return;
                }

                _transferList = new BindingList<TransferTable>();
                var transferItemList = _transferInstance.TransferItem.Where(tri => tri.Transfer == _transferInstance);

                foreach (var item in transferItemList)
                {
                    var isWIP = GetItemTypeByItemCode(item.Item.ItemCategory.ItemCode).Name.StartsWith("FWIP");
                    _transferList.Add(new TransferTable()
                    {
                        ItemCode = item.Item.ItemCategory.ItemCode,
                        LotNo = item.Item.LotNo,
                        RowNo = _transferList.Count.ToString(),
                        Description = item.Item.ItemCategory.Description,
                        Count = isWIP ? (item.Count ?? 0) : (item.Item.Count ?? 0),
                        GrossWeight = item.Item.Measure ?? 0,
                        ItemID = item.ItemID,
                        Attributes = item.Item.Attributes,
                        NetWeight = item.Item.Measure ??
                                    0 - (item.Item.OuterPackageWeight ?? 0 + item.Item.MiscPackageWeight ??
                                         0 + item.Item.InnerPackageWeight ?? 0),
                        IsChecked = false
                    });
                }

                tR2DocNoTxb.Text = _transferInstance.DocNo;
                tR2AssignDateTxb.Text =
                    _transferInstance.AssignDate.Value.ToString("d MMMM yyyy",
                        CultureInfo.CreateSpecificCulture("th-TH"));
                tR2RefNoCbb.Text = _transferInstance.RefNo;
                tR2RefNoCbb.SetDisable();
                tR2TotalCountTxb.Text = "";
                tR2TotalGrossWeightTxb.Text = "";
                tR2TotalNetWeightTxb.Text = "";

                var fromWhseList = new List<string>(_warehouseList);
                var toWhseList = new List<string>(_warehouseList);

                tR2FromWhseCbb.DataSource = fromWhseList;
                tR2ToWhseCbb.DataSource = toWhseList;
                tR2BinLocationCbb.DataSource = _binLocationList;
                tR2FromWhseCbb.SelectedItem = _transferInstance.FromWhse;
                tR2ToWhseCbb.SelectedItem = _transferInstance.ToWhse;
                tR2BinLocationCbb.SelectedItem = _transferInstance.BinLoc;

                tR2InRdb.Checked = true;
                tR2OutRdb.Enabled = false;
                tR2SubmitBtn.Enabled = false;
                tR2PrintFormBtn.Enabled = true;
                tR2ReceiveBtn.Enabled = false;

                tR2ItemDgv.Columns.Clear();
                tR2ItemDgv.DataSource = _transferList;
                tR2ItemDgv.Columns["Count"].DefaultCellStyle.Format = "N0";
                tR2ItemDgv.Columns["GrossWeight"].DefaultCellStyle.Format = "N2";
                tR2ItemDgv.Columns["NetWeight"].DefaultCellStyle.Format = "N2";
                //tR2ItemDgv.ReadOnly = true;

                tR2FromWhseCbb.SetDisable();
                tR2BinLocationCbb.SetDisable();
                tR2RefNoCbb.SetDisable();
                tR2ToWhseCbb.SetDisable();

                TrRecalculateWeight();

                TRShowPanel(ref tR2Pnl);
            }
            catch (Exception ex)
            {
                log.Error("Method: HandleTR1SubmitAction() Error: " + ex.Message); MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: HandleTR1SubmitAction()");
            }
        }

        private void HandleTR2ScanAction(string barcode)
        {
            log.Info("Start Method: HandleTR2ScanAction() No parameter");

            try
            {
                var allItem = _entities.Item.ToList();

                if (tR2OutRdb.Checked)
                {
                    var fromWhs = tR2FromWhseCbb.SelectedItem.ToString();
                    var scannedItem = allItem.FirstOrDefault(i => i.Barcode == barcode && !i.Whse.Contains(SoldWhse) && !i.Whse.Contains(InTranWhse) && !i.Whse.Contains(IssuedWhse) && !i.Whse.Contains(ReceiveWhse) && i.Whse == fromWhs);
                    if (scannedItem == null)
                    {
                        MessageBox.Show("บาร์โค้ดที่แสกนไม่ถูกต้อง / ไม่พบสินค้าที่ตรงกับในระบบ");
                        return;
                    }

                    if (scannedItem.Whse != tR2FromWhseCbb.SelectedItem.ToString())
                    {
                        MessageBox.Show("สินค้าที่แสกนไม่อยู่ในโกดังต้นทาง");
                        return;
                    }

                    if (_transferList.Any(tr => tr.ItemID == scannedItem.ID))
                    {
                        MessageBox.Show("สินค้านี้ถูกแสกนไปแล้ว");
                        return;
                        //var transferTableItem = _transferList.FirstOrDefault(tr => tr.ItemID == scannedItem.ID);
                        //if (transferTableItem != null) transferTableItem.Count++;
                    }
                    else
                    {
                        _transferList.Add(new TransferTable()
                        {
                            ItemCode = scannedItem.ItemCategory.ItemCode,
                            LotNo = scannedItem.LotNo,
                            RowNo = (_transferList.Count + 1).ToString(),
                            Count = scannedItem.Count ?? 0,
                            Description = scannedItem.ItemCategory.Description,
                            ItemID = scannedItem.ID,
                            GrossWeight = scannedItem.Measure ?? 0,
                            Attributes = scannedItem.Attributes,
                            NetWeight = (scannedItem.Measure ??
                                       0) - ((scannedItem.OuterPackageWeight ?? 0) + (scannedItem.MiscPackageWeight ??
                                            0) + (scannedItem.InnerPackageWeight ?? 0)),
                            IsChecked = false
                        });
                    }

                    TrRecalculateWeight();
                    tR2ItemDgv.Refresh();
                    tR2SubmitBtn.Enabled = true;
                    tR2PrintFormBtn.Enabled = false;
                    tR2ReceiveBtn.Enabled = false;
                }

                if (tR2InRdb.Checked)
                {
                    var transferItemList = _entities.TransferItem.Where(ti => ti.TransferID == _transferInstance.ID).ToList();
                    var transferItem = transferItemList.FirstOrDefault(i => i.Item.Barcode == barcode && !i.Item.Whse.Contains(SoldWhse) && i.Item.Whse.Contains(InTranWhse) && !i.Item.Whse.Contains(IssuedWhse) && !i.Item.Whse.Contains(ReceiveWhse));

                    if (transferItem == null)
                    {
                        MessageBox.Show("บาร์โค้ดที่แสกนไม่ถูกต้อง / ไม่พบสินค้าที่ตรงกับในระบบ");
                        return;
                    }

                    var scannedItem = transferItem.Item;
                    if (scannedItem == null)
                    {
                        MessageBox.Show("บาร์โค้ดที่แสกนไม่ถูกต้อง / ไม่พบสินค้าที่ตรงกับในระบบ");
                        return;
                    }

                    if (!scannedItem.Whse.Contains(InTranWhse))
                    {
                        MessageBox.Show("สินค้านี้ไม่อยู่ในสถานะพร้อมโอน");
                        return;
                    }

                    var tableItem = _transferList.FirstOrDefault(tr => tr.ItemID == scannedItem.ID);
                    if (tableItem != null)
                    {
                        tableItem.IsChecked = true;
                        tR2ItemDgv.Refresh();
                    }
                    else
                    {
                        MessageBox.Show("ไม่พบสินค้าที่คุณแสกน");
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Method: HandleTR2ScanAction() Error: " + ex.Message); MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: HandleTR2ScanAction()");
            }
        }

        private List<B1_TransferReq> GetTransferRequest(string trqNo, string itemCode)
        {
            log.Info(string.Format(@"Start Method: GetTransferRequest() Parameter:trqNo Value:'{0}' Parameter:itemCode Value:'{1}'", trqNo, itemCode));

            try
            {
                string whereStr = "";

                if (trqNo != "" && itemCode != "")
                {
                    whereStr = string.Format(@" and T0.[DocNum] = '{0}' and T1.[ItemCode] = '{1}'", trqNo, itemCode);
                }

                var result = _b1Entities.Database.SqlQuery<B1_TransferReq>(
                    string.Format(@"
                                    select T0.DocEntry
                                    , T0.DocNum
                                    , T0.DocDate
                                    , T0.DocDueDate
                                    , T0.CardCode
                                    , T0.CardName
                                    , T0.Filler AS 'FromWhs'
                                    , T0.ToWhsCode
                                    , T1.LineNum
                                    , T1.ItemCode
                                    , T1.Dscription AS 'ItemName'
                                    , T1.Quantity AS 'Order_Qty'
                                    , T1.OpenQty
                                    , T1.unitMsr
                                    , T1.FromWhsCod AS 'Item_FromWhs'
                                    from OWTQ T0
                                    inner join WTQ1 T1 ON T0.DocEntry = T1.DocEntry
                                    where DocStatus = 'O'
                                    {0}
                                ", whereStr)).ToList();

                return result;
            }
            catch (Exception ex)
            {
                log.Error("Method: GetTransferRequest() Error: " + ex.Message); MessageBox.Show("Error: " + ex.Message);
                return null;
            }
            finally
            {
                log.Info("End Method: GetTransferRequest()");
            }
        }

        private void TR1CreateTransferBtn_Click(object sender, EventArgs e)
        {
            log.Info("Start Method: TR1CreateTransferBtn_Click() No parameter");

            try
            {
                _transferInstance = new Transfer();
                _transferList = new BindingList<TransferTable>();
                var trReqRefNoList = _b1TransferReqList.Select(trq => trq.DocNum.ToString()).ToList();
                tR2ItemDgv.Columns.Clear();
                tR2ItemDgv.DataSource = _transferList;
                tR2ItemDgv.Columns["Count"].DefaultCellStyle.Format = "N0";
                tR2ItemDgv.Columns["GrossWeight"].DefaultCellStyle.Format = "N2";
                tR2ItemDgv.Columns["NetWeight"].DefaultCellStyle.Format = "N2";
                tR2ItemDgv.Columns.Insert(8, new DataGridViewButtonColumn()
                {
                    Text = "X",
                    Name = "delete item button",
                    HeaderText = "",
                    UseColumnTextForButtonValue = true
                });
                //tR2ItemDgv.ReadOnly = true;
                var docFormat = _entities.DocumentSetup.FirstOrDefault(ds => ds.DocType == "Transfer")
                    .DocFormat;
                var todayDocCount = _entities.Transfer.Count(tr =>
                    tr.CreationDate.Value.Year == DateTime.Now.Year &&
                    tr.CreationDate.Value.Month == DateTime.Now.Month && tr.CreationDate.Value.Day == DateTime.Now.Day);
                tR2DocNoTxb.Text = "TRF-" + ReplaceFormatText(docFormat, "", "", "", "", "", "") +
                                   $"-{todayDocCount + 1:D3}";
                tR2AssignDateTxb.Text =
                    DateTime.Now.ToString("d MMMM yyyy", CultureInfo.CreateSpecificCulture("th-TH"));
                tR2RefNoCbb.Items.Clear();
                tR2RefNoCbb.Items.AddRange(trReqRefNoList.ToArray());
                tR2RefNoCbb.Items.Insert(0, "");
                tR2RefNoCbb.SetEnable();

                var fromWhseList = new List<string>(_warehouseList);
                var toWhseList = new List<string>(_warehouseList);

                tR2FromWhseCbb.DataSource = fromWhseList;
                tR2ToWhseCbb.DataSource = toWhseList;
                tR2BinLocationCbb.DataSource = _binLocationList;
                tR2FromWhseCbb.SelectedIndex = 0;
                tR2ToWhseCbb.SelectedIndex = 0;
                tR2BinLocationCbb.SelectedIndex = 0;

                tR2OutRdb.Checked = true;
                tR2InRdb.Enabled = false;

                tR2FromWhseCbb.SetEnable();
                tR2BinLocationCbb.SetEnable();
                tR2RefNoCbb.SetEnable();
                tR2ToWhseCbb.SetEnable();

                tR2SubmitBtn.Enabled = false;
                tR2PrintFormBtn.Enabled = false;
                tR2ReceiveBtn.Enabled = false;

                TRShowPanel(ref tR2Pnl);
            }
            catch (Exception ex)
            {
                log.Error("Method: TR1CreateTransferBtn_Click() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: TR1CreateTransferBtn_Click()");
            }
        }

        private void TR1SubmitBtn_Click(object sender, EventArgs e)
        {
            log.Info("Start Method: TR1SubmitBtn_Click() No parameter");
            Cursor.Current = Cursors.WaitCursor;

            try
            {
                _tr1BarcodeBuffer = "";
                HandleTR1SubmitAction();
            }
            catch (Exception ex)
            {
                log.Error("Method: TR1SubmitBtn_Click() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: TR1SubmitBtn_Click()");
                Cursor.Current = Cursors.Default;
            }
        }

        private void TR2ItemDgv_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            log.Info("Start Method: TR2ItemDgv_CellClick() No parameter");

            try
            {
                var button = tR2ItemDgv.Columns[e.ColumnIndex] as DataGridViewButtonColumn;
                if (button != null && button.Name == "delete item button")
                {
                    var transferItem = tR2ItemDgv.Rows[e.RowIndex].DataBoundItem as TransferTable;

                    if (transferItem != null)
                    {
                        WriteLog(_transferInstance?.ID, "TF", "Delete scanned item"
                            , $"ลบสินค้า:{transferItem?.ItemID} Row:{transferItem?.RowNo} หมายเลข Lot:{transferItem?.LotNo} Gross weight:{transferItem?.GrossWeight}");
                    }

                    var toRemoveTrItem =
                        _transferList.FirstOrDefault(tr => tr.ItemID == transferItem.ItemID);
                    _transferList.Remove(toRemoveTrItem);

                    TrRecalculateWeight();
                }
            }
            catch (Exception ex)
            {
                log.Error("Method: TR2ItemDgv_CellClick() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: TR2ItemDgv_CellClick()");
            }
        }

        private void TR2ItemDgv_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            log.Info("Start Method: TR2ItemDgv_RowPrePaint() No parameter");

            try
            {
                if (!(this.tR2ItemDgv.Rows[e.RowIndex].DataBoundItem is TransferTable rowItem)) return;

                if (Convert.ToInt32(tR2ItemDgv.Rows[e.RowIndex].Cells["RowNo"].Value) != (e.RowIndex + 1))
                {
                    tR2ItemDgv.Rows[e.RowIndex].Cells["RowNo"].Value = e.RowIndex + 1;
                }

                if (!rowItem.IsChecked) return;
                tR2ItemDgv.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightBlue;

                if (_transferList.All(tr => tr.IsChecked) && !_transferInstance.IsReceived.HasValue)
                {
                    tR2ReceiveBtn.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                log.Error("Method: TR2ItemDgv_RowPrePaint() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: TR2ItemDgv_RowPrePaint()");
            }
        }

        private void TR2MagicBtn_Click(object sender, EventArgs e)
        {
            if (tR2OutRdb.Checked)
            {
                var allItem = _entities.Item.Where(i => i.Whse != tR2FromWhseCbb.SelectedItem.ToString() && !i.Whse.EndsWith(InTranWhse) && !i.Whse.EndsWith(SoldWhse) && !i.Whse.EndsWith(IssuedWhse) && !i.Whse.EndsWith(ReceiveWhse)).ToList();
                var someItem = allItem[new Random().Next(0, allItem.Count)];
                _transferList.Add(new TransferTable()
                {
                    ItemCode = someItem.ItemCategory.ItemCode,
                    LotNo = someItem.LotNo,
                    RowNo = (_transferList.Count + 1).ToString(),
                    Count = someItem.Count ?? 0,
                    Description = someItem.ItemCategory.Description,
                    ItemID = someItem.ID,
                    GrossWeight = someItem.Measure ?? 0,
                    Attributes = someItem.Attributes,
                    NetWeight = (someItem.Measure ??
                                0) - ((someItem.OuterPackageWeight ?? 0) + (someItem.MiscPackageWeight ??
                                     0) + (someItem.InnerPackageWeight ?? 0)),
                    IsChecked = false
                });

                TrRecalculateWeight();
                tR2SubmitBtn.Enabled = true;
                tR2PrintFormBtn.Enabled = false;
                tR2ReceiveBtn.Enabled = false;
            }

            if (tR2InRdb.Checked)
            {
                var allItem = _entities.Item.Where(i => i.Whse.EndsWith(InTranWhse)).ToList();
                var someItem = allItem[new Random().Next(0, allItem.Count)];
                var tableItem = _transferList.FirstOrDefault(tr => tr.ItemID == someItem.ID);
                if (tableItem != null)
                {
                    tableItem.IsChecked = true;
                    tR2ItemDgv.Refresh();
                }
                else
                {
                    MessageBox.Show("ไม่พบสินค้าที่คุณแสกน");
                }
            }
        }

        private void TR2PrintFormBtn_Click(object sender, EventArgs e)
        {
            log.Info("Start Method: TR2PrintFormBtn_Click() No parameter");

            try
            {
                var worksheet = _formWorkbook.Worksheets[4];

                var tempWorkBook = new Workbook();
                tempWorkBook.Version = ExcelVersion.Version2010;
                tempWorkBook.Worksheets.Clear();
                var currentSheet = tempWorkBook.Worksheets.AddCopy(worksheet);

                currentSheet.Pictures.Add(1, 9, GetBarcodeImage(_transferInstance.DocNo));
                currentSheet.Range["I3"].Value = _transferInstance.AssignDate.HasValue
                    ? _transferInstance.AssignDate.Value.ToString("d MMMM yyyy",
                        CultureInfo.CreateSpecificCulture("th-TH"))
                    : "ไม่มีข้อมูล";
                currentSheet.Range["I4"].Value = _transferInstance.RefNo;
                currentSheet.Range["C10"].Value = _transferInstance.Remark;
                currentSheet.Range["H7"].Value = _transferInstance.FromWhse;
                currentSheet.Range["H9"].Value = _transferInstance.ToWhse;
                currentSheet.Pictures.Add(6, 9, GetBarcodeImage120W(_transferInstance.FromWhse));
                currentSheet.Pictures.Add(9, 9, GetBarcodeImage120W(_transferInstance.ToWhse));
                currentSheet.Range["H10"].Value = _transferInstance.BinLoc;
                currentSheet.Range["J28"].Value = _transferInstance.PrintCount == null ? "พิมพ์ครั้งที่ 1" : "พิมพ์ครั้งที่ " + (_transferInstance.PrintCount.Value);

                var sortedList = _transferList.OrderBy(tr => tr.ItemCode).GroupBy(tr => tr.ItemCode).ToList();

                var rowOffset = 13;
                var itemCount = 1;

                for (var i = 0; i < sortedList.Count; i++)
                {
                    currentSheet.InsertRow(17, 2, InsertOptionsType.FormatAsBefore);
                    currentSheet.Copy(worksheet.Range["A15:J16"], worksheet.Range["A17:J18"], true);
                }

                foreach (var itemCodeGroup in sortedList)
                {
                    var noOfRowToInsert = itemCodeGroup.Count() - 2 < 0 ? 0 : itemCodeGroup.Count() - 2;
                    var itemCode = itemCodeGroup.FirstOrDefault().ItemCode;
                    var description = itemCodeGroup.FirstOrDefault().Description;
                    var firstItemId = itemCodeGroup.FirstOrDefault().ItemID;
                    var countUnit = _entities.Item.FirstOrDefault(i => i.ID == firstItemId).ItemType.UOM_Count;
                    var endRange = rowOffset + itemCodeGroup.Count() - 1;

                    if (noOfRowToInsert > 0)
                    {
                        currentSheet.InsertRow(rowOffset + 1, noOfRowToInsert, InsertOptionsType.FormatAsBefore);
                    }

                    currentSheet.Range["A" + rowOffset].Value = "" + itemCount++;
                    currentSheet.Pictures.Add(rowOffset, 2, GetBarcodeImage120W(itemCode));
                    currentSheet.Range["C" + rowOffset].Value = description;
                    currentSheet.Range["E" + rowOffset].Value = itemCodeGroup.Count().ToString();
                    currentSheet.Range["F" + rowOffset].Value = countUnit;

                    currentSheet.Range["A" + rowOffset + ":A" + endRange].Merge();
                    currentSheet.Range["B" + rowOffset + ":B" + endRange].Merge();
                    currentSheet.Range["C" + rowOffset + ":C" + endRange].Merge();

                    foreach (var item in itemCodeGroup)
                    {
                        currentSheet.Range["D" + rowOffset].Value = item.LotNo;
                        currentSheet.Range["G" + rowOffset].Value = $"{item.GrossWeight:N2}";
                        currentSheet.Range["H" + rowOffset].Value = $"{item.NetWeight:N2}";

                        rowOffset++;
                    }
                }

                if (_printEnable)
                {
                    PrintExcelByInterop(tempWorkBook, _formPrinterName);
                }
                else
                {
                    SaveFileExcel(tempWorkBook, "Transfer Form");
                }

                if (_transferInstance.PrintCount == null)
                {
                    _transferInstance.PrintCount = 2;
                }
                else
                {
                    _transferInstance.PrintCount++;
                }
            }
            catch (Exception ex)
            {
                log.Error("Method: TR2PrintFormBtn_Click() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                WriteLog(_transferInstance?.ID, "TR", "Print form"
                    , $"พิมพ์แบบฟอร์มของ TF No:{_transferInstance?.DocNo} โอนย้ายสินค้าออกจากโกดัง:{_transferInstance?.FromWhse} ไปโกดัง:{_transferInstance?.ToWhse} Bin location:{_transferInstance?.BinLoc} Transfer request:{_transferInstance?.RefNo}");

                log.Info("End Method: TR2PrintFormBtn_Click()");
            }

        }

        private void TR2ReceiveBtn_Click(object sender, EventArgs e)
        {
            log.Info("Start Method: TR2SubmitBtn_Click() No parameter");

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                var resultTransferItemList = GetGroupedResultTransferItemList(false);

                if (resultTransferItemList.Count > 0)
                {
                    var result = tR2RefNoCbb.SelectedIndex == 0 ? Interface_TransferNoRequestExcelTemplate(resultTransferItemList) : Interface_TransferRefRequestExcelTemplate(resultTransferItemList);
                    if (result)
                    {
                        _transferInstance.IsReceived = true;
                        _entities.SaveChanges();
                        tR2ReceiveBtn.Enabled = false;
                        MessageBox.Show("บันทึกเรียบร้อย");
                    }
                    else
                    {
                        tR2ReceiveBtn.Enabled = true;
                        _transferInstance.IsReceived = false;
                        MessageBox.Show("บันทึกไม่สำเร็จ กรุณาลองใหม่อีกครั้ง");
                    }
                }

                tR2SubmitBtn.Enabled = false;
                tR2PrintFormBtn.Enabled = false;
            }
            catch (Exception ex)
            {
                log.Error("Method: TR2SubmitBtn_Click() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: TR2SubmitBtn_Click()");
                Cursor.Current = Cursors.Default;
            }
        }

        private List<B1_TransferInterface> GetGroupedResultTransferItemList(bool transOut)
        {
            var transferRefNo = tR2RefNoCbb.SelectedItem?.ToString() ?? "";
            var transferRequest = _b1TransferReqList.FirstOrDefault(trr => trr.DocNum.ToString() == transferRefNo);

            var newTransferInterfaceList = new List<B1_TransferInterface>();

            foreach (var item in _transferList)
            {
                var itemType = GetItemTypeByItemCode(item.ItemCode);
                var tranItem = GetItemByID(item.ItemID);
                if (tranItem == null)
                {
                    continue;
                }

                var netPerLot = 0.0M;

                if (itemType.isItemizable ?? false)
                {
                    netPerLot = (tranItem.Measure ?? 0) - ((tranItem.MiscPackageWeight ?? 0) +
                                                           (tranItem.OuterPackageWeight ?? 0) +
                                                           (tranItem.InnerPackageWeight ?? 0));
                }
                else
                {
                    netPerLot = (tranItem.Measure ?? 0) * item.Count;
                }


                newTransferInterfaceList.Add(new B1_TransferInterface()
                {
                    ItemCode = item.ItemCode,
                    LotNo = itemType.isItemizable ?? false ? item.LotNo.Substring(0, item.LotNo.Length - 5) : item.LotNo,
                    FromWhseCode = _transferInstance.FromWhse ?? tR2FromWhseCbb.SelectedItem.ToString(),
                    ToWhseCode = _transferInstance.ToWhse ?? tR2ToWhseCbb.SelectedItem.ToString(),
                    NetPerLot = netPerLot,
                    TfrDocEntry = transferRequest == null ? "" : transferRequest.DocEntry.ToString(),
                    TfrLineNo = transferRequest == null ? "" : transferRequest.LineNum.ToString(),
                    TransferDocNum = _transferInstance.DocNo,
                    TransferRequest = transferRequest == null ? "" : transferRequest.DocNum.ToString(),
                    unitMs = transferRequest == null ? "" : transferRequest.unitMsr
                });

                if (!transOut)
                {
                    tranItem.Whse = _transferInstance.ToWhse;
                }
            }

            var groupedTransferItemList = newTransferInterfaceList.GroupBy(tri => tri.LotNo);

            var resultTransferItemList = groupedTransferItemList.Select(gtri => new B1_TransferInterface()
            {
                ItemCode = gtri.FirstOrDefault().ItemCode,
                LotNo = gtri.FirstOrDefault().LotNo,
                FromWhseCode = gtri.FirstOrDefault().FromWhseCode,
                ToWhseCode = gtri.FirstOrDefault().ToWhseCode,
                NetPerLot = gtri.Sum(tr => tr.NetPerLot),
                TfrDocEntry = gtri.FirstOrDefault().TfrDocEntry,
                TfrLineNo = gtri.FirstOrDefault().TfrLineNo,
                TransferDocNum = gtri.FirstOrDefault().TransferDocNum,
                TransferRequest = gtri.FirstOrDefault().TransferRequest,
                unitMs = gtri.FirstOrDefault().unitMs
            }).ToList();

            return resultTransferItemList;
        }

        private bool Interface_TransferNoRequestExcelTemplate(List<B1_TransferInterface> groupTransfers)
        {
            log.Info("Start Method: Interface_TransferNoRequestExcelTemplate()");

            try
            {
                //AOM
                // gen B1 header template
                var docEntry = "1";
                var docDate = DateTime.Today.ToString("yyyyMMdd", CultureInfo.CreateSpecificCulture("en-US"));
                var ref2 = "Transfer No: " + groupTransfers[0].TransferDocNum; //BC ref
                var fromWhs = groupTransfers[0].FromWhseCode;
                var toWhs = groupTransfers[0].ToWhseCode;

                var _interfaceWorkbook_Header = new Workbook();
                _interfaceWorkbook_Header.LoadFromFile(
                    ConfigurationManager.AppSettings["GenLot_HeaderCsvTemplate"],
                    ",", 1, 1);
                var worksheetHd = _interfaceWorkbook_Header.Worksheets[0];

                worksheetHd.Range["A3"].Text = docEntry;
                worksheetHd.Range["D3"].Text = docDate;
                worksheetHd.Range["K3"].Text = ref2; //comment
                worksheetHd.Range["O3"].Text = fromWhs;
                worksheetHd.Range["P3"].Text = toWhs;

                var tempWorkBookHd = new Workbook();
                tempWorkBookHd.Version = ExcelVersion.Version2010;
                tempWorkBookHd.Worksheets.Clear();
                tempWorkBookHd.Worksheets.AddCopy(worksheetHd);

                string headerPath = SaveFileCSV(tempWorkBookHd, "Header_Transfer");

                //B1 line & Batch
                int i = 3; //start at row 3
                int j = 0;

                var _interfaceWorkbook_Line = new Workbook();
                _interfaceWorkbook_Line.LoadFromFile(
                    ConfigurationManager.AppSettings["GenLot_LineCsvTemplate"],
                    ",", 1, 1);
                var worksheetLn = _interfaceWorkbook_Line.Worksheets[0];

                var _interfaceWorkbook_Batch = new Workbook();
                _interfaceWorkbook_Batch.LoadFromFile(
                    ConfigurationManager.AppSettings["GenLot_BatchCsvTemplate"],
                    ",", 1, 1);
                var worksheetBt = _interfaceWorkbook_Batch.Worksheets[0];

                foreach (B1_TransferInterface tf in groupTransfers)
                {
                    string parentKey = "1"; //always 1
                    string lineNum = j.ToString(); //start at 0
                    string itemCode = tf.ItemCode;
                    string lineQuantity = ConvertKgsToLbs(tf.NetPerLot).ToString(); //Lbs
                    string unitMs = tf.unitMs;

                    worksheetLn.Range["A" + i.ToString()].Text = parentKey;
                    worksheetLn.Range["B" + i.ToString()].Text = lineNum;
                    worksheetLn.Range["C" + i.ToString()].Text = itemCode;
                    worksheetLn.Range["E" + i.ToString()].Text = lineQuantity;
                    worksheetLn.Range["W" + i.ToString()].Text = unitMs;

                    string batchDocNum = "1";
                    string lotLineNo = lineNum;
                    string distNumber = tf.LotNo;
                    string batchQuantity = lineQuantity;
                    string baseLineNumber = lineNum;

                    worksheetBt.Range["A" + i.ToString()].Text = batchDocNum;
                    worksheetBt.Range["B" + i.ToString()].Text = lotLineNo;
                    worksheetBt.Range["C" + i.ToString()].Text = distNumber;
                    worksheetBt.Range["K" + i.ToString()].Text = batchQuantity;
                    worksheetBt.Range["L" + i.ToString()].Text = baseLineNumber;

                    i++;
                    j++;

                    WriteLog(_transferInstance?.ID, "TR", "Receive TF Item - No Ref"
                        , $"ยืนยันการรับสินค้าของ TF No:{tf.TransferDocNum} รหัสสินค้า:{tf.ItemCode} Total Net weight:{lineQuantity}");

                }

                //B1 Line
                var tempWorkBookLn = new Workbook();
                tempWorkBookLn.Version = ExcelVersion.Version2010;
                tempWorkBookLn.Worksheets.Clear();
                tempWorkBookLn.Worksheets.AddCopy(worksheetLn);

                string linePath = SaveFileCSV(tempWorkBookLn, "Line_Transfer");

                //B1 Batch
                var tempWorkBookBt = new Workbook();
                tempWorkBookBt.Version = ExcelVersion.Version2010;
                tempWorkBookBt.Worksheets.Clear();
                tempWorkBookBt.Worksheets.AddCopy(worksheetBt);

                string batchPath = SaveFileCSV(tempWorkBookBt, "Batch_Transfer");

                ////Copy XML to new one and also change template paths in XML
                //bool isTransfer = true;
                //string templatePath = ConfigurationManager.AppSettings["GenLot_XmlTemplate"];
                //string xmlOutputPath = ConfigurationManager.AppSettings["InterfaceOutputPath"] + "TransferXml_" + DateTime.Now.ToString("dd-MM-yy-hhmmss") + ".xml"; ;
                //CreateXMLInterface(isTransfer, templatePath, headerPath, linePath, batchPath, xmlOutputPath);

                ////Call .bat
                //string batName = "TransferBat_" + DateTime.Now.ToString("dd-MM-yy-hhmmss");
                //CreateBatFile(batName, xmlOutputPath);

                //Copy XML to new one and also change template paths in XML
                bool isTransfer = true;
                string templatePath = ConfigurationManager.AppSettings["GenLot_XmlTemplate"];
                string xmlFileName = "TransferXml_" + DateTime.Now.ToString("dd-MM-yy-hhmmss") + ".xml";
                string xmlOutputPath = ConfigurationManager.AppSettings["InterfaceOutputPath"] + xmlFileName;
                string localXmlPath = ConfigurationManager.AppSettings["LocalTemplateOutputPath"] + xmlFileName;
                CreateXMLInterface(isTransfer, templatePath, headerPath, linePath, batchPath, xmlOutputPath);

                //Call .bat
                string batName = "TransferBat_" + DateTime.Now.ToString("dd-MM-yy-hhmmss");
                CreateBatFile(batName, localXmlPath);

                //Detect error log from DTW
                bool result = GetInterfaceResult(batName);

                return result;
            }
            catch (Exception ex)
            {
                log.Error("Method: Interface_TransferNoRequestExcelTemplate() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
                return false;
            }
            finally
            {
                log.Info("End Method: Interface_TransferNoRequestExcelTemplate()");
            }
        }

        private bool Interface_TransferRefRequestExcelTemplate(List<B1_TransferInterface> groupTransfers)
        {
            log.Info("Start Method: Interface_TransferRefRequestExcelTemplate()");

            try
            {
                //AOM
                // gen B1 header template
                var docEntry = "1";
                var docDate = DateTime.Today.ToString("yyyyMMdd", CultureInfo.CreateSpecificCulture("en-US"));
                var ref2 = "Transfer No: " + groupTransfers[0].TransferDocNum + " and Transfer request No: " + groupTransfers[0].TransferRequest; //BC ref
                var fromWhs = groupTransfers[0].FromWhseCode;
                var toWhs = groupTransfers[0].ToWhseCode;

                var _interfaceWorkbook_Header = new Workbook();
                _interfaceWorkbook_Header.LoadFromFile(
                    ConfigurationManager.AppSettings["TransferReq_HeaderCsvTemplate"],
                    ",", 1, 1);
                var worksheetHd = _interfaceWorkbook_Header.Worksheets[0];

                worksheetHd.Range["A3"].Text = docEntry;
                worksheetHd.Range["D3"].Text = docDate;
                worksheetHd.Range["K3"].Text = ref2; //comment
                worksheetHd.Range["O3"].Text = fromWhs;
                worksheetHd.Range["P3"].Text = toWhs;

                var tempWorkBookHd = new Workbook();
                tempWorkBookHd.Version = ExcelVersion.Version2010;
                tempWorkBookHd.Worksheets.Clear();
                tempWorkBookHd.Worksheets.AddCopy(worksheetHd);

                string headerPath = SaveFileCSV(tempWorkBookHd, "Header_TransferReq");

                //B1 line & Batch
                int i = 3; //start at row 3
                int j = 0;

                var _interfaceWorkbook_Line = new Workbook();
                _interfaceWorkbook_Line.LoadFromFile(
                    ConfigurationManager.AppSettings["TransferReq_LineCsvTemplate"],
                    ",", 1, 1);
                var worksheetLn = _interfaceWorkbook_Line.Worksheets[0];

                var _interfaceWorkbook_Batch = new Workbook();
                _interfaceWorkbook_Batch.LoadFromFile(
                    ConfigurationManager.AppSettings["TransferReq_BatchCsvTemplate"],
                    ",", 1, 1);
                var worksheetBt = _interfaceWorkbook_Batch.Worksheets[0];

                foreach (B1_TransferInterface tf in groupTransfers)
                {
                    string parentKey = "1"; //always 1
                    string lineNum = j.ToString(); //start at 0
                    string itemCode = tf.ItemCode;
                    string lineQuantity = ConvertKgsToLbs(tf.NetPerLot).ToString(); //Lbs
                    string baseType = "1250000001";
                    string baseEntry = tf.TfrDocEntry;
                    string baseLine = tf.TfrLineNo;

                    worksheetLn.Range["A" + i.ToString()].Text = parentKey;
                    worksheetLn.Range["B" + i.ToString()].Text = lineNum;
                    worksheetLn.Range["C" + i.ToString()].Text = itemCode;
                    worksheetLn.Range["E" + i.ToString()].Text = lineQuantity;
                    worksheetLn.Range["AA" + i.ToString()].Text = baseType;
                    worksheetLn.Range["AB" + i.ToString()].Text = baseLine;
                    worksheetLn.Range["AC" + i.ToString()].Text = baseEntry;


                    string batchDocNum = "1";
                    string distNumber = tf.LotNo;
                    string batchQuantity = lineQuantity;
                    string baseLineNumber = lineNum;

                    worksheetBt.Range["A" + i.ToString()].Text = batchDocNum;
                    worksheetBt.Range["C" + i.ToString()].Text = distNumber;
                    worksheetBt.Range["K" + i.ToString()].Text = batchQuantity;
                    worksheetBt.Range["L" + i.ToString()].Text = baseLineNumber;

                    i++;
                    j++;

                    WriteLog(_transferInstance?.ID, "TR", "Receive TF Item - Ref"
                        , $"ยืนยันการรับสินค้าของ TF No:{tf.TransferDocNum} รหัสสินค้า:{tf.ItemCode} Total Net weight:{lineQuantity} Transfer ref:{tf.TransferRequest}");

                }

                //B1 Line
                var tempWorkBookLn = new Workbook();
                tempWorkBookLn.Version = ExcelVersion.Version2010;
                tempWorkBookLn.Worksheets.Clear();
                tempWorkBookLn.Worksheets.AddCopy(worksheetLn);

                string linePath = SaveFileCSV(tempWorkBookLn, "Line_TransferReq");

                //B1 Batch
                var tempWorkBookBt = new Workbook();
                tempWorkBookBt.Version = ExcelVersion.Version2010;
                tempWorkBookBt.Worksheets.Clear();
                tempWorkBookBt.Worksheets.AddCopy(worksheetBt);

                string batchPath = SaveFileCSV(tempWorkBookBt, "Batch_TransferReq");

                ////Copy XML to new one and also change template paths in XML
                //bool isTransfer = true;
                //string templatePath = ConfigurationManager.AppSettings["TransferReq_XmlTemplate"];
                //string xmlOutputPath = ConfigurationManager.AppSettings["InterfaceOutputPath"] + "TransferReqXml_" + DateTime.Now.ToString("dd-MM-yy-hhmmss") + ".xml"; ;
                //CreateXMLInterface(isTransfer, templatePath, headerPath, linePath, batchPath, xmlOutputPath);

                ////Call .bat
                //string batName = "TransferReqBat_" + DateTime.Now.ToString("dd-MM-yy-hhmmss");
                //CreateBatFile(batName, xmlOutputPath);

                //Copy XML to new one and also change template paths in XML
                bool isTransfer = true;
                string templatePath = ConfigurationManager.AppSettings["TransferReq_XmlTemplate"];
                string xmlFileName = "TransferReqXml_" + DateTime.Now.ToString("dd-MM-yy-hhmmss") + ".xml";
                string xmlOutputPath = ConfigurationManager.AppSettings["InterfaceOutputPath"] + xmlFileName;
                string localXmlPath = ConfigurationManager.AppSettings["LocalTemplateOutputPath"] + xmlFileName;
                CreateXMLInterface(isTransfer, templatePath, headerPath, linePath, batchPath, xmlOutputPath);

                //Call .bat
                string batName = "TransferReqBat_" + DateTime.Now.ToString("dd-MM-yy-hhmmss");
                CreateBatFile(batName, localXmlPath);

                //Detect error log from DTW
                bool result = GetInterfaceResult(batName);

                return result;
            }
            catch (Exception ex)
            {
                log.Error("Method: Interface_TransferRefRequestExcelTemplate() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
                return false;
            }
            finally
            {
                log.Info("End Method: Interface_TransferRefRequestExcelTemplate()");
            }
        }

        private void TR2SubmitBtn_Click(object sender, EventArgs e)
        {
            log.Info("Start Method: TR2SubmitBtn_Click() No parameter");
            Cursor.Current = Cursors.WaitCursor;

            try
            {
                var transferRefNo = tR2RefNoCbb.SelectedItem?.ToString() ?? "";
                var transferRequest = _b1TransferReqList.FirstOrDefault(trr => trr.DocNum.ToString() == transferRefNo);

                if (_transferList.Any(tr => transferRequest != null && tr.ItemCode != transferRequest.ItemCode))
                {
                    MessageBox.Show("สินค้าบางรายการไม่อยู่ใน Transfer Request นี้");
                    return;
                }

                var insufficientQuantityCodes = "";

                var resultTransferList = GetGroupedResultTransferItemList(true);
                foreach (var item in resultTransferList)
                {
                    var itemType = GetItemTypeByItemCode(item.ItemCode);
                    var lotNo = item.LotNo;
                    var availableQty = _b1Entities.OIBT
                        .Where(oibt =>
                            oibt.ItemCode == item.ItemCode
                            && oibt.BatchNum == lotNo
                            && oibt.WhsCode == item.FromWhseCode).Select(oibt => oibt.Quantity).FirstOrDefault();
                    var sumNet = itemType.Name == "Finish Goods - WE" ? item.NetPerLot : ConvertKgsToLbs(item.NetPerLot);

                    if ((availableQty.HasValue && availableQty < sumNet) || !availableQty.HasValue)
                    {
                        insufficientQuantityCodes +=
                            insufficientQuantityCodes.Length > 0 ? ", " + item.ItemCode : item.ItemCode;
                    }
                }

                if (insufficientQuantityCodes.Length > 0)
                {
                    MessageBox.Show("สินค้าเหล่านี้มีจำนวนในสต็อคต้นทางไม่เพียงพอ: " + insufficientQuantityCodes);
                    return;
                }


                _transferInstance.DocNo = tR2DocNoTxb.Text;
                _transferInstance.AssignDate =
                    Convert.ToDateTime(tR2AssignDateTxb.Text, CultureInfo.CreateSpecificCulture("th-TH"));
                _transferInstance.RefNo = transferRefNo;
                _transferInstance.FromWhse = tR2FromWhseCbb.SelectedItem?.ToString();
                _transferInstance.ToWhse = tR2ToWhseCbb.SelectedItem?.ToString();
                _transferInstance.BinLoc = tR2BinLocationCbb.SelectedItem?.ToString();
                _transferInstance.Remark = tR2RemarkTxb.Text;
                _transferInstance.Barcode = tR2DocNoTxb.Text;
                _transferInstance.CreationDate = DateTime.Now;

                _entities.SaveChanges();

                Transfer newTransfer;

                if (_transferInstance.ID != 0 && _entities.Transfer.Any(tr => tr.ID == _transferInstance.ID))
                {
                    newTransfer = _transferInstance;
                }
                else
                {
                    newTransfer = _entities.Transfer.Add(_transferInstance);
                }

                var transferItemList = new List<TransferItem>();
                foreach (var item in _transferList)
                {
                    var tranItem = _entities.Item.FirstOrDefault(i => i.ID == item.ItemID);
                    if (tranItem == null)
                    {
                        continue;
                    }

                    if (tranItem.Count < item.Count)
                    {
                        MessageBox.Show("สินค้าที่โอนถ่ายมีจำนวนในสต็อคไม่เพียงพอ");
                        return;
                    }

                    var itemType = GetItemTypeByItemCode(item.ItemCode);

                    if (itemType.Name.StartsWith("FWIP"))
                    {
                        //var grossWeightPerItem = tranItem.Measure / tranItem.Count;
                        var destItem = _entities.Item.FirstOrDefault(i => i.LotNo == item.LotNo && i.Whse == _transferInstance.ToWhse);

                        if (destItem != null)
                        {
                            destItem.Whse = tranItem.Whse + InTranWhse;
                            destItem.Count += item.Count;
                            //if (grossWeightPerItem != null)
                            //{
                            //    destItem.Measure = destItem.Count * grossWeightPerItem.Value;
                            //}
                        }
                        else
                        {
                            destItem = new Item()
                            {
                                Count = item.Count,
                                LotNo = tranItem.LotNo,
                                Measure = tranItem.Measure,
                                ItemType = tranItem.ItemType,
                                MiscPackageWeight = tranItem.MiscPackageWeight,
                                Attributes = tranItem.Attributes,
                                Barcode = tranItem.Barcode,
                                BinLoc = tranItem.BinLoc,
                                //DeliveryOrderItem = tranItem.DeliveryOrderItem,
                                //GenLotItem = tranItem.GenLotItem,
                                InnerPackageCount = tranItem.InnerPackageCount,
                                InnerPackageWeight = tranItem.InnerPackageWeight,
                                //IssueProductionItem = tranItem.IssueProductionItem,
                                ItemCategory = tranItem.ItemCategory,
                                ItemCategoryID = tranItem.ItemCategoryID,
                                ItemTypeID = tranItem.ItemTypeID,
                                //JobReceiveItem = tranItem.JobReceiveItem,
                                MiscPackageCount = tranItem.MiscPackageCount,
                                OuterPackageCount = tranItem.OuterPackageCount,
                                OuterPackageWeight = tranItem.OuterPackageWeight,
                                PackageType = tranItem.PackageType,
                                ReceiveDate = tranItem.ReceiveDate,
                                //TransferItem = tranItem.TransferItem,
                                VendorRef = tranItem.VendorRef,
                                Whse = tranItem.Whse + InTranWhse
                            };
                        }

                        if (tranItem.Count == item.Count)
                        {
                            tranItem.Whse += InTranWhse;
                            //RemoveItemByID(tranItem.ID);
                        }
                        else
                        {
                            if (destItem.ID == 0)
                            {
                                _entities.Item.Add(destItem);
                            }

                            tranItem.Count -= item.Count;
                            //tranItem.Measure = tranItem.Count * grossWeightPerItem.Value;
                        }

                        _entities.SaveChanges();
                        tranItem = destItem;
                    }
                    else
                    {
                        tranItem.Whse += InTranWhse;
                    }

                    transferItemList.Add(new TransferItem()
                    {
                        Item = tranItem,
                        Transfer = newTransfer,
                        Count = item.Count
                    });
                }

                _entities.TransferItem.AddRange(transferItemList);
                _entities.SaveChanges();

                MessageBox.Show("บันทึกเรียบร้อย");

                //tR2ItemDgv.ReadOnly = true;
                tR2ItemDgv.Columns.Remove("delete item button");

                tR2FromWhseCbb.SetDisable();
                tR2BinLocationCbb.SetDisable();
                tR2RefNoCbb.SetDisable();
                tR2ToWhseCbb.SetDisable();

                tR2SubmitBtn.Enabled = false;
                tR2PrintFormBtn.Enabled = true;
                tR2ReceiveBtn.Enabled = false;
            }
            catch (Exception ex)
            {
                log.Error("Method: TR2SubmitBtn_Click() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                WriteLog(_transferInstance?.ID, "TF", "Save Out"
                    , $"บันทึกการโอนย้ายสินค้าออกจากโกดัง:{_transferInstance?.FromWhse} ไปโกดัง:{_transferInstance?.ToWhse} Bin location:{_transferInstance?.BinLoc} Transfer request:{_transferInstance?.RefNo} จำนวนสินค้า:{_transferInstance?.TransferItem?.Count}");

                log.Info("End Method: TR2SubmitBtn_Click()");
                Cursor.Current = Cursors.Default;
            }
        }

        private void TransferTabPage_Enter(object sender, EventArgs e)
        {
        }

        private void TrRecalculateWeight()
        {
            log.Info("Start Method: TrRecalculateWeight() No parameter");

            try
            {
                var totalCount = _transferList.Sum(tr => tr.Count);
                var totalWeightKgs = _transferList.Sum(i => i.GrossWeight);
                var totalNetWeightKgs = _transferList.Sum(i => i.NetWeight);

                tR2TotalCountTxb.Text = $"{totalCount:N0}";
                tR2TotalGrossWeightTxb.Text = $"{totalWeightKgs:N2}";
                tR2TotalNetWeightTxb.Text = $"{totalNetWeightKgs:N2}";
            }
            catch (Exception ex)
            {
                log.Error("Method: TrRecalculateWeight() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: TrRecalculateWeight()");
            }
        }

        private void TRShowPanel(ref MetroPanel panel)
        {
            tR1Pnl.Hide();
            tR2Pnl.Hide();

            panel.Dock = DockStyle.Fill;
            panel.Show();
        }

        private void TR2Pnl_MouseHover(object sender, EventArgs e)
        {
            //tR2Pnl.Focus();
        }

        private void TR2ItemDgv_MouseHover(object sender, EventArgs e)
        {
            //tR2Pnl.Focus();
        }

        private void TR1Pnl_MouseHover(object sender, EventArgs e)
        {
            //tR1Pnl.Focus();
        }

        private void TR2ItemDgv_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            foreach (DataGridViewRow row in tR2ItemDgv.Rows)
            {
                if (!(row.DataBoundItem is TransferTable rowItem)) return;

                row.ReadOnly = true;
                if (tR2OutRdb.Checked && GetItemTypeByItemCode(rowItem.ItemCode).Name.StartsWith("FWIP"))
                {
                    row.Cells["Count"].ReadOnly = false;
                }
            }
        }

        private void TR2ItemDgv_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (!(tR2ItemDgv.Rows[e.RowIndex].DataBoundItem is TransferTable rowItem)) return;

            //var actualItem = GetItemByID(rowItem.ItemID);
            //var grossWeightPerItem = actualItem.Measure / actualItem.Count;
            //var netWeightPerItem =
            //    (actualItem.Measure - (actualItem.InnerPackageWeight + actualItem.OuterPackageWeight +
            //                           actualItem.MiscPackageWeight)) / actualItem.Count;
            //rowItem.GrossWeight = (grossWeightPerItem ?? 0) * rowItem.Count;
            //rowItem.NetWeight = (netWeightPerItem ?? 0) * rowItem.Count;

            //tR2ItemDgv.Refresh();
            TrRecalculateWeight();
        }

        private void tR1TransferNoTxb_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if (e.KeyChar == (char)Keys.Return) tR1SubmitBtn.PerformClick();
        }
    }
}