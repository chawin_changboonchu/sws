﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Windows.Forms;
using SWS.Model;

namespace SWS
{
    public partial class Inventory
    {
        private void AIPnl_Enter(object sender, EventArgs e)
        {
        }

        private void ItemSetupTabPage_Enter(object sender, EventArgs e)
        {
            log.Info("Start Method: ItemSetupTabPage_Enter() No parameter");
        }

        private void AI1CategoryDgv_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            log.Info("Start Method: AI1CategoryDgv_CellEndEdit() No parameter");

            try
            {
                var adminCategoryTableItem = aI1CategoryDgv.Rows[e.RowIndex].DataBoundItem as AdminItemCategoryTable;
                var editedItem = _entities.ItemCategory.FirstOrDefault(ic => ic.ID == adminCategoryTableItem.ID);

                editedItem.DefaultAttributes = adminCategoryTableItem.Attributes;
                editedItem.Description = adminCategoryTableItem.Description;

                WriteLog(0, "Item", "Setup", $"เปลี่ยนแปลงการตั้งค่าสินค้า {editedItem?.ItemCode} เป็นคุณสมบัติ: {editedItem?.DefaultAttributes} และรายละเอียด: {editedItem?.Description}");

                _entities.SaveChanges();
            }
            catch (Exception ex)
            {
                log.Error("Method: AI1CategoryDgv_CellEndEdit() Error: " + ex.Message); MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: AI1CategoryDgv_CellEndEdit()");
            }
        }

        private void AI1TypeDgv_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            log.Info("Start Method: AI1TypeDgv_CellEndEdit() No parameter");

            try
            {
                if (e.RowIndex <= 0 || e.ColumnIndex < 2 || e.ColumnIndex > 3)
                {
                    return;
                }

                var adminTypeTableItem = aI1TypeDgv.Rows[e.RowIndex].DataBoundItem as AdminItemTypeTable;
                var editedItem = _entities.ItemType.FirstOrDefault(it => it.ID == adminTypeTableItem.ID);

                switch (e.ColumnIndex)
                {
                    //Count
                    case 2:
                        editedItem.UOM_Count = adminTypeTableItem.UOMCount;
                        break;
                    //Measure
                    case 3:
                        editedItem.UOM_Measure = adminTypeTableItem.UOMMeasure;
                        break;
                }

                WriteLog(0, "Item", "Setup", $"เปลี่ยนแปลงการตั้งค่าสินค้า {editedItem?.Name} เป็นหน่วยนับ {editedItem?.UOM_Count} และหน่วยวัด {editedItem?.UOM_Measure}");

                _entities.SaveChanges();
            }
            catch (Exception ex)
            {
                log.Error("Method: AI1TypeDgv_CellEndEdit() Error: " + ex.Message); MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: AI1TypeDgv_CellEndEdit()");
            }
        }

        private void aI1SubmitBtn_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in aI1TypeDgv.Rows)
            {
                var adminTypeTableItem = row.DataBoundItem as AdminItemTypeTable;
                var itemType = _entities.ItemType.FirstOrDefault(it => it.ID == adminTypeTableItem.ID);
                var uomCountCbb = (DataGridViewComboBoxCell)(row.Cells["UOMCount"]);
                var uomMeasureCbb = (DataGridViewComboBoxCell)(row.Cells["UOMMeasure"]);
                itemType.UOM_Count = uomCountCbb.Value == null ? "" : uomCountCbb.Value.ToString();
                itemType.UOM_Measure = uomMeasureCbb.Value == null ? "" : uomMeasureCbb.Value.ToString();

            }

            _entities.SaveChanges();
            MessageBox.Show("บันทึกเรียบร้อย");
        }

        private void aI1SyncBtn_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                var currentList = _entities.ItemCategory.ToList();
                var itemTypeList = _entities.ItemType.ToList();
                var newItemCategoryList = new List<ItemCategory>();
                foreach (var item in _b1Entities.OITM.ToList())
                {
                    var currentItem = currentList.FirstOrDefault(i => i.ItemCode == item.ItemCode);

                    if (currentItem == null)
                    {
                        ItemType itemType;

                        currentItem = new ItemCategory()
                        {
                            ItemCode = item.ItemCode,
                            Barcode = item.ItemCode,
                            ItemGroupID = item.ItmsGrpCod,
                            LotNo = item.ItemCode,
                            Description = item.ItemName
                        };

                        switch (item.ItmsGrpCod.Value)
                        {
                            case 103:
                                currentItem.ItemType = (itemTypeList.FirstOrDefault(it => it.ID == 10));
                                break;
                            case 113:
                                currentItem.ItemType = (itemTypeList.FirstOrDefault(it => it.ID == 9));
                                break;
                            case 115:
                                if (currentItem.ItemCode.ToUpper().Contains("CARD-"))
                                {
                                    currentItem.ItemType = (itemTypeList.FirstOrDefault(it => it.ID == 3));
                                }
                                else if (currentItem.ItemCode.ToUpper().Contains("ROV"))
                                {
                                    currentItem.ItemType = (itemTypeList.FirstOrDefault(it => it.ID == 4));
                                }
                                break;
                            case 101:
                                currentItem.ItemType = (itemTypeList.FirstOrDefault(it => it.ID == 1));
                                break;
                        }

                        newItemCategoryList.Add(currentItem);
                    }
                    else
                    {
                        if (currentItem.ItemGroupID != item.ItmsGrpCod || currentItem.Description != item.ItemName)
                        {
                            currentItem.ItemCode = item.ItemCode;
                            currentItem.Barcode = item.ItemCode;
                            currentItem.ItemGroupID = item.ItmsGrpCod;
                            currentItem.LotNo = item.ItemCode;
                            currentItem.Description = item.ItemName;
                        }

                        if (currentItem.ItemType == null)
                        {
                            switch (currentItem.ItemGroupID.Value)
                            {
                                case 103:
                                    currentItem.ItemType = (itemTypeList.FirstOrDefault(it => it.ID == 10));
                                    break;
                                case 113:
                                    currentItem.ItemType = (itemTypeList.FirstOrDefault(it => it.ID == 9));
                                    break;
                                case 115:
                                    if (currentItem.ItemCode.ToUpper().Contains("CARD-"))
                                    {
                                        currentItem.ItemType = (itemTypeList.FirstOrDefault(it => it.ID == 3));
                                    }
                                    else if (currentItem.ItemCode.ToUpper().Contains("ROV"))
                                    {
                                        currentItem.ItemType = (itemTypeList.FirstOrDefault(it => it.ID == 4));
                                    }
                                    break;
                                case 101:
                                    currentItem.ItemType = (itemTypeList.FirstOrDefault(it => it.ID == 1));
                                    break;
                            }
                        }
                    }
                }

                if (newItemCategoryList.Count > 0)
                {
                    _entities.ItemCategory.AddRange(newItemCategoryList);
                    
                }
                _entities.SaveChanges();
            }
            catch (Exception ex)
            {
                log.Error("Method: aI1SyncBtn_Click() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }

            Cursor.Current = Cursors.Default;
            MessageBox.Show("บันทึกเรียบร้อย");
        }
    }
}