﻿using MetroFramework.Controls;
using Spire.Xls;
using SWS.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data.Entity;
using System.Diagnostics.Eventing.Reader;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace SWS
{
    public partial class Inventory
    {
        public void IP1Pnl_KeyPress(Object sender, KeyPressEventArgs e)
        {
            log.Info("Start Method: IP1Pnl_KeyPress() No parameter");

            try
            {
                if (e.KeyChar != (char)Keys.Return)
                {
                    _ip1BarcodeBuffer += e.KeyChar;
                    return;
                }

                HandleIP1SubmitAction();

                _ip1BarcodeBuffer = "";
            }
            catch (Exception ex)
            {
                log.Error("Method: IP1Pnl_KeyPress() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: IP1Pnl_KeyPress()");
            }
        }

        private List<B1_JobOrderInterface> GetGroupedResultIssueItemList(long jobIssueID)
        {
            var b1JobList = jobList.Where(i => i.JO_DocNum.ToString() == _issueProductionInstance.RefNo).ToList();
            var newJobOrderInterfaceList = new List<B1_JobOrderInterface>();

            foreach (var item in _scannedItemList)
            {
                var b1Job = b1JobList.FirstOrDefault(j => j.RM_ItemCode == item.ItemCode);
                var itemType = GetItemTypeByItemCode(item.ItemCode);
                var itemCategory = itemType.ItemCategory;

                newJobOrderInterfaceList.Add(new B1_JobOrderInterface()
                {
                    DocEntry = b1Job.JO_DocEntry,
                    FromWhseCode = item.Whse,
                    ItemCode = b1Job.RM_ItemCode,
                    JobIssueID = jobIssueID,
                    LineNo = b1Job.RM_LineNum ?? 0,
                    LotNo = itemType.isItemizable.Value ? item.LotNo.Substring(0, item.LotNo.Length - 5) : item.LotNo,
                    NetPerLot = item.NetWeight
                });
            }

            var groupedIssueProductionItemList = newJobOrderInterfaceList.GroupBy(joi => new { joi.LotNo, joi.FromWhseCode });

            var resultIssueProductionItemList = groupedIssueProductionItemList.Select(gipi => new B1_JobOrderInterface()
            {
                DocEntry = gipi.FirstOrDefault().DocEntry,
                FromWhseCode = gipi.FirstOrDefault().FromWhseCode,
                ItemCode = gipi.FirstOrDefault().ItemCode,
                JobIssueID = gipi.FirstOrDefault().JobIssueID,
                LineNo = gipi.FirstOrDefault().LineNo,
                LotNo = gipi.FirstOrDefault().LotNo,
                NetPerLot = gipi.Sum(i => i.NetPerLot)
            }).ToList();

            return resultIssueProductionItemList;
        }

        private List<B1_JobOrder> GetJobOrderDetail(string jobOrderNo, string itemCode)
        {
            log.Info("Start Method: GetJobOrderDetail() No parameter");

            try
            {
                string whereStr = "";
                List<B1_JobOrder> grs = new List<B1_JobOrder>();

                if (jobOrderNo != "")
                {
                    //whereStr = string.Format(@" and T0.[DocNum] = '{0}' and T0.[ItemCode] = '{1}'", jobOrderNo, itemCode);
                    whereStr = string.Format(@" and T0.[DocNum] = '{0}'", jobOrderNo);
                }

                var result = _b1Entities.Database.SqlQuery<B1_JobOrder>(
                    string.Format(@"
                        SELECT T0.[DocNum] AS 'JO_DocNum'
                        , T0.[DocEntry] AS 'JO_DocEntry'
                        , T0.[Status] AS 'JO_Status'
                        , T0.[ItemCode] AS 'JO_ItemCode'
                        , T2.[ItemName] AS 'JO_FGName'
                        , T0.[ProdName] AS 'JO_ProdName'
                        , T0.[Warehouse] AS 'JO_Warehouse'
                        , T0.[StartDate] AS 'JO_StartDate'
                        , T0.[DueDate] AS 'JO_DueDate'
                        , T0.[PlannedQty] AS 'JO_PlannedQty'
                        , T0.[CmpltQty] AS 'JO_CmpltQty'
                        , (T0.[PlannedQty] - T0.[CmpltQty]) AS 'JO_OpenQty_FG'
                        , T0.[RjctQty] AS 'JO_RejectQty_FG'
                        , T0.[Uom] AS 'JO_Uom'
                        , T0.[OriginNum] AS 'JO_SaleOrder'
                        , T0.[CardCode] AS 'JO_CustomerID'
                        , T5.[CardName] AS 'J0_CustomenName'
                        , T2.[SWW] AS 'Barcode_ColorTag'
                        , T1.[LineNum] AS 'RM_LineNum'
                        , T1.[ItemCode] AS 'RM_ItemCode'
                        , T3.[ItemName] AS 'RM_ItemName'
                        , T1.[PlannedQty] AS 'RM_PlannedQty'
                        , T1.[IssuedQty] AS 'RM_IssuedQty'
                        , T1.[IssueType] AS 'RM_IssueType'
                        , T1.[wareHouse] AS 'RM_wareHouse'
                        , T1.[ItemType] AS 'RM_ItemType'
                        , T3.[InvntryUom] AS 'RM_InvntryUom'
                        , T4.[ItmsGrpCod] AS 'RM_ItemGroup'
                        , IIF (T1.[ItemType] = 4,
		                        (Select top (1) a.BatchNum
			                        from (
			                        SELECT Ta.[ItemCode],Ta.[InDate]
			                        , Ta.[BatchNum], Ta.[WhsCode], Ta.[ItemName], Ta.[SuppSerial], Ta.[Quantity]
			                        FROM OIBT Ta
			                        WHERE Ta.[Quantity] > 0 and Ta.[ItemCode] = T1.[ItemCode] and Ta.[WhsCode] = T1.[wareHouse] --and Ta.Quantity >= T1.[IssuedQty]
			                        ) a
			                        ORDER BY a.[InDate], a.[Quantity]  ASC)
	                        ,NULL )	AS 'RM_LotSuggest'
                        , IIF (T1.[ItemType] = 4,
		                        (Select top (1) a.[Quantity]
			                        from (
			                        SELECT Ta.[ItemCode],Ta.[InDate]
			                        , Ta.[BatchNum], Ta.[WhsCode], Ta.[ItemName], Ta.[SuppSerial], Ta.[Quantity]
			                        FROM OIBT Ta
			                        WHERE Ta.[Quantity] > 0 and Ta.[ItemCode] = T1.[ItemCode] and Ta.[WhsCode] = T1.[wareHouse] --and Ta.Quantity >= T1.[IssuedQty]
			                        ) a
			                        ORDER BY a.[InDate], a.[Quantity]  ASC)
	                        ,NULL )	AS 'RM_LotSuggest_Qty'
                        , IIF (T1.[ItemType] = 4,
		                        (Select top (1) a.[SuppSerial]
			                        from (
			                        SELECT Ta.[ItemCode],Ta.[InDate]
			                        , Ta.[BatchNum], Ta.[WhsCode], Ta.[ItemName], Ta.[SuppSerial], Ta.[Quantity]
			                        FROM OIBT Ta
			                        WHERE Ta.[Quantity] > 0 and Ta.[ItemCode] = T1.[ItemCode] and Ta.[WhsCode] = T1.[wareHouse] --and Ta.Quantity >= T1.[IssuedQty]
			                        ) a
			                        ORDER BY a.[InDate], a.[Quantity]  ASC)
	                        ,NULL )	AS 'RM_LotSuggest_Attribute'
                        FROM OWOR T0
                        INNER JOIN WOR1 T1 ON T0.[DocEntry] = T1.[DocEntry]
                        left JOIN OITM T2 ON T0.[ItemCode] = T2.[ItemCode]
                        left JOIN OITM T3 ON T1.[ItemCode] = T3.[ItemCode]
                        left JOIN OITB T4 ON T3.[ItmsGrpCod] = T4.[ItmsGrpCod]
                        left JOIN OCRD T5 ON T0.[CardCode] = T5.[CardCode]
                        WHERE T0.[Status] = 'R'
                        and T1.[IssueType] = 'M'
                        and  (T0.[PlannedQty] - T0.[CmpltQty]) > 0
                        {0}
                    ", whereStr)).ToList();

                return result;
            }
            catch (Exception ex)
            {
                log.Error("Method: GetJobOrderDetail() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
                return null;
            }
            finally
            {
                log.Info("End Method: GetJobOrderDetail()");
            }
        }

        private void HandleIP1SubmitAction()
        {
            log.Info("Start Method: HandleIP1SubmitAction() No parameter");

            try
            {
                var jobNo = iP1JobNoTxb.Text.Trim();

                if (jobNo == "" && string.IsNullOrEmpty(_ip1BarcodeBuffer))
                {
                    MessageBox.Show("กรุณากรอกหมายเลขใบสั่งผลิต");
                    return;
                }

                if (!string.IsNullOrEmpty(_ip1BarcodeBuffer))
                {
                    var docNo = _ip1BarcodeBuffer.Trim();
                    _issueProductionInstance = _entities.IssueProduction.FirstOrDefault(ip => ip.DocNo == docNo || ip.RefNo == docNo);
                    if (_issueProductionInstance != null)
                    {
                        jobNo = _issueProductionInstance.RefNo;
                    }
                    else
                    {
                        if (int.TryParse(docNo, out var docNoInt))
                        {
                            var matchedJoList = jobList.Where(jo => jo.JO_DocNum == docNoInt).ToList();

                            if (matchedJoList.Count >= 1)
                            {
                                iP1JobNoTxb.Text = "" + docNoInt;
                                //iP1SubmitBtn.PerformClick();
                                //IPShowPanel(ref iP2Pnl);
                                //return;
                            }
                            else //retry
                            {
                                jobList = GetJobOrderDetail("", "");
                                matchedJoList = jobList.Where(jo => jo.JO_DocNum == docNoInt).ToList();
                                if (matchedJoList.Count >= 1)
                                {
                                    iP1JobNoTxb.Text = "" + docNoInt;
                                    //iP1SubmitBtn.PerformClick();
                                    //IPShowPanel(ref iP2Pnl);
                                    //return;
                                }
                                else
                                {
                                    MessageBox.Show("ไม่พบหมายเลขเอกสารที่แสกน");
                                    return;
                                }
                            }

                        }
                    }
                }
                else
                {
                    _issueProductionInstance = _entities.IssueProduction.FirstOrDefault(ip => ip.RefNo == jobNo);
                }

                var b1JobList = jobList.Where(i => i.JO_DocNum.ToString() == jobNo).ToList();

                if (b1JobList.Count > 0)
                {
                    jobRmDetail = b1JobList.FirstOrDefault();
                }

                var docFormat = _entities.DocumentSetup.FirstOrDefault(ds => ds.DocType == "Issue for Production")
                    .DocFormat;

                var todayDocCount = _entities.IssueProduction.Count(gl =>
                    gl.CreationDate.Value.Year == DateTime.Now.Year &&
                    gl.CreationDate.Value.Month == DateTime.Now.Month && gl.CreationDate.Value.Day == DateTime.Now.Day);

                _operationList = new List<B1_JobOrder>();

                if (_issueProductionInstance == null)
                {
                    if (IsItemFGW(jobRmDetail?.JO_ItemCode))
                    {
                        _issueProductionInstance = new IssueProduction()
                        {
                            DocNo = "TRP-" + ReplaceFormatText(docFormat, "", "", "", "", "", "") + $"-{todayDocCount + 1:D3}",
                            AssignDate = jobRmDetail.JO_StartDate,
                            CreationDate = DateTime.Now,
                            Barcode =
                                "TRP-" + ReplaceFormatText(docFormat, "", "", "", "", "", "") + $"-{todayDocCount + 1:D3}",
                            CustomerName = jobRmDetail.J0_CustomenName,
                            DueDate = jobRmDetail.JO_DueDate,
                            FinishItemID = _entities.ItemCategory
                                .FirstOrDefault(ic => ic.ItemCode == jobRmDetail.JO_ItemCode)?.ID,
                            JobOrderNo = jobRmDetail.JO_DocNum.ToString(),
                            QtyCompleted = jobRmDetail.JO_CmpltQty ?? 0,
                            QtyOpen = jobRmDetail.JO_OpenQty_FG ?? 0,
                            QtyPlanned = jobRmDetail.JO_PlannedQty ?? 0,
                            QtyRejected = jobRmDetail.JO_RejectQty_FG ?? 0,
                            RefNo = jobRmDetail.JO_DocNum.ToString(),
                            SORef = jobRmDetail.JO_SaleOrder.ToString(),
                            Whse = jobRmDetail.JO_Warehouse
                        };
                    }
                    else
                    {
                        _issueProductionInstance = new IssueProduction()
                        {
                            DocNo = "TRP-" + ReplaceFormatText(docFormat, "", "", "", "", "", "") + $"-{todayDocCount + 1:D3}",
                            AssignDate = jobRmDetail.JO_StartDate,
                            CreationDate = DateTime.Now,
                            Barcode =
                               "TRP-" + ReplaceFormatText(docFormat, "", "", "", "", "", "") + $"-{todayDocCount + 1:D3}",
                            CustomerName = jobRmDetail.J0_CustomenName,
                            DueDate = jobRmDetail.JO_DueDate,
                            FinishItemID = _entities.ItemCategory
                               .FirstOrDefault(ic => ic.ItemCode == jobRmDetail.JO_ItemCode)?.ID,
                            JobOrderNo = jobRmDetail.JO_DocNum.ToString(),
                            QtyCompleted = ConvertLbsToKgs(jobRmDetail.JO_CmpltQty ?? 0),
                            QtyOpen = ConvertLbsToKgs(jobRmDetail.JO_OpenQty_FG ?? 0),
                            QtyPlanned = ConvertLbsToKgs(jobRmDetail.JO_PlannedQty ?? 0),
                            QtyRejected = ConvertLbsToKgs(jobRmDetail.JO_RejectQty_FG ?? 0),
                            RefNo = jobRmDetail.JO_DocNum.ToString(),
                            SORef = jobRmDetail.JO_SaleOrder.ToString(),
                            Whse = jobRmDetail.JO_Warehouse
                        };
                    }


                    _entities.IssueProduction.Add(_issueProductionInstance);
                    _entities.SaveChanges();

                    if (_issueProductionRequestList == null)
                    {
                        _issueProductionRequestList = new List<IssueProductionRequest>();
                    }
                    else
                    {
                        _issueProductionRequestList.Clear();
                    }

                    foreach (var item in b1JobList)
                    {
                        var itemCategory = FindCategoryByItemCode(item.RM_ItemCode);

                        if (itemCategory != null)
                        {
                            itemCategory.DefaultAttributes = item.RM_LotSuggest_Attribute;
                            var measure = item.RM_InvntryUom == "Lbs"
                                ? ConvertLbsToKgs((decimal)item.RM_PlannedQty)
                                : (decimal)item.RM_PlannedQty;
                            var qtyCount = 0.00M;
                            var standardWeight = GetStandardItemWeightPerUnitByItemCode(itemCategory.ItemCode);
                            if (standardWeight > 0)
                            {
                                qtyCount = Math.Ceiling(Decimal.Divide(measure, standardWeight));
                            }

                            var newIssueProductionRequest = new IssueProductionRequest()
                            {
                                IssueProductionID = _issueProductionInstance.ID,
                                ItemCategoryID = itemCategory.ID,
                                ItemCategory = itemCategory,
                                Measure = measure,
                                SuggestedLot = item.RM_LotSuggest,
                                Whse = item.RM_wareHouse,
                                Count = qtyCount
                            };

                            _entities.IssueProductionRequest.Add(newIssueProductionRequest);
                            _entities.SaveChanges();

                            _issueProductionRequestList.Add(newIssueProductionRequest);
                        }
                        else
                        {
                            _operationList.Add(item);
                        }
                    }
                }
                else
                {
                    foreach (var item in b1JobList)
                    {
                        var itemCategory = FindCategoryByItemCode(item.RM_ItemCode);

                        if (itemCategory == null)
                        {
                            _operationList.Add(item);
                        }
                    }

                    _issueProductionRequestList = _entities.IssueProductionRequest
                        .Where(ip => ip.IssueProductionID == _issueProductionInstance.ID).ToList();
                }

                var finishItem =
                    _entities.ItemCategory.FirstOrDefault(ic => ic.ID == _issueProductionInstance.FinishItemID);

                var qtyCompleteCount = 0.00M;
                var qtyRejectCount = 0.00M;
                var standardWeightForThisItem = GetStandardItemWeightPerUnitByItemCode(finishItem?.ItemCode);
                if (standardWeightForThisItem > 0)
                {
                    qtyCompleteCount = decimal.Divide(_issueProductionInstance.QtyCompleted ?? 0,
                        standardWeightForThisItem);
                    qtyRejectCount = decimal.Divide(_issueProductionInstance.QtyRejected ?? 0,
                        standardWeightForThisItem);
                }

                iP2JobOrderNoTxb.Text = _issueProductionInstance.DocNo;
                iP2AssignDateTxb.Text =
                    _issueProductionInstance.AssignDate.Value.ToString("d MMMM yyyy",
                        CultureInfo.CreateSpecificCulture("th-TH"));
                iP2RefNoTxb.Text = _issueProductionInstance.RefNo;
                iP2CustomerTxb.Text = _issueProductionInstance.CustomerName;
                iP2ItemCodeTxb.Text = finishItem.ItemCode;
                iP2ItemDescTxb.Text = finishItem.Description;
                iP2WhseCbb.SelectedItem = _issueProductionInstance.Whse;
                iP2DueDateTxb.Text =
                    _issueProductionInstance.DueDate.Value.ToString("d MMMM yyyy",
                        CultureInfo.CreateSpecificCulture("th-TH"));
                iP2PlannedQtyTxb.Text = $"{_issueProductionInstance.QtyPlanned.Value:N2}";
                iP2CompletedQtyTxb.Text = $"{_issueProductionInstance.QtyCompleted.Value:N2}";
                iP2CompletedQty2Txb.Text = $"{jobRmDetail.JO_CmpltQty ?? 0:N2}";
                iP2RejectedQtyTxb.Text = $"{_issueProductionInstance.QtyRejected.Value:N2}";
                iP2OpenQtyTxb.Text = $"{_issueProductionInstance.QtyOpen.Value:N2}";
                iP2SORefTxb.Text = jobRmDetail.JO_SaleOrder.HasValue ? jobRmDetail.JO_SaleOrder.Value.ToString() : "";
                iP2CompletedQtyCountTxb.Text = Math.Ceiling(qtyCompleteCount).ToString();
                iP2RejectedQtyCountTxb.Text = Math.Ceiling(qtyRejectCount).ToString();
                iP2BcColorTxb.Text = jobRmDetail.Barcode_ColorTag ?? "";

                var countUnit = GetThaiUOMCountFromItemCode(_issueProductionInstance.ItemCategory.ItemCode);
                var measureUnit = GetThaiUOMMeasureFromItemCode(_issueProductionInstance.ItemCategory.ItemCode);
                iP2CompletedQtyCountUnitLbl.Text = countUnit;
                iP2RejectedQtyCountUnitLbl.Text = countUnit;
                iP2PlannedQtyUnitLbl.Text = measureUnit;
                iP2CompletedQtyUnitLbl.Text = measureUnit;
                iP2RejectedQtyUnitLbl.Text = measureUnit;
                iP2OpenQtyUnitLbl.Text = measureUnit;

                if (_issueProductionInstance.ItemCategory.ItemType.Name == "Finish Goods - WE")
                {
                    iP2CompletedQty2Txb.Visible = false;
                    iP2CompletedQty2UnitLbl.Visible = false;
                }
                else
                {
                    iP2CompletedQty2Txb.Visible = true;
                    iP2CompletedQty2UnitLbl.Visible = true;
                }

                iP4PrintAllChk.Checked = true;
                iP4PrintFromTxb.Text = "";
                iP4PrintToTxb.Text = "";
                iP4PrintFromTxb.Enabled = false;
                iP4PrintToTxb.Enabled = false;

                var itemRequestTableList = new List<IssueProductionRequestTable>();

                var rowno = 1;

                foreach (var item in _issueProductionRequestList)
                {
                    var jobLine = b1JobList.FirstOrDefault(jo => jo.RM_ItemCode == item.ItemCategory.ItemCode);
                    itemRequestTableList.Add(new IssueProductionRequestTable()
                    {
                        ID = item.ID,
                        RowNo = rowno++,
                        ItemCode = item.ItemCategory.ItemCode,
                        Description = item.ItemCategory.Description,
                        SuggestedLot = item.SuggestedLot,
                        LotAttributes = jobLine.RM_LotSuggest_Attribute ?? "",
                        Count = item.Count ?? 0,
                        CountUnit = item.ItemCategory.ItemType?.UOM_Count,
                        Measure = (decimal)item.Measure,
                        MeasureUnit = item.ItemCategory.ItemType?.UOM_Measure,
                        Whse = item.Whse
                    });
                }

                if (_operationList != null)
                {
                    foreach (var item in _operationList)
                    {
                        itemRequestTableList.Add(new IssueProductionRequestTable()
                        {
                            ID = -1,
                            RowNo = rowno++,
                            ItemCode = item.RM_ItemCode,
                            Description = "",
                            SuggestedLot = "",
                            LotAttributes = "",
                            Count = 0,
                            CountUnit = "",
                            Measure = (decimal)item.RM_PlannedQty, //don't know measure unit
                            MeasureUnit = item.RM_InvntryUom ?? "",
                            Whse = item.RM_wareHouse
                        });
                    }
                }

                iP2IssueProductionItemDgv.AutoGenerateColumns = true;
                iP2IssueProductionItemDgv.DataSource = itemRequestTableList;
                iP2IssueProductionItemDgv.ReadOnly = true;
                iP2IssueProductionItemDgv.Columns[7].DefaultCellStyle.Format = "N2";
                iP2IssueProductionItemDgv.Columns[5].DefaultCellStyle.Format = "N";

                IPShowPanel(ref iP2Pnl);
            }
            catch (Exception ex)
            {
                log.Error("Method: HandleIP1SubmitAction() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                WriteLog(_issueProductionInstance?.ID, "JO", "Load",
                    $"โหลดหน้า Job order ด้วยหมายเลข: {_issueProductionInstance?.DocNo} รหัสสินค้า: {_issueProductionInstance?.ItemCategory.ItemCode}");
                log.Info("End Method: HandleIP1SubmitAction()");
            }
        }

        private bool Interface_GoodsIssueExcelTemplate(List<B1_JobOrderInterface> groupIssues)
        {
            log.Info("Start Method: Interface_GoodsIssueExcelTemplate()");

            try
            {
                //AOM
                // gen B1 header template
                var docEntry = "1";
                var docDate = DateTime.Today.ToString("yyyyMMdd", CultureInfo.CreateSpecificCulture("en-US"));
                var ref2 = groupIssues[0].JobIssueID.ToString(); //BC ref

                var _interfaceWorkbook_Header = new Workbook();
                _interfaceWorkbook_Header.LoadFromFile(
                   ConfigurationManager.AppSettings["JobIssue_HeaderCsvTemplate"],
                    ",", 1, 1);
                var worksheetHd = _interfaceWorkbook_Header.Worksheets[0];

                worksheetHd.Range["A3"].Text = docEntry;
                worksheetHd.Range["E3"].Text = docDate;
                worksheetHd.Range["P3"].Text = "Job Issue ID: " + ref2 + " and Job Order DocEntry No.: " + groupIssues[0].DocEntry.ToString(); //comment

                var tempWorkBookHd = new Workbook();
                tempWorkBookHd.Version = ExcelVersion.Version2010;
                tempWorkBookHd.Worksheets.Clear();
                tempWorkBookHd.Worksheets.AddCopy(worksheetHd);

                string headerPath = SaveFileCSV(tempWorkBookHd, "Header_GoodsIssue");

                //B1 line & Batch
                int i = 3; //start at row 3
                int j = 0;

                var _interfaceWorkbook_Line = new Workbook();
                _interfaceWorkbook_Line.LoadFromFile(
                    ConfigurationManager.AppSettings["JobIssue_LineCsvTemplate"],
                    ",", 1, 1);
                var worksheetLn = _interfaceWorkbook_Line.Worksheets[0];

                var _interfaceWorkbook_Batch = new Workbook();
                _interfaceWorkbook_Batch.LoadFromFile(
                    ConfigurationManager.AppSettings["JobIssue_BatchCsvTemplate"],
                    ",", 1, 1);
                var worksheetBt = _interfaceWorkbook_Batch.Worksheets[0];

                foreach (B1_JobOrderInterface ji in groupIssues)
                {
                    string parentKey = "1"; //always 1
                    string lineNum = j.ToString(); //start at 0
                    string lineQuantity = ConvertKgsToLbs(ji.NetPerLot).ToString().Replace(",", ""); //Lbs
                    string fromWh = ji.FromWhseCode;
                    string baseType = "202";
                    string baseEntry = ji.DocEntry.ToString(); //jobRmDetail.JO_DocEntry.ToString();
                    string baseLine = ji.LineNo == null ? "0" : ji.LineNo.ToString();

                    worksheetLn.Range["A" + i.ToString()].Text = parentKey;
                    worksheetLn.Range["B" + i.ToString()].Text = lineNum;
                    worksheetLn.Range["E" + i.ToString()].Text = lineQuantity;
                    worksheetLn.Range["N" + i.ToString()].Text = fromWh;
                    worksheetLn.Range["AO" + i.ToString()].Text = baseType;
                    worksheetLn.Range["AP" + i.ToString()].Text = baseEntry;
                    worksheetLn.Range["AQ" + i.ToString()].Text = baseLine;

                    string batchDocNum = "1";
                    string distNumber = ji.LotNo;
                    string batchQuantity = lineQuantity;
                    string baseLineNumber = lineNum;

                    worksheetBt.Range["A" + i.ToString()].Text = batchDocNum;
                    worksheetBt.Range["C" + i.ToString()].Text = distNumber;
                    worksheetBt.Range["K" + i.ToString()].Text = batchQuantity;
                    worksheetBt.Range["L" + i.ToString()].Text = baseLineNumber;

                    i++;
                    j++;

                    WriteLog(_issueProductionInstance?.ID, "JI", "Save"
                        , $"บันทึกข้อมูลการเบิกสินค้า JobIssueID:{ji.JobIssueID} รหัสสินค้า:{ji.ItemCode} Total Net weight:{ji.NetPerLot} ออกจากโกดัง:{ji.FromWhseCode}");

                }

                //B1 Line
                var tempWorkBookLn = new Workbook();
                tempWorkBookLn.Version = ExcelVersion.Version2010;
                tempWorkBookLn.Worksheets.Clear();
                tempWorkBookLn.Worksheets.AddCopy(worksheetLn);

                string linePath = SaveFileCSV(tempWorkBookLn, "Line_GoodsIssue");

                //B1 Batch
                var tempWorkBookBt = new Workbook();
                tempWorkBookBt.Version = ExcelVersion.Version2010;
                tempWorkBookBt.Worksheets.Clear();
                tempWorkBookBt.Worksheets.AddCopy(worksheetBt);

                string batchPath = SaveFileCSV(tempWorkBookBt, "Batch_GoodsIssue");

                ////Copy XML to new one and also change template paths in XML
                //bool isTransfer = false;
                //string templatePath = ConfigurationManager.AppSettings["JobIssue_XmlTemplate"];
                //string xmlOutputPath = ConfigurationManager.AppSettings["InterfaceOutputPath"] + "JobIssueXml_" + DateTime.Now.ToString("dd-MM-yy-hhmmss") + ".xml"; ;
                //CreateXMLInterface(isTransfer, templatePath, headerPath, linePath, batchPath, xmlOutputPath);

                ////Call .bat
                //string batName = "JobIssueBat_" + DateTime.Now.ToString("dd-MM-yy-hhmmss");
                //CreateBatFile(batName, xmlOutputPath);

                //Copy XML to new one and also change template paths in XML
                bool isTransfer = false;
                string templatePath = ConfigurationManager.AppSettings["JobIssue_XmlTemplate"];
                string xmlFileName = "JobIssueXml_" + DateTime.Now.ToString("dd-MM-yy-hhmmss") + ".xml";
                string xmlOutputPath = ConfigurationManager.AppSettings["InterfaceOutputPath"] + xmlFileName;
                string localXmlPath = ConfigurationManager.AppSettings["LocalTemplateOutputPath"] + xmlFileName;
                CreateXMLInterface(isTransfer, templatePath, headerPath, linePath, batchPath, xmlOutputPath);

                //Call .bat
                string batName = "JobIssueBat_" + DateTime.Now.ToString("dd-MM-yy-hhmmss");
                CreateBatFile(batName, localXmlPath);

                //Detect error log from DTW
                bool result = GetInterfaceResult(batName);

                return result;
            }
            catch (Exception ex)
            {
                log.Error("Method: Interface_GoodsIssueExcelTemplate() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
                return false;
            }
            finally
            {
                log.Info("End Method: Interface_GoodsIssueExcelTemplate()");
            }
        }

        private bool Interface_GoodsReceiveExcelTemplate()
        {
            log.Info("Start Method: Interface_GoodsReceiveExcelTemplate() No parameter");

            try
            {
                //AOM
                // gen B1 header template
                var docEntry = "1";
                var docDate = iP4DateCbb.Value.ToString("yyyyMMdd", CultureInfo.CreateSpecificCulture("en-US"));
                var ref2 = "Job Receive ID: " + _jobReceiveInstance.ID; //ref BC doc no.

                var _interfaceWorkbook_Header = new Workbook();
                _interfaceWorkbook_Header.LoadFromFile(
                    ConfigurationManager.AppSettings["JobReceive_HeaderCsvTemplate"],
                    ",", 1, 1);
                var worksheetHd = _interfaceWorkbook_Header.Worksheets[0];

                worksheetHd.Range["A3"].Text = docEntry;
                worksheetHd.Range["E3"].Text = docDate;
                worksheetHd.Range["P3"].Text = ref2;

                var tempWorkBookHd = new Workbook();
                tempWorkBookHd.Version = ExcelVersion.Version2010;
                tempWorkBookHd.Worksheets.Clear();
                tempWorkBookHd.Worksheets.AddCopy(worksheetHd);

                string headerPath = SaveFileCSV(tempWorkBookHd, "Header_GoodsReceive");

                //B1 line
                string parentKey = "1";
                string lineNum = "0";

                string lineQuantity = "";
                if (_jobReceiveInstance.JobReceiveItem.FirstOrDefault().Item.ItemType.ID == 3 || _jobReceiveInstance.JobReceiveItem.FirstOrDefault().Item.ItemType.ID == 4)
                {
                    //WIP
                    lineQuantity = iP4CompletedMeasureLbsTxb.Text.Replace(",", ""); //Lbs
                }
                else if (_jobReceiveInstance.JobReceiveItem.FirstOrDefault().Item.ItemType.ID == 9)
                {
                    //FGS
                    lineQuantity = ConvertKgsToLbs(Convert.ToDecimal(iP4TotalNetWeightTxb.Text)).ToString().Replace(",", ""); //Lbs
                }
                else if (_jobReceiveInstance.JobReceiveItem.FirstOrDefault().Item.ItemType.ID == 10)
                {
                    //FGW
                    lineQuantity = iP4TotalNetWeightTxb.Text.Replace(",", ""); //Yard
                }
                else
                {
                    lineQuantity = iP4CompletedMeasureLbsTxb.Text.Replace(",", ""); //Lbs
                }

                string toWh = iP4ToWhseCbb.SelectedItem.ToString();
                string baseType = "202";
                string baseEntry = jobRmDetail.JO_DocEntry.ToString();

                var _interfaceWorkbook_Line = new Workbook();
                _interfaceWorkbook_Line.LoadFromFile(
                    ConfigurationManager.AppSettings["JobReceive_LineCsvTemplate"],
                    ",", 1, 1);
                var worksheetLn = _interfaceWorkbook_Line.Worksheets[0];

                worksheetLn.Range["A3"].Text = parentKey;
                worksheetLn.Range["B3"].Text = lineNum;
                worksheetLn.Range["E3"].Text = lineQuantity;
                worksheetLn.Range["N3"].Text = toWh;
                worksheetLn.Range["AO3"].Text = baseType;
                worksheetLn.Range["AP3"].Text = baseEntry;

                var tempWorkBookLn = new Workbook();
                tempWorkBookLn.Version = ExcelVersion.Version2010;
                tempWorkBookLn.Worksheets.Clear();
                tempWorkBookLn.Worksheets.AddCopy(worksheetLn);

                string linePath = SaveFileCSV(tempWorkBookLn, "Line_GoodsReceive");

                //B1 Batch
                string batchDocNum = "1";
                string distNumber = iP4LotNoTxb.Text;
                string batchQuantity = lineQuantity;
                string baseLineNumber = "0";

                var _interfaceWorkbook_Batch = new Workbook();
                _interfaceWorkbook_Batch.LoadFromFile(
                    ConfigurationManager.AppSettings["JobReceive_BatchCsvTemplate"],
                    ",", 1, 1);
                var worksheetBt = _interfaceWorkbook_Batch.Worksheets[0];

                worksheetBt.Range["A3"].Text = batchDocNum;
                worksheetBt.Range["C3"].Text = distNumber;
                worksheetBt.Range["K3"].Text = batchQuantity;
                worksheetBt.Range["L3"].Text = baseLineNumber;

                var tempWorkBookBt = new Workbook();
                tempWorkBookBt.Version = ExcelVersion.Version2010;
                tempWorkBookBt.Worksheets.Clear();
                tempWorkBookBt.Worksheets.AddCopy(worksheetBt);

                string batchPath = SaveFileCSV(tempWorkBookBt, "Batch_GoodsReceive");

                //Copy XML to new one and also change template paths in XML
                bool isTransfer = false;
                string templatePath = ConfigurationManager.AppSettings["JobReceive_XmlTemplate"];
                string xmlFileName = "JobReceiveXml_" + DateTime.Now.ToString("dd-MM-yy-hhmmss") + ".xml";
                string xmlOutputPath = ConfigurationManager.AppSettings["InterfaceOutputPath"] + xmlFileName;
                string localXmlPath = ConfigurationManager.AppSettings["LocalTemplateOutputPath"] + xmlFileName;
                CreateXMLInterface(isTransfer, templatePath, headerPath, linePath, batchPath, xmlOutputPath);

                //Call .bat
                string batName = "JobReceiveBat_" + DateTime.Now.ToString("dd-MM-yy-hhmmss");
                CreateBatFile(batName, localXmlPath);

                //Detect error log from DTW
                bool result = GetInterfaceResult(batName);

                return result;

            }
            catch (Exception ex)
            {
                log.Error("Method: Interface_GoodsReceiveExcelTemplate() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
                return false;
            }
            finally
            {
                log.Info("End Method: Interface_GoodsReceiveExcelTemplate()");
            }
        }

        private void iP1JobNoTxb_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if (e.KeyChar == (char)Keys.Return) iP1SubmitBtn.PerformClick();
        }

        private void IP1JobNoTxb_KeyUp(object sender, KeyEventArgs e)
        {
            log.Info("Start Method: IP1JobNoTxb_KeyUp() No parameter");

            iP1SubmitBtn.Enabled = false;

            if (iP1JobNoTxb.Text.Trim().Length > 8)
            {
                //get item list og this GR to dropdowm box
                var jobRmList = jobList.Where(i => i.JO_DocNum.ToString() == iP1JobNoTxb.Text.Trim()).FirstOrDefault();
                if (jobRmList != null)
                {
                    jobRmDetail = jobRmList;
                    iP1SubmitBtn.Enabled = true;
                }
                else //retry
                {
                    Cursor.Current = Cursors.WaitCursor;
                    jobList = GetJobOrderDetail("", "");
                    jobRmList = jobList.Where(i => i.JO_DocNum.ToString() == iP1JobNoTxb.Text.Trim()).FirstOrDefault();
                    if (jobRmList != null)
                    {
                        jobRmDetail = jobRmList;
                        iP1SubmitBtn.Enabled = true;
                    }
                    Cursor.Current = Cursors.Default;
                }
            }

            log.Info("End Method: IP1JobNoTxb_KeyUp()");
        }

        private void IP1Pnl_MouseHover(object sender, EventArgs e)
        {
            //iP1Pnl.Focus();
        }

        private void IP1SubmitBtn_Click(object sender, EventArgs e)
        {
            log.Info("Start Method: IP1SubmitBtn_Click() No parameter");
            Cursor.Current = Cursors.WaitCursor;
            _ip1BarcodeBuffer = "";
            HandleIP1SubmitAction();
            Cursor.Current = Cursors.Default;
        }

        private void IP2JobReceiveBtn_Click(object sender, EventArgs e)
        {
            log.Info("Start Method: iP2JobReceiveBtn_Click() No parameter");

            try
            {
                iP4StartOptionsPanel.Visible = true;
                iP4StartOptionsPanel.BringToFront();
                var jobReceiveList = _entities.JobReceive
                    .Where(jr => jr.IssueProductionID == _issueProductionInstance.ID)
                    .ToList();
                iP4StartOptionsCbb.Items.Clear();

                iP4StartOptionsCbb.Items.Add(new ComboBoxItem()
                {
                    Text = "      -----      กรุณาเลือก      -----      ",
                    Value = null
                });
                iP4StartOptionsCbb.Items.Add(new ComboBoxItem()
                {
                    Text = " สร้างบันทึกรับผลิตใหม่ ",
                    Value = 0
                });

                foreach (var job in jobReceiveList)
                {
                    iP4StartOptionsCbb.Items.Add(new ComboBoxItem()
                    {
                        Text = job.ID + " - " + job.CreationDate,
                        Value = job.ID
                    });
                }

                IPShowPanel(ref iP4Pnl);
            }
            catch (Exception ex)
            {
                log.Error("Method: iP2JobReceiveBtn_Click() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                WriteLog(_issueProductionInstance?.ID, "JR", "GoTo"
                    , $"ไปหน้า Job receive ของ Job order no:{_issueProductionInstance?.DocNo} รหัสสินค้า:{_issueProductionInstance?.ItemCategory?.ItemCode}");
                log.Info("End Method: iP2JobReceiveBtn_Click()");
            }
        }

        private void IP2PrintFormBtn_Click(object sender, EventArgs e)
        {
            log.Info("Start Method: IP2PrintFormBtn_Click() No parameter");

            try
            {
                var finishItemCategory =
                    _entities.ItemCategory.FirstOrDefault(ic => ic.ID == _issueProductionInstance.FinishItemID);
                var worksheet = _formWorkbook.Worksheets[1];

                var tempWorkBook = new Workbook();
                tempWorkBook.Version = ExcelVersion.Version2010;
                tempWorkBook.Worksheets.Clear();
                var currentSheet = tempWorkBook.Worksheets.AddCopy(worksheet);

                decimal qtyCompleteCount = 0.00M;
                decimal qtyRejectCount = 0.00M;
                var standardWeightForThisItem = GetStandardItemWeightPerUnitByItemCode(finishItemCategory?.ItemCode);
                if (standardWeightForThisItem > 0)
                {
                    if (!IsItemFGW(finishItemCategory?.ItemCode))
                    {
                        standardWeightForThisItem = ConvertLbsToKgs(standardWeightForThisItem);
                    }

                    qtyCompleteCount = decimal.Divide(_issueProductionInstance.QtyCompleted ?? 0,
                        standardWeightForThisItem);
                    qtyRejectCount = decimal.Divide(_issueProductionInstance.QtyRejected ?? 0,
                        standardWeightForThisItem);
                }

                var docNoBarcode = currentSheet.Pictures.Add(1, 9, GetBarcodeImage(_issueProductionInstance.DocNo));
                currentSheet.Range["J3"].Value = _issueProductionInstance.AssignDate.HasValue
                    ? _issueProductionInstance.AssignDate.Value.ToString("d MMMM yyyy",
                        CultureInfo.CreateSpecificCulture("th-TH"))
                    : "ไม่มีข้อมูล";
                currentSheet.Range["J4"].Value = _issueProductionInstance?.RefNo ?? "";
                currentSheet.Range["C7"].Value =
                    _issueProductionInstance?.CustomerName ?? "";
                currentSheet.Range["C8"].Value = finishItemCategory?.ItemCode ?? "";
                var finishItemBarcode = currentSheet.Pictures.Add(8, 4, GetBarcodeImage(finishItemCategory.ItemCode));
                finishItemBarcode.LeftColumnOffset = 500;
                currentSheet.Range["C9"].Value = finishItemCategory?.Description ?? "";
                currentSheet.Range["C10"].Value = _issueProductionInstance.Whse ?? "";
                currentSheet.Range["C11"].Value = jobRmDetail.Barcode_ColorTag ?? "";
                currentSheet.Range["I7"].Value = _issueProductionInstance.DueDate.HasValue
                    ? _issueProductionInstance.DueDate.Value.ToString("d MMMM yyyy",
                        CultureInfo.CreateSpecificCulture("th-TH"))
                    : "ไม่มีข้อมูล";
                currentSheet.Range["K63"].Value = _issueProductionInstance.PrintCount == null ? "พิมพ์ครั้งที่ 1" : "พิมพ์ครั้งที่ " + (_issueProductionInstance.PrintCount.Value);

                var countUnit = GetEngUOMCountFromItemCode(finishItemCategory?.ItemCode);
                var measureUnit = GetEngUOMMeasureFromItemCode(finishItemCategory?.ItemCode);

                if (IsItemFGW(finishItemCategory?.ItemCode))
                {
                    currentSheet.Range["H9"].Value = "";
                    currentSheet.Range["K8"].Value = $"{measureUnit}";
                    currentSheet.Range["K9"].Value = $"{measureUnit}/{countUnit}";
                    currentSheet.Range["K10"].Value = $"{measureUnit}/{countUnit}";
                    currentSheet.Range["K11"].Value = $"{measureUnit}";
                    currentSheet.Range["G13"].Value = "Length";
                    currentSheet.Range["F22"].Value = "Length";
                }
                else
                {
                    currentSheet.Range["K8"].Value = $"{measureUnit}";
                    currentSheet.Range["K9"].Value = $"Lbs/{measureUnit}/{countUnit}";
                    currentSheet.Range["K10"].Value = $"{measureUnit}/{countUnit}";
                    currentSheet.Range["K11"].Value = $"{measureUnit}";
                }

                currentSheet.Range["I8"].Value = $"{_issueProductionInstance.QtyPlanned:N2}";
                currentSheet.Range["I9"].Value = $"{_issueProductionInstance.QtyCompleted:N2}";
                currentSheet.Range["I10"].Value = $"{_issueProductionInstance.QtyRejected:N2}";
                currentSheet.Range["I11"].Value = $"{_issueProductionInstance.QtyOpen:N2}";
                currentSheet.Range["J9"].Value = Math.Ceiling(qtyCompleteCount).ToString();
                currentSheet.Range["J10"].Value = Math.Ceiling(qtyRejectCount).ToString();

                docNoBarcode.LeftColumnOffset = 750;

                //worksheet.InsertRow(14, 47, InsertOptionsType.FormatAsBefore);

                for (int i = 3; i < _issueProductionRequestList.Count + _operationList.Count; i++)
                {
                    currentSheet.InsertRow(16, 2, InsertOptionsType.FormatAsBefore);
                    currentSheet.Copy(worksheet.Range["A18:K19"], worksheet.Range["A16:K17"], true);
                    currentSheet.Range["B16:B17"].Merge();
                    currentSheet.Range["C16:C17"].Merge();
                }

                var rowOffset = 14;
                var itemCount = 1;
                var b1JobList = jobList.Where(i => i.JO_DocNum.ToString() == _issueProductionInstance.RefNo).ToList();

                foreach (var requestItem in _issueProductionRequestList)
                {
                    var jobLine = b1JobList.FirstOrDefault(jo => jo.RM_ItemCode == requestItem.ItemCategory.ItemCode);
                    currentSheet.Range["A" + rowOffset].Value = "" + itemCount++;
                    currentSheet.Pictures.Add(rowOffset, 2, GetBarcodeImage120W(requestItem.ItemCategory.Barcode));
                    currentSheet.Range["C" + rowOffset].Value = requestItem.ItemCategory.Description;
                    currentSheet.Range["D" + rowOffset].Value = requestItem.SuggestedLot;
                    currentSheet.Range["E" + rowOffset].Value = jobLine.RM_LotSuggest_Attribute ?? "";
                    currentSheet.Range["F" + rowOffset].Value = $"{requestItem.Count ?? 0:N2}";
                    currentSheet.Range["G" + rowOffset].Value = $"{requestItem.Measure ?? 0:N2}";
                    currentSheet.Range["H" + rowOffset].Value = requestItem.Whse;

                    rowOffset += 2;
                }

                foreach (var operation in _operationList)
                {
                    currentSheet.Range["A" + rowOffset].Value = "" + itemCount++;
                    currentSheet.Range["B" + rowOffset].Value = operation.RM_ItemCode;
                    currentSheet.Range["F" + rowOffset].Value = "0.00";
                    currentSheet.Range["G" + rowOffset].Value = $"{operation.RM_PlannedQty ?? 0:N2}";
                    currentSheet.Range["H" + rowOffset].Value = operation.RM_wareHouse;

                    rowOffset += 2;
                }

                currentSheet.Columns[4].AutoFitColumns();
                currentSheet.Columns[5].AutoFitColumns();
                currentSheet.Columns[6].AutoFitColumns();
                currentSheet.Columns[7].AutoFitColumns();
                currentSheet.Columns[8].AutoFitColumns();

                if (_printEnable)
                {
                    PrintExcelByInterop(tempWorkBook, _formPrinterName);
                }
                else
                {
                    SaveFileExcel(tempWorkBook, "Issue Production Form");
                }

                if (_issueProductionInstance.PrintCount == null)
                {
                    _issueProductionInstance.PrintCount = 2;
                }
                else
                {
                    _issueProductionInstance.PrintCount++;
                }
            }
            catch (Exception ex)
            {
                log.Error("Method: IP2PrintFormBtn_Click() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                WriteLog(_issueProductionInstance?.ID, "JO", "Print form"
                    , $"พิมพ์แบบฟอร์มของ Job order no:{_issueProductionInstance?.DocNo} รหัสสินค้า:{_issueProductionInstance?.ItemCategory.ItemCode}");
                log.Info("End Method: IP2PrintFormBtn_Click()");
            }
        }

        private void IP2ScanBarcodeIssueBtn_Click(object sender, EventArgs e)
        {
            try
            {
                WriteLog(_issueProductionInstance?.ID, "JI", "GoTo"
                    , $"ไปหน้า Job issue ของ Job order no:{_issueProductionInstance?.DocNo}");

                var jobIssueList = _entities.IssueProductionItem.Where(ii => ii.IssueProductionID == _issueProductionInstance.ID).GroupBy(ii => ii.JobIssueID).ToList();

                iP5issueListCbb.Items.Clear();

                iP5issueListCbb.Items.Add(new ComboBoxItem()
                {
                    Text = "      -----      กรุณาเลือก      -----      ",
                    Value = null
                });
                iP5issueListCbb.Items.Add(new ComboBoxItem()
                {
                    Text = " สร้างบันทึกเบิกวัตถุดิบใหม่ ",
                    Value = 0
                });

                foreach (var job in jobIssueList)
                {
                    iP5issueListCbb.Items.Add(new ComboBoxItem()
                    {
                        Text = job.Key.Value + " - " + job.FirstOrDefault().CreationDate,
                        Value = job.Key.Value
                    });
                }

                IPShowPanel(ref iP5Pnl);
            }
            catch (Exception ex)
            {
                log.Error("Method: IP2ScanBarcodeIssueBtn_Click() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }

        }

        private void IP3_RecalculateTotalWeight()
        {
            log.Info("Start Method: IP3_RecalculateTotalWeight() No parameter");

            foreach (DataGridViewRow row in iP3ScannedItemDgv.Rows)
            {
                var tableItem = row.DataBoundItem as IssueProductionIssueTable;
                var item = GetItemByID(tableItem.ID);

                if (item != null && item.ItemType.isItemizable != null && (!item.ItemType.isItemizable.Value))
                {
                    var netWeight = (item.Measure ?? 0) -
                                    ((item.OuterPackageWeight ?? 0) +
                                     (item.InnerPackageWeight ?? 0) +
                                     (item.MiscPackageWeight ?? 0));
                    var itemCount = tableItem.Count;
                    tableItem.NetWeight = netWeight * itemCount;
                    iP3ScannedItemDgv.Refresh();
                }

            }
            iP3TotalGrossWeightTxb.Text = _scannedItemList.Sum(si => si.GrossWeight).ToString();
            iP3TotalNetWeightTxb.Text = _scannedItemList.Sum(si => si.NetWeight).ToString();

            log.Info("End Method: IP3_RecalculateTotalWeight()");
        }

        private void IP3CancelBtn_Click(object sender, EventArgs e)
        {
            WriteLog(_issueProductionInstance?.ID, "JI", "Exit", $"ออกจากหน้า Job issue:{_scannedItemList?.FirstOrDefault()?.ID}");

            if (_jobIssueIsSaved)
            {
                IPShowPanel(ref iP2Pnl);
            }
            else if (IP3ConfirmBeforeLeave())
            {
                IPShowPanel(ref iP1Pnl);
                _exitFromButton = true;
            }
        }

        private void IP3Pnl_Enter(object sender, EventArgs e)
        {
            log.Info("Start Method: IP3Pnl_Enter() No parameter");

            try
            {

            }
            catch (Exception ex)
            {
                log.Error("Method: IP3Pnl_Enter() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: IP3Pnl_Enter()");
            }
        }

        private void IP3Pnl_KeyPress(object sender, KeyPressEventArgs e)
        {
            log.Info("Start Method: IP3Pnl_KeyPress() No parameter");

            try
            {
                if (e.KeyChar != (char)Keys.Return)
                {
                    _ip3BarcodeBuffer += e.KeyChar;
                    return;
                }

                if (_jobIssueSentToB1)
                {
                    MessageBox.Show($"รายการเบิกวัตถุดิบนี้ได้ส่งไปที่ B1 แล้ว ไม่สามารถแก้ไขได้");
                    return;
                }

                var scannedItem = _entities.Item.FirstOrDefault(i => i.Barcode == _ip3BarcodeBuffer);

                _ip3BarcodeBuffer = "";

                if (scannedItem == null)
                {
                    MessageBox.Show($"ไม่พบสินค้าที่แสกน");
                    return;
                }

                var isItemizable = scannedItem.ItemType.isItemizable ?? false;
                if (scannedItem.Whse.Contains(InTranWhse) || scannedItem.Whse.Contains(IssuedWhse) ||
                    scannedItem.Whse.Contains(SoldWhse) || scannedItem.Whse.Contains(ReceiveWhse))
                {
                    MessageBox.Show($"สินค้าที่แสกนไม่อยู่ในสถานะพร้อมเบิก");
                    return;
                }

                if (scannedItem != null && _scannedItemList.Any(i => i.LotNo == scannedItem.LotNo))
                {
                    return;
                }

                if (scannedItem != null && _issueProductionRequestList.Select(ipr => ipr.ItemCategory)
                        .Contains(scannedItem.ItemCategory))
                {
                    var mappedItem = new IssueProductionIssueTable()
                    {
                        RowNo = _scannedItemList.Count + 1,
                        ID = scannedItem.ID,
                        ItemCode = scannedItem.ItemCategory.ItemCode,
                        Description = scannedItem.ItemCategory.Description,
                        LotNo = scannedItem.LotNo,
                        GrossWeight = scannedItem.Measure ?? 0,
                        NetWeight = (scannedItem.Measure ?? 0) - (scannedItem.OuterPackageWeight ?? 0),
                        Whse = scannedItem.Whse,
                        Count = isItemizable ? (int)(scannedItem.Count ?? 0) : 1
                    };
                    _scannedItemList.Add(mappedItem);

                    WriteLog(_issueProductionInstance?.ID, "JI", "Scan issue barcode"
                        , $"สแกนสินค้า:{scannedItem.ItemCategory.ItemCode} หมายเลข Lot:{scannedItem.LotNo} โกดัง:{scannedItem.Whse}");
                }
                else
                {
                    MessageBox.Show($"สินค้าที่แสกนไม่อยู่ในรหัสสินค้า");
                    return;
                }

                IP3_RecalculateTotalWeight();
                iP3SendB1Btn.Enabled = false;
                iP3SubmitBtn.Enabled = true;
                _jobIssueIsSaved = false;
                //iP3ScannedItemDgv.AutoResizeColumns();
            }
            catch (Exception ex)
            {
                log.Error("Method: IP3Pnl_KeyPress() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: IP3Pnl_KeyPress()");
            }
        }
        private void IP3Pnl_MouseHover(object sender, EventArgs e)
        {
            //iP3Pnl.Focus();
        }

        private void IP3ScannedItemDgv_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            log.Info("Start Method: IP3ScannedItemDgv_CellClick() No parameter");

            try
            {
                var button = iP3ScannedItemDgv.Columns[e.ColumnIndex] as DataGridViewButtonColumn;
                if (button != null && button.Name == "delete button")
                {
                    var IssueProductionIssueTableItem =
                        iP3ScannedItemDgv.Rows[e.RowIndex].DataBoundItem as IssueProductionIssueTable;
                    if (_scannedItemDeleteList.All(i => i != IssueProductionIssueTableItem))
                    {
                        _scannedItemDeleteList.Add(IssueProductionIssueTableItem);
                    }
                    _scannedItemList.Remove(IssueProductionIssueTableItem);
                    IP3_RecalculateTotalWeight();

                    if (IssueProductionIssueTableItem != null)
                    {
                        WriteLog(_issueProductionInstance?.ID, "JI", "Delete issue item"
                            , $"ลบสินค้า:{IssueProductionIssueTableItem.ItemCode} Row:{IssueProductionIssueTableItem.RowNo} หมายเลข Lot:{IssueProductionIssueTableItem.LotNo} Net weight:{IssueProductionIssueTableItem.NetWeight}");
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Method: IP3ScannedItemDgv_CellClick() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: IP3ScannedItemDgv_CellClick()");
                if (!_jobIssueSentToB1)
                {
                    iP3SendB1Btn.Enabled = false;
                    iP3SubmitBtn.Enabled = true;
                }
            }
        }

        private void IP3ScannedItemDgv_MouseHover(object sender, EventArgs e)
        {
            //iP3Pnl.Focus();
        }

        private void IP3SubmitBtn_Click(object sender, EventArgs e)
        {
            log.Info("Start Method: IP3SubmitBtn_Click() No parameter");
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                long newJobIssueID;
                var newIssueProductionItemList = new List<IssueProductionItem>();
                if (_currentJobIssueID == 0)
                {
                    var lastJobIssueID = _entities.IssueProductionItem.Max(ipi => ipi.JobIssueID);
                    newJobIssueID = lastJobIssueID + 1 ?? 1;
                    _currentJobIssueID = newJobIssueID;
                }
                else
                {
                    newJobIssueID = _currentJobIssueID;
                }

                var groupedByItemIDItemList = _scannedItemList.GroupBy(i => i.ID).ToList();
                var resultIssueList = GetGroupedResultIssueItemList(newJobIssueID);

                if (_scannedItemDeleteList.Count > 0)
                {
                    foreach (var item in _scannedItemDeleteList)
                    {
                        var lastIssueItem = _entities.IssueProductionItem.OrderByDescending(i => i.CreationDate)
                            .FirstOrDefault(i =>
                                i.ItemID == item.ID && i.IssueProductionID == _issueProductionInstance.ID &&
                                i.JobIssueID == _currentJobIssueID);
                        if (lastIssueItem != null && !(lastIssueItem.IsCanceled ?? true))
                        {
                            var deletedItem = GetItemByID(lastIssueItem.ItemID);

                            var issueProductionItem = _entities.IssueProductionItem.Add(new IssueProductionItem()
                            {
                                IssueProductionID = _issueProductionInstance.ID,
                                ItemID = item.ID,
                                CreationDate = iP3DateCbb.Value,
                                JobIssueID = newJobIssueID,
                                IsCanceled = true
                            });

                            if (deletedItem.ItemType.Name.StartsWith("FWIP"))
                            {
                                var existingItem = _entities.Item.FirstOrDefault(i =>
                                    i.LotNo == deletedItem.LotNo &&
                                    i.ItemCategoryID == deletedItem.ItemCategoryID &&
                                    i.Whse == deletedItem.Whse.Replace(IssuedWhse, ""));

                                if (existingItem == null)
                                {
                                    deletedItem.Whse = deletedItem.Whse.Replace(IssuedWhse, "");
                                }
                                else
                                {
                                    existingItem.Count += deletedItem.Count;
                                    deletedItem.Count = 0;
                                    //var issueProductionItemList = _entities.IssueProductionItem.Where(ii => ii)
                                    //issueProductionItem.ItemID = existingItem.ID;
                                    //_entities.Item.Remove(deletedItem);
                                }
                            }
                            else
                            {
                                deletedItem.Whse = deletedItem.Whse.Replace(IssuedWhse, "");
                            }


                            _entities.SaveChanges();
                        }
                    }
                }

                foreach (var groupItems in groupedByItemIDItemList)
                {
                    var itemTable = groupItems.FirstOrDefault();
                    var itemID = itemTable.ID;
                    var item = GetItemByID(itemID);
                    var amountToTransfer = groupItems.Count();

                    if (item.ItemType.Name.StartsWith("FWIP"))
                    {
                        var fwip = groupItems.FirstOrDefault();

                        amountToTransfer = (int)Math.Ceiling(fwip.NetWeight / item.Measure.Value);

                        if (amountToTransfer < item.Count)
                        {
                            var issuedItem = new Item()
                            {
                                Count = amountToTransfer,
                                LotNo = item.LotNo,
                                Measure = item.Measure,
                                ItemType = item.ItemType,
                                MiscPackageWeight = item.MiscPackageWeight,
                                Attributes = item.Attributes,
                                Barcode = item.Barcode,
                                BinLoc = item.BinLoc,
                                InnerPackageCount = item.InnerPackageCount,
                                InnerPackageWeight = item.InnerPackageWeight,
                                ItemCategory = item.ItemCategory,
                                ItemCategoryID = item.ItemCategoryID,
                                ItemTypeID = item.ItemTypeID,
                                MiscPackageCount = item.MiscPackageCount,
                                OuterPackageCount = item.OuterPackageCount,
                                OuterPackageWeight = item.OuterPackageWeight,
                                PackageType = item.PackageType,
                                ReceiveDate = item.ReceiveDate,
                                VendorRef = item.VendorRef,
                                Whse = item.Whse.Contains(IssuedWhse) ? item.Whse : item.Whse + IssuedWhse
                            };

                            _entities.Item.Add(issuedItem);
                            _scannedItemList.Remove(itemTable);
                            _scannedItemList.Add(new IssueProductionIssueTable()
                            {
                                RowNo = itemTable.RowNo,
                                ID = issuedItem.ID,
                                ItemCode = issuedItem.ItemCategory.ItemCode,
                                Description = issuedItem.ItemCategory.Description,
                                LotNo = issuedItem.LotNo,
                                GrossWeight = issuedItem.Measure ?? 0,
                                NetWeight = itemTable.NetWeight,
                                Whse = issuedItem.Whse,
                                Count = (int)(issuedItem.Count ?? 0)
                            });
                            item.Count -= amountToTransfer;
                            item = issuedItem;
                        }
                        else if (amountToTransfer == item.Count)
                        {
                            //item.Whse = _issueProductionInstance.Whse + IssuedWhse;
                            item.Whse = item.Whse.Contains(IssuedWhse) ? item.Whse : item.Whse + IssuedWhse;
                        }
                        else
                        {
                            MessageBox.Show("จำนวนที่เบิกเกินจำนวนที่มีในระบบ");
                            return;
                        }

                        _entities.SaveChanges();
                    }
                    else
                    {
                        //item.Whse = _issueProductionInstance.Whse + IssuedWhse;
                        item.Whse = item.Whse.Contains(IssuedWhse) ? item.Whse : item.Whse + IssuedWhse;
                    }

                    var lastIssueItem = _entities.IssueProductionItem.OrderByDescending(i => i.CreationDate)
                        .FirstOrDefault(i =>
                            i.ItemID == item.ID && i.IssueProductionID == _issueProductionInstance.ID &&
                            i.JobIssueID == _currentJobIssueID);

                    if (lastIssueItem == null || lastIssueItem.IsCanceled == true)
                    {
                        newIssueProductionItemList.Add(new IssueProductionItem()
                        {
                            IssueProductionID = _issueProductionInstance.ID,
                            ItemID = item.ID,
                            CreationDate = iP3DateCbb.Value,
                            JobIssueID = newJobIssueID,
                            IsCanceled = false
                        });
                    }

                    _entities.SaveChanges();
                }

                var addedIssueProductionItemList = _entities.IssueProductionItem.AddRange(newIssueProductionItemList).ToList();

                _entities.SaveChanges();
                iP3SubmitBtn.Enabled = false;
                iP3SendB1Btn.Enabled = true;
                _jobIssueIsSaved = true;
                MessageBox.Show("บันทึกเรียบร้อย");
                //IPShowPanel(ref iP2Pnl);
            }
            catch (Exception ex)
            {
                log.Error("Method: IP3SubmitBtn_Click() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: IP3SubmitBtn_Click()");
                Cursor.Current = Cursors.Default;
            }
        }
        private void IP4AmountPerPackageTxb_KeyPress(object sender, KeyPressEventArgs e)
        {
            EnforceNumericKeyPress(e);
        }

        private void IP4AmountPerPackageTxb_TextChanged(object sender, EventArgs e)
        {
            if (int.TryParse(iP4AmountPerPackageTxb.Text, out var amountPerPack) &&
                int.TryParse(iP4CompletedCountQtyTxb.Text, out var itemCount))
            {
                iP4InnerPackagingCountTxb.Text = "" + (amountPerPack * itemCount);
            }
        }

        private void IP4CompletedCountQtyTxb_KeyPress(object sender, KeyPressEventArgs e)
        {
            EnforceDecimalKeyPress(e);
        }

        private void iP4CompletedCountQtyTxb_KeyUp(object sender, KeyEventArgs e)
        {
            if (decimal.TryParse(iP4CompletedCountQtyTxb.Text, out var inputCount))
            {
                if (IsItemFGW(_issueProductionInstance.ItemCategory.ItemCode))
                {
                    var standardLength = GetStandardItemWeightPerUnitByItemCode(_issueProductionInstance.ItemCategory.ItemCode);
                    iP4CompletedMeasureTxb.Text = $"{standardLength * inputCount:N2}";
                }
                else
                {
                    var standardWeightInLbs = GetStandardItemWeightPerUnitByItemCode(_issueProductionInstance.ItemCategory.ItemCode);

                    iP4CompletedMeasureTxb.Text = $"{ConvertLbsToKgs(standardWeightInLbs * inputCount):N2}";
                    iP4CompletedMeasureLbsTxb.Text = $"{standardWeightInLbs * inputCount:N2}";
                }
            }
        }

        private void iP4CompletedCountQtyTxb_TextChanged(object sender, EventArgs e)
        {
            if (_jobReceiveItemType.isItemizable != null && _jobReceiveItemType.isItemizable.Value && !IsItemFGW(_issueProductionInstance.ItemCategory.ItemCode))
            {
                iP4OuterPackagingCountTxb.Text = iP4CompletedCountQtyTxb.Text;
                if (int.TryParse(iP4CompletedCountQtyTxb.Text, out var totalCount) &&
                    int.TryParse(iP4AmountPerPackageTxb.Text, out var amountPerPack))
                {
                    iP4InnerPackagingCountTxb.Text = "" + (totalCount * amountPerPack);
                }
            }
        }

        private void IP4CompletedMeasureLbsTxb_KeyPress(object sender, KeyPressEventArgs e)
        {
            EnforceDecimalKeyPress(e);
        }

        private void iP4CompletedMeasureLbsTxb_KeyUp(object sender, KeyEventArgs e)
        {
            if (!decimal.TryParse(iP4CompletedMeasureLbsTxb.Text, out var inputInLbs)) return;
            if (IsItemFGS(_issueProductionInstance.ItemCategory.ItemCode))
            {
                var standardWeightInLbs = GetStandardItemWeightPerUnitByItemCode(_issueProductionInstance.ItemCategory.ItemCode);

                iP4CompletedCountQtyTxb.Text = $"{Math.Ceiling(inputInLbs / standardWeightInLbs):N0}";
                iP4CompletedMeasureTxb.Text = $"{ConvertLbsToKgs(inputInLbs):N2}";
            }
            else
            {
                iP4CompletedMeasureTxb.Text = $"{ConvertLbsToKgs(inputInLbs):N2}";
            }
        }

        private void IP4CompletedMeasureTxb_KeyPress(object sender, KeyPressEventArgs e)
        {
            EnforceDecimalKeyPress(e);
        }

        private void iP4CompletedMeasureTxb_KeyUp(object sender, KeyEventArgs e)
        {
            if (!decimal.TryParse(iP4CompletedMeasureTxb.Text, out var inputInKgs)) return;
            if (IsItemFGW(_issueProductionInstance.ItemCategory.ItemCode))
            {
                var standardLength = GetStandardItemWeightPerUnitByItemCode(_issueProductionInstance.ItemCategory.ItemCode);
                iP4CompletedCountQtyTxb.Text = $"{Math.Ceiling(inputInKgs / standardLength):N0}";
            }
            else if (IsItemFGS(_issueProductionInstance.ItemCategory.ItemCode))
            {
                var standardWeightInLbs = GetStandardItemWeightPerUnitByItemCode(_issueProductionInstance.ItemCategory.ItemCode);
                var standardWeightInKgs = ConvertLbsToKgs(standardWeightInLbs);

                iP4CompletedCountQtyTxb.Text = $"{Math.Ceiling(inputInKgs / standardWeightInKgs):N0}";
                iP4CompletedMeasureLbsTxb.Text = $"{ConvertKgsToLbs(inputInKgs):N2}";
            }
            else
            {
                iP4CompletedMeasureLbsTxb.Text = $"{ConvertKgsToLbs(inputInKgs):N2}";
            }
        }

        private bool IP3ConfirmBeforeLeave()
        {
            log.Info("Start Method: IP3ConfirmBeforeLeave()");
            try
            {
                var allowClose = true;
                if (!_jobIssueIsSaved && _scannedItemList != null && _scannedItemList.Count > 0)
                {
                    var confirmResult = MessageBox.Show("คุณยังไม่ได้บันทึกการทำงานในหน้านี้ คุณแน่ใจว่าต้องการออกจากหน้านี้?",
                        "คำเตือน!",
                        MessageBoxButtons.YesNo);
                    if (confirmResult == DialogResult.Yes)
                    {
                        _scannedItemList.Clear();
                        RevertAllEntitiesChanges();
                    }
                    else
                    {
                        allowClose = false;
                    }
                }

                return allowClose;
            }
            catch (Exception ex)
            {
                log.Error("Method: IP3ConfirmBeforeLeave() Error: " + ex.Message); MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: IP3ConfirmBeforeLeave()");
            }

            return false;
        }

        private bool IP4ConfirmBeforeLeave()
        {
            log.Info("Start Method: IP4ConfirmBeforeLeave()");
            try
            {
                var allowClose = true;
                _leavingJobReceive = false;

                if (_jobReceiveInstance != null && (_jobReceiveInstance.IsReceived == null || !_jobReceiveInstance.IsReceived.Value))
                {
                    var confirmResult = MessageBox.Show("คุณยังไม่ได้บันทึกการทำงานในหน้านี้ คุณแน่ใจว่าต้องการออกจากหน้านี้?",
                        "คำเตือน!",
                        MessageBoxButtons.YesNo);
                    if (confirmResult == DialogResult.Yes)
                    {
                        Ip4RevertChanges();
                        allowClose = true;
                        //RevertAllEntitiesChanges();
                    }
                    else
                    {
                        allowClose = false;
                        iP4Pnl.Focus();
                    }
                }

                return allowClose;
            }
            catch (Exception ex)
            {
                log.Error("Method: IP4ConfirmBeforeLeave() Error: " + ex.Message); MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: IP4ConfirmBeforeLeave()");
            }

            return false;
        }

        private void Ip4RevertChanges()
        {
            if (!(_jobReceiveInstance != null && _jobReceiveInstance.ID > 0))
            {
                _leavingJobReceive = false;
                return;
            }

            if (_jobReceiveList != null && _jobReceiveList.Count > 0)
            {
                if (_jobReceiveList.FirstOrDefault()?.ItemID != null && _jobReceiveList.FirstOrDefault()?.ItemID > 0)
                {
                    var itemList = _entities.Item.ToList();
                    var jrItemList = _entities.JobReceiveItem.ToList();
                    var itemsToDelete = new List<Item>();
                    var jobReceiveItemsToDelete = new List<JobReceiveItem>();

                    foreach (var item in _jobReceiveList)
                    {
                        itemsToDelete.Add(itemList.FirstOrDefault(i => i.ID == item.ItemID));
                        jobReceiveItemsToDelete.Add(jrItemList.FirstOrDefault(jri =>
                            jri.ItemID == item.ItemID && jri.JobReceiveID == _jobReceiveInstance.ID));
                    }

                    _entities.Item.RemoveRange(itemsToDelete);
                    _entities.JobReceiveItem.RemoveRange(jobReceiveItemsToDelete);
                }
            }

            _entities.JobReceive.Remove(_jobReceiveInstance);
            _entities.SaveChanges();
        }

        private void IP4FactoryCbb_SelectedIndexChanged(object sender, EventArgs e)
        {
            IP4UpdateLotFormatTextbox();
        }

        private void iP4FormulaCbb_SelectedIndexChanged(object sender, EventArgs e)
        {
            IP4UpdateLotFormatTextbox();
        }

        private void IP4GenerateLotBtn_Click(object sender, EventArgs e)
        {
            log.Info("Start Method: IP4GenerateLotBtn_Click() No parameter");

            try
            {
                //if (outPackageWeight == 0 || inPackageWeight == 0 || miscPackageWeight == 0)
                //{
                //    MessageBox.Show("จำนวนไม่ถูกต้อง กรุณาแก้ไข");
                //    return;
                //}

                if (iP4FactoryCbb.SelectedItem == "")
                {
                    MessageBox.Show("กรูณาเลือกโรงงานผลิต");
                    return;
                }

                if (string.IsNullOrEmpty(iP4MachineTxb.Text))
                {
                    MessageBox.Show("กรูณาเลือกเครื่องที่ผลิต");
                    return;
                }

                if (_jobReceiveList != null && _jobReceiveList.Count > 0)
                {
                    if (_jobReceiveList.FirstOrDefault()?.ItemID != null && _jobReceiveList.FirstOrDefault()?.ItemID > 0)
                    {
                        var itemList = _entities.Item.ToList();
                        var jrItemList = _entities.JobReceiveItem.ToList();
                        var itemsToDelete = new List<Item>();
                        var jobReceiveItemsToDelete = new List<JobReceiveItem>();

                        foreach (var item in _jobReceiveList)
                        {
                            itemsToDelete.Add(itemList.FirstOrDefault(i => i.ID == item.ItemID));
                            jobReceiveItemsToDelete.Add(jrItemList.FirstOrDefault(jri =>
                                jri.ItemID == item.ItemID && jri.JobReceiveID == _jobReceiveInstance.ID));
                        }

                        _entities.Item.RemoveRange(itemsToDelete);
                        _entities.JobReceiveItem.RemoveRange(jobReceiveItemsToDelete);
                        _entities.SaveChanges();
                    }
                }

                IP4InitializeJobReceiveList(true, true);

                iP4JobReceiveItemDgv.Columns.Clear();
                iP4JobReceiveItemDgv.DataSource = _jobReceiveList;
                iP4JobReceiveItemDgv.Columns.Insert(iP4JobReceiveItemDgv.Columns.Count, new DataGridViewButtonColumn()
                {
                    Text = "พิมพ์บาร์โค้ด",
                    Name = "print barcode button",
                    HeaderText = "",
                    UseColumnTextForButtonValue = true
                });
                iP4JobReceiveItemDgv.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                //iP4JobReceiveItemDgv.Columns[iP4JobReceiveItemDgv.Columns.Count - 1].Visible = false;
                iP4JobReceiveItemDgv.Columns["NetWeight"].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                iP4JobReceiveItemDgv.Columns["GrossWeight"].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                iP4JobReceiveItemDgv.Columns["AmountPerPackage"].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                iP4JobReceiveItemDgv.Columns["RowNo"].ReadOnly = true;
                iP4JobReceiveItemDgv.Columns["LotNo"].ReadOnly = true;
                iP4JobReceiveItemDgv.Columns["NetWeight"].ReadOnly = true;
                iP4JobReceiveItemDgv.Columns["GrossWeight"].DefaultCellStyle.Format = "N2";
                iP4JobReceiveItemDgv.Columns["NetWeight"].DefaultCellStyle.Format = "N2";
                iP4JobReceiveItemDgv.Columns["RowNo"].Width = 40;
                iP4JobReceiveItemDgv.Columns["print barcode button"].Width = 100;
                iP4PrintBarcodeBtn.Enabled = true;
            }
            catch (Exception ex)
            {
                log.Error("Method: IP4GenerateLotBtn_Click() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                WriteLog(_issueProductionInstance?.ID, "JR", "Create sub lot"
                    , $"JobReceiveID:{_jobReceiveInstance?.ID} รหัสสินค้า:{iP4ItemCodeTxb.Text} Lot no:{iP4LotNoTxb.Text} จำนวนที่ผลิตเสร็จ:{iP4CompletedCountQtyTxb.Text}");

                log.Info("End Method: IP4GenerateLotBtn_Click()");
            }
        }

        private void IP4InitializeJobReceiveList(bool isItemizable, bool isSaving)
        {
            log.Info(string.Format(@"Start Method: IP4InitializeJobReceiveList() Parameter:isItemizable Value:'{0}'",
                isItemizable.ToString()));

            try
            {
                var completedQty = Convert.ToInt32(Convert.ToDouble(iP4CompletedCountQtyTxb.Text));
                var completedMeasure = Convert.ToDecimal(iP4CompletedMeasureTxb.Text);

                var outPackageWeight = Convert.ToDecimal(iP4OuterPackagingWeightTxb.Text);
                var inPackageWeight = Convert.ToDecimal(iP4InnerPackagingWeightTxb.Text);
                var miscPackageWeight = Convert.ToDecimal(iP4MiscPackagingWeightTxb.Text);
                var outPackageCount = Convert.ToInt32(Convert.ToDouble(iP4OuterPackagingCountTxb.Text));
                var inPackageCount = Convert.ToInt32(Convert.ToDouble(iP4InnerPackagingCountTxb.Text));
                var miscPackageCount = Convert.ToInt32(Convert.ToDouble(iP4MiscPackagingCountTxb.Text));

                //var outPkgWeightPerPiece = outPackageCount != 0 ? outPackageWeight / outPackageCount : 0;
                var inPkgWeightPerPiece = inPackageCount != 0 ? inPackageWeight / inPackageCount : 0;
                //var miscPkgWeightPerPiece = miscPackageCount != 0 ? miscPackageWeight / miscPackageCount : 0;

                var outPkgCountPerItem = completedQty != 0 ? outPackageCount / completedQty : 0;
                var inPkgCountPerItem = completedQty != 0 ? inPackageCount / completedQty : 0;
                var miscPkgCountPerItem = completedQty != 0 ? miscPackageCount / completedQty : 0;

                var outPkgWeightPerItem = completedQty != 0 ? outPackageWeight / completedQty : 0;
                var inPkgWeightPerItem = completedQty != 0 ? inPackageWeight / completedQty : 0;
                var miscPkgWeightPerItem = completedQty != 0 ? miscPackageWeight / completedQty : 0;

                //var totalPkgWeightPerPiece = outPkgWeightPerPiece + inPkgWeightPerPiece + miscPkgWeightPerPiece;
                var totalPkgWeightPerItem = outPkgWeightPerItem + inPkgWeightPerItem + miscPkgWeightPerItem;

                var weightPerItem = completedQty != 0 ? completedMeasure / completedQty : 0;

                if (_jobReceiveInstance.ID > 0)
                {
                    _jobReceiveInstance = _entities.JobReceive.FirstOrDefault(jr => jr.ID == _jobReceiveInstance.ID);

                    if (_jobReceiveInstance != null && isSaving)
                    {
                        _jobReceiveInstance.BinLoc = iP4BinLocationCbb.SelectedItem.ToString();
                        _jobReceiveInstance.CreationDate = DateTime.Now;
                        _jobReceiveInstance.FactoryNo = iP4FactoryCbb.SelectedItem.ToString();
                        _jobReceiveInstance.FinishItemID = _issueProductionInstance.FinishItemID;
                        _jobReceiveInstance.InnerPackageCount = Convert.ToInt32(iP4InnerPackagingCountTxb.Text.Replace(",", ""));
                        _jobReceiveInstance.InnerPackageWeight = Convert.ToDecimal(iP4InnerPackagingWeightTxb.Text.Replace(",", ""));
                        _jobReceiveInstance.IssueProductionID = _issueProductionInstance.ID;
                        _jobReceiveInstance.OuterPackageWeight = Convert.ToDecimal(iP4OuterPackagingWeightTxb.Text.Replace(",", ""));
                        _jobReceiveInstance.OuterPackageCount = Convert.ToInt32(iP4OuterPackagingCountTxb.Text.Replace(",", ""));
                        _jobReceiveInstance.MiscPackageWeight = Convert.ToDecimal(iP4MiscPackagingWeightTxb.Text.Replace(",", ""));
                        _jobReceiveInstance.MiscPackageCount = Convert.ToInt32(iP4MiscPackagingCountTxb.Text.Replace(",", ""));
                        _jobReceiveInstance.QtyCompletedCount = Convert.ToDecimal(iP4CompletedCountQtyTxb.Text.Replace(",", ""));
                        _jobReceiveInstance.QtyCompletedMeasure = Convert.ToDecimal(iP4CompletedMeasureTxb.Text.Replace(",", ""));
                        _jobReceiveInstance.Shift = iP4ShiftCbb.SelectedItem.ToString();
                        _jobReceiveInstance.Formula = isItemizable ? $"{Convert.ToInt32(iP4FormulaCbb.SelectedItem):D2}" : "";
                        _jobReceiveInstance.ToWhse = iP4ToWhseCbb.SelectedItem.ToString();
                        _jobReceiveInstance.MachineNo = iP4MachineTxb.Text;
                        _jobReceiveInstance.AssignDate = iP4DateCbb.Value;
                        _jobReceiveInstance.PackageType = GetPackageTypeByName(iP4PackagingCbb.SelectedItem.ToString());
                    }
                }
                else
                {
                    _jobReceiveInstance = _entities.JobReceive.Add(new JobReceive()
                    {
                        BinLoc = iP4BinLocationCbb.SelectedItem.ToString(),
                        CreationDate = DateTime.Now,
                        FactoryNo = iP4FactoryCbb.SelectedItem.ToString(),
                        FinishItemID = _issueProductionInstance.FinishItemID,
                        InnerPackageCount = Convert.ToInt32(iP4InnerPackagingCountTxb.Text),
                        InnerPackageWeight = Convert.ToDecimal(iP4InnerPackagingWeightTxb.Text),
                        IssueProductionID = _issueProductionInstance.ID,
                        OuterPackageWeight = Convert.ToDecimal(iP4OuterPackagingWeightTxb.Text),
                        OuterPackageCount = Convert.ToInt32(iP4OuterPackagingCountTxb.Text),
                        MiscPackageWeight = Convert.ToDecimal(iP4MiscPackagingWeightTxb.Text),
                        MiscPackageCount = Convert.ToInt32(iP4MiscPackagingCountTxb.Text),
                        QtyCompletedCount = Convert.ToDecimal(iP4CompletedCountQtyTxb.Text),
                        QtyCompletedMeasure = Convert.ToDecimal(iP4CompletedMeasureTxb.Text),
                        Shift = iP4ShiftCbb.SelectedItem.ToString(),
                        ToWhse = iP4ToWhseCbb.SelectedItem.ToString(),
                        MachineNo = iP4MachineTxb.Text,
                        PackageType = GetPackageTypeByName(iP4PackagingCbb.SelectedItem.ToString()),
                        AssignDate = iP4DateCbb.Value,
                        Formula = isItemizable ? $"{Convert.ToInt32(iP4FormulaCbb.SelectedItem):D2}" : ""
                    });
                }
                _entities.SaveChanges();

                _jobReceiveList = new BindingList<JobReceiveTable>();

                var jobReceiveItemList = new List<JobReceiveItem>();

                if (_jobReceiveInstance != null)
                {
                    jobReceiveItemList = _entities.JobReceiveItem
                        .Where(jri => jri.JobReceiveID == _jobReceiveInstance.ID)
                        .ToList();
                }

                if (jobReceiveItemList.Count > 0)
                {
                    var itemCount = 1;
                    foreach (var item in jobReceiveItemList)
                    {
                        _jobReceiveList.Add(new JobReceiveTable()
                        {
                            LotNo = item.Item.LotNo,
                            RowNo = item.Item.ItemType.isItemizable != null && item.Item.ItemType.isItemizable.Value
                                ? itemCount++
                                : 0,
                            AmountPerPackage = Convert.ToInt32(item.Item.Count.Value), //todo change this for WFG
                            NetWeight = item.Item.Measure.Value -
                                        (item.Item.OuterPackageWeight.Value + item.Item.InnerPackageWeight.Value +
                                         item.Item.MiscPackageWeight.Value),
                            GrossWeight = item.Item.Measure.Value,
                            Attributes = item.Item.Attributes,
                            ItemID = item.ItemID,
                            IsPrinted = item.IsPrinted ?? false,
                            IsSentToB1 = item.IsSentToB1 ?? false
                        });
                    }
                }
                else
                {
                    var itemCount = 1;

                    if (isItemizable)
                    {
                        int.TryParse(iP4AmountPerPackageTxb.Text, out var amountPerItem);
                        if (amountPerItem <= 0)
                        {
                            amountPerItem = 1;
                        }
                        for (int i = 0; i < completedQty; i++)
                        {
                            _jobReceiveList.Add(new JobReceiveTable()
                            {
                                RowNo = itemCount,
                                LotNo = iP4LotNoTxb.Text + $"-{itemCount:D4}",
                                Attributes = _issueProductionInstance.ItemCategory.DefaultAttributes,
                                AmountPerPackage = amountPerItem,
                                GrossWeight = Math.Round(weightPerItem, 2),
                                NetWeight = Math.Round(weightPerItem - totalPkgWeightPerItem, 2)
                            });
                            itemCount++;
                        }
                    }
                    else
                    {
                        var amountPerItem = 1;
                        int.TryParse(iP4CompletedCountQtyTxb.Text, out amountPerItem);
                        _jobReceiveList.Add(new JobReceiveTable()
                        {
                            RowNo = 0,
                            LotNo = iP4LotNoTxb.Text,
                            Attributes = _issueProductionInstance.ItemCategory.DefaultAttributes,
                            AmountPerPackage = amountPerItem,
                            GrossWeight = Math.Round(weightPerItem, 2),
                            NetWeight = Math.Round(weightPerItem - totalPkgWeightPerItem, 2)
                        });
                    }

                    if (_jobReceiveList.FirstOrDefault()?.ItemID != null && _jobReceiveList.FirstOrDefault()?.ItemID > 0)
                    {
                        var itemList = _entities.Item.ToList();
                        var jrItemList = _entities.JobReceiveItem.ToList();
                        var itemsToDelete = new List<Item>();
                        var jobReceiveItemsToDelete = new List<JobReceiveItem>();

                        foreach (var item in _jobReceiveList)
                        {
                            itemsToDelete.Add(itemList.FirstOrDefault(i => i.ID == item.ItemID));
                            jobReceiveItemsToDelete.Add(jrItemList.FirstOrDefault(jri =>
                                jri.ItemID == item.ItemID && jri.JobReceiveID == _jobReceiveInstance.ID));
                        }

                        _entities.Item.RemoveRange(itemsToDelete);
                        _entities.JobReceiveItem.RemoveRange(jobReceiveItemsToDelete);
                        _entities.SaveChanges();
                    }

                    var lastItemInLot = _entities.Item.Where(i => i.ItemCategoryID == _jobReceiveInstance.FinishItemID)
                        .OrderByDescending(i => i.ID).FirstOrDefault(i => i.LotNo.StartsWith(iP4LotNoTxb.Text.Trim()));
                    var currentRunningNumber = 0;

                    if (isItemizable && lastItemInLot != null)
                    {
                        var lotNo = lastItemInLot.LotNo.Substring(lastItemInLot.LotNo.Length - 4, 4);

                        currentRunningNumber = Convert.ToInt32(lotNo);
                    }


                    var newItemList = _jobReceiveList.Select(item => new Item()
                    {
                        Measure = item.GrossWeight,
                        Count = item.AmountPerPackage, //todo more value
                        LotNo = iP4LotNoTxb.Text + (isItemizable ? $"-{currentRunningNumber + item.RowNo:D4}" : ""),
                        Attributes = _issueProductionInstance.ItemCategory.DefaultAttributes,
                        Barcode = GetItemCategoryByItemCategoryID(_issueProductionInstance.ItemCategory.ID).ItemCode + "_" + iP4LotNoTxb.Text + (isItemizable ? $"-{currentRunningNumber + item.RowNo:D4}" : "") + (isItemizable ? $"_Gross={item.GrossWeight:N2}_Net={item.NetWeight:N2}_{DateTime.Today.ToShortDateString()}" : $"_NetPerItem={item.NetWeight:N2}_NetPerLot{item.NetWeight * item.AmountPerPackage:N2}_{DateTime.Today.ToShortDateString()}"),
                        BinLoc = iP4BinLocationCbb.SelectedItem.ToString(),
                        ItemCategoryID = _issueProductionInstance.ItemCategory.ID,
                        ItemTypeID = _jobReceiveItemType.ID,
                        InnerPackageCount = inPkgCountPerItem,
                        InnerPackageWeight = inPkgWeightPerPiece * inPkgCountPerItem,
                        OuterPackageWeight = outPkgWeightPerItem,
                        OuterPackageCount = outPkgCountPerItem,
                        MiscPackageWeight = miscPkgWeightPerItem,
                        MiscPackageCount = miscPkgCountPerItem,
                        ReceiveDate = DateTime.Today,
                        Whse = iP4ToWhseCbb.SelectedItem.ToString() + ReceiveWhse,
                        PackageType = GetPackageTypeByName(iP4PackagingCbb.SelectedItem.ToString())
                    })
                        .ToList();

                    var addedItemList = _entities.Item.AddRange(newItemList).ToList();
                    _entities.SaveChanges();

                    var tmpRow = 1;

                    _jobReceiveList = new BindingList<JobReceiveTable>();

                    foreach (var item in newItemList)
                    {
                        _jobReceiveList.Add(new JobReceiveTable()
                        {
                            LotNo = item.LotNo,
                            RowNo = tmpRow++,
                            GrossWeight = item.Measure ?? 0,
                            ItemID = item.ID,
                            NetWeight = (item.Measure ?? 0) - ((item.InnerPackageWeight ?? 0)
                                                             + (item.OuterPackageWeight ?? 0)
                                                             + (item.MiscPackageWeight ?? 0)),
                            Attributes = item.Attributes,
                            AmountPerPackage = Convert.ToInt32(Math.Ceiling(item.Count ?? 0))
                        });
                    }

                    var newJobReceiveList = newItemList.Select(il => new JobReceiveItem()
                    {
                        ItemID = il.ID,
                        JobReceiveID = _jobReceiveInstance.ID
                    });
                    _entities.JobReceiveItem.AddRange(newJobReceiveList);
                    _entities.SaveChanges();
                }

                IP4RecalculateWeight();
            }
            catch (Exception ex)
            {
                log.Error("Method: IP4InitializeJobReceiveList() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: IP4InitializeJobReceiveList()");
            }
        }

        private void IP4InnerPackageWeightPerPieceTxb_KeyPress(object sender, KeyPressEventArgs e)
        {
            EnforceDecimalKeyPress(e);
        }

        private void IP4InnerPackageWeightPerPieceTxb_TextChanged(object sender, EventArgs e)
        {
            if (decimal.TryParse(iP4InnerPackageWeightPerPieceTxb.Text, out var innerPerPiece) &&
                int.TryParse(iP4InnerPackagingCountTxb.Text, out var itemCount))
            {
                iP4InnerPackagingWeightTxb.Text = $"{(innerPerPiece * itemCount):N2}";
            }
        }

        private void IP4InnerPackagingCountTxb_KeyPress(object sender, KeyPressEventArgs e)
        {
            EnforceDecimalKeyPress(e);
        }

        private void IP4InnerPackagingCountTxb_TextChanged(object sender, EventArgs e)
        {
            if (decimal.TryParse(iP4InnerPackageWeightPerPieceTxb.Text, out var innerPerPiece) &&
                int.TryParse(iP4InnerPackagingCountTxb.Text, out var itemCount))
            {
                iP4InnerPackagingWeightTxb.Text = $"{(innerPerPiece * itemCount):N2}";
            }
        }

        private void IP4InnerPackagingWeightTxb_KeyPress(object sender, KeyPressEventArgs e)
        {
            EnforceDecimalKeyPress(e);
        }

        private void IP4JobReceiveItemDgv_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            log.Info("Start Method: IP4JobReceiveItemDgv_CellClick() No parameter");
            try
            {
                var button = iP4JobReceiveItemDgv.Columns[e.ColumnIndex] as DataGridViewButtonColumn;
                if (button != null && button.Name == "print barcode button")
                {
                    var jobReceiveTableItem = iP4JobReceiveItemDgv.Rows[e.RowIndex].DataBoundItem as JobReceiveTable;

                    if (_jobReceiveInstance.ID > 0)
                    {
                        IPHandlePrintBarcode(jobReceiveTableItem.RowNo, jobReceiveTableItem.RowNo);
                        jobReceiveTableItem.IsPrinted = true;
                        var itemID = _jobReceiveList[jobReceiveTableItem.RowNo - 1].ItemID;
                        var JobReceiveID = _jobReceiveInstance.ID;

                        var jobReceiveItem = _entities.JobReceiveItem.FirstOrDefault(jri => jri.JobReceiveID == JobReceiveID && jri.ItemID == itemID);
                        if (jobReceiveItem != null)
                        {
                            jobReceiveItem.IsPrinted = true;
                            _entities.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Method: IP4JobReceiveItemDgv_CellClick() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: IP4JobReceiveItemDgv_CellClick()");
            }
        }

        private void IP4JobReceiveItemDgv_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            log.Info("Start Method: IP4JobReceiveItemDgv_CellClick() No parameter");
            try
            {
                var completedQty = Convert.ToInt32(Convert.ToDouble(iP4CompletedCountQtyTxb.Text));

                var outPackageWeight = Convert.ToDecimal(iP4OuterPackagingWeightTxb.Text);
                var inPackageWeight = Convert.ToDecimal(iP4InnerPackagingWeightTxb.Text);
                var miscPackageWeight = Convert.ToDecimal(iP4MiscPackagingWeightTxb.Text);
                var outPackageCount = Convert.ToInt32(Convert.ToDouble(iP4OuterPackagingCountTxb.Text));
                var inPackageCount = Convert.ToInt32(Convert.ToDouble(iP4InnerPackagingCountTxb.Text));
                var miscPackageCount = Convert.ToInt32(Convert.ToDouble(iP4MiscPackagingCountTxb.Text));

                var inPkgWeightPerPiece = inPackageCount != 0 ? (inPackageWeight / inPackageCount) : 0;

                var outPkgWeightPerItem = completedQty != 0 ? (outPackageWeight / completedQty) : 0;
                var miscPkgWeightPerItem = completedQty != 0 ? (miscPackageWeight / completedQty) : 0;

                var editItem = iP4JobReceiveItemDgv.CurrentRow.DataBoundItem as JobReceiveTable;

                var perItemTareWeight = outPkgWeightPerItem + (inPkgWeightPerPiece * editItem.AmountPerPackage) + miscPkgWeightPerItem;

                editItem.NetWeight = editItem.GrossWeight - perItemTareWeight;

                var jobreceiveitem = _entities.JobReceiveItem.FirstOrDefault(jr => jr.ItemID == editItem.ItemID && jr.JobReceiveID == _jobReceiveInstance.ID);
                jobreceiveitem.Item.Measure = Convert.ToDecimal(editItem.GrossWeight.ToString());
                jobreceiveitem.Item.OuterPackageWeight = outPkgWeightPerItem;
                jobreceiveitem.Item.InnerPackageWeight = inPkgWeightPerPiece * editItem.AmountPerPackage;
                jobreceiveitem.Item.MiscPackageWeight = miscPkgWeightPerItem;
                jobreceiveitem.Item.Count = editItem.AmountPerPackage;
                _entities.SaveChanges();

                IP4RecalculateWeight();

                iP4JobReceiveItemDgv.Refresh();
            }
            catch (Exception ex)
            {
                log.Error("Method: IP4JobReceiveItemDgv_CellClick() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: IP4JobReceiveItemDgv_CellClick()");
            }
        }

        private void IP4JobReceiveItemDgv_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            var jobReceiveTableItem = iP4JobReceiveItemDgv.Rows[e.RowIndex].DataBoundItem as JobReceiveTable;

            if (jobReceiveTableItem == null)
            {
                return;
            }

            if (jobReceiveTableItem.IsPrinted)
            {
                iP4JobReceiveItemDgv.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightBlue;
            }

            if (jobReceiveTableItem.IsSentToB1)
            {
                iP4JobReceiveItemDgv.Rows[e.RowIndex].ReadOnly = true;
            }

            if (_jobReceiveItemType.isItemizable ?? false) //FG
            {
                if (_jobReceiveList != null && _jobReceiveList.Count > 0 && _jobReceiveList.All(jr => jr.IsPrinted) && (!_jobReceiveInstance.IsReceived ?? true))
                {
                    iP4SubmitBtn.Enabled = true;
                }
                else
                {
                    iP4SubmitBtn.Enabled = false;
                }
            }
            else //WIP
            {
                if (_jobReceiveList != null && _jobReceiveList.Count > 0)
                {
                    iP4SubmitBtn.Enabled = true;
                }
                else
                {
                    iP4SubmitBtn.Enabled = false;
                }
            }


        }

        private void IP4MachineTxb_KeyPress(object sender, KeyPressEventArgs e)
        {
            EnforceNumericKeyPress(e);
        }

        private void IP4MachineTxb_KeyUp(object sender, KeyEventArgs e)
        {
            IP4UpdateLotFormatTextbox();
        }

        private void IP4MiscPackagingCountTxb_KeyPress(object sender, KeyPressEventArgs e)
        {
            EnforceDecimalKeyPress(e);
        }

        private void IP4MiscPackagingWeightTxb_KeyPress(object sender, KeyPressEventArgs e)
        {
            EnforceDecimalKeyPress(e);
        }

        private void IP4OuterPackagingCountTxb_KeyPress(object sender, KeyPressEventArgs e)
        {
            EnforceDecimalKeyPress(e);
        }

        private void IP4OuterPackagingCountTxb_TextChanged(object sender, EventArgs e)
        {
            if (decimal.TryParse(iP4TareWeightPerPieceTxb.Text, out var tarePerPiece) &&
                int.TryParse(iP4OuterPackagingCountTxb.Text, out var itemCount))
            {
                iP4OuterPackagingWeightTxb.Text = $"{(tarePerPiece * itemCount):N2}";
            }
        }

        private void IP4OuterPackagingWeightTxb_KeyPress(object sender, KeyPressEventArgs e)
        {
            EnforceDecimalKeyPress(e);
        }

        private void IP4PackagingCbb_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (iP4PackagingCbb.SelectedItem.ToString())
            {
                case "กระสอบแดง":
                    iP4TareWeightPerPieceTxb.Text = "0.21";
                    iP4TareWeightPerPieceTxb.SetReadOnly();
                    break;
                case "กระสอบเหลือง":
                    iP4TareWeightPerPieceTxb.Text = "0.22";
                    iP4TareWeightPerPieceTxb.SetReadOnly();
                    break;
                default:
                    iP4TareWeightPerPieceTxb.Text = "0";
                    iP4TareWeightPerPieceTxb.SetNormal();
                    break;
            }
        }

        private void IP4Pnl_Leave(object sender, EventArgs e)
        {

        }

        private void IP4Pnl_Validated(object sender, EventArgs e)
        {
            if (!_exitFromButton)
            {
                _leavingJobReceive = true;
            }

            _exitFromButton = false;
        }

        private void IP4Pnl_Validating(object sender, CancelEventArgs e)
        {



        }

        private void IP4PrintAllChk_CheckedChanged(object sender, EventArgs e)
        {
            if (iP4PrintAllChk.Checked)
            {
                iP4PrintFromTxb.Text = "";
                iP4PrintToTxb.Text = "";
                iP4PrintFromTxb.Enabled = false;
                iP4PrintToTxb.Enabled = false;
            }
            else
            {
                iP4PrintFromTxb.Enabled = true;
                iP4PrintToTxb.Enabled = true;
            }
        }

        private void IP4PrintBarcodeBtn_Click(object sender, EventArgs e)
        {
            log.Info("Start Method: IP4PrintBarcodeBtn_Click() No parameter");
            IPHandlePrintBarcode();
            iP4JobReceiveItemDgv.Refresh();
        }

        private void IP4PrintFromTxb_KeyPress(object sender, KeyPressEventArgs e)
        {
            EnforceNumericKeyPress(e);
        }

        private void IP4PrintToTxb_KeyPress(object sender, KeyPressEventArgs e)
        {
            EnforceNumericKeyPress(e);
        }

        private void IP4RecalculateWeight()
        {
            if (_jobReceiveList == null || _jobReceiveList.Count <= 0) return;
            iP4TotalCountTxb.Text = _jobReceiveList.Sum(jr => jr.AmountPerPackage).ToString();
            iP4TotalGrossWeightTxb.Text = $"{_jobReceiveList.Sum(jr => jr.GrossWeight):N2}";
            iP4TotalNetWeightTxb.Text = $"{_jobReceiveList.Sum(jr => jr.NetWeight):N2}";
        }
        private void IP4ShiftCbb_SelectedIndexChanged(object sender, EventArgs e)
        {
            IP4UpdateLotFormatTextbox();
        }

        private void IP4StartOptionsCbb_SelectedIndexChanged(object sender, EventArgs e)
        {
            log.Info("Start Method: IP4StartOptionsCbb_SelectedIndexChanged() No parameter");

            try
            {
                WriteLog(_issueProductionInstance?.ID, "JR", "Select JR", $"เลือก Job order option:{((ComboBoxItem)iP4StartOptionsCbb.SelectedItem).Text}");

                if (((ComboBoxItem)iP4StartOptionsCbb.SelectedItem).Value == null) return;
                var finishItemCategory =
                    _entities.ItemCategory.FirstOrDefault(ic => ic.ID == _issueProductionInstance.FinishItemID);
                var finishItemType = finishItemCategory.ItemType;

                if (((ComboBoxItem)iP4StartOptionsCbb.SelectedItem).Value.ToString() == "0") //create new
                {
                    var previousJobReceive = _entities.JobReceive.OrderByDescending(jr => jr.CreationDate)
                        .FirstOrDefault(jr => jr.IssueProductionID == _issueProductionInstance.ID);

                    iP4SubmitBtn.Enabled = true;
                    iP4PrintBarcodeBtn.Enabled = false;
                    _jobReceiveInstance = new JobReceive();
                    if (finishItemType.isItemizable.Value) //if FG
                    {
                        iP4TotalWeightPnl.Visible = true;
                        iP4JobReceiveItemDgv.Visible = true;
                        iP4GenerateLotBtn.Visible = true;
                        iP4GenerateLotBtn.Enabled = true;
                        iP4SpiralChk.Checked = false;
                        iP4ConeCbb.DataSource = _coneNoSpiralList;
                    }
                    else //if WIP
                    {
                        iP4TotalWeightPnl.Visible = false;
                        iP4JobReceiveItemDgv.Visible = false;
                        iP4GenerateLotBtn.Visible = false;
                    }

                    _jobReceiveList = new BindingList<JobReceiveTable>();
                    iP4JobReceiveItemDgv.DataSource = _jobReceiveList;

                    _jobReceiveItemType =
                        _entities.ItemType.FirstOrDefault(it => it.ID == finishItemType.ID);
                    iP4JobNoTxb.Text = _issueProductionInstance.JobOrderNo;
                    iP4ItemCodeTxb.Text = _issueProductionInstance.ItemCategory.ItemCode;
                    if (IsItemFGW(_issueProductionInstance.ItemCategory.ItemCode))
                    {
                        iP4FactoryCbb.DataSource = null;
                        iP4FactoryCbb.Items.Clear();
                        iP4FactoryCbb.Items.Add("WPD");
                        iP4FactoryCbb.SetDisable();
                    }
                    else
                    {
                        iP4FactoryCbb.SetEnable();
                        iP4FactoryCbb.DataSource = _factoryList;
                    }
                    iP4ShiftCbb.DataSource = _shiftList;
                    iP4PackagingCbb.DataSource = _packageList;
                    switch (finishItemType.Name.Trim())
                    {
                        case "Finish Goods - SP":
                            iP4FormulaCbb.DataSource = _formulaSList;
                            break;
                        case "Finish Goods - WE":
                            iP4FormulaCbb.DataSource = _formulaWList;
                            break;
                    }

                    if (previousJobReceive == null)
                    {
                        switch (GetProductionWarehouseFromJobOrder(jobRmDetail.JO_DocNum.ToString()))
                        {
                            case "2":
                                iP4FactoryCbb.SelectedItem = "2";
                                break;
                            case "4":
                                iP4FactoryCbb.SelectedItem = "4";
                                break;
                            default:
                                iP4FactoryCbb.SelectedIndex = 0;
                                break;
                        }
                        iP4StartOptionsCbb.SelectedIndex = 1;
                        iP4MachineTxb.Text = @"1";
                        iP4OuterPackagingWeightTxb.Text = @"0";
                        iP4OuterPackagingCountTxb.Text = @"0";
                        iP4CompletedCountQtyTxb.Text = @"1";
                        iP4InnerPackagingWeightTxb.Text = @"0";
                        iP4InnerPackagingCountTxb.Text = @"0";
                        //iP4CompletedMeasureTxb.Text = @"0";
                        iP4MiscPackagingWeightTxb.Text = @"0";
                        iP4MiscPackagingCountTxb.Text = @"0";
                        //iP4CompletedMeasureLbsTxb.Text = @"0";
                        iP4ToWhseCbb.DataSource = _warehouseList;
                        iP4ToWhseCbb.SelectedItem = _issueProductionInstance.Whse;
                        iP4BinLocationCbb.DataSource = _binLocationList;
                        iP4BinLocationCbb.SelectedItem = "A";
                        iP4ShiftCbb.SelectedItem = "A";
                        iP4FormulaCbb.SelectedItem = "1";
                        iP4TareWeightPerPieceTxb.Text = "";
                        iP4InnerPackageWeightPerPieceTxb.Text = "";
                        iP4AmountPerPackageTxb.Text = finishItemType.Name == "Finish Goods - SP" ? "18" : "1";
                        iP4PackagingCbb.SelectedIndex = 0;
                    }
                    else
                    {
                        _jobReceiveItemType =
                            previousJobReceive.ItemCategory.ItemType;
                        iP4JobNoTxb.Text = _issueProductionInstance.JobOrderNo;
                        iP4ItemCodeTxb.Text = _issueProductionInstance.ItemCategory.ItemCode;
                        if (IsItemFGW(_issueProductionInstance.ItemCategory.ItemCode))
                        {
                            iP4FactoryCbb.DataSource = null;
                            iP4FactoryCbb.Items.Clear();
                            iP4FactoryCbb.Items.Add("WPD");
                            iP4FactoryCbb.SelectedItem = "WPD";
                            iP4FactoryCbb.SetDisable();
                        }
                        else
                        {
                            iP4FactoryCbb.DataSource = _factoryList;
                            iP4FactoryCbb.SelectedItem = previousJobReceive.FactoryNo.Trim();
                            iP4FactoryCbb.SetDisable();
                        }
                        iP4ShiftCbb.DataSource = _shiftList;
                        switch (finishItemType.Name.Trim())
                        {
                            case "Finish Goods - SP":
                                iP4FormulaCbb.DataSource = _formulaSList;
                                iP4SpiralChk.Checked = previousJobReceive.IsSpiral ?? false;
                                iP4ConeCbb.DataSource = (previousJobReceive.IsSpiral ?? false) ? _coneSpiralList : _coneNoSpiralList;
                                iP4ConeCbb.SelectedItem = previousJobReceive.ConeType ?? "";
                                iP4PlasticChk.Checked = previousJobReceive.isPlasticWrapped ?? false;
                                switch (iP4ConeCbb.SelectedItem.ToString())
                                {
                                    case "หลอด(พลาสติก)Local (5°)":
                                        _coneWeight = (decimal)0.06;
                                        break;
                                    case "หลอด(กระดาษ)Export (5°)":
                                        _coneWeight = (decimal)0.04;
                                        break;
                                    case "หลอด(พลาสติก)Local (3°)":
                                        _coneWeight = (decimal)0.04;
                                        break;
                                    case "หลอด(กระดาษ)Export (3°)":
                                        _coneWeight = (decimal)0.03;
                                        break;
                                }
                                if (iP4PlasticChk.Checked)
                                {
                                    _plasticWeight = (decimal)0.005671;
                                }
                                else
                                {
                                    _plasticWeight = 0;
                                }
                                break;
                            case "Finish Goods - WE":
                                iP4FormulaCbb.DataSource = _formulaWList;
                                break;
                        }
                        iP4PackagingCbb.DataSource = _packageList;
                        iP4MachineTxb.Text = previousJobReceive.MachineNo;
                        iP4OuterPackagingWeightTxb.Text = $@"{previousJobReceive.OuterPackageWeight:N2}";
                        iP4OuterPackagingCountTxb.Text = $@"{previousJobReceive.OuterPackageCount ?? 0:N0}";
                        iP4CompletedCountQtyTxb.Text = @"1";
                        iP4InnerPackagingWeightTxb.Text = $@"{previousJobReceive.InnerPackageWeight:N2}";
                        iP4InnerPackagingCountTxb.Text = $@"{previousJobReceive.InnerPackageCount ?? 0:N0}";
                        //iP4CompletedMeasureTxb.Text = $@"{previousJobReceive.QtyCompletedMeasure:N2}";
                        iP4MiscPackagingWeightTxb.Text = $@"{previousJobReceive.MiscPackageWeight:N2}";
                        iP4MiscPackagingCountTxb.Text = $@"{previousJobReceive.MiscPackageCount ?? 0:N0}";
                        //iP4CompletedMeasureLbsTxb.Text =
                        //    $@"{ConvertKgsToLbs(Convert.ToDecimal(previousJobReceive.QtyCompletedMeasure)):N2}";
                        iP4ToWhseCbb.DataSource = _warehouseList;
                        iP4ToWhseCbb.SelectedItem = _issueProductionInstance.Whse.Trim();
                        iP4BinLocationCbb.DataSource = _binLocationList;
                        iP4BinLocationCbb.SelectedItem = previousJobReceive.BinLoc.Trim();
                        iP4ShiftCbb.SelectedItem = previousJobReceive.Shift.Trim();
                        iP4FormulaCbb.SelectedItem = previousJobReceive.Formula.Trim();
                        iP4AmountPerPackageTxb.Text = finishItemType.Name == "Finish Goods - SP" ? "18" : "1";
                        iP4PackagingCbb.SelectedItem = GetNameByPackageType(previousJobReceive.PackageType).Trim();
                        switch (iP4PackagingCbb.SelectedItem.ToString())
                        {
                            case "กระสอบแดง":
                                iP4TareWeightPerPieceTxb.Text = "0.21";
                                iP4TareWeightPerPieceTxb.SetReadOnly();
                                break;
                            case "กระสอบเหลือง":
                                iP4TareWeightPerPieceTxb.Text = "0.22";
                                iP4TareWeightPerPieceTxb.SetReadOnly();
                                break;
                            default:
                                iP4TareWeightPerPieceTxb.Text = "0";
                                iP4TareWeightPerPieceTxb.SetNormal();
                                break;
                        }
                        iP4InnerPackageWeightPerPieceTxb.Text = $"{_coneWeight + _plasticWeight}"; ;
                    }

                    if (IsItemFGW(_issueProductionInstance.ItemCategory.ItemCode))
                    {
                        var standardLength = GetStandardItemWeightPerUnitByItemCode(_issueProductionInstance.ItemCategory.ItemCode);
                        iP4CompletedMeasureTxb.Text = $"{standardLength:N2}";
                    }
                    else
                    {
                        var standardWeightInLbs = GetStandardItemWeightPerUnitByItemCode(_issueProductionInstance.ItemCategory.ItemCode);

                        iP4CompletedMeasureTxb.Text = $"{ConvertLbsToKgs(standardWeightInLbs):N2}";
                        iP4CompletedMeasureLbsTxb.Text = $"{standardWeightInLbs:N2}";
                    }

                    iP4DateCbb.Value = DateTime.Today;
                    iP4CompletedCountQtyTxb.SetNormal();
                    iP4CompletedMeasureTxb.SetReadOnly(); //experiment
                    iP4CompletedMeasureLbsTxb.SetReadOnly();//experiment
                    iP4OuterPackagingCountTxb.SetNormal();
                    iP4OuterPackagingWeightTxb.SetNormal();
                    //iP4InnerPackagingCountTxb.SetNormal();
                    //iP4InnerPackagingWeightTxb.SetNormal();
                    iP4MiscPackagingCountTxb.SetNormal();
                    iP4MiscPackagingWeightTxb.SetNormal();
                    iP4AmountPerPackageTxb.SetNormal();
                    iP4TareWeightPerPieceTxb.SetNormal();
                    iP4InnerPackageWeightPerPieceTxb.SetNormal();
                    iP4BinLocationCbb.SetEnable();
                    iP4DateCbb.Enabled = true;
                    iP4SpiralChk.Enabled = true;
                    iP4PlasticChk.Enabled = true;
                    iP4FactoryCbb.SetEnable();
                    iP4MachineTxb.SetNormal();
                    iP4PackagingCbb.SetEnable();
                    iP4ShiftCbb.SetEnable();
                    iP4FormulaCbb.SetEnable();
                    iP4ToWhseCbb.SetDisable();
                    iP4ConeCbb.SetEnable();
                    iP4JobReceiveItemDgv.ReadOnly = false;
                    iP4TotalCountTxb.Text = "";
                    iP4TotalGrossWeightTxb.Text = "";
                    iP4TotalNetWeightTxb.Text = "";
                    IP4UpdateLotFormatTextbox();
                }
                else //existing
                {
                    iP4SubmitBtn.Enabled = false;
                    iP4PrintBarcodeBtn.Enabled = true;
                    var selectedItem = Convert.ToInt64((iP4StartOptionsCbb.SelectedItem as ComboBoxItem).Value);
                    _jobReceiveInstance = _entities.JobReceive.FirstOrDefault(jr => jr.ID == selectedItem);

                    if (_jobReceiveInstance != null)
                    {
                        var isItemizable = false;
                        if (finishItemCategory.ItemGroupID == 113 || finishItemCategory.ItemGroupID == 103) //if FG
                        {
                            isItemizable = true;
                            iP4TotalWeightPnl.Visible = true;
                            iP4JobReceiveItemDgv.Visible = true;
                            iP4GenerateLotBtn.Visible = true;
                            iP4GenerateLotBtn.Enabled = false;
                        }
                        else //if WIP
                        {
                            iP4TotalWeightPnl.Visible = false;
                            iP4JobReceiveItemDgv.Visible = false;
                            iP4GenerateLotBtn.Visible = false;
                        }

                        _jobReceiveItemType =
                            _jobReceiveInstance.ItemCategory.ItemType;
                        iP4JobNoTxb.Text = _issueProductionInstance.JobOrderNo;
                        iP4ItemCodeTxb.Text = _issueProductionInstance.ItemCategory.ItemCode;
                        if (IsItemFGW(_issueProductionInstance.ItemCategory.ItemCode))
                        {
                            iP4FactoryCbb.DataSource = null;
                            iP4FactoryCbb.Items.Clear();
                            iP4FactoryCbb.Items.Add("WPD");
                            iP4FactoryCbb.SelectedItem = "WPD";
                            iP4FactoryCbb.SetDisable();
                        }
                        else
                        {
                            iP4FactoryCbb.DataSource = _factoryList;
                            iP4FactoryCbb.SelectedItem = _jobReceiveInstance.FactoryNo.Trim();
                            iP4FactoryCbb.SetDisable();
                        }
                        iP4ShiftCbb.DataSource = _shiftList;
                        switch (finishItemType.Name.Trim())
                        {
                            case "Finish Goods - SP":
                                iP4FormulaCbb.DataSource = _formulaSList;
                                iP4SpiralChk.Checked = _jobReceiveInstance.IsSpiral ?? false;
                                iP4ConeCbb.DataSource = (_jobReceiveInstance.IsSpiral ?? false) ? _coneSpiralList : _coneNoSpiralList;
                                iP4ConeCbb.SelectedItem = _jobReceiveInstance.ConeType;
                                iP4PlasticChk.Checked = _jobReceiveInstance.isPlasticWrapped ?? false;
                                break;
                            case "Finish Goods - WE":
                                iP4FormulaCbb.DataSource = _formulaWList;
                                break;
                        }
                        iP4PackagingCbb.DataSource = _packageList;
                        iP4MachineTxb.Text = _jobReceiveInstance.MachineNo;
                        iP4OuterPackagingWeightTxb.Text = $@"{_jobReceiveInstance.OuterPackageWeight:N2}";
                        iP4OuterPackagingCountTxb.Text = $@"{_jobReceiveInstance.OuterPackageCount ?? 0:N0}";
                        iP4CompletedCountQtyTxb.Text = $@"{(int)_jobReceiveInstance.QtyCompletedCount:N0}";
                        iP4InnerPackagingWeightTxb.Text = $@"{_jobReceiveInstance.InnerPackageWeight:N2}";
                        iP4InnerPackagingCountTxb.Text = $@"{_jobReceiveInstance.InnerPackageCount ?? 0:N0}";
                        iP4CompletedMeasureTxb.Text = $@"{_jobReceiveInstance.QtyCompletedMeasure:N2}";
                        iP4MiscPackagingWeightTxb.Text = $@"{_jobReceiveInstance.MiscPackageWeight:N2}";
                        iP4MiscPackagingCountTxb.Text = $@"{_jobReceiveInstance.MiscPackageCount ?? 0:N0}";
                        iP4CompletedMeasureLbsTxb.Text =
                            $@"{ConvertKgsToLbs(Convert.ToDecimal(_jobReceiveInstance.QtyCompletedMeasure)):N2}";
                        iP4ToWhseCbb.DataSource = _warehouseList;
                        iP4ToWhseCbb.SelectedItem = _issueProductionInstance.Whse.Trim();
                        iP4BinLocationCbb.DataSource = _binLocationList;
                        iP4BinLocationCbb.SelectedItem = _jobReceiveInstance.BinLoc.Trim();
                        iP4ShiftCbb.SelectedItem = _jobReceiveInstance.Shift.Trim();
                        iP4FormulaCbb.SelectedItem = _jobReceiveInstance.Formula.Trim();
                        iP4TareWeightPerPieceTxb.Text = "";
                        iP4InnerPackageWeightPerPieceTxb.Text = "";
                        iP4AmountPerPackageTxb.Text = finishItemType.Name == "Finish Goods - SP" ? "18" : "1";
                        iP4PackagingCbb.SelectedItem = GetNameByPackageType(_jobReceiveInstance.PackageType).Trim();
                        iP4DateCbb.Value = _jobReceiveInstance.AssignDate ?? DateTime.Today;
                        iP4CompletedCountQtyTxb.SetReadOnly();
                        iP4CompletedMeasureTxb.SetReadOnly();
                        iP4CompletedMeasureLbsTxb.SetReadOnly();
                        iP4OuterPackagingCountTxb.SetReadOnly();
                        iP4OuterPackagingWeightTxb.SetReadOnly();
                        iP4InnerPackagingCountTxb.SetReadOnly();
                        iP4InnerPackagingWeightTxb.SetReadOnly();
                        iP4MiscPackagingCountTxb.SetReadOnly();
                        iP4MiscPackagingWeightTxb.SetReadOnly();
                        iP4AmountPerPackageTxb.SetReadOnly();
                        iP4TareWeightPerPieceTxb.SetReadOnly();
                        iP4InnerPackageWeightPerPieceTxb.SetReadOnly();
                        iP4BinLocationCbb.SetDisable();
                        iP4ConeCbb.SetDisable();
                        iP4DateCbb.Enabled = false;
                        iP4MachineTxb.SetReadOnly();
                        iP4PackagingCbb.SetDisable();
                        iP4ShiftCbb.SetDisable();
                        iP4FormulaCbb.SetDisable();
                        iP4ToWhseCbb.SetDisable();
                        iP4SpiralChk.Enabled = false;
                        iP4PlasticChk.Enabled = false;
                        //iP4JobReceiveItemDgv.ReadOnly = true;
                        IP4UpdateLotFormatTextbox();

                        IP4InitializeJobReceiveList(isItemizable, false);

                        iP4JobReceiveItemDgv.Columns.Clear();
                        iP4JobReceiveItemDgv.DataSource = _jobReceiveList;
                        iP4JobReceiveItemDgv.Columns.Insert(iP4JobReceiveItemDgv.Columns.Count, new DataGridViewButtonColumn()
                        {
                            Text = "พิมพ์บาร์โค้ด",
                            Name = "print barcode button",
                            HeaderText = "",
                            UseColumnTextForButtonValue = true
                        });
                        iP4JobReceiveItemDgv.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                        iP4JobReceiveItemDgv.Columns[iP4JobReceiveItemDgv.Columns.Count - 1].Visible = true;
                        iP4JobReceiveItemDgv.Columns["NetWeight"].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                        iP4JobReceiveItemDgv.Columns["GrossWeight"].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                        iP4JobReceiveItemDgv.Columns["AmountPerPackage"].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                        //iP4JobReceiveItemDgv.ReadOnly = true;
                        iP4JobReceiveItemDgv.Columns["GrossWeight"].DefaultCellStyle.Format = "N2";
                        iP4JobReceiveItemDgv.Columns["NetWeight"].DefaultCellStyle.Format = "N2";
                        iP4JobReceiveItemDgv.Columns["RowNo"].Width = 40;
                        iP4JobReceiveItemDgv.Columns["print barcode button"].Width = 100;
                    }
                }

                var countUnit = GetThaiUOMCountFromItemType(_jobReceiveItemType);
                var measureUnit = GetThaiUOMMeasureFromItemType(_jobReceiveItemType);

                iP4CompletedCountUnitLbl.Text = countUnit;
                iP4CompletedMeasureUnitLbl.Text = measureUnit;
                iP4TotalGrossWeightUnitLbl.Text = measureUnit;
                iP4TotalNetWeightUnitLbl.Text = measureUnit;

                iP4AmountPerPackageTxb.Text = IsItemFGS(_issueProductionInstance.ItemCategory.ItemCode) ? "18" : "";

                if (_jobReceiveItemType.Name == "Finish Goods - WE")
                {
                    iP4CompletedMeasureLbsTxb.Visible = false;
                    iP4CompletedMeasureLbsUnitTxb.Visible = false;

                    iP4CompletedMeasure1Lbl.Text = "ความยาวสุทธิ";
                    iP4CompletedMeasure2Lbl.Visible = false;
                }
                else if (_jobReceiveItemType.Name == "Finish Goods - SP")
                {
                    iP4CompletedMeasureLbsTxb.Visible = true;
                    iP4CompletedMeasureLbsUnitTxb.Visible = true;

                    iP4CompletedMeasure1Lbl.Text = "น.น.รวม";
                    iP4CompletedMeasure2Lbl.Text = "น.น.รวม";
                    iP4CompletedMeasure2Lbl.Visible = true;
                }
                else
                {
                    iP4CompletedMeasureLbsTxb.Visible = true;
                    iP4CompletedMeasureLbsUnitTxb.Visible = true;

                    iP4CompletedMeasure1Lbl.Text = "น.น.สุทธิ";
                    iP4CompletedMeasure2Lbl.Text = "น.น.สุทธิ";
                    iP4CompletedMeasure2Lbl.Visible = true;
                }

                if (!_jobReceiveItemType.isItemizable ?? false)
                {
                    iP4FormulaCbb.Visible = false;
                    iP4FormulaLbl.Visible = false;
                }
                else
                {
                    iP4FormulaCbb.Visible = true;
                    iP4FormulaLbl.Visible = true;
                }

                if ((_jobReceiveItemType.isItemizable ?? false) && !IsItemFGW(_issueProductionInstance.ItemCategory.ItemCode))
                {
                    iP4TareInfoPnl.Visible = true;
                    iP4SpiralChk.Visible = true;
                }
                else
                {
                    iP4TareInfoPnl.Visible = false;
                    iP4SpiralChk.Visible = false;
                    iP4InnerPackagingWeightTxb.Text = "0";
                    iP4InnerPackagingWeightTxb.Text = "0";
                    iP4MiscPackagingWeightTxb.Text = "0";
                }

                iP4StartOptionsPanel.Visible = false;
            }
            catch (Exception ex)
            {
                log.Error("Method: IP4StartOptionsCbb_SelectedIndexChanged() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: IP4StartOptionsCbb_SelectedIndexChanged()");
            }
        }

        private void IP4SubmitBtn_Click(object sender, EventArgs e)
        {
            log.Info("Start Method: IP4SubmitBtn_Click() No parameter");
            Cursor.Current = Cursors.WaitCursor;

            try
            {
                if (iP4FactoryCbb.SelectedItem == "")
                {
                    MessageBox.Show("กรูณาเลือกโรงงานผลิต");
                    return;
                }

                if (string.IsNullOrEmpty(iP4MachineTxb.Text))
                {
                    MessageBox.Show("กรูณาเลือกเครื่องที่ผลิต");
                    return;
                }

                if (!int.TryParse(iP4CompletedCountQtyTxb.Text, out var completeQty))
                {
                    MessageBox.Show("กรูณาใส่จำนวนรับผลิตให้ถูกต้อง");
                    return;
                }
                else if (completeQty <= 0)
                {
                    MessageBox.Show("กรูณาใส่จำนวนรับผลิตให้ถูกต้อง");
                    return;
                }

                if (_jobReceiveInstance.ID == 0)
                {
                    var isItemizable = _jobReceiveItemType.isItemizable ?? false;
                    IP4InitializeJobReceiveList(isItemizable, true);
                }

                _jobReceiveInstance.BinLoc = iP4BinLocationCbb.SelectedItem.ToString();
                _jobReceiveInstance.CreationDate = DateTime.Now;
                _jobReceiveInstance.FactoryNo = iP4FactoryCbb.SelectedItem.ToString();
                _jobReceiveInstance.FinishItemID = _issueProductionInstance.FinishItemID;
                _jobReceiveInstance.InnerPackageCount = Convert.ToInt32(iP4InnerPackagingCountTxb.Text);
                _jobReceiveInstance.InnerPackageWeight = Convert.ToDecimal(iP4InnerPackagingWeightTxb.Text);
                _jobReceiveInstance.IssueProductionID = _issueProductionInstance.ID;
                _jobReceiveInstance.OuterPackageWeight = Convert.ToDecimal(iP4OuterPackagingWeightTxb.Text);
                _jobReceiveInstance.OuterPackageCount = Convert.ToInt32(iP4OuterPackagingCountTxb.Text);
                _jobReceiveInstance.MiscPackageWeight = Convert.ToDecimal(iP4MiscPackagingWeightTxb.Text);
                _jobReceiveInstance.MiscPackageCount = Convert.ToInt32(iP4MiscPackagingCountTxb.Text);
                _jobReceiveInstance.QtyCompletedCount = Convert.ToDecimal(iP4CompletedCountQtyTxb.Text);
                _jobReceiveInstance.QtyCompletedMeasure = Convert.ToDecimal(iP4CompletedMeasureTxb.Text);
                _jobReceiveInstance.Shift = iP4ShiftCbb.SelectedItem.ToString();
                _jobReceiveInstance.Formula = (_jobReceiveItemType.isItemizable ?? false) ? $"{Convert.ToInt32(iP4FormulaCbb.SelectedItem):D2}" : "";
                _jobReceiveInstance.ToWhse = iP4ToWhseCbb.SelectedItem.ToString();
                _jobReceiveInstance.MachineNo = iP4MachineTxb.Text;
                _jobReceiveInstance.AssignDate = iP4DateCbb.Value;
                _jobReceiveInstance.PackageType = GetPackageTypeByName(iP4PackagingCbb.SelectedItem.ToString());
                if (_jobReceiveItemType.Name == "Finish Goods - SP")
                {
                    _jobReceiveInstance.ConeType = iP4ConeCbb.SelectedItem.ToString();
                    _jobReceiveInstance.IsSpiral = iP4SpiralChk.Checked;
                    _jobReceiveInstance.isPlasticWrapped = iP4PlasticChk.Checked;
                }
                _jobReceiveInstance.IsReceived = true;

                //iP4JobReceiveItemDgv.ReadOnly = true;
                iP4JobReceiveItemDgv.Columns[iP4JobReceiveItemDgv.Columns.Count - 1].Visible = true;

                iP4SubmitBtn.Enabled = false;
                iP4GenerateLotBtn.Enabled = false;
                iP4JobReceiveItemDgv.ReadOnly = true;
                iP4PrintBarcodeBtn.Enabled = true;

                var result = Interface_GoodsReceiveExcelTemplate();
                if (result)
                {
                    var jobReceiveItemsToSend = new List<JobReceiveTable>();

                    if (_jobReceiveItemType.isItemizable.Value)
                    {
                        jobReceiveItemsToSend = _jobReceiveList.Where(jr => jr.IsPrinted && !jr.IsSentToB1).ToList();
                    }
                    else
                    {
                        jobReceiveItemsToSend = _jobReceiveList.Where(jr => !jr.IsSentToB1).ToList();
                    }

                    WriteLog(_issueProductionInstance?.ID, "JR", "Save"
                        , $"JobReceiveID:{_jobReceiveInstance?.ID} รหัสสินค้า:{iP4ItemCodeTxb.Text} Lot no:{iP4LotNoTxb.Text} จำนวนที่ต้องการบันทึกลงระบบ:{jobReceiveItemsToSend.Count} น.น.สุทธิรวม:{jobReceiveItemsToSend.Sum(i => i.NetWeight)}");

                    foreach (var jobReceiveTable in jobReceiveItemsToSend)
                    {
                        jobReceiveTable.IsSentToB1 = true;
                        var jobreceiveItem = _entities.JobReceiveItem.FirstOrDefault(jr => jr.ItemID == jobReceiveTable.ItemID && jr.JobReceiveID == _jobReceiveInstance.ID);
                        if (jobreceiveItem != null)
                        {
                            jobreceiveItem.IsSentToB1 = true;
                            var item = jobreceiveItem.Item;
                            if (!item.ItemType.isItemizable.Value)
                            {
                                var existingItem = _entities.Item.FirstOrDefault(i =>
                                    i.LotNo == item.LotNo && i.ItemCategory.ItemCode == item.ItemCategory.ItemCode && i.Whse == item.Whse.Replace(ReceiveWhse, "") && i.ID != item.ID);
                                if (existingItem != null)
                                {
                                    existingItem.Count += item.Count;
                                    jobreceiveItem.Item = existingItem;
                                    jobReceiveTable.ItemID = jobreceiveItem.Item.ID;
                                    _entities.Item.Remove(item);
                                }
                                else
                                {
                                    item.Whse = item.Whse.Replace(ReceiveWhse, "");
                                }
                            }
                            else
                            {
                                item.Whse = item.Whse.Replace(ReceiveWhse, "");
                            }
                        }
                    }
                    _entities.SaveChanges();
                    iP4FactoryCbb.SetDisable();
                    iP4ShiftCbb.SetDisable();
                    iP4MachineTxb.SetReadOnly();
                    iP4FormulaCbb.SetDisable();
                    iP4PackagingCbb.SetDisable();
                    iP4ConeCbb.SetDisable();
                    iP4TareWeightPerPieceTxb.SetReadOnly();
                    iP4InnerPackageWeightPerPieceTxb.SetReadOnly();
                    iP4AmountPerPackageTxb.SetReadOnly();
                    iP4MiscPackagingWeightTxb.SetReadOnly();
                    iP4MiscPackagingCountTxb.SetReadOnly();
                    iP4SpiralChk.Enabled = false;
                    iP4PlasticChk.Enabled = false;
                    iP4CompletedCountQtyTxb.SetReadOnly();
                    iP4CompletedMeasureTxb.SetReadOnly();
                    iP4BinLocationCbb.SetDisable();
                    MessageBox.Show("บันทึกเรียบร้อย");
                }
                else
                {
                    //Enable Save btn
                    _jobReceiveInstance.IsReceived = false;
                    iP4SubmitBtn.Enabled = true;
                    //RevertAllEntitiesChanges();
                    //MessageBox.Show("บันทึกไม่สำเร็จ กรุณาลองใหม่อีกครั้ง");
                }
            }
            catch (Exception ex)
            {
                log.Error("Method: IP4SubmitBtn_Click() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: IP4SubmitBtn_Click()");
                Cursor.Current = Cursors.Default;
            }
        }
        private void IP4TareWeightPerPieceTxb_KeyPress(object sender, KeyPressEventArgs e)
        {
            EnforceDecimalKeyPress(e);
        }

        private void IP4TareWeightPerPieceTxb_TextChanged(object sender, EventArgs e)
        {
            if (decimal.TryParse(iP4TareWeightPerPieceTxb.Text, out var outerPerPiece) &&
                int.TryParse(iP4OuterPackagingCountTxb.Text, out var itemCount))
            {
                iP4OuterPackagingWeightTxb.Text = $"{(outerPerPiece * itemCount):N2}";
            }
        }

        private void IP4UpdateLotFormatTextbox()
        {
            log.Info("Start Method: IP4UpdateLotFormatTextbox() No parameter");

            try
            {
                if (_issueProductionInstance.ItemCategory.ItemGroupID.Value != 113 &&
                    _issueProductionInstance.ItemCategory.ItemGroupID.Value != 103)
                {
                    iP4FormulaCbb.SelectedItem = "A";
                }
                if (iP4FactoryCbb.SelectedItem == null || string.IsNullOrEmpty(iP4MachineTxb.Text) ||
                    iP4ShiftCbb.SelectedItem == null || iP4FormulaCbb.SelectedItem == null) return;
                if (_issueProductionInstance?.ItemCategory.ItemGroupID == null) return;
                switch (_issueProductionInstance.ItemCategory.ItemGroupID.Value)//todo replace with real values
                {
                    case 113: //FGS
                        iP4LotNoTxb.Text = ReplaceFormatText(_jobReceiveItemType.LotFormat,
                            iP4FactoryCbb.SelectedItem.ToString(), iP4MachineTxb.Text,
                            iP4ShiftCbb.SelectedItem.ToString(), "", "", $"{Convert.ToInt32(iP4FormulaCbb.SelectedItem):D1}", _issueProductionInstance.ItemCategory.ItemCode);
                        break;
                    case 103: //FGW
                        iP4LotNoTxb.Text = ReplaceFormatText(_jobReceiveItemType.LotFormat,
                            iP4FactoryCbb.SelectedItem.ToString(), iP4MachineTxb.Text,
                            iP4ShiftCbb.SelectedItem.ToString(), "", "", $"{Convert.ToInt32(iP4FormulaCbb.SelectedItem):D2}", _issueProductionInstance.ItemCategory.ItemCode);
                        break;
                    default:
                        iP4LotNoTxb.Text = ReplaceFormatText(_jobReceiveItemType.LotFormat,
                            iP4FactoryCbb.SelectedItem.ToString(), iP4MachineTxb.Text,
                            iP4ShiftCbb.SelectedItem.ToString(), "0.00", "0001", "");
                        break;
                }
            }
            catch (Exception ex)
            {
                log.Error("Method: IP4UpdateLotFormatTextbox() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: IP4UpdateLotFormatTextbox()");
            }
        }

        private void IPHandlePrintBarcode(int start = 0, int end = 0)
        {
            log.Info("Start Method: IPHandlePrintBarcode() No parameter");
            try
            {
                bool isFG = !(_jobReceiveItemType.Name.StartsWith("FWIP") ||
                              _jobReceiveItemType.Name.StartsWith("Raw")); //todo check if WIP item
                bool isFGW = _jobReceiveItemType.Name == "Finish Goods - WE";

                var printStart = 0;
                var printEnd = 0;

                if (start > 0 && end > 0)
                {
                    printStart = start;
                    printEnd = end;
                }
                else if (iP4PrintAllChk.Checked)
                {
                    printStart = 1;
                    printEnd = isFG ? _jobReceiveList.Count : 1;
                }
                else
                {
                    if (string.IsNullOrEmpty(iP4PrintFromTxb.Text) || string.IsNullOrEmpty(iP4PrintToTxb.Text))
                    {
                        MessageBox.Show("กรุณาใส่ตัวเลขให้ถูกค้อง");
                        return;
                    }

                    printStart = Convert.ToInt32(iP4PrintFromTxb.Text);
                    printEnd = Convert.ToInt32(iP4PrintToTxb.Text);
                }

                var jobReceivePrintList = new List<JobReceiveTable>();
                if (!isFG)
                {
                    var amount = _jobReceiveList.FirstOrDefault().AmountPerPackage;
                    for (int i = 0; i < Math.Min(amount, printEnd - printStart + 1); i++)
                    {
                        jobReceivePrintList.Add(_jobReceiveList.FirstOrDefault());
                    }
                }
                else //FG
                {
                    jobReceivePrintList = _jobReceiveList.Skip(printStart - 1).Take(printEnd - printStart + 1).ToList();
                }

                var totalRowCount = jobReceivePrintList.Count;

                var pageCount = totalRowCount;

                WriteLog(_issueProductionInstance?.ID, "JR", "Print Barcode",
                    $"Print barcode ย่อยของหมายเลข Lot:{jobReceivePrintList.FirstOrDefault()?.LotNo} จาก Row:{printStart} ถึง Row:{printEnd} เป็นจำนวน(ชิ้น):{totalRowCount}");


                //var lastPageRowCount = (int)Math.Round((double)(totalRowCount % 100) / 2, MidpointRounding.AwayFromZero);
                var tempWorkBook = new Workbook();
                tempWorkBook.Version = ExcelVersion.Version2010;
                tempWorkBook.Worksheets.Clear();
                var worksheet = isFG ? _barcodeFgWorkbook.Worksheets[0] : _barcodeWorkbook.Worksheets[0];
                if (isFGW)
                {
                    worksheet = _barcodeFgWorkbook.Worksheets[1];
                }

                if (isFG)
                {
                    worksheet.Rows[0].RowHeight = 54;
                    worksheet.Rows[1].RowHeight = 120;
                    worksheet.Rows[2].RowHeight = 60;
                    worksheet.Rows[3].RowHeight = 48;

                    for (int i = 0; i < pageCount; i++)
                    {
                        tempWorkBook.Worksheets.AddCopy(worksheet);
                    }

                    var smallFont = tempWorkBook.CreateFont();
                    smallFont.KnownColor = ExcelColors.Black;
                    smallFont.IsBold = false;
                    smallFont.IsItalic = false;
                    smallFont.FontName = "Calibri";
                    smallFont.Size = 9;

                    var mediumFont = tempWorkBook.CreateFont();
                    mediumFont.KnownColor = ExcelColors.Black;
                    mediumFont.IsBold = false;
                    mediumFont.IsItalic = false;
                    mediumFont.FontName = "Calibri";
                    mediumFont.Size = 11;

                    var largeFont = tempWorkBook.CreateFont();
                    largeFont.KnownColor = ExcelColors.Black;
                    largeFont.IsBold = true;
                    largeFont.IsItalic = false;
                    largeFont.FontName = "Calibri";
                    largeFont.Size = 13;

                    var itemList = _entities.Item.ToList();

                    var sheetCount = 0;

                    foreach (var jobreceiveitem in jobReceivePrintList)
                    {
                        var item = itemList.FirstOrDefault(i => i.ID == jobreceiveitem.ItemID);

                        var BarCodePicture = tempWorkBook.Worksheets[sheetCount].Pictures
                            .Add(2, 1, GetQRcodeImageForFG(item.Barcode));
                        var itemCodeText = item.ItemCategory.ItemCode;
                        //var lotNoText = "\n" + item.LotNo;
                        var measureText = $"_{(item.Measure ?? 0):N2}";
                        var dateText = "\n";
                        if (IsItemFGS(itemCodeText))
                        {
                            dateText = _jobReceiveInstance.AssignDate.HasValue
                                ? "\nวันที่ผลิต " + _jobReceiveInstance.AssignDate.Value.ToString("dd/MM/yyyy")
                                : "\nวันที่ผลิต " + DateTime.Now.ToString("dd/MM/yyyy");
                        }

                        //if (isFGW)
                        //{
                        var lotNoText = "\n" + item.LotNo.Substring(0, item.LotNo.Length - 5);
                        //}

                        tempWorkBook.Worksheets[sheetCount].Range[3, 1].RichText.Text =
                            itemCodeText + lotNoText + measureText + dateText;

                        tempWorkBook.Worksheets[sheetCount].Range[3, 1].IsWrapText = true;
                        tempWorkBook.Worksheets[sheetCount].Range[3, 1].RichText.SetFont(
                            0,
                            itemCodeText.Length + 1, largeFont);
                        tempWorkBook.Worksheets[sheetCount].Range[3, 1].RichText.SetFont(
                            itemCodeText.Length,
                            itemCodeText.Length + lotNoText.Length + measureText.Length, mediumFont);
                        tempWorkBook.Worksheets[sheetCount].Range[3, 1].RichText.SetFont(
                            itemCodeText.Length + lotNoText.Length + measureText.Length,
                            itemCodeText.Length + lotNoText.Length + measureText.Length + dateText.Length - 1, smallFont);

                        //leftPicture.LeftColumnOffset = ;
                        BarCodePicture.TopRowOffset = 5;

                        sheetCount++;
                        jobreceiveitem.IsPrinted = true;
                        var jobreceiveItem = _entities.JobReceiveItem.FirstOrDefault(jr => jr.ItemID == item.ID && jr.JobReceiveID == _jobReceiveInstance.ID);
                        if (jobreceiveItem != null)
                        {
                            jobreceiveItem.IsPrinted = true;
                        }
                    }
                    _entities.SaveChanges();
                }
                else
                {
                    //worksheet.Columns[0].ColumnWidth = 5.14;
                    //worksheet.Columns[1].ColumnWidth = 14.29;
                    //worksheet.Columns[2].ColumnWidth = 5.14;
                    //worksheet.Columns[3].ColumnWidth = 5.14;
                    //worksheet.Columns[4].ColumnWidth = 14.29;
                    //worksheet.Columns[5].ColumnWidth = 5.14;
                    //worksheet.Rows[0].RowHeight = 78.75;
                    //worksheet.Rows[3].RowHeight = 78.75;
                    //worksheet.Rows[6].RowHeight = 78.75;
                    //worksheet.Rows[1].RowHeight = 60;
                    //worksheet.Rows[4].RowHeight = 60;
                    //worksheet.Rows[7].RowHeight = 60;
                    //worksheet.Rows[2].RowHeight = 7.5;
                    //worksheet.Rows[5].RowHeight = 7.5;

                    for (int i = 0; i < pageCount; i++)
                    {
                        tempWorkBook.Worksheets.AddCopy(worksheet);
                    }

                    var itemList = _entities.Item.ToList();

                    var sheetCount = 0;

                    var mediumFont = tempWorkBook.CreateFont();
                    mediumFont.KnownColor = ExcelColors.Black;
                    mediumFont.IsBold = false;
                    mediumFont.IsItalic = false;
                    mediumFont.FontName = "Calibri";
                    mediumFont.Size = 8;

                    var smallFont = tempWorkBook.CreateFont();
                    smallFont.KnownColor = ExcelColors.Black;
                    smallFont.IsBold = false;
                    smallFont.IsItalic = false;
                    smallFont.FontName = "Calibri";
                    smallFont.Size = 7;

                    var logo = new Bitmap(ConfigurationManager.AppSettings["Logo90"]);

                    foreach (var jobreceiveitem in jobReceivePrintList)
                    {
                        var item = itemList.FirstOrDefault(i => i.ID == jobreceiveitem.ItemID);
                        //tempWorkBook.Worksheets[0].SetRowHeight(rowCount, 170);
                        var leftPicture = tempWorkBook.Worksheets[sheetCount].Pictures
                            .Add(3, 1, GetQRcodeImage(item.Barcode));
                        var rightPicture = tempWorkBook.Worksheets[sheetCount].Pictures
                            .Add(1, 1, GetQRcodeImage(item.Barcode));
                        var leftLogo = tempWorkBook.Worksheets[sheetCount].Pictures.Add(2, 1, logo);
                        var rightLogo = tempWorkBook.Worksheets[sheetCount].Pictures.Add(4, 1, logo);

                        tempWorkBook.Worksheets[sheetCount].Range[1, 2].RichText.Text =
                            item.ItemCategory.LotNo + "\n" + item.LotNo;
                        tempWorkBook.Worksheets[sheetCount].Range[3, 2].RichText.Text =
                            item.ItemCategory.LotNo + "\n" + item.LotNo;
                        tempWorkBook.Worksheets[sheetCount].Range[1, 2].IsWrapText = true;
                        tempWorkBook.Worksheets[sheetCount].Range[3, 2].IsWrapText = true;

                        tempWorkBook.Worksheets[sheetCount].Range[1, 2].RichText.SetFont(0,
                            item.ItemCategory.LotNo.Length + item.LotNo.Length, mediumFont);
                        //tempWorkBook.Worksheets[sheetCount].Range[1, 2].RichText.SetFont(item.ItemCategory.LotNo.Length + item.LotNo.Length + 1, item.ItemCategory.LotNo.Length + item.LotNo.Length + 1, smallFont);
                        tempWorkBook.Worksheets[sheetCount].Range[3, 2].RichText.SetFont(0,
                            item.ItemCategory.LotNo.Length + item.LotNo.Length, mediumFont);
                        //tempWorkBook.Worksheets[sheetCount].Range[3, 2].RichText.SetFont(item.ItemCategory.LotNo.Length + item.LotNo.Length + 1, item.ItemCategory.LotNo.Length + item.LotNo.Length + 1, smallFont);
                        //leftPicture.LeftColumnOffset = ;
                        leftPicture.TopRowOffset = 65;
                        rightPicture.TopRowOffset = 65;

                        leftLogo.Width = 25;
                        leftLogo.Height = 29;
                        rightLogo.Width = 25;
                        rightLogo.Height = 29;
                        leftLogo.TopRowOffset = 2;
                        rightLogo.TopRowOffset = 2;
                        leftLogo.LeftColumnOffset = 50;
                        rightLogo.LeftColumnOffset = 50;
                        //tempWorkBook.Worksheets[0].SetRowHeight(rowCount, 100);
                        //tempWorkBook.Worksheets[0].Pictures.Add(rowCount++, 1, GetBarcodeImage(item.Barcode));

                        sheetCount++;
                        jobreceiveitem.IsPrinted = true;
                        var jobreceiveItem = _entities.JobReceiveItem.FirstOrDefault(jr => jr.ItemID == item.ID && jr.JobReceiveID == _jobReceiveInstance.ID);
                        if (jobreceiveItem != null)
                        {
                            jobreceiveItem.IsPrinted = true;
                        }
                    }
                    _entities.SaveChanges();
                }

                if (_printEnable)
                {
                    PrintExcelByInterop(tempWorkBook, _barcodePrinterName);
                }
                else
                {
                    SaveFileExcel(tempWorkBook, "Job Receive Barcode");
                }

                //_issueProductionInstance.PrintCount++;
            }
            catch (Exception ex)
            {
                log.Error("Method: IPHandlePrintBarcode() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: IPHandlePrintBarcode()");
            }
        }
        private void IPMagicBtn_Click(object sender, EventArgs e)
        {
            log.Info("Start Method: IPMagicBtn_Click() No parameter");

            try
            {
                if (_jobIssueSentToB1)
                {
                    MessageBox.Show($"รายการเบิกวัตถุดิบนี้ได้ส่งไปที่ B1 แล้ว ไม่สามารถแก้ไขได้");
                    return;
                }

                var categoryList = _issueProductionRequestList.Select(ipr => ipr.ItemCategoryID.Value).ToList();
                var allItem = _entities.Item.Where(i => categoryList.Contains(i.ItemCategoryID.Value) && !i.Whse.Contains(IssuedWhse) && !i.Whse.Contains(InTranWhse) && !i.Whse.Contains(SoldWhse) && !i.Whse.Contains(ReceiveWhse)).ToList();
                //var random1 = new Random().Next(_issueProductionRequestList.Count - 1);
                //var randomIPRequest = _issueProductionRequestList.Skip(random1).Take(1).FirstOrDefault();
                //var itemList = _entities.Item.Where(i => i.ItemCategoryID == randomIPRequest.ItemCategoryID).ToList();
                //var random2 = new Random().Next(allItem.Count - 1);
                //var randomItem = itemList.Skip(random2).Take(1).FirstOrDefault();
                if (allItem.Count == 0)
                {
                    return;
                }
                var random3 = new Random().Next(allItem.Count);
                var randomItem = allItem.OrderBy(i => i.ID).Skip(random3).Take(1).FirstOrDefault();

                if (_issueProductionRequestList.Select(ipr => ipr.ItemCategory).Contains(randomItem.ItemCategory))
                {
                    if (!randomItem.ItemType.Name.StartsWith("FWIP") || !_scannedItemList.Any(i =>
                            i.LotNo == randomItem.LotNo && i.ItemCode == randomItem.ItemCategory.ItemCode))
                    {
                        var mappedItem = new IssueProductionIssueTable()
                        {
                            RowNo = _scannedItemList.Count + 1,
                            ID = randomItem.ID,
                            ItemCode = randomItem.ItemCategory.ItemCode,
                            Description = randomItem.ItemCategory.Description,
                            LotNo = randomItem.LotNo,
                            GrossWeight = randomItem.Measure.Value,
                            NetWeight = randomItem.Measure.Value - randomItem.OuterPackageWeight.Value,
                            Whse = randomItem.Whse,
                            Count = randomItem.ItemType.isItemizable.Value ? (int)(randomItem.Count ?? 0) : 1
                        };
                        _scannedItemList.Add(mappedItem);
                    }
                }
                else
                {
                    MessageBox.Show("สินค้าที่แสกนไม่อยู่ในรหัสสินค้า");
                }

                IP3_RecalculateTotalWeight();
                iP3SendB1Btn.Enabled = false;
                iP3SubmitBtn.Enabled = true;
                _jobIssueIsSaved = false;
                //iP3ScannedItemDgv.AutoResizeColumns();
            }
            catch (Exception ex)
            {
                log.Error("Method: IPMagicBtn_Click() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: IPMagicBtn_Click()");
            }
        }

        private void IPShowPanel(ref MetroPanel panel)
        {
            iP1Pnl.Hide();
            iP2Pnl.Hide();
            iP3Pnl.Hide();
            iP4Pnl.Hide();
            iP5Pnl.Hide();

            //panel.Dock = DockStyle.Fill;
            panel.Show();
        }

        private void IssueTabPage_Enter(object sender, EventArgs e)
        {
        }
    }
}