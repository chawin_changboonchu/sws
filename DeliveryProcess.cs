﻿using MetroFramework.Controls;
using Spire.Xls;
using SWS.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data.Entity;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;

namespace SWS
{
    public partial class Inventory
    {
        public void DO1Pnl_KeyPress(Object sender, KeyPressEventArgs e)
        {
            log.Info("Start Method: DO1Pnl_KeyPress() No parameter");

            try
            {
                if (e.KeyChar != (char)Keys.Return)
                {
                    _do1BarcodeBuffer += e.KeyChar;
                    return;
                }

                HandleDO1ScanAction(_do1BarcodeBuffer);

                _do1BarcodeBuffer = "";
            }
            catch (Exception ex)
            {
                log.Error("Method: DO1Pnl_KeyPress() Error: " + ex.Message); MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: DO1Pnl_KeyPress()");
            }
        }

        public void DL1Pnl_KeyPress(Object sender, KeyPressEventArgs e)
        {
            log.Info("Start Method: DL1Pnl_KeyPress() No parameter");

            try
            {
                if (e.KeyChar != (char)Keys.Return)
                {
                    _dl1BarcodeBuffer += e.KeyChar;
                    return;
                }

                HandleDL1SubmitAction();

                _dl1BarcodeBuffer = "";
            }
            catch (Exception ex)
            {
                log.Error("Method: DL1Pnl_KeyPress() Error: " + ex.Message); MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: DL1Pnl_KeyPress()");
            }
        }

        private void HandleDO1ScanAction(string barcode)
        {
            if ((_deliveryOrderInstance.IsDelivered.HasValue && _deliveryOrderInstance.IsDelivered.Value) || (_deliveryOrderInstance.isCanceled.HasValue && _deliveryOrderInstance.isCanceled.Value))
            {
                return;
            }

            var allItem = _entities.Item.ToList();
            var scannedItem = allItem.FirstOrDefault(i => i.Barcode == barcode);

            if (scannedItem == null)
            {
                MessageBox.Show("บาร์โค้ดที่แสกนไม่ถูกต้อง / ไม่พบสินค้าที่ตรงกับในระบบ");
                return;
            }

            if (_deliveryOrderInstance.ItemCategoryID != scannedItem.ItemCategoryID)
            {
                MessageBox.Show("บาร์โค้ดที่แสกนไม่ตรงกับสินค้าในหมวดที่จะส่ง");
                return;
            }

            if (_deliveryOrderList.Any(tr => tr.ItemID == scannedItem.ID))
            {
                MessageBox.Show("สินค้านี้ถูกแสกนไปแล้ว");
                return;
            }

            _deliveryOrderList.Add(new DeliveryOrderTable()
            {
                LotNo = scannedItem.LotNo,
                RowNo = (_deliveryOrderList.Count + 1).ToString(),
                GrossWeight = scannedItem.Measure ?? 0,
                ItemID = scannedItem.ID
            });

            dO1SubmitBtn.Enabled = true;
            dO1SendToB1Btn.Enabled = false;
            DoRecalculateWeight();
        }

        private void DelArrTabPage_Enter(object sender, EventArgs e)
        {
        }

        private void DL1CreateDelBtn_Click(object sender, EventArgs e)
        {
            log.Info("Start Method: dL1CreateDelBtn_Click() No parameter");

            _firstDgvLoad = true;
            _isCreatingDeliveryArrange = true;
            _deliveryArrangeInstance = new DeliveryArrange();
            var openCustomerList = soList.Select(so => so.Customer_Name).Distinct().ToList();
            dL2CustomerCbb.Items.Clear();

            dL2CustomerCbb.Items.Add(new ComboBoxItem()
            {
                Text = "   ----   กรุณาเลือก   ----   ",
                Value = null
            });

            foreach (var customer in openCustomerList)
            {
                dL2CustomerCbb.Items.Add(new ComboBoxItem()
                {
                    Text = customer,
                    Value = customer
                });
            }

            dL2CustomerCbb.SelectedIndex = 0;

            //dL2PackageTypeCbb.DataSource = _packageList;
            dL2SaleOrderCbb.Items.Clear();
            dL2ItemCodeCbb.Items.Clear();
            dL2DueDateTxb.Text = "";
            dL2CustomerCbb.SetEnable();
            dL2CreateDelBtn.Enabled = false;
            dL3PrintFormBtn.Enabled = false;
            dL2SaleOrderCbb.SetDisable();
            dL2ItemCodeCbb.SetDisable();
            dL2DueDateTxb.SetReadOnly();
            dL2ItemDeliveryCountTxb.SetNormal();
            dL2ItemDeliveryMeasureTxb.SetNormal();
            dL2ItemDeliveryMeasureLbsTxb.SetNormal();
            //dL2PackageTypeCbb.SetEnable();
            dL2AddItemBtn.Enabled = false;
            dL2SubmitBtn.Enabled = false;

            _deliveryArrangeList = new BindingList<DeliveryArrangeTable>();
            dL2DeliveryDgv.Columns.Clear();

            DLShowPanel(ref dL2Pnl);
            //delArrTabPage.Controls.Add(dL2Pnl);

            WriteLog(_deliveryArrangeInstance?.ID, "DA", "Load", "ไปหน้าสร้างใบจัดสินค้า");
            log.Info("End Method: dL1CreateDelBtn_Click()");
        }

        private void DL2AddItemBtn_Click(object sender, EventArgs e)
        {
            log.Info("Start Method: DL2AddItemBtn_Click() No parameter");

            try
            {
                if (string.IsNullOrEmpty(dL2ItemDeliveryCountTxb.Text) ||
                    string.IsNullOrEmpty(dL2ItemDeliveryMeasureTxb.Text))
                {
                    MessageBox.Show("กรุณากรอกจำนวนและปริมาณสินค้าที่จะส่ง");
                    return;
                }

                var selectedItem = _entities.Item
                    .FirstOrDefault(i => i.ItemCategory.ItemCode == soRmDetail.ItemCode);
                var selectedCategory = _entities.ItemCategory.FirstOrDefault(ic => ic.ItemCode == soRmDetail.ItemCode);

                _deliveryArrangeList.Add(new DeliveryArrangeTable()
                {
                    RowNo = (_deliveryArrangeList.Count + 1).ToString(),
                    ItemCode = soRmDetail.ItemCode,
                    Customer = soRmDetail.Customer_Name,
                    Description = soRmDetail.ItemName,
                    Count = Convert.ToDecimal(dL2ItemDeliveryCountTxb.Text),
                    Measure = Convert.ToDecimal(dL2ItemDeliveryMeasureTxb.Text),
                    SaleOrder = soRmDetail.DocNum.ToString(),
                    SuggestedLot = soRmDetail.LotSuggest,
                    BinLocation = selectedItem?.BinLoc ?? "",
                    Whse = soRmDetail.WhsCode,
                    UOM_Count = selectedCategory?.ItemType?.UOM_Count ?? "",
                    UOM_Weight = selectedCategory?.ItemType?.UOM_Measure ?? "",
                    Contact = soRmDetail.Cusmoter_ContractNo,
                    BillAddress = soRmDetail.ShipTo,
                    LineNum = soRmDetail.LineNum.ToString(),
                    LineItemWhse = soRmDetail.WhsCode
                    //PackageType = dL2PackageTypeCbb.SelectedItem.ToString()
                });


                if (_firstDgvLoad)
                {
                    dL2DeliveryDgv.DataSource = _deliveryArrangeList;
                    dL2DeliveryDgv.RowHeadersVisible = false;
                    dL2DeliveryDgv.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.DisplayedCells);
                    dL2DeliveryDgv.Columns["Route"].Visible = false;
                    dL2DeliveryDgv.Columns["RowNo"].ReadOnly = true;
                    dL2DeliveryDgv.Columns["Customer"].ReadOnly = true;
                    dL2DeliveryDgv.Columns["BillAddress"].ReadOnly = false;
                    dL2DeliveryDgv.Columns["SaleOrder"].ReadOnly = true;
                    dL2DeliveryDgv.Columns["ItemCode"].ReadOnly = true;
                    dL2DeliveryDgv.Columns["Description"].ReadOnly = true;
                    dL2DeliveryDgv.Columns["SuggestedLot"].ReadOnly = true;
                    dL2DeliveryDgv.Columns["Count"].ReadOnly = true;
                    dL2DeliveryDgv.Columns["UOM_Count"].ReadOnly = true;
                    dL2DeliveryDgv.Columns["Measure"].ReadOnly = true;
                    dL2DeliveryDgv.Columns["UOM_Weight"].ReadOnly = true;
                    dL2DeliveryDgv.Columns["Whse"].ReadOnly = true;
                    dL2DeliveryDgv.Columns["BinLocation"].ReadOnly = true;
                    dL2DeliveryDgv.Columns.Add(new DataGridViewButtonColumn()
                    {
                        Text = "X",
                        Name = "delete do button",
                        HeaderText = "",
                        UseColumnTextForButtonValue = true
                    });
                    dL2DeliveryDgv.Columns.Insert(0, new DataGridViewComboBoxColumn()
                    {
                        DataSource = _routeList,
                        HeaderText = "เส้นทาง",
                        DataPropertyName = "Route",
                    });
                    _firstDgvLoad = false;
                }


                dL2SubmitBtn.Enabled = true;
            }
            catch (Exception ex)
            {
                log.Error("Method: DL2AddItemBtn_Click() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                if (_deliveryArrangeList.Count > 0)
                {
                    WriteLog(_deliveryArrangeInstance?.ID, "DA", "Add DO list",
                        $"เพิ่มรายการที่จะส่ง Row no:{_deliveryArrangeList[_deliveryArrangeList.Count - 1]?.RowNo} SO no:{_deliveryArrangeList[_deliveryArrangeList.Count - 1]?.SaleOrder} Customer:{_deliveryArrangeList[_deliveryArrangeList.Count - 1]?.Customer} ItemCode:{_deliveryArrangeList[_deliveryArrangeList.Count - 1]?.ItemCode} Qty:{_deliveryArrangeList[_deliveryArrangeList.Count - 1]?.Measure}");

                }
                log.Info("End Method: DL2AddItemBtn_Click()");
            }
        }

        private void DL2CreateDelBtn_Click(object sender, EventArgs e)
        {
            log.Info("Start Method: dL2CreateDelBtn_Click() No parameter");
            try
            {
                InitializeDl3Panel();
            }
            catch (Exception ex)
            {
                log.Error("Method: dL2CreateDelBtn_Click() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                WriteLog(_deliveryOrderInstance?.ID, "DO", "CREATE P3",
                    $"สร้างใบจัดสินค้าจาก DA no:{_deliveryArrangeInstance?.DocNo} จำนวนDO list:{_deliveryArrangeList?.Count}");
                log.Info("End Method: dL2CreateDelBtn_Click()");
            }

            log.Info("End Method: dL2CreateDelBtn_Click()");
        }

        private void InitializeDl3Panel()
        {
            dL3DocNoTxb.Text = _deliveryArrangeInstance.DocNo;
            dL3AssignDateTxb.Text =
                _deliveryArrangeInstance.AssignDate.Value.ToString("d MMMM yyyy",
                    CultureInfo.CreateSpecificCulture("th-TH"));

            foreach (var item in _deliveryArrangeList)
            {
                var itemType = GetItemTypeByItemCode(item.ItemCode);

                if (itemType.Name.Trim() == "Finish Goods - WE")
                {
                    item.IsValid = true;
                }
                else
                {
                    var itemInWhse = GetItemOnHandSF1FromItemCode(item.ItemCode);

                    if (itemInWhse != null && itemInWhse.TotalQty > item.Measure)
                    {
                        item.IsValid = true;
                    }
                    else
                    {
                        item.IsValid = false;
                    }
                }
            }

            dL3DeliveryDgv.Columns.Clear();
            dL3DeliveryDgv.DataSource = _deliveryArrangeList;
            dL3DeliveryDgv.RowHeadersVisible = false;
            dL3DeliveryDgv.Columns.Insert(dL3DeliveryDgv.Columns.Count, new DataGridViewButtonColumn()
            {
                Text = "สร้างใบส่งสินค้า",
                Name = "new do button",
                HeaderText = "",
                UseColumnTextForButtonValue = true
            });

            RefreshDl3DeliveryDgv();
            //RefreshDl3DeliveryDgv();

            dL3DeliveryDgv.Columns["new do button"].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;

            DLShowPanel(ref dL3Pnl);
        }

        private void DL2CustomerCbb_SelectedIndexChanged(object sender, EventArgs e)
        {
            log.Info("Start Method: DL2CustomerCbb_SelectedIndexChanged() No parameter");

            try
            {
                if (((ComboBoxItem)dL2CustomerCbb.SelectedItem).Value == null) return;

                _dl2CustomerName = ((ComboBoxItem)dL2CustomerCbb.SelectedItem).Value.ToString();
                var openSaleOrderList = soList.Where(so => so.Customer_Name == _dl2CustomerName).Select(so => so.DocNum)
                    .Distinct().ToList();
                dL2SaleOrderCbb.Items.Clear();
                dL2SaleOrderCbb.Items.Add(new ComboBoxItem()
                {
                    Text = "   ----   กรุณาเลือก   ----   ",
                    Value = null
                });

                foreach (var saleOrder in openSaleOrderList)
                {
                    dL2SaleOrderCbb.Items.Add(new ComboBoxItem()
                    {
                        Text = saleOrder.ToString(),
                        Value = saleOrder
                    });
                }

                dL2ItemCodeCbb.Items.Clear();
                dL2DueDateTxb.Text = "";
                if (dL2CustomerCbb.Enabled)
                {
                    dL2SaleOrderCbb.SetEnable();
                    dL2ItemCodeCbb.SetDisable();
                    dL2DueDateTxb.SetReadOnly();
                }
                else
                {
                    dL2SaleOrderCbb.SetDisable();
                    dL2ItemCodeCbb.SetDisable();
                    dL2DueDateTxb.SetReadOnly();
                }

                dL2AddItemBtn.Enabled = false;
                dL2SaleOrderCbb.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                log.Error("Method: DL2CustomerCbb_SelectedIndexChanged() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: DL2CustomerCbb_SelectedIndexChanged()");
            }
        }

        private void DL2ItemCodeCbb_SelectedIndexChanged(object sender, EventArgs e)
        {
            log.Info("Start Method: DL2ItemCodeCbb_SelectedIndexChanged() No parameter");

            try
            {
                if (((ComboBoxItem)dL2ItemCodeCbb.SelectedItem).Value == null) return;

                _dl2ItemCode = ((ComboBoxItem)dL2ItemCodeCbb.SelectedItem).Text;
                var lineNum = ((ComboBoxItem)dL2ItemCodeCbb.SelectedItem).Value.ToString();

                var deliveryArrangeDeliveryOrder =
                    _deliveryArrangeInstance.DeliveryArrangeDeliveryOrder.FirstOrDefault(dado =>
                        dado.DeliveryOrder.SORef == _dl2SaleOrder);
                var deliveryOrder = deliveryArrangeDeliveryOrder?.DeliveryOrder;

                soRmDetail = soList.FirstOrDefault(so =>
                    so.DocNum.ToString() == _dl2SaleOrder && so.Customer_Name == _dl2CustomerName &&
                    so.ItemCode == _dl2ItemCode && so.LineNum == Convert.ToInt32(lineNum));
                if (soRmDetail != null)
                {
                    _dl2DueDate = soRmDetail.DocDueDate;

                    dL2ItemStockCountTxb.Text = $"{soRmDetail.Item_AllOnHand ?? 0:N2}";
                    dL2ItemDeliveryCountTxb.Text = "";
                    dL2ItemStockMeasureTxb.Text = $"{ConvertLbsToKgs(soRmDetail.Item_AllOnHand ?? 0):N2}";
                    dL2ItemDeliveryMeasureTxb.Text = $"{ConvertLbsToKgs(soRmDetail.Order_Qty ?? 0):N2}";
                    dL2ItemStockMeasureLbsTxb.Text = $"{soRmDetail.Item_AllOnHand ?? 0:N2}";
                    dL2ItemDeliveryMeasureLbsTxb.Text = $"{soRmDetail.Order_Qty ?? 0:N2}";
                    if (string.IsNullOrEmpty(deliveryOrder?.PackageType))
                    {
                        //dL2PackageTypeCbb.SelectedIndex = -1;
                    }
                    else
                    {
                        //dL2PackageTypeCbb.SelectedItem = deliveryOrder?.PackageType;
                    }


                    if (IsItemFGW(_dl2ItemCode))
                    {
                        dL2ItemStockMeasureTxb.Text = $"{soRmDetail.Item_AllOnHand ?? 0:N2}";
                        dL2ItemDeliveryMeasureLbsLbl.Visible = false;
                        dL2ItemDeliveryMeasureLbsTxb.Visible = false;
                        metroLabel188.Visible = false;
                    }
                    else
                    {
                        dL2ItemDeliveryMeasureLbsLbl.Visible = true;
                        dL2ItemDeliveryMeasureLbsTxb.Visible = true;
                        metroLabel188.Visible = true;
                    }
                }

                var countUnit = GetThaiUOMCountFromItemCode(_dl2ItemCode);
                var measureUnit = GetThaiUOMMeasureFromItemCode(_dl2ItemCode);

                dL2ItemDeliveryCountUnitLbl.Text = countUnit;
                dL2ItemDeliveryMeasureUnitLbl.Text = measureUnit;
                dL2ItemStockCountUnitLbl.Text = countUnit;
                dL2ItemStockMeasureUnitLbl.Text = measureUnit;

                dL2DueDateTxb.Text = _dl2DueDate.ToString("d MMMM yyyy", CultureInfo.CreateSpecificCulture("th-TH"));
                if (dL2ItemCodeCbb.Enabled)
                {
                    dL2DueDateTxb.SetReadOnly();
                    dL2SaleOrderCbb.SetEnable();
                    dL2ItemCodeCbb.SetEnable();
                    dL2AddItemBtn.Enabled = true;
                }
                else
                {
                    dL2DueDateTxb.SetReadOnly();
                    dL2SaleOrderCbb.SetDisable();
                    dL2ItemCodeCbb.SetDisable();
                    dL2AddItemBtn.Enabled = false;
                }

            }
            catch (Exception ex)
            {
                log.Error("Method: DL2ItemCodeCbb_SelectedIndexChanged() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: DL2ItemCodeCbb_SelectedIndexChanged()");
            }
        }

        private void DL2ItemDeliveryCountTxb_KeyPress(object sender, KeyPressEventArgs e)
        {
            EnforceDecimalKeyPress(e);
        }

        private void DL2ItemDeliveryMeasureLbsTxb_KeyPress(object sender, KeyPressEventArgs e)
        {
            EnforceDecimalKeyPress(e);
        }

        private void DL2ItemDeliveryMeasureLbsTxb_KeyUp(object sender, KeyEventArgs e)
        {
            var standardWeightLbs = GetStandardItemWeightPerUnitByItemCode(_dl2ItemCode);
            decimal inputWeightLbs = 0;
            if (decimal.TryParse(dL2ItemDeliveryMeasureLbsTxb.Text, out inputWeightLbs) && standardWeightLbs > 0)
            {
                dL2ItemDeliveryCountTxb.Text = $"{Math.Ceiling(inputWeightLbs / standardWeightLbs):N0}";
                dL2ItemDeliveryMeasureTxb.Text = $@"{inputWeightLbs:N2}";
            }
        }

        private void DL2ItemDeliveryMeasureTxb_KeyPress(object sender, KeyPressEventArgs e)
        {
            EnforceDecimalKeyPress(e);
        }

        private void DL2ItemDeliveryMeasureTxb_KeyUp(object sender, KeyEventArgs e)
        {
            var standardWeightLbs = GetStandardItemWeightPerUnitByItemCode(_dl2ItemCode);
            if (IsItemFGW(_dl2ItemCode))
            {
                decimal inputLength = 0;
                if (decimal.TryParse(dL2ItemDeliveryMeasureTxb.Text, out inputLength) && standardWeightLbs > 0)
                {
                    dL2ItemDeliveryCountTxb.Text = $"{Math.Ceiling(inputLength / standardWeightLbs):N0}";
                }
            }
            else
            {
                decimal inputWeight = 0;
                if (decimal.TryParse(dL2ItemDeliveryMeasureTxb.Text, out inputWeight) && standardWeightLbs > 0)
                {
                    var inputWeightKgs = ConvertKgsToLbs(inputWeight);
                    var standardWeightKgs = ConvertLbsToKgs(standardWeightLbs);
                    dL2ItemDeliveryCountTxb.Text = $"{Math.Ceiling(inputWeightKgs / standardWeightKgs):N0}";
                    dL2ItemDeliveryMeasureLbsTxb.Text = $@"{inputWeightKgs:N2}";
                }
            }

        }

        private void DL2SaleOrderCbb_SelectedIndexChanged(object sender, EventArgs e)
        {
            log.Info("Start Method: DL2SaleOrderCbb_SelectedIndexChanged() No parameter");

            try
            {
                if (((ComboBoxItem)dL2SaleOrderCbb.SelectedItem).Value == null) return;

                _dl2SaleOrder = ((ComboBoxItem)dL2SaleOrderCbb.SelectedItem).Value.ToString();
                var openItemCode = soList
                    .Where(so => so.DocNum.ToString() == _dl2SaleOrder && so.Customer_Name == _dl2CustomerName)
                    .ToList();

                dL2ItemCodeCbb.Items.Clear();
                dL2ItemCodeCbb.Items.Add(new ComboBoxItem()
                {
                    Text = "   ----   กรุณาเลือก   ----   ",
                    Value = null
                });

                foreach (var item in openItemCode)
                {
                    dL2ItemCodeCbb.Items.Add(new ComboBoxItem()
                    {
                        Text = item.ItemCode,
                        Value = item.LineNum
                    });
                }

                if (dL2SaleOrderCbb.Enabled)
                {
                    dL2SaleOrderCbb.SetEnable();
                    dL2ItemCodeCbb.SetEnable();
                }
                else
                {
                    dL2SaleOrderCbb.SetDisable();
                    dL2ItemCodeCbb.SetDisable();
                }

                dL2AddItemBtn.Enabled = false;
                dL2ItemCodeCbb.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                log.Error("Method: DL2SaleOrderCbb_SelectedIndexChanged() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: DL2SaleOrderCbb_SelectedIndexChanged()");
            }
        }

        private void DL2SubmitBtn_Click(object sender, EventArgs e)
        {
            log.Info("Start Method: DL2SubmitBtn_Click() No parameter");

            try
            {
                var newDeliveryArrange = new DeliveryArrange();
                var docFormat = _entities.DocumentSetup.FirstOrDefault(ds => ds.DocType == "Delivery Arrange")
                    .DocFormat;
                var todayDocCount = _entities.DeliveryArrange.Count(gl =>
                    gl.CreationDate.Value.Year == DateTime.Now.Year &&
                    gl.CreationDate.Value.Month == DateTime.Now.Month && gl.CreationDate.Value.Day == DateTime.Now.Day);

                if (_deliveryArrangeInstance == null || string.IsNullOrEmpty(_deliveryArrangeInstance.RefNo))
                {
                    newDeliveryArrange = _entities.DeliveryArrange.Add(new DeliveryArrange()
                    {
                        AssignDate = dL2ShipDateDpk.Value,
                        Barcode = "TRD-" + ReplaceFormatText(docFormat, "", "", "", "", "", "") + $"-{todayDocCount + 1:D3}",
                        CreationDate = DateTime.Now,
                        DocNo = "TRD-" + ReplaceFormatText(docFormat, "", "", "", "", "", "") + $"-{todayDocCount + 1:D3}",
                        DueDate = Convert.ToDateTime(dL2DueDateTxb.Text, CultureInfo.CreateSpecificCulture("th-TH")),
                        RefNo = soRmDetail.DocNum.ToString()
                    });

                    _entities.SaveChanges();

                    _deliveryArrangeInstance = newDeliveryArrange;

                    foreach (var item in _deliveryArrangeList)
                    {
                        var newDeliveryOrder = _entities.DeliveryOrder.Add(new DeliveryOrder()
                        {
                            CreationDate = DateTime.Now,
                            ContactNo = item.Contact,
                            CustomerName = item.Customer,
                            BillAddress = item.BillAddress,
                            DocNo = "",
                            ItemCategory = FindCategoryByItemCode(item.ItemCode),
                            QtyCount = item.Count,
                            QtyMeasure = item.Measure,
                            Route = item.Route,
                            SORef = item.SaleOrder,
                            Whse = item.Whse,
                            SuggestedLot = item.SuggestedLot,
                            BinLoc = item.BinLocation,
                            PackageType = item.PackageType,
                        });

                        _entities.DeliveryArrangeDeliveryOrder.Add(new DeliveryArrangeDeliveryOrder()
                        {
                            DeliveryArrange = _deliveryArrangeInstance,
                            DeliveryOrder = newDeliveryOrder
                        });
                        _entities.SaveChanges();
                        item.DeliveryOrderID = newDeliveryOrder.ID;
                    }
                }
                else
                {
                    foreach (var item in _deliveryArrangeList)
                    {
                        var currentDeliveryOrder =
                            _entities.DeliveryOrder.FirstOrDefault(dl => dl.ID == item.DeliveryOrderID);

                        currentDeliveryOrder.CreationDate = DateTime.Now;
                        currentDeliveryOrder.ContactNo = item.Contact;
                        currentDeliveryOrder.CustomerName = item.Customer;
                        currentDeliveryOrder.BillAddress = item.BillAddress;
                        currentDeliveryOrder.DocNo = "";
                        currentDeliveryOrder.ItemCategory = FindCategoryByItemCode(item.ItemCode);
                        currentDeliveryOrder.QtyCount = item.Count;
                        currentDeliveryOrder.QtyMeasure = item.Measure;
                        currentDeliveryOrder.Route = item.Route;
                        currentDeliveryOrder.SORef = item.SaleOrder;
                        currentDeliveryOrder.Whse = item.Whse;
                        currentDeliveryOrder.SuggestedLot = item.SuggestedLot;
                        currentDeliveryOrder.BinLoc = item.BinLocation;
                        currentDeliveryOrder.PackageType = item.PackageType;
                    }

                    _deliveryArrangeInstance.AssignDate = dL2ShipDateDpk.Value;
                    _entities.SaveChanges();

                    //var dldoList = _entities.DeliveryArrangeDeliveryOrder
                    //    .Where(dldo => dldo.DeliveryArrangeID == newDeliveryArrange.ID).ToList();
                    //_entities.DeliveryArrangeDeliveryOrder.RemoveRange(dldoList);

                    //var doList = _entities.DeliveryOrder.ToList();
                    //var dosToDelete = dldoList.Select(dldo => doList.FirstOrDefault(d => d.ID == dldo.DeliveryOrderID)).ToList();
                    //var deliveryOrderId = dosToDelete.FirstOrDefault().ID;

                    //_entities.DeliveryOrder.RemoveRange(dosToDelete);

                    //var doiList = _entities.DeliveryOrderItem.ToList();
                    //var doisToDelete = doiList.Where(doi => doi.DeliveryOrderID == deliveryOrderId).ToList();
                    //_entities.DeliveryOrderItem.RemoveRange(doisToDelete);
                    //_entities.SaveChanges();
                }

                _entities.SaveChanges();
                MessageBox.Show("บันทึกเรียบร้อย");
                dL2CreateDelBtn.Enabled = true;
                dL3PrintFormBtn.Enabled = true;
            }
            catch (Exception ex)
            {
                log.Error("Method: DL2SubmitBtn_Click() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                WriteLog(_deliveryOrderInstance?.ID, "DA", "SAVE",
                    $"บันทึกรายการที่จะส่ง DA no:{_deliveryArrangeInstance?.DocNo} จำนวนDO list:{_deliveryArrangeList?.Count}");

                log.Info("End Method: DL2SubmitBtn_Click()");
            }
        }

        private void DL3DeliveryDgv_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            log.Info("Start Method: DL3DeliveryDgv_CellClick() No parameter");
            try
            {
                if (dL3DeliveryDgv.Columns[e.ColumnIndex] is DataGridViewButtonColumn button && button.Name == "new do button")
                {
                    if (!(dL3DeliveryDgv.Rows[e.RowIndex].DataBoundItem is DeliveryArrangeTable deliveryArrangeTable) || !deliveryArrangeTable.IsValid)
                    {
                        return;
                    }

                    var deliveryOrder =
                        _entities.DeliveryOrder.FirstOrDefault(dlo => dlo.ID == deliveryArrangeTable.DeliveryOrderID);

                    if (deliveryOrder?.IsDelivered != null && deliveryOrder.IsDelivered.Value)
                    {
                        dO1PrintFormBtn.Enabled = true;
                        dO1SubmitBtn.Enabled = false;
                        dO1SendToB1Btn.Enabled = true;
                        dO1TerminateDoBtn.Enabled = true;
                        dO1MessageLbl.Text = "";
                    }
                    else
                    {
                        dO1PrintFormBtn.Enabled = false;
                        dO1SubmitBtn.Enabled = true;
                        dO1SendToB1Btn.Enabled = false;
                        dO1TerminateDoBtn.Enabled = false;
                        dO1MessageLbl.Text = "";
                    }

                    if (deliveryOrder?.isSentToB1 != null && deliveryOrder.isSentToB1.Value)
                    {
                        dO1PrintFormBtn.Enabled = true;
                        dO1SubmitBtn.Enabled = false;
                        dO1SendToB1Btn.Enabled = false;
                        dO1TerminateDoBtn.Enabled = true;
                        dO1MessageLbl.Text = "";
                    }

                    if (deliveryOrder?.isCanceled != null && deliveryOrder.isCanceled.Value)
                    {
                        dO1PrintFormBtn.Enabled = false;
                        dO1SubmitBtn.Enabled = false;
                        dO1SendToB1Btn.Enabled = false;
                        dO1TerminateDoBtn.Enabled = false;
                        dO1MessageLbl.Text = "ใบจัดส่งนี้ถูกยกเลิกแล้ว";
                    }

                    dO1RefNoTxb.SetReadOnly();

                    InitializeDeliveryOrder(e);
                }
            }
            catch (Exception ex)
            {
                log.Error("Method: DL3DeliveryDgv_CellClick() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                WriteLog(_deliveryOrderInstance?.ID, "DO", "CREATE",
                    $"สร้างใบจัดสินค้าเพื่อลูกค้าของ DO no:{_deliveryOrderInstance?.DocNo} ลูกค้า:{_deliveryOrderInstance?.CustomerName} รหัสสินค้า:{_deliveryOrderInstance?.ItemCategory.ItemCode} Sale order:{_deliveryOrderInstance?.SORef} DA no:{_deliveryArrangeInstance?.ID}");
                log.Info("End Method: DL3DeliveryDgv_CellClick()");
            }
        }

        private void DL3PrintFormBtn_Click(object sender, EventArgs e)
        {
            log.Info("Start Method: DL3PrintFormBtn_Click() No parameter");
            try
            {
                var totalRowCount = _deliveryArrangeList.Count;

                var worksheet = _formWorkbook.Worksheets[2];

                var tempWorkBook = new Workbook();
                tempWorkBook.Version = ExcelVersion.Version2010;
                tempWorkBook.Worksheets.Clear();
                var currentSheet = tempWorkBook.Worksheets.AddCopy(worksheet);

                var barcodeImage = currentSheet.Pictures.Add(1, 11, GetBarcodeImage(_deliveryArrangeInstance.DocNo));
                currentSheet.Range["L3"].Value = _deliveryArrangeInstance.AssignDate.HasValue
                    ? _deliveryArrangeInstance.AssignDate.Value.ToString("d MMMM yyyy",
                        CultureInfo.CreateSpecificCulture("th-TH"))
                    : "ไม่มีข้อมูล";
                currentSheet.Range["L4"].Value = _deliveryArrangeInstance.RefNo;
                currentSheet.Range["D7"].Value = _deliveryArrangeInstance.DueDate.HasValue
                    ? _deliveryArrangeInstance.AssignDate.Value.ToString("d MMMM yyyy",
                        CultureInfo.CreateSpecificCulture("th-TH"))
                    : "ไม่มีข้อมูล";
                currentSheet.Range["D10"].Value = _deliveryArrangeInstance.Remark ?? "";
                currentSheet.Range["M27"].Value = _deliveryArrangeInstance.PrintCount == null ? "พิมพ์ครั้งที่ 1" : "พิมพ์ครั้งที่ " + (_deliveryArrangeInstance.PrintCount.Value);

                barcodeImage.LeftColumnOffset = 150;

                for (int i = 3; i < totalRowCount; i++)
                {
                    currentSheet.InsertRow(15, 2, InsertOptionsType.FormatAsBefore);
                    currentSheet.Copy(worksheet.Range["A14:M14"], worksheet.Range["A15:M15"], true);
                }

                var itemList = _entities.Item.ToList();

                var rowOffset = 13;
                var itemCount = 1;
                var maxRowNumber = 62;

                var sortedList = _deliveryArrangeList.OrderBy(da => da.Route).ThenBy(da => da.Customer);

                foreach (var item in sortedList)
                {
                    currentSheet.Range["A" + rowOffset].Value = item.Route;
                    //currentSheet.Range["B" + rowOffset].Value = "" + itemCount++;
                    currentSheet.Range["C" + rowOffset].Value = item.Customer;
                    currentSheet.Range["D" + rowOffset].Value = item.BillAddress;
                    currentSheet.Range["E" + rowOffset].Value = item.SaleOrder;
                    currentSheet.Pictures.Add(rowOffset, 6, GetBarcodeImage120W(item.ItemCode));
                    currentSheet.Range["G" + rowOffset].Value = item.Description;
                    currentSheet.Range["H" + rowOffset].Value = $"{item.Count:N2}";
                    currentSheet.Range["I" + rowOffset].Value = item.UOM_Count;
                    currentSheet.Range["J" + rowOffset].Value = $"{item.Measure:N2}";
                    currentSheet.Range["K" + rowOffset].Value = item.UOM_Weight;
                    currentSheet.Range["L" + rowOffset].Value = item.Whse;
                    currentSheet.Range["M" + rowOffset].Value = item.BinLocation;
                    //currentSheet.Rows[rowOffset].AutoFitRows();
                    rowOffset++;
                }

                if (_printEnable)
                {
                    PrintExcelByInterop(tempWorkBook, _formPrinterName);
                }
                else
                {
                    SaveFileExcel(tempWorkBook, "Delivery Arrange Form");
                }

                if (_deliveryArrangeInstance.PrintCount == null)
                {
                    _deliveryArrangeInstance.PrintCount = 2;
                }
                else
                {
                    _deliveryArrangeInstance.PrintCount++;
                }
            }
            catch (Exception ex)
            {
                log.Error("Method: DL3PrintFormBtn_Click() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                WriteLog(_deliveryOrderInstance?.ID, "DA", "SAVE",
                    $"พิมพ์แบบฟอร์มรายการจัดสินค้า DA no:{_deliveryArrangeInstance?.DocNo} จำนวนDO list:{_deliveryArrangeList?.Count}");
                log.Info("End Method: DL3PrintFormBtn_Click()");
            }

            log.Info("End Method: DL3PrintFormBtn_Click()");
        }

        private void DL3SubmitBtn_Click(object sender, EventArgs e)
        {
            log.Info("Start Method: DL3SubmitBtn_Click() No parameter");
            try
            {
                var newDeliveryArrange =
                    _entities.DeliveryArrange.FirstOrDefault(dr => dr.ID == _deliveryArrangeInstance.ID);

                _entities.SaveChanges();

                _deliveryArrangeInstance = newDeliveryArrange;
                MessageBox.Show("บันทึกเรียบร้อย");
            }
            catch (Exception ex)
            {
                log.Error("Method: DL3SubmitBtn_Click() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: DL3SubmitBtn_Click()");
            }

            log.Info("End Method: DL3SubmitBtn_Click()");
        }

        private void DLShowPanel(ref MetroPanel panel)
        {
            dL1Pnl.Hide();
            dL2Pnl.Hide();
            dL3Pnl.Hide();
            dO1Pnl.Hide();

            panel.Dock = DockStyle.Fill;
            panel.Show();
        }
        private void DO1ItemDgv_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            log.Info("Start Method: DO1ItemDgv_CellClick() No parameter");
            try
            {
                var button = dO1ItemDgv.Columns[e.ColumnIndex] as DataGridViewButtonColumn;
                if (button != null && button.Name == "delete button")
                {
                    var deliveryOrderTableItem = dO1ItemDgv.Rows[e.RowIndex].DataBoundItem as DeliveryOrderTable;

                    if (deliveryOrderTableItem != null)
                    {
                        WriteLog(_deliveryOrderInstance?.ID, "DO", "Delete scanned item"
                            , $"ลบสินค้า:{deliveryOrderTableItem?.ItemID} Row:{deliveryOrderTableItem?.RowNo} หมายเลข Lot:{deliveryOrderTableItem?.LotNo} Gross weight:{deliveryOrderTableItem?.GrossWeight}");
                    }

                    var toRemoveDoItem =
                        _deliveryOrderList.FirstOrDefault(dol => dol.ItemID == deliveryOrderTableItem.ItemID);
                    _deliveryOrderList.Remove(toRemoveDoItem);

                    dO1SubmitBtn.Enabled = true;
                    dO1SendToB1Btn.Enabled = false;
                    DoRecalculateWeight();
                }
            }
            catch (Exception ex)
            {
                log.Error("Method: DO1ItemDgv_CellClick() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: DO1ItemDgv_CellClick()");
            }
        }

        private void DO1MagicRandomItemBtn_Click(object sender, EventArgs e)
        {
            log.Info("Start Method: DO1MagicRandomItemBtn_Click() No parameter");
            try
            {
                if ((_deliveryOrderInstance.IsDelivered.HasValue && _deliveryOrderInstance.IsDelivered.Value) || (_deliveryOrderInstance.isCanceled.HasValue && _deliveryOrderInstance.isCanceled.Value))
                {
                    return;
                }

                var allItem = _entities.Item.ToList();
                var someItem = allItem.Where(i => i.ItemCategory.ID == _deliveryOrderInstance.ItemCategory.ID && !i.Whse.EndsWith(SoldWhse) && !i.Whse.EndsWith(InTranWhse) && !i.Whse.EndsWith(IssuedWhse) && !i.Whse.EndsWith(ReceiveWhse)).Skip(_deliveryOrderList.Count).Take(1).FirstOrDefault();

                if (someItem == null)
                {
                    MessageBox.Show("ไม่พบสินค้าที่แสกนในระบบ");
                    return;
                }
                _deliveryOrderList.Add(new DeliveryOrderTable()
                {
                    LotNo = someItem.LotNo,
                    RowNo = (_deliveryOrderList.Count + 1).ToString(),
                    GrossWeight = someItem.Measure ?? 0,
                    ItemID = someItem.ID
                });

                dO1SubmitBtn.Enabled = true;
                dO1SendToB1Btn.Enabled = false;
                DoRecalculateWeight();
            }
            catch (Exception ex)
            {
                log.Error("Method: DO1MagicRandomItemBtn_Click() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: DO1MagicRandomItemBtn_Click()");
            }
        }

        private void DO1PrintFormBtn_Click(object sender, EventArgs e)
        {
            log.Info("Start Method: DO1PrintFormBtn_Click() No parameter");
            try
            {
                var worksheet = _formWorkbook.Worksheets[3];

                var tempWorkBook = new Workbook();
                tempWorkBook.Version = ExcelVersion.Version2010;
                tempWorkBook.Worksheets.Clear();
                var currentSheet = tempWorkBook.Worksheets.AddCopy(worksheet);

                InitDeliveryOrderHeaderExcel(currentSheet);

                var colOffset = 1;
                var rowOffset = 13;
                var itemCount = 1;
                var sheetCount = 0;
                var maxRowNumber = 37;

                foreach (var item in _deliveryOrderList)
                {
                    if (rowOffset > maxRowNumber)
                    {
                        if (colOffset < 10)
                        {
                            colOffset += 3;
                            rowOffset = 13;
                        }
                        else
                        {
                            sheetCount++;
                            currentSheet = tempWorkBook.Worksheets.AddCopy(worksheet);
                            InitDeliveryOrderHeaderExcel(currentSheet);
                            colOffset = 1;
                            rowOffset = 13;
                        }
                    }

                    currentSheet.Range[rowOffset, colOffset].Value = "" + itemCount++;
                    currentSheet.Range[rowOffset, colOffset + 1].Value = $"{item.GrossWeight:N2}";

                    rowOffset++;
                }

                if (_printEnable)
                {
                    PrintExcelByInterop(tempWorkBook, _formPrinterName);
                }
                else
                {
                    SaveFileExcel(tempWorkBook, "Delivery Order Form");
                }

                if (_deliveryOrderInstance.PrintCount == null)
                {
                    _deliveryOrderInstance.PrintCount = 2;
                }
                else
                {
                    _deliveryOrderInstance.PrintCount++;
                }
            }
            catch (Exception ex)
            {
                log.Error("Method: DO1PrintFormBtn_Click() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                WriteLog(_deliveryOrderInstance?.ID, "DO", "Print form"
                    , $"พิมพ์แบบฟอร์มของ DO No:{_deliveryOrderInstance?.DocNo} ลูกค้า:{_deliveryOrderInstance?.CustomerName} รหัสสินค้า:{_deliveryOrderInstance?.ItemCategory.ItemCode} SO no:{_deliveryOrderInstance?.SORef} Qty count:{_deliveryOrderInstance?.QtyCount} Qty measure:{_deliveryOrderInstance.QtyMeasure}");

                log.Info("End Method: DO1PrintFormBtn_Click()");
            }

            log.Info("End Method: DO1PrintFormBtn_Click()");
        }

        private void DO1SubmitBtn_Click(object sender, EventArgs e)
        {
            log.Info("Start Method: DO1SubmitBtn_Click() No parameter");
            try
            {
                Cursor.Current = Cursors.WaitCursor;


                foreach (var item in _deliveryOrderList)
                {
                    var currentItem = GetItemByID(item.ItemID);

                    if (!_entities.DeliveryOrderItem.Any(di =>
                        di.DeliveryOrderID == _deliveryOrderInstance.ID && di.ItemID == item.ItemID))
                    {
                        _entities.DeliveryOrderItem.Add(new DeliveryOrderItem()
                        {
                            Item = currentItem,
                            DeliveryOrder = _deliveryOrderInstance
                        });

                        currentItem.Whse = currentItem.Whse.Contains(SoldWhse) ? currentItem.Whse : currentItem.Whse + SoldWhse;
                    }
                }

                _entities.SaveChanges();

                dO1SubmitBtn.Enabled = false;
                dO1PrintFormBtn.Enabled = true;
                dO1SendToB1Btn.Enabled = true;
                dO1TerminateDoBtn.Enabled = true;
                _deliveryOrderInstance.IsDelivered = true;
                _deliveryOrderIsSaved = true;
                MessageBox.Show("บันทึกเรียบร้อย");
            }
            catch (Exception ex)
            {
                log.Error("Method: DO1SubmitBtn_Click() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: DO1SubmitBtn_Click()");
                Cursor.Current = Cursors.Default;
            }
        }

        private List<B1_DeliveryInterface> GetGroupedResultDeliveryItemList()
        {
            var deliveryInterfaceList = new List<B1_DeliveryInterface>();

            foreach (var item in _deliveryOrderList)
            {
                var currentItem = GetItemByID(item.ItemID);
                var saleOrders = soList.Where(so => so.DocNum.ToString() == _deliveryOrderInstance.SORef).ToList();
                var saleOrder = saleOrders.FirstOrDefault();
                var lineNum =
                    _deliveryArrangeList.FirstOrDefault(da => _deliveryOrderInstance != null && da.DeliveryOrderID == _deliveryOrderInstance.ID).LineNum;
                var lineItemWhse = _deliveryArrangeList.FirstOrDefault(da =>
                    _deliveryOrderInstance != null && da.DeliveryOrderID == _deliveryOrderInstance.ID).LineItemWhse;

                deliveryInterfaceList.Add(new B1_DeliveryInterface()
                {
                    LotNo = currentItem.ItemType.isItemizable ?? false ? item.LotNo.Substring(0, item.LotNo.Length - 5) : item.LotNo,
                    ItemCode = currentItem.ItemCategory.ItemCode,
                    ContractNo = _deliveryOrderInstance.ContactNo,
                    CustomerCode = saleOrder.Customer_Code,
                    DeliveryDocNum = saleOrder.DocNum.ToString(),
                    DueDate = saleOrder.DocDueDate,
                    ItemLotFromWhseCode = currentItem.Whse,
                    LineNo = Convert.ToInt32(lineNum),
                    NetPerLot = (currentItem.Measure ?? 0) - ((currentItem.MiscPackageWeight ?? 0) + (currentItem.OuterPackageWeight ?? 0) + (currentItem.InnerPackageWeight ?? 0)),
                    SoDocEntry = saleOrder.DocEntry
                });
            }

            var groupedDeliveryOrderItemList = deliveryInterfaceList.GroupBy(dli => dli.LotNo);

            var resultDeliveryOrderItemList = groupedDeliveryOrderItemList.Select(gdoi => new B1_DeliveryInterface()
            {
                LotNo = gdoi.FirstOrDefault().LotNo,
                ItemCode = gdoi.FirstOrDefault().ItemCode,
                ContractNo = gdoi.FirstOrDefault().ContractNo,
                CustomerCode = gdoi.FirstOrDefault().CustomerCode,
                DeliveryDocNum = gdoi.FirstOrDefault().DeliveryDocNum,
                DueDate = gdoi.FirstOrDefault().DueDate,
                ItemLotFromWhseCode = gdoi.FirstOrDefault().ItemLotFromWhseCode,
                LineNo = gdoi.FirstOrDefault().LineNo,
                NetPerLot = gdoi.Sum(d => d.NetPerLot),
                SoDocEntry = gdoi.FirstOrDefault().SoDocEntry
            }).ToList();

            return resultDeliveryOrderItemList;
        }

        private bool Interface_DeliveryExcelTemplate(List<B1_DeliveryInterface> groupDelivery)
        {
            log.Info("Start Method: Interface_DeliveryExcelTemplate()");

            try
            {
                //AOM
                // gen B1 header template
                var docEntry = "1";
                var docDate = DateTime.Today.ToString("yyyyMMdd", CultureInfo.CreateSpecificCulture("en-US"));
                var docDueDate = groupDelivery[0].DueDate.ToString("yyyyMMdd", CultureInfo.CreateSpecificCulture("en-US")); //_deliveryArrangeInstance.DueDate == null ? "" : ((DateTime)_deliveryArrangeInstance.DueDate).ToString("yyyyMMdd", CultureInfo.CreateSpecificCulture("en-US"));
                var custometCode = groupDelivery[0].CustomerCode; //soList.Where(s => s.Customer_Name == _deliveryOrderInstance.CustomerName && s.DocNum.ToString() == _deliveryOrderInstance.SORef).FirstOrDefault().Customer_Code;
                var contractNo = groupDelivery[0].ContractNo; //_deliveryOrderInstance.ContactNo;
                var ref2 = "Delivery DocNo: " + groupDelivery[0].DeliveryDocNum; //_deliveryOrderInstance.DocNo; //BC ref

                var _interfaceWorkbook_Header = new Workbook();
                _interfaceWorkbook_Header.LoadFromFile(
                    ConfigurationManager.AppSettings["Delivery_HeaderCsvTemplate"],
                    ",", 1, 1);
                var worksheetHd = _interfaceWorkbook_Header.Worksheets[0];

                worksheetHd.Range["A3"].Text = docEntry;
                worksheetHd.Range["E3"].Text = docDate;
                worksheetHd.Range["F3"].Text = docDueDate;
                worksheetHd.Range["G3"].Text = custometCode;
                worksheetHd.Range["J3"].Text = contractNo;
                worksheetHd.Range["P3"].Text = ref2; //comment

                var tempWorkBookHd = new Workbook();
                tempWorkBookHd.Version = ExcelVersion.Version2010;
                tempWorkBookHd.Worksheets.Clear();
                tempWorkBookHd.Worksheets.AddCopy(worksheetHd);

                string headerPath = SaveFileCSV(tempWorkBookHd, "Header_Delivery");

                //B1 line & Batch
                int i = 3; //start at row 3
                int j = 0;

                var _interfaceWorkbook_Line = new Workbook();
                _interfaceWorkbook_Line.LoadFromFile(
                    ConfigurationManager.AppSettings["Delivery_LineCsvTemplate"],
                    ",", 1, 1);
                var worksheetLn = _interfaceWorkbook_Line.Worksheets[0];

                var _interfaceWorkbook_Batch = new Workbook();
                _interfaceWorkbook_Batch.LoadFromFile(
                    ConfigurationManager.AppSettings["Delivery_BatchCsvTemplate"],
                    ",", 1, 1);
                var worksheetBt = _interfaceWorkbook_Batch.Worksheets[0];

                foreach (B1_DeliveryInterface dv in groupDelivery)
                {
                    string parentKey = "1"; //always 1
                    string lineNum = j.ToString(); //start at 0
                    string itemCode = dv.ItemCode; //_deliveryOrderInstance.ItemCategory.ItemCode;
                    string lineQuantity = ConvertKgsToLbs(dv.NetPerLot).ToString(); //Lbs
                    string fromWh = dv.ItemLotFromWhseCode; //_deliveryOrderInstance.Whse;
                    string baseType = "17";
                    string baseEntry = dv.SoDocEntry.ToString();
                    string baseLine = dv.LineNo == null ? "0" : dv.LineNo.ToString();

                    worksheetLn.Range["A" + i.ToString()].Text = parentKey;
                    worksheetLn.Range["B" + i.ToString()].Text = lineNum;
                    worksheetLn.Range["C" + i.ToString()].Text = itemCode;
                    worksheetLn.Range["E" + i.ToString()].Text = lineQuantity;
                    worksheetLn.Range["N" + i.ToString()].Text = fromWh;
                    worksheetLn.Range["AO" + i.ToString()].Text = baseType;
                    worksheetLn.Range["AP" + i.ToString()].Text = baseEntry;
                    worksheetLn.Range["AQ" + i.ToString()].Text = baseLine;

                    string batchDocNum = "1";
                    string distNumber = dv.LotNo;
                    string batchQuantity = lineQuantity;
                    string baseLineNumber = lineNum;

                    worksheetBt.Range["A" + i.ToString()].Text = batchDocNum;
                    worksheetBt.Range["C" + i.ToString()].Text = distNumber;
                    worksheetBt.Range["K" + i.ToString()].Text = batchQuantity;
                    worksheetBt.Range["L" + i.ToString()].Text = baseLineNumber;

                    i++;
                    j++;

                    WriteLog(_deliveryOrderInstance?.ID, "DO", "Save"
                        , $"บันทึกข้อมูลใบส่งสินค้า DO no:{dv.DeliveryDocNum} ลูกค้า:{dv.CustomerCode} รหัสสินค้า:{dv.ItemCode} Lot no:{dv.LotNo} จำนวน:{lineQuantity} จากโกดัง:{fromWh}");

                }

                //B1 Line
                var tempWorkBookLn = new Workbook();
                tempWorkBookLn.Version = ExcelVersion.Version2010;
                tempWorkBookLn.Worksheets.Clear();
                tempWorkBookLn.Worksheets.AddCopy(worksheetLn);

                string linePath = SaveFileCSV(tempWorkBookLn, "Line_Delivery");

                //B1 Batch
                var tempWorkBookBt = new Workbook();
                tempWorkBookBt.Version = ExcelVersion.Version2010;
                tempWorkBookBt.Worksheets.Clear();
                tempWorkBookBt.Worksheets.AddCopy(worksheetBt);

                string batchPath = SaveFileCSV(tempWorkBookBt, "Batch_Delivery");

                //Copy XML to new one and also change template paths in XML
                bool isTransfer = false;
                string templatePath = ConfigurationManager.AppSettings["Delivery_XmlTemplate"];
                string xmlOutputPath = ConfigurationManager.AppSettings["InterfaceOutputPath"] + "DeliveryXml_" + DateTime.Now.ToString("dd-MM-yy-hhmmss") + ".xml"; ;
                CreateXMLInterface(isTransfer, templatePath, headerPath, linePath, batchPath, xmlOutputPath);

                //Call .bat
                string batName = "DeliveryBat_" + DateTime.Now.ToString("dd-MM-yy-hhmmss");
                CreateBatFile(batName, xmlOutputPath);

                //Detact error log from DTW
                var result = GetInterfaceResult(batName);

                return result;
            }
            catch (Exception ex)
            {
                log.Error("Method: Interface_DeliveryExcelTemplate() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
                return false;
            }
            finally
            {
                log.Info("End Method: Interface_DeliveryExcelTemplate()");
            }
        }

        private void DoRecalculateWeight()
        {
            log.Info("Start Method: DoRecalculateWeight() No parameter");
            try
            {
                var itemList = _deliveryOrderList.Select(dlo => GetItemByID(dlo.ItemID)).ToList();
                var totalCount = _deliveryOrderList.Count;
                var totalWeightKgs = itemList.Sum(i => i.Measure);
                var totalTareKgs = itemList.Sum(i => i.InnerPackageWeight) + itemList.Sum(i => i.OuterPackageWeight) +
                                   itemList.Sum(i => i.MiscPackageWeight);
                var totalNetWeightKgs = totalWeightKgs - totalTareKgs;
                var totalWeightLbs = ConvertKgsToLbs(totalNetWeightKgs.Value);

                dO1QtyCountTxb.Text = $"{totalCount:N}";
                dO1QtyWeightTxb.Text = $"{totalWeightKgs:N2}";
                dO1WeightLbsTxb.Text = $"{totalWeightLbs:N2}";
                dO1NetWeightTxb.Text = $"{totalNetWeightKgs:N2}";
                dO1TareTxb.Text = $"{totalTareKgs:N2}";

                dO1SubmitBtn.Enabled = _deliveryOrderList.Count > 0 && !_deliveryOrderIsSaved;
            }
            catch (Exception ex)
            {
                log.Error("Method: DoRecalculateWeight() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: DoRecalculateWeight()");
            }
        }

        private B1_ItemInWhse GetItemOnHandSF1FromItemCode(string itemCode)
        {
            log.Info(string.Format(@"Start Method: GetItemOnHandSF1FromItemCode() Parameter:itemCode Value:'{0}'", itemCode));

            try
            {
                if (string.IsNullOrEmpty(itemCode))
                {
                    return null;
                }

                var result = _b1Entities.Database.SqlQuery<B1_ItemInWhse>(
                    string.Format(@"
                                SELECT[ItemCode], [WhsCode],SUM([Quantity]) AS TotalQty
                                FROM OIBT
                                WHERE ItemCode = '{0}'
                                    GROUP BY[ItemCode],[WhsCode]
                                HAVING WhsCode = 'SF1'
                                ", itemCode)).FirstOrDefault();

                return result;
            }
            catch (Exception ex)
            {
                log.Error("Method: GetItemOnHandSF1FromItemCode() Error: " + ex.Message); MessageBox.Show("Error: " + ex.Message);
                return null;
            }
            finally
            {
                log.Info("End Method: GetItemOnHandSF1FromItemCode()");
            }
        }

        private List<B1_Delivery> GetSaleOrderDetail(string soNo, string itemCode)
        {
            log.Info(string.Format(@"Start Method: GetSaleOrderDetail() Parameter:soNo Value:'{0}' Parameter:itemCode Value:'{1}'", soNo, itemCode));

            try
            {
                string whereStr = "";

                if (soNo != "" && itemCode != "")
                {
                    whereStr = string.Format(@" and T0.[DocNum] = '{0}' and T1.[ItemCode] = '{1}'", soNo, itemCode);
                }

                var result = _b1Entities.Database.SqlQuery<B1_Delivery>(
                    string.Format(@"
                                    SELECT T0.[DocEntry]
                                    , T0.[DocNum]
                                    , T0.[DocType]
                                    , T0.[DocStatus]
                                    , T0.[CreateDate]
                                    , T0.[DocDueDate]
                                    , T0.[CardCode] AS 'Customer_Code'
                                    , T0.[CardName] AS 'Customer_Name'
                                    , T0.[NumAtCard] AS 'Cusmoter_ContractNo'
                                    , T0.[Address] AS 'BillTo'
                                    , T0.[Address2] AS 'ShipTo'
                                    , T1.[LineNum]
                                    , T1.[ItemCode]
                                    , T2.[ItemName]
                                    , T1.[ItemType]
                                    , T1.[WhsCode]
                                    , T1.[ShipDate]
                                    , T1.[Quantity] AS 'Order_Qty'
                                    , T1.[OpenQty]
                                    , T2.[OnHand] AS 'Item_AllOnHand'
                                    , T3.OnHand AS 'Item_InWhOnHand'
                                    , IIF (T1.[ItemType] = 4,
		                                    (Select top (1) a.BatchNum
			                                    from (
			                                    SELECT Ta.[ItemCode],Ta.[InDate]
			                                    , Ta.[BatchNum], Ta.[WhsCode], Ta.[ItemName], Ta.[SuppSerial], Ta.[Quantity]
			                                    FROM OIBT Ta
			                                    WHERE Ta.[Quantity] > 0 and Ta.[ItemCode] = T1.[ItemCode] and Ta.[WhsCode] = T1.[WhsCode]
			                                    ) a
			                                    ORDER BY a.[InDate], a.[Quantity]  ASC)
	                                    ,NULL )	AS 'LotSuggest'
                                    , IIF (T1.[ItemType] = 4,
		                                    (Select top (1) a.[Quantity]
			                                    from (
			                                    SELECT Ta.[ItemCode],Ta.[InDate]
			                                    , Ta.[BatchNum], Ta.[WhsCode], Ta.[ItemName], Ta.[SuppSerial], Ta.[Quantity]
			                                    FROM OIBT Ta
			                                    WHERE Ta.[Quantity] > 0 and Ta.[ItemCode] = T1.[ItemCode] and Ta.[WhsCode] = T1.[WhsCode]
			                                    ) a
			                                    ORDER BY a.[InDate], a.[Quantity]  ASC)
	                                    ,NULL )	AS 'LotSuggest_Qty'
                                    FROM [dbo].[ORDR]  T0
                                    INNER JOIN [dbo].[RDR1]  T1 ON T0.[DocEntry] = T1.[DocEntry]
                                    LEFT JOIN [dbo].[OITM]  T2 ON T1.[ItemCode] = T2.[ItemCode]
                                    LEFT JOIN OITW T3 ON T1.ItemCode = T3.ItemCode and T1.WhsCode = T3.WhsCode
                                    WHERE T0.[DocStatus]  = 'O'
                                    and ISNULL(T1.[OpenQty],0) > 0
                                    and T0.[DocDueDate] >= GETDATE()-10
                                    {0}
                                ", whereStr)).ToList();

                return result;
            }
            catch (Exception ex)
            {
                log.Error("Method: GetSaleOrderDetail() Error: " + ex.Message); MessageBox.Show("Error: " + ex.Message);
                return null;
            }
            finally
            {
                log.Info("End Method: GetSaleOrderDetail()");
            }
        }

        private void InitDeliveryOrderHeaderExcel(Worksheet currentSheet)
        {
            var itemList = _deliveryOrderList.Select(dlo => GetItemByID(dlo.ItemID)).ToList();
            var isFGW = IsItemFGW(itemList.FirstOrDefault().ItemCategory.ItemCode);
            var totalCount = _deliveryOrderList.Count;
            var totalWeightKgs = itemList.Sum(i => i.Measure);
            var totalTareKgs = itemList.Sum(i => i.InnerPackageWeight) + itemList.Sum(i => i.OuterPackageWeight) +
                               itemList.Sum(i => i.MiscPackageWeight);
            var totalNetWeightKgs = totalWeightKgs - totalTareKgs;
            var totalNetWeightLbs = ConvertKgsToLbs(totalNetWeightKgs.Value);
            var totalAmountInPkg = itemList.Sum(i => i.Count);
            var totalOutTare = itemList.Sum(i => i.OuterPackageWeight);
            var totalInTare = itemList.Sum(i => i.InnerPackageWeight);

            currentSheet.Pictures.Add(1, 10, GetBarcodeImage(_deliveryOrderInstance.DocNo));
            currentSheet.Range["J3"].Text = _deliveryOrderInstance.CreationDate.HasValue
                ? _deliveryOrderInstance.CreationDate.Value.ToString("d MMMM yyyy",
                    CultureInfo.CreateSpecificCulture("th-TH"))
                : "ไม่มีข้อมูล";
            currentSheet.Range["J4"].Value = _deliveryOrderInstance.SORef;
            currentSheet.Range["C7"].Value = _deliveryOrderInstance.CustomerName;
            currentSheet.Range["C8"].Value = _deliveryOrderInstance.BillAddress;
            currentSheet.Range["C9"].Value = _deliveryOrderInstance.ItemCategory.Item.FirstOrDefault()?.LotNo.Substring(0, _deliveryOrderInstance.ItemCategory.Item.FirstOrDefault().LotNo.Length - 5);
            currentSheet.Pictures.Add(7, 10, GetBarcodeImage(_deliveryOrderInstance.ItemCategory.ItemCode));
            currentSheet.Range["J9"].Value = _deliveryOrderInstance.ItemCategory.Description;
            currentSheet.Range["J10"].Value = _deliveryOrderInstance.ContactNo;

            if (isFGW)
            {
                currentSheet.Range["B12"].Value = "YDs";
                currentSheet.Range["E12"].Value = "YDs";
                currentSheet.Range["H12"].Value = "YDs";
                currentSheet.Range["K12"].Value = "YDs";
                currentSheet.Range["D39"].Value = "พับ";
                currentSheet.Range["A40"].Value = "ความยาวรวม";
                currentSheet.Range["D40"].Value = "YDs";
                currentSheet.Range["C39"].Value = $"{totalCount:N}";
                currentSheet.Range["C40"].Value = $"{totalWeightKgs ?? 0:N2}";
                currentSheet.Range["F39"].Value = "";
                currentSheet.Range["H39"].Value = "";
                currentSheet.Range["F40"].Value = "";
                currentSheet.Range["H40"].Value = "";
                currentSheet.Range["I40"].Value = "";
                currentSheet.Range["K40"].Value = "";
                currentSheet.Range["A41"].Value = "";
                currentSheet.Range["C41"].Value = "";
                currentSheet.Range["D41"].Value = "";
                currentSheet.Range["E41"].Value = "";
                currentSheet.Range["F41"].Value = "";
                currentSheet.Range["F39"].Style.Borders.LineStyle = LineStyleType.None;
                currentSheet.Range["H39"].Style.Borders.LineStyle = LineStyleType.None;
                currentSheet.Range["F40"].Style.Borders.LineStyle = LineStyleType.None;
                currentSheet.Range["H40"].Style.Borders.LineStyle = LineStyleType.None;
                currentSheet.Range["I40"].Style.Borders.LineStyle = LineStyleType.None;
                currentSheet.Range["K40"].Style.Borders.LineStyle = LineStyleType.None;
                currentSheet.Range["A41"].Style.Borders.LineStyle = LineStyleType.None;
                currentSheet.Range["C41"].Style.Borders.LineStyle = LineStyleType.None;
                currentSheet.Range["D41"].Style.Borders.LineStyle = LineStyleType.None;
                currentSheet.Range["E41"].Style.Borders.LineStyle = LineStyleType.None;
                currentSheet.Range["F41"].Style.Borders.LineStyle = LineStyleType.None;
            }
            else
            {
                currentSheet.Range["C39"].Value = $"{totalCount:N0}"; //count
                currentSheet.Range["C40"].Value = $"{totalWeightKgs ?? 0:N2}"; //gross in kgs
                currentSheet.Range["C41"].Value = $"{totalNetWeightKgs ?? 0:N2}"; //net in kgs
                currentSheet.Range["E41"].Value = $"{totalNetWeightLbs:N2}"; //net in lbs
                currentSheet.Range["H39"].Value = $"{totalAmountInPkg ?? 0:N0}"; //amount in pkg
                currentSheet.Range["H40"].Value = $"{totalOutTare ?? 0:N2}"; //out tare
                currentSheet.Range["K40"].Value = $"{totalInTare ?? 0:N2}"; //in tare
                currentSheet.Range["K55"].Value = _deliveryOrderInstance.PrintCount == null ? "พิมพ์ครั้งที่ 1" : "พิมพ์ครั้งที่ " + (_deliveryOrderInstance.PrintCount.Value);
            }
        }

        private void InitializeDeliveryOrder(DataGridViewCellEventArgs e)
        {
            log.Info("Start Method: DL3DeliveryDgv_CellClick() No parameter");
            try
            {
                var deliveryArrangeTableItem = dL3DeliveryDgv.Rows[e.RowIndex].DataBoundItem as DeliveryArrangeTable;
                var deliverOrder =
                    _entities.DeliveryOrder.FirstOrDefault(dlo => dlo.ID == deliveryArrangeTableItem.DeliveryOrderID);
                _deliveryOrderInstance = deliverOrder;
                var docFormat = _entities.DocumentSetup.FirstOrDefault(ds => ds.DocType == "Delivery Order").DocFormat;
                var todayDocCount = _entities.DeliveryOrder.Count(dl =>
                    DbFunctions.TruncateTime(dl.CreationDate.Value) == DbFunctions.TruncateTime(DateTime.Today) && !string.IsNullOrEmpty(dl.DocNo));
                //                var todayDocCount = _entities.DeliveryOrder.Count(gl =>
                //gl.CreationDate.Value.Year == DateTime.Now.Year && gl.CreationDate.Value.Month == DateTime.Now.Month &&
                //    gl.CreationDate.Value.Day == DateTime.Now.Day && !string.IsNullOrEmpty(gl.DocNo));
                var test = _entities.DeliveryOrder.Where(gl =>
                    gl.CreationDate.Value.Year == DateTime.Now.Year && gl.CreationDate.Value.Month == DateTime.Now.Month &&
                    gl.CreationDate.Value.Day == DateTime.Now.Day && !string.IsNullOrEmpty(gl.DocNo)).ToList();
                _deliveryOrderList = new BindingList<DeliveryOrderTable>();
                _deliveryOrderIsSaved = _deliveryOrderInstance.IsDelivered ?? false;
                if (string.IsNullOrEmpty(_deliveryOrderInstance.DocNo))
                {
                    _deliveryOrderInstance.DocNo =
                        "DO-" + ReplaceFormatText(docFormat, "", "", "", "", "", "") + $"-{todayDocCount + 1:D3}";
                }

                if (_deliveryOrderInstance.isCanceled.HasValue && _deliveryOrderInstance.isCanceled.Value)
                {
                    _deliveryOrderIsSaved = true;
                }

                foreach (var item in _deliveryOrderInstance.DeliveryOrderItem)
                {
                    var deliveryOrder =
                        _entities.DeliveryOrder.FirstOrDefault(dl => dl.DocNo == _deliveryOrderInstance.DocNo);
                    _deliveryOrderList.Add(new DeliveryOrderTable()
                    {
                        LotNo = item.Item.LotNo,
                        RowNo = (_deliveryOrderList.Count + 1).ToString(),
                        GrossWeight = item.Item.Measure ?? 0,
                        ItemID = item.Item.ID
                    });
                }
                dO1ItemDgv.Columns.Clear();
                dO1ItemDgv.DataSource = _deliveryOrderList;
                if (_deliveryOrderList.Count == 0)
                {
                    dO1ItemDgv.Columns.Insert(dO1ItemDgv.Columns.Count, new DataGridViewButtonColumn()
                    {
                        Text = "X",
                        Name = "delete button",
                        HeaderText = "",
                        UseColumnTextForButtonValue = true
                    });
                }

                if (_deliveryOrderIsSaved)
                {
                    dO1SubmitBtn.Enabled = false;
                }



                dO1DocNoTxb.Text = _deliveryOrderInstance.DocNo;
                dO1ItemCodeTxb.Text = _deliveryOrderInstance.ItemCategory.ItemCode;
                dO1AssignDateTxb.Text = DateTime.Now.ToString("d MMMM yyyy", CultureInfo.CreateSpecificCulture("th-TH"));
                dO1AddressTxb.Text = _deliveryOrderInstance.BillAddress;
                dO1ItemDescTxb.Text = _deliveryOrderInstance.ItemCategory.Description;
                dO1RefNoTxb.Text = _deliveryOrderInstance.SORef;
                dO1RemarkTxb.Text = _deliveryOrderInstance.Remark;
                dO1TelephoneTxb.Text = _deliveryOrderInstance.ContactNo;
                dO1CustomerTxb.Text = _deliveryOrderInstance.CustomerName;
                DoRecalculateWeight();

                var countUnit = GetThaiUOMCountFromItemCode(_deliveryOrderInstance.ItemCategory.ItemCode);
                var measureUnit = GetThaiUOMMeasureFromItemCode(_deliveryOrderInstance.ItemCategory.ItemCode);

                dO1QtyCountUnitLbl.Text = countUnit;
                dO1QtyWeightUnitLbl.Text = measureUnit;
                if (countUnit == "พับ")
                {
                    dO1QtyWeightLbl.Text = "ความยาวรวม";
                    dO1NetWeightLbl.Visible = false;
                    dO1NetWeightTxb.Visible = false;
                    dO1NetWeightUnitLbl.Visible = false;
                    dO1EqualLbl.Visible = false;
                    dO1WeightLbsTxb.Visible = false;
                    dO1WeightLbsUnitLbl.Visible = false;
                    dO1TareLbl.Visible = false;
                    dO1TareTxb.Visible = false;
                    dO1TareUnitLbl.Visible = false;
                }
                else
                {
                    dO1QtyWeightLbl.Text = "น.น.รวม";
                    dO1NetWeightLbl.Visible = true;
                    dO1NetWeightTxb.Visible = true;
                    dO1NetWeightUnitLbl.Visible = true;
                    dO1EqualLbl.Visible = true;
                    dO1WeightLbsTxb.Visible = true;
                    dO1WeightLbsUnitLbl.Visible = true;
                    dO1TareLbl.Visible = true;
                    dO1TareTxb.Visible = true;
                    dO1TareUnitLbl.Visible = true;
                }


                DLShowPanel(ref dO1Pnl);
            }
            catch (Exception ex)
            {
                log.Error("Method: DL3DeliveryDgv_CellClick() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: DL3DeliveryDgv_CellClick()");
            }
        }

        private void DL1SubmitBtn_Click(object sender, EventArgs e)
        {
            log.Info("Start Method: DL1SubmitBtn_Click() No parameter");

            try
            {
                _dl1BarcodeBuffer = "";
                HandleDL1SubmitAction();
            }
            catch (Exception ex)
            {
                log.Error("Method: DL1SubmitBtn_Click() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: DL1SubmitBtn_Click()");
            }
        }

        private void HandleDL1SubmitAction()
        {
            log.Info("Start Method: DL1SubmitBtn_Click() No parameter");

            try
            {
                _isCreatingDeliveryArrange = false;
                var delNo = dL1DelNoTxb.Text.Trim();

                if (delNo == "" && string.IsNullOrEmpty(_dl1BarcodeBuffer))
                {
                    MessageBox.Show("กรุณากรอกหมายเลขใบจัดของ");
                    return;
                }

                if (!string.IsNullOrEmpty(_dl1BarcodeBuffer))
                {
                    var docNo = _dl1BarcodeBuffer.Trim();
                    _deliveryArrangeInstance = _entities.DeliveryArrange.FirstOrDefault(dr => dr.DocNo == docNo);
                    if (_deliveryArrangeInstance == null)
                    {
                        //if (int.TryParse(docNo, out var docNoInt))
                        //{
                        //    var matchedSoList = soList.Where(so => so.DocNum == docNoInt).ToList();

                        //    if (matchedSoList.Count == 1)
                        //    {
                        //        dL1DelNoTxb.Text = "" + docNoInt;
                        //        dL1SubmitBtn.PerformClick();
                        //        DLShowPanel(ref dL2Pnl);
                        //        return;
                        //    }
                        //    else //retry
                        //    {
                        //        soList = GetSaleOrderDetail("", "");
                        //        matchedSoList = soList.Where(so => so.DocNum == docNoInt).ToList();

                        //        if (matchedSoList.Count == 1)
                        //        {
                        //            dL1DelNoTxb.Text = "" + docNoInt;
                        //            dL1SubmitBtn.PerformClick();
                        //            DLShowPanel(ref dL2Pnl);
                        //            return;
                        //        }
                        //    }
                        //}

                        MessageBox.Show("ไม่พบหมายเลขเอกสารที่แสกน");
                        return;
                    }
                    _dl1BarcodeBuffer = "";
                }
                else
                {
                    _deliveryArrangeInstance = _entities.DeliveryArrange.FirstOrDefault(dr => dr.DocNo == delNo);
                }

                if (_deliveryArrangeInstance != null)
                {
                    var deliveryOrderList = _entities.DeliveryArrangeDeliveryOrder
                        .Where(dado => dado.DeliveryArrangeID == _deliveryArrangeInstance.ID)
                        .Select(dado => dado.DeliveryOrder).ToList();


                    _deliveryArrangeList = new BindingList<DeliveryArrangeTable>();

                    foreach (var dlo in deliveryOrderList)
                    {
                        var soDetail = soList.FirstOrDefault(so =>
                                        so.DocNum.ToString() == dlo.SORef && so.Customer_Name == dlo.CustomerName &&
                                        so.ItemCode == dlo.ItemCategory.ItemCode);
                        _deliveryArrangeList.Add(new DeliveryArrangeTable()
                        {
                            RowNo = (_deliveryArrangeList.Count + 1).ToString(),
                            ItemCode = dlo.ItemCategory.ItemCode,
                            Customer = dlo.CustomerName,
                            Description = dlo.ItemCategory.Description,
                            Count = dlo.QtyCount ?? 0,
                            Measure = dlo.QtyMeasure ?? 0,
                            SaleOrder = dlo.SORef,
                            SuggestedLot = dlo.SuggestedLot,
                            BinLocation = dlo.BinLoc,
                            Whse = dlo.Whse,
                            UOM_Count = dlo.ItemCategory.ItemType.UOM_Count,
                            UOM_Weight = dlo.ItemCategory.ItemType.UOM_Measure,
                            Contact = dlo.ContactNo,
                            DeliveryOrderID = dlo.ID,
                            Route = dlo.Route,
                            BillAddress = dlo.BillAddress
                        });
                    }
                    dL2ShipDateDpk.Value = _deliveryArrangeInstance.AssignDate.HasValue ? _deliveryArrangeInstance.AssignDate.Value : DateTime.Today;
                }
                else
                {
                    MessageBox.Show("ไม่พบหมายเลขใบจัดสินค้าที่ระบุ");
                    return;
                }

                dL2CustomerCbb.SetDisable();
                dL2SaleOrderCbb.SetDisable();
                dL2ItemCodeCbb.SetDisable();
                dL2ItemDeliveryCountTxb.SetReadOnly();
                dL2ItemDeliveryMeasureTxb.SetReadOnly();
                dL2ItemDeliveryMeasureLbsTxb.SetReadOnly();
                //dL2PackageTypeCbb.SetDisable();
                dL2AddItemBtn.Enabled = false;
                dL2SubmitBtn.Enabled = false;
                dL2CreateDelBtn.Enabled = true;
                dL3PrintFormBtn.Enabled = true;
                dL2DeliveryDgv.Columns.Clear();
                dL2DeliveryDgv.DataSource = _deliveryArrangeList;
                dL2DeliveryDgv.RowHeadersVisible = false;
                dL2DeliveryDgv.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.DisplayedCells);
                dL2DeliveryDgv.Columns["Route"].Visible = false;
                dL2DeliveryDgv.Columns["RowNo"].ReadOnly = true;
                dL2DeliveryDgv.Columns["Customer"].ReadOnly = true;
                dL2DeliveryDgv.Columns["BillAddress"].ReadOnly = false;
                dL2DeliveryDgv.Columns["SaleOrder"].ReadOnly = true;
                dL2DeliveryDgv.Columns["ItemCode"].ReadOnly = true;
                dL2DeliveryDgv.Columns["Description"].ReadOnly = true;
                dL2DeliveryDgv.Columns["SuggestedLot"].ReadOnly = true;
                dL2DeliveryDgv.Columns["Count"].ReadOnly = true;
                dL2DeliveryDgv.Columns["UOM_Count"].ReadOnly = true;
                dL2DeliveryDgv.Columns["Measure"].ReadOnly = true;
                dL2DeliveryDgv.Columns["UOM_Weight"].ReadOnly = true;
                dL2DeliveryDgv.Columns["Whse"].ReadOnly = true;
                dL2DeliveryDgv.Columns["BinLocation"].ReadOnly = true;
                dL2DeliveryDgv.Columns.Insert(0, new DataGridViewComboBoxColumn()
                {
                    DataSource = _routeList,
                    HeaderText = "เส้นทาง",
                    DataPropertyName = "Route",
                    Name = "Route"
                });

                WriteLog(_deliveryArrangeInstance?.ID, "DA", "Load",
                    $"โหลดหน้า Delivery arrange ด้วยหมายเลขใบจัดสินค้า: {_deliveryArrangeInstance?.DocNo}");

                //dL3DeliveryDgv.DataSource = _deliveryArrangeList;
                DLShowPanel(ref dL2Pnl);
                //InitializeDl3Panel();
            }
            catch (Exception ex)
            {
                log.Error("Method: DL1SubmitBtn_Click() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: DL1SubmitBtn_Click()");
            }

            log.Info("End Method: DL1SubmitBtn_Click()");
        }

        private void DO1Pnl_MouseHover(object sender, EventArgs e)
        {
            //dO1Pnl.Focus();
        }

        private void DO1ItemDgv_MouseHover(object sender, EventArgs e)
        {
            //dO1Pnl.Focus();
        }

        private void DL3DeliveryDgv_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {

        }

        private void DL1Pnl_MouseHover(object sender, EventArgs e)
        {
            //dL1Pnl.Focus();
        }

        private void DL2DeliveryDgv_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            var comboBox = dL2DeliveryDgv.Rows[e.RowIndex].Cells[e.ColumnIndex] as DataGridViewComboBoxCell;
            if (comboBox != null)
            {
                var deliveryArrangeTableItem = dL2DeliveryDgv.Rows[e.RowIndex].DataBoundItem as DeliveryArrangeTable;
                if (deliveryArrangeTableItem != null && deliveryArrangeTableItem.Route != null)
                {
                    deliveryArrangeTableItem.Route = comboBox.Value.ToString();
                }
            }
        }

        private void DL2DeliveryDgv_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var comboBox = dL2DeliveryDgv.Rows[e.RowIndex].Cells["Route"] as DataGridViewComboBoxCell;
            if (comboBox != null)
            {
                var deliveryArrangeTableItem = dL2DeliveryDgv.Rows[e.RowIndex].DataBoundItem as DeliveryArrangeTable;
                if (comboBox.Value == null && !string.IsNullOrEmpty(deliveryArrangeTableItem.Route))
                {
                    comboBox.Value = deliveryArrangeTableItem.Route;
                }
                if (string.IsNullOrEmpty(deliveryArrangeTableItem.Route) && comboBox.Value != null)
                {
                    deliveryArrangeTableItem.Route = comboBox.Value.ToString();
                }
            }
        }

        private void DL2DeliveryDgv_SelectionChanged(object sender, EventArgs e)
        {
            if (_isCreatingDeliveryArrange) return;
            if (dL2DeliveryDgv.CurrentRow == null) return;
            var deliveryArrangeTableItem = dL2DeliveryDgv.CurrentRow.DataBoundItem as DeliveryArrangeTable;

            dL2SaleOrderCbb.Items.Clear();
            dL2CustomerCbb.Items.Clear();
            dL2ItemCodeCbb.Items.Clear();
            var saleOrderCbbItem = new ComboBoxItem()
            {
                Text = deliveryArrangeTableItem.SaleOrder,
                Value = Convert.ToInt32(deliveryArrangeTableItem.SaleOrder)
            };
            var customerCbbItem = new ComboBoxItem()
            {
                Text = deliveryArrangeTableItem.Customer,
                Value = deliveryArrangeTableItem.Customer
            };
            var itemCodeCbbItem = new ComboBoxItem()
            {
                Text = deliveryArrangeTableItem.ItemCode,
                Value = deliveryArrangeTableItem.ItemCode
            };
            dL2SaleOrderCbb.Items.Add(saleOrderCbbItem);
            dL2CustomerCbb.Items.Add(customerCbbItem);
            dL2ItemCodeCbb.Items.Add(itemCodeCbbItem);

            _dl2SaleOrder = deliveryArrangeTableItem.SaleOrder;
            _dl2CustomerName = deliveryArrangeTableItem.Customer;
            _dl2ItemCode = deliveryArrangeTableItem.ItemCode;

            dL2CustomerCbb.SelectedItem = customerCbbItem;
            dL2SaleOrderCbb.SelectedIndex = dL2SaleOrderCbb.FindStringExact(saleOrderCbbItem.Text);
            dL2ItemCodeCbb.SelectedIndex = dL2ItemCodeCbb.FindStringExact(itemCodeCbbItem.Text);

            //_dl2SaleOrder = ((ComboBoxItem)dL2SaleOrderCbb.SelectedItem).Value.ToString();
            //_dl2CustomerName = ((ComboBoxItem)dL2CustomerCbb.SelectedItem).Value.ToString();
            //_dl2ItemCode = ((ComboBoxItem)dL2ItemCodeCbb.SelectedItem).Value.ToString();

            soRmDetail = soList.FirstOrDefault(so =>
                so.DocNum.ToString() == _dl2SaleOrder && so.Customer_Name == _dl2CustomerName &&
                so.ItemCode == _dl2ItemCode);

            dL2ItemStockCountTxb.Text = $"{soRmDetail.Item_AllOnHand ?? 0:N2}";
            dL2ItemDeliveryCountTxb.Text = $"{deliveryArrangeTableItem.Count:N0}";
            dL2ItemStockMeasureTxb.Text = $"{ConvertLbsToKgs(soRmDetail.Item_AllOnHand ?? 0):N2}";
            dL2ItemDeliveryMeasureTxb.Text = $"{ConvertLbsToKgs(soRmDetail.Order_Qty ?? 0):N2}";
            dL2ItemStockMeasureLbsTxb.Text = $"{soRmDetail.Item_AllOnHand ?? 0:N2}";
            dL2ItemDeliveryMeasureLbsTxb.Text = $"{soRmDetail.Order_Qty ?? 0:N2}";
            dL2DueDateTxb.Text = soRmDetail.DocDueDate.ToString("d MMMM yyyy", CultureInfo.CreateSpecificCulture("th-TH"));
        }

        private void DL2ItemDeliveryCountTxb_KeyUp(object sender, KeyEventArgs e)
        {
            var standardWeightLbs = GetStandardItemWeightPerUnitByItemCode(_dl2ItemCode);
            if (standardWeightLbs <= 0) return;
            var standardWeightKgs = ConvertLbsToKgs(standardWeightLbs);
            if (!int.TryParse(dL2ItemDeliveryCountTxb.Text, out var itemCount)) return;
            if (IsItemFGW(_dl2ItemCode))
            {
                dL2ItemDeliveryMeasureTxb.Text = $"{(itemCount * standardWeightLbs):N2}";
            }
            else
            {
                dL2ItemDeliveryMeasureTxb.Text = $"{(itemCount * standardWeightKgs):N2}";
                dL2ItemDeliveryMeasureLbsTxb.Text = $"{(itemCount * standardWeightLbs):N2}";
            }
        }

        private void dL2DeliveryDgv_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            log.Info("Start Method: dL2DeliveryDgv_CellClick() No parameter");
            try
            {
                var button = dL2DeliveryDgv.Columns[e.ColumnIndex] as DataGridViewButtonColumn;
                if (button != null && button.Name == "delete do button")
                {
                    var deliveryArrangeTable = dL2DeliveryDgv.Rows[e.RowIndex].DataBoundItem as DeliveryArrangeTable;

                    _deliveryArrangeList.Remove(deliveryArrangeTable);
                    dL2DeliveryDgv.Refresh();
                }

                if (_deliveryArrangeList.Count == 0)
                {
                    dL2SubmitBtn.Enabled = false;
                }
                else
                {
                    dL2SubmitBtn.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                log.Error("Method: dL2DeliveryDgv_CellClick() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                WriteLog(_deliveryOrderInstance?.ID, "DO", "CREATE",
                    $"สร้างใบจัดสินค้าเพื่อลูกค้าของ DO no:{_deliveryOrderInstance?.DocNo} ลูกค้า:{_deliveryOrderInstance?.CustomerName} รหัสสินค้า:{_deliveryOrderInstance?.ItemCategory.ItemCode} Sale order:{_deliveryOrderInstance?.SORef} DA no:{_deliveryArrangeInstance?.ID}");
                log.Info("End Method: dL2DeliveryDgv_CellClick()");
            }
        }

        private void dL1DelNoTxb_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if (e.KeyChar == (char)Keys.Return) dL1SubmitBtn.PerformClick();
        }
    }
}