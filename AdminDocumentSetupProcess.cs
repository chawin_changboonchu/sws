﻿using System;
using System.Linq;
using System.Windows.Forms;
using SWS.Model;

namespace SWS
{
    public partial class Inventory
    {
        private void AD1SelectBtn_Click(object sender, EventArgs e)
        {
            log.Info("Start Method: AD1SelectBtn_Click() No parameter");

            if (aD1OptionsLsb.SelectedItem != null)
            {
                aD1DocFormatTxb.Text += aD1OptionsLsb.SelectedItem.ToString();
                if (aD1DocTypeCbb.SelectedItem is DocumentSetup documentSetup)
                {
                    SyncExample(documentSetup.DocType);
                }
            }

            log.Info("End Method: AD1SelectBtn_Click()");
        }

        private void AD1ClearBtn_Click(object sender, EventArgs e)
        {
            log.Info("Start Method: AD1ClearBtn_Click() No parameter");

            aD1DocFormatTxb.Text = "";
            aD1ExampleTxb.Text = "";

            WriteLog(0, "Doc", "Setup", $"เคลียร์การตั้งค่า Doc");
            log.Info("End Method: AD1ClearBtn_Click()");
        }

        private void AD1DocTypeCbb_SelectedIndexChanged(object sender, EventArgs e)
        {
            log.Info("Start Method: AD1DocTypeCbb_SelectedIndexChanged() No parameter");

            if (aD1DocTypeCbb.SelectedItem is DocumentSetup documentSetup)
            {
                aD1DocFormatTxb.Text = documentSetup.DocFormat;
                SyncExample(documentSetup.DocType);
            }

            log.Info("End Method: AD1DocTypeCbb_SelectedIndexChanged()");
        }

        private void SyncExample(string docType)
        {
            string prefix = "";
            switch (docType)
            {
                case "Generate Lot":
                    prefix = "TRL";
                    break;
                case "Issue for Production":
                    prefix = "TRP";
                    break;
                case "Delivery Arrange":
                    prefix = "TRD";
                    break;
                case "Delivery Order":
                    prefix = "DO";
                    break;
                case "Transfer":
                    prefix = "TRF";
                    break;
            }

            aD1ExampleTxb.Text = prefix + "-" + ReplaceFormatTextExample(aD1DocFormatTxb.Text);
        }

        private void AD1SubmitBtn_Click(object sender, EventArgs e)
        {
            log.Info("Start Method: AD1SubmitBtn_Click() No parameter");
            try
            {
                var documentSetup = aD1DocTypeCbb.SelectedItem as DocumentSetup;
                _entities.DocumentSetup.FirstOrDefault(it => it.DocType == documentSetup.DocType).DocFormat =
                    aD1DocFormatTxb.Text;
                _entities.SaveChanges();

                WriteLog(0, "Doc", "Setup", $"เปลี่ยนการตั้งค่า Doc ของ {documentSetup?.DocType} เป็น {aD1DocFormatTxb.Text}");
                MessageBox.Show("บันทึกเสร็จสิ้น");
            }
            catch (Exception ex)
            {
                log.Error("Method: AD1SubmitBtn_Click() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: AD1SubmitBtn_Click()");
            }
        }
    }
}