﻿using MetroFramework.Forms;
using Spire.Xls;
using SWS.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Drawing;
using System.Drawing.Printing;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using MetroFramework.Controls;
using System.Data;

namespace SWS
{
    public partial class Inventory : MetroForm
    {
        private const string InTranWhse = "_ITR";
        private const string IssuedWhse = "_ISS";
        private const string SoldWhse = "_SLD";
        private const string ReceiveWhse = "_RCV";

        private const string genlotReport = "รายงานการโอน Lot วัตถุดิบ";
        private const string issueReport = "รายงานการเบิกวัตถุดิบ";
        private const string receiveReport = "รายงานการรับสินค้าผลิตเสร็จ";
        private const string transferReport = "รายงานการโอนถ่ายสินค้า";
        private const string deliveryReport = "รายงานการจัดส่งสินค้า";
        private const string stockReport = "รายงานจำนวนสินค้าคงคลัง";

        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        //General
        private SWSEntities _entities;
        private List<string> _factoryList;
        private List<string> _formulaSList;
        private List<string> _formulaWList;
        private Workbook _formWorkbook;
        private Worksheet _formWorksheet;
        private readonly string _barcodePrinterName;
        private readonly string _formPrinterName;
        private B1Entities _b1Entities;
        private List<B1_TransferReq> _b1TransferReqList;
        private Workbook _barcodeFgWorkbook;
        private Workbook _barcodeWorkbook;
        private Worksheet _barcodeWorksheet;
        private List<string> _binLocationList;
        private string _content;
        private User _currentUser;

        //Barcode Key buffer
        private string _gl1BarcodeBuffer;
        private string _ip1BarcodeBuffer;
        private string _ip3BarcodeBuffer;
        private string _dl1BarcodeBuffer;
        private string _do1BarcodeBuffer;
        private string _tr1BarcodeBuffer;
        private string _tr2BarcodeBuffer;

        //Gen Lot
        private GenLot _genLotInstance;
        private List<GenLotItemTable> _genLotList;
        private bool _isKgs;

        //Issue Production Tab
        private IssueProduction _issueProductionInstance;
        private List<IssueProductionRequest> _issueProductionRequestList;
        private JobReceive _jobReceiveInstance;
        private ItemType _jobReceiveItemType;
        private BindingList<JobReceiveTable> _jobReceiveList;
        private bool _leavingJobReceive;
        private bool _leavingJobIssue;
        private bool _exitFromButton;
        private List<B1_JobOrder> _operationList;
        private List<string> _packageList;
        private bool _printEnable;
        private List<string> _routeList;
        private BindingList<IssueProductionIssueTable> _scannedItemList;
        private List<IssueProductionIssueTable> _scannedItemDeleteList;
        private long _currentJobIssueID;
        private List<string> _shiftList;
        private bool _jobIssueIsSaved;
        private bool _jobIssueSentToB1;
        private List<string> _coneNoSpiralList;
        private List<string> _coneSpiralList;
        private decimal _plasticWeight;
        private decimal _coneWeight;

        //Delivery
        private DeliveryArrange _deliveryArrangeInstance;
        private BindingList<DeliveryArrangeTable> _deliveryArrangeList;
        private DeliveryOrder _deliveryOrderInstance;
        private BindingList<DeliveryOrderTable> _deliveryOrderList;
        private string _dl2CustomerName;
        private DateTime _dl2DueDate;
        private string _dl2ItemCode;
        private string _dl2SaleOrder;
        private bool _isCreatingDeliveryArrange;
        private bool _leavingDelivery;
        private bool _deliveryOrderIsSaved;
        private bool _firstDgvLoad;

        //Transfer
        private Transfer _transferInstance;
        private BindingList<TransferTable> _transferList;
        private List<string> _uomCountUnits;
        private List<string> _uomMeasureUnits;

        //Admin User Tab
        private BindingList<AdminUserUserTable> _userList;
        private List<string> _warehouseList;
        private List<string> _reportWarehouseList;
        private B1_Genlot grItemDetail;
        private List<B1_Genlot> grList;
        private List<B1_JobOrder> jobList;
        private B1_JobOrder jobRmDetail;
        private List<B1_Delivery> soList;
        private B1_Delivery soRmDetail;

        //RePrint
        private bool isreprint = false;

        public Inventory()
        {
            log.Info("Start Method: Inventory() No parameter");

            InitializeComponent();

            (this.iP3Pnl as Control).KeyPress += new KeyPressEventHandler(IP3Pnl_KeyPress);
            (this.gL1Pnl as Control).KeyPress += new KeyPressEventHandler(GL1Pnl_KeyPress);
            (this.tR2Pnl as Control).KeyPress += new KeyPressEventHandler(TR2Pnl_KeyPress);
            (this.dO1Pnl as Control).KeyPress += new KeyPressEventHandler(DO1Pnl_KeyPress);
            (this.iP1Pnl as Control).KeyPress += new KeyPressEventHandler(IP1Pnl_KeyPress);
            (this.dL1Pnl as Control).KeyPress += new KeyPressEventHandler(DL1Pnl_KeyPress);
            (this.tR1Pnl as Control).KeyPress += new KeyPressEventHandler(TR1Pnl_KeyPress);

            _entities = new SWSEntities();
            _b1Entities = new B1Entities();

            _barcodePrinterName = ConfigurationManager.AppSettings["BarcodePrinter"];
            _formPrinterName = ConfigurationManager.AppSettings["FormPrinter"];
            _printEnable = ConfigurationManager.AppSettings["SendToPrinter"] == "false" ? false : true;

            versionLbl.Text = "Ver. " + Assembly.GetExecutingAssembly().GetName().Version;

            _uomCountUnits = new List<string>
            {
                "",
                "Bale",
                "Fold",
                "Cone",
                "Tank",
                "Box",
                "Piece",
                "Bag",
                "Palette"
            };

            _uomMeasureUnits = new List<string>
            {
                "Kgs",
                "Yds"
            };

            _factoryList = new List<string>
            {
                "",
                "2",
                "4"
            };

            _formulaWList = new List<string>
            {
                "1",
                "2",
                "3",
                "4",
                "5",
                "6",
                "7",
                "8",
                "9",
                "10"
            };

            _formulaSList = new List<string>
            {
                "1",
                "2",
                "3",
                "4",
                "5",
                "6",
                "7",
                "8",
                "9"
            };

            _shiftList = new List<string>
            {
                "A",
                "B",
                "C"
            };

            _packageList = new List<string>
            {
                "ไม่ระบุ",
                "กระสอบแดง",
                "กระสอบเหลือง",
                "กล่อง",
                "พาเลท"
            };

            _warehouseList = new List<string>
            {
                "01",
                "PWS",
                "SF1",
                "SF2",
                "SF3",
                "SPD",
                "SPD2",
                "SPD4",
                "SPK",
                "SQC",
                "SRM",
                "SSP",
                "WFG",
                "WPD",
                "WRM",
                "WSP",
                "WSU",
                "WZC",
                "ZWC",
            };

            _reportWarehouseList = new List<string>
            {
                "ทั้งหมด",
                "PWS",
                "SF1",
                "SF2",
                "SF3",
                "SPD",
                "SPD2",
                "SPD4",
                "SPK",
                "SQC",
                "SRM",
                "SSP",
                "WFG",
                "WPD",
                "WRM",
                "WSP",
                "WSU",
                "WZC",
                "ZWC",
            };

            _binLocationList = new List<string>
            {
                "",
                "A",
                "B",
                "C",
                "D",
                "E",
                "F",
                "G",
                "H",
                "I",
                "J",
                "K",
                "L",
                "M",
                "N",
                "O",
                "P",
                "Q",
                "R",
                "S",
                "T",
                "U",
                "V",
                "W",
                "X",
                "Y",
                "Z"
            };

            _routeList = new List<string>
            {
                "1",
                "2",
                "3",
                "4",
                "5",
                "6",
                "7",
                "8",
                "9"
            };

            _coneNoSpiralList = new List<string>
            {
                "หลอด(พลาสติก)Local (5°)",
                "หลอด(กระดาษ)Export (5°)"
            };

            _coneSpiralList = new List<string>
            {
                "หลอด(พลาสติก)Local (3°)",
                "หลอด(กระดาษ)Export (3°)"
            };

            tR2FromWhseCbb.SelectedIndex = 2;
            tR2ToWhseCbb.SelectedIndex = 4;

            genLotTabPage.Controls.Add(gL1Pnl);
            gL1Pnl.Dock = DockStyle.Fill;
            gL1Pnl.Visible = true;
            gL2Pnl.Visible = false;

            issueTabPage.Controls.Add(iP1Pnl);
            iP1Pnl.Dock = DockStyle.Fill;
            iP1Pnl.Visible = true;
            iP2Pnl.Visible = false;
            iP3Pnl.Visible = false;

            transferTabPage.Controls.Add(tR1Pnl);
            tR1Pnl.Dock = DockStyle.Fill;
            tR1Pnl.Visible = true;
            tR2Pnl.Visible = false;

            delArrTabPage.Controls.Add(dL1Pnl);
            dL1Pnl.Dock = DockStyle.Fill;
            dL1Pnl.Visible = true;
            dL3Pnl.Visible = false;
            dL2Pnl.Visible = false;
            dO1Pnl.Visible = false;

            mainTabControl.Hide();
            lGFormPnl.Show();
            logoutLnk.Visible = false;

            log.Info("End Method: Inventory()");
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            log.Info("Start Method: Form1_Load() No parameter");

            _formWorkbook = new Workbook();
            _formWorkbook.LoadFromFile(ConfigurationManager.AppSettings["ExcelformTemplate"]);
            _barcodeWorkbook = new Workbook();
            _barcodeWorkbook.LoadFromFile(ConfigurationManager.AppSettings["RmBarcodeTemplate"]);
            _barcodeFgWorkbook = new Workbook();
            _barcodeFgWorkbook.LoadFromFile(ConfigurationManager.AppSettings["FgBarcodeTemplate"]);

            //Admin user tab
            aU1NewRoleCbb.DataSource = _entities.Role.ToList();
            aU1NewRoleCbb.DisplayMember = "Name";
            aU1NewRoleCbb.ValueMember = "ID";
            aU1UserListDgv.EditMode = DataGridViewEditMode.EditOnEnter;

            //Admin Lot Setup tab
            aL1LotTypeCbb.DataSource = _entities.ItemType.Where(it => !it.Name.StartsWith("unused")).ToList();
            aL1LotTypeCbb.DisplayMember = "Name";
            aL1LotTypeCbb.ValueMember = "ID";

            //Admin Doc Setup tab
            aD1DocTypeCbb.DataSource = _entities.DocumentSetup.ToList();
            aD1DocTypeCbb.DisplayMember = "DocType";
            aD1DocTypeCbb.ValueMember = "DocType";

            foreach (MetroTabPage tabpage in mainTabControl.TabPages)
            {
                var allPanels = tabpage.GetChildControls<MetroPanel>();
                foreach (MetroPanel panel in allPanels)
                {
                    var allTextBoxes = panel.GetChildControls<MetroTextBox>();
                    foreach (var txb in allTextBoxes)
                    {
                        if (txb.ReadOnly)
                        {
                            txb.SetReadOnly();
                        }
                        else
                        {
                            txb.SetNormal();
                        }
                    }

                    var allComboBoxes = panel.GetChildControls<MetroComboBox>();
                    foreach (var cbb in allComboBoxes)
                    {
                        if (cbb.Enabled)
                        {
                            cbb.SetEnable();
                        }
                        else
                        {
                            cbb.SetDisable();
                        }
                    }
                }
            }

            #region B1TestInterface

            //List<dynamic> MyList = _b1Entities.CollectionFromSql(@"SELECT T0.[DocNum] AS 'GR_NO', T0.[DocEntry] AS 'GR_DocEntry', T0.[DocStatus] AS 'GR_DocStatus'
            //    , T0.[CreateDate] AS 'GR_CreateDate', T0.[CardCode] AS 'Customer_Code', T0.[CardName] AS 'Customer_Name'
            //    , T1.[LineNum] AS 'GR_Item_RowNo', T1.[LineStatus] AS 'GR_Item_RowStatus', T1.[ItemCode] AS 'GR_Item_Code'
            //    , T1.[Dscription] AS 'GR_Item_Name', T1.[Quantity] AS 'GR_Item_Qty', T1.[WhsCode] AS 'GR_Item_WH'
            //    , T4.[DistNumber] AS 'Batch_No', T4.[MnfSerial] AS 'Batch_Att_1', T4.[InDate] AS 'Batch_Adm_Date'
            //    , T6.[ItmsGrpCod] AS 'Item_Group_Code', T6.[ItmsGrpNam] AS Item_Group_Name, T7.[BatchNum] AS 'Item_Batch_Name'
            //    , T7.[SuppSerial] AS 'Item_Batch_Att_1', T7.[InDate] AS 'Item_Batch_Adm_Date', T7.[WhsCode] AS 'Item_Batch_WH', T7.[Quantity] AS 'Item_Batch_Qty'
            //    , T8.[DocNum] AS 'PO_No', T8.[DocEntry] AS 'PO_DocEntry'
            //FROM OPDN T0
            //    INNER JOIN PDN1 T1 ON T0.[DocEntry] = T1.[DocEntry]
            //left join OITL T2 on t1.docentry = T2.[ApplyEntry] and T1.[LineNum] = T2.[ApplyLine] and T2.[ApplyType] = 20
            //left JOIN ITL1 T3 ON T2.LogEntry = T3.LogEntry
            //left join OBTN T4 on T4.[ItemCode] = T3.[ItemCode] and T3.[MdAbsEntry] = t4.[absentry]
            //INNER JOIN OITM T5 ON T1.[ItemCode] = T5.[ItemCode]
            //INNER JOIN OITB T6 ON T5.[ItmsGrpCod] = T6.[ItmsGrpCod]
            //INNER JOIN OIBT T7 ON T5.[ItemCode] = T7.[ItemCode] and T4.[DistNumber] = T7.[BatchNum]
            //left join OPOR T8 ON T8.DocEntry = T1.[BaseEntry]
            //left JOIN POR1 T9 ON T9.[TrgetEntry] = T0.[DocEntry] WHERE T0.[DocStatus] = 'O' and T1.[LineStatus] = 'O'
            //and T1.WhsCode = 'SQC' and T6.[ItmsGrpCod] = 101
            //and T7.[WhsCode] = 'SQC'
            //and isnull(T7.Quantity, 0) > 0",
            //    new Dictionary<string, object>()).ToList();

            //var firstCol = MyList[0].ToString();

            //var result = _b1Entities.Database.SqlQuery<B1_Genlot>(
            //    @"SELECT T0.[DocNum] AS 'GR_NO', T0.[DocEntry] AS 'GR_DocEntry', T0.[DocStatus] AS 'GR_DocStatus'
            //    , T0.[CreateDate] AS 'GR_CreateDate', T0.[CardCode] AS 'Customer_Code', T0.[CardName] AS 'Customer_Name'
            //    , T1.[LineNum] AS 'GR_Item_RowNo', T1.[LineStatus] AS 'GR_Item_RowStatus', T1.[ItemCode] AS 'GR_Item_Code'
            //    , T1.[Dscription] AS 'GR_Item_Name', T1.[Quantity] AS 'GR_Item_Qty', T1.[WhsCode] AS 'GR_Item_WH'
            //    , T4.[DistNumber] AS 'Batch_No', T4.[MnfSerial] AS 'Batch_Att_1', T4.[InDate] AS 'Batch_Adm_Date'
            //    , T6.[ItmsGrpCod] AS 'Item_Group_Code', T6.[ItmsGrpNam] AS Item_Group_Name, T7.[BatchNum] AS 'Item_Batch_Name'
            //    , T7.[SuppSerial] AS 'Item_Batch_Att_1', T7.[InDate] AS 'Item_Batch_Adm_Date', T7.[WhsCode] AS 'Item_Batch_WH', T7.[Quantity] AS 'Item_Batch_Qty'
            //    , T8.[DocNum] AS 'PO_No', T8.[DocEntry] AS 'PO_DocEntry'
            //FROM OPDN T0
            //    INNER JOIN PDN1 T1 ON T0.[DocEntry] = T1.[DocEntry]
            //left join OITL T2 on t1.docentry = T2.[ApplyEntry] and T1.[LineNum] = T2.[ApplyLine] and T2.[ApplyType] = 20
            //left JOIN ITL1 T3 ON T2.LogEntry = T3.LogEntry
            //left join OBTN T4 on T4.[ItemCode] = T3.[ItemCode] and T3.[MdAbsEntry] = t4.[absentry]
            //INNER JOIN OITM T5 ON T1.[ItemCode] = T5.[ItemCode]
            //INNER JOIN OITB T6 ON T5.[ItmsGrpCod] = T6.[ItmsGrpCod]
            //INNER JOIN OIBT T7 ON T5.[ItemCode] = T7.[ItemCode] and T4.[DistNumber] = T7.[BatchNum]
            //left join OPOR T8 ON T8.DocEntry = T1.[BaseEntry]
            //left JOIN POR1 T9 ON T9.[TrgetEntry] = T0.[DocEntry] WHERE T0.[DocStatus] = 'O' and T1.[LineStatus] = 'O'
            //and T1.WhsCode = 'SQC' and T6.[ItmsGrpCod] = 101
            //and T7.[WhsCode] = 'SQC'
            //and isnull(T7.Quantity, 0) > 0").ToList();

            //var test = result.FirstOrDefault();

            #endregion B1TestInterface

            log.Info("End Method: Form1_Load()");
        }

        private void GenerateReport(string sqlText, string reportName)
        {
            log.Info(string.Format(
                @"Start Method: GenerateReport() parameter: string sqlText = '{0}', string reportName = '{1}'", sqlText,
                reportName));
            try
            {
                DataTable dt = new DataTable();

                using (var context = new SWSEntities())
                {
                    var conn = context.Database.Connection;
                    var connectionState = conn.State;
                    try
                    {
                        if (connectionState != ConnectionState.Open) conn.Open();
                        using (var cmd = conn.CreateCommand())
                        {
                            cmd.CommandText = sqlText;
                            cmd.CommandType = CommandType.Text;
                            using (var reader = cmd.ExecuteReader())
                            {
                                dt.Load(reader);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        log.Error("Method: GenerateReport() Query DB Error: " + ex.Message);
                        throw;
                    }
                    finally
                    {
                        if (connectionState != ConnectionState.Closed) conn.Close();
                    }
                }

                int colCount = dt.Columns.Count;
                Workbook book = new Workbook();
                Worksheet sheet = book.Worksheets[0];
                sheet.InsertDataTable(dt, true, 1, 1);
                sheet.AllocatedRange.AutoFitColumns();
                sheet.Range[1, 1, 1, colCount].Style.Color = Color.LightGray;
                sheet.FreezePanes(1, 1);



                //SaveFileExcel(book, reportName);

                string fileName = reportName + " " + DateTime.Now.ToString("dd-MM-yy-hhmmss") + ".xlsx";
                string pathName = Application.StartupPath + @"\Temp\" + fileName;
                book.SaveToFile(fileName, ExcelVersion.Version2010);
                System.Diagnostics.Process.Start(fileName);

            }
            catch (Exception ex)
            {
                log.Error("Method: GenerateReport() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: GenerateReport()");
            }
        }

        private void GenReportBtn_Click(object sender, EventArgs e)
        {
            log.Info("Start Method: GenReportBtn_Click() no parameter");
            try
            {
                string reportName = "";
                string sqlt = "";
                string startDate =
                    startDatePicker.Value.ToString("yyyy-MM-dd", CultureInfo.CreateSpecificCulture("en-US"));
                string endDate = endDatePicker.Value.ToString("yyyy-MM-dd", CultureInfo.CreateSpecificCulture("en-US"));

                string whereEx = "";
                if (startDate != "" && endDate != "")
                {
                    whereEx = string.Format(@"SET @startDate = '{0}'
                                                    SET @endDate = '{1} 23:59:59.999'", startDate, endDate);
                }

                if (reportNameCbb.SelectedItem.ToString().Trim() == genlotReport)
                {
                    #region GenLot

                    reportName = "GenLot";

                    sqlt = string.Format(@"DECLARE @startDate DATETIME = NULL
                                                    DECLARE @endDate DATETIME = NULL

                                                    {0}

                                                    SELECT GL.[ID] 
                                                          ,GL.[DocNo] AS GenLot_DocNo
                                                          ,GL.[CreationDate] AS GenLot_CreateDate
	                                                      ,GL.[RefNo] AS GR_NO
                                                          ,GL.[AssignDate] AS GR_StartDate
                                                          ,GL.[FromWhse]
                                                          ,GL.[ToWhse]      
                                                          ,GL.[Remark]
	                                                      ,CG.ItemCode AS Item_Code
                                                          ,GL.[LotNo]
	                                                      ,GL.[TotalQty] AS Total_Qty_Bales
                                                          ,GL.[TotalTare] AS Total_Tare_Kgs
                                                          ,GL.[TotalGrossWeight] AS Total_GrossWeight_Kgs
	                                                      ,IT.[LotNo] AS SubLot_No
	                                                      ,IT.[Attributes]
                                                          ,IT.[Measure] AS GrossWeight_Kgs
                                                          ,IT.Measure - ISNULL(IT.OuterPackageWeight,0) AS NetWeight_Kgs
	                                                      ,IT.[OuterPackageWeight] AS PackageWeight_Kgs
                                                          ,IT.[VendorRef]
	                                                      ,IT.[ReceiveDate]
                                                      FROM [dbo].[GenLot] GL
                                                      inner join [dbo].[GenLotItem] GI ON GL.ID = GI.GenLotID
                                                      inner join [dbo].[Item] IT ON GI.ItemID = IT.ID
                                                      inner join [dbo].[ItemCategory] CG ON IT.ItemCategoryID = CG.ID
                                                      WHERE (@startDate IS NULL OR (GL.[CreationDate] >= @startDate AND GL.[CreationDate] <= @endDate))"
                        , whereEx);

                    #endregion
                }
                else if (reportNameCbb.SelectedItem.ToString().Trim() == issueReport)
                {
                    #region JobIssue

                    reportName = "JobIssue";

                    sqlt = string.Format(@"DECLARE @startDate DATETIME = NULL
                                                    DECLARE @endDate DATETIME = NULL

                                                    {0}

                                                    SELECT ISI.[JobIssueID]	                                                      
	                                                      ,ISI.CreationDate AS IssueDate
														  ,ICIssue.ItemCode AS Issue_ItemCode
	                                                      ,IT.[LotNo] AS IssueItem_LotNo
														  ,IT.Whse AS IssueItem_FromWhs
	                                                      ,IT.BinLoc AS BinLocation
	                                                      ,IT.[ReceiveDate]
	                                                      ,IIF(IT.ItemTypeID = 3 OR IT.ItemTypeID = 4, (IT.Count * IT.Measure),IT.[Measure]) AS 'GrossWeight_Kgs/Yards'
														  ,IIF(IT.ItemTypeID = 3 OR IT.ItemTypeID = 4, (IT.Count * (ISNULL(IT.Measure,0) - ISNULL(IT.[OuterPackageWeight],0)- ISNULL(IT.[InnerPackageWeight],0) - ISNULL(IT.[MiscPackageWeight],0) )),(ISNULL(IT.Measure,0) - ISNULL(IT.[OuterPackageWeight],0)- ISNULL(IT.[InnerPackageWeight],0) - ISNULL(IT.[MiscPackageWeight],0))) AS 'NetWeight_Kgs/Yards'
	                                                      ,IT.[OuterPackageWeight] AS PackageWeight_Kgs
                                                          ,IT.[InnerPackageWeight]
                                                          ,IT.[MiscPackageWeight]	 
                                                          ,CASE WHEN ISI.[IsCanceled] = 1 THEN 'Yes' ELSE 'No' END AS IsRevert
														  ,JI.[ID] AS JobOrderID
                                                          ,JI.[JobOrderNo]
                                                          ,JI.[DocNo] AS BarCodeJO_DocNo
	                                                      ,JI.[CreationDate]
                                                          ,JI.[AssignDate]
	                                                      ,JI.[DueDate]
	                                                      ,ICFG.ItemCode AS FGCode
                                                          ,JI.[QtyPlanned]
                                                          ,JI.[QtyCompleted]
                                                          ,JI.[QtyRejected]
                                                          ,JI.[QtyOpen]
	                                                      ,JI.[Whse] AS FG_ToWhs                                                          
                                                          ,JI.[SORef]
                                                          ,JI.[CustomerName]
														  ,JI.[Remark]
                                                      FROM [dbo].[IssueProduction] JI
                                                      INNER JOIN [dbo].[IssueProductionItem] ISI ON JI.ID = ISI.IssueProductionID
                                                      INNER JOIN [dbo].[Item] IT ON ISI.ItemID = IT.ID
                                                      INNER JOIN [dbo].[ItemCategory] ICIssue ON IT.ItemCategoryID = ICIssue.ID
                                                      INNER JOIN [dbo].[ItemCategory] ICFG ON JI.FinishItemID = ICFG.ID
                                                      WHERE IT.Whse like '%_ISS' AND (@startDate IS NULL OR (ISI.CreationDate >= @startDate AND ISI.CreationDate <= @endDate))"
                        , whereEx);

                    #endregion
                }
                else if (reportNameCbb.SelectedItem.ToString().Trim() == receiveReport)
                {
                    #region JobReceive

                    reportName = "JobReceive";

                    sqlt = string.Format(@"DECLARE @startDate DATETIME = NULL
                                                    DECLARE @endDate DATETIME = NULL

                                                    {0}

                                                    SELECT JR.[ID] AS JobReceive_ID                                                      
	                                                  ,ICJr.ItemCode AS Received_ItemCode
													  ,IT.[LotNo] AS ReceivedItem_LotNo
	                                                  ,IIF(IT.ItemTypeID = 3 OR IT.ItemTypeID = 4, (IT.Count * IT.Measure),IT.[Measure]) AS 'GrossWeight_Kgs/Yards'
													  ,IIF(IT.ItemTypeID = 3 OR IT.ItemTypeID = 4, (IT.Count * (ISNULL(IT.Measure,0) - ISNULL(IT.[OuterPackageWeight],0)- ISNULL(IT.[InnerPackageWeight],0) - ISNULL(IT.[MiscPackageWeight],0) )),(ISNULL(IT.Measure,0) - ISNULL(IT.[OuterPackageWeight],0)- ISNULL(IT.[InnerPackageWeight],0) - ISNULL(IT.[MiscPackageWeight],0))) AS 'NetWeight_Kgs/Yards'
	                                                  ,IT.[OuterPackageWeight] AS PackageWeight
                                                      ,IT.[InnerPackageWeight]
                                                      ,IT.[MiscPackageWeight]
													  ,JR.[CreationDate] AS ReceiveDate
	                                                  ,JR.[AssignDate]                                                      
	                                                  ,JR.[Shift]
	                                                  ,JR.[Formula]
	                                                  ,JR.[MachineNo]
													  ,JR.[FactoryNo]
	                                                  ,JR.[ToWhse] AS ReceivedItem_ToWhs                                                     
	                                                  ,JR.[BinLoc] AS BinLocation
                                                      ,JR.[QtyCompletedCount] AS Total_Qty
                                                      ,JR.[QtyCompletedMeasure] AS Total_GrossWeight
                                                      ,JR.[InnerPackageCount] AS Total_InnerPackageCount
                                                      ,JR.[InnerPackageWeight] AS Total_InnerPackageWeight
													  ,JR.[OuterPackageCount] AS Total_PackageCount
                                                      ,JR.[OuterPackageWeight] AS Total_PackageWeight
													  ,JR.[MiscPackageCount] AS Total_MiscPackageCount
                                                      ,JR.[MiscPackageWeight] AS Total_MiscPackageWeight                                                                                                           
                                                      ,JR.[PackageType]
	                                                  ,JR.[Remark]	                                                  	                                                  
													  ,JR.[IssueProductionID] AS JobOrderID
	                                                  ,JO.JobOrderNo 
                                                  FROM [dbo].[JobReceive] JR
                                                  INNER JOIN [dbo].[JobReceiveItem] RCI ON JR.ID = RCI.JobReceiveID
                                                  INNER JOIN [dbo].[Item] IT ON RCI.ItemID = IT.ID
                                                  INNER JOIN [dbo].[ItemCategory] ICJr ON IT.ItemCategoryID = ICJr.ID
                                                  INNER JOIN [dbo].[ItemCategory] ICFG ON JR.FinishItemID = ICFG.ID
                                                  INNER JOIN [dbo].[IssueProduction] JO ON JR.IssueProductionID = JO.ID
                                                  WHERE (@startDate IS NULL OR (IT.[ReceiveDate] >= @startDate AND IT.[ReceiveDate] <= @endDate))"
                        , whereEx);

                    #endregion
                }
                else if (reportNameCbb.SelectedItem.ToString().Trim() == transferReport)
                {
                    //AOMM

                    #region Transfer

                    reportName = "Transfer";

                    if (fromWhCbb.SelectedItem != null && fromWhCbb.SelectedItem.ToString() != "ทั้งหมด" && toWhCbb.SelectedItem != null && toWhCbb.SelectedItem.ToString() != "ทั้งหมด")
                    {
                        //All
                        whereEx += string.Format(@" 
                                                    SET @fromWh = '{0}'
                                                    SET @toWh = '{1}'", fromWhCbb.SelectedItem, toWhCbb.SelectedItem);
                    }
                    else if (fromWhCbb.SelectedItem != null && fromWhCbb.SelectedItem.ToString() != "ทั้งหมด" && toWhCbb.SelectedItem != null)
                    {
                        //Select From Wh only
                        whereEx += string.Format(@" 
                                                    SET @fromWh = '{0}' ", fromWhCbb.SelectedItem);
                    }
                    else if (fromWhCbb.SelectedItem != null && toWhCbb.SelectedItem != null && toWhCbb.SelectedItem.ToString() != "ทั้งหมด")
                    {
                        //Select To Wh only
                        whereEx += string.Format(@" 
                                                    SET @toWh = '{0}' ", toWhCbb.SelectedItem);
                    }

                    sqlt = string.Format(@"DECLARE @startDate DATETIME = NULL
                                                    DECLARE @endDate DATETIME = NULL
                                                    DECLARE @fromWh varchar(10) = NULL
                                                    DECLARE @toWh varchar(10) = NULL

                                                    {0}

                                                    SELECT TF.[DocNo] AS Transfer_DocNo	                                                                                                     
	                                                      ,IT.[LotNo] AS Item_LotNo
	                                                      ,IIF(IT.ItemTypeID = 3 OR IT.ItemTypeID = 4, (IT.Count * IT.Measure),IT.[Measure]) AS 'GrossWeight_Kgs/Yards'
														  ,IIF(IT.ItemTypeID = 3 OR IT.ItemTypeID = 4, (IT.Count * (ISNULL(IT.Measure,0) - ISNULL(IT.[OuterPackageWeight],0)- ISNULL(IT.[InnerPackageWeight],0) - ISNULL(IT.[MiscPackageWeight],0) )),(ISNULL(IT.Measure,0) - ISNULL(IT.[OuterPackageWeight],0)- ISNULL(IT.[InnerPackageWeight],0) - ISNULL(IT.[MiscPackageWeight],0))) AS 'NetWeight_Kgs/Yards'
	                                                      ,IT.[OuterPackageWeight] AS PackageWeight_Kgs
                                                          ,IT.[InnerPackageWeight]
                                                          ,IT.[MiscPackageWeight]	                                                      
                                                          ,TF.[FromWhse] AS Item_FromWhs
	                                                      ,IT.BinLoc AS FromBinLocation
														  ,TF.[ToWhse]
                                                          ,TF.[BinLoc] AS ToBinLocation	
														  ,TF.[RefNo] AS Transfer_Request
														  ,TF.[CreationDate] AS Transfer_DocDate
                                                          ,TF.[AssignDate]                                                          
                                                          ,TF.[IsReceived]      
	                                                      ,IT.ReceiveDate          
	                                                      ,TF.[Remark]
														  ,TF.[ID] AS Transfer_DocID
                                                      FROM [dbo].[Transfer] TF
                                                      INNER JOIN [dbo].[TransferItem] TFI ON TF.ID = TFI.TransferID
                                                      INNER JOIN [dbo].[Item] IT ON TFI.ItemID = IT.ID
                                                      WHERE (@startDate IS NULL OR (TF.[CreationDate] >= @startDate AND TF.[CreationDate] <= @endDate))
                                                      AND ((@fromWh IS NULL AND @toWh IS NULL) OR (@fromWh IS NULL AND TF.[ToWhse] = @toWh) OR (TF.[FromWhse] = @fromWh AND @toWh IS NULL) OR (TF.[FromWhse] = @fromWh AND TF.[ToWhse] = @toWh)  OR (TF.[FromWhse] = CONCAT(@fromWh,'_ITR')))"
                        , whereEx);

                    #endregion
                }
                else if (reportNameCbb.SelectedItem.ToString().Trim() == deliveryReport)
                {
                    #region Delivery

                    reportName = "Delivery";

                    sqlt = string.Format(@"DECLARE @startDate DATETIME = NULL
                                                    DECLARE @endDate DATETIME = NULL

                                                    {0}

                                                    SELECT DO.DocNo AS DO_No
                                                    ,DO.CreationDate AS DO_Creation_Date                                                    
                                                    ,DO.[SORef] AS SaleOrder_No
                                                    ,DO.[CustomerName]
                                                    ,DO.[ContactNo]
                                                    ,DO.[BillAddress]       
                                                    ,CASE WHEN DO.[isCanceled] = 1 THEN 'Yes' ELSE 'No' END AS IsRevert
                                                    ,DO.[QtyCount] AS DO_Total_Qty
                                                    ,DO.[QtyMeasure] AS DO_Total_Weight
													,IC.ItemCode
                                                    ,IT.[LotNo] AS LotNo
													,IT.Whse AS Item_FromWhs
                                                    ,IT.BinLoc AS BinLocation
                                                    ,IIF(IT.ItemTypeID = 3 OR IT.ItemTypeID = 4, (IT.Count * IT.Measure),IT.[Measure]) AS 'GrossWeight_Kgs/Yards'
													,IIF(IT.ItemTypeID = 3 OR IT.ItemTypeID = 4, (IT.Count * (ISNULL(IT.Measure,0) - ISNULL(IT.[OuterPackageWeight],0)- ISNULL(IT.[InnerPackageWeight],0) - ISNULL(IT.[MiscPackageWeight],0) )),(ISNULL(IT.Measure,0) - ISNULL(IT.[OuterPackageWeight],0)- ISNULL(IT.[InnerPackageWeight],0) - ISNULL(IT.[MiscPackageWeight],0))) AS 'NetWeight_Kgs/Yards'
                                                    ,IT.[OuterPackageWeight] AS PackageWeight_Kgs
                                                    ,IT.[InnerPackageWeight]
                                                    ,IT.[MiscPackageWeight]                                                    
													,DO.[Route] AS DO_Route
													,DO.[SuggestedLot] AS DO_SuggestedLot
                                                    ,DA.[DocNo] AS DeliveryArrange_No
                                                    ,DA.[CreationDate] AS DeliveryArrange_CreateDate
                                                    ,DA.[AssignDate] AS DeliveryArrange_AssignDate
                                                    ,DA.[DueDate] AS SaleOrder_DueDate
                                                    ,DA.[Remark] AS DeliveryArrange_Remark
                                                    ,DA.[ID] AS DeliveryArrange_ID
                                                    FROM [dbo].[DeliveryArrange] DA
                                                    INNER JOIN [dbo].[DeliveryArrangeDeliveryOrder] DAD ON DA.ID = DAD.DeliveryArrangeID
                                                    INNER JOIN [dbo].[DeliveryOrder] DO ON DAD.DeliveryOrderID = DO.ID
                                                    INNER JOIN [dbo].[ItemCategory] IC ON DO.ItemCategoryID = IC.ID
                                                    LEFT JOIN [dbo].[DeliveryOrderItem] DOI ON DO.ID = DOI.DeliveryOrderID
                                                    LEFT JOIN [dbo].[Item] IT ON DOI.ItemID = IT.ID
                                                    WHERE (@startDate IS NULL OR (DA.[AssignDate] >= @startDate AND DA.[AssignDate] <= @endDate))"
                        , whereEx);

                    #endregion
                }
                else if (reportNameCbb.SelectedItem.ToString().Trim() == stockReport)
                {
                    #region StockOnHand

                    reportName = "StockOnHand";

                    sqlt = string.Format(@"DECLARE @startDate DATETIME = NULL
                                                    DECLARE @endDate DATETIME = NULL

                                                    {0}

                                                    SELECT IC.ItemCode
                                                      ,IT.LotNo
                                                      ,IT.Whse
                                                      ,IT.BinLoc
                                                      ,SUM(IIF(IC.ItemTypeID = 3 OR IC.ItemTypeID = 4, (IT.Count * IT.Measure),IT.[Measure])) AS StockOnHand
                                                      FROM [dbo].[Item] IT
                                                      INNER JOIN [dbo].[ItemCategory] IC ON IT.ItemCategoryID = IC.ID
                                                      GROUP BY IT.Whse,IT.BinLoc,IC.ItemCode,IT.LotNo"
                        , whereEx);

                    #endregion
                }
                else
                {
                    MessageBox.Show("กรุณาเลือกรายงานที่ต้องการ");
                }

                if (sqlt != "")
                {
                    GenerateReport(sqlt, reportName);
                }
            }
            catch (Exception ex)
            {
                log.Error("Method: GenReportBtn_Click() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: GenReportBtn_Click()");
            }
        }

        private void Inventory_FormClosing(object sender, FormClosingEventArgs e)
        {
            WriteLog(0, "Form", "Closing", $"ปิดโปรแกรม");
            if (_leavingJobReceive)
            {
                var allowClose = IP4ConfirmBeforeLeave();
                e.Cancel = !allowClose;
            }

            if (_leavingJobIssue)
            {
                var allowClose = IP3ConfirmBeforeLeave();
                e.Cancel = !allowClose;
            }

            if (_leavingDelivery)
            {
                var allowClose = DO1ConfirmBeforeLeave();
                e.Cancel = !allowClose;
            }
        }

        private void MainTabControl_Deselecting(object sender, TabControlCancelEventArgs e)
        {
            if (_leavingJobReceive)
            {
                if (!IP4ConfirmBeforeLeave())
                {
                    e.Cancel = true;
                }
            }

            if (_leavingJobIssue)
            {
                if (!IP3ConfirmBeforeLeave())
                {
                    e.Cancel = true;
                }
            }

            if (_leavingDelivery)
            {
                if (!DO1ConfirmBeforeLeave())
                {
                    e.Cancel = true;
                }
            }
        }

        private void LogoutLnk_Click(object sender, EventArgs e)
        {
            log.Info("Start Method: LogoutLnk_Click() No parameter");
            WriteLog(0, "Log out", "Log out", $"user:{_currentUser.UserName} ออกจากระบบ");

            if (_leavingJobReceive)
            {
                if (!IP4ConfirmBeforeLeave())
                {
                    return;
                }
            }

            if (_leavingJobIssue)
            {
                if (!IP3ConfirmBeforeLeave())
                {
                    return;
                }
            }

            if (_leavingDelivery)
            {
                if (!DO1ConfirmBeforeLeave())
                {
                    return;
                }
            }

            _currentUser = null;
            mainTabControl.Hide();
            lGFormPnl.Show();
            logoutLnk.Visible = false;

            log.Info("End Method: LogoutLnk_Click()");
        }

        private void mainTabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            log.Info("Start Method: mainTabControl_SelectedIndexChanged() No parameter");
            Cursor.Current = Cursors.WaitCursor;

            try
            {
                if (mainTabControl.SelectedTab == null)
                {
                    return;
                }

                switch (mainTabControl.SelectedTab.Name)
                {
                    case "genLotTabPage":
                        grList = GetGRDetail("", "").ToList();
                        gL1GrNoTxb.Text = "";
                        gL1ItemCodeCbb.DataSource = null;
                        gL1ItemCodeCbb.Text = "";
                        gL1ItemCodeCbb.SetDisable();
                        gL1SubmitBtn.Enabled = false;
                        GLShowPanel(ref gL1Pnl);
                        break;
                    case "issueTabPage":
                        var result = GetJobOrderDetail("", "");
                        iP2JobReceiveBtn.Visible = true;
                        switch (_currentUser.Role.Name.Trim())
                        {
                            case "ADM":
                                jobList = result.ToList();
                                break;
                            case "SRM":
                                jobList = result.Where(jo =>
                                        jo.JO_Warehouse.ToUpper().Trim() == "SPD" ||
                                        jo.RM_wareHouse.ToUpper().Trim() == "SPD" ||
                                        jo.JO_Warehouse.ToUpper().Trim() == "SPD2" ||
                                        jo.RM_wareHouse.ToUpper().Trim() == "SPD2" ||
                                        jo.JO_Warehouse.ToUpper().Trim() == "SPD4" ||
                                        jo.RM_wareHouse.ToUpper().Trim() == "SPD4")
                                    .ToList();
                                iP2JobReceiveBtn.Visible = false;
                                break;
                            case "SP2":
                                jobList = result.Where(jo =>
                                        jo.JO_Warehouse.ToUpper().Trim() == "SPD" ||
                                        jo.RM_wareHouse.ToUpper().Trim() == "SPD" ||
                                        jo.JO_Warehouse.ToUpper().Trim() == "SPD2" ||
                                        jo.RM_wareHouse.ToUpper().Trim() == "SPD2")
                                    .ToList();
                                break;
                            case "SP4":
                                jobList = result.Where(jo =>
                                        jo.JO_Warehouse.ToUpper().Trim() == "SPD" ||
                                        jo.RM_wareHouse.ToUpper().Trim() == "SPD" ||
                                        jo.JO_Warehouse.ToUpper().Trim() == "SPD4" ||
                                        jo.RM_wareHouse.ToUpper().Trim() == "SPD4")
                                    .ToList();
                                break;
                            case "SMG":
                                jobList = result.Where(jo =>
                                        jo.JO_Warehouse.ToUpper().Trim() == "SPD" ||
                                        jo.RM_wareHouse.ToUpper().Trim() == "SPD" ||
                                        jo.JO_Warehouse.ToUpper().Trim() == "SPD2" ||
                                        jo.RM_wareHouse.ToUpper().Trim() == "SPD2" ||
                                        jo.JO_Warehouse.ToUpper().Trim() == "SPD4" ||
                                        jo.RM_wareHouse.ToUpper().Trim() == "SPD4")
                                    .ToList();
                                break;
                            case "WRM":
                                jobList = result.Where(jo => jo.JO_Warehouse.ToUpper().Trim() == "WFG").ToList();
                                iP2JobReceiveBtn.Visible = false;
                                break;
                            case "WPD":
                                jobList = result.Where(jo => jo.JO_Warehouse.ToUpper().Trim() == "WFG").ToList();
                                break;
                            case "WMG":
                                jobList = result.Where(jo => jo.JO_Warehouse.ToUpper().Trim() == "WFG").ToList();
                                break;
                            default:
                                jobList = result.ToList();
                                break;
                        }

                        iP2WhseCbb.Items.Clear();
                        iP2WhseCbb.Items.AddRange(_warehouseList.ToArray());
                        IPShowPanel(ref iP1Pnl);
                        break;
                    case "delArrTabPage":
                        soList = GetSaleOrderDetail("", "");
                        DLShowPanel(ref dL1Pnl);
                        break;
                    case "transferTabPage":
                        _b1TransferReqList = GetTransferRequest("", "");
                        break;
                    case "reportTabPage":

                        //var reportCbbList = new List<ComboBoxItem>();
                        //var i = 0;
                        //foreach (var item in reportNameCbb.Items)
                        //{
                        //    reportCbbList.Add(new ComboBoxItem()
                        //    {
                        //        Text = item.ToString(),
                        //        Value = i++
                        //    });
                        //}

                        //reportNameCbb.Items.Clear();
                        //reportNameCbb.DataSource = reportCbbList;
                        reportNameCbb.Items.Clear();

                        switch (_currentUser.Role.Name.Trim())
                        {
                            case "SRM":
                                reportNameCbb.Items.Add(genlotReport);
                                reportNameCbb.Items.Add(issueReport);
                                reportNameCbb.Items.Add(receiveReport);
                                reportNameCbb.Items.Add(transferReport);
                                break;
                            case "SMG":
                                reportNameCbb.Items.Add(genlotReport);
                                reportNameCbb.Items.Add(issueReport);
                                reportNameCbb.Items.Add(receiveReport);
                                reportNameCbb.Items.Add(transferReport);
                                reportNameCbb.Items.Add(stockReport);
                                break;
                            case "SP2":
                                reportNameCbb.Items.Add(issueReport);
                                reportNameCbb.Items.Add(receiveReport);
                                reportNameCbb.Items.Add(transferReport);
                                break;
                            case "SP4":
                                reportNameCbb.Items.Add(issueReport);
                                reportNameCbb.Items.Add(receiveReport);
                                reportNameCbb.Items.Add(transferReport);
                                break;
                            case "WRM":
                                reportNameCbb.Items.Add(issueReport);
                                reportNameCbb.Items.Add(receiveReport);
                                reportNameCbb.Items.Add(transferReport);
                                break;
                            case "WPD":
                                reportNameCbb.Items.Add(issueReport);
                                reportNameCbb.Items.Add(receiveReport);
                                reportNameCbb.Items.Add(transferReport);
                                break;
                            case "WMG":
                                reportNameCbb.Items.Add(issueReport);
                                reportNameCbb.Items.Add(receiveReport);
                                reportNameCbb.Items.Add(transferReport);
                                reportNameCbb.Items.Add(stockReport);
                                break;
                            case "DEL":
                                reportNameCbb.Items.Add(deliveryReport);
                                reportNameCbb.Items.Add(transferReport);
                                break;
                            case "ADM":
                                reportNameCbb.Items.Add(genlotReport);
                                reportNameCbb.Items.Add(issueReport);
                                reportNameCbb.Items.Add(receiveReport);
                                reportNameCbb.Items.Add(transferReport);
                                reportNameCbb.Items.Add(deliveryReport);
                                reportNameCbb.Items.Add(stockReport);
                                break;
                        }

                        var fromWhseList = new List<string>(_reportWarehouseList);
                        var toWhseList = new List<string>(_reportWarehouseList);

                        fromWhCbb.DataSource = fromWhseList;
                        toWhCbb.DataSource = toWhseList;
                        break;
                    case "adminUserTabPage":
                        InitializeUserTable();
                        break;
                    case "lotSetupTabPage":

                        break;
                    case "itemSetupTabPage":
                        aI1CategoryDgv.Columns.Clear();
                        aI1TypeDgv.Columns.Clear();
                        var itemCategoryList = _entities.ItemCategory.ToList();
                        var itemTypeList = _entities.ItemType.ToList();

                        var categoryTableList = new List<AdminItemCategoryTable>();
                        var typeTableList = new List<AdminItemTypeTable>();

                        foreach (var cat in itemCategoryList)
                        {
                            categoryTableList.Add(new AdminItemCategoryTable()
                            {
                                ID = cat.ID,
                                Attributes = cat.DefaultAttributes,
                                Code = cat.ItemCode,
                                Description = cat.Description,
                                Group = cat.ItemGroupID.ToString() //todo replace id with group name
                            });
                        }

                        foreach (var type in itemTypeList)
                        {
                            typeTableList.Add(new AdminItemTypeTable()
                            {
                                ID = type.ID,
                                Name = type.Name.Replace("unused", "").Trim(),
                                UOMCount = type.UOM_Count,
                                UOMMeasure = type.UOM_Measure,
                                itemizable = type.isItemizable ?? false
                            });
                        }

                        aI1CategoryDgv.AutoGenerateColumns = false;
                        aI1CategoryDgv.DataSource = categoryTableList;
                        aI1TypeDgv.AutoGenerateColumns = false;
                        aI1TypeDgv.DataSource = typeTableList;
                        DataGridViewCell dataGridViewCell = new DataGridViewTextBoxCell();

                        aI1CategoryDgv.Columns.Add(new DataGridViewColumn()
                        {
                            Name = "Group",
                            DataPropertyName = "Group",
                            CellTemplate = dataGridViewCell,
                            HeaderText = "หมวดหมู่"
                        });
                        aI1CategoryDgv.Columns.Add(new DataGridViewColumn()
                        {
                            Name = "Code",
                            DataPropertyName = "Code",
                            CellTemplate = dataGridViewCell,
                            HeaderText = "รหัสสินค้า"
                        });
                        aI1CategoryDgv.Columns.Add(new DataGridViewColumn()
                        {
                            Name = "Attributes",
                            DataPropertyName = "Attributes",
                            CellTemplate = dataGridViewCell,
                            HeaderText = "คุณสมบัติ"
                        });
                        aI1CategoryDgv.Columns.Add(new DataGridViewColumn()
                        {
                            Name = "Description",
                            DataPropertyName = "Description",
                            CellTemplate = dataGridViewCell,
                            HeaderText = "รายละเอียด"
                        });

                        aI1CategoryDgv.Columns["Group"].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                        aI1CategoryDgv.Columns["Group"].ReadOnly = true;
                        aI1CategoryDgv.Columns["Code"].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                        aI1CategoryDgv.Columns["Code"].ReadOnly = true;

                        aI1TypeDgv.Columns.Add(new DataGridViewColumn()
                        {
                            Name = "Name",
                            DataPropertyName = "Name",
                            CellTemplate = dataGridViewCell,
                            HeaderText = "ประเภท"
                        });

                        aI1TypeDgv.Columns.Add(new DataGridViewComboBoxColumn()
                        {
                            DataSource = _uomCountUnits,
                            Name = "UOMCount",
                            DataPropertyName = "UOMCount",
                            HeaderText = "หน่วยนับ"
                        });

                        aI1TypeDgv.Columns.Add(new DataGridViewComboBoxColumn()
                        {
                            DataSource = _uomMeasureUnits,
                            Name = "UOMMeasure",
                            DataPropertyName = "UOMMeasure",
                            HeaderText = "หน่วยวัด"
                        });

                        aI1TypeDgv.Columns.Add(new DataGridViewCheckBoxColumn()
                        {
                            Name = "Itemizable",
                            DataPropertyName = "Itemizable",
                            HeaderText = "แตก Lot",
                            ReadOnly = true
                        });

                        aI1TypeDgv.Columns["Name"].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                        aI1TypeDgv.Columns["Name"].ReadOnly = true;

                        foreach (DataGridViewRow row in aI1TypeDgv.Rows)
                        {
                            var adminTypeTableItem = row.DataBoundItem as AdminItemTypeTable;
                            var uomCountCbb = (DataGridViewComboBoxCell)(row.Cells["UOMCount"]);
                            var uomMeasureCbb = (DataGridViewComboBoxCell)(row.Cells["UOMMeasure"]);
                            uomCountCbb.Value = adminTypeTableItem.UOMCount.Trim();
                            uomMeasureCbb.Value = adminTypeTableItem.UOMMeasure.Trim();
                        }

                        break;
                    case "docSetupTabPage":

                        break;
                    case "reprintTabPage":
                        isreprint = false;
                        reprintInfoPnl.Enabled = false;
                        itemCodetxb.Text = "";
                        lotNoTxb.Text = "";
                        createDateTxb.Text = "";
                        grossWgTxb.Text = "";
                        break;
                }
            }
            catch (Exception ex)
            {
                log.Error("Method: mainTabControl_SelectedIndexChanged() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                WriteLog(0, "Tab", "Change tab",
                    $"เปลี่ยนเมนูไปหน้า:{(mainTabControl.SelectedTab == null ? (object)"" : mainTabControl.SelectedTab.Name)}");
                log.Info("End Method: mainTabControl_SelectedIndexChanged()");
                Cursor.Current = Cursors.Default;
            }
        }

        private void MetroButton1_Click(object sender, EventArgs e)
        {
            log.Info("Start Method: MetroButton1_Click() No parameter");

            _formWorksheet.Pictures.Add(1, 1, GetQRcodeImage("test9876543"));
            //workbook.Save();
            var saveDialog = new SaveFileDialog
            {
                Filter = @"Excel Worksheets (*.xlsx)|*.xlsx",
                FileName = "Export File " + DateTime.Now.ToString("dd-MM-yy-hhmmss"),
                DefaultExt = "xlsx"
            };
            saveDialog.OpenFile();
            if (saveDialog.ShowDialog() == DialogResult.OK)
            {
                _formWorkbook.SaveToFile(saveDialog.FileName, ExcelVersion.Version2016);
            }

            log.Info("End Method: MetroButton1_Click()");
        }

        private void MetroButton11_Click(object sender, EventArgs e)
        {
            log.Info("Start Method: MetroButton11_Click() No parameter");
            WriteLog(_issueProductionInstance?.ID, "JO", "Exit",
                $"ออกจากหน้า Job order no:{_issueProductionInstance?.DocNo}");


            iP2Pnl.Visible = false;
            issueTabPage.Controls.Add(iP1Pnl);
            iP1Pnl.Dock = DockStyle.Fill;
            iP1Pnl.Visible = true;

            log.Info("End Method: MetroButton11_Click()");
        }

        private void MetroButton17_Click(object sender, EventArgs e)
        {
            log.Info("Start Method: MetroButton17_Click() No parameter");

            iP1Pnl.Visible = false;
            issueTabPage.Controls.Add(iP2Pnl);
            iP2Pnl.Dock = DockStyle.Fill;
            iP2Pnl.Visible = true;

            log.Info("End Method: MetroButton17_Click()");
        }

        private void MetroButton18_Click_1(object sender, EventArgs e)
        {
            log.Info("Start Method: MetroButton18_Click_1() No parameter");
            WriteLog(_deliveryArrangeInstance?.ID, "DA", "Exit",
                $"ออกจากหน้า Select SOs ของ DA no:{_deliveryArrangeInstance?.DocNo}");


            delArrTabPage.Controls.Add(dL1Pnl);
            dL1Pnl.Dock = DockStyle.Fill;
            dL1Pnl.Visible = true;
            dL2Pnl.Visible = false;
            dL3Pnl.Visible = false;
            dO1Pnl.Visible = false;

            log.Info("End Method: MetroButton18_Click_1()");
        }

        private void MetroButton2_Click(object sender, EventArgs e)
        {
            log.Info("Start Method: MetroButton2_Click() No parameter");

            var bmp = GetQRcodeImage("test12345");

            log.Info("End Method: MetroButton2_Click()");
        }

        private void MetroButton31_Click(object sender, EventArgs e)
        {
            log.Info("Start Method: MetroButton31_Click() No parameter");
            WriteLog(_transferInstance?.ID, "TR", "Exit"
                , $"ออกจากหน้า TF No:{_transferInstance?.DocNo}");


            transferTabPage.Controls.Add(tR1Pnl);
            tR1Pnl.Dock = DockStyle.Fill;
            tR1Pnl.Visible = true;
            tR2Pnl.Visible = false;

            log.Info("End Method: MetroButton31_Click()");
        }

        private void DO1CancelBtn_Click(object sender, EventArgs e)
        {
            log.Info("Start Method: DO1CancelBtn_Click() No parameter");
            WriteLog(_deliveryOrderInstance?.ID, "DO", "Exit",
                $"ออกจากหน้า DO ของ DO no:{_deliveryOrderInstance?.DocNo}");

            if (_deliveryOrderIsSaved)
            {
                if (_deliveryArrangeList != null)
                {
                    RefreshDl3DeliveryDgv();
                }

                DLShowPanel(ref dL3Pnl);
            }

            if (DO1ConfirmBeforeLeave())
            {
                DLShowPanel(ref dL3Pnl);
                _exitFromButton = true;
            }

            log.Info("End Method: DO1CancelBtn_Click()");
        }

        private void RefreshDl3DeliveryDgv()
        {
            foreach (DataGridViewRow row in dL3DeliveryDgv.Rows)
            {
                var deliveryArrangeTableItem = row.DataBoundItem as DeliveryArrangeTable;

                if (deliveryArrangeTableItem != null && !deliveryArrangeTableItem.IsValid)
                {
                    row.DefaultCellStyle.BackColor = Color.WhiteSmoke;
                    row.Cells["new do button"].Value = "สินค้าไม่พอส่ง";
                    continue;
                }
                else
                {
                    row.Cells["new do button"].Value = "สร้างใบส่งสินค้า";
                }

                var deliveryOrder =
                    _entities.DeliveryOrder.FirstOrDefault(dlo => dlo.ID == deliveryArrangeTableItem.DeliveryOrderID);

                if (deliveryOrder?.IsDelivered != null && deliveryOrder.IsDelivered.Value)
                {
                    row.DefaultCellStyle.BackColor = Color.LightBlue;
                    row.Cells["new do button"].Value = "ดูใบจัดส่ง";
                }

                if (deliveryOrder?.isSentToB1 != null && deliveryOrder.isSentToB1.Value)
                {
                    row.DefaultCellStyle.BackColor = Color.LightBlue;
                    row.DefaultCellStyle.ForeColor = Color.MediumBlue;
                    row.Cells["new do button"].Value = "ดูใบจัดส่ง";
                }

                if (deliveryOrder?.isCanceled != null && deliveryOrder.isCanceled.Value)
                {
                    row.DefaultCellStyle.BackColor = Color.LightSalmon;
                    row.Cells["new do button"].Value = "ดูใบจัดส่ง";
                }
            }
        }

        private void MetroButton39_Click(object sender, EventArgs e)
        {
            log.Info("Start Method: MetroButton39_Click() No parameter");
            WriteLog(_deliveryArrangeInstance?.ID, "DA", "Exit",
                $"ออกจากหน้า Create DO P3:{_deliveryArrangeInstance?.ID}");

            delArrTabPage.Controls.Add(dL1Pnl);
            dL1Pnl.Dock = DockStyle.Fill;
            dL1Pnl.Visible = true;
            dL2Pnl.Visible = false;
            dL3Pnl.Visible = false;
            dO1Pnl.Visible = false;

            log.Info("End Method: MetroButton39_Click()");
        }

        private void IP4CancelBtn_Click(object sender, EventArgs e)
        {
            log.Info("Start Method: MetroButton53_Click() No parameter");
            WriteLog(_issueProductionInstance?.ID, "JR", "Exit",
                $"ออกจากหน้า Job receive ID:{_jobReceiveInstance?.ID}");

            if (IP4ConfirmBeforeLeave())
            {
                issueTabPage.Controls.Add(iP2Pnl);
                iP2Pnl.Dock = DockStyle.Fill;
                iP2Pnl.Visible = true;
                //iP2Pnl.Focus();
                iP4Pnl.Visible = false;
                _exitFromButton = true;
            }

            log.Info("End Method: MetroButton53_Click()");
        }

        private void TR2RefNoCbb_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (tR2RefNoCbb.SelectedItem.ToString().Length > 0)
                {
                    var docNum = tR2RefNoCbb.SelectedItem.ToString();
                    var req = _b1TransferReqList.FirstOrDefault(tr => tr.DocNum.ToString() == docNum);
                    if (req != null)
                    {
                        tR2FromWhseCbb.SelectedItem = req.FromWhs;
                        tR2ToWhseCbb.SelectedItem = req.ToWhsCode;
                    }

                    tR2FromWhseCbb.SetDisable();
                    tR2ToWhseCbb.SetDisable();
                }
                else
                {
                    tR2FromWhseCbb.SelectedIndex = 0;
                    tR2ToWhseCbb.SelectedIndex = 0;
                    tR2FromWhseCbb.SetEnable();
                    tR2ToWhseCbb.SetEnable();
                }
            }
            catch (Exception ex)
            {
                log.Error("Method: TR2RefNoCbb_SelectedIndexChanged() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void iP3Pnl_Validated(object sender, EventArgs e)
        {
            if (!_exitFromButton)
            {
                _leavingJobIssue = true;
            }

            _exitFromButton = false;
        }

        private bool DO1ConfirmBeforeLeave()
        {
            log.Info("Start Method: DO1ConfirmBeforeLeave()");
            try
            {
                var allowClose = true;
                if (!_deliveryOrderIsSaved && _deliveryOrderList != null && _deliveryOrderList.Count > 0)
                {
                    var confirmResult = MessageBox.Show(
                        "คุณยังไม่ได้บันทึกการทำงานในหน้านี้ คุณแน่ใจว่าต้องการออกจากหน้านี้?",
                        "คำเตือน!",
                        MessageBoxButtons.YesNo);
                    if (confirmResult == DialogResult.Yes)
                    {
                        _deliveryOrderList.Clear();
                        //RevertAllEntitiesChanges();
                    }
                    else
                    {
                        allowClose = false;
                    }
                }

                return allowClose;
            }
            catch (Exception ex)
            {
                log.Error("Method: DO1ConfirmBeforeLeave() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: DO1ConfirmBeforeLeave()");
            }

            return false;
        }

        private void dO1Pnl_Validated(object sender, EventArgs e)
        {
            if (!_exitFromButton)
            {
                _leavingDelivery = true;
            }

            _exitFromButton = false;
        }

        private void lGFirstLogSubmitBtn_Click(object sender, EventArgs e)
        {
            var password1 = lGFirstLogPassword1Txb.Text;
            var password2 = lGFirstLogPassword2Txb.Text;

            if (password1 != password2)
            {
                MessageBox.Show("ท่านกรอกรหัสผ่านไม่ตรงกัน กรุณาลองใหม่อีกครั้ง");
                return;
            }
            else if (password1.Length < 6)
            {
                MessageBox.Show("รหัสผ่านต้องมีความยาว 6 ตัวขึ้นไป");
                return;
            }
            else
            {
                var hashedPswd = ComputeSha256Hash(password1);
                _currentUser.Password = hashedPswd;
                _currentUser.FirstLogin = false;
                lGFirstLogPnl.SendToBack();
                lGFirstLogPnl.Visible = false;
                lGFirstLogPassword1Txb.Text = "";
                lGFirstLogPassword2Txb.Text = "";
                HandleLoginSequence();
            }
        }

        private void iP3ScannedItemDgv_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            IP3_RecalculateTotalWeight();
            iP3SendB1Btn.Enabled = false;
            iP3SubmitBtn.Enabled = true;
        }

        private void gLBarcodePnl_MouseHover(object sender, EventArgs e)
        {
            gL1BarcodePnl.BackColor = Color.LightSkyBlue;
            gL1Pnl.Focus();
        }

        private void gLBarcodePnl_MouseLeave(object sender, EventArgs e)
        {
            gL1BarcodePnl.BackColor = Color.Transparent;
        }

        private void iP1BarcodePnl_MouseHover(object sender, EventArgs e)
        {
            iP1BarcodePnl.BackColor = Color.LightSkyBlue;
            iP1Pnl.Focus();
        }

        private void iP1BarcodePnl_MouseLeave(object sender, EventArgs e)
        {
            iP1BarcodePnl.BackColor = Color.Transparent;
        }

        private void dL1BarcodePnl_MouseHover(object sender, EventArgs e)
        {
            dL1BarcodePnl.BackColor = Color.LightSkyBlue;
            dL1Pnl.Focus();
        }

        private void dL1BarcodePnl_MouseLeave(object sender, EventArgs e)
        {
            dL1BarcodePnl.BackColor = Color.Transparent;
        }

        private void tR1BarcodePnl_MouseHover(object sender, EventArgs e)
        {
            tR1BarcodePnl.BackColor = Color.LightSkyBlue;
            tR1Pnl.Focus();
        }

        private void tR1BarcodePnl_MouseLeave(object sender, EventArgs e)
        {
            tR1BarcodePnl.BackColor = Color.Transparent;
        }

        private void iP3BarcodePnl_MouseHover(object sender, EventArgs e)
        {
            iP3BarcodePnl.BackColor = Color.LightSkyBlue;
            iP3Pnl.Focus();
        }

        private void iP3BarcodePnl_MouseLeave(object sender, EventArgs e)
        {
            iP3BarcodePnl.BackColor = Color.Transparent;
        }

        private void dO1BarcodePnl_MouseHover(object sender, EventArgs e)
        {
            dO1BarcodePnl.BackColor = Color.LightSkyBlue;
            dO1Pnl.Focus();
        }

        private void dO1BarcodePnl_MouseLeave(object sender, EventArgs e)
        {
            dO1BarcodePnl.BackColor = Color.Transparent;
        }

        private void tR2BarcodePnl_MouseHover(object sender, EventArgs e)
        {
            tR2BarcodePnl.BackColor = Color.LightSkyBlue;
            tR2Pnl.Focus();
        }

        private void tR2BarcodePnl_MouseLeave(object sender, EventArgs e)
        {
            tR2BarcodePnl.BackColor = Color.Transparent;
        }

        private void lGPasswordTxb_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void lGUserNameTxb_Enter(object sender, EventArgs e)
        {
            BeginInvoke((Action)delegate { lGUserNameTxb.SelectAll(); });
        }

        private void lGPasswordTxb_Enter(object sender, EventArgs e)
        {
            BeginInvoke((Action)delegate { lGPasswordTxb.SelectAll(); });
        }

        private void lGPasswordTxb_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                lGSubmitBtn.PerformClick();

                e.Handled = true;
                e.SuppressKeyPress = true;
            }
        }

        private void iP5issueListCbb_SelectedIndexChanged(object sender, EventArgs e)
        {
            log.Info("Start Method: iP5issueListCbb_SelectedIndexChanged() No parameter");
            try
            {
                if (((ComboBoxItem)iP5issueListCbb.SelectedItem).Value == null)
                {
                    return;
                }
                _scannedItemList = new BindingList<IssueProductionIssueTable>();
                _scannedItemDeleteList = new List<IssueProductionIssueTable>();
                var requestItemizable =
                    GetItemTypeByItemCode(_issueProductionRequestList.FirstOrDefault().ItemCategory.ItemCode)
                        .isItemizable;

                if (((ComboBoxItem)iP5issueListCbb.SelectedItem).Value.ToString() == "0") //create new
                {
                    _currentJobIssueID = 0;
                    iP3SendB1Btn.Enabled = false;
                    iP3SubmitBtn.Enabled = false;
                    _jobIssueIsSaved = false;
                    _jobIssueSentToB1 = false;
                }
                else //existing
                {
                    var selectedItem = Convert.ToInt64((iP5issueListCbb.SelectedItem as ComboBoxItem).Value);
                    _currentJobIssueID = selectedItem;
                    var issueItemList = _entities.IssueProductionItem.Where(ipi => ipi.JobIssueID == selectedItem)
                        .GroupBy(ipi => ipi.ItemID)
                        .ToList();
                    _jobIssueSentToB1 = issueItemList.All(ii => ii.All(k => k.IsSentToB1 ?? false));

                    foreach (var item in issueItemList)
                    {
                        var lastItem = item.OrderByDescending(i => i.CreationDate)
                            .FirstOrDefault();
                        if (!lastItem.IsCanceled.Value)
                        {
                            var scannedItem = lastItem.Item;
                            var grossWeight = (scannedItem.Measure ?? 0);
                            var netWeight = (scannedItem.Measure ?? 0) -
                                            ((scannedItem.OuterPackageWeight ?? 0) +
                                             (scannedItem.InnerPackageWeight ?? 0) +
                                             (scannedItem.MiscPackageWeight ?? 0));
                            var itemCount = (int)(scannedItem.Count ?? 0);

                            var mappedItem = new IssueProductionIssueTable()
                            {
                                RowNo = _scannedItemList.Count + 1,
                                ID = scannedItem.ID,
                                ItemCode = scannedItem.ItemCategory.ItemCode,
                                Description = scannedItem.ItemCategory.Description,
                                LotNo = scannedItem.LotNo,
                                GrossWeight = grossWeight,
                                NetWeight = requestItemizable.Value ? netWeight : (itemCount * netWeight),
                                Whse = scannedItem.Whse,
                                Count = (int)(scannedItem.Count ?? 0)
                            };
                            _scannedItemList.Add(mappedItem);
                        }

                    }

                    iP3SendB1Btn.Enabled = true;
                    iP3SubmitBtn.Enabled = false;
                    _jobIssueIsSaved = true;
                }

                iP3TotalNetWeightTxb.Text = "";
                iP3TotalGrossWeightTxb.Text = "";
                iP3DateCbb.Value = DateTime.Now;
                iP3ScannedItemDgv.Columns.Clear();
                iP3ScannedItemDgv.DataSource = _scannedItemList;
                if (!_jobIssueSentToB1)
                {
                    iP3ScannedItemDgv.Columns.Add(new DataGridViewButtonColumn()
                    {
                        Text = "ลบ",
                        Name = "delete button",
                        HeaderText = "ลบ",
                        UseColumnTextForButtonValue = true
                    });
                }

                iP3ScannedItemDgv.Columns["GrossWeight"].DefaultCellStyle.Format = "N2";
                iP3ScannedItemDgv.Columns["NetWeight"].DefaultCellStyle.Format = "N2";
                iP3ScannedItemDgv.Columns["RowNo"].ReadOnly = true;
                iP3ScannedItemDgv.Columns["ItemCode"].ReadOnly = true;
                iP3ScannedItemDgv.Columns["Description"].ReadOnly = true;
                iP3ScannedItemDgv.Columns["LotNo"].ReadOnly = true;
                iP3ScannedItemDgv.Columns["Count"].ReadOnly = (requestItemizable ?? false) || _jobIssueIsSaved || _jobIssueSentToB1;
                iP3ScannedItemDgv.Columns["GrossWeight"].ReadOnly = true;
                iP3ScannedItemDgv.Columns["NetWeight"].ReadOnly = true;
                iP3ScannedItemDgv.Columns["Whse"].ReadOnly = true;
                iP3ScannedItemDgv.Columns["GrossWeight"].Visible = requestItemizable ?? false;
                iP3Pnl.Focus();
                iP3TotalGrossWeightPnl.Visible = requestItemizable ?? false;

                if (_jobIssueSentToB1)
                {
                    iP3ScannedItemDgv.ReadOnly = true;
                    iP3SubmitBtn.Enabled = false;
                    iP3SendB1Btn.Enabled = false;
                    _jobIssueIsSaved = true;
                }
                else
                {
                    iP3ScannedItemDgv.ReadOnly = false;
                }

                IP3_RecalculateTotalWeight();
                IPShowPanel(ref iP3Pnl);
            }
            catch (Exception ex)
            {
                log.Error("Method: iP5issueListCbb_SelectedIndexChanged() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: iP5issueListCbb_SelectedIndexChanged()");
            }
        }

        private void iP3ScannedItemDgv_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            foreach (DataGridViewRow row in iP3ScannedItemDgv.Rows)
            {
                var rowItem = row.DataBoundItem as IssueProductionIssueTable;
                if (rowItem.Whse.Contains(IssuedWhse))
                {
                    row.Cells["Count"].ReadOnly = true;
                }
                else
                {
                    var isItemizable = GetItemTypeByItemCode(rowItem.ItemCode).isItemizable;
                    if (!isItemizable.Value)
                    {
                        row.Cells["Count"].ReadOnly = false;
                    }
                }

                IP3_RecalculateTotalWeight();
            }
        }

        private void iP3SendB1Btn_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                long newJobIssueID;
                var newIssueProductionItemList = new List<IssueProductionItem>();
                if (_currentJobIssueID == 0)
                {
                    var lastJobIssueID = _entities.IssueProductionItem.Max(ipi => ipi.JobIssueID);
                    newJobIssueID = lastJobIssueID + 1 ?? 1;
                }
                else
                {
                    newJobIssueID = _currentJobIssueID;
                }

                var insufficientQuantityCodes = "";
                var groupedByItemIDItemList = _scannedItemList.GroupBy(i => i.ID).ToList();
                var resultIssueList = GetGroupedResultIssueItemList(newJobIssueID);

                foreach (var item in resultIssueList)
                {
                    var itemType = GetItemTypeByItemCode(item.ItemCode);
                    var lotNo = item.LotNo;
                    var availableQty = _b1Entities.OIBT
                        .Where(oibt =>
                            oibt.ItemCode == item.ItemCode
                            && oibt.BatchNum == lotNo
                            && oibt.WhsCode == item.FromWhseCode.Replace(IssuedWhse, "")).Select(oibt => oibt.Quantity).FirstOrDefault();
                    var sumNet = itemType.Name == "Finish Goods - WE" ? item.NetPerLot : ConvertKgsToLbs(item.NetPerLot);

                    if ((availableQty.HasValue && availableQty < sumNet) || !availableQty.HasValue)
                    {
                        insufficientQuantityCodes +=
                            insufficientQuantityCodes.Length > 0
                                ? ", " + item.ItemCode + " ใน " + item.FromWhseCode.Replace(IssuedWhse, "")
                                : item.ItemCode + " ใน " + item.FromWhseCode.Replace(IssuedWhse, "");
                    }
                    item.FromWhseCode = item.FromWhseCode.Replace(IssuedWhse, "");
                }

                if (insufficientQuantityCodes.Length > 0)
                {
                    MessageBox.Show("สินค้าเหล่านี้มีจำนวนสต็อคในโกดังต้นทางไม่เพียงพอ: " + insufficientQuantityCodes);
                    return;
                }

                if (resultIssueList != null && resultIssueList.Count > 0)
                {
                    var result = Interface_GoodsIssueExcelTemplate(resultIssueList);
                    if (result)
                    {
                        var issueItemList = _entities.IssueProductionItem.Where(ii => ii.JobIssueID == _currentJobIssueID)
                            .ToList();
                        foreach (var issueItem in issueItemList)
                        {
                            issueItem.IsSentToB1 = true;
                        }

                        _jobIssueSentToB1 = true;
                        iP3SendB1Btn.Enabled = false;
                        iP3SubmitBtn.Enabled = false;
                        iP3ScannedItemDgv.Columns["delete button"].Visible = false;
                        iP3ScannedItemDgv.Columns["Count"].ReadOnly = true;
                        MessageBox.Show("บันทึกเข้า B1 เรียบร้อยแล้ว");
                    }
                    else
                    {
                        //Enable Save btn
                        _issueProductionInstance.IsIssued = false;
                        iP3SubmitBtn.Enabled = true;
                        iP3SendB1Btn.Enabled = true;

                        //RevertAllEntitiesChanges();
                        MessageBox.Show("บันทึกไม่สำเร็จ กรุณาลองใหม่อีกครั้ง");
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Method: iP3SendB1Btn_Click() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: iP3SendB1Btn_Click()");
                Cursor.Current = Cursors.Default;
            }
        }

        private void dO1SendToB1Btn_Click(object sender, EventArgs e)
        {
            log.Info("Start Method: dO1SendToB1Btn_Click() No parameter");
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var resultDeliveryOrderItemList = GetGroupedResultDeliveryItemList();
                var insufficientQuantityCodes = "";

                foreach (var item in resultDeliveryOrderItemList)
                {
                    var itemType = GetItemTypeByItemCode(item.ItemCode);
                    var lotNo = item.LotNo;
                    var availableQty = _b1Entities.OIBT
                        .Where(oibt =>
                            oibt.ItemCode == item.ItemCode
                            && oibt.BatchNum == lotNo
                            && oibt.WhsCode == item.ItemLotFromWhseCode.Replace(SoldWhse, "")).Select(oibt => oibt.Quantity).FirstOrDefault();
                    var sumNet = itemType.Name == "Finish Goods - WE"
                        ? item.NetPerLot
                        : ConvertKgsToLbs(item.NetPerLot);

                    if ((availableQty.HasValue && availableQty < sumNet) || !availableQty.HasValue)
                    {
                        insufficientQuantityCodes +=
                            insufficientQuantityCodes.Length > 0 ? ", " + item.ItemCode : item.ItemCode;
                    }

                    item.ItemLotFromWhseCode = item.ItemLotFromWhseCode.Replace(SoldWhse, "");
                }

                if (insufficientQuantityCodes.Length > 0)
                {
                    MessageBox.Show("สินค้าเหล่านี้มีจำนวนในสต็อคต้นทางไม่เพียงพอ: " + insufficientQuantityCodes);
                    return;
                }

                if (resultDeliveryOrderItemList != null && resultDeliveryOrderItemList.Count > 0)
                {
                    var result = Interface_DeliveryExcelTemplate(resultDeliveryOrderItemList);

                    if (result)
                    {
                        //Update IsReceived
                        dO1SubmitBtn.Enabled = false;
                        dO1SendToB1Btn.Enabled = false;
                        dO1PrintFormBtn.Enabled = true;
                        //dO1TerminateDoBtn.Enabled = true;
                        _deliveryOrderInstance.IsDelivered = true;
                        _deliveryOrderIsSaved = true;
                        _deliveryOrderInstance.isSentToB1 = true;
                        _entities.SaveChanges();
                        MessageBox.Show("บันทึกเรียบร้อย");
                    }
                    else
                    {
                        //Enable Save btn
                        dO1SubmitBtn.Enabled = false;
                        dO1SendToB1Btn.Enabled = true;
                        dO1PrintFormBtn.Enabled = true;
                        //dO1TerminateDoBtn.Enabled = false;
                        _deliveryOrderInstance.IsDelivered = false;
                        _deliveryOrderIsSaved = false;
                        _deliveryOrderInstance.isSentToB1 = false;
                        _entities.SaveChanges();
                        MessageBox.Show("บันทึกไม่สำเร็จ กรุณาลองใหม่อีกครั้ง");
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Method: dO1SendToB1Btn_Click() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                log.Info("End Method: dO1SendToB1Btn_Click()");
                Cursor.Current = Cursors.Default;
            }
        }

        private void dO1TerminateDoBtn_Click(object sender, EventArgs e)
        {

            var confirmResult = MessageBox.Show("คุณต้องการจะยกเลิกใบจัดส่งนี้หรือไม่?",
                "คำเตือน!",
                MessageBoxButtons.YesNo);
            if (confirmResult == DialogResult.Yes)
            {
                var remark = "";
                while (string.IsNullOrEmpty(remark))
                {
                    remark = ShowConfirmDialog();
                }

                _deliveryOrderInstance.isCanceled = true;
                _deliveryOrderInstance.Remark = remark;
                foreach (var item in _deliveryOrderList)
                {
                    var currentItem = GetItemByID(item.ItemID);

                    currentItem.Whse = currentItem.Whse.Replace(SoldWhse, "");
                }

                _entities.SaveChanges();
                var docNo = _deliveryArrangeInstance.DocNo;
                _deliveryOrderList.Clear();
                dL1DelNoTxb.Text = docNo.Trim();
                dL1SubmitBtn.PerformClick();
                dL2CreateDelBtn.PerformClick();
                dL3DeliveryDgv.Refresh();
                dO1TerminateDoBtn.Enabled = false;
                dO1CancelBtn.PerformClick();
                //DLShowPanel(ref dL3Pnl);
            }
            else
            {
            }
        }

        private void dL3DeliveryDgv_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            //RefreshDl3DeliveryDgv();
        }

        private void iP4SpiralChk_CheckedChanged(object sender, EventArgs e)
        {
            if (iP4SpiralChk.Checked)
            {
                iP4ConeCbb.DataSource = _coneSpiralList;
            }
            else
            {
                iP4ConeCbb.DataSource = _coneNoSpiralList;
            }
        }

        private void iP4PlasticChk_CheckedChanged(object sender, EventArgs e)
        {
            if (iP4PlasticChk.Checked)
            {
                _plasticWeight = (decimal)0.005671;
            }
            else
            {
                _plasticWeight = 0;
            }

            var innerWeight = _coneWeight + _plasticWeight;

            iP4InnerPackageWeightPerPieceTxb.Text = $"{innerWeight}";
        }

        private void iP4ConeCbb_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (iP4ConeCbb.SelectedItem.ToString())
            {
                case "หลอด(พลาสติก)Local (5°)":
                    _coneWeight = (decimal)0.06;
                    break;
                case "หลอด(กระดาษ)Export (5°)":
                    _coneWeight = (decimal)0.04;
                    break;
                case "หลอด(พลาสติก)Local (3°)":
                    _coneWeight = (decimal)0.04;
                    break;
                case "หลอด(กระดาษ)Export (3°)":
                    _coneWeight = (decimal)0.03;
                    break;
            }

            var innerWeight = _coneWeight + _plasticWeight;

            iP4InnerPackageWeightPerPieceTxb.Text = $"{innerWeight}";
        }

        private void iP1JobNoTxb_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                iP1SubmitBtn.PerformClick();

                e.Handled = true;
                e.SuppressKeyPress = true;
            }
        }

        private void dL1DelNoTxb_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                dL1SubmitBtn.PerformClick();

                e.Handled = true;
                e.SuppressKeyPress = true;
            }
        }

        private void tR1TransferNoTxb_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                tR1SubmitBtn.PerformClick();

                e.Handled = true;
                e.SuppressKeyPress = true;
            }
        }

        private void reportNameCbb_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (reportNameCbb.SelectedItem.ToString().Trim() == transferReport)
            {
                whReportPnl.Visible = true;
                fromWhCbb.Enabled = true;
                toWhCbb.Enabled = true;
            }
            else
            {
                whReportPnl.Visible = false;
            }
        }

        private void checkIdBtn_Click(object sender, EventArgs e)
        {
            log.Info("Start Method: checkIdBtn_Click()");

            try
            {
                //AOMM
                isreprint = false;
                reprintInfoPnl.Enabled = false;
                itemCodetxb.Text = "";
                lotNoTxb.Text = "";
                createDateTxb.Text = "";
                grossWgTxb.Text = "";

                if (itemIdTxb.Text.Trim() != "")
                {
                    long itid = 0;
                    try
                    {
                        itid = Convert.ToInt64(itemIdTxb.Text.Trim());
                    }
                    catch
                    {
                        MessageBox.Show("ID ไม่ถูกต้อง");
                        log.Error(
                            "Method: checkIdBtn_Click() Error: ID ไม่ถูกต้อง cannot convert input string to long");
                        return;
                    }

                    var reprintItems = (from t in _entities.Item
                                        join c in _entities.ItemCategory
                                            on t.ItemCategoryID equals c.ID
                                        where t.ID == itid
                                        select new
                                        {
                                            itemId = t.ID,
                                            lotNo = t.LotNo,
                                            createDate = t.ReceiveDate,
                                            grossWeight = t.Measure,
                                            itemCode = c.ItemCode,
                                            netWeight = t.Measure - (t.InnerPackageWeight ?? 0) - (t.OuterPackageWeight ?? 0) -
                                                        (t.MiscPackageWeight ?? 0),
                                            att = t.Attributes,
                                            vendor = t.VendorRef,
                                            itemType = t.ItemTypeID,
                                            amountPerPack = (t.ItemTypeID == 3 || t.ItemTypeID == 4) ? 1 : t.InnerPackageCount
                                        })?.FirstOrDefault();

                    if (reprintItems != null)
                    {
                        itemCodetxb.Text = reprintItems.itemCode;
                        lotNoTxb.Text = reprintItems.lotNo;
                        createDateTxb.Text = reprintItems.createDate?.ToString("dd-MM-yyyy",
                            CultureInfo.CreateSpecificCulture("th-TH"));
                        grossWgTxb.Text = reprintItems.grossWeight?.ToString();

                        if (reprintItems.itemType == 1)
                        {
                            //Rm
                            if (_genLotList == null)
                            {
                                _genLotList = new List<GenLotItemTable>();
                            }

                            _genLotList.Clear();

                            GenLotItemTable genLotPrint = new GenLotItemTable();
                            genLotPrint.ID = reprintItems.itemId;
                            genLotPrint.Row = 1;
                            genLotPrint.LotNumber = reprintItems.lotNo;
                            genLotPrint.Attributes = reprintItems.att;
                            genLotPrint.GrossWeight = (double)reprintItems.grossWeight;
                            genLotPrint.NetWeight = (double)reprintItems.netWeight;
                            genLotPrint.VendorRef = reprintItems.vendor;
                            genLotPrint.IsPrinted = false;
                            genLotPrint.IsSentToB1 = false;

                            _genLotList.Add(genLotPrint);

                            if (_genLotInstance == null)
                            {
                                _genLotInstance = new GenLot();
                            }

                            _genLotInstance.ID = (long)_entities.GenLotItem.Where(i => i.ItemID == reprintItems.itemId)
                                ?.FirstOrDefault()?.ID;
                            _genLotInstance.LotNo = reprintItems.lotNo;
                        }
                        else if (reprintItems.itemType == 3 || reprintItems.itemType == 4 ||
                                 reprintItems.itemType == 9 || reprintItems.itemType == 10 ||
                                 reprintItems.itemType == 11)
                        {
                            //Wip & FG
                            if (_jobReceiveList == null)
                            {
                                _jobReceiveList = new BindingList<JobReceiveTable>();
                            }

                            _jobReceiveList.Clear();

                            JobReceiveTable jrt = new JobReceiveTable();
                            jrt.ItemID = reprintItems.itemId;
                            jrt.RowNo = 1;
                            jrt.LotNo = reprintItems.lotNo;
                            jrt.Attributes = reprintItems.att;
                            jrt.AmountPerPackage = (int)reprintItems.amountPerPack;
                            jrt.GrossWeight = (decimal)reprintItems.grossWeight;
                            jrt.NetWeight = (decimal)reprintItems.netWeight;
                            jrt.IsPrinted = false;
                            jrt.IsSentToB1 = false;

                            _jobReceiveList.Add(jrt);
                            _jobReceiveItemType = _entities.ItemType.Where(i => i.ID == reprintItems.itemType)
                                ?.FirstOrDefault();

                            if (_jobReceiveInstance == null)
                            {
                                _jobReceiveInstance = new JobReceive();
                            }

                            _jobReceiveInstance.ID = (long)_entities.JobReceiveItem
                                .Where(i => i.ItemID == reprintItems.itemId)?.FirstOrDefault()?.ID;
                            _jobReceiveInstance.AssignDate = reprintItems.createDate;
                        }

                        isreprint = true;
                        reprintInfoPnl.Enabled = true;
                    }
                    else
                    {
                        MessageBox.Show("ID ไม่ถูกต้อง");
                    }

                }
            }
            catch (Exception ex)
            {
                log.Error("Method: checkIdBtn_Click() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                WriteLog(0, "RePrint", "Print BC by ID", $"ตรวจสอบข้อมูลสินค้าจาก ID: {itemIdTxb.Text}");
                log.Info("End Method: checkIdBtn_Click()");
            }
        }

        private void rePrintBcBtn_Click(object sender, EventArgs e)
        {
            log.Info("Start Method: rePrintBcBtn_Click()");

            try
            {
                //AOMM
                if (isreprint && _genLotList?.Count == 1)
                {
                    GLHandlePrintBarcode(1, 1);
                    _genLotList.Clear();
                }
                else if (isreprint && _jobReceiveList?.Count == 1)
                {
                    IPHandlePrintBarcode(1, 1);
                    _jobReceiveList.Clear();
                }
                else
                {
                    MessageBox.Show("ไม่สามารถพิมพ์บาร์โค้ดนี้ได้");
                }
            }
            catch (Exception ex)
            {
                log.Error("Method: rePrintBcBtn_Click() Error: " + ex.Message);
                MessageBox.Show("Error: " + ex.Message);
            }
            finally
            {
                isreprint = false;
                reprintInfoPnl.Enabled = false;
                itemCodetxb.Text = "";
                lotNoTxb.Text = "";
                createDateTxb.Text = "";
                grossWgTxb.Text = "";

                WriteLog(0, "RePrint", "Print BC by ID", $"พิมพ์บาร์โค้ดจากสินค้า ID: {itemIdTxb.Text}");
                log.Info("End Method: rePrintBcBtn_Click()");
            }
        }

        private void gL2GenLotTableDGV_EditingControlShowing(object sender,
            DataGridViewEditingControlShowingEventArgs e)
        {
            e.Control.KeyPress -= GrossWeightColumn_KeyPress;
            //var textbox = gL2GenLotTableDGV.Columns[gL2GenLotTableDGV.CurrentCell.ColumnIndex] as TextBox;
            if (gL2GenLotTableDGV.Columns[gL2GenLotTableDGV.CurrentCell.ColumnIndex].Name == "GrossWeight")
            {
                var tb = e.Control as TextBox;
                if (tb != null)
                {
                    tb.KeyPress += GrossWeightColumn_KeyPress;
                }
            }
        }

        private void GrossWeightColumn_KeyPress(object sender, KeyPressEventArgs e)
        {
            EnforceDecimalKeyPress(e);
        }

        private void dL2DeliveryDgv_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void aI1TypeDgv_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void aU1UserListDgv_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void dL3DeliveryDgv_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            var row = dL3DeliveryDgv.Rows[e.RowIndex];

            if (row.Cells[e.ColumnIndex] is DataGridViewButtonCell button)
            {

            }
        }
    }
}
//.HasValue ? user.CreationDate.Value.ToString("d MMMM yyyy", CultureInfo.CreateSpecificCulture("th-TH")) : "ไม่มีข้อมูล"
