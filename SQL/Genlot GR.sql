SELECT T0.[DocNum] AS 'GR_NO', T0.[DocEntry] AS 'GR_DocEntry', T0.[DocStatus] AS 'GR_DocStatus'
                , T0.[CreateDate] AS 'GR_CreateDate', T0.[CardCode] AS 'Customer_Code', T0.[CardName] AS 'Customer_Name'
                , T1.[LineNum] AS 'GR_Item_RowNo', T1.[LineStatus] AS 'GR_Item_RowStatus', T1.[ItemCode] AS 'GR_Item_Code'
                , T1.[Dscription] AS 'GR_Item_Name', T1.[Quantity] AS 'GR_Item_Qty', T1.[WhsCode] AS 'GR_Item_WH'
                , T4.[DistNumber] AS 'Batch_No', T4.[MnfSerial] AS 'Batch_Att_1', T4.[InDate] AS 'Batch_Adm_Date'
                , T6.[ItmsGrpCod] AS 'Item_Group_Code', T6.[ItmsGrpNam] AS Item_Group_Name, T7.[BatchNum] AS 'Item_Batch_Name'
                , T7.[SuppSerial] AS 'Item_Batch_Att_1', T7.[InDate] AS 'Item_Batch_Adm_Date', T7.[WhsCode] AS 'Item_Batch_WH', T7.[Quantity] AS 'Item_Batch_Qty'
                , T8.[DocNum] AS 'PO_No', T8.[DocEntry] AS 'PO_DocEntry'
                FROM OPDN T0
                    INNER JOIN PDN1 T1 ON T0.[DocEntry] = T1.[DocEntry]
                left join OITL T2 on t1.docentry = T2.[ApplyEntry] and T1.[LineNum] = T2.[ApplyLine] and T2.[ApplyType] = 20
                left JOIN ITL1 T3 ON T2.LogEntry = T3.LogEntry
                left join OBTN T4 on T4.[ItemCode] = T3.[ItemCode] and T3.[MdAbsEntry] = t4.[absentry]
                INNER JOIN OITM T5 ON T1.[ItemCode] = T5.[ItemCode]
                INNER JOIN OITB T6 ON T5.[ItmsGrpCod] = T6.[ItmsGrpCod]
                INNER JOIN OIBT T7 ON T5.[ItemCode] = T7.[ItemCode] and T4.[DistNumber] = T7.[BatchNum]
                left join OPOR T8 ON T8.DocEntry = T1.[BaseEntry]
                left JOIN POR1 T9 ON T9.[TrgetEntry] = T0.[DocEntry] 
                WHERE T0.[DocStatus] = 'O' and T1.[LineStatus] = 'O'
                and T1.WhsCode = 'SQC' and T6.[ItmsGrpCod] = 101
                and T7.[WhsCode] = 'SQC'
                and isnull(T7.Quantity, 0) > 0