                                    SELECT T0.[DocEntry]
                                    , T0.[DocNum]
                                    , T0.[DocType]
                                    , T0.[DocStatus]
                                    , T0.[CreateDate]
                                    , T0.[DocDueDate]
                                    , T0.[CardCode] AS 'Customer_Code'
                                    , T0.[CardName] AS 'Customer_Name'
                                    , T0.[NumAtCard] AS 'Cusmoter_ContractNo'
                                    , T0.[Address] AS 'BillTo'
                                    , T0.[Address2] AS 'ShipTo'
                                    , T1.[LineNum]
                                    , T1.[ItemCode]
                                    , T2.[ItemName]
                                    , T1.[ItemType]
                                    , T1.[WhsCode]
                                    , T1.[ShipDate]
                                    , T1.[Quantity] AS 'Order_Qty'
                                    , T1.[OpenQty]
                                    , T2.[OnHand] AS 'Item_AllOnHand'
                                    , T3.OnHand AS 'Item_InWhOnHand'
                                    , IIF (T1.[ItemType] = 4, 
		                                    (Select top (1) a.BatchNum 
			                                    from (
			                                    SELECT Ta.[ItemCode],Ta.[InDate]
			                                    , Ta.[BatchNum], Ta.[WhsCode], Ta.[ItemName], Ta.[SuppSerial], Ta.[Quantity] 
			                                    FROM OIBT Ta 
			                                    WHERE Ta.[Quantity] > 0 and Ta.[ItemCode] = T1.[ItemCode] and Ta.[WhsCode] = T1.[WhsCode]
			                                    ) a
			                                    ORDER BY a.[InDate], a.[Quantity]  ASC) 
	                                    ,NULL )	AS 'LotSuggest'
                                    , IIF (T1.[ItemType] = 4, 
		                                    (Select top (1) a.[Quantity]
			                                    from (
			                                    SELECT Ta.[ItemCode],Ta.[InDate]
			                                    , Ta.[BatchNum], Ta.[WhsCode], Ta.[ItemName], Ta.[SuppSerial], Ta.[Quantity] 
			                                    FROM OIBT Ta 
			                                    WHERE Ta.[Quantity] > 0 and Ta.[ItemCode] = T1.[ItemCode] and Ta.[WhsCode] = T1.[WhsCode] 
			                                    ) a
			                                    ORDER BY a.[InDate], a.[Quantity]  ASC) 
	                                    ,NULL )	AS 'LotSuggest_Qty'
                                    FROM [dbo].[ORDR]  T0 
                                    INNER JOIN [dbo].[RDR1]  T1 ON T0.[DocEntry] = T1.[DocEntry] 
                                    LEFT JOIN [dbo].[OITM]  T2 ON T1.[ItemCode] = T2.[ItemCode] 
                                    LEFT JOIN OITW T3 ON T1.ItemCode = T3.ItemCode and T1.WhsCode = T3.WhsCode 
                                    WHERE T0.[DocStatus]  = 'O'  
                                    and ISNULL(T1.[OpenQty],0) > 0 
                                    and T0.[DocDueDate] >= GETDATE()-10