USE [SWS_Barcode]
GO
DELETE FROM [dbo].[GenLotItem]
GO
DELETE FROM [dbo].[GenLot]
GO
DELETE FROM [dbo].[TransferItem]
GO
DELETE FROM [dbo].[JobReceiveItem]
GO
DELETE FROM [dbo].[JobReceive]
GO
DELETE FROM [dbo].[IssueProductionRequest]
GO
DELETE FROM [dbo].[IssueProductionItem]
GO
DELETE FROM [dbo].[IssueProduction]
GO
DELETE FROM [dbo].[DeliveryOrderItem]
GO
DELETE FROM [dbo].[DeliveryArrangeDeliveryOrder]
GO
DELETE FROM [dbo].[DeliveryOrder]
GO
DELETE FROM [dbo].[Item]
GO