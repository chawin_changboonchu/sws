SELECT T0.[DocNum] AS 'JO_DocNum'
                        , T0.[DocEntry] AS 'JO_DocEntry'
                        , T0.[Status] AS 'JO_Status'
                        , T0.[ItemCode] AS 'JO_ItemCode'
                        , T2.[ItemName] AS 'JO_FGName' 
                        , T0.[ProdName] AS 'JO_ProdName'
                        , T0.[Warehouse] AS 'JO_Warehouse'
                        , T0.[StartDate] AS 'JO_StartDate'
                        , T0.[DueDate] AS 'JO_DueDate'
                        , T0.[PlannedQty] AS 'JO_PlannedQty'
                        , T0.[CmpltQty] AS 'JO_CmpltQty'
                        , (T0.[PlannedQty] - T0.[CmpltQty]) AS 'JO_OpenQty_FG'
                        , T0.[RjctQty] AS 'JO_RejectQty_FG'
                        , T0.[Uom] AS 'JO_Uom'
                        , T0.[OriginNum] AS 'JO_SaleOrder'
                        , T0.[CardCode] AS 'JO_CustomerID'
                        , T5.[CardName] AS 'J0_CustomenName'
                        , T2.[SWW] AS 'Barcode_ColorTag'
                        , T1.[LineNum] AS 'RM_LineNum'
                        , T1.[ItemCode] AS 'RM_ItemCode'
                        , T3.[ItemName] AS 'RM_ItemName'
                        , T1.[PlannedQty] AS 'RM_PlannedQty'
                        , T1.[IssuedQty] AS 'RM_IssuedQty'
                        , T1.[IssueType] AS 'RM_IssueType'
                        , T1.[wareHouse] AS 'RM_wareHouse'
                        , T1.[ItemType] AS 'RM_ItemType'
                        , T3.[InvntryUom] AS 'RM_InvntryUom'
                        , T4.[ItmsGrpCod] AS 'RM_ItemGroup'
                        , IIF (T1.[ItemType] = 4, 
		                        (Select top (1) a.BatchNum 
			                        from (
			                        SELECT Ta.[ItemCode],Ta.[InDate]
			                        , Ta.[BatchNum], Ta.[WhsCode], Ta.[ItemName], Ta.[SuppSerial], Ta.[Quantity] 
			                        FROM OIBT Ta 
			                        WHERE Ta.[Quantity] > 0 and Ta.[ItemCode] = T1.[ItemCode] and Ta.[WhsCode] = T1.[wareHouse] --and Ta.Quantity >= T1.[IssuedQty]
			                        ) a
			                        ORDER BY a.[InDate], a.[Quantity]  ASC) 
	                        ,NULL )	AS 'RM_LotSuggest'
                        , IIF (T1.[ItemType] = 4, 
		                        (Select top (1) a.[Quantity]
			                        from (
			                        SELECT Ta.[ItemCode],Ta.[InDate]
			                        , Ta.[BatchNum], Ta.[WhsCode], Ta.[ItemName], Ta.[SuppSerial], Ta.[Quantity] 
			                        FROM OIBT Ta 
			                        WHERE Ta.[Quantity] > 0 and Ta.[ItemCode] = T1.[ItemCode] and Ta.[WhsCode] = T1.[wareHouse] --and Ta.Quantity >= T1.[IssuedQty]
			                        ) a
			                        ORDER BY a.[InDate], a.[Quantity]  ASC) 
	                        ,NULL )	AS 'RM_LotSuggest_Qty'
                        FROM OWOR T0  
                        INNER JOIN WOR1 T1 ON T0.[DocEntry] = T1.[DocEntry]
                        left JOIN OITM T2 ON T0.[ItemCode] = T2.[ItemCode]
                        left JOIN OITM T3 ON T1.[ItemCode] = T3.[ItemCode] 
                        left JOIN OITB T4 ON T3.[ItmsGrpCod] = T4.[ItmsGrpCod] 
                        left JOIN OCRD T5 ON T0.[CardCode] = T5.[CardCode]
                        WHERE T0.[Status] = 'R' 
                        and T1.[IssueType] = 'M' 
                        and  (T0.[PlannedQty] - T0.[CmpltQty]) > 0
						--and T3.[ItemName] is not null
						--Order by T0.[ItemCode]
